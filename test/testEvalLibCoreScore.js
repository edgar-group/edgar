var should = require('should');
var lib = require('../common/libeval');

describe('Test evaluation functions: questionScore- 100% penalty all around', function () {
  it('simple - unanswered - 100% penalty all around', function () {
    var userAnswers = [];
    var correctAnswers = [2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.1,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: false,
        is_unanswered: true,
        is_partial: false,
        score: 0.1,
        score_perc: 0.1 / 15,
      });
  });

  it('simple - 1 correct / 1 answered - 100% penalty all around', function () {
    var userAnswers = [2];
    var correctAnswers = [2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1.0,
      });
  });

  it('simple - 0 correct / 1 answered - 100% penalty all around', function () {
    var userAnswers = [2];
    var correctAnswers = [3];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
      });
  });

  it('bad grading model - 0 points for correct - 0 correct / 1 answered - 100% penalty all around', function () {
    var userAnswers = [2];
    var correctAnswers = [3];
    var gradingModel = {
      correct_score: 0,
      incorrect_score: 5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: 5,
        score_perc: 0,
      });
  });

  it('INCORRECT - 2 correct / 1/2 answered - 100% penalty all around', function () {
    var userAnswers = [2, 4];
    var correctAnswers = [4, 5];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
      });
  });

  it('INCORRECT - 0 correct / 2 answered - 100% penalty all around', function () {
    var userAnswers = [2, 1];
    var correctAnswers = [4, 5];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
      });
  });

  it('CORRECT - 3 correct / 3 answered - 100% penalty all around', function () {
    var userAnswers = [2, 1, 3];
    var correctAnswers = [1, 2, 3];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1.0,
      });
  });
  it('INCORRECT - 2 correct, 1 wrong / 3 answered - 100% penalty all around', function () {
    var userAnswers = [2, 1, 3];
    var correctAnswers = [1, 2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
      });
  });
  it('INCORRECT - 2 correct, 3 wrong / 5 answered - 100% penalty all around', function () {
    var userAnswers = [1, 2, 3, 4, 5];
    var correctAnswers = [1, 2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -10,
      unanswered_score: 0,
    };
    var penalty_percentages = [100, 100, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -10,
        score_perc: -10 / 15,
      });
  });

  it('PARTIAL - 2 correct (50%-50% penalty), 1 wrong / 1 answered', function () {
    var userAnswers = [2];
    var correctAnswers = [1, 2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [50, 50, 100, 100, 100];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: true,
        score: 15 - 0.5 * (15 + 5),
        score_perc: (15 - 0.5 * (15 + 5)) / 15,
      });
  });

  it('PARTIAL - 2 correct (10% penalty ALL AROUND), 3 wrong / 5 answered', function () {
    var userAnswers = [1, 2, 3, 4, 5];
    var correctAnswers = [1, 2];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [20, 20, 20, 20, 20];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: true,
        score: 15 - 0.6 * (15 + 5),
        score_perc: (15 - 0.6 * (15 + 5)) / 15,
      });
  });
  it('PARTIAL - 2 correct (distinct penalties ALL AROUND), 3 wrong / 5 answered', function () {
    var userAnswers = [1, 2, 3, 4, 5];
    var correctAnswers = [1, 3];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0,
    };
    var penalty_percentages = [50, 40, 30, 20, 10];
    lib
      .getQuestionScore(userAnswers, correctAnswers, penalty_percentages, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: true,
        score: 15 - 0.7 * (15 + 5),
        score_perc: (15 - 0.7 * (15 + 5)) / 15,
      });
  });
});

// describe('Test should.js- 100% penalty all around', function() {

//     it('should evalute 5 as 5- 100% penalty all around', function() {
//         (2+3).should.be.exactly(5).and.be.a.Number();
//     });
//     it('should comapre objects like I expect it :)- 100% penalty all around', function() {
//         ({
//             a: 1,
//             b: 2,
//             c: 3,
//         }).should.eql({
//             a: 1,
//             c: 3,
//             b: 2
//         });
//     });
//     it('should comapre objects like I expect it :) (2)- 100% penalty all around', function() {
//         ({
//             a: 1,
//             b: 2,
//             c: 3,
//         }).should.not.eql({
//             a: 1,
//             c: 3,
//             b: 3
//         });
//     });
//     it('should comapre objects like I expect it :) (3)- 100% penalty all around', function() {
//         ({
//             a: 1,
//             b: 2,
//             c: 3,
//         }).should.not.eql({
//             a: 1,
//             c: 3,
//             b: '2'
//         });
//     });
// });
