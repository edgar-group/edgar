var should = require('should');
var assert = require('assert');
var lib = require('./../common/libeval');
var testService = require('./../services/testService');

var convertrs = function (arr) {
  var rv = {
    fields: [],
    rows: [],
  };
  var i;
  if (arr.length) {
    for (var property in arr[0]) {
      if (arr[0].hasOwnProperty(property)) {
        rv.fields.push({
          name: property,
        });
      }
    }
    arr.forEach(function (row) {
      var newrow = {};
      for (i = 0; i < rv.fields.length; ++i) {
        newrow['C' + i] = row[rv.fields[i].name];
      }
      rv.rows.push(newrow);
    });
  }
  return rv;
};

describe('Test evaluation functions: SQL questionScore (testEvalLib.js)', function () {
  it('00 - A) test convertrs function - empty', function () {
    var rs = [];
    convertrs(rs).should.eql({
      fields: [],
      rows: [],
    });
  });

  it('00 - B) test convertrs function - vanilla', function () {
    var rs = [
      {
        fname: 'Igor',
        lname: 'Ludi',
      },
      {
        fname: 'Darko',
        lname: 'M',
      },
    ];
    convertrs(rs).should.eql({
      fields: [
        {
          name: 'fname',
        },
        {
          name: 'lname',
        },
      ],
      rows: [
        {
          C0: 'Igor',
          C1: 'Ludi',
        },
        {
          C0: 'Darko',
          C1: 'M',
        },
      ],
    });
  });
  it('00 - C) test convertrs function - one row, empty objects', function () {
    var rs = [{}];
    convertrs(rs).should.eql({
      fields: [],
      rows: [{}],
    });
  });

  // getSQLQuestionScore: function(userRs, correctRs, checkTupleOrder, checkColumnOrder, gradingModel) {
  it('0 - both empty ', function () {
    var userRs = [];
    var correctRs = [];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 1, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1.0,
        hint: 'Correct. Well done!',
      });
  });

  it('1 - different row count ', function () {
    var userRs = [{}];
    var correctRs = [{}, {}];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 1, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Uneven row count.',
      });
  });

  it('2 - different columns count ', function () {
    var userRs = [{ a: 1 }, { a: 2 }];
    var correctRs = [{}, {}];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 1, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Different number of columns.',
      });
  });
  it('2 - different columns order (1), check ON ', function () {
    var userRs = [{ a: 1, b: 2 }];
    var correctRs = [{ b: 2, a: 1 }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 1, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Uneven columns order (checkColumnMode=1).',
      });
  });

  it('3 - different columns order (2), check OFF ', function () {
    var userRs = [{ a: 1, b: 2, c: 3, d: 4 }];
    var correctRs = [{ b: 2, a: 1, d: 4, c: 3 }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('3 - different columns order, ignore column CASE ', function () {
    var userRs = [{ a: 1, b: 2, c: 3, D: 4 }];
    var correctRs = [{ b: 2, A: 1, d: 4, c: 3 }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('4 - different columns names', function () {
    var userRs = [{ a: 1, b: 2, c: 3, D: 4 }];
    var correctRs = [{ b: 2, A: 1, d: 4, h: 3 }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: `Can not find correct recordset's column "h" in user's recordset.`,
      });
  });

  it('4 - plain row #3 difference ', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
    ];
    var correctRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 4, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Row #3: user(b) !== correct(b)): 2 !== 4',
      });
  });
  it('5 - row #3 difference in data types string vs int (NEW: datatypes are IGNORED!!!)', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
    ];
    var correctRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: '2', c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 15 / 15,
        hint: 'Correct. Well done!', //'Row #3: user(b) !== correct(b)): 2 !== 2'
      });
  });

  it('6 - different tuple order, equal sets (1)', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 3, b: 4, c: 5, D: 6 },
      { a: 4, b: 5, c: 6, D: 7 },
    ];
    var correctRs = [
      { a: 3, b: 4, c: 5, D: 6 },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 4, b: 5, c: 6, D: 7 },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), false, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('6 (B) - different tuple order, equal sets (1), trimmed strings', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 'x  ' },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 3, b: 4, c: 5, D: 6 },
      { a: 4, b: 5, c: 6, D: 'xx' },
    ];
    var correctRs = [
      { a: 3, b: 4, c: 5, D: 6 },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 1, b: 2, c: 3, D: '  x' },
      { a: 4, b: 5, c: 6, D: ' xx ' },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), false, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('7 - different tuple order, UNequal sets (2)', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 3, b: 4, c: 5, D: 6 },
      { a: 4, b: 5, c: 6, D: 7 },
    ];
    var correctRs = [
      { a: 3, b: 4, c: 6, D: 6 },
      { a: 2, b: 3, c: 4, D: 5 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 4, b: 5, c: 6, D: 7 },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), false, 3, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Cannot find user row #3 in correct rows (check tuple order is OFF).',
      });
  });

  it('8 - Realistic data set, equal)', function () {
    var userRs = [
      { first_name: 'Denis', last_name: 'Dudak' },
      { first_name: 'Ivan', last_name: 'Radak' },
      { first_name: 'Sven', last_name: 'Vidak' },
      { first_name: 'Željko', last_name: 'Findak' },
      { first_name: 'Silvestar', last_name: 'Badak' },
      { first_name: 'Jakov', last_name: 'Vidak' },
      { first_name: 'Duje', last_name: 'Medak' },
      { first_name: 'Jurica', last_name: 'Vidak' },
    ];
    var correctRs = [
      { first_name: 'Denis', last_name: 'Dudak' },
      { first_name: 'Ivan', last_name: 'Radak' },
      { first_name: 'Sven', last_name: 'Vidak' },
      { first_name: 'Željko', last_name: 'Findak' },
      { first_name: 'Silvestar', last_name: 'Badak' },
      { first_name: 'Jakov', last_name: 'Vidak' },
      { first_name: 'Duje', last_name: 'Medak' },
      { first_name: 'Jurica', last_name: 'Vidak' },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), false, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('9 - testExecCode', function (done) {
    var userSQL = `SELECT imestudent, prezimestudent from student WHERE trim(prezimestudent) like '%ić'`;
    var correctSQL = `SELECT imestudent, prezimestudent from student WHERE trim(prezimestudent) like '%ić'`;
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .execAndGradeSQL(testService, 13, 1, userSQL, correctSQL, false, 3, gradingModel)
      .then(function (result) {
        result.should.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1,
          hint: 'Correct. Well done!',
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('10 - testExecCode different results', function (done) {
    var userSQL = `SELECT first_name, last_name from student WHERE last_name like '%dek'`;
    var correctSQL = `SELECT first_name, last_name from student WHERE last_name like '%dak'`;
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .execAndGradeSQL(testService, 13, 1, userSQL, correctSQL, false, 3, gradingModel)
      .then(function (result) {
        result.should.not.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1,
          hint: 'Correct. Well done!',
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('11 - testExecCode different results', function (done) {
    var userSQL = `SELECT firrst_name ha ha ha`;
    var correctSQL = `SELECT first_name, last_name from student WHERE last_name like '%dak'`;
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .execAndGradeSQL(testService, 13, 1, userSQL, correctSQL, false, 3, gradingModel)
      .then(function (result) {
        result.should.not.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1,
          hint: 'Correct. Well done!',
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('12 - Realistic data set, double column names)', function () {
    var userRs = {
      fields: [
        {
          name: 'abc',
        },
        {
          name: 'abc',
        },
      ],
      rows: [
        {
          C0: 'Denis',
          C1: 'Dudak',
        },
        {
          C0: 'Ivan',
          C1: 'Radak',
        },
        {
          C0: 'Sven',
          C1: 'Vidak',
        },
        {
          C0: 'Željko',
          C1: 'Findak',
        },
        {
          C0: 'Silvestar',
          C1: 'Badak',
        },
        {
          C0: 'Jakov',
          C1: 'Vidak',
        },
        {
          C0: 'Duje',
          C1: 'Medak',
        },
        {
          C0: 'Jurica',
          C1: 'Vidak',
        },
      ],
    };
    var correctRs = {
      fields: [
        {
          name: 'abc',
        },
        {
          name: 'abc',
        },
      ],
      rows: [
        {
          C0: 'Denis',
          C1: 'Dudak',
        },
        {
          C0: 'Ivan',
          C1: 'Radak',
        },
        {
          C0: 'Sven',
          C1: 'Vidak',
        },
        {
          C0: 'Željko',
          C1: 'Findak',
        },
        {
          C0: 'Silvestar',
          C1: 'Badak',
        },
        {
          C0: 'Jakov',
          C1: 'Vidak',
        },
        {
          C0: 'Duje',
          C1: 'Medak',
        },
        {
          C0: 'Jurica',
          C1: 'Vidak',
        },
      ],
    };
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), false, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 1,
        hint: 'Correct. Well done!',
      });
  });

  it('13 - Realistic data set, double column names, wrong order)', function () {
    var userRs = {
      fields: [
        {
          name: 'abc',
        },
        {
          name: 'abc',
        },
      ],
      rows: [
        {
          C0: 'Denis',
          C1: 'Dudak',
        },
        {
          C0: 'Ivan',
          C1: 'Radak',
        },
      ],
    };
    var correctRs = {
      fields: [
        {
          name: 'abc',
        },
        {
          name: 'abc',
        },
      ],
      rows: [
        {
          C1: 'Denis',
          C0: 'Dudak',
        },
        {
          C1: 'Ivan',
          C0: 'Radak',
        },
      ],
    };
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib.getSQLQuestionScore(userRs, correctRs, false, 1, gradingModel).should.eql({
      is_correct: false,
      is_incorrect: true,
      is_unanswered: false,
      is_partial: false,
      score: -5,
      score_perc: -5 / 15,
      hint: 'Cannot find user row #1 in correct rows (check tuple order is OFF).',
    });
  });

  it('14 - testExecCode, bad natural join (double columns)', function (done) {
    var userSQL = `SELECT * from mjesto JOIN zupanija ON mjesto.sifzupanija = zupanija.sifzupanija`;
    var correctSQL = `SELECT mjesto.*, nazzupanija from mjesto JOIN zupanija ON mjesto.sifzupanija = zupanija.sifzupanija`;
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .execAndGradeSQL(testService, 13, 1, userSQL, correctSQL, false, 3, gradingModel)
      .then(function (result) {
        result.should.eql({
          is_correct: false,
          is_incorrect: true,
          is_unanswered: false,
          is_partial: false,
          score: -5,
          score_perc: -5 / 15,
          hint: 'Different number of columns.',
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });
});
