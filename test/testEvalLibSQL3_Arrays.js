var should = require('should');
var assert = require('assert');
var lib = require('./../common/libeval');
var testService = require('./../services/testService');

var convertrs = function (arr) {
  var rv = {
    fields: [],
    rows: [],
  };
  var i;
  if (arr.length) {
    for (var property in arr[0]) {
      if (arr[0].hasOwnProperty(property)) {
        rv.fields.push({
          name: property,
        });
      }
    }
    arr.forEach(function (row) {
      var newrow = {};
      for (i = 0; i < rv.fields.length; ++i) {
        newrow['C' + i] = row[rv.fields[i].name];
      }
      rv.rows.push(newrow);
    });
  }
  return rv;
};

describe('Test evaluation functions: SQL questionScore (testEvalLib.js)', function () {
  it('00 - Array row #1 - EQUAL toString representation', function () {
    var userRs = [{ a: 1, b: 2, c: 3, D: [1, 2, 3, 4, 'luce'] }];
    var correctRs = [{ a: 1, b: 2, c: 3, D: [1, 2, 3, 4, 'luce'] }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 15 / 15,
        hint: 'Correct. Well done!',
      });
  });
  it('01 - Array row #3 - UN-EQUAL toString representation', function () {
    var userRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: [1, 2, 3, 4, 'luce'] },
    ];
    var correctRs = [
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: 4 },
      { a: 1, b: 2, c: 3, D: [1, 2, 3, 4, 'marin'] },
    ];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: false,
        is_incorrect: true,
        is_unanswered: false,
        is_partial: false,
        score: -5,
        score_perc: -5 / 15,
        hint: 'Row #3: user(D) !== correct(D)): 1,2,3,4,luce !== 1,2,3,4,marin',
      });
  });
  it('03 - NESTED Array row #1 - EQUAL toString representation', function () {
    var userRs = [{ a: 1, b: 2, c: 3, D: [1, 2, 3, 4, ['marin', 'luce']] }];
    var correctRs = [{ a: 1, b: 2, c: 3, D: [1, 2, 3, 4, ['marin', 'luce']] }];
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib
      .getSQLQuestionScore(convertrs(userRs), convertrs(correctRs), true, 3, gradingModel)
      .should.eql({
        is_correct: true,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: false,
        score: 15,
        score_perc: 15 / 15,
        hint: 'Correct. Well done!',
      });
  });
});
