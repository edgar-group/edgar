var assert = require('assert');
var should = require('should');
var lib = require('./../common/libeval');
var testService = {}; //require('./../services/testService');
var utils = require('./../common/utils');

var inputEvalDataNumb = {
  inputs: [
    { id: 0, input: '13 19 16 ' },
    { id: 1, input: '9 3 2 ' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '50.00',
      diff_order: true,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '13 19 16 ',
    },
    {
      id: 1,
      percentage: '50.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '9 3 2 ',
    },
  ],
  inputsFixed: [{ id: 2, input: '1 2 3' }],
  evalConfigsFixed: [
    {
      id: 2,
      input: '1 2 3',
      output: '1 2 3 ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
    },
  ],
};

var inputEvalDataNumbFixed = {
  inputsFixed: [{ id: 0, input: '1 2 3' }],
  evalConfigsFixed: [
    {
      id: 0,
      input: '1 2 3',
      output: '1 2 3 ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
    },
  ],
};

var inputEvalDataStringAll = {
  inputs: [
    { id: 0, input: '0ELk"]$k,i}O7&o>S vm' },
    { id: 1, input: 'SR15~+If>@p/,)3X*l,*' },
    { id: 2, input: "$l<$9,V{vmd[dV]@'*}7" },
    { id: 3, input: 'X?~=(4\\?u&SOU+W&Xp! ' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '0ELk"]$k,i}O7&o>S vm',
    },
    {
      id: 1,
      percentage: '40.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: false,
      regex: '',
      input: 'SR15~+If>@p/,)3X*l,*',
    },
    {
      id: 2,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: true,
      regex: '',
      input: "$l<$9,V{vmd[dV]@'*}7",
    },
    {
      id: 3,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: false,
      regex: '',
      input: 'X?~=(4\\?u&SOU+W&Xp! ',
    },
  ],
  inputsFixed: [{ id: 4, input: 'a b c d e f g h i j k l m n o p q u r s ' }],
  evalConfigsFixed: [
    {
      id: 4,
      input: 'a b c d e f g h i j k l m n o p q u r s ',
      output: 'a b c d e f g h i j ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
    },
  ],
};

var inputEvalDataStringAlpha = {
  inputs: [
    { id: 0, input: 'aBcDeFgHiJkLmNoPqRsT' },
    { id: 1, input: '0 a 1 B 2 c 3 D 4 e 5 f' },
    { id: 2, input: 'aaaa1aaaa777\nbbb\nzzz' },
    { id: 3, input: 'a\tb\tc\td\tA\tB\tC\tD\tE\tW\t' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: 'aBcDeFgHiJkLmNoPqRsT',
    },
    {
      id: 1,
      percentage: '40.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: false,
      regex: '',
      input: '0 a 1 B 2 c 3 D 4 e 5 f',
    },
    {
      id: 2,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: true,
      regex: '',
      input: 'aaaa1aaaa777\nbbb\nzzz',
    },
    {
      id: 3,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: false,
      regex: '',
      input: 'a\tb\tc\td\tA\tB\tC\tD\tE\tW\t',
    },
  ],
  inputsFixed: [{ id: 4, input: 'a b c d e f g h i j k l m n o p q u r s ' }],
  evalConfigsFixed: [
    {
      id: 4,
      input: 'a b c d e f g h i j k l m n o p q u r s ',
      output: 'a b c d e f g h i j ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
    },
  ],
};

var correctSolutionNumb = `#include <stdio.h>
    #include <stdlib.h>
    int main(){
        int a,b,c;
        scanf("%d %d %d", &a, &b, &c);
        printf("%d %d %d ", a,b,c);
        return 0;
    }`;

var correctSolutionString = `#include <stdio.h>
    #include <stdlib.h>
    int main(){
        char c;
        int i;
        for(i=0; i<20; i++){
            scanf("%c", &c);
            if( (c >= 65 && c <= 90) || (c >= 97 && c<= 122 )){
                printf("%c ", c);
            }
        }
        return 0;
    }`;

var id_c_programming_language = 4;
var id_course = 2007;

describe('Test evaluation functions: C questionScore (testEvalLibCodeRunner.2.js)', function () {
  //grade test
  it('13. Should pass: test with two questions with three C type tests', function (done) {
    //this.timeout(5000);
    var sqlGlobal = [
      {
        ordinal: 1,
        answers_permutation: null,
        weights_permutation: null,
        student_answers: null,
        correct_answers_permutation: null,
        sql_alt_assertion: '',
        sql_test_fixture: '',
        student_answer_code: `
                            #include <stdio.h>
                            #include <stdlib.h>
                            
                            int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        student_answer_code_pl: 4,
        sql_answer: null,
        check_tuple_order: false,
        id_check_column_mode: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '30.00',
        id_test_part: 11,
        id: 100,
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_question_id: 1,
        c_question_answer_id: 1,
        id_programming_language: 4,
      },
      {
        ordinal: 2,
        answers_permutation: null,
        weights_permutation: null,
        student_answers: null,
        correct_answers_permutation: null,
        sql_alt_assertion: '',
        sql_test_fixture: '',
        student_answer_code: `
                            #include <stdio.h>
                            #include <stdlib.h>
                            
                            int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        student_answer_code_pl: 4,
        sql_answer: null,
        check_tuple_order: false,
        id_check_column_mode: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '30.00',
        id_test_part: 11,
        id: 101,
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_question_id: 2,
        c_question_answer_id: 2,
        id_programming_language: 4,
      },
    ];
    var tests = [
      {
        //fixed without output (20%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 1, //fixed
        percentage: 20.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: '0 1 2 3 4 5 6 7 8 9',
        output: null,
        random_test_type_id: null,
        low_bound: null,
        upper_bound: null,
        elem_count: null,
        arguments: null,
        file: null,
        is_public: false,
      },
      {
        //random (80%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 2, //fixed
        percentage: 80.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: null,
        output: null,
        random_test_type_id: 1, //integers
        low_bound: 1,
        upper_bound: 1,
        elem_count: 10,
        arguments: null,
        file: null,
        is_public: false,
      },
      {
        //fixed with output - elimination (100%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 1, //fixed
        percentage: 100.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: '0 1 2 3 4 5 6 7 8 9',
        output: '0 9',
        random_test_type_id: null,
        low_bound: null,
        upper_bound: null,
        elem_count: null,
        arguments: null,
        file: null,
        is_public: false,
      },
    ];

    var db = {};
    db.any = function (sql, params) {
      if (params.c_question_answer_id) {
        return new Promise(function (resolve, reject) {
          return resolve(tests);
        });
      } else {
        return new Promise(function (resolve, reject) {
          return resolve(sqlGlobal);
        });
      }
    };
    var result = {
      status: {
        id: 14,
        description: 'Finished',
      },
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '1 1',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
      ],
    };
    var testServiceMock = {};
    testServiceMock.execCCodeWithInputsTI = function (dontCare, alsoDontCare, alsoDontCare2, aDC3) {
      return new Promise(function (resolve, reject) {
        return resolve(result);
      });
    };

    lib
      .gradeTest(1, db, testServiceMock, id_course)
      .then(function (rv) {
        delete rv.qscores[0].j0;
        delete rv.qscores[1].j0;
        rv.should.eql({
          correct_no: 2,
          incorrect_no: 0,
          unanswered_no: 0,
          partial_no: 0,
          score: 30,
          passed: true,
          score_perc: 1.0,
          qscores: [
            {
              id: 100,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15.0,
              score_perc: 1.0,
              hint: 'Correct. Well done!',
              c_outcome: [
                {
                  expected: '0 9',
                  input: '0 1 2 3 4 5 6 7 8 9',
                  percentage: 20,
                  hint: 'Correct. Well done!',
                  output: '0 9',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '1 1 1 1 1 1 1 1 1 1 ',
                  expected: '1 1',
                  hint: 'Correct. Well done!',
                  output: '1 1',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 80,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '0 1 2 3 4 5 6 7 8 9',
                  expected: '0 9',
                  hint: 'Correct. Well done!',
                  output: '0 9',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 100,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
              ],
            },
            {
              id: 101,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15.0,
              score_perc: 1.0,
              hint: 'Correct. Well done!',
              c_outcome: [
                {
                  input: '0 1 2 3 4 5 6 7 8 9',
                  expected: '0 9',
                  hint: 'Correct. Well done!',
                  output: '0 9',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 20,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '1 1 1 1 1 1 1 1 1 1 ',
                  expected: '1 1',
                  hint: 'Correct. Well done!',
                  output: '1 1',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 80,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '0 1 2 3 4 5 6 7 8 9',
                  expected: '0 9',
                  hint: 'Correct. Well done!',
                  output: '0 9',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 100,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
              ],
            },
          ],
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
      });
  });

  //grade test
  it('14. Should pass: test with three questions (correct, partial, incorrect)', function (done) {
    //this.timeout(5000);
    var sqlGlobal = [
      {
        ordinal: 1,
        student_answer_code: `
                            #include <stdio.h>
                            #include <stdlib.h>
                            
                            int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        student_answer_code_pl: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '45.00',
        id_test_part: 11,
        id: 100,
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_question_id: 1,
        c_question_answer_id: 1,
        id_programming_language: 4,
      },
      {
        ordinal: 2,
        student_answer_code: `
                            #include <stdio.h>
                            #include <stdlib.h>
                            
                            int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        student_answer_code_pl: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '45.00',
        id_test_part: 11,
        id: 101,
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_question_id: 2,
        c_question_answer_id: 2,
        id_programming_language: 4,
      },
      {
        ordinal: 3,
        student_answer_code: `
                            #include <stdio.h>
                            #include <stdlib.h>
                            
                            int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        student_answer_code_pl: 4,
        correct_score: '15.00',
        incorrect_score: '-5.00',
        unanswered_score: '0.00',
        threshold: 1,
        test_pass_perc: '0.0',
        tp_pass_perc: '0.0',
        max_score: '45.00',
        id_test_part: 11,
        id: 102,
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_question_id: 2,
        c_question_answer_id: 2,
        id_programming_language: 4,
      },
    ];
    var tests = [
      {
        //fixed without output (20%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 1, //fixed
        percentage: 20.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: '20 20 20 20 20 20 20 20 20 20',
        output: '20 20',
        random_test_type_id: null,
        low_bound: null,
        upper_bound: null,
        elem_count: null,
        arguments: null,
        file: null,
        is_public: false,
      },
      {
        //random (80%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 1, //fixed
        percentage: 80.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: '80 80 80 80 80 80 80 80 80 80',
        output: '80 80',
        random_test_type_id: null,
        low_bound: null,
        upper_bound: null,
        elem_count: null,
        arguments: null,
        file: null,
        is_public: false,
      },
      {
        //fixed with output - elimination (100%)
        c_prefix: `#include <stdio.h>
                            #include <stdlib.h>`,
        c_source: `int main(){
                            int i, c, max=-1000, min=1000;
                            for(i=0; i<10; i++){
                                scanf("%d", &c);
                                if(c > max) max = c;
                                if(c < min) min = c;
                            }
                            printf("%d %d", min, max);
                            return 0;
                        }`,
        c_suffix: '',
        c_test_type_id: 1, //fixed
        percentage: 100.0,
        allow_diff_order: true,
        allow_diff_letter_size: false,
        trim_whitespace: true,
        regex_override: '',
        input: '100 100 100 100 100 100 100 100 100 100',
        output: '100 100',
        random_test_type_id: null,
        low_bound: null,
        upper_bound: null,
        elem_count: null,
        arguments: null,
        file: null,
        is_public: false,
      },
    ];
    var db = {};
    db.any = function (sql, params) {
      if (params.c_question_answer_id) {
        return new Promise(function (resolve, reject) {
          return resolve(tests);
        });
      } else {
        return new Promise(function (resolve, reject) {
          return resolve(sqlGlobal);
        });
      }
    };
    var resultPartial = {
      status: {
        id: 14,
        description: 'Finished',
      },
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 0',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '80 80',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '100 100',
        },
      ],
    };
    var resultWrong = {
      status: {
        id: 14,
        description: 'Finished',
      },
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '20 20',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '80 80',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 0',
        },
      ],
    };
    var resultCorrect = {
      status: {
        id: 14,
        description: 'Finished',
      },
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '20 20',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '80 80',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '100 100',
        },
      ],
    };
    var testServiceMock = {};
    testServiceMock.execCCodeWithInputsTI = function (id_test_instance, ordinal, source, inputs) {
      return new Promise(function (resolve, reject) {
        if (ordinal == 2) {
          //partial => 80% = 0.8*15
          return resolve(resultPartial);
        } else if (ordinal == 3) {
          //wrong => -5 points
          return resolve(resultWrong);
        } else {
          //correct => 15 points
          return resolve(resultCorrect);
        }
      });
    };
    lib
      .gradeTest(1, db, testServiceMock)
      .then(function (rv) {
        delete rv.qscores[0].j0;
        delete rv.qscores[1].j0;
        delete rv.qscores[2].j0;
        rv.should.eql({
          correct_no: 1,
          incorrect_no: 1,
          unanswered_no: 0,
          partial_no: 1,
          score: 15.0 - 5.0 + (0.8 * (15 - -5) + -5),
          passed: true,
          score_perc: (15.0 - 5.0 + (0.8 * (15 - -5) + -5)) / 45,
          qscores: [
            {
              id: 100,
              is_correct: true,
              is_incorrect: false,
              is_partial: false,
              is_unanswered: false,
              score: 15.0,
              score_perc: 1.0,
              hint: 'Correct. Well done!',
              c_outcome: [
                {
                  input: '20 20 20 20 20 20 20 20 20 20',
                  expected: '20 20',
                  percentage: 20,
                  hint: 'Correct. Well done!',
                  output: '20 20',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '80 80 80 80 80 80 80 80 80 80',
                  expected: '80 80',
                  percentage: 80,
                  hint: 'Correct. Well done!',
                  output: '80 80',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '100 100 100 100 100 100 100 100 100 100',
                  expected: '100 100',
                  percentage: 100,
                  hint: 'Correct. Well done!',
                  output: '100 100',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
              ],
            },
            {
              id: 101,
              is_correct: false,
              is_incorrect: false,
              is_partial: true,
              is_unanswered: false,
              score: 0.8 * (15 - -5) + -5,
              score_perc: 0.8,
              hint: 'Partial score',
              c_outcome: [
                {
                  input: '20 20 20 20 20 20 20 20 20 20',
                  expected: '20 20',
                  hint: 'Incorrect output',
                  output: '0 0',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: false,
                  percentage: 20,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '80 80 80 80 80 80 80 80 80 80',
                  expected: '80 80',
                  hint: 'Correct. Well done!',
                  output: '80 80',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 80,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '100 100 100 100 100 100 100 100 100 100',
                  expected: '100 100',
                  hint: 'Correct. Well done!',
                  output: '100 100',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 100,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
              ],
            },
            {
              id: 102,
              is_correct: false,
              is_incorrect: true,
              is_partial: false,
              is_unanswered: false,
              score: -5.0,
              score_perc: -5 / 15.0,
              hint: 'Failed on all or major tests.',
              c_outcome: [
                {
                  input: '20 20 20 20 20 20 20 20 20 20',
                  expected: '20 20',
                  hint: 'Correct. Well done!',
                  output: '20 20',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 20,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '80 80 80 80 80 80 80 80 80 80',
                  expected: '80 80',
                  hint: 'Correct. Well done!',
                  output: '80 80',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: true,
                  percentage: 80,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
                {
                  input: '100 100 100 100 100 100 100 100 100 100',
                  expected: '100 100',
                  hint: 'Incorrect output',
                  output: '0 0',
                  mode: 'check elements order : true, case sensitive : true, ignore whitespace : true',
                  isCorrect: false,
                  percentage: 100,
                  comment: undefined,
                  stderr: undefined,
                  is_public: false,
                },
              ],
            },
          ],
        });
        done();
      })
      .catch(function (reason) {
        console.log('Failed:' + reason);
        assert(false);
      });
  });
});
