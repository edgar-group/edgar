var assert = require('assert');
var lib = require('../common/libeval');
var should = require('should');

describe('Test evaluation functions: questionPassed', function () {
  it('should evalute question as passed - simple, all weights = 1, one correct answer', function () {
    var userAnswers = [3];
    var correctAnswers = [3];
    var weights = [1, 1, 1, 1, 1];
    var threshold = 1;

    assert.equal(true, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as failed - simple, all weights = 1, one wrong answer', function () {
    var userAnswers = [3];
    var correctAnswers = [2];
    var weights = [1, 1, 1, 1, 1];
    var threshold = 1;

    assert.equal(false, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as failed - simple, all weights = 1, no answers', function () {
    var userAnswers = [];
    var correctAnswers = [2];
    var weights = [1, 1, 1, 1, 1];
    var threshold = 1;

    assert.equal(false, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as passed - simple, all weights = 1, two correct answers, th=1', function () {
    var userAnswers = [3, 1];
    var correctAnswers = [1, 3];
    var weights = [1, 1, 1, 1, 1];
    var threshold = 1;

    assert.equal(true, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as passed - simple, all weights = 1, two correct answers, th=2', function () {
    var userAnswers = [3, 1];
    var correctAnswers = [1, 3];
    var weights = [1, 1, 1, 1, 1];
    var threshold = 2;

    assert.equal(true, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as passed - different weights, 50% correct answers, th=2', function () {
    var userAnswers = [3, 1];
    var correctAnswers = [2, 3];
    var weights = [1, 1, 2, 1, 1];
    var threshold = 2;

    assert.equal(true, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('should evalute question as failed - different weights, 50% correct answers, th=2', function () {
    var userAnswers = [3, 1];
    var correctAnswers = [2, 3];
    var weights = [2, 1, 1, 1, 1];
    var threshold = 2;

    assert.equal(false, lib.questionPassed(userAnswers, correctAnswers, weights, threshold));
  });

  it('can sum bools (return values)', function () {
    var userAnswers1 = [3, 1];
    var correctAnswers1 = [2, 3, 1];
    var weights1 = [2, 1, 3, 1, 1];
    var threshold1 = 5;

    var userAnswers2 = [3, 1, 4];
    var correctAnswers2 = [2, 3, 4];
    var weights2 = [1, 1, 1, 1, 1];
    var threshold2 = 2;

    assert.equal(
      2,
      lib.questionPassed(userAnswers1, correctAnswers1, weights1, threshold1) +
        lib.questionPassed(userAnswers2, correctAnswers2, weights2, threshold2)
    );
  });
});

describe('Test evaluation functions: testPartPassed', function () {
  it('should evalute as passed - simple, empty arrays, 0% pass perc', function () {
    var userAnswers = [];
    var correctAnswers = [[3]];
    var weights = [[1, 1, 1, 1, 1]];
    var threshold = 1;
    var passPerc = 0.0;

    assert.equal(
      true,
      lib.testPartPassed(userAnswers, correctAnswers, weights, threshold, passPerc)
    );
  });

  it('should evalute as failed - simple, empty arrays, 1% pass perc', function () {
    var userAnswers = [];
    var correctAnswers = [[3]];
    var weights = [[1, 1, 1, 1, 1]];
    var threshold = 1;
    var passPerc = 0.01;

    assert.equal(
      false,
      lib.testPartPassed(userAnswers, correctAnswers, weights, threshold, passPerc)
    );
  });

  it('should evalute question as passed - various qs, 100% pass perc', function () {
    var userAnswers = [
      [3, 1],
      [3, 1],
      [1, 3],
    ];
    var correctAnswers = [
      [1, 3],
      [1, 3],
      [1, 3],
    ];
    var weights = [
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
    ];
    var threshold = 2;
    var passPerc = 1.0;
    assert.equal(
      true,
      lib.testPartPassed(userAnswers, correctAnswers, weights, threshold, passPerc)
    );
  });

  it('should evalute question as failed - various qs, 100% pass perc', function () {
    var userAnswers = [
      [3, 1],
      [3, 1],
      [1, 3],
    ];
    var correctAnswers = [
      [1, 3],
      [1, 3],
      [2, 3],
    ];
    var weights = [
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
    ];
    var threshold = 2;
    var passPerc = 1.0;
    assert.equal(
      false,
      lib.testPartPassed(userAnswers, correctAnswers, weights, threshold, passPerc)
    );
  });

  it('should evalute question as true - various qs, 66% pass perc', function () {
    var userAnswers = [
      [3, 1],
      [3, 1],
      [1, 3],
    ];
    var correctAnswers = [
      [1, 3],
      [1, 3],
      [2, 3],
    ];
    var weights = [
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
      [1, 1, 1, 1, 1],
    ];
    var threshold = 2;
    var passPerc = 0.66;
    assert.equal(
      true,
      lib.testPartPassed(userAnswers, correctAnswers, weights, threshold, passPerc)
    );
  });
});
