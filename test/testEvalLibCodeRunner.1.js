var assert = require('assert');
var should = require('should');
var lib = require('./../common/libeval');
var testService = {}; //require('./../services/testService');
var utils = require('./../common/utils');

/*
var evalConfig = [{
    input            : '',
    output           : '',
    percentage       : 100.00,
    diff_order       : false,
    case_insensitive : true,
    trim_whitespace  : true,
    regex            : ''
}];

*/
/*
 [ { percentage: '50.00',
    diff_order: true,
    case_insensitive: true,
    trim_whitespace: true,
    regex: '',
    input: '16 19 12 ',
    output: '16 19 12 ' },
  { percentage: '50.00',
    diff_order: false,
    case_insensitive: true,
    trim_whitespace: true,
    regex: '',
    input: '2 3 0 ',
    output: '2 3 0 ' },
  { input: '1 2 3',
    output: '1 2 3 ',
    percentage: '100.00',
    diff_order: false,
    case_insensitive: true,
    trim_whitespace: true,
    regex: '' } ]

       outputs :  [ { number: 0, isExecuted: true, data: '16 14 16 ' },
  { number: 1, isExecuted: true, data: '8 4 2 ' },
  { number: 2, isExecuted: true, data: '1 2 3 ' } ]


  hints:  [ { input: '13 11 19 ',
    hint: 'Correct. Well done!',
    output: '13 11 19 ',
    mode: 'allowed different order of elements : true, case insensitive : true, trimed and reduced whitespaces : true',
    isCorrect: true },
  { input: '2 5 0 ',
    hint: 'Correct. Well done!',
    output: '2 5 0 ',
    mode: 'allowed different order of elements : false, case insensitive : true, trimed and reduced whitespaces : true',
    isCorrect: true },
  { input: '1 2 3',
    hint: 'Correct. Well done!',
    output: '1 2 3 ',
    mode: 'allowed different order of elements : false, case insensitive : true, trimed and reduced whitespaces : true',
    isCorrect: true } ]



 inputEvalData :  { inputs: [ '13 19 16 ', '9 3 2 ' ],
  evalConfigs:
   [ { percentage: '50.00',
       diff_order: true,
       case_insensitive: true,
       trim_whitespace: true,
       regex: '',
       input: '13 19 16 ' },
     { percentage: '50.00',
       diff_order: false,
       case_insensitive: true,
       trim_whitespace: true,
       regex: '',
       input: '9 3 2 ' } ],
  inputsFixed: [ '1 2 3' ],
  evalConfigsFixed:
   [ { input: '1 2 3',
       output: '1 2 3 ',
       percentage: '100.00',
       diff_order: false,
       case_insensitive: true,
       trim_whitespace: true,
       regex: '' } ] }

-------------------
*/

var inputEvalDataNumb = {
  inputs: [
    { id: 0, input: '13 19 16 ' },
    { id: 1, input: '9 3 2 ' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '50.00',
      diff_order: true,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '13 19 16 ',
      is_public: false,
    },
    {
      id: 1,
      percentage: '50.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '9 3 2 ',
      is_public: false,
    },
  ],
  inputsFixed: [{ id: 2, input: '1 2 3' }],
  evalConfigsFixed: [
    {
      id: 2,
      input: '1 2 3',
      output: '1 2 3 ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      is_public: false,
    },
  ],
};

var inputEvalDataNumbFixed = {
  inputsFixed: [{ id: 0, input: '1 2 3' }],
  evalConfigsFixed: [
    {
      id: 0,
      input: '1 2 3',
      output: '1 2 3 ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      is_public: false,
    },
  ],
};

var inputEvalDataStringAll = {
  inputs: [
    { id: 0, input: '0ELk"]$k,i}O7&o>S vm' },
    { id: 1, input: 'SR15~+If>@p/,)3X*l,*' },
    { id: 2, input: "$l<$9,V{vmd[dV]@'*}7" },
    { id: 3, input: 'X?~=(4\\?u&SOU+W&Xp! ' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: '0ELk"]$k,i}O7&o>S vm',
      is_public: false,
    },
    {
      id: 1,
      percentage: '40.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: false,
      regex: '',
      input: 'SR15~+If>@p/,)3X*l,*',
      is_public: false,
    },
    {
      id: 2,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: true,
      regex: '',
      input: "$l<$9,V{vmd[dV]@'*}7",
      is_public: false,
    },
    {
      id: 3,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: false,
      regex: '',
      input: 'X?~=(4\\?u&SOU+W&Xp! ',
      is_public: false,
    },
  ],
  inputsFixed: [{ id: 4, input: 'a b c d e f g h i j k l m n o p q u r s ' }],
  evalConfigsFixed: [
    {
      id: 4,
      input: 'a b c d e f g h i j k l m n o p q u r s ',
      output: 'a b c d e f g h i j ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      is_public: false,
    },
  ],
};

var inputEvalDataStringAlpha = {
  inputs: [
    { id: 0, input: 'aBcDeFgHiJkLmNoPqRsT' },
    { id: 1, input: '0 a 1 B 2 c 3 D 4 e 5 f' },
    { id: 2, input: 'aaaa1aaaa777\nbbb\nzzz' },
    { id: 3, input: 'a\tb\tc\td\tA\tB\tC\tD\tE\tW\t' },
  ],
  evalConfigs: [
    {
      id: 0,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      input: 'aBcDeFgHiJkLmNoPqRsT',
      is_public: false,
    },
    {
      id: 1,
      percentage: '40.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: false,
      regex: '',
      input: '0 a 1 B 2 c 3 D 4 e 5 f',
      is_public: false,
    },
    {
      id: 2,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: false,
      trim_whitespace: true,
      regex: '',
      input: 'aaaa1aaaa777\nbbb\nzzz',
      is_public: false,
    },
    {
      id: 3,
      percentage: '20.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: false,
      regex: '',
      input: 'a\tb\tc\td\tA\tB\tC\tD\tE\tW\t',
      is_public: false,
    },
  ],
  inputsFixed: [{ id: 4, input: 'a b c d e f g h i j k l m n o p q u r s ' }],
  evalConfigsFixed: [
    {
      id: 4,
      input: 'a b c d e f g h i j k l m n o p q u r s ',
      output: 'a b c d e f g h i j ',
      percentage: '100.00',
      diff_order: false,
      case_insensitive: true,
      trim_whitespace: true,
      regex: '',
      is_public: false,
    },
  ],
};

var correctSolutionNumb = `#include <stdio.h>
    #include <stdlib.h>
    int main(){
        int a,b,c;
        scanf("%d %d %d", &a, &b, &c);
        printf("%d %d %d ", a,b,c);
        return 0;
    }`;

var correctSolutionString = `#include <stdio.h>
    #include <stdlib.h>
    int main(){
        char c;
        int i;
        for(i=0; i<20; i++){
            scanf("%c", &c);
            if( (c >= 65 && c <= 90) || (c >= 97 && c<= 122 )){
                printf("%c ", c);
            }
        }
        return 0;
    }`;

var id_c_programming_language = 4;
var id_course = 2007;

describe('Test evaluation functions: C questionScore (testEvalLibCodeRunner.1.js)', function () {
  // getCQuestionScore: function(evalConfig, studentResult, gradingModel) {
  //execAndGradeC: function(testService, id_test_instance, ordinal, userC, correctC, inputEvalData , gradingModel)

  it('0 - TIMEOUT error', function () {
    var evalConfig = {};
    var studentResult = {
      status: {
        id: 5,
        description: 'Time limit exceeded',
      },
      internal_message: 'Time limit exceeded',
      results: [],
    };
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    lib.getCQuestionScore(evalConfig, studentResult, gradingModel).should.eql({
      is_correct: false,
      is_incorrect: true,
      is_unanswered: false,
      is_partial: false,
      score: -5,
      score_perc: -5 / 15,
      hint: utils.cScoreHint(studentResult),
    });
  });

  it('1 - compile error', function (done) {
    this.timeout(2000);
    var userC = `not C code`;
    var correctC = correctSolutionNumb;

    var studentResult = {
      status: {
        id: 6,
        description: 'Compile error',
      },
      compile_output: 'Error : Command failed: gcc',
      internal_message: null,
      results: [],
    };
    var teacherResult = {
      status: {
        id: 14,
        description: 'Accepted',
      },
      compile_output: null,
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
        },
      ],
    };
    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };
    var testServiceMock = {
      execCCodeWithInputsTI: function (
        idTestInstance,
        ordinal,
        code,
        inputs,
        id_course,
        id_pl_student
      ) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    //doesnt matter what inputEvalData contains, as long as there is some input in it
    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3698,
        1,
        userC,
        correctC,
        inputEvalDataNumb,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        //result.should.have.property('hint');
        //result['hint'].should.match(.Error: Command failed: gcc.);

        //var recvHint = result['hint'];
        delete result.j0;

        result.should.eql({
          is_correct: false,
          is_incorrect: true,
          is_unanswered: false,
          is_partial: false,
          score: -5,
          score_perc: -5 / 15,
          hint: utils.cScoreHint(studentResult), //recvHint
          //"Error: Command failed: gcc -o source ./source.c ./source.c:1:1: error: unknown type name 'not' not C code ^ ./source.c:1:7: error: expected '=', ',', ';', 'asm' or '__attribute__' before 'code' not C code ^"
        });
        //maybe replace compile output with just "compile failed" ????????
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('2 - empty answer', function (done) {
    this.timeout(2000);

    var userC = ``;
    var correctC = correctSolutionNumb;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          resolve({});
        });
      },
    };

    //doesnt matter what inputEvalData contains, as long as there is some input in it
    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3698,
        1,
        userC,
        correctC,
        inputEvalDataNumb,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: false,
          is_unanswered: true,
          is_partial: false,
          score: 0.0,
          score_perc: 0.0 / 15,
          hint: 'Empty answer.',
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('3 - compile error in teachers code', function (done) {
    var userC = correctSolutionNumb;
    var correctC = `not C code`;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    var teacherResult = {
      status: {
        id: 6,
        description: 'Compile error',
      },
      compile_output: 'Error : Command failed: gcc',
      internal_message: null,
      results: [],
    };
    var studentResult = {
      status: {
        id: 14,
        description: 'Accepted',
      },
      compile_output: null,
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    //doesnt matter what inputEvalData contains, as long as there is some input in it
    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3698,
        1,
        userC,
        correctC,
        inputEvalDataNumb,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: false,
          is_unanswered: true,
          is_partial: false,
          score: 0.0,
          score_perc: 0.0 / 15,
          hint: "Question has a wrong answer (teacher's error!)!!",
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  //code runner will break the questioning before judge0 returns result
  // or if judge0s timelimit is smaller then coderunners then judge0 will return time limit exceeded
  it('4 - infinite loop answer', function (done) {
    this.timeout(5000);

    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                while(1){};
                return 0;
            }`;
    var correctC = correctSolutionNumb;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //this is for second version when judge0 breaks execution
    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 5,
            description: 'Time limit exceeded',
          },
        },
      ],
    };
    var teacherResult = {
      status: {
        id: 14,
        description: 'Accepted',
      },
      compile_output: null,
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    //doesnt matter what inputEvalData contains, as long as there is some input in it
    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3698,
        1,
        userC,
        correctC,
        inputEvalDataNumbFixed,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: true,
          is_unanswered: false,
          is_partial: false,
          score: -5,
          score_perc: -5 / 15,
          hint: 'Failed on all or major tests.',
          c_outcome: [
            {
              input: '1 2 3',
              percentage: '100.00',
              hint: 'Test execution failed: Time limit exceeded', //`Test execution failed: ${outputs[i].status.description}`
              output: utils.cScoreHint(studentResult.results[0]), //`Status: ${outputs[i].status.description}, Internal message: ${outputs[i].internal_message}`, //stderr ??
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: false,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  //includes case sensitive tests also !!!
  it('5 - ignore whitespace ', function (done) {
    this.timeout(5000);
    //prints an extra space, will fail on IGNORE WHITESPACE = false, will pass otherwise
    var userC = `#include <stdio.h>
        #include <stdlib.h>
        int main(){
            char c;
            int i;
            for(i=0; i<20; i++){
                scanf("%c", &c);
                if( (c >= 65 && c <= 90) || (c >= 97 && c<= 122 )){
                    printf("%c  ", c);
                }
            }
            return 0;
        }`;
    var correctC = correctSolutionString;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: 0,
      unanswered_score: 0.0,
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'E L k k i O o S v m  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'S R I f p X l  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'l V v m d d V  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'X u S O U W X p  ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'a b c d e f g h i j  ',
        },
      ],
    };
    var teacherResult = {
      status: {
        id: 14,
        description: 'Accepted',
      },
      compile_output: null,
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'E L k k i O o S v m ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'S R I f p X l ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'l V v m d d V ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'x u s o u w x p ',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'a b c d e f g h i j ',
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    //doesnt matter what inputEvalData contains, as long as there is some input in it
    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalDataStringAll,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: true,
          score: 0.4 * 15,
          score_perc: 0.4,
          hint: 'Partial score',
          c_outcome: [
            {
              input: '0ELk"]$k,i}O7&o>S vm',
              expected: 'E L k k i O o S v m ',
              percentage: '20.00',
              hint: 'Correct. Well done!',
              output: 'E L k k i O o S v m  ',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              comment: undefined,
              stderr: undefined,
              is_public: false,
            },
            {
              input: 'SR15~+If>@p/,)3X*l,*',
              expected: 'S R I f p X l ',
              percentage: '40.00',
              hint: 'Incorrect output',
              output: 'S R I f p X l  ',
              mode: 'check elements order : false, case sensitive : true, ignore whitespace : false',
              isCorrect: false,
              comment: undefined,
              stderr: undefined,
              is_public: false,
            },
            {
              input: "$l<$9,V{vmd[dV]@'*}7",
              expected: 'l V v m d d V ',
              percentage: '20.00',
              hint: 'Correct. Well done!',
              output: 'l V v m d d V  ',
              mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
              isCorrect: true,
              comment: undefined,
              stderr: undefined,
              is_public: false,
            },
            {
              input: 'X?~=(4\\?u&SOU+W&Xp! ',
              expected: 'x u s o u w x p ',
              percentage: '20.00',
              hint: 'Incorrect output',
              output: 'X u S O U W X p  ',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : false',
              isCorrect: false,
              comment: undefined,
              stderr: undefined,
              is_public: false,
            },
            {
              input: 'a b c d e f g h i j k l m n o p q u r s ',
              expected: 'a b c d e f g h i j ',
              percentage: '100.00',
              hint: 'Correct. Well done!',
              output: 'a b c d e f g h i j  ',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              comment: undefined,
              stderr: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  // it('6 - case sensitive ', function (done) {
  //     this.timeout(5000);
  //     //prints all letters as upper case, will fail on CASE SENSITIVE = true, will pass otherwise
  //     var userC = `#include <stdio.h>
  //         #include <stdlib.h>
  //         int main(){
  //             char c;
  //             int i;
  //             for(i=0; i<20; i++){
  //                 scanf("%c", &c);
  //                 if( c >= 'a' && c <= 'z'){
  //                     printf("%c ", c-'a'+'A');
  //                 }else if(c >= 'A' && c<= 'Z' ){
  //                     printf("%c ", c);
  //                 }
  //             }
  //             return 0;
  //         }`;
  //     var correctC = correctSolutionString;

  //     var gradingModel = {
  //         correct_score: 15,
  //         incorrect_score: -5,
  //         unanswered_score: 0.0
  //     };

  //     var studentResult = {
  //         status: {
  //             id: 14,
  //             description: "Finished"
  //         },
  //         internal_message: null,
  //         results: [
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'A B C D E F G H I J K L M N O P Q R S T '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'A B C D E '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'A A A A A A A A B B B Z Z Z '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'A B C D A B C D E W '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'A B C D E F G H I J '
  //             }
  //         ]
  //     };
  //     var teacherResult = {
  //         status: {
  //             id: 14,
  //             description: "Accepted"
  //         },
  //         compile_output: null,
  //         internal_message: null,
  //         results: [
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'a B c D e F g H i J k L m N o P q R s T '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'a B c D e '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'a a a a a a a a b b b z z z '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'a b c d A B C D E W '
  //             },
  //             {
  //                 status: {
  //                     id: 3,
  //                     description: "Accepted"
  //                 },
  //                 stdout: 'a b c d e f g h i j '
  //             }
  //         ]
  //     };

  //     var testServiceMock = {
  //         execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
  //             return new Promise((resolve, reject) => {
  //                 if (code == userC) resolve(studentResult);
  //                 else resolve(teacherResult);
  //             });
  //         }
  //     }

  //     //doesnt matter what inputEvalData contains, as long as there is some input in it
  //     lib.execAndGradeC(testServiceMock, undefined, 3700, 1, userC, correctC, inputEvalDataStringAlpha, gradingModel)
  //         .then(function (result) {
  //             delete result.j0;
  //             result.should.eql({
  //                 is_correct: false,
  //                 is_incorrect: false,
  //                 is_unanswered: false,
  //                 is_partial: true,
  //                 score: 0.4 * (15 - (-5)) + (-5),
  //                 score_perc: 0.4,
  //                 hint: 'Partial score',
  //                 c_outcome:
  //                     [{
  //                         input: 'aBcDeFgHiJkLmNoPqRsT',
  //                         expected: 'a b c d e f g h i j k l m n o p q r s t',
  //                         percentage: '20.00',
  //                         hint: 'Correct. Well done!',
  //                         output: 'A B C D E F G H I J K L M N O P Q R S T ',
  //                         mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
  //                         isCorrect: true
  //                     },
  //                     {
  //                         input: '0 a 1 B 2 c 3 D 4 e 5 f',
  //                         expected: 'a B c D e ',
  //                         percentage: '40.00',
  //                         hint: 'Incorrect output',
  //                         output: 'A B C D E ',
  //                         mode: 'check elements order : false, case sensitive : true, ignore whitespace : false',
  //                         isCorrect: false
  //                     },
  //                     {
  //                         input: 'aaaa1aaaa777\nbbb\nzzz',
  //                         expected: 'a a a a a a a a b b b z z z',
  //                         percentage: '20.00',
  //                         hint: 'Incorrect output',
  //                         output: 'A A A A A A A A B B B Z Z Z ',
  //                         mode: 'check elements order : false, case sensitive : true, ignore whitespace : true',
  //                         isCorrect: false
  //                     },
  //                     {
  //                         input: 'a\tb\tc\td\tA\tB\tC\tD\tE\tW\t',
  //                         expected: 'a b c d a b c d e w ',
  //                         percentage: '20.00',
  //                         hint: 'Correct. Well done!',
  //                         output: 'A B C D A B C D E W ',
  //                         mode: 'check elements order : false, case sensitive : false, ignore whitespace : false',
  //                         isCorrect: true
  //                     },
  //                     {
  //                         input: 'a b c d e f g h i j k l m n o p q u r s ',
  //                         expected: 'a b c d e f g h i j',
  //                         percentage: '100.00',
  //                         hint: 'Correct. Well done!',
  //                         output: 'A B C D E F G H I J ',
  //                         mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
  //                         isCorrect: true
  //                     }]
  //             });
  //             done();
  //         }).catch(function (err) {
  //             console.log('error', err);
  //             done();
  //         });
  // });

  it('7 - different order ', function (done) {
    this.timeout(3000);
    //prints SUM, MIN , MAX, will fail on DIFF ORDER = false, will pass otherwise
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", sum, min, max);
                return 0;
            }`;
    //prints MAX, MIN, SUM
    var correctC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", max, min, sum);
                return 0;
            }`;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputsFixed: [
        { id: 0, input: '0 1 2 3 4 5 6 7 8 9' },
        { id: 1, input: '0 1 2 3 4 5 6 7 8 9' },
      ],
      evalConfigsFixed: [
        {
          id: 0,
          percentage: '50.00',
          diff_order: true,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '9 0 45',
          is_public: false,
        },
        {
          id: 1,
          percentage: '50.00',
          diff_order: false,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '9 0 45',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '45 0 9',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '45 0 9',
        },
      ],
    };
    var teacherResult = {
      status: {
        id: 14,
        description: 'Accepted',
      },
      compile_output: null,
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '9 0 45',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '9 0 45',
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: true,
          score: 0.5 * (15 - -5) + -5,
          score_perc: 0.5,
          hint: 'Partial score',
          c_outcome: [
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '9 0 45',
              percentage: '50.00',
              hint: 'Correct. Well done!',
              output: '45 0 9',
              mode: 'check elements order : true, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '9 0 45',
              percentage: '50.00',
              hint: 'Incorrect output',
              output: '45 0 9',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: false,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('8 - one fixed input with output without source', function (done) {
    //prints SUM, MIN , MAX
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", sum, min, max);
                return 0;
            }`;
    //prints MAX, MIN, SUM
    var correctC = undefined;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputsFixed: [{ id: 0, input: '0 1 2 3 4 5 6 7 8 9' }],
      evalConfigsFixed: [
        {
          id: 0,
          percentage: '100.00',
          diff_order: true,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '9 0 45',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '45 0 9',
        },
      ],
    };

    var teacherResult = null;

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1.0,
          hint: 'Correct. Well done!',
          c_outcome: [
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              percentage: '100.00',
              expected: '9 0 45',
              hint: 'Correct. Well done!',
              output: '45 0 9',
              mode: 'check elements order : true, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('9 - one input without output with source', function (done) {
    //prints SUM, MIN , MAX
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", sum, min, max);
                return 0;
            }`;
    //prints MAX, MIN, SUM
    var correctC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", max, min, sum);
                return 0;
            }`;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputs: [{ id: 0, input: '0 1 2 3 4 5 6 7 8 9' }],
      evalConfigs: [
        {
          id: 0,
          percentage: '100.00',
          diff_order: true,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '45 0 9',
        },
      ],
    };

    var teacherResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '9 0 45',
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1.0,
          hint: 'Correct. Well done!',
          c_outcome: [
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '9 0 45',
              percentage: '100.00',
              hint: 'Correct. Well done!',
              output: '45 0 9',
              mode: 'check elements order : true, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  //if source is not defined when fetched from db it is set to = ''
  it('10 - one input without output or source', function (done) {
    //prints SUM, MIN , MAX
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000, sum=0;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                    sum += c;
                }
                printf("%d %d %d", sum, min, max);
                return 0;
            }`;

    var correctC = '';

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputs: [{ id: 0, input: '0 1 2 3 4 5 6 7 8 9' }],
      evalConfigs: [
        {
          id: 0,
          percentage: '100.00',
          diff_order: true,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '45 0 9',
        },
      ],
    };

    var teacherResult = {
      status: {
        id: 6,
        description: 'Compilation Error',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 6,
            description: 'Compilation Error',
          },
          stdout: null,
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: false,
          is_unanswered: true,
          is_partial: false,
          score: 0.0,
          score_perc: 0.0 / 15,
          hint: "Question has a wrong answer (teacher's error!)!!",
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('11 - regular expression', function (done) {
    //prints SUM, MIN , MAX
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                }
                printf("min=%d max=%d", min, max);
                return 0;
            }`;

    //prints 45, 0 , 9
    var correctC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                }
                printf("min=%d max=%d", min, max);
                return 0;
            }`;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputs: [{ id: 0, input: '0 1 2 3 4 5 6 7 8 9' }],
      evalConfigs: [
        {
          id: 0,
          percentage: '100.00',
          diff_order: false,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '/min=0.*max=9/g',
          input: '0 1 2 3 4 5 6 7 8 9',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'min=0 max=9',
        },
      ],
    };

    var teacherResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: 'min=0 max=9',
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: true,
          is_incorrect: false,
          is_unanswered: false,
          is_partial: false,
          score: 15,
          score_perc: 1.0,
          hint: 'Correct. Well done!',
          c_outcome: [
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              percentage: '100.00',
              hint: 'Correct. Well done!',
              expected: '(defined via regex, matched)',
              output: 'min=0 max=9',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });

  it('12 - wrong answer on elimination test', function (done) {
    //prints SUM, MIN , MAX
    var userC = `#include <stdio.h>
            #include <stdlib.h>
            int main(){
                int i, c, max=-1000, min=1000;
                for(i=0; i<10; i++){
                    scanf("%d", &c);
                    if(c > max) max = c;
                    if(c < min) min = c;
                }
                printf("%d %d", min, max);
                return 0;
            }`;

    //prints 45, 0 , 9
    var correctC = ``;

    var gradingModel = {
      correct_score: 15,
      incorrect_score: -5,
      unanswered_score: 0.0,
    };

    //sum = 45, max = 9, min=0
    let inputEvalData = {
      inputsFixed: [
        { id: 0, input: '0 1 2 3 4 5 6 7 8 9' },
        { id: 1, input: '0 1 2 3 4 5 6 7 8 9' },
        { id: 2, input: '0 1 2 3 4 5 6 7 8 9' },
      ],
      evalConfigsFixed: [
        {
          id: 0,
          percentage: '100.00',
          diff_order: false,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '10 2',
          is_public: false,
        },
        {
          id: 1,
          percentage: '50.00',
          diff_order: false,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '0 9',
          is_public: false,
        },
        {
          id: 2,
          percentage: '50.00',
          diff_order: false,
          case_insensitive: true,
          trim_whitespace: true,
          regex: '',
          input: '0 1 2 3 4 5 6 7 8 9',
          output: '0 9',
          is_public: false,
        },
      ],
    };

    var studentResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
      ],
    };

    var teacherResult = {
      status: {
        id: 14,
        description: 'Finished',
      },
      internal_message: null,
      results: [
        {
          status: {
            id: 3,
            description: 'Accepted',
          },
          stdout: '0 9',
        },
      ],
    };

    var testServiceMock = {
      execCCodeWithInputsTI: function (idTestInstance, ordinal, code, inputs) {
        return new Promise((resolve, reject) => {
          if (code == userC) resolve(studentResult);
          else resolve(teacherResult);
        });
      },
    };

    lib
      .execAndGradeC(
        testServiceMock,
        undefined,
        3700,
        1,
        userC,
        correctC,
        inputEvalData,
        gradingModel,
        id_course,
        id_c_programming_language,
        id_c_programming_language
      )
      .then(function (result) {
        delete result.j0;
        result.should.eql({
          is_correct: false,
          is_incorrect: true,
          is_unanswered: false,
          is_partial: false,
          score: -5,
          score_perc: -5 / 15,
          hint: 'Failed on all or major tests.',
          c_outcome: [
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '10 2',
              hint: 'Incorrect output',
              output: '0 9',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: false,
              percentage: '100.00',
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '0 9',
              hint: 'Correct. Well done!',
              output: '0 9',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              percentage: '50.00',
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
            {
              input: '0 1 2 3 4 5 6 7 8 9',
              expected: '0 9',
              hint: 'Correct. Well done!',
              output: '0 9',
              mode: 'check elements order : false, case sensitive : false, ignore whitespace : true',
              isCorrect: true,
              percentage: '50.00',
              stderr: undefined,
              comment: undefined,
              is_public: false,
            },
          ],
        });
        done();
      })
      .catch(function (err) {
        console.log('error', err);
      });
  });
});
