var should = require('should');
var assert = require('assert');
var lib = require('./../common/utils');

describe('Test objectToCamelCase()', function () {
  it('Plain object ', function () {
    var obj = {
      correct_score: 15,
      some_string: 'abc',
      _is_array: [1, 2, 3],
    };
    lib.objectToCamelCase(obj).should.eql({
      correctScore: 15,
      someString: 'abc',
      IsArray: [1, 2, 3],
    });
  });
});
