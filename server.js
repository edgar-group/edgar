var winston = require('winston');
var osInfo = require('./common/osInfo');
var moment = require('moment');
var http = require('http');
var path = require('path');
var os = require('os');
var fs = require('fs');
const { format } = require('winston');
const { errors, label } = format;
var globals = require('./common/globals');

let winstonFormat;
let level;
if (process.env.NODE_ENV === 'production') {
  level = 'info';
  winstonFormat = winston.format.json();
} else {
  level = 'debug';
  winstonFormat = winston.format.printf(({ level, message, timestamp, stack }) => {
    if (stack) {
      return `${timestamp} ${level}: ${message} - ${stack}`;
    } else {
      return `${timestamp} ${level}: ${message}`;
    }
  });
}
if (!!process.env.LOG_LEVEL) {
  level = process.env.LOG_LEVEL;
}
winston.configure({
  //u formatu je dodan timestamp, i stavljen je json radi strukture
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    errors({ stack: true }),
    winstonFormat
  ),
  defaultMeta: {
    hostname: osInfo.getHostName(),
    script: osInfo.getScriptName(),
    appInstance: osInfo.appInstance(),
    // source: 'edgar'
  },
  transports: [new winston.transports.Console()],
});
winston.level = level;

var port = parseInt(process.env.PORT || 5000);
port += process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0;
global.hostname = os.hostname();
global.appInstance = process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0;
global.appRoot = path.resolve(__dirname);

winston.info('global.appInstance ' + global.appInstance);
winston.info('global.appRoot ' + global.appRoot);

var app = require('./app'); // must be after golbal.appRoot=...
var config = require('./config/config');
var mailer = require('./common/mailer');

app.set('port', port);

var globals = require('./common/globals'); // must be after golbal.appRoot=...

let dir = `${global.appRoot}/${globals.PUBLIC_FOLDER}/`;
if (!fs.existsSync(dir)) {
  winston.info('Creating public folder: ' + dir);
  fs.mkdirSync(dir);
}
dir = `${dir}/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/`;
if (!fs.existsSync(dir)) {
  winston.info('Creating public test instance download folder: ' + dir);
  fs.mkdirSync(dir);
}

if (!fs.existsSync(globals.getQuestionsAttachmentsFolder())) {
  winston.info(
    'Creating public questions attachement folder: ' + globals.getQuestionsAttachmentsFolder()
  );
  fs.mkdirSync(globals.getQuestionsAttachmentsFolder());
}

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

var bindto = process.env.NODE_BIND || 'localhost';

server.listen(port, bindto, function () {
  winston.info(`--> listening on (bindto, port) = (${bindto}, ${server.address().port})`);
});

server.on('error', onError);
server.on('listening', onListening);

process.on('uncaughtException', function (err) {
  winston.error(err);
  winston.error('>>>>>>>>>>>>>>>>>>>>>>>> ------------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
  winston.error('>>>>>>>>>>>>>>>>>>>>>>>> LET IT DIE!! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
  winston.error('>>>>>>>>>>>>>>>>>>>>>>>> ------------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
  process.exit(1);
});

// done.

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  winston.error('>>>>>>>>>>>>>>>>>>>>>>>>>>> onError', error);
  winston.error(error);

  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  winston.info('********************************************');
  var addr = server.address();
  winston.info(`Server address is: ${JSON.stringify(addr)}`);
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  winston.info('Running the magnificent server on ' + bind);
  winston.info(`Using database: ${config.db}.`);
  winston.info(`Log level is ${winston.level}.`);
  winston.info('ENV is: ' + process.env.NODE_ENV);
  winston.info('Node version is: ' + process.versions.node);
  winston.info('********************************************');
  let summary =
    'Started Edgar on host ' +
    os.hostname() +
    ' in ' +
    process.env.NODE_ENV +
    ' environement using Node ' +
    process.versions.node;
  if (process.env.NODE_ENV === 'development') {
    winston.info(summary);
  } else {
    mailer.sendmail(summary);
  }
  winston.info('Good luck...');
}

// for Lecture quiz app
if (port === 1337) {
  winston.info(`Port is 1337, STARTING socket.io on this instance.`);
  var socket = require('socket.io');
  var io = socket(server, {
    path: '/wss/socket.io',
  });
  // var nsp = io.of('/lecture');

  io.on('connection', function (socket) {
    winston.info('user connected:');
    winston.info(socket.handshake.query.room);

    socket.join(socket.handshake.query.room);

    io.to(socket.handshake.query.room + '-user-count').emit('user-joined', {
      username: 'for-future-use',
    });

    socket.on('disconnect', function () {
      // winston.info('user disconnected');
    });

    socket.on('start-student', function (ordinal) {
      io.to(socket.handshake.query.room).emit('start', {
        ordinal: ordinal,
      });
    });

    socket.on('subscribe-to-user-count', function () {
      socket.join(socket.handshake.query.room + '-user-count');
    });

    socket.on('stop-question', function () {
      io.to(socket.handshake.query.room).emit('stop');
    });

    socket.on('end-test', function () {
      io.to(socket.handshake.query.room).emit('end');
    });

    socket.on('free-text-answer', function (answer) {
      io.to(socket.handshake.query.room).emit('free-text-answer', {
        answer: answer,
      });
    });

    socket.on('all-answers', function (answers) {
      io.to(socket.handshake.query.room).emit('all-answers', {
        answers: answers,
      });
    });

    socket.on('score', function (score) {
      io.to(socket.handshake.query.room).emit('score', {
        score: score,
      });
    });
  });
} else {
  winston.info(`Port is not 1337, NOT starting socket.io on this instance.`);
}
