'use strict';
var fs = require('fs');
var crypto = require('crypto');
var moment = require('moment');
var globals = require('./globals');
const marked = require('marked');
const markedHighlight = require('marked-highlight');
var winston = require('winston');
var escape = require('escape-html');
const os = require('os');
const Diff = require('text-diff');
var db = require('./../db').db;
const archiver = require('archiver');
var config = require.main.require('./config/config');
var mjAPI = require('mathjax-node');
let html = require('html-entities');
const hljs = require('highlight.js/lib/common');
const diff_match_patch = require('diff-match-patch');
mjAPI.config({
  MathJax: {
    showProcessingMessages: true,
    jax: ['input/TeX', 'output/HTML-CSS'],
    tex2jax: {
      inlineMath: [
        ['$', '$'],
        ['\\(', '\\)'],
      ],
    },
    TeX: {
      TagSide: 'left',
      Macros: {
        RR: '{\\bf R}',
        bold: ['{\\bf #1}', 1],
      },
    },
  },
});
mjAPI.start();
const markedRenderer = new marked.Marked(
  markedHighlight.markedHighlight({
    langPrefix: 'hljs language-',
    highlight(code, lang) {
      if (['nohighlight', 'plaintext'].includes(('' + lang).trim())) {
        return code;
      } else {
        const language = hljs.getLanguage(lang);
        if (typeof language === 'undefined') {
          return hljs.highlightAuto(code).value;
        } else {
          return hljs.highlight(code, { language: language.name, ignoreIllegals: true }).value;
        }
      }
    },
  })
);

const crc_a_table =
  '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';
const crc_b_table = crc_a_table.split(' ').map(function (s) {
  return parseInt(s, 16);
});

const utils = {
  objCRC32: (obj) => {
    return utils.strCRC32(JSON.stringify(obj));
  },
  strCRC32: (str) => {
    var crc = -1;
    for (var i = 0, iTop = str.length; i < iTop; i++) {
      crc = (crc >>> 8) ^ crc_b_table[(crc ^ str.charCodeAt(i)) & 0xff];
    }
    return (crc ^ -1) >>> 0;
  },

  useMinio: () => {
    return !!(config.minioConfig && config.minioConfig.endPoint && config.useMinIO);
  },

  /**
   * Fast UUID generator, RFC4122 version 4 compliant.
   * @author Jeff Ward (jcward.com).
   * @license MIT license
   * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
   **/
  UUID: (function () {
    var self = {};
    var lut = [];
    for (var i = 0; i < 256; i++) {
      lut[i] = (i < 16 ? '0' : '') + i.toString(16);
    }
    self.generate = function () {
      var d0 = (Math.random() * 0xffffffff) | 0;
      var d1 = (Math.random() * 0xffffffff) | 0;
      var d2 = (Math.random() * 0xffffffff) | 0;
      var d3 = (Math.random() * 0xffffffff) | 0;
      return (
        lut[d0 & 0xff] +
        lut[(d0 >> 8) & 0xff] +
        lut[(d0 >> 16) & 0xff] +
        lut[(d0 >> 24) & 0xff] +
        '-' +
        lut[d1 & 0xff] +
        lut[(d1 >> 8) & 0xff] +
        '-' +
        lut[((d1 >> 16) & 0x0f) | 0x40] +
        lut[(d1 >> 24) & 0xff] +
        '-' +
        lut[(d2 & 0x3f) | 0x80] +
        lut[(d2 >> 8) & 0xff] +
        '-' +
        lut[(d2 >> 16) & 0xff] +
        lut[(d2 >> 24) & 0xff] +
        lut[d3 & 0xff] +
        lut[(d3 >> 8) & 0xff] +
        lut[(d3 >> 16) & 0xff] +
        lut[(d3 >> 24) & 0xff]
      );
    };
    return self;
  })(),

  randomIntInclusive: function (low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
  },

  /**
   * @param {String} source
   * @param {String} out
   * @returns {Promise}
   */
  zipDirectory: function (source, out) {
    const archive = archiver('zip', { zlib: { level: 1 } });
    const stream = fs.createWriteStream(out);

    return new Promise((resolve, reject) => {
      archive
        .directory(source, false)
        .on('error', (err) => reject(err))
        .pipe(stream);

      stream.on('close', () => resolve());
      archive.finalize();
    });
  },
  deleteFolderRecursiveSync: function (path) {
    if (fs.existsSync(path)) {
      fs.readdirSync(path).forEach(function (file, index) {
        var curPath = path + '/' + file;
        if (fs.lstatSync(curPath).isDirectory()) {
          // recurse
          deleteFolderRecursive(curPath);
        } else {
          // delete file
          winston.info('deleting file ' + curPath);
          fs.unlinkSync(curPath);
        }
      });
      winston.info('deleting folder ', path);
      fs.rmdirSync(path);
    }
  },
  objectToCamelCase: function (obj) {
    var rv = {};
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        let cc = prop.replace(/_([a-z])/g, function (g) {
          return g[1].toUpperCase();
        });
        rv[cc] = obj[prop];
      }
    }
    return rv;
  },

  replaceAll: function (orig, search, replacement) {
    return orig.replace(new RegExp(search, 'g'), replacement);
  },
  getHashedImageName: function (filename, ext) {
    var shasum = crypto.createHash('md5');
    shasum.update(config.SECRET + filename);
    return shasum.digest('hex') + ext;
  },
  getRandomHash: function (len) {
    return crypto.randomBytes(len || 20).toString('hex');
  },
  getTinyImageHtml: function (alt_id2) {
    return `<div class="profilepictiny"><img width="35" height="35" class="rounded-circle" src="/images/faces/${utils.getHashedImageName(
      alt_id2 + '.jpg',
      '.jpg'
    )}"></div>`;
  },
  getTinyImageHtmlMinimal: function (alt_id2) {
    return `<img width="35" height="35" class="rounded-circle m-1" src="/images/faces/${utils.getHashedImageName(
      alt_id2 + '.jpg',
      '.jpg'
    )}">`;
  },
  getImageHtml: function (alt_id2) {
    return `<div class="profilepic"><img height="75" class="rounded-circle" src="/images/faces/${utils.getHashedImageName(
      alt_id2 + '.jpg',
      '.jpg'
    )}"></div>`;
  },
  getImageWithNameHtml: function (alt_id2, name) {
    return `${this.getImageHtml(alt_id2)}<p>${name}</p>`;
  },
  getResponsiveImageHtml: function (alt_id2) {
    return `<div class="mx-2"><img style="width:100%;" src="/images/faces/${utils.getHashedImageName(
      alt_id2 + '.jpg',
      '.jpg'
    )}"></div>`;
  },
  getBigImageHtml: function (alt_id2) {
    return `<div class="profilepicbig"><img height="100" src="/images/faces/${utils.getHashedImageName(
      alt_id2 + '.jpg',
      '.jpg'
    )}"></div>`;
  },
  stampSQL: function (user, SQL) {
    return `-- user:  $(user)\n-- time:${moment().format('MMMM Do YYYY, h:mm:ss a')}\n\n ${SQL}`;
  },
  string2BSClass: function (str, succSubString) {
    var test = str.toLowerCase();
    if (test.indexOf('error') >= 0) {
      return 'danger';
    } else if (test.indexOf('warn') >= 0) {
      return 'warning';
    } else {
      for (let i = 0; i < succSubString.length; ++i) {
        if (test.indexOf(succSubString[i]) >= 0) return 'success';
      }
      return 'default';
    }
  },
  _checkResponseBasic: function (resp, scoreHtml) {
    if (!resp)
      return '<h2 class="text-danger"><i class="fas fa-hand-paper"></i> Not executed.</h2>';
    if (!resp.status) {
      return `<div class="border border-danger p-2"><pre>Empty response status: ${JSON.stringify(
        resp
      )}</pre></div>`;
    }
    if (resp.status && resp.status.description.toLowerCase().indexOf('error') >= 0) {
      return `<div class="border border-danger p-2"><pre>${JSON.stringify(resp)}</pre></div>`;
    }
    if (resp.status && resp.status.id === 100) {
      return `<div class="border border-danger bg-light m-2 p-2 text-center">
                        <i class="fas fa-exclamation-triangle text-warning"></i>
                        <i class="far fa-hand-paper"></i>
                        You have been pushed back!
                        <i class="far fa-surprise"></i>
                        System is experiencing heavy load. Submissions with lower repeated runs have advantage. Please, try again later...
                        <i class="fas fa-exclamation-triangle text-warning"></i>
                        </div>`;
    }
    if (!resp) {
      return `${scoreHtml}<div class="card-header">
                <span class="badge badge-warning"><h4>Status: Empty answer</h4></span>
            </div>`;
    }
    return null;
  },
  _getHeaderHtml: function (resp) {
    return `<div class="card-header d-flex justify-content-between align-items-baseline">
                <span class="badge badge-${utils.string2BSClass(resp.status.description, [
                  'finished',
                  'accepted',
                ])}"><h4>Status: ${resp.status.description}</h4></span>
                <div>
                    <span class="badge badge-secondary">
                        Started at: ${moment(resp.submitted).format('MMMM Do YYYY, h:mm:ss a')}
                    </span>
                    <span class="badge badge-secondary">
                        Finished at: ${moment(resp.finished).format('h:mm:ss a')}
                    </span>
                    <span class="badge badge-secondary">
                        Duration: ${new Date(resp.finished) - new Date(resp.submitted)} ms
                    </span>
                </div>
            </div>
            <table class="table table-striped edgar no-dt">
                <tr>
                    <th class="edgar-code">Language</th>
                    <th class="edgar-code">Compiler options</th>
                    <th class="edgar-code">Compiler output</th>
                    <th class="edgar-code">Message(s)</th>
                </tr>
                <tr>
                    <td class="edgar-code">${resp.language.name}</td>
                    <td class="edgar-code">${resp.compiler_options}</td>
                    <td class="edgar-code" style="white-space: break-spaces;" >${(resp.results
                      ? resp.results
                      : []
                    )
                      .filter((res) => res.compile_output)
                      .filter((v, i) => i === 0)
                      .map((res) => res.compile_output)
                      .join('\n')}</td>
                    <td class="edgar-code" style="white-space: break-spaces;">${(resp.results
                      ? resp.results
                      : []
                    )
                      .filter((res) => res.message)
                      .filter((v, i) => i === 0)
                      .map((res) => res.message)
                      .join('\n')}</td>
                </tr>
            </table>`;
  },
  _getTestsHtml(resp, score, showExpected, hideTestDetails, verboseTests) {
    return (
      `<h3>Tests(${resp.results && resp.results.length}):</h3>` +
      (resp.results ? resp.results : [])
        .map(
          (elem, idx) =>
            `
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="d-flex w-100 justify-content-between">
                        <div>
                        <span class="aii-answer-badge badge badge-primary border border-dark"><h5>${
                          1 + idx
                        }.</h5></span>` +
            (true
              ? `` // bunker this for a while...
              : `<span title="${
                  score && score.c_outcome && score.c_outcome[idx].hint
                }" class="badge badge-${utils.string2BSClass(elem.status.description, [
                  'finished',
                  'accepted',
                ])}">
                                <h5>Status: ${elem.status.description}</h5>
                            </span>`) +
            `<span class="ml-2 border border-dark badge badge-${utils.string2BSClass(
              (score && score.c_outcome && score.c_outcome[idx].isCorrect) == true
                ? 'Correct'
                : 'Error',
              ['correct']
            )}">
                            <h5>${
                              (score && score.c_outcome && score.c_outcome[idx].isCorrect) == true
                                ? 'Correct'
                                : 'Incorrect'
                            }</h5>
                        </span>` +
            ((score && score.c_outcome && score.c_outcome[idx].isCorrect) == true
              ? ``
              : `
                        <span class="ml-2 badge badge-warning border border-dark">
                            <h5>-${
                              score && score.c_outcome && score.c_outcome[idx].percentage
                            }%</h5>
                        </span>` +
                (score && score.c_outcome && score.c_outcome[idx].comment
                  ? `<span class="ml-2 badge badge-info">
                                <small>${score.c_outcome[idx].comment}</small>
                            </span>`
                  : '') +
                (score && score.c_outcome && score.c_outcome[idx].hint
                  ? `<span class="ml-2 badge badge-info">
                                <small>${score.c_outcome[idx].comment}</small>
                                <small>Hint: ${score.c_outcome[idx].hint}</small>
                            </span>`
                  : '') +
                (score && score.c_outcome && score.c_outcome[idx].stderr
                  ? `<span class="ml-2 badge badge-info">
                                <pre>${escape(score.c_outcome[idx].stderr)}</pre>
                            </span>`
                  : '')) +
            `</div><div>` +
            (!verboseTests
              ? ``
              : `<span class="badge badge-secondary">
                            ${score && score.c_outcome && score.c_outcome[idx].mode}
                        </span>
                        <span class="badge badge-secondary">
                            Finished at: ${moment(elem.finished).format(
                              'MMMM Do YYYY, h:mm:ss a'
                            )}, Time: ${elem.time}, Memory: ${elem.memory}
                        </span>`) +
            `</div>` +
            ((score &&
              score.c_outcome &&
              score.c_outcome.length &&
              score.c_outcome[idx].is_public === true) ||
            !hideTestDetails
              ? `</div><table class="table code-results no-dt mt-2">
                        <tr>
                            <td class="edgar-code">stdin</td>
                            <td class="edgar-code"><pre>${
                              score &&
                              score.c_outcome &&
                              escape(utils.markWhitespace(score.c_outcome[idx].input))
                            }</pre></td>
                        </tr>
                        <tr>
                            <td class="edgar-code">stdout</td>
                            <td class="edgar-code"><pre>${
                              score &&
                              score.c_outcome &&
                              escape(utils.markWhitespace(score.c_outcome[idx].output))
                            }</pre></td>
                        </tr>
                        ` +
                (showExpected ||
                (score && score.c_outcome && score.c_outcome[idx].is_public === true)
                  ? `<tr>
                                <td class="edgar-code">expected</td>
                                <td class="edgar-code"><pre>${
                                  score &&
                                  score.c_outcome &&
                                  escape(utils.markWhitespace(score.c_outcome[idx].expected))
                                }</pre></td>
                            </tr>`
                  : '') +
                (score.is_correct == false &&
                (showExpected || score.c_outcome[idx].is_public === true)
                  ? `<tr>
                                <td class="edgar-code">diff</td>
                                <td class="edgar-code"><pre>${utils.getTextDiffAsHtml(
                                  score.c_outcome[idx].output,
                                  score.c_outcome[idx].expected,
                                  true
                                )}
                                </pre></td>
                            </tr>
                            `
                  : '') +
                `<tr>
                            <td class="edgar-code">stderr</td>
                            <td class="edgar-code"><pre>${escape(elem.stderr)}</pre></td>
                        </tr>
                    </table>`
              : '') +
            `</li>`
        )
        .join('') +
      `
                </ul>
            </div>`
    );
  },
  getTextDiffAsHtml(source1, other1, markWhitespace) {
    let source = source1 ? source1 : '';
    let other = other1 ? other1 : '';
    if (markWhitespace) {
      source = utils.markWhitespace(source, true);
      other = utils.markWhitespace(other, true);
    }
    let dmp = new diff_match_patch();
    let diff = dmp.diff_main(source, other);
    return dmp.diff_prettyHtml(diff).replaceAll('&para;', globals.SYMBOL_ENTER);
  },
  // markSpace(code) {
  //   return code && code.replace(/\r\n/g, '\n').replace(/ /g, globals.SYMBOL_SPACE);
  // },
  markWhitespace(code, skipNewline) {
    code = code || '';
    code = code.replace(/\r\n/g, '\n'); // always normalize newlines
    !skipNewline && (code = code.replace(/\n/g, `${globals.SYMBOL_ENTER}\n`));
    return code
      .replace(/ /g, globals.SYMBOL_SPACE)
      .replace(/\t/g, `${globals.SYMBOL_TAB}\n`)
      .replace(/[\x00-\x00]/g, `${globals.SYMBOL_NULL}\n`)
      .replace(/[\x00-\x09\x0B-\x1F]/g, `${globals.SYMBOL_NON_PRINTABLE}\n`);
  },
  _getPlaygroundOutputHtml(resp) {
    return (
      `<ul class="list-group list-group-flush">` +
      (resp.results ? resp.results : [])
        .map(
          (elem) =>
            `<li class="list-group-item">
                    <div class="d-flex w-100 justify-content-end">
                        <div>
                            <span class="badge badge-secondary">
                                Finished at: ${moment(elem.finished).format(
                                  'MMMM Do YYYY, h:mm:ss a'
                                )}, Time: ${elem.time}, Memory: ${elem.memory}
                            </span>
                        </div>
                    </div>
                    <table class="table code-results no-dt mt-2">
                        <tr>
                            <td class="edgar-code">stdin</td>
                            <td class="edgar-code"><pre>${escape(
                              Buffer.from(elem.test_case.input, 'base64').toString('utf8')
                            )}</pre></td>
                        </tr>
                        <tr>
                            <td class="edgar-code">stdout</td>
                            <td class="edgar-code"><pre>${escape(elem.stdout)}</pre></td>
                        </tr>
                        <tr>
                            <td class="edgar-code">stderr</td>
                            <td class="edgar-code"><pre>${escape(elem.stderr)}</pre></td>
                        </tr>
                    </table>
                </li>`
        )
        .join('') +
      `</ul>`
    );
  },
  _getScoreHtml: function (score) {
    if (score) {
      return `
                <table class="table table-hover edgar no-dt">
                    <thead>
                    <tr>
                        <th>Correct</th>
                        <th>Incorrect</th>
                        <th>Partial</th>
                        <th>Unanswered</th>
                        <th>Score</th>
                        <th>Score %</th>
                        <th>Hint</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${
                              score.is_correct
                                ? '<i class="fas fa-check-circle fa-2x text-success"></i>'
                                : ''
                            }</td>
                            <td>${
                              score.is_incorrect
                                ? '<i class="fas fa-check-circle fa-2x text-danger"></i>'
                                : ''
                            }</td>
                            <td>${
                              score.is_partial
                                ? '<i class="fas fa-check-circle fa-2x text-warning"></i>'
                                : ''
                            }</td>
                            <td>${
                              score.is_unanswered ? '<i class="fas fa-check-circle fa-2x"></i>' : ''
                            }</td>
                            <td>${score.score}</td>
                            <td>${100 * score.score_perc}</td>
                            <td>${score.hint}</td>
                        </tr>
                    </tbody>
                </table>`;
    } else {
      return '';
    }
  },
  judge0response2Html: function ({
    resp = undefined,
    score = undefined,
    showExpected = false,
    headerOnly = false,
    hideTestDetails = false,
    verboseTests = true,
    isPlayground = false,
  } = {}) {
    try {
      let basic;

      var scoreHtml = utils._getScoreHtml(score);

      if ((basic = utils._checkResponseBasic(resp, scoreHtml))) {
        return `${basic}${scoreHtml}`;
      }

      if (resp.status.id !== 14 || headerOnly) {
        return `${scoreHtml}${utils._getHeaderHtml(resp)}`;
      } else if (isPlayground) {
        return `${scoreHtml}
                    <div class="card">
                        ${utils._getHeaderHtml(resp)}
                        ${utils._getPlaygroundOutputHtml(resp)}
                    </div>`;
      } else {
        return `${scoreHtml}
                    <div class="card">
                        ${utils._getHeaderHtml(resp)}
                        ${utils._getTestsHtml(
                          resp,
                          score,
                          showExpected,
                          hideTestDetails,
                          verboseTests
                        )}
                    </div>`;
      }
    } catch (error) {
      winston.error(error);
      winston.error('Response is:' + JSON.stringify(resp));
      return 'Error formatting j0 response.';
    }
  },
  mdEnhancers: function (html) {
    var matches = html.match(/(<!--\s*collapse.*-->)((?!collapse)[^])*<!--\s*\/collapse\s*-->/gi);
    if (matches && matches.length) {
      var newHtml = html;
      matches.forEach(function (val) {
        var old = val;
        var header = val.substring(0, val.indexOf('-->') + 3);
        val = val.replace(header, '');
        val = val.substring(0, val.lastIndexOf('<!--'));
        var title = (header.split('title') || [undefined, undefined])[1];
        if (title) {
          var idx1 = title.indexOf('"');
          var idx2 = title.indexOf('"', idx1 + 1);
          if (idx1 > 0 && idx2 > 0) {
            title = title.substring(idx1 + 1, idx2);
          }
        } else {
          title = 'Show';
        }
        var divid = 'enhance-md-collapse-' + utils.UUID.generate();
        var template =
          '<button type="button" class="btn btn-success pull-right" data-toggle="collapse" data-target="#' +
          divid +
          '">' +
          title +
          '</button>' +
          '<div id="' +
          divid +
          '" class="collapse enhance-md-collapse">' +
          val +
          '</div>';
        newHtml = newHtml.replace(old, template);
      });
      return newHtml;
    }
    return html;
  },
  // boxes diagram to html element div so that md renderer leaves it alone (otherwise it would be rendered as <p>...)
  diagramsToPlainText(md) {
    let newMd = md
      .replace(/(<!--\s*(diagram).*-->)/gi, '<div><!-- diagram -->')
      .replace(/(<!--\s*(\/diagram).*-->)/gi, '<!-- /diagram --></div>')
      .replace(/(<!--\s*(inline-diagram).*-->)/gi, '<div><!-- inline-diagram -->')
      .replace(/(<!--\s*(\/inline-diagram).*-->)/gi, '<!-- /inline-diagram --></div>');
    return newMd;
  },

  md2Html: function (md) {
    if (!md) return '';
    md = utils.diagramsToPlainText(md);
    var items = md.split('$$');
    //console.log('items.length', items.length, items);
    if (items.length > 1 && items.length % 2 === 1) {
      var promises = [];
      for (let i = 1; i < items.length; i += 2) {
        promises.push(
          new Promise((resolve, reject) => {
            var formula = items[i];
            mjAPI.typeset(
              {
                math: formula,
                format: 'TeX', // "inline-TeX", "MathML"
                svg: true,
              },
              function (data) {
                if (!data.errors) {
                  resolve(data.svg);
                } else {
                  reject(data.errors);
                }
              }
            );
          })
        );
        items[i] = 'MJPLACEHOLDER' + i;
      }
      let md = markedRenderer.parse(
        utils.replaceAll(
          utils.replaceAll(items.join(''), globals.IMG_FOLDER_PLACEHOLDER, globals.IMG_FOLDER),
          globals.TUTORIALS_IMG_FOLDER_PLACEHOLDER,
          globals.TUTORIALS_IMG_FOLDER
        )
        // , {
        //     renderer: renderer
        // }
      );
      md = utils.mdEnhancers(md);
      //console.log('marked (join)', md);
      return new Promise((resolve, reject) => {
        Promise.all(promises)
          .then(function (svgs) {
            for (let i = 0; i < svgs.length; ++i) {
              md = md.replace('MJPLACEHOLDER' + (1 + i * 2), svgs[i]);
            }
            //console.log('!!!! replaced placholders', md);
            resolve(md);
          })
          .catch((error) => {
            winston.error(error);
            return 'Error occured when converting to HTML: ' + error;
          });
      });
    } else {
      // uneven number of $$s.
      // console.log(
      //   'parsing',
      //   utils.replaceAll(
      //     utils.replaceAll(md, globals.IMG_FOLDER_PLACEHOLDER, globals.IMG_FOLDER),
      //     globals.TUTORIALS_IMG_FOLDER_PLACEHOLDER,
      //     globals.TUTORIALS_IMG_FOLDER
      //   )
      // );
      return new Promise((resolve, reject) => {
        resolve(
          utils.mdEnhancers(
            markedRenderer.parse(
              utils.replaceAll(
                utils.replaceAll(md, globals.IMG_FOLDER_PLACEHOLDER, globals.IMG_FOLDER),
                globals.TUTORIALS_IMG_FOLDER_PLACEHOLDER,
                globals.TUTORIALS_IMG_FOLDER
              )
              // , {
              //     renderer: renderer
              // }
            )
          )
        );
      });
    }
  },

  // mergeCCode: function(prefix, body, suffix) {
  //     return `/* <prefix> */\n\n${prefix}
  //           \n\n/* </prefix>\n<body> */\n\n${body}
  //           \n\n/* </body>\n<suffix> */\n\n${suffix}
  //           \n\n/* </suffix>\n`;
  // },
  mergeCode: function (prefix, body, suffix, delimiter = '') {
    let rv = '';
    if (body && body.trim() !== '') {
      rv += prefix ? `${prefix + delimiter}\n` : '';
      rv += body;
      rv += suffix ? `\n${delimiter + suffix}` : '';
    }
    return rv;
  },
  mergeSQLCode: function (prefix, body, suffix) {
    return utils.mergeCode(prefix, body, suffix, '\n;\n');
  },

  setCourseParams: async function (session) {
    try {
      let c = await db.one(
        `SELECT course.*, (
                     SELECT count(*)
                       FROM code_runner join playground_code_runner
                         ON code_runner.id = playground_code_runner.id_code_runner
                      WHERE id_course = course.id
                        AND student_can_see
                ) as show_playground
                   FROM course
                  WHERE id = $(id_course)`,
        {
          id_course: session.currCourseId,
        }
      );
      session.usesMobileApp = c.uses_mobile_app;
      session.showEventLog = c.show_event_log;
      session.showCodeMirror = c.show_codemirror;
      session.showPlaygroundStudent = c.show_playground > 0;
    } catch (error) {
      winston.error(error);
    }
  },

  getViewPagePath: function (pagename, baseUrl) {
    if (pagename) {
      if (pagename.startsWith('~')) {
        return `${global.appRoot}/views/${pagename.substring(1)}`;
      } else {
        return `.${baseUrl}/${pagename}`;
      }
    } else {
      return null;
    }
  },
  cModeToString: function (allow_diff_order, case_insensitive, trim_whitespace) {
    return `check elements order : ${allow_diff_order}, case sensitive : ${!case_insensitive}, ignore whitespace : ${trim_whitespace}`;
  },
  cScoreHint: function (object) {
    var s = object.status ? `Status: ${object.status.description} ; ` : '';
    s += object.compile_output ? `Compiler output: ${object.compile_output} ; ` : '';
    s += object.stderr ? `Stderr: ${object.stderr} ; ` : '';
    s += object.internal_message ? `Internal message: ${object.internal_message} ;` : '';
    s += object.output ? object.output : '';
    return s;
  },
  secondsToHHmm: function (seconds) {
    let hours = Math.floor(seconds / 3600);
    let minutes = Math.floor((seconds - hours * 3600) / 60);

    if (hours < 10) {
      hours = `0${hours}`;
    }

    if (minutes < 10) {
      minutes = `0${minutes}`;
    }

    return `${hours}:${minutes}`;
  },
  getHintForHtmlTable: function (result) {
    if (result.score) {
      if (result.score.is_correct) {
        return ', <span style="background-color:lime;"> <i class="far fa-smile" aria-hidden="true"></i> Correct! Well done! </span>';
      } else {
        return (
          ', <span style="background-color:lemonchiffon;"> <i class="far fa-frown" aria-hidden="true"></i> Missed it. Hint: ' +
          result.score.hint +
          '</span>'
        );
      }
    } else {
      return '';
    }
  },
  getWarningForHtmlTable: function (result) {
    if (result.warning) {
      return `<div class="alert alert-warning text-left">${result.warning}</div>`;
    } else {
      return '';
    }
  },
  formatValueForHtmlTable: function (val) {
    return val == null ? 'null' : val;
  },
  getHtmlTable: function (result) {
    return (
      ` <p>Result (count = ${result.data.rows.length}, db = ${
        result.db
      } <span>${utils.getHintForHtmlTable(result)}</span>):</p>
                ${utils.getWarningForHtmlTable(result)}
                <table class="table table-striped no-dt">
                    <tr>
                        <td class="edgar-code">#</td>
                        ` +
      result.data.fields
        .map((x) => `<td class="edgar-code edgar-code-header">${x.name}</td>`)
        .join('') +
      `</tr>` +
      result.data.rows
        .map(
          (row, idx) =>
            `<tr><td class="edgar-code">${idx + 1}</td>` +
            result.data.fields
              .map(
                (f, idx2) =>
                  `<td class="edgar-code">${utils.formatValueForHtmlTable(row['C' + idx2])}</td>`
              )
              .join('') +
            `</tr>`
        )
        .join('') +
      `</table>`
    );
  },
  formatTicketConversation(comments) {
    let popoverContent = '';
    if (comments && comments.length > 0) {
      comments.forEach((comment) => {
        popoverContent += `${moment(comment.ts_created).local()}  <b>(${moment(
          comment.ts_created
        ).fromNow()})</b>`;
        popoverContent += `<br><p class='ml-3'>${html.encode(
          comment.description || comment.reply
        )}</p><hr/>`;
      });
    } else {
      popoverContent = 'no comments?';
    }
    return popoverContent;
  },
  getUserName() {
    return os.userInfo().username;
  },
  getHostName() {
    return os.hostname;
  },
  getCurrentTimestamp(ms = false) {
    return moment().format('HH:mm:ss' + (ms ? '.SSS' : ''));
  },
  getLogString(msg) {
    return `${utils.getUserName()}@${utils.getHostName()} [${utils.getCurrentTimestamp()}]: ${msg}`;
  },
  isAllowedAnonymousLogEvent(event) {
    return globals.ALLOWED_ANONYMOUS_LOG_EVENTS.includes(event);
  },
};

module.exports = utils;
