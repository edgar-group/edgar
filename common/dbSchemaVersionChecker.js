const fs = require('fs');
var winston = require('winston');
var globals = require('./globals');
const dbSchemaFolder = './db/db-schema/migrations';
const fileNameRegex = 'migration_([0-9-.]+).sql';

let helpers = {
  getFileNames: () => {
    let rv = [];
    fs.readdirSync(dbSchemaFolder).forEach((file) => {
      rv.push(file);
    });
    return rv;
  },
  getTagsFromFileNames: (fileNames) => {
    const vf = fileNames
      .filter((fn) => fn.match(fileNameRegex))
      .map((tag) => tag.match(fileNameRegex)[1]);

    return vf.map((x) => x.split('-').join('.'));
  },
  getTagsFromDatabase: (db) => {
    return db.many(`SELECT tag FROM versions`);
  },
  versionsComparer: (a, b) => {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, '').split('.');
    var segmentsB = b.replace(regExStrip0, '').split('.');
    var l = Math.min(segmentsA.length, segmentsB.length);

    for (i = 0; i < l; i++) {
      diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
      if (diff) {
        return diff;
      }
    }
    return segmentsA.length - segmentsB.length;
  },
};

let service = {
  checkIfDbSchemaIsUpToDate: (db) => {
    return new Promise((resolve, reject) => {
      helpers.getTagsFromDatabase(db).then((databaseTags) => {
        const fileTags = helpers.getTagsFromFileNames(helpers.getFileNames());
        const newestFileTag = fileTags.sort(helpers.versionsComparer).reverse()[0],
          newestDatabaseTag = databaseTags
            .map((x) => x.tag)
            .sort(helpers.versionsComparer)
            .reverse()[0];
        globals.APP_VERSION = newestDatabaseTag;
        if (newestDatabaseTag != newestFileTag) {
          const err = `!!! Database schema version tag ${newestDatabaseTag} is not up-to-date with file schema version tag ${newestFileTag}. Please apply the new database schema from db/db-schema/ sql files !!!`;
          globals.WARNING_MESSAGE = 'Database schema mismatch.';
          winston.error(err);
          resolve();
        } else {
          const msg = `Database and file schema version tags match`;
          winston.info(msg);
          resolve();
        }
      });
    });
  },
};

module.exports = service;
