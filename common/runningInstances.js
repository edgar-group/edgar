'use strict';
var winston = require('winston');
var db = require('../db').db;
const runningInstances = {
  clearAll: (session) => {
    session.running_rev_instances = [];
    session.running_instances = [];
    delete session.id_test_instance;
    delete session.id_test_instance_rev;
    winston.debug('Deleted all ongoing exams and reviews.');
  },
  canSee: async (idti, id_student, isReview = false) => {
    try {
      // Sometimes, rarely, the idti is not in the session, but it is in the DB.
      // I don't know why (could be that student visits the instances page in other tab eg), but I'll try to recover it.
      const ti = await db.oneOrNone(
        `SELECT test_instance.id, test_instance.id_test, test.hint_result
             FROM test_instance
             JOIN test
               ON test_instance.id_test = test.id
             JOIN student
               ON student.id = $(id_student)  -- for teacher, this is alterego student, not the one in the test_instance!!
            WHERE test_instance.id = $(idti)
              AND (test_instance.id_student = $(id_student) OR student.id_app_user IS NOT NULL)  -- teacher can see all
              ${isReview ? 'AND test_instance.ts_submitted IS NOT NULL ' : ''}
              `,
        {
          idti: idti,
          id_student,
        }
      );
      if (ti && ti.id) {
        if (isReview) {
          return {
            id_test_instance_rev: ti.id,
            id_test: ti.id_test,
            ts: new Date(),
          };
        } else {
          return {
            id_test_instance: ti.id,
            id_test: ti.id_test,
            hint_result: ti.hint_result,
            ts: new Date(),
          };
        }
      } else {
        winston.error(
          `canSee: HACK attack? Can not find idti ${idti} in the database (test_instances) for student.id =  ${id_student}!`
        );
        return null;
      }
    } catch (error) {
      winston.error(`Error in RunningInstances.canSee: ${error}, message: ${error.message}`);
      winston.error(error);
    }
  },
  addExamInstance: (req, id_test, hint_result) => {
    const id_test_instance = req.session.id_test_instance;
    if (!req.session.running_instances) {
      req.session.running_instances = [];
    }
    let newArray = req.session.running_instances.filter(function (item) {
      return item.id_test_instance != id_test_instance;
    });
    newArray.push({
      id_test_instance,
      id_test,
      hint_result,
      ts: new Date(),
    });
    winston.info(
      req.session.passport.user +
        ': SETTING req.session.running_instances to ' +
        JSON.stringify(newArray)
    );
    req.session.running_instances = newArray;
  },

  // This will get the current test instance from the session, or from the DB if it's not in the session
  // Of course, it is checked if the exam id belongs to the user
  // If the foldIn is set, then it will "fold in" this DB data to the running_instances in the session to avoid future DB hits
  getCurrTestInstance: async (req, storeFoldIn = true) => {
    // console.log(
    //   `idti: ${req.query.idti}  |  req.session.id_test_instance: ${req.session.id_test_instance}  |  All running instances: `,
    //   req.session.running_instances
    // );
    let rv = {
      id_test_instance: req.session.id_test_instance, // the LAST started exam, we need this for the bootstrapping of the SPA, and first log requests
    };
    req.session.running_instances = req.session.running_instances || [];
    if (req.query.idti) {
      rv = req.session.running_instances.find((item) => {
        return item.id_test_instance == req.query.idti;
      });
      if (!rv) {
        // Sometimes, rarely, the idti is not in the session, but it is in the DB.
        // I don't know why (could be that student visits the instances page in other tab eg), but I'll try to recover it.
        rv = await runningInstances.canSee(req.query.idti, req.session.studentId);
        if (rv && rv.id_test_instance) {
          // I don't want to store fold-ins for eg logging ops that may arrive after submit (bcs I clean the exam data on submit)
          if (storeFoldIn) {
            req.session.running_instances.push(rv);
            winston.info(`Recovered: fold in idti: ${JSON.stringify(rv)}`);
          }
        } else {
          winston.error(
            `HACK attack? Can not find idti ${req.query.idti} in ${JSON.stringify(
              req.session.running_instances
            )} for user ${req.session.passport.user} NOR in test_instances!`
          );
        }
      }
    } else if (!req.session.id_test_instance) {
      winston.warn(
        `PANIC: idti and req.session.id_test_instance is undefined for user ${
          req.session.passport.user
        }: idti = ${req.query.idti}, running_instances = ${JSON.stringify(
          req.session.running_instances
        )}`
      );
    }
    return rv;
  },
  getCurrTestInstanceId: async (req, storeFoldIn = true) => {
    let rv = await runningInstances.getCurrTestInstance(req, storeFoldIn);
    rv = rv && rv.id_test_instance;
    return rv;
  },
  // ********************************************************************************
  // ********************   Review:  ************************************************
  // ********************************************************************************
  addReviewInstance: (req, show_solutions) => {
    const id_test_instance_rev = req.session.id_test_instance_rev;
    if (!req.session.running_rev_instances) {
      req.session.running_rev_instances = [];
    }
    let newArray = req.session.running_rev_instances.filter(function (item) {
      return item.id_test_instance_rev != id_test_instance_rev;
    });
    newArray.push({
      id_test_instance_rev,
      show_solutions,
      ts: new Date(),
    });
    winston.info(
      req.session.passport.user +
        ': SETTING req.session.running_rev_instances to ' +
        JSON.stringify(newArray)
    );
    req.session.running_rev_instances = newArray;
  },
  getCurrTestInstanceRev: async (req, storeFoldIn = true) => {
    let rv = {
      id_test_instance_rev: req.session.id_test_instance_rev, // the LAST started review
    };
    req.session.running_rev_instances = req.session.running_rev_instances || [];
    if (req.query.idti) {
      rv = req.session.running_rev_instances.find((item) => {
        return item.id_test_instance_rev == req.query.idti;
      });
      if (!rv) {
        // Sometimes, rarely, the idti is not in the session, but it is in the DB.
        // I don't know why (could be that student visits the instances page in other tab eg), but I'll try to recover it.
        rv = await runningInstances.canSee(req.query.idti, req.session.studentId, true);
        if (rv && rv.id_test_instance_rev) {
          // I don't want to store fold-ins for eg logging ops that may arrive after submit (bcs I clean the exam data on submit)
          if (storeFoldIn) {
            req.session.running_rev_instances.push(rv);
            winston.info(`Recovered in REVIEW: fold in idti: ${JSON.stringify(rv)}`);
          }
        } else {
          winston.error(
            `HACK attack? Can not find idti ${req.query.idti} in ${JSON.stringify(
              req.session.running_rev_instances
            )} for user ${req.session.passport.user} NOR in canSee (database)!`
          );
        }
      }
    } else if (!req.session.id_test_instance_rev) {
      winston.error(
        `PANIC: idti and req.session.id_test_instance_rev is undefined for user ${
          req.session.passport.user
        }: idti = ${req.query.idti}, running_instances = ${JSON.stringify(
          req.session.running_rev_instances
        )}`
      );
    }
    return rv;
  },
  getCurrIdTestInstanceRev: async (req) => {
    let rv = await runningInstances.getCurrTestInstanceRev(req);
    rv = rv && rv.id_test_instance_rev;
    return rv;
  },
  // ********************************************************************************
  // ********************   Cleanup:  ************************************************
  // ********************************************************************************
  removeFromRunningTestInstances(req, currTI) {
    winston.info(
      `Removing ${currTI} from running instances for user ${
        req.session.passport.user
      }. Before: ${JSON.stringify({
        id_test_instance: req.session.id_test_instance,
        running_instances: req.session.running_instances,
      })}`
    );
    delete req.session.id_test_instance;
    let newArray = req.session.running_instances
      ? req.session.running_instances.filter(function (item) {
          return item.id_test_instance != currTI;
        })
      : [];
    req.session.running_instances = newArray;
    winston.info(
      `Removed ${currTI} from running instances for user ${
        req.session.passport.user
      }. After: ${JSON.stringify({
        id_test_instance: req.session.id_test_instance,
        running_instances: req.session.running_instances,
      })}`
    );
  },
};

module.exports = runningInstances;
