//export { };
let winston = require('winston');
const utils = require('./../common/utils');
import { ScoreResult } from './Score';
import Score from './Score';
import CodeRunnerService from './CodeRunnerService';
import { ScoringEnvironment } from './IQuestionScorer';
const safeEval = require('safe-eval');
const templateService = require('../services/templateService');

export default class CustomEvalScriptScorer {
  private gradingModel: GradingModel;
  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Custom Eval Script Scorer';
  }
  // does not traverse the prototype chain, that's all right here
  getMethods = (obj) =>
    Object.getOwnPropertyNames(obj).filter((item) => typeof obj[item] === 'function');

  async getScore(scoreResult: ScoreResult): Promise<ScoreResult> {
    try {
      scoreResult.record.gradingModel = this.gradingModel;
      //if (scoreResult.record.data_object) {
      let evalResult = await templateService.evalDataObject(
        scoreResult.record.id_course,
        scoreResult.record.data_object
      );
      scoreResult.dataObject = evalResult.data_object;
      scoreResult.dataObject.eval_log = evalResult.log;
      //}
      // For convinence, I'll copy the methods from the nested dataObject to the "root"
      //   so that the teachers can use them via this.method = this.dataObject.method;
      // I will leave the data enclosed inside the dataObject property so as not to clash with scoreResult props
      let methods = this.getMethods(scoreResult.dataObject);
      methods.forEach(
        (methodName) => (scoreResult[methodName] = scoreResult.dataObject[methodName])
      );

      if (scoreResult.record.id_script_programming_language == 1) {
        // JS is hard-coded to 1.
        let userObject = safeEval(scoreResult.record.eval_script, scoreResult);
        Object.assign(userObject, scoreResult);
        if (userObject.getScore) {
          scoreResult.score = userObject.getScore();
        } else {
          throw 'Custom JS eval script does not have getScore() method';
        }
      } else {
        let res: any = await CodeRunnerService.execCustomCodeWithInput(
          scoreResult.record.id_course,
          scoreResult.record.eval_script,
          {
            id: 1,
            input: JSON.stringify({
              dataObject: scoreResult.dataObject,
              score: scoreResult.score,
              j0: scoreResult.j0,
              record: scoreResult.record,
              data: scoreResult.data,
            }),
          },
          scoreResult.record.id_script_programming_language,
          0,
          ScoringEnvironment.Irrelevant
        );
        scoreResult.stdout = res.results[0].stdout;
        scoreResult.score = JSON.parse(res.results[0].stdout);
      }

      return scoreResult;
    } catch (error) {
      winston.error(error);
      scoreResult.score = Score.newEmptyScore(
        "Error occured during teacher's CUSTOM eval script. Please fix your script."
      );
      scoreResult.success = false;
      scoreResult.error = error;
      scoreResult.error.errorMessage = '' + error;
      return scoreResult;
    }
  }
}
