var winston = require('winston');
import ABCQuestionScorer from './ABCQuestionScorer';
import SQLQuestionScorer from './SQLQuestionScorer';
import JSONQuestionScorer from './JSONQuestionScorer';
import CodeQuestionScorer from './CodeQuestionScorer';
import FreeTextQuestionScorer from './FreeTextQuestionScorer';
import CustomEvalScriptScorer from './CustomEvalScriptScorer';
import Score, { ScoreResult } from './Score';
import ScorerService from './ScorerService';
import OrderedElementsQuestionScorer from './OrderedElementsQuestionScorer';
const ScriptLog = require.main.require('./models/ScriptLogModel.js');
import ConnectedElementsQuestionScorer from './ConnectedElementsQuestionScorer';
import DiagramQuestionScorer from './DiagramQuestionScorer';
import ComplexQuestionScorer from './ComplexQuestionScorer';
import { ScoringEnvironment } from './IQuestionScorer';

export = class Scorer {
  private static questionPassed(userAnswers, correctAnswers, weights, threshold): boolean {
    let sum = 0;
    for (let i = 0; userAnswers && i < userAnswers.length; ++i) {
      if (correctAnswers.indexOf(userAnswers[i]) >= 0) {
        sum += weights[userAnswers[i] - 1];
      }
    }
    return sum >= threshold;
  }

  private static testPartPassed(
    tpUserAnswers,
    tpCorrectAnswers,
    tpWeights,
    threshold,
    passPercentage
  ): boolean {
    let sum = 0;
    for (let i = 0; i < tpUserAnswers.length; ++i) {
      sum += this.questionPassed(tpUserAnswers[i], tpCorrectAnswers[i], tpWeights[i], threshold)
        ? 1
        : 0;
    }
    if (tpUserAnswers.length) {
      return sum / tpUserAnswers.length >= passPercentage;
    } else {
      return passPercentage === 0;
    }
  }

  // testPassed (tpUserAnswers, tpCorrectAnswers, tpWeights, threshold, passPercentage): boolean {
  //     return this.testPartPassed(tpUserAnswers, tpCorrectAnswers, tpWeights, threshold, passPercentage);
  // }

  public static async scoreExam(id_test_instance) {
    let testParts = {}; // test parts are not ordered, so I'll have to gather questions for each test part:
    let testPartIds = new Set<number>(); // to keep a distinct list
    let codeQuestions = false;

    let rs = await ScorerService.getAllQuestionsForExamInstance(id_test_instance);
    let promises = [];

    for (let i = 0; i < rs.length; ++i) {
      let rec = rs[i];
      rec.i = i;

      if (rec.id_test_part) {
        if (testParts[rec.id_test_part] === undefined) {
          testParts[rec.id_test_part] = {
            userAnswers: [],
            correctAnswers: [],
            weights: [],
          };
        }
        testParts[rec.id_test_part].userAnswers.push(rec.student_answers);
        testParts[rec.id_test_part].correctAnswers.push(rec.correct_answers_permutation);
        testParts[rec.id_test_part].weights.push(rec.weights_permutation);
        // overwriting every time, that's ok, test part can hav only one grading scheme:
        testParts[rec.id_test_part].threshold = rec.threshold;
        testParts[rec.id_test_part].passPerc = Number.parseFloat(rec.tp_pass_perc);
        testPartIds.add(rec.id_test_part);
      }

      let gradingModel: GradingModel = {
        correct_score: Number.parseFloat(rec.correct_score),
        incorrect_score: Number.parseFloat(rec.incorrect_score),
        unanswered_score: Number.parseFloat(rec.unanswered_score),
      };
      rec.gradingModel = gradingModel;

      if (rec.sql_answer) {
        codeQuestions = true;
        promises.push(new SQLQuestionScorer(gradingModel).getScore(rec));
      } else if (rec.json_answer) {
        codeQuestions = true;
        promises.push(new JSONQuestionScorer(gradingModel).getScore(rec));
      } else if (rec.c_question_id) {
        codeQuestions = true;
        promises.push(
          new CodeQuestionScorer(gradingModel).getScore(rec, ScoringEnvironment.Submitted, 0) // submit (gradeTest) has 0 (highest) priority
        );
      } else if (rec.type_name === 'Free text') {
        // Free text qs are always correct
        promises.push(new FreeTextQuestionScorer(gradingModel).getScore(rec));
      } else if (rec.type_name.startsWith('Complex')) {
        // Free text qs are always correct
        promises.push(
          new ComplexQuestionScorer(gradingModel).getScore(rec, ScoringEnvironment.Submitted)
        );
      } else if (rec.type_name === 'OrderedElements') {
        promises.push(new OrderedElementsQuestionScorer(gradingModel).getScore(rec));
      } else if (rec.type_name === 'ConnectedElements') {
        promises.push(new ConnectedElementsQuestionScorer(gradingModel).getScore(rec));
      } else if (rec.type_name.toUpperCase().indexOf('DIAGRAM') >= 0) {
        promises.push(new DiagramQuestionScorer(gradingModel).getScore(rec));
      } else {
        // ABC question or Scale question
        promises.push(new ABCQuestionScorer(gradingModel).getScore(rec));
      }
    }

    let qscores: Array<ScoreResult> = await Promise.all(promises);

    let userEvals = [];
    for (let i = 0; i < qscores.length; ++i) {
      if (qscores[i].record.eval_script) {
        userEvals.push(
          new CustomEvalScriptScorer(qscores[i].record.gradingModel).getScore(qscores[i])
        );
      }
    }
    let userScores: Array<ScoreResult> = await Promise.all(userEvals);
    // console.log("***************   userScores *******************");
    // console.log(JSON.stringify(userScores, null, 2));

    for (let i = 0; i < userScores.length; ++i) {
      qscores.find((x) => x.record.i === userScores[i].record.i).score = userScores[i].score;
      await ScriptLog.log(
        userScores[i].record.id,
        true,
        `Score: ${userScores[i].score}\nData object:\n${JSON.stringify(userScores[i].dataObject)}`
      );
      // console.log("Updated: ", qscores.find(x => x.record.i === userScores[i].record.i));
    }

    let tscore = {
      correct_no: 0,
      incorrect_no: 0,
      unanswered_no: 0,
      partial_no: 0,
      score: 0,
      score_perc: 0,
      qscores: [],
      passed: false,
    };
    for (let i = 0; i < qscores.length; ++i) {
      let item: ScoreResult = qscores[i];
      let qscore = item.score;
      // console.log('qscore', qscore);
      qscore.id_test_instance_question = item.record.id;
      //console.log(`Submit test, id_test_instance = ${id_test_instance}, resolved promise, rec.ordinal = ${item.rec.ordinal}`);
      tscore.qscores[item.record.i] = qscore; // TODO: wtf?
      tscore.correct_no += qscore.is_correct ? 1 : 0;
      tscore.incorrect_no += qscore.is_incorrect ? 1 : 0;
      tscore.unanswered_no += qscore.is_unanswered ? 1 : 0;
      tscore.partial_no += qscore.is_partial ? 1 : 0;
      tscore.score += qscore.score;
    }
    let maxScore = Number.parseFloat(qscores[0].record.max_score);
    if (maxScore === 0.0) {
      tscore.score_perc = 1.0; // glass half full kind of guy...
    } else {
      tscore.score_perc = tscore.score / maxScore;
    }

    let testPassed = tscore.score_perc >= Number.parseFloat(qscores[0].record.test_pass_perc);
    // Test parts passed are not supported for code questions and mixed content for the time being, TODO.
    if (!codeQuestions) {
      let allTpPassed = true;
      // Calculate whether test parts passed:
      for (let id of Array.from(testPartIds.values())) {
        if (
          !this.testPartPassed(
            testParts[id].userAnswers,
            testParts[id].correctAnswers,
            testParts[id].weights,
            testParts[id].threshold,
            testParts[id].passPerc
          )
        ) {
          allTpPassed = false;
          break;
        }
      }
      tscore.passed = allTpPassed && testPassed;
    } else {
      tscore.passed = testPassed;
    }

    return tscore;
  }

  public static async gradeLectureQuiz(id_lecture_quiz_instance, database) {
    var lscore = {
      correct_no: 0,
      incorrect_no: 0,
      unanswered_no: 0,
      partial_no: 0,
      score: 0,
      qscores: [],
      score_perc: 0,
    };

    let rs = await database.any(
      `SELECT tiq.id,
            tiq.ordinal,
            tiq.answers_permutation,
            tiq.weights_permutation,
            tiq.penalty_percentage_permutation,
            tiq.student_answers,
            tiq.student_answer_text,
            tiq.correct_answers_permutation,
            gm.correct_score,
            gm.incorrect_score,
            gm.unanswered_score,
            gm.threshold,
            test.max_score
        FROM test_instance ti
        JOIN test_instance_question tiq
          ON ti.id = tiq.id_test_instance
        JOIN test
          ON ti.id_test = test.id
        JOIN grading_model  gm
          ON tiq.id_grading_model = gm.id

        WHERE ti.id = $(id_lecture_quiz_instance)
        ORDER BY ordinal`,
      {
        id_lecture_quiz_instance: id_lecture_quiz_instance,
      }
    );

    var qscores: Array<ScoreResult> = [];

    for (let i = 0; i < rs.length; ++i) {
      let rec = rs[i];
      rec.i = i;

      let gradingModel = {
        correct_score: Number.parseFloat(rec.correct_score),
        incorrect_score: Number.parseFloat(rec.incorrect_score),
        unanswered_score: Number.parseFloat(rec.unanswered_score),
      };

      if (rec.student_answer_text !== null) {
        // grade text questions?
        // Yes -> they are always correct!
        qscores.push({
          record: rec,
          score: Score.newCorrectScore(gradingModel),
        });
      } else {
        // ABC question
        qscores.push(await new ABCQuestionScorer(gradingModel).getScore(rec));
      }
    }

    for (let i = 0; i < qscores.length; ++i) {
      var item = qscores[i];
      let qscore = item.score;
      qscore.id_test_instance_question = item.record.id;
      lscore.qscores[item.record.i] = qscore;
      lscore.correct_no += qscore.is_correct ? 1 : 0;
      lscore.incorrect_no += qscore.is_incorrect ? 1 : 0;
      lscore.unanswered_no += qscore.is_unanswered ? 1 : 0;
      lscore.partial_no += qscore.is_partial ? 1 : 0;
      lscore.score += qscore.score;
    }
    if (Number.parseFloat(qscores[0].record.max_score) > 0) {
      lscore.score_perc = lscore.score / Number.parseFloat(qscores[0].record.max_score);
    } else {
      lscore.score_perc = 0;
    }
    // console.log('Resolve overall lscore: ', lscore);
    return lscore;
  }

  // Only for ad-hoc checking of questions at runtime
  // does not make sense for ABC, Free text, etc.
  public static async scoreQuestionAtRuntime(
    id_test_instance,
    ordinal,
    code: string,
    id_programming_language?: number,
    priority?: number
  ) {
    let rec = await ScorerService.getQuestionForExamInstance(id_test_instance, ordinal);
    let gradingModel: GradingModel = {
      correct_score: Number.parseFloat(rec.correct_score),
      incorrect_score: Number.parseFloat(rec.incorrect_score),
      unanswered_score: Number.parseFloat(rec.unanswered_score),
    };
    rec.student_answer_code = code;
    let result: ScoreResult;
    if (rec.sql_answer) {
      result = await new SQLQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.json_answer) {
      result = await new JSONQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.c_question_id) {
      rec.student_answer_code_pl = id_programming_language;
      result = await new CodeQuestionScorer(gradingModel).getScore(
        rec,
        ScoringEnvironment.Runtime,
        priority
      );
    } else if (rec.type_name.toUpperCase().startsWith('COMPLEX')) {
      rec.text_answer = code;
      result = await new ComplexQuestionScorer(gradingModel).getScore(
        rec,
        ScoringEnvironment.Runtime,
        priority
      );
    } else {
      throw new Error('Unknown question type for ad-hoc/runtime scoring...');
    }
    if (result.record.eval_script) {
      result = await new CustomEvalScriptScorer(gradingModel).getScore(result);
    }
    delete result.record;
    return result;
  }

  // Only for ad-hoc checking of questions at design time or at tutorials etc
  //
  public static async scoreQuestionNoExam(
    id_course,
    id_question,
    code: string,
    id_programming_language?: number,
    priority?: number,
    eval_script?: string
  ) {
    let rec = await ScorerService.getQuestionNoExam(id_course, id_question);
    let gradingModel: GradingModel = {
      correct_score: 1,
      incorrect_score: -1,
      unanswered_score: 0,
    };
    let result: ScoreResult;
    if (rec.sql_answer) {
      rec.student_answer_code = code;
      rec.sql_answer = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      result = await new SQLQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.json_answer) {
      rec.student_answer_code = code;
      rec.json_answer = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      result = await new JSONQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.c_question_id) {
      rec.student_answer_code = code;
      rec.c_source = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      rec.student_answer_code_pl = id_programming_language;
      result = await new CodeQuestionScorer(gradingModel).getScore(rec, ScoringEnvironment.Runtime);
    } else if (rec.type_name.toUpperCase().indexOf('FREE TEXT') >= 0) {
      rec.student_answer_text = code;
      result = {
        score: Score.newCorrectScore(gradingModel),
        record: rec,
        success: true,
      };
    } else {
      throw new Error('Unknown question type for no-exam scoring...');
    }
    // console.log("Result is (1):");
    // console.log(JSON.stringify(result, null, 2));
    // console.log("on to custom script...");
    if (result.record.eval_script || eval_script) {
      if (eval_script) result.record.eval_script = eval_script;
      result = await new CustomEvalScriptScorer(gradingModel).getScore(result);
    }
    // console.log("***************   userScores *******************");
    // console.log(JSON.stringify(result, null, 2));
    return result;
  }
  public static async scoreQuestionTutorial(
    id_course,
    id_question,
    code: string,
    id_programming_language?: number,
    priority?: number,
    eval_script?: string
  ) {
    let rec = await ScorerService.getQuestionNoExam(id_course, id_question);
    let gradingModel: GradingModel = {
      correct_score: 1,
      incorrect_score: -1,
      unanswered_score: 0,
    };
    let result: ScoreResult;
    if (rec.sql_answer) {
      rec.student_answer_code = code;
      //rec.sql_answer = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      result = await new SQLQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.json_answer) {
      throw new Error('todo');
      rec.student_answer_code = code;
      rec.json_answer = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      result = await new JSONQuestionScorer(gradingModel).getScore(rec);
    } else if (rec.c_question_id) {
      rec.student_answer_code = code;
      //rec.c_source = code; // THIS ALSO, but it's OK - this is DESIGN time only!!
      rec.student_answer_code_pl = id_programming_language;
      result = await new CodeQuestionScorer(gradingModel).getScore(
        rec,
        ScoringEnvironment.Tutorial
      );
    } else if (rec.type_name.toUpperCase().indexOf('FREE TEXT') >= 0) {
      throw new Error('todo');
      rec.student_answer_text = code;
      result = {
        score: Score.newCorrectScore(gradingModel),
        record: rec,
        success: true,
      };
    } else {
      throw new Error('Unknown question type for no-exam scoring...');
    }
    // console.log("Result is (1):");
    // console.log(JSON.stringify(result, null, 2));
    // console.log("on to custom script...");
    if (result.record.eval_script || eval_script) {
      if (eval_script) result.record.eval_script = eval_script;
      result = await new CustomEvalScriptScorer(gradingModel).getScore(result);
    }
    // console.log("***************   userScores *******************");
    // console.log(JSON.stringify(result, null, 2));
    return result;
  }
};
