let winston = require('winston');
import { IQuestionScorer } from './IQuestionScorer';
import Score from './Score';
import { ScoreResult } from './Score';
import AbstractQuestionScorer from './AbstractQuestionScorer';

export default class ConnectedElementsQuestionScorer extends AbstractQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    super();
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default ConnectedElements Question Scorer';
  }

  getScore(record): Promise<ScoreResult> {
    try {
      let {
        student_answers,
        correct_answers_permutation,
        correctness,
      } = record;

      let scoreResult: ScoreResult = {
        score: null,
        record: record,
      };

      if (student_answers === null || student_answers.length === 0) {
        scoreResult.score = Score.newUnansweredScore(this.gradingModel);
        return Promise.resolve(scoreResult);
      }

      let penalty = 0;
      let differences = this.countDifferences(student_answers, correct_answers_permutation);

      // eg correctens = {90,70}   bcs leading 100 is implicit
      correctness.unshift(100);
      // {100, 90,70}
      penalty =  (differences >= correctness.length) ? 100 : 100 - correctness[differences];

      scoreResult.score = this.getScoreFromPenalty(penalty, this.gradingModel);
      if (penalty > 0)
        scoreResult.score.hint = `Partial score: penalty: ${penalty}%, differences: ${differences}`;
      return Promise.resolve(scoreResult);

    } catch (error) {
      winston.error(error);
      return Promise.resolve({
        score: Score.newEmptyScore('Error occured during grading connected elements question ...'),
        record: record,
        success: true,
      });
    }
  }

  countDifferences(arr1: number[], arr2: number[]): number {
    let diffCount = 0;
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        diffCount++;
      }
    }

    return diffCount;
  }
}
