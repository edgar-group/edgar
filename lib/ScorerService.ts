const db = require('../db').db;

export default class ScorerService {
  public static async getAllQuestionsForExamInstance(id_test_instance) {
    return await this._getQuestionsForExamInstance(id_test_instance, undefined);
  }
  public static async getQuestionForExamInstance(id_test_instance, ordinal) {
    return (await this._getQuestionsForExamInstance(id_test_instance, ordinal))[0];
  }
  private static async _getQuestionsForExamInstance(id_test_instance, ordinal) {
    return await db.any(
      `SELECT
            id_course,
            question_type.type_name,
            tiq.id_question,
            tiq.ordinal,
            tiq.answers_permutation,
            tiq.weights_permutation,
            tiq.penalty_percentage_permutation,
            tiq.student_answers,
            tiq.correct_answers_permutation,
            tiq.student_answer_code,
            tiq.student_answer_code_pl,
            tiq.student_answer_text,
            tiq.uploaded_files,
            diagram_question_answer.diagram_answer,
            gm.correct_score,
            gm.incorrect_score,
            gm.unanswered_score,
            gm.threshold,
            CASE WHEN (oecm.correctness IS NOT NULL) THEN oecm.correctness ELSE cecm.correctness END AS correctness,
            test.pass_percentage as test_pass_perc,
            test_part.pass_percentage as tp_pass_perc,
            test.max_score,
            test_part.id as id_test_part,
            tiq.id,
            sqlqa.sql_alt_assertion,
            sqlqa.sql_test_fixture,
            sqlqa.sql_answer,
            sqlqa.check_tuple_order,
            sqlqa.id_check_column_mode,

            tqa.text_answer,

            jsonqa.json_alt_assertion,
            jsonqa.json_test_fixture,
            jsonqa.json_answer,
            jsonqa.assert_deep,
            jsonqa.assert_strict,

            cqa.c_prefix, cqa.c_source, cqa.c_suffix, cqa.id_question as c_question_id,
            cqa.id as c_question_answer_id,
            CASE
              WHEN (cqa.id_programming_language IS NOT NULL) THEN cqa.id_programming_language
              ELSE question_programming_language.id_programming_language  -- used for complex questions
            END AS id_programming_language,

            gend.data_object,
            script.eval_script,
            script.id_script_programming_language,
            complex_question.setup_script,
            complex_question.test_cases,
            ti.ts_submitted,
            ti.id as id_test_instance

        FROM test_instance ti
        JOIN test_instance_question tiq
          ON ti.id = tiq.id_test_instance
        JOIN question
          ON tiq.id_question = question.id
        JOIN question_type
          ON question.id_question_type = question_type.id
        JOIN test
          ON ti.id_test = test.id
        JOIN grading_model  gm
          ON tiq.id_grading_model = gm.id
        LEFT JOIN diagram_question_answer
               ON question.id = diagram_question_answer.id_question
        LEFT JOIN ordered_element_question
               ON question.id = ordered_element_question.id_question
        LEFT JOIN ordered_element_correctness_model oecm
               ON ordered_element_question.id_ordered_element_correctness_model = oecm.id
        LEFT JOIN connected_elements_question
               ON question.id = connected_elements_question.id_question
        LEFT JOIN connected_elements_correctness_model cecm
               ON connected_elements_question.id_connected_elements_correctness_model = cecm.id
        LEFT JOIN test_part
               ON tiq.id_test_part = test_part.id
        LEFT JOIN sql_question_answer sqlqa
               ON tiq.id_question = sqlqa.id_question
        LEFT JOIN json_question_answer jsonqa
               ON tiq.id_question = jsonqa.id_question
        LEFT JOIN c_question_answer cqa
               ON tiq.id_question = cqa.id_question
        LEFT JOIN text_question_answer tqa
               ON tiq.id_question = tqa.id_question
        LEFT JOIN test_instance_question_generated gend
               ON gend.id_test_instance_question = tiq.id
        LEFT JOIN question_eval_script script
               ON script.id_question = question.id
        LEFT JOIN complex_question
               ON complex_question.id_question = question.id
        LEFT JOIN question_programming_language
               ON question_programming_language.id_question = question.id

        WHERE ti.id = $(id_test_instance)
          ${ordinal ? ' AND tiq.ordinal = $(ordinal)' : ''}
        ORDER BY tiq.ordinal`,
      { id_test_instance, ordinal }
    );
  }

  public static async getQuestionNoExam(id_course, id_question) {
    return await db.one(
      `SELECT
            $(id_course) AS id_course,
            question_type.type_name,
            question.id as id_question,
            ---
            sqlqa.sql_alt_assertion,
            sqlqa.sql_test_fixture,
            sqlqa.sql_answer,
            sqlqa.check_tuple_order,
            sqlqa.id_check_column_mode,
            ---
            tqa.text_answer,
            ---
            jsonqa.json_alt_assertion,
            jsonqa.json_test_fixture,
            jsonqa.json_answer,
            jsonqa.assert_deep,
            jsonqa.assert_strict,
            ---
            cqa.c_prefix, cqa.c_source, cqa.c_suffix, cqa.id_question as c_question_id,
            cqa.id as c_question_answer_id, cqa.id_programming_language,
            ----
            qdo.data_object,
            script.eval_script,
            script.id_script_programming_language
        FROM question
        JOIN question_type
          ON question.id_question_type = question_type.id
        LEFT JOIN sql_question_answer sqlqa
               ON question.id = sqlqa.id_question
        LEFT JOIN json_question_answer jsonqa
               ON question.id = jsonqa.id_question
        LEFT JOIN text_question_answer tqa
               ON question.id = tqa.id_question
        LEFT JOIN c_question_answer cqa
               ON question.id = cqa.id_question
        LEFT JOIN question_data_object qdo     -- NOTE THE DIFF (not "test_instance_question_generated"); this is design time!
               ON qdo.id_question = question.id
        LEFT JOIN question_eval_script script
               ON script.id_question = question.id

        WHERE question.id = $(id_question)
        `,
      { id_question, id_course }
    );
  }
  public static async getTestsForCodeQuestion(c_question_answer_id) {
    return await db.any(
      `SELECT
            100 + row_number() over (order by cqt.ordinal, cqt.id) as test_case_ordinal,
            cqt.id, cqt.comment,
            cqa.c_prefix, cqa.c_source, cqa.c_suffix, cqa.id_question as c_question_id,
            cqa.id_programming_language,
            cqt.c_test_type_id, cqt.percentage, cqt.allow_diff_order,
            cqt.allow_diff_letter_size, cqt.trim_whitespace, cqt.regex_override, cqt.is_public,
            fxt.input, fxt.output,
            rmt.random_test_type_id, rmt.low_bound, rmt.upper_bound, rmt.elem_count
            -- gnt.arguments, gtf.file

            FROM c_question_answer cqa
            JOIN c_question_test cqt
              ON cqa.id = cqt.c_question_answer_id
            LEFT JOIN fixed_test fxt
              ON cqt.id = fxt.c_question_test_id
            LEFT JOIN random_test rmt
              ON cqt.id = rmt.c_question_test_id
            WHERE cqa.id = $(c_question_answer_id)
            ORDER BY cqt.id`,
      {
        c_question_answer_id: c_question_answer_id,
      }
    );
  }
}
