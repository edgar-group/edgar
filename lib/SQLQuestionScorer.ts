//export { };
let winston = require('winston');
const utils = require('./../common/utils');
import { ScoreResult } from './Score';
import Score from './Score';
import { IQuestionScorer } from './IQuestionScorer';
import CodeRunnerService from './CodeRunnerService';

export default class SQLQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;
  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default SQL Question Scorer';
  }

  async getScore(rec): Promise<ScoreResult> {
    try {
      let res: ScoreResult;
      let corrCode = utils.mergeSQLCode(
        rec.sql_test_fixture,
        rec.sql_answer,
        rec.sql_alt_assertion
      );
      let studCode = utils.mergeSQLCode(
        rec.sql_test_fixture,
        rec.student_answer_code,
        rec.sql_alt_assertion
      );

      let lcode = (corrCode + studCode).toLowerCase();
      if (
        lcode.indexOf('insert') > 0 ||
        lcode.indexOf('update') > 0 ||
        lcode.indexOf('delete') > 0 ||
        lcode.indexOf('alter') > 0
      ) {
        // execute sequentially to avoid deadlock:
        res = await this.execAndScoreSQLSequential(
          rec.id_question,
          studCode,
          corrCode,
          rec.check_tuple_order,
          rec.id_check_column_mode
        );
      } else {
        res = await this.execAndScoreSQLParallel(
          rec.id_question,
          studCode,
          corrCode,
          rec.check_tuple_order,
          rec.id_check_column_mode
        );
      }
      res.record = rec;
      res.success = true;
      return res;
    } catch (error) {
      winston.error(error);
      return {
        score: Score.newEmptyScore('Error occured during scoring...'),
        record: rec,
        data: 'Error occured:' + JSON.stringify(error),
        success: true,
        error: error,
      };
    }
  }

  getSQLQuestionScore(userRs, correctRs, checkTupleOrder, checkColumnMode): Score {
    if (!correctRs || !correctRs.rows) {
      return Score.newUnansweredScore(
        this.gradingModel,
        "Question has a wrong answer (teacher's error!)!!"
      );
    }
    if (!userRs) {
      return Score.newInCorrectScore(this.gradingModel, 'No dataset, query failed?');
    }
    if (!userRs.rows || userRs.rows.length !== correctRs.rows.length) {
      return Score.newInCorrectScore(this.gradingModel, 'Uneven row count.');
    }

    if (correctRs.rows.length) {
      /*var userCols = [];
            var correctCols = [];
            var corr2userMapping = {};
            for (let property in userRs[0]) {
                if (userRs[0].hasOwnProperty(property)) {
                    userCols.push(property);
                }
            }
            for (let property in correctRs[0]) {
                if (correctRs[0].hasOwnProperty(property)) {
                    correctCols.push(property);
                }
            }*/
      if (userRs.fields.length !== correctRs.fields.length) {
        return Score.newInCorrectScore(this.gradingModel, 'Different number of columns.');
      }
      if (checkColumnMode === 1 || checkColumnMode === 2) {
        for (let i = 0; checkColumnMode === 1 && i < correctRs.fields.length; ++i) {
          if (correctRs.fields[i].name.toLowerCase() !== userRs.fields[i].name.toLowerCase()) {
            return Score.newInCorrectScore(
              this.gradingModel,
              'Uneven columns order (checkColumnMode=1).'
            );
          }
        }
        // Everything is the same, setup trivial mapping:
        for (let i = 0; i < correctRs.fields.length; ++i) {
          correctRs.fields[i].mapping = { correct: 'C' + i, user: 'C' + i };
        }
      } else if (checkColumnMode === 3 || checkColumnMode === 4) {
        var used = {};
        for (let i = 0; i < correctRs.fields.length; ++i) {
          for (let j = 0; j < userRs.fields.length; ++j) {
            if (
              correctRs.fields[i].name.toLowerCase() === userRs.fields[j].name.toLowerCase() &&
              !used[j]
            ) {
              correctRs.fields[i].mapping = { correct: 'C' + i, user: 'C' + j };
              used[j] = true;
              break;
            }
          }
          if (!correctRs.fields[i].mapping) {
            if (checkColumnMode === 3) {
              return Score.newInCorrectScore(
                this.gradingModel,
                `Can not find correct recordset's column "${correctRs.fields[i].name}" in user's recordset.`
              );
            } else {
              // 4 is the most permissive mode, could not match them by name, so fallback to order:
              for (let k = 0; k < correctRs.fields.length; ++k) {
                correctRs.fields[k].mapping = {
                  correct: 'C' + k,
                  user: 'C' + k,
                };
              }
              break; // !!
            }
          }
        }
      } else {
        return Score.newUnansweredScore(
          this.gradingModel,
          "Unknown checkColumnMode (teacher's error!)!!"
        );
      }
      if (checkTupleOrder) {
        for (let i = 0; i < correctRs.rows.length; ++i) {
          if (!this.isTupleEqual(correctRs.rows[i], userRs.rows[i], correctRs.fields)) {
            return Score.newInCorrectScore(
              this.gradingModel,
              this.getTupleDiffHint(correctRs.rows[i], userRs.rows[i], correctRs.fields, i)
            );
          }
        }
      } else {
        var dupCorrectRows = correctRs.rows.slice();
        for (let i = 0; i < userRs.rows.length; ++i) {
          let found = false;
          for (let j = 0; j < dupCorrectRows.length; ++j) {
            if (this.isTupleEqual(dupCorrectRows[j], userRs.rows[i], correctRs.fields)) {
              dupCorrectRows.splice(j, 1);
              found = true;
              break;
            }
          }
          if (!found) {
            return Score.newInCorrectScore(
              this.gradingModel,
              `Cannot find user row #${i + 1} in correct rows (check tuple order is OFF).`
            );
          }
        }
      }
    }
    return Score.newCorrectScore(this.gradingModel);
  }

  isTupleEqual(correctTuple, userTuple, cols): boolean {
    for (let j = 0; j < cols.length; ++j) {
      /*console.log(' comparing:', correctTuple[cols[j].mapping.correct] , userTuple[cols[j].mapping.user]);*/
      if (!cols[j].mapping) {
        return false;
      } else if (
        correctTuple[cols[j].mapping.correct] instanceof Array && // Arrays are compared using toString representation
        userTuple[cols[j].mapping.user] instanceof Array
      ) {
        // which is generally NOT correct, but for this purpose seems to be best fitting.
        if (
          correctTuple[cols[j].mapping.correct].toString() !==
          userTuple[cols[j].mapping.user].toString()
        ) {
          return false;
        }
      } else if (correctTuple[cols[j].mapping.correct] !== userTuple[cols[j].mapping.user]) {
        // This will catch BOTH:
        //   1. type differences, and
        //   2. value differences
        // As of 2017-11-02, regular values are also compared as strings!!
        // TODO: this should be a question parameter, so that we can have strict and loose comparison
        // So, let's UNDO type differences:
        let correctValue = correctTuple[cols[j].mapping.correct]
          ? correctTuple[cols[j].mapping.correct].toString()
          : '<NULL>';
        let userValue = userTuple[cols[j].mapping.user]
          ? userTuple[cols[j].mapping.user].toString()
          : '<NULL>';
        let correctValueNum = new Number(correctValue);
        let userValueNum = new Number(userValue);
        // If they can both be parsed as numbers, I'll compare them as numbers
        // e.g. "2.70" and "2.7" will be evaluated as equal
        /*console.log(`${correctValue} ${correctValueNum} ${userValue} ${userValueNum}`);*/
        // JS is a mess!! If I use the recommended:
        //     https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/instanceof
        // I will fail miserably, since these values will not be number but object, and Numner.isNaN will return false!
        //   so I'm casting to fall back on the global isNaN:
        if (!isNaN(correctValueNum as number) && !isNaN(userValueNum as number)) {
          if (correctValueNum.toString() !== userValueNum.toString()) {
            return false;
          }
        } else if (correctValue.trim() !== userValue.trim()) {
          // Added trim() to make the comparison even more "loose"
          return false;
        }
      }
    }
    return true;
  }

  getTupleDiffHint(correctTuple, userTuple, cols, rownum): string {
    for (let j = 0; j < cols.length; ++j) {
      if (!cols[j].mapping) {
        return `Can not find correct recordset's column ${cols[j].name} in user's recordset.`;
      }
      // using concatenation instead of correctTuple[cols[j].mapping.correct].toString() to handle null (ie avoid null.toString())
      if ('' + correctTuple[cols[j].mapping.correct] !== '' + userTuple[cols[j].mapping.user]) {
        //console.log(`${typeof(userTuple[cols[j].mapping.user])} !== ${typeof(correctTuple[cols[j].mapping.correct])}`);
        return `Row #${rownum + 1}: user(${cols[j].name}) !== correct(${cols[j].name})): ${
          userTuple[cols[j].mapping.user]
        } !== ${correctTuple[cols[j].mapping.correct]}`;
      }
    }
    return '';
  }

  async execAndScoreSQLParallel(
    id_question,
    userSQL,
    correctSQL,
    checkTupleOrder,
    checkColumnMode
  ): Promise<ScoreResult> {
    if (userSQL.trim() === '') {
      return {
        score: Score.newUnansweredScore(this.gradingModel, 'Empty answer.'),
      };
    } else {
      var promises = [];
      // note the false flags - bcs I already added prefix+suffix bcs I wanted to see if there are any ins/upd/del command in the entire script:
      promises.push(CodeRunnerService.execSQLCodeQuestion(id_question, userSQL, false, false));
      promises.push(CodeRunnerService.execSQLCodeQuestion(id_question, correctSQL, false, false));
      let results = await Promise.all(promises);
      if (results[0].success) {
        return {
          score: this.getSQLQuestionScore(
            results[0].data,
            results[1].data,
            checkTupleOrder,
            checkColumnMode
          ),
          data: results[0].data,
          db: results[0].db,
        };
      } else {
        return {
          score: Score.newInCorrectScore(this.gradingModel, JSON.stringify(results[0].error)),
          error: results[0].error,
        };
      }
    }
  }
  async execAndScoreSQLSequential(
    id_question,
    userSQL,
    correctSQL,
    checkTupleOrder,
    checkColumnMode
  ): Promise<ScoreResult> {
    if (userSQL.trim() === '') {
      return {
        score: Score.newUnansweredScore(this.gradingModel, 'Empty answer.'),
      };
    } else {
      // note the false flags - bcs I already added prefix+suffix bcs I wanted to see if there are any ins/upd/del command in the entire script:
      let userResult: any = await CodeRunnerService.execSQLCodeQuestion(
        id_question,
        userSQL,
        false,
        false
      );
      if (userResult.success) {
        let corrResult: any = await CodeRunnerService.execSQLCodeQuestion(
          id_question,
          correctSQL,
          false,
          false
        );
        return {
          score: this.getSQLQuestionScore(
            userResult.data,
            corrResult.data,
            checkTupleOrder,
            checkColumnMode
          ),
          data: userResult.data,
          db: userResult.db,
        };
      } else {
        return {
          score: Score.newInCorrectScore(this.gradingModel, JSON.stringify(userResult.error)),
          error: userResult.error,
        };
      }
    }
  }
}
