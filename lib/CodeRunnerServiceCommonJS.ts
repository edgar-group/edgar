import CodeRunnerService from './CodeRunnerService';

export = class CodeRunnerServiceCommonJS {
  // This is just a bridge so that CommonJS modules can use CodeRunnerService as well as ES6 modules/typescript.

  public static async execSQLCodeCodeRunner(id_code_runner, code: string) {
    return CodeRunnerService.execSQLCodeCodeRunner(id_code_runner, code);
  }

  public static async execSQLCodeTestInstance(
    id_test_instance,
    ordinal,
    code: string,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return CodeRunnerService.execSQLCodeTestInstance(
      id_test_instance,
      ordinal,
      code,
      append_prefix_suffix,
      append_presentation_query
    );
  }

  public static async execSQLCodeQuestion(
    id_question,
    code: string,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return CodeRunnerService.execSQLCodeQuestion(
      id_question,
      code,
      append_prefix_suffix,
      append_presentation_query
    );
  }

  public static async execJSCodeTestInstance(
    id_test_instance,
    ordinal,
    code,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return CodeRunnerService.execJSCodeTestInstance(
      id_test_instance,
      ordinal,
      code,
      append_prefix_suffix,
      append_presentation_query
    );
  }

  public static async execJSCodeQuestion(
    id_question,
    code,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return CodeRunnerService.execJSCodeQuestion(
      id_question,
      code,
      append_prefix_suffix,
      append_presentation_query
    );
  }

  public static async execCodeQuestionWithInputs(
    id_question,
    code,
    inputs,
    id_course,
    id_programming_language,
    priority,
    env
  ) {
    return CodeRunnerService.execCodeQuestionWithInputs(
      id_question,
      code,
      inputs,
      id_course,
      id_programming_language,
      priority,
      env
    );
  }

  public static async execCustomCodeWithInput2(
    id_course,
    code,
    input,
    id_code_runner,
    priority,
    id_programming_language
  ) {
    return CodeRunnerService.execCustomCodeWithInput2(
      id_course,
      code,
      input,
      id_code_runner,
      priority,
      id_programming_language
    );
  }
};
