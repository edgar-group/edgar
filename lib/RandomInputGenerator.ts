let winston = require('winston');
const crypto = require('crypto');

const GENERATOR_SUBTYPE = {
  intNumber: 1,
  floatNumber: 2,
  fixAlphaText: 3,
  fixAllText: 4,
  rangeAlphaText: 5,
  rangeAllText: 6,
};
const charOptions = {
  alphaNum: 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789',
  alpha: 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ',
};

export default class RandomInputGenerator {
  public static printableChars(upTo?) {
    //127 normal , max 255
    if (upTo == undefined || upTo > 255) {
      upTo = 127;
    }
    var chars = [];
    for (let i = 32; i <= upTo; i++) {
      chars.push(String.fromCharCode(i));
    }
    var charsString = chars.join('');
    return charsString;
  }

  public static randomText(howMany, chars) {
    var rnd = crypto.randomBytes(howMany);
    var value = new Array(howMany);
    var len = chars.length;

    for (var i = 0; i < howMany; i++) {
      value[i] = chars[rnd[i] % len];
    }

    return value.join('');
  }

  public static randomNumbers(type, low, high, count, precision?) {
    var numbers = new Array(count);
    var stringNumbers = '';
    for (var i = 0; i < numbers.length; i++) {
      if (GENERATOR_SUBTYPE.intNumber == type) {
        numbers[i] = this.randomIntInclusive(low, high);
        stringNumbers += numbers[i] + ' ';
      } else {
        numbers[i] = this.randomFloat(low, high);

        if (precision != undefined) {
          numbers[i] = numbers[i].toFixed(precision);
        } else {
          numbers[i] = numbers[i].toFixed(2);
        }

        stringNumbers += numbers[i] + ' ';
      }
    }
    return stringNumbers;
  }

  public static randomFloat(low, high) {
    return Math.random() * (high - low) + low;
  }

  public static randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
  }

  public static randomIntInclusive(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
  }

  public static randomGenerator(type, lowBound, upperBound, count, precision?) {
    let howMany, chars;
    switch (type) {
      case GENERATOR_SUBTYPE.intNumber:
      case GENERATOR_SUBTYPE.floatNumber:
        var numbers = this.randomNumbers(type, lowBound, upperBound, count, precision);
        var numberss = [];
        numberss.push(numbers);
        return {
          inputs: numbers,
          inputCount: 1,
        };
      case GENERATOR_SUBTYPE.fixAllText: //fixed number, all printable characters
        howMany = count;
        chars = this.printableChars();
        var text = this.randomText(howMany, chars);
        var texts = [];
        texts.push(text);
        return {
          inputs: text,
          inputCount: 1,
        };
      case GENERATOR_SUBTYPE.fixAlphaText: //fixed number, only alphabet
        howMany = count;
        chars = charOptions.alpha;
        var text = this.randomText(howMany, chars);
        var texts = [];
        texts.push(text);
        return {
          inputs: text,
          inputCount: 1,
        };
      case GENERATOR_SUBTYPE.rangeAllText: //in range count, all printable characters
        howMany = this.randomIntInclusive(lowBound, upperBound);
        chars = this.printableChars();
        var text = this.randomText(howMany, chars);
        var texts = [];
        texts.push(text);
        return {
          inputs: text,
          inputCount: 1,
        };
      case GENERATOR_SUBTYPE.rangeAlphaText: //in range count, only alphabet
        howMany = this.randomIntInclusive(lowBound, upperBound);
        chars = charOptions.alpha;
        var text = this.randomText(howMany, chars);
        var texts = [];
        texts.push(text);
        return {
          inputs: text,
          inputCount: 1,
        };
      default:
        winston.error('Invalid generator type');
        throw 'Invalid generator type';
    }
  }

  public static randomNumbersGenerator(type, lowBound, upperBound, count, precision) {
    switch (type) {
      case GENERATOR_SUBTYPE.intNumber:
      case GENERATOR_SUBTYPE.floatNumber:
        return this.randomNumbers(type, lowBound, upperBound, count, precision);
      default:
        winston.error('invalid number type');
        throw 'Invalid number type';
    }
  }
  public static randomIntNumbersGenerator(lowBound, upperBound, count) {
    return this.randomNumbers(GENERATOR_SUBTYPE.intNumber, lowBound, upperBound, count);
  }

  public static randomFloatNumbersGenerator(lowBound, upperBound, count, precision) {
    return this.randomNumbers(
      GENERATOR_SUBTYPE.floatNumber,
      lowBound,
      upperBound,
      count,
      precision
    );
  }

  public static randomTextGenerator(type, count, lowBound, upperBound) {
    let howMany, chars;
    switch (type) {
      case GENERATOR_SUBTYPE.fixAllText: //fixed number, all printable characters
        howMany = count;
        chars = this.printableChars();
        break;
      case GENERATOR_SUBTYPE.fixAlphaText: //fixed number, only alphabet
        howMany = count;
        chars = charOptions.alpha;
        break;
      case GENERATOR_SUBTYPE.rangeAllText: //in range count, all printable characters
        howMany = this.randomIntInclusive(lowBound, upperBound);
        chars = this.printableChars();
        break;
      case GENERATOR_SUBTYPE.rangeAlphaText: //in range count, only alphabet
        howMany = this.randomIntInclusive(lowBound, upperBound);
        chars = charOptions.alpha;
        break;
      default:
        winston.error('invalid text generator type');
        throw 'Invalid text generator type';
    }
    var text = this.randomText(howMany, chars);
    return text;
  }
}
