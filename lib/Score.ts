export enum QuestionScoringMode {
  Deduct = 'Deduct',
  Add = 'Add',
  Compare = 'Compare',
}
export default class Score {
  is_correct: boolean = false;
  is_incorrect: boolean = false;
  is_unanswered: boolean = false;
  is_partial: boolean = false;
  score: number = 0;
  score_perc: number = 0;
  hint?: string;
  scoring_mode?: QuestionScoringMode;
  c_outcome?: any;

  id_test_instance_question?: number;

  public static newEmptyScore(hint: string = 'Empty score, error occured?'): Score {
    let rv = new Score();
    rv.score = 0;
    rv.score_perc = 0;
    rv.hint = hint;
    rv.is_unanswered = true;
    return rv;
  }
  public static newUnansweredScore(gm: GradingModel, hint: string = 'Unanswered.'): Score {
    let rv = new Score();
    rv.is_unanswered = true;
    rv.score = gm.unanswered_score;
    rv.score_perc = gm.correct_score !== 0 ? gm.unanswered_score / gm.correct_score : 0;
    rv.hint = hint;
    return rv;
  }
  public static newCorrectScore(gm: GradingModel, hint: string = 'Correct. Well done!'): Score {
    let rv = new Score();
    rv.is_correct = true;
    rv.score = gm.correct_score;
    rv.score_perc = 1.0;
    rv.hint = hint;
    return rv;
  }
  public static newInCorrectScore(gm: GradingModel, hint: string = 'Incorrect score.'): Score {
    let rv = new Score();
    rv.is_incorrect = true;
    rv.score = gm.incorrect_score;
    rv.score_perc = gm.correct_score !== 0 ? gm.incorrect_score / gm.correct_score : 0;
    rv.hint = hint;
    return rv;
  }
}

export class ScoreResult {
  score: Score;
  record?: any;
  stdout?: any;
  data?: any;
  success?: boolean;
  db?: string;
  j0?: any;
  error?: any;
  dataObject?: any;
  public static newEmptyQuestionRecord() {
    return {
      idCourse: 0,
      idQuestion: 0,
    };
  }
  public static newUnansweredScoreResult(
    gm: GradingModel,
    hint: string = 'Unanswered.'
  ): ScoreResult {
    return {
      score: Score.newUnansweredScore(gm, hint),
      record: ScoreResult.newEmptyQuestionRecord(),
    };
  }
  public static newEmptyScoreResult(hint: string): ScoreResult {
    return {
      score: Score.newEmptyScore(hint),
      record: ScoreResult.newEmptyQuestionRecord(),
    };
  }
  public static newCorrectScoreResult(gm: GradingModel, hint: string): ScoreResult {
    return {
      score: Score.newCorrectScore(gm, hint),
      record: ScoreResult.newEmptyQuestionRecord(),
    };
  }
  public static newInCorrectScoreResult(gm: GradingModel, hint: string): ScoreResult {
    return {
      score: Score.newInCorrectScore(gm, hint),
      record: ScoreResult.newEmptyQuestionRecord(),
    };
  }
}
