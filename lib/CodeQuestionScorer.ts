//export { };
let winston = require('winston');
const utils = require('./../common/utils');
import { ScoreResult } from './Score';
import Score from './Score';
import RandomInputGenerator from './RandomInputGenerator';
import { IQuestionScorer, ScoringEnvironment } from './IQuestionScorer';
import ScorerService from './ScorerService';
import CodeRunnerService from './CodeRunnerService';
import AbstractQuestionScorer from './AbstractQuestionScorer';
const minioService = require('./../services/minioService');
var globals = require('../common/globals');
const GENERATOR_TYPE = {
  fixed: 1,
  random: 2,
  generator: 3,
};
export interface ITestCaseRecord {
  id: number;
  test_case_ordinal: number;
  comment: string;
  cPrefix: string;
  cSource: string;
  cSuffix: string;
  c_question_id: number;
  id_programming_language: number;
  c_test_type_id: number;
  percentage: number;
  allow_diff_order: boolean;
  allow_diff_letter_size: boolean;
  trim_whitespace: boolean;
  regex_override: string;
  is_public: boolean;
  input: string;
  output: string;
  random_test_type_id: number;
  low_bound: number;
  upper_bound: number;
  elem_count: number;
}
export default class CodeQuestionScorer extends AbstractQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;
  constructor(model: GradingModel) {
    super();
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default Code Question Scorer';
  }

  async getScore(rec, env: ScoringEnvironment, priority?): Promise<ScoreResult> {
    try {
      // console.log('XXXXXXXXXXXXXXXXXXXXXXXXX');
      // let prefixDir = `${globals.TI_UPLOAD_FOLDER}/${rec.id_test_instance}`;
      // let prefix = `${prefixDir}/${rec.ordinal}`;
      // let uploadedFiles = await minioService.getObjectsInBucket(
      //   globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
      //   prefix
      // );
      // console.log({ bucket: globals.MINIO_TMP_UPLOAD_BUCKET_NAME, prefix });
      // console.log(uploadedFiles);
      // console.log(rec);

      let corrCode = utils.mergeCode(rec.c_prefix, rec.c_source, rec.c_suffix);
      let studCode = utils.mergeCode(rec.c_prefix, rec.student_answer_code, rec.c_suffix);

      let tests = await ScorerService.getTestsForCodeQuestion(rec.c_question_answer_id);
      // console.log('TESTS:', JSON.stringify(tests, null, 2));
      let inputEvalData = this.getInputEvalData(tests);
      console.log('input eval data:', JSON.stringify(inputEvalData, null, 2));

      let res: ScoreResult = await this.execAndGradeCode(
        rec,
        studCode,
        corrCode,
        inputEvalData,
        priority || 100,
        env
      );

      res.record = rec;
      return res;
    } catch (error) {
      winston.error(error);
      return {
        score: Score.newEmptyScore('Error occured during grading...'),
        record: rec,
        data: 'Error occured:' + JSON.stringify(error),
        success: true,
      };
    }
  }

  private getCodeQuestionScore(evalConfig, studentResult /*, genPerc*/): Score {
    let score;
    if (!studentResult.status) {
      winston.error('studentResult.status is undefined. studentResult is:');
      winston.error(studentResult);
    }
    if (!studentResult.status || studentResult.status.id !== 14) {
      return Score.newInCorrectScore(this.gradingModel, utils.cScoreHint(studentResult));
    }
    var score_perc = 100; // we start with the assumption that everything is correct, and deduct
    var hints = [];
    var outputs = studentResult.results; //outputs [] : {id, isExecuted, data, input, output}

    //var corrOutputs = corrRes.results.outputs;
    //var passedTestsCount = 0;

    for (let i = 0; i < outputs.length; i++) {
      // console.log("THE getCQuestionScore.currConfig : ", evalConfig);
      // console.log("studentOutput : ", outputs[i]);
      //let currConfig = evalConfig.find(o => o.id == outputs[i].id);
      let currConfig = evalConfig[i]; // they are always in the same order
      if (outputs[i].status.id !== 3) {
        score_perc -= currConfig.percentage;
        hints.push({
          input: currConfig.input,
          percentage: currConfig.percentage,
          hint: `Test execution failed: ${outputs[i].status.description}`,
          output: utils.cScoreHint(outputs[i]),
          expected: currConfig.output,
          mode: utils.cModeToString(
            currConfig.diff_order,
            currConfig.case_insensitive,
            currConfig.trim_whitespace
          ),
          isCorrect: false, //failed
        });
      } else {
        var userOutput = outputs[i].stdout;
        var corrOutput = currConfig.output;
        // console.log('BEFORE trim whitespace');
        // console.log(userOutput, "#", Buffer.from(userOutput).toString('base64'));
        // console.log(corrOutput, "#", Buffer.from(corrOutput).toString('base64'));

        // Always normalize newline:
        if (userOutput) userOutput = userOutput.replace(new RegExp('\r\n', 'g'), '\n');
        if (corrOutput) corrOutput = corrOutput.replace(new RegExp('\r\n', 'g'), '\n');

        if (currConfig.trim_whitespace == true) {
          if (userOutput) userOutput = this.trimWhiteSpace(userOutput); // replace(/\s+/g, ' ')
          if (corrOutput) corrOutput = this.trimWhiteSpace(corrOutput); // replace(/\s+/g, ' ').
        }
        if (currConfig.case_insensitive == true) {
          if (userOutput) userOutput = userOutput.toLowerCase();
          if (corrOutput) corrOutput = corrOutput.toLowerCase();
        }

        if (currConfig.regex) {
          let inputstring = currConfig.regex;
          let flags;
          let pattern;
          if (inputstring.match(/.*\/([gimy]*)$/) !== null) {
            flags = inputstring.replace(/.*\/([gimy]*)$/, '$1');
            let re = new RegExp('/' + flags + '$');
            pattern = inputstring.replace(re, '').replace(/^[\/](.*?)$/, '$1');
          } else {
            flags = '';
            pattern = inputstring;
          }
          var regex = new RegExp(pattern, flags);

          var result = regex.test(userOutput);
          if (result == true) {
            //passedTestsCount += 1;
            // if (currConfig.percentage < 100){ //just gathering points
            //     points += parseFloat(currConfig.percentage);
            // }
            hints.push({
              input: currConfig.input,
              percentage: currConfig.percentage,
              hint: 'Correct. Well done!',
              expected: '(defined via regex, matched)',
              output: outputs[i].stdout, //outputs[i].data, //userOutput as it was
              mode: utils.cModeToString(
                currConfig.diff_order,
                currConfig.case_insensitive,
                currConfig.trim_whitespace
              ),
              isCorrect: true, //passed
            });
          } else {
            score_perc -= currConfig.percentage;
            hints.push({
              input: currConfig.input,
              percentage: currConfig.percentage,
              hint: 'Incorrect output',
              expected: 'to match regex: ' + currConfig.regex,
              output: outputs[i].stdout, //outputs[i].data, //userOutput as it was
              mode: utils.cModeToString(
                currConfig.diff_order,
                currConfig.case_insensitive,
                currConfig.trim_whitespace
              ),
              isCorrect: false, //failed
            });
          }
        } else {
          if (currConfig.diff_order == true) {
            // TODO: handle userOutput or corrOutput === null
            var userOutputList = userOutput.split(' ');
            var corrOutputList = corrOutput.split(' ');

            if (userOutputList.length != corrOutputList.length) {
              hints.push({
                input: currConfig.input,
                percentage: currConfig.percentage,
                hint: 'Different/incorrect element count',
                output: outputs[i].stdout,
                expected: currConfig.output,
                mode: utils.cModeToString(
                  currConfig.diff_order,
                  currConfig.case_insensitive,
                  currConfig.trim_whitespace
                ),
                isCorrect: false, //failed
              });
              score_perc -= currConfig.percentage;
            } else {
              let same = true;
              for (let i = 0; i < corrOutputList.length; i++) {
                if (userOutputList.indexOf(corrOutputList[i]) <= -1) {
                  same = false;
                  break;
                }
              }
              if (same == false) {
                hints.push({
                  input: currConfig.input,
                  percentage: currConfig.percentage,
                  hint: 'Incorrect output',
                  output: outputs[i].stdout, //outputs[i].data, //userOutput as it was
                  expected: currConfig.output,
                  mode: utils.cModeToString(
                    currConfig.diff_order,
                    currConfig.case_insensitive,
                    currConfig.trim_whitespace
                  ),
                  isCorrect: false, //failed
                });
                score_perc -= currConfig.percentage;
              } else {
                //passedTestsCount += 1;
                hints.push({
                  input: currConfig.input,
                  percentage: currConfig.percentage,
                  hint: 'Correct. Well done!',
                  output: outputs[i].stdout,
                  expected: currConfig.output,
                  mode: utils.cModeToString(
                    currConfig.diff_order,
                    currConfig.case_insensitive,
                    currConfig.trim_whitespace
                  ),
                  isCorrect: true, //passed
                });
              }
            }
          } else {
            // console.log(JSON.stringify(userOutput));
            // console.log(JSON.stringify(corrOutput));
            // console.log('userOutput === corrOutput', userOutput === corrOutput);
            if (userOutput === corrOutput) {
              //passedTestsCount += 1;
              hints.push({
                input: currConfig.input,
                percentage: currConfig.percentage,
                hint: 'Correct. Well done!',
                output: outputs[i].stdout,
                expected: currConfig.output,
                mode: utils.cModeToString(
                  currConfig.diff_order,
                  currConfig.case_insensitive,
                  currConfig.trim_whitespace
                ),
                isCorrect: true, //passed
              });
            } else {
              score_perc -= currConfig.percentage;
              hints.push({
                input: currConfig.input,
                percentage: currConfig.percentage,
                hint: 'Incorrect output',
                output: outputs[i].stdout,
                expected: currConfig.output,
                mode: utils.cModeToString(
                  currConfig.diff_order,
                  currConfig.case_insensitive,
                  currConfig.trim_whitespace
                ),
                isCorrect: false, //failed
              });
            }
          }
        }
      }
      if (hints.length) {
        hints[hints.length - 1].comment = currConfig.comment;
        hints[hints.length - 1].stderr = outputs[i].stderr;
        hints[hints.length - 1].is_public = currConfig.is_public;
      }
    }
    score_perc = score_perc < 0 ? 0 : score_perc;
    if (score_perc === 0) {
      score = Score.newInCorrectScore(this.gradingModel, 'Failed on all or major tests.');
    } else if (score_perc == 100) {
      score = Score.newCorrectScore(this.gradingModel);
    } else {
      score = {
        is_correct: false,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: true,
        score:
          (score_perc / 100) *
            (this.gradingModel.correct_score - this.gradingModel.incorrect_score) +
          this.gradingModel.incorrect_score,
        score_perc: score_perc / 100,
        hint: 'Partial score',
      };
    }
    score.c_outcome = hints;
    return score;
  }

  private async execAndGradeCode(
    rec,
    userC,
    correctC,
    inputEvalData: { inputs: any[]; evalConfigs: any[] },
    // id_course,
    // id_pl_stud,
    // id_pl_teacher,
    priority,
    env: ScoringEnvironment
  ): Promise<ScoreResult> {
    if (!inputEvalData.inputs || !inputEvalData.inputs.length) {
      return ScoreResult.newUnansweredScoreResult(
        this.gradingModel,
        "No tests for question. Teacher's error?"
      );
    }

    if (!userC || userC.trim() === '') {
      return ScoreResult.newUnansweredScoreResult(this.gradingModel, 'Empty answer.');
    }

    if (!rec.student_answer_code_pl) {
      return ScoreResult.newUnansweredScoreResult(
        this.gradingModel,
        "Unknown programming language for student's code."
      );
    }
    // let inputsStudent = [];
    // let inputsTeacher = [];
    // let evalConfig = [];
    const inputsTeacher = inputEvalData.inputs
      .filter((x) => x.c_test_type_id != GENERATOR_TYPE.fixed)
      .slice(0); // Teacher will executore only non-fixed tests, if there are any

    // if (inputEvalData.inputs && inputEvalData.inputs.length) {
    //   inputsTeacher = inputEvalData.inputs.slice(0);
    //   inputsStudent = inputEvalData.inputs.slice(0);
    //   evalConfig = inputEvalData.evalConfigs.slice(0);
    // }
    // if (inputEvalData.inputsFixed && inputEvalData.inputsFixed.length) {
    //   for (let i = 0; i < inputEvalData.inputsFixed.length; i++) {
    //     inputsStudent.push(inputEvalData.inputsFixed[i]);
    //     // Zašto ovdje nije dodano i u nastavnika?  Ja dodajem:
    //     // Odgovor: zato jer vec ima fiksni izlaz, nije ga potrebno izvrsavati za profesora (dodatno mozda ni nema profesorovog koda)
    //     evalConfig.push(inputEvalData.evalConfigsFixed[i]);
    //   }
    // }
    // console.log("\n\n\inputEvalData : ", inputEvalData);
    // console.log("\n\n\ninputsStudent : ", inputsStudent);
    // console.log("\n\n\ninputsTeacher : ", inputsTeacher);
    // console.log("\n\n\nevalConfig : ", evalConfig);
    let resultStudent, resultTeacher;
    if (inputsTeacher && inputsTeacher.length) {
      resultTeacher = await CodeRunnerService.execCodeQuestionWithInputs(
        rec.id_question,
        correctC,
        inputsTeacher,
        rec.id_course,
        rec.id_programming_language,
        priority,
        env
      );
    }
    resultStudent = await CodeRunnerService.execCodeQuestionWithInputs(
      rec.id_question,
      userC,
      inputEvalData.inputs,
      rec.id_course,
      rec.student_answer_code_pl,
      priority,
      env
    );

    //var studentsResult = (inputsTeacher && inputsTeacher.length) ? results[1] : results[0];
    //var allGood = true;
    if (!resultStudent || !resultStudent.status) {
      return ScoreResult.newEmptyScoreResult('Service unavailable.');
    } else if (
      resultStudent.status.id === 100 ||
      (resultTeacher && resultTeacher.status.id === 100)
    ) {
      return ScoreResult.newEmptyScoreResult('Pushback (service unavailable).');
      // It is possible that only one of (one or two http requests) ended up with 503
      // so I will set it to either one (see below score.j0 = studentsResult):
      //studentsResult = (results[0].status.id == 100) ? results[0] : results[1];
    }

    if (inputsTeacher && inputsTeacher.length) {
      //var teacherResult = results[0];
      //console.log("teacherResult : ", teacherResult);
      // const randIds = inputEvalData.inputs.map((i: any) => i.id);
      const randGenEvalConfig = inputEvalData.evalConfigs.filter(
        (cfg: any) => cfg.c_test_type_id != GENERATOR_TYPE.fixed
      );
      if (resultTeacher.status.id !== 14) {
        return ScoreResult.newEmptyScoreResult("Question has a wrong answer (teacher's error!)!!");
      } else {
        //outputs [] : {id, isExecuted, data, input, output}
        const corrOutputs = resultTeacher.results;
        for (let i = 0; i < corrOutputs.length; i++) {
          if (corrOutputs[i].status.id !== 3) {
            return ScoreResult.newEmptyScoreResult(
              "Question has a wrong answer (teacher's error!)!!"
            );
          } else {
            randGenEvalConfig[i].output = corrOutputs[i].stdout;
            //u eval configu je definirano da se tocni izlazi zovu 'output'
            //tu se vadi samo stdout od profesora jer su status i moguci errori provjereni ovdje prije ocjenjivanja za studenta
            //dodatno : posto vise nema 'id' poredak dodavanja konfiguracija u evalConfig se ne smije mijenjati jer bude se krivo pospajalo (input <-> output)
            //evalConfig.find(o => o.id == corrOutputs[i].id).output = corrOutputs[i];  // .output
          }
        }
      }
    }
    const score = this.getCodeQuestionScore(
      inputEvalData.evalConfigs,
      resultStudent /*, privateinputEvalData.generatorPercentage*/
    );
    // console.log("--------------------------------------------\n");
    // console.log("score: ", score);
    // console.log("--------------------------------------------\n");
    //console.log("score.hint: ", score.hint);
    //console.log("--------------------------------------------\n");
    /*
        if(results[0].success !== undefined && results[1].success !== undefined)
          score.success = results[0].success && results[1].success;
        else if (results[0].success !== undefined || results[1].success !== undefined)
          score.success = results[0].success !== undefined ? results[0].success : results[1].success;
        else
          score.success = false;

        if(score.success !== true){
          let mssg = results[0].error ? results[0].error.message + ' ' : '';
          mssg += results[1].error ? results[1].error.message  + ' ' : '';

          score.error = {
            message : mssg.length > 0 ? mssg : "An error occured (more info @server)."
          };
        }
        */
    //winston.info("CodeQuestionScorer returngin SCORE");
    // console.log('XXXX', {
    //   score,
    //   outcome: JSON.stringify(score),
    // });
    return {
      score,
      j0: resultStudent,
      record: rec,
    };
    //winston.info('score for ord', ordinal, score);
  }
  private getInputEvalData(testCases: ITestCaseRecord[]): { inputs: any[]; evalConfigs: any[] } {
    //for random and generator - run for teacher and student
    //  must generate inputs first
    // const arrRandomNumbers = [];
    // const evalGenConfigs: any[] = [];

    //fixed - run for student only (has fixed output)
    // const inputsFixed = [];
    // const evalConfigsFixed = [];

    //fixed without output - run for teacher and student
    const inputs = [];
    const evalConfigs = [];

    // TODO create new array in which you will put test id -> rtc

    // let generator_percentage = 0;
    //let percentage_without_elimin = 0.0;

    for (let i = 0; i < testCases.length; i++) {
      if (testCases[i].c_test_type_id == GENERATOR_TYPE.fixed) {
        if (testCases[i].output || !testCases[i].regex_override) {
          inputs.push({
            id: testCases[i].id,
            ordinal: testCases[i].test_case_ordinal,
            input: testCases[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
            c_test_type_id: testCases[i].c_test_type_id,
          });

          evalConfigs.push({
            id: testCases[i].id,
            ordinal: testCases[i].test_case_ordinal,
            input: testCases[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
            output: testCases[i].output,
            percentage: testCases[i].percentage,
            diff_order: testCases[i].allow_diff_order,
            case_insensitive: testCases[i].allow_diff_letter_size,
            trim_whitespace: testCases[i].trim_whitespace,
            regex: testCases[i].regex_override,
            comment: testCases[i].comment,
            is_public: testCases[i].is_public,
            c_test_type_id: testCases[i].c_test_type_id,
          });
        } else if (testCases[i].cSource && testCases[i].cSource.trim().length > 0) {
          inputs.push({
            id: testCases[i].id,
            ordinal: testCases[i].test_case_ordinal,
            input: testCases[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
            c_test_type_id: testCases[i].c_test_type_id,
          });

          evalConfigs.push({
            id: testCases[i].id,
            ordinal: testCases[i].test_case_ordinal,
            input: testCases[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
            percentage: testCases[i].percentage,
            diff_order: testCases[i].allow_diff_order,
            case_insensitive: testCases[i].allow_diff_letter_size,
            trim_whitespace: testCases[i].trim_whitespace,
            regex: testCases[i].regex_override,
            comment: testCases[i].comment,
            is_public: testCases[i].is_public,
            c_test_type_id: testCases[i].c_test_type_id,
          });
        } else {
          winston.warn('Neither source nor output found while grading for fixed input');
          throw 'Neither source nor output found while grading for fixed input';
        }
      } else if (testCases[i].c_test_type_id == GENERATOR_TYPE.random) {
        //if(tests[i].percentage < 100) percentage_without_elimin += Number.parseFloat(tests[i].percentage);
        const randomInput = RandomInputGenerator.randomGenerator(
          testCases[i].random_test_type_id,
          testCases[i].low_bound,
          testCases[i].upper_bound,
          testCases[i].elem_count
        );
        inputs.push({
          id: testCases[i].id,
          ordinal: testCases[i].test_case_ordinal,
          input: randomInput.inputs,
          c_test_type_id: testCases[i].c_test_type_id,
        });
        evalConfigs.push({
          id: testCases[i].id,
          ordinal: testCases[i].test_case_ordinal,
          input: randomInput.inputs,
          percentage: testCases[i].percentage,
          diff_order: testCases[i].allow_diff_order,
          case_insensitive: testCases[i].allow_diff_letter_size,
          trim_whitespace: testCases[i].trim_whitespace,
          regex: testCases[i].regex_override,
          comment: testCases[i].comment,
          is_public: testCases[i].is_public,
          c_test_type_id: testCases[i].c_test_type_id,
        });
      } else {
        // if (testCases[i].percentage < 100) generator_percentage += testCases[i].percentage;
        winston.warn('generator not implemented');
        throw 'generator not implemented - unknown test cast type?';
      }
    }

    return {
      inputs: inputs,
      evalConfigs: evalConfigs,
    };
  }
  // private getInputEvalData(tests) {
  //   //for random and generator - run for teacher and student
  //   //  must generate inputs first
  //   let arrRandomNumbers = [];
  //   var evalGenConfigs = [];

  //   //fixed - run for student only (has fixed output)
  //   let inputsFixed = [];
  //   var evalConfigsFixed = [];

  //   //fixed without output - run for teacher and student
  //   let inputs = [];
  //   var evalConfigs = [];

  //   // TODO create new array in which you will put test id -> rtc

  //   let generator_percentage = 0;
  //   //let percentage_without_elimin = 0.0;

  //   for (let i = 0; i < tests.length; i++) {
  //     if (tests[i].c_test_type_id == GENERATOR_TYPE.fixed) {
  //       //if(tests[i].percentage < 100) percentage_without_elimin += Number.parseFloat(tests[i].percentage);

  //       if (tests[i].output || !tests[i].regex_override) {
  //         inputsFixed.push({
  //           id: tests[i].id,
  //           input: tests[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
  //         });

  //         evalConfigsFixed.push({
  //           id: tests[i].id,
  //           input: tests[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
  //           output: tests[i].output,
  //           percentage: tests[i].percentage,
  //           diff_order: tests[i].allow_diff_order,
  //           case_insensitive: tests[i].allow_diff_letter_size,
  //           trim_whitespace: tests[i].trim_whitespace,
  //           regex: tests[i].regex_override,
  //           comment: tests[i].comment,
  //           is_public: tests[i].is_public,
  //         });
  //       } else if (tests[i].c_source && tests[i].c_source.trim().length > 0) {
  //         inputs.push({
  //           id: tests[i].id,
  //           input: tests[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
  //         });

  //         evalConfigs.push({
  //           id: tests[i].id,
  //           input: tests[i].input.replace(new RegExp('\r\n', 'g'), '\n'),
  //           percentage: tests[i].percentage,
  //           diff_order: tests[i].allow_diff_order,
  //           case_insensitive: tests[i].allow_diff_letter_size,
  //           trim_whitespace: tests[i].trim_whitespace,
  //           regex: tests[i].regex_override,
  //           comment: tests[i].comment,
  //           is_public: tests[i].is_public,
  //         });
  //       } else {
  //         winston.warn('Neither source nor output found while grading for fixed input');
  //         throw 'Neither source nor output found while grading for fixed input';
  //       }
  //     } else if (tests[i].c_test_type_id == GENERATOR_TYPE.random) {
  //       //if(tests[i].percentage < 100) percentage_without_elimin += Number.parseFloat(tests[i].percentage);
  //       arrRandomNumbers.push(
  //         RandomInputGenerator.randomGenerator(
  //           parseInt(tests[i].random_test_type_id),
  //           Number.parseFloat(tests[i].low_bound),
  //           Number.parseFloat(tests[i].upper_bound),
  //           Number.parseInt(tests[i].elem_count)
  //         )
  //       );
  //       evalGenConfigs.push({
  //         id: tests[i].id,
  //         percentage: tests[i].percentage,
  //         diff_order: tests[i].allow_diff_order,
  //         case_insensitive: tests[i].allow_diff_letter_size,
  //         trim_whitespace: tests[i].trim_whitespace,
  //         regex: tests[i].regex_override,
  //         comment: tests[i].comment,
  //         is_public: tests[i].is_public,
  //       });
  //     } else {
  //       if (tests[i].percentage < 100)
  //         generator_percentage += Number.parseFloat(tests[i].percentage);
  //       winston.warn('generator not implemented');
  //     }
  //   }
  //   for (let i = 0; i < arrRandomNumbers.length; i++) {
  //     inputs.push({
  //       id: evalGenConfigs[i].id,
  //       input: arrRandomNumbers[i].inputs,
  //     });
  //     evalGenConfigs[i].input = arrRandomNumbers[i].inputs;
  //     evalConfigs.push(evalGenConfigs[i]);
  //   }

  //   return {
  //     inputs: inputs,
  //     evalConfigs: evalConfigs,
  //     inputsFixed: inputsFixed,
  //     evalConfigsFixed: evalConfigsFixed,
  //     generatorPercentage: generator_percentage,
  //   };
  // }
}
