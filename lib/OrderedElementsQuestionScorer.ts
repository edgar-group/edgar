let winston = require('winston');
import { IQuestionScorer } from './IQuestionScorer';
import Score from './Score';
import { ScoreResult } from './Score';
import AbstractQuestionScorer from './AbstractQuestionScorer';
export default class OrderedElementsQuestionScorer extends AbstractQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    super();
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default OrderedElements Question Scorer';
  }

  getScore(record): Promise<ScoreResult> {
    try {
      let { student_answers, correct_answers_permutation, correctness } = record;
      let scoreResult: ScoreResult = {
        score: null,
        record: record,
      };
      if (student_answers === null || student_answers.length === 0) {
        scoreResult.score = Score.newUnansweredScore(this.gradingModel);
        return Promise.resolve(scoreResult);
      }

      let penalty = 0;

      let unPermutedArray = [];
      for (let i = 0; i < student_answers.length; i++) {
        unPermutedArray.push(
          correct_answers_permutation.findIndex((ordinal) => ordinal == student_answers[i]) + 1
        );
      }

      const longestSubseqLength = this.longestIncreasingSubsequence(unPermutedArray);
      const wrongOrderElementSteps = student_answers.length - longestSubseqLength;

      // eg correctnes = {90,70}   bcs leading 100 is implicit
      correctness.unshift(100);
      // {100, 90,70}
      penalty =
        wrongOrderElementSteps >= correctness.length
          ? 100
          : 100 - correctness[wrongOrderElementSteps];

      scoreResult.score = this.getScoreFromPenalty(penalty, this.gradingModel);
      if (penalty > 0)
        scoreResult.score.hint = `Partial score: penalty: ${penalty}%, longestSubseqLength: ${longestSubseqLength}, wrongOrderElementSteps: ${wrongOrderElementSteps}`;
      return Promise.resolve(scoreResult);

    } catch (error) {
      winston.error(error);
      return Promise.resolve({
        score: Score.newEmptyScore('Error occured during grading...'),
        record: record,
        success: true,
      });
    }
  }

  longestIncreasingSubsequence(arr: number[]): number {
    const n = arr.length;
    const lis = new Array(n).fill(1);

    for (let i = 1; i < n; i++) {
      for (let j = 0; j < i; j++) {
        if (arr[i] > arr[j] && lis[i] < lis[j] + 1) {
          lis[i] = lis[j] + 1;
        }
      }
    }

    return Math.max(...lis);
  }
}
