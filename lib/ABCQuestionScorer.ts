let winston = require('winston');
import { IQuestionScorer } from './IQuestionScorer';
import Score from './Score';
import { ScoreResult } from './Score';
export default class ABCQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Default ABC Question Scorer';
  }

  getScore(record): Promise<ScoreResult> {
    try {
      let { student_answers, correct_answers_permutation, penalty_percentage_permutation } = record;
      let scoreResult: ScoreResult = {
        score: null,
        record: record,
      };
      if (student_answers === null || student_answers.length === 0) {
        scoreResult.score = Score.newUnansweredScore(this.gradingModel);
        return Promise.resolve(scoreResult);
      }
      if (!correct_answers_permutation) {
        // this is for Scale questions, they have no notion of correct answer
        scoreResult.score = Score.newCorrectScore(this.gradingModel, '');
        return Promise.resolve(scoreResult);
      }
      let penalty = 0;
      for (let i = 1; i <= penalty_percentage_permutation.length; ++i) {
        let isCorrect = correct_answers_permutation.indexOf(i) >= 0;
        let studIsCorrect = student_answers.indexOf(i) >= 0;
        penalty += penalty_percentage_permutation[i - 1] * +(isCorrect !== studIsCorrect);
      }
      if (penalty > 100) penalty = 100;
      if (penalty < 0) penalty = 0;
      let score =
        this.gradingModel.correct_score -
        (penalty * (this.gradingModel.correct_score - this.gradingModel.incorrect_score)) / 100;
      scoreResult.score = {
        is_correct: penalty === 0,
        is_incorrect: penalty === 100,
        is_unanswered: false,
        is_partial: penalty > 0 && penalty < 100,
        score: score,
        score_perc:
          this.gradingModel.correct_score !== 0 ? score / this.gradingModel.correct_score : 0,
      };
      return Promise.resolve(scoreResult);
    } catch (error) {
      winston.error(error);
      return Promise.resolve({
        score: Score.newEmptyScore('Error occured during grading...'),
        record: record,
        success: true,
      });
    }
  }
}
