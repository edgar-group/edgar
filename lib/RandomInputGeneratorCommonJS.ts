import RandomInputGenerator from './RandomInputGenerator';
export = class RandomInputGeneratorCommonJS {
  public static printableChars(upTo?) {
    return RandomInputGenerator.printableChars(upTo);
  }

  public static randomText(howMany, chars) {
    return RandomInputGenerator.randomText(howMany, chars);
  }

  public static randomNumbers(type, low, high, count, precision?) {
    return RandomInputGenerator.randomNumbers(type, low, high, count, precision);
  }

  public static randomFloat(low, high) {
    return RandomInputGenerator.randomFloat(low, high);
  }

  public static randomInt(low, high) {
    return RandomInputGenerator.randomInt(low, high);
  }

  public static randomIntInclusive(low, high) {
    return RandomInputGenerator.randomIntInclusive(low, high);
  }

  public static randomGenerator(type, lowBound, upperBound, count, precision?) {
    return RandomInputGenerator.randomGenerator(type, lowBound, upperBound, count);
  }

  public static randomNumbersGenerator(type, lowBound, upperBound, count, precision) {
    return RandomInputGenerator.randomNumbersGenerator(
      type,
      lowBound,
      upperBound,
      count,
      precision
    );
  }
  public static randomIntNumbersGenerator(lowBound, upperBound, count) {
    return RandomInputGenerator.randomIntNumbersGenerator(lowBound, upperBound, count);
  }

  public static randomFloatNumbersGenerator(lowBound, upperBound, count, precision) {
    return RandomInputGenerator.randomFloatNumbersGenerator(lowBound, upperBound, count, precision);
  }

  public static randomTextGenerator(type, count, lowBound, upperBound) {
    return RandomInputGenerator.randomTextGenerator(type, count, lowBound, upperBound);
  }
};
