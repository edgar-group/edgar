import { ScoreResult } from './Score';
export enum ScoringEnvironment {
  Runtime = 'Runtime',
  Submitted = 'Submitted',
  Irrelevant = 'Irrelevant',
  Tutorial = 'Tutorial',
}
export interface IQuestionScorerConstructor {
  new (gradingModel: GradingModel): IQuestionScorer;
}
export interface IQuestionScorer {
  getName(): string;
  getScore(args: any, env: ScoringEnvironment, priority?: number): Promise<ScoreResult>;
}
