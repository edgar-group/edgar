let winston = require('winston');
import { IQuestionScorer, ScoringEnvironment } from './IQuestionScorer';
import Score, { QuestionScoringMode } from './Score';
import { ScoreResult } from './Score';
import CodeRunnerService from './CodeRunnerService';
const utils = require('./../common/utils');
const minioService = require('./../services/minioService');
var globals = require('../common/globals');

export interface IComplexQuestionTestCase {
  run_script: string;
  eval_script: string;
  comment: string;
  is_public: boolean;
  expected: string;
  percentage: number;
}

export default class ComplexQuestionScorer implements IQuestionScorer {
  private gradingModel: GradingModel;

  constructor(model: GradingModel) {
    this.gradingModel = model;
  }

  getName(): string {
    return 'Complex Question Scorer';
  }

  async getScore(record, env: ScoringEnvironment, priority?): Promise<ScoreResult> {
    try {
      let j0;
      if (utils.useMinio()) {
        let uploadedFiles;
        if (record.ts_submitted) {
          uploadedFiles = [
            {
              bucketName: globals.PUBLIC_TI_DOWNLOAD_FOLDER,
              filename: record.uploaded_files,
              original_name: undefined, // this is a zip - it'll be extracted...
            },
          ];
        } else {
          let prefixDir = `${globals.TI_UPLOAD_FOLDER}/${record.id_test_instance}`;
          let prefix = `${prefixDir}/${record.ordinal}`;

          uploadedFiles = await minioService.getObjectsInBucket(
            globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
            prefix
          );
          uploadedFiles = uploadedFiles.map((obj) => {
            return {
              bucketName: globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
              filename: obj.name,
              original_name: obj.name.split('/').pop(),
            };
          });
        }

        // console.table(uploadedFiles);

        // console.log('args:', {
        //   id_question: record.id_question,
        //   setup_script: record.setup_script,
        //   test_cases: record.test_cases,
        //   uploadedFiles,
        //   id_course: record.id_course,
        //   id_programming_language: record.id_programming_language,
        //   priority: priority,
        //   env,
        // });
        if (uploadedFiles) {
          j0 = await CodeRunnerService.execComplexQuestion(
            record.id_question,
            record.setup_script,
            record.text_answer,
            record.test_cases,
            uploadedFiles.map((x) => x.filename),
            record.id_course,
            record.id_programming_language,
            priority,
            env
          );
        } else {
          return {
            score: Score.newInCorrectScore(this.gradingModel, 'Missing files (upload) to grade.'),
            record: record,
          };
        }
      } else {
        winston.error(
          'Complex question scorer with file system storage (upload) not implemented yet.'
        );
        return {
          score: Score.newUnansweredScore(
            this.gradingModel,
            'Complex question scorer with file system storage (upload) not implemented yet.'
          ),
          record: record,
        };
      }

      let scoreResult: ScoreResult = {
        score: this.getComplexQuestionScore(record.test_cases, j0),
        record: record,
        j0,
      };

      return scoreResult;
    } catch (error) {
      winston.error(error);
      return {
        score: Score.newEmptyScore('Error occured during grading...'),
        record: record,
        success: true,
      };
    }
  }

  private getComplexQuestionScore(testCases, studentResult): Score {
    if (!studentResult.status) {
      winston.error('studentResult.status is undefined. studentResult is:');
      winston.error(studentResult);
    }
    if (!studentResult.status || studentResult.status.id !== 14) {
      return Score.newInCorrectScore(this.gradingModel, utils.cScoreHint(studentResult));
    }
    var hints = [];
    var outputs = studentResult.results; //outputs [] : {id, isExecuted, data, input, output}
    let score_perc = 100;
    let mode: QuestionScoringMode;
    if (testCases.filter((x) => x.expected === '<<deduct>>').length == testCases.length) {
      mode = QuestionScoringMode.Deduct;
    } else if (testCases.filter((x) => x.expected === '<<add>>').length == testCases.length) {
      mode = QuestionScoringMode.Add;
      score_perc = 0;
    } else {
      mode = QuestionScoringMode.Compare;
    }
    for (let i = 0; i < outputs.length; i++) {
      let testCaseConfig: IComplexQuestionTestCase = testCases[i]; // they are always in the same order
      if (outputs[i].status.id !== 3) {
        hints.push({
          input: `Run script: ${testCaseConfig.run_script} Eval script: ${testCaseConfig.eval_script}`,
          percentage: testCaseConfig.percentage,
          hint: `Test execution failed: ${outputs[i].status.description}`,
          output: outputs[i].stdout,
          isCorrect: false,
          stderr: outputs[i].stderr,
          expected: testCaseConfig.expected,
        });
        score_perc = 0;
      } else {
        let isCorrect = false;
        let hint = 'Correct.';
        if (mode === QuestionScoringMode.Deduct) {
          if (parseFloat(outputs[i].stdout) >= 0) {
            score_perc -= parseFloat(outputs[i].stdout);
            isCorrect = parseFloat(outputs[i].stdout) === 0.0;
            hint = isCorrect
              ? 'Correct.'
              : 'Incorrect, deducted ' + parseFloat(outputs[i].stdout) + '%';
          } else {
            score_perc = 0; // if the teacher's script failed, we'll grade the submission as 0, that way  the student will complain
            hint =
              'Teacher script failed to produce parsable percentage to deduct (output = ' +
              outputs[i].stdout +
              '), fallback to incorrect.';
          }
        } else if (mode === QuestionScoringMode.Add) {
          if (parseFloat(outputs[i].stdout) >= 0) {
            score_perc += parseFloat(outputs[i].stdout);
            isCorrect = parseFloat(outputs[i].stdout) > 0.0;
            hint = 'Correct, or partially correct: added ' + parseFloat(outputs[i].stdout) + '%';
          } else {
            score_perc = -1e6; // if the teacher's script failed, we'll grade the submission as 0, that way  the student will complain
            hint =
              'Teacher script failed to produce parsable percentage to add (output = ' +
              outputs[i].stdout +
              '), fallback to incorrect.';
          }
        } else if (mode === QuestionScoringMode.Compare && testCaseConfig.expected) {
          if (
            testCaseConfig.expected.toLowerCase().trim() !==
            (outputs[i].stdout || '').toLowerCase().trim()
          ) {
            score_perc -= testCaseConfig.percentage;
            hint = 'Output not matching expected output.';
          } else {
            isCorrect = true;
          }
        } else {
          // Compare mode but no expected output - give it a pass
          isCorrect = true;
        }

        hints.push({
          input: `Run script: ${testCaseConfig.run_script} Eval script: ${testCaseConfig.eval_script}`,
          percentage: testCaseConfig.percentage,
          hint,
          output: outputs[i].stdout,
          isCorrect,
          stderr: outputs[i].stderr,
          comment: testCaseConfig.comment,
          is_public: testCaseConfig.is_public,
          expected: testCaseConfig.expected,
        });
      }
    }
    let score: Score;
    score_perc = score_perc < 0 ? 0 : score_perc;
    score_perc = score_perc > 100 ? 100 : score_perc;
    if (score_perc === 0) {
      score = Score.newInCorrectScore(this.gradingModel, 'Failed on all or major tests.');
    } else if (score_perc == 100) {
      score = Score.newCorrectScore(this.gradingModel);
    } else {
      score = {
        is_correct: false,
        is_incorrect: false,
        is_unanswered: false,
        is_partial: true,
        score:
          (score_perc / 100) *
            (this.gradingModel.correct_score - this.gradingModel.incorrect_score) +
          this.gradingModel.incorrect_score,
        score_perc: score_perc / 100,
        hint: 'Partial score',
        c_outcome: hints,
        scoring_mode: mode,
      };
    }
    score.c_outcome = hints;
    // console.log(JSON.stringify(score, null, 4));
    return score;
  }
}
