import { ScoringEnvironment } from './IQuestionScorer';

var db = require('../db').db;
var winston = require('winston');
var request = require('request');
var globals = require('../common/globals');
var AdmZip = require('adm-zip');
const utils = require('./../common/utils');
const minioService = require('../services/minioService');
const BASE64_ENCODE_CODE_SUBMISSIONS = true;

export default class CodeRunnerService {
  private static async submitToJudge0CodeRunner(
    host,
    path,
    port,
    code,
    text_answer,
    inputs,
    rtc_question,
    rtc_tests,
    j0_id,
    compiler_options,
    files,
    priority,
    env: ScoringEnvironment
  ) {
    if (code.length > 500000) {
      winston.error('Request too large (max is 500,000), code length is: ' + code.length);
      winston.error('First 20,000 chars (max is 500,000):' + code.substring(0, 19999));
      return 'Request too large (max is 500,000), code length is: ' + code.length;
    } else {
      // host = 'localhost';
      // port = '10084';

      // console.log('submitToJudge0CodeRunner', {
      //   host,
      //   path,
      //   port,
      //   code,
      //   inputs,
      //   rtc_question,
      //   rtc_tests,
      //   j0_id,
      //   compiler_options,
      //   files,
      //   priority,
      // });
      // here you should query for all the constraints
      if (BASE64_ENCODE_CODE_SUBMISSIONS) {
        code = Buffer.from(code).toString('base64');
        if (text_answer) text_answer = Buffer.from(text_answer).toString('base64');
        if (inputs && inputs.length) {
          let b64inputs = [];
          inputs.forEach((element) => {
            if (element.input) {
              b64inputs.push({
                id: element.id,
                input: Buffer.from(element.input).toString('base64'),
              });
            } else if (element.run_script) {
              // complex questions
              b64inputs.push({
                id: element.id,
                run_script: Buffer.from(element.run_script).toString('base64'),
                eval_script: Buffer.from(element.eval_script).toString('base64'),
              });
            }
          });
          inputs = b64inputs;
        }
      }
      let additional_files;
      let additional_files_list = undefined;
      if (files && files.length) {
        if (utils.useMinio()) {
          winston.debug('Zipping files...', globals.getQuestionsAttachmentsBucketName());
          // console.table(files);
          let rv = await minioService.zipObjects(
            files.map((x) => x.bucketName || globals.getQuestionsAttachmentsBucketName()),
            files.map((x) => x.filename),
            files.map((x) => x.original_name)
          );
          additional_files = rv.base64Zip;
          additional_files_list = rv.filenames; // zipObjects will exapond/unzip zip files that do not have original_name, and thus one entry yields multiple files(names)
        } else {
          var zip = new AdmZip();
          files.forEach((file) => {
            winston.info(`Zipping: ${globals.getQuestionsAttachmentsFolder()}/${file.filename}`);
            zip.addLocalFile(
              `${globals.getQuestionsAttachmentsFolder()}/${file.filename}`,
              '',
              file.original_name.trim().split(new RegExp('\\s+')).join('_')
            );
          });
          additional_files = zip.toBuffer().toString('base64');
          additional_files_list = files.map((x) => x.original_name);
        }
      }
      if (BASE64_ENCODE_CODE_SUBMISSIONS && additional_files_list && additional_files_list.length) {
        additional_files_list = additional_files_list.map((x) => Buffer.from(x).toString('base64'));
      }
      var options = {
        url: port == '443' ? `https://${host}${path}` : `http://${host}:${port}${path}`,
        json: true,
        body: {
          source: code,
          text_answer: text_answer,
          language_id: j0_id,
          compiler_options,
          additional_files,
          additional_files_list,
          inputs: inputs,
          runtime_constraints_question: rtc_question,
          runtime_constraints_tests: rtc_tests,
          priority: priority >= 0 ? priority : 1e6,
          env,
        },
        headers: {
          'User-Agent': 'request',
        },
        timeout: 10 * 60000,
      };
      // winston.debug('submitToJudge0CodeRunner body');
      // winston.debug(JSON.stringify(options));

      return new Promise((resolve, reject) => {
        var callb = function (error, response, body) {
          if (!error) {
            if (body && body.error) {
              // nothing much to do here, will be handled later, eg pushback will end up here
            } else if (BASE64_ENCODE_CODE_SUBMISSIONS && body.results && body.results.length) {
              body.results.forEach((element) => {
                if (element.stdout)
                  element.stdout = Buffer.from(element.stdout, 'base64').toString();
                if (element.stderr)
                  element.stderr = Buffer.from(element.stderr, 'base64').toString();
                if (element.compile_output)
                  element.compile_output = Buffer.from(element.compile_output, 'base64').toString();
                if (element.message)
                  element.message = Buffer.from(element.message, 'base64').toString();
              });
            }
            winston.debug(JSON.stringify(body, null, 2));
            resolve(body);
          } else {
            winston.error(error);
            reject(error);
          }
        };
        request.post(options, callb);
      });
    }
  }
  private static async submitToDatabase(
    host: string,
    path: string,
    port: string,
    dbSchema: string,
    timeout: string,
    code: string,
    propertyName: string
  ) {
    let bodyArg = {
      db_schema: dbSchema,
      timeout,
    };
    bodyArg[propertyName] = code;
    var options = {
      url: port == '443' ? `https://${host}${path}` : `http://${host}:${port}${path}`,
      json: true,
      body: bodyArg,
      headers: {
        'User-Agent': 'request',
        //'Content-Type': 'application/json',
        //'Content-Length': Buffer.byteLength(data)
      },
      timeout: 1200000, // runner will kill it sooner, so I'll just wait here...
      pool: {
        maxSockets: Infinity,
      },
    };

    return new Promise(function (resolve, reject) {
      winston.debug('POSTing: ' + JSON.stringify(options));
      request.post(options, function (error, res, body) {
        if (!error && res.statusCode == 200) {
          //
          resolve(body);
        } else {
          winston.error(error);
          reject(error);
        }
      });
    });
  }

  private static async _execSQLCodeCodeRunner(
    SQL: string,
    args: any,
    code: string,
    append_prefix_suffix: boolean,
    append_presentation_query: boolean
  ) {
    let cr = await db.one(SQL, args);
    if (append_prefix_suffix) {
      code = utils.mergeSQLCode(cr.sql_test_fixture, code, cr.sql_alt_assertion);
    }
    if (append_presentation_query) {
      code = utils.mergeSQLCode(undefined, code, cr.sql_alt_presentation_query);
    }
    return await this.submitToDatabase(
      cr.host,
      cr.path,
      cr.port,
      cr.db_schema,
      cr.timeout,
      code,
      'sql'
    );
  }
  private static async _execJSCodeCodeRunner(
    SQL: string,
    args: any,
    code: string,
    append_prefix_suffix: boolean,
    append_presentation_query: boolean
  ) {
    let cr = await db.one(SQL, args);
    if (append_prefix_suffix) {
      code = utils.mergeSQLCode(cr.json_test_fixture, code, cr.json_alt_assertion);
    }
    if (append_presentation_query) {
      code = utils.mergeSQLCode(undefined, code, cr.json_alt_presentation_query);
    }
    return await this.submitToDatabase(
      cr.host,
      cr.path,
      cr.port,
      cr.db_schema,
      cr.timeout,
      code,
      'query'
    );
  }

  // PUBLIC:
  // This is cutsom code executer, unrelated to question - that is nothing will be appended...
  public static async execSQLCodeCodeRunner(id_code_runner, code: string) {
    winston.debug(`Executing SQL question (id_code_runner=${id_code_runner}) ...`);
    return await this._execSQLCodeCodeRunner(
      `SELECT host,
                    port,
                    path,
                    db_schema,
                    timeout
               FROM code_runner
              WHERE id = $(id_code_runner)
                    `,
      {
        id_code_runner: id_code_runner,
      },
      code,
      false,
      false
    );
  }

  public static async execSQLCodeTestInstance(
    id_test_instance,
    ordinal,
    code: string,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    winston.debug(`Executing SQL question (ti.id = ${id_test_instance} ordinal=${ordinal}) ...`);
    return await this._execSQLCodeCodeRunner(
      `SELECT cr_sql.host as host,
                       cr_sql.port as port,
                       cr_sql.path as path,
                       cr_sql.db_schema as db_schema,
                       cr_sql.timeout as timeout,
                       sql_test_fixture,
                       sql_alt_assertion,
                       sql_alt_presentation_query
                    FROM test_instance_question tiq
                     LEFT JOIN sql_question_answer ON sql_question_answer.id_question    = tiq.id_question
                     LEFT JOIN code_runner cr_sql  ON sql_question_answer.id_code_runner = cr_sql.id
                    WHERE id_test_instance = $(id_test_instance)
                      AND ordinal = $(ordinal)
                    `,
      {
        id_test_instance: id_test_instance,
        ordinal: ordinal,
      },
      code,
      append_prefix_suffix,
      append_presentation_query
    );
  }
  public static async execSQLCodeQuestion(
    id_question,
    code: string,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    winston.debug(`Executing SQL question (question.id=${id_question}) ...`);
    return await this._execSQLCodeCodeRunner(
      `SELECT host, port, path, db_schema, timeout,
                    sql_test_fixture,
                    sql_alt_assertion,
                    sql_alt_presentation_query
                FROM sql_question_answer
                JOIN code_runner on sql_question_answer.id_code_runner = code_runner.id
                WHERE sql_question_answer.id_question = $(id_question)
                `,
      {
        id_question: id_question,
      },
      code,
      append_presentation_query,
      append_prefix_suffix
    );
  }
  public static async execJSCodeTestInstance(
    id_test_instance,
    ordinal,
    code,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return await this._execJSCodeCodeRunner(
      `SELECT host, port, path,
                    json_test_fixture,
                    json_alt_assertion,
                    json_alt_presentation_query
                FROM test_instance_question tiq
                JOIN json_question_answer
                  ON json_question_answer.id_question = tiq.id_question
                JOIN code_runner
                  ON json_question_answer.id_code_runner = code_runner.id
                WHERE id_test_instance = $(id_test_instance)
                      AND ordinal = $(ordinal)
                    `,
      {
        id_test_instance: id_test_instance,
        ordinal: ordinal,
      },
      code,
      append_presentation_query,
      append_prefix_suffix
    );
  }
  public static async execJSCodeQuestion(
    id_question,
    code,
    append_prefix_suffix: boolean = true,
    append_presentation_query: boolean = false
  ) {
    return await this._execJSCodeCodeRunner(
      `SELECT host, port, path, json_test_fixture
                FROM json_question_answer
                JOIN code_runner on json_question_answer.id_code_runner = code_runner.id
                WHERE json_question_answer.id_question = $(id_question)
                `,
      { id_question },
      code,
      append_presentation_query,
      append_prefix_suffix
    );
  }

  //checkSingleCQuestionQ(idQuestion, req.body.source, db, idCourse, req.body.id_programming_language)
  public static async execCodeQuestionWithInputs(
    id_question,
    code,
    inputs,
    id_course,
    id_programming_language,
    priority,
    env: ScoringEnvironment
  ) {
    winston.debug(`executing execCodeQuestionWithInputs(id_question = ${id_question})`);
    // console.log('YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY');
    // console.log(inputs);
    // console.log('YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY');
    let [rtcQ, rtcT, CR, j0, files] = await this.getSubmitParams(
      id_question,
      id_course,
      id_programming_language
    );
    let inputs_ids = [];
    inputs.forEach((input) => inputs_ids.push(input.id));
    rtcT = rtcT.filter((rtc_test) => inputs_ids.includes(rtc_test.id));

    files.forEach((element) => {
      element.bucketName = globals.getQuestionsAttachmentsBucketName();
    });

    return this.submitToJudge0CodeRunner(
      CR.host,
      CR.path,
      CR.port,
      code,
      undefined, // text_answer,
      inputs,
      rtcQ,
      rtcT,
      j0.id,
      j0.compiler_options,
      files,
      priority,
      env
    );
  }
  private static async getSubmitParams(id_question, id_course, id_programming_language) {
    // TODO: optimize these SQLs, reduce the number of queries...
    let runtime_constraints_question = db.manyOrNone(
      `SELECT
            rtc.name,
            COALESCE (rtc_q.override_value,
              rtc_c.override_value,
              rtc.default_value) AS value

            FROM programming_language pl
            CROSS JOIN runtime_constraint rtc
            LEFT JOIN runtime_constraint_course rtc_c
                   ON rtc.id = rtc_c.id_runtime_constraint
                  AND pl.id = rtc_c.id_programming_language
                  AND rtc_c.id_course = $(id_course)
            LEFT JOIN runtime_constraint_question rtc_q
                   ON rtc.id = rtc_q.id_runtime_constraint
                  AND pl.id = rtc_q.id_programming_language
                  AND rtc_q.id_question = $(id_question)
            WHERE pl.id = $(id_programming_language)
            ORDER BY pl.id, rtc.id`,
      {
        id_course,
        id_question,
        id_programming_language,
      }
    );

    let runtime_constraints_tests = db.manyOrNone(
      `SELECT
            cqt.id, rtc.name, rtc_t.override_value AS value

            FROM c_question_test cqt
            CROSS JOIN programming_language pl
            CROSS JOIN runtime_constraint rtc
            LEFT JOIN runtime_constraint_test rtc_t
                        ON rtc.id = rtc_t.id_runtime_constraint
                       AND pl.id = rtc_t.id_programming_language
                       AND rtc_t.id_c_question_test = cqt.id
            WHERE pl.id = $(id_programming_language)
              AND cqt.id IN (SELECT c_question_test.id FROM c_question_test
                              WHERE c_question_answer_id IN
                                (SELECT c_question_answer.id FROM c_question_answer WHERE id_question = $(id_question)))
              AND rtc_t.override_value IS NOT NULL
            ORDER BY cqt.id, rtc.id`,
      {
        id_programming_language,
        id_question,
      }
    );

    let code_runner = db.one(
      `SELECT
            cr.host, cr.port, cr.path

            FROM code_runner cr
            WHERE id = (SELECT id_code_runner FROM course_code_runner
                        WHERE id_course = $(id_course)
                        AND id_programming_language = $(id_programming_language))`,
      {
        id_course,
        id_programming_language,
      }
    );

    let j0_id = db.oneOrNone(
      `SELECT judge0_id AS id, compiler_options
            FROM programming_language
            WHERE id = $(id_programming_language)`,
      {
        id_programming_language,
      }
    );

    let privateAttachments = db.manyOrNone(
      `SELECT filename, original_name
            FROM question_attachment
            WHERE id_question = $(id_question)
            AND NOT is_public `,
      {
        id_question,
      }
    );

    let [rtcQ, rtcT, CR, j0, files] = await Promise.all([
      runtime_constraints_question,
      runtime_constraints_tests,
      code_runner,
      j0_id,
      privateAttachments,
    ]);
    // console.table(files);
    return [rtcQ, rtcT, CR, j0, files];
  }
  public static async execComplexQuestion(
    id_question,
    code,
    text_answer,
    inputs,
    submittedFiles,
    id_course,
    id_programming_language,
    priority,
    env: ScoringEnvironment
  ) {
    winston.debug(`executing execComplexQuestion(id_question = ${id_question})`);
    // TODO: optimize these SQLs, reduce the number of queries...
    let [rtcQ, rtcT, CR, j0, files] = await this.getSubmitParams(
      id_question,
      id_course,
      id_programming_language
    );
    // removed the input (test-case) runtime constraint for this, it is an overkill
    let mergedFiles = [];
    if (submittedFiles && submittedFiles.length) {
      mergedFiles = mergedFiles.concat(submittedFiles);
    }
    if (files && files.length) {
      mergedFiles = mergedFiles.concat(files);
    }
    // console.table(mergedFiles);
    return this.submitToJudge0CodeRunner(
      CR.host,
      CR.path,
      CR.port,
      code,
      text_answer,
      inputs,
      rtcQ,
      rtcT,
      j0.id,
      j0.compiler_options,
      mergedFiles,
      priority,
      env
    );
  }

  public static async execCustomCodeWithInput(
    id_course,
    code: string,
    input,
    id_programming_language,
    priority,
    env: ScoringEnvironment
  ) {
    winston.debug(`executing execCustomCodeWithInput(code = ${code})`);
    // TODO: optimize these SQLs, reduce the number of queries...
    // let runtime_constraints_question = db.manyOrNone(`SELECT
    //     rtc.name,
    //     COALESCE (rtc_q.override_value,
    //       rtc_c.override_value,
    //       rtc.default_value) AS value

    //     FROM programming_language pl
    //     CROSS JOIN runtime_constraint rtc
    //     LEFT JOIN runtime_constraint_course rtc_c
    //            ON rtc.id = rtc_c.id_runtime_constraint
    //           AND pl.id = rtc_c.id_programming_language
    //           AND rtc_c.id_course = $(id_course)
    //     LEFT JOIN runtime_constraint_question rtc_q
    //            ON rtc.id = rtc_q.id_runtime_constraint
    //           AND pl.id = rtc_q.id_programming_language
    //           AND rtc_q.id_question = $(id_question)
    //     WHERE pl.id = $(id_programming_language)
    //     ORDER BY pl.id, rtc.id`, {
    //     id_course,
    //     id_question,
    //     id_programming_language
    // });

    // let runtime_constraints_tests = db.manyOrNone(`SELECT
    //     cqt.id, rtc.name, rtc_t.override_value AS value

    //     FROM c_question_test cqt
    //     CROSS JOIN programming_language pl
    //     CROSS JOIN runtime_constraint rtc
    //     LEFT JOIN runtime_constraint_test rtc_t
    //                 ON rtc.id = rtc_t.id_runtime_constraint
    //                AND pl.id = rtc_t.id_programming_language
    //                AND rtc_t.id_c_question_test = cqt.id
    //     WHERE pl.id = $(id_programming_language)
    //       AND cqt.id IN (SELECT c_question_test.id FROM c_question_test
    //                       WHERE c_question_answer_id IN
    //                         (SELECT c_question_answer.id FROM c_question_answer WHERE id_question = $(id_question)))
    //       AND rtc_t.override_value IS NOT NULL
    //     ORDER BY cqt.id, rtc.id`, {
    //     id_programming_language, id_question
    // });

    let code_runner = db.one(
      `SELECT
            cr.host, cr.port, cr.path

            FROM code_runner cr
            WHERE id = (SELECT id_code_runner FROM course_code_runner
                        WHERE id_course = $(id_course)
                        AND id_programming_language = $(id_programming_language))`,
      {
        id_course,
        id_programming_language,
      }
    );

    let j0_id = db.oneOrNone(
      `SELECT judge0_id AS id, compiler_options
            FROM programming_language
            WHERE id = $(id_programming_language)`,
      {
        id_programming_language,
      }
    );

    // let privateAttachments = db.manyOrNone(`SELECT filename, label
    //     FROM question_attachment
    //     WHERE id_question = $(id_question)
    //     AND NOT is_public `, {
    //     id_question
    // });

    let [CR, j0] = await Promise.all([code_runner, j0_id]);
    // let inputs_ids = [];
    // inputs.forEach(input => inputs_ids.push(input.id));

    // rtcT = rtcT.filter(rtc_test => inputs_ids.includes(rtc_test.id));

    return this.submitToJudge0CodeRunner(
      CR.host,
      CR.path,
      CR.port,
      code,
      undefined, // text_answer,
      [input],
      [],
      [],
      j0.id,
      j0.compiler_options,
      null,
      priority,
      env
    );
  }

  // This is used eg from tutorials to execute custom code.
  // prog language does not need to be set if the course uses only one language
  public static async execCustomCodeWithInput2(
    id_course,
    code: string,
    input,
    id_code_runner,
    priority,
    env: ScoringEnvironment,
    id_programming_language?
  ) {
    winston.debug(`executing execCustomCodeWithInput2(code = ${code})`);

    let code_runner = db.one(
      `SELECT
            cr.host, cr.port, cr.path
            FROM code_runner cr
            WHERE id = $(id_code_runner)`,
      {
        id_code_runner,
      }
    );
    let progLangClause = '';
    if (id_programming_language) {
      progLangClause = ` WHERE programming_language.id = $(id_programming_language) `;
    }
    let j0_id = db.oneOrNone(
      `SELECT judge0_id AS id, compiler_options
               FROM programming_language
               JOIN course_code_runner
                 ON course_code_runner.id_course = $(id_course)
                AND course_code_runner.id_code_runner =  $(id_code_runner)
                AND course_code_runner.id_programming_language = programming_language.id
                ${progLangClause}
                ORDER BY id desc LIMIT 1`,
      {
        id_course,
        id_code_runner,
        id_programming_language,
      }
    );

    // let privateAttachments = db.manyOrNone(`SELECT filename, label
    //     FROM question_attachment
    //     WHERE id_question = $(id_question)
    //     AND NOT is_public `, {
    //     id_question
    // });

    let [CR, j0] = await Promise.all([code_runner, j0_id]);
    // let inputs_ids = [];
    // inputs.forEach(input => inputs_ids.push(input.id));

    // rtcT = rtcT.filter(rtc_test => inputs_ids.includes(rtc_test.id));
    // console.log({ CR, j0 });
    return this.submitToJudge0CodeRunner(
      CR.host,
      CR.path,
      CR.port,
      code,
      undefined, // text_answer,
      [input],
      [],
      [],
      j0.id,
      j0.compiler_options,
      null,
      priority,
      env
    );
  }
}
