var winston = require('winston');
var request = require('request');

export default class HttpService {
  public static async postJsonToUrl(host: string, path: string, port: string, payload: object) {
    var options = {
      url: port == '443' ? `https://${host}/${path}` : `http://${host}:${port}/${path}`,
      json: true,
      body: payload, // JSON.stringify(payload)
      // timeout: 1200000,
      pool: {
        maxSockets: Infinity,
      },
    };
    return new Promise(function (resolve, reject) {
      winston.debug('POSTing JSON:\n' + JSON.stringify(options, undefined, 4));
      request.post(options, function (error, res, body) {
        if (!error && res.statusCode == 200) {
          resolve(body);
        } else {
          // winston.error(error);
          reject(error);
        }
      });
    });
  }
}
