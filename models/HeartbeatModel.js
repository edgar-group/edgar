const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const winston = require('winston');

var HeartbeatSchema = new Schema({
  _id: {
    type: Number,
  }, //'test_instance_id': Number,
  id_test: Number,
  username: String,
  fullname: String,
  progress: Number,
  expireTs: {
    type: Date,
    default: Date.now,
  },
  beats: [Schema.Types.Mixed],
});

HeartbeatSchema.statics.saveHeartbeat = async function (currTI, id_test, req, currAnswers) {
  try {
    var progress = 0;
    if (currAnswers && currAnswers.length) {
      progress =
        (100 *
          currAnswers
            .map(function (item) {
              if (
                item &&
                ((item.multichoice && item.multichoice.length) ||
                  (item.answersStatic && item.answersStatic.length) ||
                  (item.codeAnswer && item.codeAnswer.code && item.codeAnswer.code.trim().length) ||
                  (item.textAnswer && item.textAnswer.trim().length))
              ) {
                return 1;
              } else {
                return 0;
              }
            })
            .reduce((prev, curr) => prev + curr, 0)) /
        currAnswers.length;
    }
    var query = {
      _id: currTI,
    };
    var update = {
      id_test,
      username: req.session.passport.user,
      fullname: req.session.fullname,
      progress: progress,
      expireTs: new Date(),
      $push: {
        beats: {
          $each: [
            {
              ts: new Date(),
              ip: req.headers['x-forwarded-for'] || req.socket.remoteAddress,
            },
          ],
          $slice: -200,
        },
      },
    };
    var options = {
      upsert: true,
    };
    console.log('save hb', update);
    await model.findOneAndUpdate(query, update, options);
  } catch (error) {
    winston.error(error);
  }
};
const model = mongoose.model('Heartbeat', HeartbeatSchema);
module.exports = model;
// db.Heartbeat.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 604800 } )
// // that's SEVEN days.
