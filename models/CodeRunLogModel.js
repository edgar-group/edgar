var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var codeRunLogSchema = new Schema({
  _id: { type: String }, // id_test+id_question,
  id_test: Number,
  id_question: Number,
  ts_modified: Date,
  code_runs: [Schema.Types.Mixed],
});

getCodeRunId = (id_test, id_question) => `${id_test}+${id_question}`;

codeRunLogSchema.statics.getCodeRuns = async function (id_test, id_question) {
  try {
    const rv = await this.model('CodeRunLog').findById(getCodeRunId(id_test, id_question));
    if (rv && rv.code_runs) {
      return rv.code_runs;
    }
  } catch (error) {
    winston.error(error);
  }
  return [];
};

codeRunLogSchema.statics.getCodeRunsTail = function (id_test, id_question, length, seconds) {
  try {
    const rv = this.model('CodeRunLog').find(
      { _id: getCodeRunId(id_test, id_question) },
      length ? { code_runs: { $slice: -1 * length } } : {}
    );

    if (rv[0]) rv = rv[0];
    if (rv && rv.code_runs) {
      // TODO: move the filtering to the mongo, can mongoose do that? could not find $filter operator
      if (seconds) {
        let now = new Date().getTime();
        let mseconds = 1000 * seconds;
        rv.code_runs = rv.code_runs.filter((x) => now - x.ts.getTime() < mseconds);
      }
      return rv.code_runs;
    }
  } catch (error) {
    winston.error(error);
  }
  return [];
};

codeRunLogSchema.statics.logCodeRun = async function (
  username,
  id_student,
  id_test,
  code,
  id_question,
  ordinal,
  score,
  is_correct
) {
  if (!username || !id_test) {
    winston.warn(
      'Warning: logCodeRun: unknown session user or id_test for log entry:' +
        JSON.stringify({
          username,
          id_test,
          code,
          id_question,
          ordinal,
          score,
          is_correct,
        })
    );
  } else {
    try {
      var query = {
        _id: getCodeRunId(id_test, id_question),
      };
      var update = {
        id_test,
        id_question: id_question,
        ts_modified: new Date(),
        $push: {
          code_runs: {
            ts: new Date(),
            username: username,
            id_student,
            id_question,
            ordinal,
            code,
            score,
            is_correct,
          },
        },
      };
      var options = {
        upsert: true,
      };
      const item = await this.model('CodeRunLog').findOneAndUpdate(query, update, options);
      winston.debug('CodeRunLog saved.');
      return item;
    } catch (error) {
      winston.error(error);
      winston.error('Attempted test query: ' + JSON.stringify(query));
      winston.error('Attempted test update: ' + JSON.stringify(update));
    }
  }
};

module.exports = mongoose.model('CodeRunLog', codeRunLogSchema);
