var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const winston = require('winston');

QuestionSequenceSchema = new Schema({
  created: String,
  expireTs: {
    type: Date,
    default: Date.now,
  },
  sequence: [Number],
});
QuestionSequenceSchema.statics.insertSequence = async function (userCreated, sequence) {
  try {
    var newQuestionSequence = new this({
      created: userCreated,
      sequence: sequence,
    });
    const qs = await newQuestionSequence.save();
    return qs.id;
  } catch (error) {
    winston.error(error);
  }
};

// this is just a wrapper that returns callback to promise so that i can use it more elegantly later
QuestionSequenceSchema.statics.getById = async function (id) {
  const model = this.model('QuestionSequence');
  try {
    return await model.findById(id);
  } catch (error) {
    winston.error(error);
  }
};

module.exports = mongoose.model('QuestionSequence', QuestionSequenceSchema);

// Execute this @server:
// db.questionsequences.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 2 * 604800 } )
// db.QuestionSequence.createIndex( { "expireTs": 1 }, { expireAfterSeconds: 2 * 604800 } )
// that's 4 * SEVEN days = 2 weeks.
