'use strict';
var mongoose = require('mongoose');
var winston = require('winston');
var Schema = mongoose.Schema;

var ongoingTestsSchema = new Schema({
  _id: String, // username is id
  currAnswers: Schema.Types.Mixed,
  currTestProperties: Schema.Types.Mixed,
});

ongoingTestsSchema.statics.setCurrTestProperties = async function (session, alltp) {
  let username = session.anon_alt_id || session.passport.user;
  try {
    var query = {
      _id: username,
    };
    var update = {
      currTestProperties: alltp,
    };
    var options = {
      upsert: true,
    };
    await this.model('OngoingTests').findOneAndUpdate(query, update, options);
    winston.debug('CurrTestProperties saved.');
  } catch (error) {
    winston.error(error);
  }
};

ongoingTestsSchema.statics.updateCurrTestProperties = async function (
  session,
  tp,
  id_test_instance
) {
  var model = this.model('OngoingTests');
  var username = session.anon_alt_id || session.passport.user;
  try {
    let rv = (await model.findById(username)) || {};
    let alltp = rv.currTestProperties || {};
    alltp[id_test_instance || session.id_test_instance] = tp;
    await model.setCurrTestProperties(session, alltp);
    return tp;
  } catch (err) {
    winston.error(err);
  }
};

ongoingTestsSchema.statics.setCurrAnswers = async function (session, ca, usernameToUpdate) {
  // for lecture quiz, check if it's an anonymous user
  try {
    var username = session.anon_alt_id || usernameToUpdate || session.passport.user;
    winston.debug('Updating CA (all answers) for ' + username);
    var query = {
      _id: username,
    };
    var update = {
      currAnswers: ca,
    };
    var options = {
      upsert: true,
    };
    const item = this.model('OngoingTests').findOneAndUpdate(query, update, options);
    winston.debug('Current answers saved.');
    return item;
  } catch (error) {
    winston.error(error);
  }
};

ongoingTestsSchema.statics.updateCurrAnswer = async function (
  usernameToUpdate,
  id_test_instance,
  ordinal,
  answer
) {
  const model = this.model('OngoingTests');
  try {
    winston.debug('Updating one answer for ' + usernameToUpdate);
    const rv = await model.findById(usernameToUpdate);
    let ca = (rv && rv.currAnswers) || {};
    if (!ca[id_test_instance]) ca[id_test_instance] = [];
    // console.log('ca before:', JSON.stringify(ca));
    ca[id_test_instance][ordinal - 1] = answer;
    // console.log('ca after:', JSON.stringify(ca));
    var query = {
      _id: usernameToUpdate,
    };
    var update = {
      currAnswers: ca,
    };
    var options = {
      upsert: true,
    };
    const item = this.model('OngoingTests').findOneAndUpdate(query, update, options);
    winston.debug('Answer updated:' + item);
    return item;
  } catch (error) {
    winston.error(error);
  }
};

ongoingTestsSchema.statics.clearTestData2 = async function (session, currTI) {
  var model = this.model('OngoingTests');
  var username = session.anon_alt_id || session.passport.user;
  try {
    const rv = await model.findById(username);
    // Deleteing just my 'part', ie id_test_instance
    let ca = (rv && rv.currAnswers) || {};
    delete ca[currTI];

    let tp = (rv && rv.currTestProperties) || {};
    delete tp[currTI];

    if (Object.keys(ca).length === 0) {
      return await model.clearAllTestsData(session);
    } else {
      return await Promise.all([
        model.setCurrAnswers(session, ca, username),
        model.setCurrTestProperties(session, tp),
      ]);
    }
  } catch (error) {
    winston.error(error);
  }
};
ongoingTestsSchema.statics.clearAllTestsData = async function (session) {
  var username = session.anon_alt_id || session.passport.user;
  try {
    return await this.model('OngoingTests').findByIdAndDelete(username);
  } catch (error) {
    winston.error(error);
  }
};
ongoingTestsSchema.statics.getCurrentAnswers = async function (username, id_test_instance) {
  try {
    const rv = await this.model('OngoingTests').findById(
      username,
      `currAnswers.${id_test_instance}`
    );
    // console.log('getCurrentAnswers currAnswers:', currAnswers);
    if (rv && rv.currAnswers && rv.currAnswers[id_test_instance]) {
      ///session.currAnswers = currAnswers.currAnswers;
      //session.currTestProperties = currAnswers.currTestProperties;
      // winston.info(
      //   `getCurrentAnswers: recovered currAnswers for ${username}. Length:  ${rv.currAnswers[id_test_instance].length}`
      // );
      return rv.currAnswers[id_test_instance];
    } else {
      return [];
    }
  } catch (error) {
    winston.error(error);
  }
};

ongoingTestsSchema.statics.getCurrTestProperties = async function (session, id_test_instance) {
  try {
    const rv = await this.model('OngoingTests').findById(session.passport.user);
    if (rv && rv.currTestProperties) {
      return rv.currTestProperties[id_test_instance || session.id_test_instance];
    } else {
      return null;
    }
  } catch (error) {
    winston.error(error);
  }
};

// find student's answers for given test instance
// username = student's alt_id
// id_test_instance = test instance id
ongoingTestsSchema.statics.getStudentAnswers = async function (username, id_test_instance) {
  try {
    const rv = await this.model('OngoingTests').findById(username);
    if (rv && rv.currAnswers) {
      return rv.currAnswers[id_test_instance];
    }
  } catch (error) {
    winston.error(error);
  }
};

//var ongoingTestsModel = mongoose.model('OngoingTests', ongoingTestsSchema, 'OngoingTests');

module.exports = mongoose.model('OngoingTests', ongoingTestsSchema);
