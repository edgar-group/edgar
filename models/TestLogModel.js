var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var utils = require('../common/utils');
var testLogSchema = new Schema({
  _id: { type: Number }, //'test_instance_id': Number,
  username: String,
  fullname: String,
  events: [Schema.Types.Mixed],
  //   eventsCount: Number,
});
const MAX_EVENT_ITEMS = 5000;
const LAST_EVENT_ITEMS = 4500;

testLogSchema.statics.getLogEvents = async function (_id) {
  try {
    const rv = await this.model('TestLog').findById(_id);
    if (rv && rv.events) {
      return rv.events;
    }
  } catch (error) {
    winston.error(error);
  }
  return [];
};

testLogSchema.statics.sliceLogEvents = async function (username, _id) {
  try {
    const rv = await this.model('TestLog').findById(_id);
    let arrSize = rv && rv.events && rv.events.length;
    winston.info('Slicing TestLog for ' + username + ' and _id = ' + _id);
    const item2 = await this.model('TestLog').findOneAndUpdate(
      {
        _id,
      },
      { $set: { events: rv.events.slice(-LAST_EVENT_ITEMS) } },
      {
        upsert: true,
        new: true,
        select: {
          _id: 1,
          arrSize: { $size: '$events' },
        },
      }
    );
    winston.info(
      `TestLog sliced for ${username} and _id = ${_id} from ${arrSize} to ${item2} events.`
    );
    return item2;
  } catch (error) {
    winston.error(error);
  }
};

testLogSchema.statics.logEvent = async function (session, entry, currTI) {
  let query, update;
  try {
    entry.mongoTs = new Date();
    if (!session.id_test_instance && !utils.isAllowedAnonymousLogEvent(entry.eventName)) {
      winston.warn(
        'logEvent: ' +
          JSON.stringify({
            username: session.anon_alt_id || (session.passport && session.passport.user),
            id_test_instance: session.id_test_instance,
            currTI: currTI,
            entry,
          })
      );
    }
    let id_test_instance = session.id_test_instance || currTI;
    var username = session.anon_alt_id || (session.passport && session.passport.user);
    if (!username || !id_test_instance) {
      if (!utils.isAllowedAnonymousLogEvent(entry.eventName)) {
        winston.warn(
          'Warning: unknown session user (' +
            username +
            ') or (id_test_instance, currTI) = (' +
            session.id_test_instance +
            ', ' +
            currTI +
            ') for log entry:' +
            JSON.stringify(entry)
        );
      }
    } else {
      query = {
        _id: id_test_instance,
      };
      entry.username = username;
      entry.fullname = session.fullname;
      update = {
        $push: {
          events: entry,
        },
      };
      var options = {
        upsert: true,
        select: {
          _id: 1,
          arrSize: { $size: '$events' },
        },
      };
      const item = this.model('TestLog').findOneAndUpdate(query, update, options);
      // winston.debug('TestLog saved.');
      let arrSize = item && item._doc && item._doc.arrSize;
      if (arrSize > MAX_EVENT_ITEMS) {
        this.sliceLogEvents(username, id_test_instance); // don't wait for this to finish
      }
      return item;
    }
  } catch (error) {
    winston.error(error);
    winston.error('Attempted test query: ' + JSON.stringify(query));
    winston.error('Attempted test update: ' + JSON.stringify(update));
    winston.error('Attempted test log entry is ' + JSON.stringify(entry));
    return null;
  }
};

//var testLogModel = mongoose.model('TestLog', testLogSchema, 'TestLog'); // collection name

module.exports = mongoose.model('TestLog', testLogSchema);
