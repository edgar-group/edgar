'use strict';
const mongoose = require('mongoose');
const winston = require('winston');
const Schema = mongoose.Schema;

const tutorialProgressSchema = new Schema({
  _id: Schema.Types.Number,
  courseId: Schema.Types.Number,
  studentId: Schema.Types.Number,
  tutorialId: Schema.Types.Number,
  startedTs: {
    type: Schema.Types.Date,
    default: Date.now,
  },
  finishedTs: {
    type: Schema.Types.Date,
  },
  latestStepOrdinal: {
    type: Schema.Types.Number,
    default: 1,
  },
  events: [Schema.Types.Mixed],
  currentAnswers: [Schema.Types.Mixed],
});

tutorialProgressSchema.statics.getTutorialProgress = async function (
  tutorialId,
  courseId,
  studentId
) {
  try {
    const model = this.model('tutorialProgress');
    return await model.findOne({
      tutorialId: tutorialId,
      courseId: courseId,
      studentId: studentId,
    });
  } catch (error) {
    winston.error(error);
  }
};
tutorialProgressSchema.statics.getTutorialProgresses = async function (courseId, studentId) {
  try {
    const model = this.model('tutorialProgress');
    return await model.find(
      {
        courseId: courseId,
        studentId: studentId,
      },
      {
        tutorialId: 1,
        latestStepOrdinal: 1,
        startedTs: 1,
        finishedTs: 1,
        events: {
          $filter: {
            input: '$events',
            as: 'event',
            cond: {
              $eq: ['$$event.eventName', 'Step progress'],
            },
          },
        },
      }
    );
  } catch (error) {
    winston.error(error);
  }
};
function getStepNumber(string) {
  const match = string.match(/Opened step (\d+)/);
  if (match) {
    return parseInt(match[1]);
  }
  return 0;
}
tutorialProgressSchema.statics.getUsageReport = async function (
  courseId,
  tutorialId,
  arrStudentIds
) {
  try {
    const model = this.model('tutorialProgress');
    const report = await model.aggregate([
      {
        $match: {
          $and: [{ tutorialId, courseId }, { studentId: { $in: arrStudentIds } }],
        },
      },
      {
        $project: {
          _id: 0,
          studentId: 1,
          tutorialId: 1,
          events: {
            $filter: {
              input: '$events',
              as: 'event',
              cond: {
                $eq: ['$$event.eventName', 'Step progress'],
              },
            },
          },
          loadCount: {
            $size: {
              $filter: {
                input: '$events',
                as: 'event',
                cond: { $eq: ['$$event.eventName', 'Loading tutorial'] },
              },
            },
          },
        },
      },
    ]);
    report.forEach((student) => {
      student.steps = {};
      student.totalMinutes = 0;
      student.totalReasonableMinutes = 0;
      student.events.forEach((event, index) => {
        const stepNumber = getStepNumber(event.eventData);
        const stats = student.steps[stepNumber] || { count: 0, totalMinutes: 0 };
        stats.count++;
        if (index < student.events.length - 1) {
          const nextEvent = student.events[index + 1];
          const minutes = (nextEvent.mongoTs - event.mongoTs) / 60000;
          stats.totalMinutes += minutes;
          student.totalMinutes += minutes;
          if (minutes < 100) {
            student.totalReasonableMinutes += minutes;
          }
        }
        student.steps[stepNumber] = stats;
      });
      delete student.events;
    });
    return report;
    // return await model
    //   .aggregate()
    //   .match({
    //     courseId,
    //     tutorialId,
    //   })
    //   .project({
    //     _id: 0,
    //     studentId: 1,
    //     latestStepOrdinal: 1,
    //     startedTs: 1,
    //     finishedTs: 1,
    //   });
  } catch (error) {
    winston.error(error);
  }
};
// tutorialProgressSchema.statics.updateLatestStepOrdinal = async function (
//   courseId,
//   studentId,
//   tutorialId,
//   newOrdinal
// ) {
//   try {
//     const model = this.model('tutorialProgress');
//     return await model.findOneAndUpdate(
//       {
//         courseId: courseId,
//         studentId: studentId,
//         tutorialId: tutorialId,
//       },
//       {
//         latestStepOrdinal: newOrdinal,
//       },
//       {
//         upsert: true,
//       }
//     );
//   } catch (error) {
//     winston.error(error);
//   }
// };

tutorialProgressSchema.statics.updateTutorialAnswers = async function (
  courseId,
  studentId,
  tutorialId,
  answers
) {
  try {
    const model = this.model('tutorialProgress');
    const query = {
      courseId: courseId,
      studentId: studentId,
      tutorialId: tutorialId,
    };
    const update = {
      currentAnswers: answers,
      $push: {
        events: {
          mongoTs: new Date(),
          eventName: 'Update tutorial answers',
          eventData: JSON.stringify(answers),
        },
      },
    };
    const options = {
      upsert: true,
    };
    const rv = await model.findOneAndUpdate(query, update, options);
    return rv.currentAnswers;
  } catch (error) {
    winston.error(error);
  }
};

tutorialProgressSchema.statics.updateLatestStepOrdinal = async function (
  courseId,
  studentId,
  tutorialId,
  newOrdinal
) {
  try {
    const model = this.model('tutorialProgress');
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        latestStepOrdinal: newOrdinal,
        $push: {
          events: {
            mongoTs: new Date(),
            eventName: 'Step progress',
            eventData: `Opened step ${newOrdinal}`,
          },
        },
      };
    return await model.findOneAndUpdate(query, update);
  } catch (error) {
    winston.error(error);
  }
};

tutorialProgressSchema.statics.markAsFinished = async function (
  courseId,
  studentId,
  tutorialId,
  finishedTs
) {
  try {
    const model = this.model('tutorialProgress');
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        finishedTs: finishedTs,
      };
    return await model.findOneAndUpdate(query, update);
  } catch (error) {
    winston.error(error);
  }
};

tutorialProgressSchema.statics.clear = async function (tutorialId) {
  try {
    const model = this.model('tutorialProgress');
    const query = {
      tutorialId: tutorialId,
    };
    return await model.deleteMany(query);
  } catch (error) {
    winston.error(error);
  }
};

tutorialProgressSchema.statics.logEvent = async function (courseId, studentId, tutorialId, event) {
  try {
    event.mongoTs = new Date();
    const query = {
        courseId: courseId,
        studentId: studentId,
        tutorialId: tutorialId,
      },
      update = {
        $push: {
          events: event,
        },
      },
      options = {
        upsert: true,
        setDefaultsOnInsert: true,
      };
    return await this.model('tutorialProgress').findOneAndUpdate(query, update, options);
  } catch (error) {
    winston.error(error);
  }
};

module.exports = mongoose.model('tutorialProgress', tutorialProgressSchema);
