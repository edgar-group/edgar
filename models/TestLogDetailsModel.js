var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');

var testLogDetailsSchema = new Schema(
  {
    id_test_instance: { type: Number },
    ordinal: { type: Number },
    code: { type: String },
    result: Schema.Types.Mixed,
  },
  { capped: { size: 20737418240, autoIndexId: true } } // cca 20GB
);

testLogDetailsSchema.statics.getLoggedResults = async function (id_test_instance) {
  try {
    const rv = await this.model('TestLogDetails').find({ id_test_instance: id_test_instance });
    if (rv) {
      // flatten:
      return rv.map((x) => {
        return {
          ...x.result,
          code: x.code,
          ordinal: x.ordinal,
        };
      });
    } else {
      return [];
    }
  } catch (error) {
    winston.error(error);
  }
};

testLogDetailsSchema.statics.logResult = async function (id_test_instance, ordinal, code, entry) {
  try {
    entry.mongoTs = new Date();
    let logEntry = new TestLogDetailsModel({
      id_test_instance: id_test_instance,
      ordinal,
      code,
      result: entry,
    });
    await logEntry.save();
  } catch (error) {
    winston.error(error);
  }
};

const TestLogDetailsModel = mongoose.model('TestLogDetails', testLogDetailsSchema);
testLogDetailsSchema.index({
  id_test_instance: 1,
});

module.exports = TestLogDetailsModel;
