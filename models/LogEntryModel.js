var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LogConfigSchema = new Schema({
  label: String,
  type: String,
  path: String,
});

var LogEntrySchema = new Schema(
  {
    logConfig: LogConfigSchema,
    hostname: String,
    appInstance: Number,
    message: String,
    ts_log: Date,
    ts_collector: Date,
    script: String,
    level: String,
  },
  { capped: { size: 10485760, autoIndexId: true } } // cca 10M
);

LogEntrySchema.statics.getLogEntries = async function (rowLimit, dateFrom) {
  var dateClause = undefined;
  if (!rowLimit) rowLimit = 1000;
  if (dateFrom) {
    var currDate = new Date(dateFrom);
    var nextDate = new Date(currDate.getTime() + 86400000);
    dateClause = {
      ts_collector: {
        $gte: '"' + dateFrom,
        $lt: '"' + nextDate.toISOString().split('T')[0],
      },
    };
  }
  return await this.model('LogEntry')
    .find(dateClause)
    .sort({
      ts_collector: -1,
    })
    .limit(rowLimit);

  // var parseData = async function (data) {
  //     var arr = [];
  //     data.forEach(function (row) {
  //         row['edgar']['timestamp'] = moment(row['@timestamp'].slice(1, -1)).format("YYYY-MM-DD hh:mm:ss");
  //         arr.push(row['edgar']);
  //     });
  //     return arr;
  // };
};
module.exports = mongoose.model('LogEntry', LogEntrySchema);

// WTF Mongoose, WTF?

// The first argument is the singular name of the collection your model is
// for.Mongoose automatically looks
// for the plural version of your model name.For example,
//     if you use

// const MyModel = mongoose.model('Ticket', mySchema);
// Then Mongoose will create the model
// for your tickets collection, not your ticket collection.

// db.createCollection("logentries", {
//     capped: true,
//     size: 1048576, // 10MB
//     storageEngine: {
//         wiredTiger: {
//             configString: "block_compressor=zlib"
//         }
//     }
// })
