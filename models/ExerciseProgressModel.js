'use strict';
const mongoose = require('mongoose');
const winston = require('winston');
const Schema = mongoose.Schema;

const exerciseProgressSchema = new Schema(
  {
    exerciseId: Schema.Types.Number,
    studentId: Schema.Types.Number,
    startedTs: {
      type: Schema.Types.Date,
      default: Date.now,
    },
    finishedTs: {
      type: Schema.Types.Date,
    },
    latestQuestionOrdinal: {
      type: Schema.Types.Number,
      default: 1,
    },
    events: [Schema.Types.Mixed],
    currentAnswers: [Schema.Types.Mixed],
    currentDifficultyLevelId: Schema.Types.Number,
  },
  {
    _id: false,
  }
);

exerciseProgressSchema.statics.getLog = async function (exerciseId, studentId) {
  try {
    const model = this.model('exerciseProgress');
    return await model.findOne({
      exerciseId: exerciseId,
      studentId: studentId,
    });
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.getStatus = async function (exerciseId, studentId) {
  try {
    const model = this.model('exerciseProgress');
    const item = await model.findOne({
      exerciseId: exerciseId,
      studentId: studentId,
    });
    winston.debug(`Getting exercise progress status`);
    if (item) {
      return {
        currentAnswers: item.currentAnswers,
        currentDifficultyLevelId: item.currentDifficultyLevelId,
        latestQuestionOrdinal: item.latestQuestionOrdinal,
        startedTs: item.startedTs,
      };
    }
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.getExerciseAnswers = async function (exerciseId, studentId) {
  const model = this.model('exerciseProgress');
  try {
    const query = {
      exercise: exerciseId,
      studentId: studentId,
    };
    return await model.findOne(query);
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.updateExerciseAnswers = async function (
  exerciseId,
  studentId,
  answers
) {
  const model = this.model('exerciseProgress');
  try {
    const query = {
      exerciseId: exerciseId,
      studentId: studentId,
    };
    const update = {
      currentAnswers: answers,
    };
    const options = {
      upsert: true,
    };
    const rv = await model.findOneAndUpdate(query, update, options);
    return rv.currentAnswers;
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.updateLatestQuestionOrdinal = async function (
  exerciseId,
  studentId,
  newOrdinal
) {
  try {
    const model = this.model('exerciseProgress');
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        latestQuestionOrdinal: newOrdinal,
        $push: {
          events: {
            eventName: 'Question progress',
            eventData: `From ${newOrdinal - 1} to ${newOrdinal}`,
            mongoTs: new Date(),
          },
        },
      };
    return await model.findOneAndUpdate(query, update);
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.updateDifficultyLevel = async function (
  exerciseId,
  studentId,
  difficultyLevelId
) {
  try {
    const model = this.model('exerciseProgress');
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        currentDifficultyLevelId: difficultyLevelId,
        $push: {
          events: {
            eventName: 'Difficulty level change',
            eventData: `To difficulty level id ${difficultyLevelId}`,
            mongoTs: new Date(),
          },
        },
      };
    return await model.findOneAndUpdate(query, update);
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.markAsFinished = async function (exerciseId, studentId, finishedTs) {
  try {
    const model = this.model('exerciseProgress');
    const query = {
        exercise: exerciseId,
        studentId: studentId,
      },
      update = {
        latestQuestionOrdinal: 1,
        finishedTs: finishedTs,
      };
    return await model.findOneAndUpdate(query, update);
  } catch (error) {
    winston.error(error);
  }
};

exerciseProgressSchema.statics.logEvent = async function (exerciseId, studentId, event) {
  try {
    event.mongoTs = new Date();
    const query = {
        exerciseId: exerciseId,
        studentId: studentId,
      },
      update = {
        $push: {
          events: event,
        },
      },
      options = {
        upsert: true,
        setDefaultsOnInsert: true,
      };
    return await this.model('exerciseProgress').findOneAndUpdate(query, update, options);
  } catch (error) {
    winston.error(error);
  }
};

module.exports = mongoose.model('exerciseProgress', exerciseProgressSchema);
