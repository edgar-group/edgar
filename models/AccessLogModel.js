var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
const useragent = require('useragent');

var AccessLogSchema = new Schema(
  {
    user: String,
    instance: String,
    method: String,
    url: String,
    statusCode: String,
    ip: String,
    // queryString: Schema.Types.Mixed,
    ts: Date,
    responseTime: Number,
    userAgent: String,
  },
  { capped: { size: 20971520, autoIndexId: true } }
);
AccessLogSchema.index({
  user: 1,
});

AccessLogSchema.statics.getAccessLogEntries = async function (rowLimit, dateFrom, username) {
  if (!username) return [];
  var dateClause = undefined;
  if (!rowLimit) rowLimit = 1000;
  if (dateFrom) {
    var currDate = new Date(dateFrom);
    var nextDate = new Date(currDate.getTime() + 86400000);
    dateClause = {
      ts: {
        $gte: '"' + dateFrom,
        $lt: '"' + nextDate.toISOString().split('T')[0],
      },
    };
  }
  return await this.model('AccessLog')
    .find({
      user: username,
      ...dateClause,
    })
    .sort({
      ts: -1,
    })
    .limit(rowLimit);
};
AccessLogSchema.statics.log = async function (req, res) {
  if (req.user && !req.url.startsWith('/metrics') && !req.url.startsWith('/images')) {
    const startTime = process.hrtime(); // Start time for calculating response time
    const responseFinishedCallback = async () => {
      try {
        const endTime = process.hrtime(startTime);
        const responseTime = endTime[0] * 1e3 + endTime[1] / 1e6; // Convert to milliseconds
        const agent = useragent.parse(req.headers['user-agent']);
        const logEntry = new AccessLogModel({
          user: req.user && req.user.nameID ? req.user.nameID : req.user, // on logout req.user is object, this will go away when I drop SAML
          instance: global.appInstance + '@' + global.hostname,
          method: req.method,
          url: req.url,
          statusCode: res.statusCode,
          ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
          // headers: req.headers,
          // queryString: req.query,
          // cookies: req.cookies,
          ts: new Date(), // Timestamp of the request
          responseTime: responseTime,
          userAgent: agent.toString(),
        });
        //winston.debug(JSON.stringify(logEntry));
        await logEntry.save();
      } catch (error) {
        winston.error(
          `Error on saving AccessLogModel: ${JSON.stringify({
            user: req.user,
            instance: global.appInstance + '@' + global.hostname,
            method: req.method,
            url: req.url,
            statusCode: res.statusCode,
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            ts: new Date(), // Timestamp of the request
          })}`
        );
        winston.error(error);
      }
      // winston.debug('AccessLogModel saved.');
    };
    // Listen for response end event to get complete information
    res.on('finish', responseFinishedCallback);
  }
};

const AccessLogModel = mongoose.model('AccessLog', AccessLogSchema);

module.exports = AccessLogModel;

// WTF Mongoose, WTF?

// The first argument is the singular name of the collection your model is
// for.Mongoose automatically looks
// for the plural version of your model name.For example,
//     if you use

// const MyModel = mongoose.model('Ticket', mySchema);
// Then Mongoose will create the model
// for your tickets collection, not your ticket collection.

// db.createCollection("logentries", {
//     capped: true,
//     size: 1048576, // 10MB
//     storageEngine: {
//         wiredTiger: {
//             configString: "block_compressor=zlib"
//         }
//     }
// })
