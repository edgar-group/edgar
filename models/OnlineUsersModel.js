var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const winston = require('winston');
var OnlineUsersSchema = new Schema(
  {
    _id: String, // username
    imgSrc: String,
    rolename: String,
    username: String,
    fullname: String,
    currCourse: String,
    currAcademicYear: String,
    lastRoute: String,
    ts_modified: {
      type: Date,
      default: Date.now,
      expires: 60 * 60,
    },
  },
  { _id: false }
);

OnlineUsersSchema.statics.getOnlineUsers = async function () {
  try {
    return await this.model('OnlineUsers')
      .find({})
      .sort([['ts_modified', -1]])
      .exec();
  } catch (error) {
    winston.error(error);
  }
};
OnlineUsersSchema.statics.getOnlineUsersCount = async function (mins) {
  try {
    return await this.model('OnlineUsers')
      .find({
        ts_modified: {
          $gt: new Date(new Date() - mins * 60 * 1000),
        },
      })
      .countDocuments()
      .exec();
  } catch (error) {
    winston.error(error);
  }
};

module.exports = mongoose.model('OnlineUsers', OnlineUsersSchema);
// db.OnlineUsers.createIndex( { "ts_modified": 1 }, { expireAfterSeconds: 60*60 } )  created via mongoose expires attr (above)
