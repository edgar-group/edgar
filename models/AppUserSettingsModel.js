const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const winston = require('winston');

var appUserSettingsSchema = new Schema({
  username: String,
  currCourseId: Number,
  currAcademicYearId: Number,
  cmSkin: String,
  markWhitespace: {
    type: Boolean,
    default: false,
  },
  showQuestionHeader: {
    type: Boolean,
    default: false,
  },
});
appUserSettingsSchema.index({
  username: 1,
});

appUserSettingsSchema.statics.saveProps = async function (username, propsObject) {
  try {
    winston.debug(
      'AppUserSettings.saveProps(' + username + ', ' + JSON.stringify(propsObject) + ')'
    );
    const query = {
      username,
    };
    const currProps = await AppUserSettings.findOne(query);
    let newProps = currProps || {
      cmSkin: 'ambiance',
    };
    for (let key of Object.keys(propsObject)) {
      newProps[key] = propsObject[key];
    }
    var options = {
      upsert: true,
    };
    const item = AppUserSettings.findOneAndUpdate(query, newProps, options);
    winston.debug('AppUserSettings updated:' + item);
    return item;
  } catch (error) {
    winston.error(error);
  }
};
// return plain props object, lean and mean :-)
appUserSettingsSchema.statics.getProps = async function (username) {
  try {
    const query = {
      username,
    };
    const currProps = await AppUserSettings.findOne(query).lean();
    delete currProps._id;
    delete currProps.__v;
    return currProps;
  } catch (error) {
    winston.error(error);
  }
};
const AppUserSettings = mongoose.model('AppUserSettings', appUserSettingsSchema);

module.exports = AppUserSettings;
