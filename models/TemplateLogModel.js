var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var winston = require('winston');
var templateLogSchema = new Schema(
  {
    _id: { type: Number }, // id_test_instance_question,
    ts_modified: Date,
    success: Boolean,
    log: String,
    error: String,
  },
  { capped: { size: 10485760, autoIndexId: true } } // cca 10M
);

templateLogSchema.statics.log = async function (id_test_instance_question, success, log, error) {
  try {
    var query = {
      _id: id_test_instance_question,
    };
    var update = {
      success,
      _id: id_test_instance_question,
      ts_modified: new Date(),
      log,
      error,
    };
    var options = {
      upsert: true,
    };
    const item = await this.model('TemplateLog').findOneAndUpdate(query, update, options);
    winston.debug('TemplateLog saved.');
    return item;
  } catch (error) {
    winston.error(error);
    winston.error('Error on saving TemplateLog ', err);
    winston.error('Attempted test query: ' + JSON.stringify(query));
    winston.error('Attempted test update: ' + JSON.stringify(update));
  }
};

module.exports = mongoose.model('TemplateLog', templateLogSchema);
