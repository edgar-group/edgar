'use strict';
var db = require('../db').db;
var winston = require('winston');
var utils = require('../common/utils');
var OngoingTests = require('../models/OngoingTests.js');
var Scorer = require('../lib/Scorer');
var globals = require('../common/globals');
var fs = require('fs');
const moment = require('moment');
var Heartbeat = require.main.require('./models/HeartbeatModel.js');
var TestLog = require.main.require('./models/TestLogModel.js');
const templateService = require('../services/templateService');
const minioService = require('./minioService');
const { log } = require('console');
const config = require.main.require('./config/config');
var service = {
  getTestInstancePreviewModel: async function (id_test_instance) {
    let qs = await db.any(
      ` SELECT tiq.ordinal, question.id, question.is_active, question_text, tiq.answers_permutation, tiq.correct_answers_permutation
                , array_agg(answer_text ORDER BY question_answer.ordinal) as abc_answers
            FROM test_instance_question tiq
            JOIN question on tiq.id_question = question.id
            LEFT JOIN question_answer
                    ON question.id = question_answer.id_question
            WHERE id_test_instance = $(id_test_instance)
            GROUP BY tiq.ordinal, question.id, question.is_active, question_text, tiq.answers_permutation, tiq.correct_answers_permutation
            ORDER BY tiq.ordinal `,
      {
        id_test_instance: id_test_instance,
      }
    );

    for (let g = 0; g < qs.length; g++) {
      var q = qs[g];
      q.html = await utils.md2Html(q.question_text);
      var htmlAnswersPerm = [];
      if (q.abc_answers) {
        // Rearange answers to current permutation:
        for (let i = 0; q.answers_permutation && i < q.answers_permutation.length; ++i) {
          let permIdx = q.answers_permutation[i] - 1;
          htmlAnswersPerm.push({
            aHtml: await utils.md2Html(q.abc_answers[permIdx]),
            ordinal: String.fromCharCode(97 + i),
          });
        }
        let corrAnswers = [];
        for (
          let i = 0;
          q.correct_answers_permutation && i < q.correct_answers_permutation.length;
          ++i
        ) {
          corrAnswers.push(String.fromCharCode(97 + q.correct_answers_permutation[i] - 1));
        }
        q.correct_answers_permutation = corrAnswers.join(', ');
        q.abc_html = htmlAnswersPerm;
      }
    }
    return qs;
  },

  zipUploadedFiles: async function (id_test_instance, username, studAnswers) {
    if (utils.useMinio()) {
      return await minioService.zipUploadedFiles(id_test_instance, username, studAnswers);
    }
    let dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/${globals.TI_UPLOAD_FOLDER}/${id_test_instance}`;
    for (var ord = 0; ord < studAnswers.length; ++ord) {
      let srcDir = `${dir}/${ord + 1}/`;
      winston.debug('Checking dir: ' + srcDir);
      if (fs.existsSync(srcDir)) {
        let destFilename = utils.UUID.generate() + '.zip';
        // these folders MUST exist, I've created them at startup
        let yearFolder = new Date().getFullYear();
        let destZipFolder = `${global.appRoot}/${globals.PUBLIC_FOLDER}/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${yearFolder}`;
        if (!fs.existsSync(destZipFolder)) {
          winston.debug('Creating dir ' + destZipFolder);
          fs.mkdirSync(destZipFolder);
        }
        let destZipFile = `${destZipFolder}/${destFilename}`;
        winston.debug('Zipping dir ' + srcDir + ' to ' + destZipFile);
        try {
          await utils.zipDirectory(srcDir, destZipFile);
          studAnswers[ord].uploaded_files = `${yearFolder}/${destFilename}`;
          await utils.deleteFolderRecursiveSync(srcDir);
        } catch (error) {
          winston.error('Error zipping dir ' + dir + ' to ' + destZipFile);
          winston.error(error);
        }
      }
    }
    try {
      await utils.deleteFolderRecursiveSync(dir);
    } catch (error) {
      winston.error('Error deleteing dir ' + dir);
      winston.error(error);
    }
  },
  reevaluateTestAsync: async function (id_test_instance) {
    let score = await Scorer.scoreExam(id_test_instance, db);
    await service.updateTestScore(id_test_instance, score);
    return true;
  },
  gradeTest: function (id_test_instance) {
    return Scorer.scoreExam(id_test_instance, db)
      .then(function (tscore) {
        return service.updateTestScore(id_test_instance, tscore);
      })
      .then(function () {
        winston.debug('UpdateTestScore done.');
        return new Promise((resolve) => {
          resolve({
            success: true,
            async_submit: true,
          }); // this is just to match the prototype of the gradeTestSync
        });
      });
  },

  updateTestInstanceWithSubmitValues: function (id_test_instance, studAnswers) {
    winston.info('studAnswers ' + JSON.stringify(studAnswers));
    return db
      .many(
        `SELECT ordinal, has_answers, type_name
                   FROM test_instance_question tiq
                   JOIN question ON tiq.id_question = question.id
                   JOIN question_type ON question.id_question_type = question_type.id
                  WHERE id_test_instance = $(id_test_instance)
                  ORDER BY ordinal`,
        {
          id_test_instance: id_test_instance,
        }
      )
      .then(function (answerTypes) {
        var lookup = {};
        for (let i = 0; i < answerTypes.length; ++i) {
          lookup[answerTypes[i].ordinal] = answerTypes[i];
        }
        return db.tx(function (t) {
          let statements = [];
          statements.push(
            t.result(
              `UPDATE test_instance
                                 SET ts_submitted = current_timestamp
                               WHERE id =$1
                                 AND ts_submitted IS NULL`,
              [id_test_instance]
            )
          );
          for (var ord = 0; ord < studAnswers.length; ++ord) {
            if (
              lookup[ord + 1] &&
              (lookup[ord + 1].has_answers ||
                lookup[ord + 1].type_name.toUpperCase().indexOf('SCALE') >= 0)
            ) {
              // ABC question don't have uplaoded files, at least for the time being? Is there a use case?
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answers = $1::integer[]
                                  WHERE ordinal = $2
                                    AND id_test_instance =$3`,
                  [studAnswers[ord].multichoice, ord + 1, id_test_instance]
                )
              );
            } else if (lookup[ord + 1] && lookup[ord + 1].type_name === 'Free text') {
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answer_text = $1,
                                    uploaded_files = $2
                                  WHERE ordinal = $3
                                    AND id_test_instance =$4`,
                  [
                    studAnswers[ord].textAnswer || studAnswers[ord].codeAnswer.code, //TODO: remove the or part eventually
                    studAnswers[ord].uploaded_files,
                    ord + 1,
                    id_test_instance,
                  ]
                )
              );
            } else if (
              lookup[ord + 1] &&
              lookup[ord + 1].type_name.toUpperCase().indexOf('DIAGRAM') >= 0
            ) {
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answer_text = $1,
                                        uploaded_files = $2
                                  WHERE ordinal = $3
                                    AND id_test_instance =$4`,
                  [
                    studAnswers[ord].textAnswer,
                    studAnswers[ord].uploaded_files,
                    ord + 1,
                    id_test_instance,
                  ]
                )
              );
            } else {
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answer_code = $1,
                                    student_answer_code_pl=$2,
                                    uploaded_files = $3
                                  WHERE ordinal = $4
                                    AND id_test_instance =$5`,
                  [
                    studAnswers[ord].codeAnswer.code,
                    studAnswers[ord].codeAnswer.languageId,
                    studAnswers[ord].uploaded_files,
                    ord + 1,
                    id_test_instance,
                  ]
                )
              );
            }
          }
          return t.batch(statements);
        });
      });
  },
  updateTestScore: function (id_test_instance, tscore) {
    return db.tx(function (t) {
      let statements = [];
      statements.push(
        t.result(
          `
                                 UPDATE test_instance
                                    SET correct_no    = $1,
                                        incorrect_no  = $2,
                                        unanswered_no = $3,
                                        partial_no    = $4,
                                        score         = $5,
                                        passed        = $6,
                                        score_perc    = $7
                                  WHERE id =$8`,
          [
            tscore.correct_no,
            tscore.incorrect_no,
            tscore.unanswered_no,
            tscore.partial_no,
            tscore.score,
            tscore.passed,
            tscore.score_perc,
            id_test_instance,
          ]
        )
      );
      for (var ord = 0; ord < tscore.qscores.length; ++ord) {
        let qs = tscore.qscores[ord];
        statements.push(
          t.result(
            `
                                 UPDATE test_instance_question
                                    SET is_correct    = $1,
                                        is_incorrect  = $2,
                                        is_partial    = $3,
                                        is_unanswered = $4,
                                        score         = $5,
                                        score_perc    = $6,
                                        hint          = $7,
                                        c_eval_data   = $8
                                  WHERE id =$9`,
            [
              qs.is_correct,
              qs.is_incorrect,
              qs.is_partial,
              qs.is_unanswered,
              qs.score,
              qs.score_perc,
              qs.hint,
              JSON.stringify(qs.c_outcome), //THIS
              qs.id_test_instance_question,
            ]
          )
        );
      }
      return t.batch(statements);
    });
  },
  getQuestion: async function (ordinal, idTestInstance, showCorrect, showInActiveQuestion) {
    let question, programming_languages, scale_items, attachments;
    winston.debug(`getting /question/${ordinal}`);

    question = await db.one(
      `SELECT
            test.id_course,
            question.upload_files_no,
            question.required_upload_files_no,
            question.id,
            grading_model.model_name as grading_model_name,
            test_instance_question.id as id_test_instance_question,
            question_text,
            is_active,
            upload_files_no,
            required_upload_files_no,
            qt.type_name,
            has_answers,
            display_option,
            time_limit,
            CASE
              WHEN (test_instance_question.ts_started IS NOT NULL AND test_instance_question.ts_started + time_limit * INTERVAL '1 second' > CURRENT_TIMESTAMP)
                THEN CEIL(EXTRACT(EPOCH FROM (time_limit * INTERVAL '1 second' - (CURRENT_TIMESTAMP - test_instance_question.ts_started))))
              WHEN (test_instance_question.ts_started IS NOT NULL AND test_instance_question.ts_started + time_limit * INTERVAL '1 second' <= CURRENT_TIMESTAMP)
                THEN 0
              ELSE CEIL(time_limit)
			      END as time_left,
            test_instance_question.ts_started,
            correct_answers_permutation,
            answers_permutation,
            student_answers,
            student_answer_code,
            hint,
            c_eval_data,
            uploaded_files,
            student_answer_text,
            test_instance_question_manual_grade.comment as manual_comment,
            teacher.alt_id2 as grader_alt_id2,
            teacher.first_name || ' ' || teacher.last_name as grader_name,
            test_instance_question.score,
            test_instance_question.score_perc,

            check_column_mode.mode_desc,
            question_data_object.data_object,
            question_text_html,
            question_abc_answers_html
            ${
              showCorrect
                ? `
            , sql_question_answer.sql_answer,
            text_question_answer.text_answer,
            c_question_answer.c_prefix,
            c_question_answer.c_suffix,
            c_question_answer.c_source,
            c_question_answer.id_programming_language,
            json_question_answer.json_answer,
            diagram_question_answer.diagram_answer
            `
                : ''
            }

            FROM question

        JOIN test_instance_question
          ON question.id = test_instance_question.id_question
        JOIN grading_model
          ON test_instance_question.id_grading_model = grading_model.id
        JOIN test_instance
          ON test_instance_question.id_test_instance = test_instance.id
        JOIN test
          ON test_instance.id_test = test.id

        JOIN question_type qt
          ON question.id_question_type = qt.id
        LEFT JOIN ordered_element_question
               ON question.id = ordered_element_question.id_question
        LEFT JOIN question_data_object
               ON question.id = question_data_object.id_question
        LEFT JOIN test_instance_question_generated
               ON test_instance_question.id = test_instance_question_generated.id_test_instance_question

        LEFT JOIN test_instance_question_manual_grade
               ON test_instance_question.id = test_instance_question_manual_grade.id_test_instance_question
        LEFT JOIN app_user teacher
               ON test_instance_question_manual_grade.id_app_user = teacher.id

        LEFT JOIN sql_question_answer
               ON question.id = sql_question_answer.id_question
        LEFT JOIN check_column_mode
               ON sql_question_answer.id_check_column_mode = check_column_mode.id
        ${
          showCorrect
            ? `
            LEFT JOIN text_question_answer
            ON question.id = text_question_answer.id_question

            LEFT JOIN c_question_answer
            ON question.id = c_question_answer.id_question

            LEFT JOIN json_question_answer
            ON question.id = json_question_answer.id_question

            LEFT JOIN diagram_question_answer
            ON question.id = diagram_question_answer.id_question
            `
            : ''
        }

        WHERE test_instance_question.id_test_instance = $(id_test_instance)
          AND test_instance_question.ordinal = $(ordinal)
          `,
      {
        id_test_instance: idTestInstance,
        ordinal: ordinal,
      }
    );
    let abcAnswers;
    let answersStatic;
    let answersDynamic;
    let diagram_question_answer;
    if (question.has_answers === true && question.type_name !== 'ConnectedElements') {
      abcAnswers = await db.any(
        `SELECT is_correct, answer_text, ordinal
          FROM question_answer
         WHERE id_question = $(id_question)
         ORDER BY ordinal`,
        {
          id_question: question.id,
        }
      );
    } else if (question.type_name === 'ConnectedElements') {
      answersDynamic = await db.any(
        `SELECT answer_text_dynamic, ordinal
          FROM connected_elements_question_answer
         WHERE id_question = $(id_question)
         ORDER BY ordinal`,
        {
          id_question: question.id,
        }
      );

      answersStatic = await db.any(
        `SELECT answer_text_static, ordinal
          FROM connected_elements_question_answer
         WHERE id_question = $(id_question)
           AND (trim(answer_text_static) <> '') IS TRUE  -- only those that have static answers, bcs there can be more dynamic than static answers
         ORDER BY ordinal`,
        {
          id_question: question.id,
        }
      );
    } else if (question.type_name.toUpperCase().indexOf('DIAGRAM') >= 0) {
      diagram_question_answer = await db.oneOrNone(
        `SELECT initial_diagram_answer
          FROM diagram_question_answer
         WHERE id_question = $(id_question)`,
        {
          id_question: question.id,
        }
      );
    }

    let html;
    if (question.data_object) {
      if (question.question_text_html) {
        html = question.question_text_html;
        for (let i = 0; abcAnswers && i < abcAnswers.length; ++i) {
          abcAnswers[i].answer_text = question.question_abc_answers_html[i];
        }
      } else {
        let rv = await templateService.generateAndSave(
          question.id_course,
          question.id_test_instance_question,
          question.question_text,
          abcAnswers, // will be updated in-place, by ref
          question.data_object
        );
        html = rv.html_text;
        // not necesary, updated in place:
        // abcAnswers = rv.abc_answers;
      }
    } else {
      html = await utils.md2Html(question.question_text);
    }

    if (question.has_answers === true) {
      var htmlAnswersPermABC = [];
      var htmlAnswersPermStatic = [];
      var htmlAnswersPermDynamic = [];

      if (question.type_name === 'ConnectedElements') {
        // Static answers are always in the same order, dynamic answers are in random order
        for (let j = 0; j < answersStatic.length; j++) {
          htmlAnswersPermStatic.push({
            aHtml: await utils.md2Html(answersStatic[j].answer_text_static),
            ordinal: j + 1,
          });
        }

        const dynamicArray = [...question.answers_permutation];
        for (let j = 0; j < dynamicArray.length; j++) {
          let permIdx = dynamicArray[j] - 1;
          htmlAnswersPermDynamic.push({
            aHtml: await utils.md2Html(answersDynamic[permIdx].answer_text_dynamic),
            ordinal: j + 1,
          });
        }
      } else {
        // Rearange answers to current permutation:
        for (
          let i = 0;
          question.answers_permutation && i < question.answers_permutation.length;
          ++i
        ) {
          let permIdx = question.answers_permutation[i] - 1;
          htmlAnswersPermABC.push({
            aHtml: await utils.md2Html(abcAnswers[permIdx].answer_text),
            // aHtml: abcAnswers[permIdx].answer_text,
            ordinal: i + 1,
          });
        }
      }
    } else if (
      question.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
      question.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
      question.type_name.toUpperCase().indexOf('CODE') >= 0 ||
      question.type_name.toUpperCase().startsWith('COMPLEX')
    ) {
      programming_languages = await db.any(
        `SELECT programming_language.id, name, hello_world
           FROM question_programming_language
           JOIN programming_language
             ON question_programming_language.id_programming_language = programming_language.id
            AND question_programming_language.id_question = $(id_question)
          ORDER BY programming_language.id`,
        {
          id_question: question.id,
        }
      );
    } else if (question.type_name.toUpperCase().indexOf('SCALE') >= 0) {
      scale_items = await db.any(
        ` SELECT scale_item.value, label
              FROM question_scale
              JOIN scale
                ON question_scale.id_scale = scale.id
              JOIN scale_item
                ON scale.id = scale_item.id_scale
              WHERE id_question = $(id_question)
              ORDER BY ordinal`,
        {
          id_question: question.id,
        }
      );
    }

    attachments = await db.any(
      `SELECT filename, label, is_public
           FROM question_attachment
          WHERE id_question = $(id_question)`,
      {
        id_question: question.id,
      }
    );

    if (showCorrect) {
      if (question.type_name.toUpperCase().indexOf('SQL') >= 0) {
        question.code_answer = question.sql_answer;
      } else if (question.type_name.toUpperCase().indexOf('JSON') >= 0) {
        question.code_answer = question.json_answer;
      } else if (question.type_name.toUpperCase().indexOf('FREE TEXT') >= 0) {
        question.code_answer = await utils.md2Html(question.text_answer);
      } else {
        question.code_answer = question.c_source;
        // Don't merge prefix and suffix, we don't want students to see that
      }
    }
    return {
      success: true,
      id: question.id,
      gradingModelName: question.grading_model_name,
      upload_files_no: question.upload_files_no,
      required_upload_files_no: question.required_upload_files_no,
      tempUploadedFileNames:
        question.upload_files_no > 0
          ? await this.getTempUploadedFilesForQuestion(
              idTestInstance,
              ordinal,
              question.upload_files_no
            )
          : [],
      ordinal: ordinal,
      type: question.type_name,
      content: question.is_active
        ? html
        : showInActiveQuestion
        ? `<h1>Not an active question</h1>${html}`
        : 'Not an active question',
      answersStatic: htmlAnswersPermStatic,
      answersDynamic: htmlAnswersPermDynamic,
      answers: question.is_active
        ? htmlAnswersPermABC
        : showInActiveQuestion
        ? htmlAnswersPermABC
        : [],
      displayOption: question.display_option,
      timeLimit: question.time_limit,
      timeLeft: question.time_left,
      correctAnswers: showCorrect ? question.correct_answers_permutation : undefined,
      studentAnswers: showCorrect ? question.student_answers : undefined,
      studentAnswerCode: showCorrect ? question.student_answer_code : undefined,
      studentAnswerText: question.student_answer_text,
      correctAnswerCode: showCorrect ? question.code_answer : undefined,
      initialDiagramAnswer:
        diagram_question_answer && diagram_question_answer.initial_diagram_answer
          ? diagram_question_answer.initial_diagram_answer
          : undefined,
      correctAnswerDiagram: question.diagram_answer,
      programmingLanguages: programming_languages,
      modeDesc: question.mode_desc,
      hint: showCorrect ? question.hint : undefined,
      multiCorrect:
        question.correct_answers_permutation && question.correct_answers_permutation.length > 1,
      c_eval_data: question.c_eval_data,
      attachments: attachments,
      uploaded_files: question.uploaded_files
        ? `/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${question.uploaded_files}`
        : undefined,
      scaleItems: question.type_name.toUpperCase().indexOf('SCALE') >= 0 ? scale_items : undefined,
      manualComment: question.manual_comment,
      graderImage: question.grader_alt_id2
        ? utils.getTinyImageHtml(question.grader_alt_id2)
        : undefined,
      graderName: question.grader_name,
      score: question.score,
      scorePerc: question.score_perc,
    };
  },

  async getTempUploadedFilesForQuestion(id_test_instance, ordinal, upload_files_no) {
    let rv = new Array(upload_files_no);
    try {
      if (utils.useMinio()) {
        let prefix = `${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${ordinal}`;
        rv = await minioService.getObjectsInBucket(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, prefix);
        rv = rv.map((obj) => obj.name.split('/').pop());
      } else {
        var dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${ordinal}/`;
        if (fs.existsSync(dir)) {
          let files = fs.readdirSync(dir);
          for (let i = 0; files && i < files.length; ++i) {
            if (files[i].startsWith('upload_')) {
              let idx = files[i].split('_')[1];
              rv[idx] = files[i];
            }
          }
        }
      }
      rv = rv.map((filename) => {
        let arr = filename.split('_');
        if (arr.length > 2) {
          arr.shift();
          arr.shift();
          return arr.join('_');
        } else {
          return filename;
        }
      });
    } catch (error) {
      winston.error(error);
    }
    return rv;
  },

  getRunStats: async function (id_test_instance) {
    return db.manyOrNone(
      `WITH run_per_instance AS (
                SELECT id_test_instance, count(test_instance_question_run.id) as cnt, max(ts_submitted) as max_ts_submitted
                  FROM test_instance_question
             LEFT JOIN test_instance_question_run
                    ON id_test_instance_question  = test_instance_question.id
                 WHERE id_test_instance = $(id_test_instance)
              GROUP BY id_test_instance
                )
            SELECT tiq.id, tiq.ordinal, count(*) as qcnt, run_per_instance.cnt as tcnt,
                    greatest (0,
                        CASE WHEN (qpace.id is null) then 0
                        else
                            CASE WHEN qpace.timeouts[1 + COUNT(*)] is null THEN qpace.timeouts[array_upper(qpace.timeouts, 1)]
                            ELSE qpace.timeouts[1 + COUNT(*)]
                            END

                        end,
                        CASE WHEN (tpace.id is null) then 0
                        else

                            CASE WHEN tpace.timeouts[1 +run_per_instance.cnt] is null THEN tpace.timeouts[array_upper(tpace.timeouts, 1)]
                            ELSE tpace.timeouts[1 + run_per_instance.cnt]
                            END
                        end
                        ) as timeout,
                        (CASE WHEN (tpace.id IS NOT NULL) THEN max_ts_submitted ELSE MAX(run.ts_submitted) END) +  greatest (0,
                            CASE WHEN (qpace.id is null) then 0
                            else
                                CASE WHEN qpace.timeouts[1 + COUNT(*)] is null THEN qpace.timeouts[array_upper(qpace.timeouts, 1)]
                                ELSE qpace.timeouts[1 + COUNT(*)]
                                END

                            end,
                            CASE WHEN (tpace.id is null) then 0
                            else

                                CASE WHEN tpace.timeouts[1 +run_per_instance.cnt] is null THEN tpace.timeouts[array_upper(tpace.timeouts, 1)]
                                ELSE tpace.timeouts[1 + run_per_instance.cnt]
                                END
                            end
                        )  * INTERVAL '1 second' <= now() as can_run,
                        extract(epoch from (
                            (CASE WHEN (tpace.id IS NOT NULL) THEN max_ts_submitted ELSE MAX(run.ts_submitted) END) +
                            greatest (0,
                                CASE WHEN (qpace.id is null) then 0
                                else
                                    CASE WHEN qpace.timeouts[1 + COUNT(*)] is null THEN qpace.timeouts[array_upper(qpace.timeouts, 1)]
                                    ELSE qpace.timeouts[1 + COUNT(*)]
                                    END

                                end,
                                CASE WHEN (tpace.id is null) then 0
                                else

                                    CASE WHEN tpace.timeouts[1 +run_per_instance.cnt] is null THEN tpace.timeouts[array_upper(tpace.timeouts, 1)]
                                    ELSE tpace.timeouts[1 + run_per_instance.cnt]
                                    END
                                end
                            )  * INTERVAL '1 second' - now())) as time_wait

                        FROM test_instance
                JOIN test_instance_question tiq
                ON test_instance.id = tiq.id_test_instance
                JOIN test_pace
                ON test_instance.id_test = test_pace.id_test
                JOIN run_per_instance
                ON test_instance.id = run_per_instance.id_test_instance

                LEFT JOIN test_instance_question_run run
                       ON run.id_test_instance_question = tiq.id
                LEFT JOIN pace qpace
                    ON test_pace.id_pace_question = qpace.id
                LEFT JOIN pace tpace
                    ON test_pace.id_pace_test = tpace.id
                WHERE test_instance.id = $(id_test_instance)
                GROUP BY test_instance.id, tiq.id, tiq.ordinal, run_per_instance.cnt, run_per_instance.max_ts_submitted, qpace.id, qpace.timeouts, tpace.id, tpace.timeouts
                ORDER BY tiq.ordinal`,
      {
        id_test_instance: id_test_instance,
      }
    );
  },
  getStalkData: async function (id_test, id_academic_year) {
    var promises = [];
    var rs = await db.any(
      `
                SELECT student.id,
                       test.id as id_test,
                       test_instance.id as id_test_instance,
                       student.first_name || ' ' || last_name AS full_name,
                       alt_id2,
                       test.title,
                       ts_started :: timestamp(0) :: varchar,
                       (current_timestamp - ts_started)::interval(0)::varchar as duration,
                       ip_address,
                       coalesce(room, 'unknown') as room
                FROM student
                JOIN student_course ON student.id = student_course.id_student
                JOIN test ON student_course.id_course = test.id_course
                 AND student_course.id_academic_year = $(id_academic_year) --BCS global tests: test.id_academic_year
                JOIN test_instance ON test_instance.id_student = student.id
                 AND test_instance.id_test = test.id
                LEFT JOIN computer ON ip_address = computer.ip
                WHERE id_test = $(id_test)
                  AND ts_submitted IS NULL
                ORDER BY score DESC`,
      {
        id_test: id_test,
        id_academic_year,
      }
    );
    promises.push(
      Heartbeat.find({
        //id_test: id_test,  because of maxrun>1 exams, can have a bunch of test-instances/hbs
        _id: {
          $in: rs.map((x) => x.id_test_instance),
        },
      }).exec()
    );
    promises.push(
      TestLog.find({
        _id: {
          $in: rs.map((x) => x.id_test_instance),
        },
        'events.eventName': 'Lost focus',
      }).exec()
    );
    promises.push(
      db.oneOrNone(
        `SELECT '/stalk/' || course_acronym || '/' || test.title_abbrev AS url
                FROM test
                JOIN course
                  ON test.id_course = course.id
               WHERE test.id = $1 AND allow_anonymous_stalk`,
        [id_test]
      )
    );
    promises.push(
      db.manyOrNone(
        `SELECT ip, room
                FROM computer`
      )
    );
    return Promise.all(promises).then(
      function (args) {
        // returned data is in arguments[0], arguments[1], ... arguments[n]
        // you can process it here
        //let rs = args[0];
        let hbs = args[0];
        // console.log('hbs', JSON.stringify(hbs, null, 2));
        let lfs = args[1];
        let ip2Room = {};
        for (let computer of args[3]) {
          ip2Room[computer.ip] = computer.room;
        }
        let rooms = {};
        rs.forEach(function (row) {
          row.imgHtml = utils.getImageHtml(row.alt_id2);
          if (!rooms[row.room]) {
            rooms[row.room] = 1;
          }
          row.lfs = 0;
          for (let i = 0; i < lfs.length; ++i) {
            if (row.id_test_instance === lfs[i]._id) {
              if (lfs[i].events) {
                row.lfs = 0; //lfs[i].events.filter(x => x.eventName === 'Lost focus').length;
                let prev = null;
                let max = 0,
                  sum = 0;
                for (let idx = 0; idx < lfs[i].events.length; ++idx) {
                  if (lfs[i].events[idx].eventName === 'Lost focus') {
                    row.lfs++;
                    prev = lfs[i].events[idx];
                  } else if (lfs[i].events[idx].eventName === 'Got focus' && prev) {
                    let diff = lfs[i].events[idx].mongoTs - prev.mongoTs;
                    if (diff > max) max = diff;
                    sum += diff;
                    prev = null;
                  }
                }
                row.lfsTotal =
                  Math.floor(moment.duration(sum).asHours()) + moment.utc(sum).format(':mm:ss');
                row.lfsMax =
                  Math.floor(moment.duration(max).asHours()) + moment.utc(max).format(':mm:ss');
                //row.lfsAvg = row.lfs ?  Math.floor(moment.duration(sum / row.lfs).asHours()) + moment.utc(sum / row.lfs).format(':mm:ss') : 0;
                if (prev) {
                  let ms = moment().diff(moment(prev.mongoTs, 'DD/MM/YYYY HH:mm:ss'));
                  let d = moment.duration(ms);
                  row.lfsOn = Math.floor(d.asHours()) + moment.utc(ms).format(':mm:ss');
                }
              }

              break;
            }
          }

          for (let i = 0; i < hbs.length; ++i) {
            if (row.id_test_instance == hbs[i]._id) {
              row.hbCount = hbs[i].beats.length;
              let lastBeat = hbs[i].beats[row.hbCount - 1];
              if (lastBeat.ip) {
                if (row.ip_address != lastBeat.ip) {
                  row.ip_address += `/${lastBeat.ip} (cnt=${
                    new Set(hbs[i].beats.map((x) => x.ip)).size
                  })`;
                }
                row.room = ip2Room[lastBeat.ip] || 'unknown';
                if (!rooms[row.room]) {
                  rooms[row.room] = 1;
                }
              }
              if (lastBeat.ts) lastBeat = lastBeat.ts;
              var ms = moment().diff(moment(lastBeat, 'DD/MM/YYYY HH:mm:ss'));
              var d = moment.duration(ms);
              if (d / 1000 < 100) {
                row.hbColor = 'red';
              } else if (d / 1000 < 500) {
                row.hbColor = 'yellow';
              } else {
                row.hbColor = 'gray';
              }
              var s = Math.floor(d.asHours()) + moment.utc(ms).format(':mm:ss');
              row.lastHbAgo = s;
              row.progress = hbs[i].progress + '%';
              row.d = d;
              break;
            }
          }
        });
        rs.sort(function (a, b) {
          if (a.lfs !== b.lfs) {
            return b.lfs - a.lfs;
          } else if (a.d && b.d) {
            return a.d - b.d;
          } else {
            return 0;
          }
        });
        return {
          rs: rs,
          rooms: Object.keys(rooms),
          publicStalkUrl: args[2] ? args[2].url : null,
        };
      },
      function (err) {
        winston.error(err);
        return {
          rs: [],
          rooms: [],
        };
      }
    );
  },
};

module.exports = service;
