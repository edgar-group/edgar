'use strict';
const config = require.main.require('./config/config');
const db = require('../db').db;
const rscript = require('js-call-r');
const path = require('path');

const queries = {
  adaptivityModels: `SELECT id as value, name
    FROM adaptivity_model`,
  nodes: `SELECT DISTINCT node.id as value, node_name, type_name,
    '[' || type_name || '] ' || node_name as name
    FROM v_roots_children join node on id_child = node.id
    JOIN node_type on node.id_node_type = node_type.id
    WHERE id_root = (select id_root_node from course where id = $(id_course))
    ORDER BY node_name;`,
  selectedNodes: `SELECT id_node
    FROM exercise_node
    WHERE id_exercise = $(id_exercise)`,
  exercise: `SELECT *
    FROM exercise
    WHERE id = $(id_exercise)`,
  questions: `SELECT DISTINCT question.id, version, 
    CASE WHEN (question.is_active)  THEN '&#10003;' ELSE '&#10008;' END AS is_active,
    LEFT(question_text, 512) AS question_text,
    to_char(question.ts_created, 'YYYY-MM-DD HH24:MI:SS') AS ts_created_sl,
    question_type.type_name,
    COUNT(id_student) AS no_answers,
		ROUND(100. * SUM(is_correct) / COUNT(id_student), 2) AS correct_percentage,
    question_assigned_difficulty.id AS question_difficulty_id,
    id_difficulty_level,
    substring(app_user_diff.first_name from 1 for 1) || '. ' || app_user_diff.last_name 
        AS user_modified_difficulty,
    to_char(question_assigned_difficulty.ts_modified, 'YYYY-MM-DD HH24:MI:SS') 
        AS assigned_difficulty_ts_modified,
    qcd_1.difficulty difficulty_1, qcd_1.difficulty_std_err difficulty_std_err_1, 
    qcd_2.difficulty difficulty_2, qcd_2.difficulty_std_err difficulty_std_err_2, 
    qcd_2.discriminating_power discriminating_power_2,
    qcd_2.discriminating_power_std_err discriminating_power_std_err_2
    FROM v_exercise_question 
    JOIN question ON question.id = v_exercise_question.id_question
    JOIN question_type ON id_question_type = question_type.id
    LEFT JOIN v_student_question_instance_correct
        ON v_student_question_instance_correct.id_question = question.id
    LEFT JOIN question_assigned_difficulty 
        ON question_assigned_difficulty.id_question = question.id
        AND question_assigned_difficulty.id_adaptivity_model = $(id_adaptivity_model)
    LEFT JOIN app_user app_user_diff 
        ON app_user_diff.id = question_assigned_difficulty.id_user_modified
    LEFT JOIN question_computed_difficulty qcd_1
        ON question.id = qcd_1.id_question
        AND qcd_1.discriminating_power IS NULL
    LEFT JOIN question_computed_difficulty qcd_2
        ON question.id = qcd_2.id_question
        AND qcd_2.discriminating_power IS NOT NULL
    LEFT JOIN question_difficulty_computation qdc_1
        ON qdc_1.id = qcd_1.id_question_difficulty_computation
        AND qdc_1.id_exercise = $(id_exercise)
    LEFT JOIN question_difficulty_computation qdc_2
        ON qdc_2.id = qcd_2.id_question_difficulty_computation
        AND qdc_2.id_exercise = $(id_exercise)
    WHERE v_exercise_question.id_exercise = $(id_exercise)
    GROUP BY question.id, version, question.is_active, question_text, 
        question_type.type_name, id_difficulty_level, 
        app_user_diff.first_name, app_user_diff.last_name, 
        assigned_difficulty_ts_modified, question_assigned_difficulty.id,
        difficulty_1, difficulty_std_err_1, difficulty_2, difficulty_std_err_2, 
        discriminating_power_2,discriminating_power_std_err_2`,
  questionDifficultyLevels: `SELECT id, name, ordinal, rgb_color
    FROM question_difficulty_level
    WHERE id_adaptivity_model = $(id_adaptivity_model)
    ORDER BY ordinal`,
};

async function listExercises(courseId, academicYearId, studentId) {
  return await db.any(
    `SELECT exercise.id, title, description, is_active,
                exercise_student.ts_modified AS last_activity,
                COUNT(DISTINCT exercise_student.id_student) AS no_students,
                COUNT(v_exercise_question) AS no_questions
            FROM exercise LEFT JOIN v_exercise_question 
                ON exercise.id = v_exercise_question.id_exercise
            LEFT JOIN exercise_student
                ON exercise.id = exercise_student.id_exercise
                AND exercise_student.id_student = $(id_student)
            WHERE exercise.id_course = $(id_course)
                AND id_academic_year = $(id_academic_year)
            GROUP BY exercise.id, title, description, 
                exercise_student.ts_modified
            ORDER BY title`,
    {
      id_course: courseId,
      id_academic_year: academicYearId,
      id_student: studentId,
    }
  );
}

function getExerciseData(id, courseId) {
  return db
    .task((t) =>
      t.batch([
        t.any(queries.adaptivityModels),
        t.any(queries.nodes, {
          id_course: courseId,
        }),
        t.any(queries.selectedNodes, {
          id_exercise: id,
        }),
        t.one(queries.exercise, {
          id_exercise: id,
        }),
      ])
    )
    .then((data) => ({
      adaptivity_models: data[0],
      nodes: data[1],
      selected_nodes: data[2].map((n) => parseInt(n.id_node)),
      exercise: data[3],
    }));
}

function getExerciseQuestions(id) {
  return db
    .one(
      `SELECT title, id_adaptivity_model
        FROM exercise
        WHERE id = $(id)`,
      {
        id: id,
      }
    )
    .then((exerciseData) => {
      return db
        .task((t) =>
          t.batch([
            t.any(queries.questions, {
              id_exercise: id,
              id_adaptivity_model: exerciseData.id_adaptivity_model,
            }),
            t.any(queries.questionDifficultyLevels, {
              id_adaptivity_model: exerciseData.id_adaptivity_model,
            }),
          ])
        )
        .then((data) => ({
          questions: data[0],
          difficultyLevels: data[1],
        }));
    });
}

async function saveAssignedQuestionDifficulties(exerciseId, difficulties, appUserId) {
  const adaptivityModelId = (
    await db.one(
      `SELECT id_adaptivity_model
        FROM exercise
        WHERE id = $(id)`,
      {
        id: exerciseId,
      }
    )
  ).id_adaptivity_model;

  return db.tx((t) =>
    t.batch(
      difficulties.map((d) =>
        d.id
          ? d.id_difficulty_level !== ''
            ? db.none(
                `UPDATE public.question_assigned_difficulty
                            SET id_difficulty_level = $(id_difficulty_level),
                            id_user_modified = $(id_user_modified)
                            WHERE id = $(id)
                                AND id_adaptivity_model = $(id_adaptivity_model)`,
                {
                  id_difficulty_level: d.id_difficulty_level,
                  id_user_modified: appUserId,
                  id: d.id,
                  id_adaptivity_model: adaptivityModelId,
                }
              )
            : db.none(
                `DELETE FROM public.question_assigned_difficulty
                            WHERE id = $(id)`,
                {
                  id: d.id,
                }
              )
          : d.id_difficulty_level !== '' &&
            db.none(
              `INSERT INTO public.question_assigned_difficulty (id_question,
                        id_adaptivity_model, id_user_created, id_user_modified, 
                        id_difficulty_level) VALUES ($(id_question), 
                        $(id_adaptivity_model), $(id_app_user), $(id_app_user), 
                        $(id_difficulty_level))`,
              {
                id_question: d.id_question,
                id_adaptivity_model: adaptivityModelId,
                id_app_user: appUserId,
                id_difficulty_level: d.id_difficulty_level,
              }
            )
      )
    )
  );
}

async function computeQuestionDifficulties(exerciseId, minAnswers, modelParamsNumber, appUserId) {
  const computationId = parseInt(
    (
      await db.one(
        `INSERT INTO public.question_difficulty_computation (
            id_exercise, id_user_created, no_model_params, ts_started)
            VALUES($(id_exercise), $(id_user_created), $(no_model_params), $(ts_started))
            RETURNING id`,
        {
          id_exercise: exerciseId,
          id_user_created: appUserId,
          no_model_params: modelParamsNumber,
          ts_started: new Date(),
        }
      )
    ).id
  );

  console.time('preparing-data');
  const categoriesSql = `SELECT v_eq.id_question
    FROM v_student_question_instance_answer v_sqia JOIN v_exercise_question v_eq
        ON v_sqia.id_question = v_eq.id_question
    JOIN test_instance_question tiq ON tiq.id_question = v_eq.id_question
    WHERE id_exercise = $(id_exercise)
    GROUP BY v_eq.id_question
    HAVING COUNT(CASE WHEN tiq.is_correct THEN 1 END) != COUNT(v_eq.id_question)
        AND COUNT(CASE WHEN tiq.is_correct THEN 1 END) != 0
        AND COUNT(tiq.*) >= $(min_answers)
    ORDER BY id_question`;
  const crosstabCategories = await db.many(categoriesSql, {
    id_exercise: exerciseId,
    min_answers: minAnswers,
  });
  const csvFilePath = `${config.tmpPath}/edgar-irt-input-${+new Date()}.csv`;
  await db.none(
    `COPY (
                SELECT ${crosstabCategories.map((c) => `"${c.id_question}"`).join(', ')}
                FROM crosstab(
                    'SELECT DISTINCT v_sqia.*
                    FROM v_student_question_instance_answer v_sqia 
                    JOIN v_exercise_question v_eq
                        ON v_sqia.id_question = v_eq.id_question
                    WHERE id_exercise = $(id_exercise)
                    ORDER BY id_student',
                    '${categoriesSql}')
                AS pivotTable(id_student int, 
                    ${crosstabCategories.map((c) => `"${c.id_question}" int`).join(', ')})
                ORDER BY id_student
            ) TO $(path) WITH CSV DELIMITER $(delimiter)`,
    {
      id_exercise: parseInt(exerciseId),
      min_answers: minAnswers,
      path: csvFilePath,
      delimiter: ',',
    }
  );
  console.timeEnd('preparing-data');

  console.time('irt-processing');
  const scriptPath = path.join(
    __dirname,
    'r-scripts',
    `dichotomous-${modelParamsNumber}-param-model.R`
  );
  const results = rscript.callSync(scriptPath, {
    file: csvFilePath,
  });
  console.timeEnd('irt-processing');

  if (results.error) {
    throw new Error(results.error);
  }

  const difficulties = results.coefficients.slice(
    0,
    results.coefficients.length / modelParamsNumber
  );
  let discriminatingPowers = [];

  if (modelParamsNumber >= 2)
    discriminatingPowers = results.coefficients.slice(results.coefficients.length / 2);

  await db.task((t) =>
    t.batch([
      crosstabCategories.map((q, i) =>
        db.none(
          `DELETE FROM public.question_computed_difficulty qcd
                    USING question_difficulty_computation qdc
                    WHERE qcd.id_question_difficulty_computation = qdc.id
                        AND id_question = $(id_question)
                        AND id_exercise = $(id_exercise)
                        AND discriminating_power ${
                          modelParamsNumber == 2 ? 'IS NOT NULL' : 'IS NULL'
                        }`,
          {
            id_question: q.id_question,
            id_exercise: exerciseId,
          }
        )
      ),
    ])
  );

  await db.task((t) =>
    t.batch([
      db.none(
        `UPDATE public.question_difficulty_computation
                SET ts_finished=$(ts_finished), 
                    no_questions=$(no_questions), 
                    min_difficulty=$(min_difficulty), 
                    min_discriminating_power=$(min_discriminating_power), 
                    mean_difficulty=$(mean_difficulty),
                    mean_discriminating_power=$(mean_discriminating_power), 
                    max_difficulty=$(max_difficulty), 
                    max_discriminating_power=$(max_discriminating_power),
                    results_json=$(results)
                WHERE id = $(id)`,
        {
          id: computationId,
          ts_finished: new Date(),
          no_questions: results.coefficients.length / 2,
          min_difficulty: Math.min(...difficulties.map((d) => d.value)),
          min_discriminating_power:
            modelParamsNumber >= 2 ? Math.min(...discriminatingPowers.map((d) => d.value)) : null,
          mean_difficulty:
            difficulties.reduce((sum, current) => sum + current.value, 0) / difficulties.length,
          mean_discriminating_power:
            modelParamsNumber >= 2
              ? discriminatingPowers.reduce((sum, current) => sum + current.value, 0) /
                discriminatingPowers.length
              : null,
          max_difficulty: Math.max(...difficulties.map((d) => d.value)),
          max_discriminating_power:
            modelParamsNumber >= 2 ? Math.max(...discriminatingPowers.map((d) => d.value)) : null,
          results: JSON.stringify(results),
        }
      ),
      crosstabCategories.map((q, i) =>
        db.none(
          `INSERT INTO public.question_computed_difficulty(
                        id_question_difficulty_computation, id_question, 
                        difficulty, difficulty_std_err, 
                        discriminating_power, discriminating_power_std_err)
                    VALUES ($(id_qdc), $(id_question), $(difficulty), 
                        $(difficulty_std_err), $(discriminating_power),
                        $(discriminating_power_std_err))`,
          {
            id_qdc: computationId,
            id_question: q.id_question,
            difficulty: difficulties[i].value,
            difficulty_std_err: difficulties[i]['std.err'],
            discriminating_power: modelParamsNumber >= 2 ? discriminatingPowers[i].value : null,
            discriminating_power_std_err:
              modelParamsNumber >= 2 ? discriminatingPowers[i]['std.err'] : null,
          }
        )
      ),
    ])
  );

  return results;
}

async function deleteQuestionDifficultyComputation(exerciseId, modelParamsNumber) {
  const computationIds = await db.any(
    `SELECT id
        FROM question_difficulty_computation qdc
        WHERE qdc.id_exercise = $(id_exercise)
        AND no_model_params = $(no_model_params)`,
    {
      id_exercise: exerciseId,
      no_model_params: modelParamsNumber,
    }
  );

  computationIds.forEach(async (cId) => {
    await db.tx((t) =>
      t.batch([
        db.none(
          `DELETE FROM public.question_difficulty_computation
                WHERE id = $(id)`,
          {
            id: parseInt(cId.id),
          }
        ),
        db.none(
          `DELETE FROM public.question_computed_difficulty qcd
                WHERE qcd.id_question_difficulty_computation = $(id_qdc)`,
          {
            id_qdc: parseInt(cId.id),
          }
        ),
      ])
    );
  });

  return computationIds;
}

module.exports = {
  listExercises: listExercises,
  getExerciseData: getExerciseData,
  getExerciseQuestions: getExerciseQuestions,
  saveAssignedQuestionDifficulties: saveAssignedQuestionDifficulties,
  computeQuestionDifficulties: computeQuestionDifficulties,
  deleteQuestionDifficultyComputation: deleteQuestionDifficultyComputation,
  queries: queries,
};
