'use strict';
var db = require('../db').db;
var winston = require('winston');
var utils = require('../common/utils');
var safeEval = require('safe-eval');
var utils = require('../common/utils');
const Handlebars = require('handlebars');
var TemplateLog = require.main.require('./models/TemplateLogModel.js');

function merge() {
  // taken from: https://attacomsian.com/blog/javascript-merge-objects
  // create a new object
  let target = {};

  // deep merge the object into the target object
  const merger = (obj) => {
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
          // if the property is a nested object
          target[prop] = merge(target[prop], obj[prop]);
        } else {
          // for regular property
          target[prop] = obj[prop];
        }
      }
    }
  };

  // iterate through all objects and
  // deep merge them with target
  for (let i = 0; i < arguments.length; i++) {
    merger(arguments[i]);
  }

  return target;
}

var service = {
  evalDataObject: async function (id_course, data_object) {
    let log;
    try {
      let parents = await db.any(
        `SELECT data_object from global_data_object
                UNION ALL
                SELECT data_object from course where course.id =$(id_course) AND data_object IS NOT NULL`,
        {
          id_course,
        }
      );
      let masterObject = {};
      log = '';
      for (let i = 0; i < parents.length; ++i) {
        log += '\nParsing:\n' + parents[i].data_object;
        var evaluated = safeEval(parents[i].data_object, masterObject);
        // console.log(JSON.stringify(evaluated));
        // console.log(evaluated);
        masterObject = merge(masterObject, evaluated);
      }
      // console.log("master object");
      // console.log(JSON.stringify(masterObject));
      // console.log(masterObject);
      data_object = data_object || '{}';
      log += '\nParsing:\n' + data_object;
      let dataObject = safeEval(data_object, masterObject);
      // console.log("data object");
      Object.assign(dataObject, masterObject);
      if (dataObject.init) {
        dataObject.init();
        log += '\nInvoking init() function...';
      } else {
        log += '\nWarning: init() function NOT present.';
      }
      // console.log(JSON.stringify(dataObject));
      // console.log(dataObject);
      return {
        data_object: dataObject,
        log,
      };
    } catch (error) {
      winston.error(error);
      return {
        error: '' + error,
        data_object: '' + error,
        log,
      };
    }
  },
  async compileTemplates(id_course, mds, data_object) {
    let log = 'COMPILE LOG at ' + utils.getCurrentTimestamp(true);
    try {
      log += '\nEvaulating data object... (see log in Template data object section)';
      let obj = await this.evalDataObject(id_course, data_object);
      // if not error...
      //  Now I can evaluate templates in order with the SAME data object,
      //   even if some template modifies the object the new value will be applied to the templates downstream
      let htmls = [];
      for (let i = 0; i < mds.length; ++i) {
        log += '\nConverting MD to HTML...:\n';
        let html = await utils.md2Html(mds[i]);
        log += '\nCompiling HTML:\n' + html;
        const template = Handlebars.compile(html);
        log += '\nRendering template using data object...';
        html = template(obj.data_object);
        htmls.push(html);
        log += '\nRendered:\n' + html;
        log += '\nData object is:\n' + JSON.stringify(obj.data_object);
      }

      return {
        log,
        htmls,
        data_object: JSON.stringify(obj.data_object),
      };
    } catch (error) {
      winston.error(error);
      winston.error({
        error: log,
      });
      return {
        log,
        html: 'Error occured generating HTML(1), please check your data object, see log for details.',
        data_object,
        error: {
          message: 'An error occured: ' + error + '.',
        },
      };
    }
  },

  async generateAndSave(
    id_course,
    id_test_instance_question,
    question_text,
    abc_answers,
    txt_data_object
  ) {
    let log = '' + utils.getCurrentTimestamp();
    try {
      let templates = [question_text];
      if (abc_answers && abc_answers.length) {
        templates = templates.concat(abc_answers.map((x) => x.answer_text));
      }
      log = `${log}\nCompiling templates for:\n${templates}`;
      let rv = await this.compileTemplates(id_course, templates, txt_data_object);
      let html_text;
      html_text = rv.htmls[0];
      if (abc_answers && abc_answers.length) {
        rv.htmls.shift();
        if (rv.htmls.length != abc_answers.length) throw 'rv.htmls.length != abc_answers.length';
        for (let i = 0; i < abc_answers.length; ++i) {
          abc_answers[i].answer_text = rv.htmls[i];
        }
      }
      log = `${log}\n${rv.log}\n\nDONE. Answers split (if any). Inserting into test_instance_question_generated...\n`;
      await db.none(
        `INSERT INTO test_instance_question_generated (id_test_instance_question, data_object, question_text_html, question_abc_answers_html)
                        VALUES ($(id_test_instance_question), $(data_object), $(question_text_html), $(question_abc_answers_html))
                        `,
        {
          id_test_instance_question,
          data_object: rv.data_object,
          question_text_html: html_text,
          question_abc_answers_html: abc_answers && abc_answers.map((x) => x.answer_text),
        }
      );
      log = `${log}\n** ALL DONE **\n${utils.getCurrentTimestamp(true)}`;
      await TemplateLog.log(id_test_instance_question, true, log);

      return {
        html_text,
        abc_answers,
      };
    } catch (error) {
      winston.error(error);
      await TemplateLog.log(id_test_instance_question, true, log, '' + error);
      return {
        html_text:
          'Error occured generating HTML(2) for tiq.id ' +
          id_test_instance_question +
          ', please check your data object, see log for details.',
        abc_answers,
        error: {
          message: '' + error,
        },
      };
    }
  },
};

module.exports = service;
