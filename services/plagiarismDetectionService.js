'use strict';
var db = require('../db').db;

// documentation http://yomguithereal.github.io/talisman/metrics/#damerau-levenshtein
const bag = require('talisman/metrics/bag');
const dice = require('talisman/metrics/dice');
const jaccard = require('talisman/metrics/jaccard');
const jaroWinkler = require('talisman/metrics/jaro-winkler');
const similarity = require('talisman/metrics/lcs').similarity;
const lig3 = require('talisman/metrics/lig').lig3;

const similarityMethods = {
  1: (a, b) => {
    return lig3(a, b);
  },
  2: (a, b) => {
    return jaroWinkler(a, b);
  },
  3: (a, b) => {
    return dice(a, b);
  },
  4: (a, b) => {
    return similarity(a, b);
  },
  5: (a, b) => {
    return 1 - bag(a, b) / Math.max(a.length, b.length);
  },
  6: (a, b) => {
    return jaccard(a, b);
  },
};

async function calculateSimilarity(
  data,
  questions,
  studentsPerQuestion,
  similarityMethod,
  cutoffPercentage,
  recalculate,
  examId,
  testId
) {
  let similarityPerQuestion = {};
  let dataPerQuestion = {};
  let colorPerQuestion = {};

  // check if data already exists in the database
  let row = await db.any(`
        SELECT 
            similarity_per_question,
            data_per_question, 
            students_per_question,
            questions,
            color_per_question
        FROM plag_detection_data
        WHERE id_plag_detection_algorithm = ${testId} AND id_test = ${examId};`);

  // if there is no data, or recalculate is on, calculate the data
  if (row.length == 0 || recalculate == 'true') {
    for (let i in questions) {
      let question = questions[i];
      let questionData = data[question];
      let students = studentsPerQuestion[question];
      similarityPerQuestion[question] = {};
      dataPerQuestion[question] = {
        averageSimilarity: 0,
        minSimilarity: undefined,
        maxSimilarity: undefined,
        studentsTotal: students.length,
      };
      let counter = 0;
      for (let j = 0; j < students.length - 1; j++) {
        let firstStudent = students[j];
        for (let k = j + 1; k < students.length; k++) {
          let secondStudent = students[k];
          let codeFirst = questionData[firstStudent].replace(/\s+/g, '');
          let codeSecond = questionData[secondStudent].replace(/\s+/g, '');
          if (codeFirst.length && codeSecond.length) {
            let similarity = similarityMethods[similarityMethod](
              questionData[firstStudent].replace(/\s+/g, ','),
              questionData[secondStudent].replace(/\s+/g, ',')
            );
            let key = '' + firstStudent + '+' + secondStudent;
            similarityPerQuestion[question][key] =
              Math.round((similarity + Number.EPSILON) * 10000) / 100;
            dataPerQuestion[question].averageSimilarity =
              dataPerQuestion[question].averageSimilarity + similarityPerQuestion[question][key];

            if (
              dataPerQuestion[question].minSimilarity == undefined ||
              dataPerQuestion[question].minSimilarity > similarityPerQuestion[question][key]
            ) {
              dataPerQuestion[question].minSimilarity = similarityPerQuestion[question][key];
            }

            if (
              dataPerQuestion[question].maxSimilarity == undefined ||
              dataPerQuestion[question].maxSimilarity < similarityPerQuestion[question][key]
            ) {
              dataPerQuestion[question].maxSimilarity = similarityPerQuestion[question][key];
            }

            counter++;
          }
        }
      }
      dataPerQuestion[question].averageSimilarity = Math.round(
        ((dataPerQuestion[question].averageSimilarity / (counter + Number.EPSILON)) * 100) / 100
      );
      colorPerQuestion[question] = getColorPerQuestion(
        similarityPerQuestion[question],
        dataPerQuestion[question].averageSimilarity
      );
    }

    // insert or update data
    if (row.length == 0) {
      db.none(
        `
                INSERT INTO plag_detection_data (
                    id_plag_detection_algorithm, 
                    id_test, 
                    similarity_per_question, 
                    data_per_question, 
                    students_per_question, 
                    questions, 
                    color_per_question
                )
                VALUES ( $1, $2, $3, $4, $5, $6, $7 );`,
        [
          testId,
          examId,
          JSON.stringify(similarityPerQuestion),
          JSON.stringify(dataPerQuestion),
          JSON.stringify(studentsPerQuestion),
          JSON.stringify(questions),
          JSON.stringify(colorPerQuestion),
        ]
      );
    } else {
      db.any(
        `
                UPDATE plag_detection_data
                SET (
                    similarity_per_question, 
                    data_per_question,
                    students_per_question,
                    questions,
                    color_per_question
                    ) = ( $3, $4, $5, $6, $7 )
                WHERE id_plag_detection_algorithm = $1 AND id_test = $2;`,
        [
          testId,
          examId,
          JSON.stringify(similarityPerQuestion),
          JSON.stringify(dataPerQuestion),
          JSON.stringify(studentsPerQuestion),
          JSON.stringify(questions),
          JSON.stringify(colorPerQuestion),
        ]
      );
    }
  } else {
    row = row[0];
    similarityPerQuestion = JSON.parse(row['similarity_per_question']);
    dataPerQuestion = JSON.parse(row['data_per_question']);
    studentsPerQuestion = JSON.parse(row['students_per_question']);
    questions = JSON.parse(row['questions']);
    colorPerQuestion = JSON.parse(row['color_per_question']);
  }

  let cleanedData = removeNonSuspicious(
    questions,
    similarityPerQuestion,
    cutoffPercentage,
    studentsPerQuestion
  );

  return {
    similarityPerQuestion: cleanedData.newSimilarityPerQuestion,
    dataPerQuestion: dataPerQuestion,
    studentsPerQuestion: cleanedData.newStudentsPerQuestion,
    questions: questions,
    colorPerQuestion: colorPerQuestion,
    suspiciousStudents: cleanedData.allSuspiciousStudents,
  };
}

function removeNonSuspicious(
  questions,
  similarityPerQuestion,
  cutoffPercentage,
  studentsPerQuestion
) {
  let newSimilarityPerQuestion = {};
  let newStudentsPerQuestion = {};
  let allSuspiciousStudents = new Set();
  for (let question of questions) {
    let similarity = similarityPerQuestion[question];
    let suspiciousStudents = new Set();
    let newSimilarity = {};
    for (let key in similarity) {
      if (similarity[key] >= cutoffPercentage) {
        suspiciousStudents.add(key.split('+')[0]);
        suspiciousStudents.add(key.split('+')[1]);
      }
    }
    for (let elem of suspiciousStudents) {
      allSuspiciousStudents.add(elem);
    }

    for (let key in similarity) {
      if (suspiciousStudents.has(key.split('+')[0]) || suspiciousStudents.has(key.split('+')[1])) {
        newSimilarity[key] = similarity[key];
      }
    }
    newSimilarityPerQuestion[question] = newSimilarity;
    newStudentsPerQuestion[question] = Array.from(suspiciousStudents).sort();
  }
  return {
    newSimilarityPerQuestion,
    newStudentsPerQuestion,
    allSuspiciousStudents: Array.from(allSuspiciousStudents).sort(),
  };
}

function getColorPerQuestion(similarities, avgPct) {
  let res = {};
  let colorS = '87%';
  let colorL = '50%';

  let percentColors = [
    { pct: 0.0, h: 120 },
    { pct: avgPct / 100, h: 120 },
    { pct: 1.0, h: 0 },
  ];

  for (let similarity of Object.keys(similarities)) {
    let decPtc = similarities[similarity] / 100;
    let i;
    for (i = 1; i < percentColors.length - 1; i++) {
      if (decPtc < percentColors[i].pct) {
        break;
      }
    }
    let lower = percentColors[i - 1];
    let upper = percentColors[i];
    let range = upper.pct - lower.pct;
    let rangePct = (decPtc - lower.pct) / range;
    let pctLower = 1 - rangePct;
    let pctUpper = rangePct;
    let h = Math.floor(lower.h * pctLower + upper.h * pctUpper);
    res[similarity] = 'hsl(' + [h, colorS, colorL].join(',') + ')';
  }
  return res;
}

module.exports = {
  calculateSimilarity: calculateSimilarity,
  similarityMethods: similarityMethods,
  getColorPerQuestion: getColorPerQuestion,
};
