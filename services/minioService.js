'use strict';
var winston = require('winston');
var utils = require('../common/utils');
var globals = require('../common/globals');
const fs = require('fs');
const os = require('os');
const path = require('path');
const config = require.main.require('./config/config');
var unzipper = require('unzipper');
var unzip = require('unzip-stream');
var multer = require('multer');
var tmp = require('tmp');
var AdmZip = require('adm-zip');
const { Transform } = require('stream');
const minio = require('minio');
const { message } = require('gulp-typescript/release/utils');
const minioClient = utils.useMinio() ? new minio.Client(config.minioConfig) : null;

const FILE_SIZE_LIMIT = 20 * 1024 * 1024; // 50MB;

var uploadTestInstancePrivate = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: FILE_SIZE_LIMIT,
  },
}).single('testInstanceFile');

var service = {
  uploadTestInstanceFile: async function (req, res, id_test_instance) {
    uploadTestInstancePrivate(req, res, async function (err) {
      if (err) {
        // An error occurred when uploading
        winston.error('error! ' + err);
        if (err.code === 'LIMIT_FILE_SIZE') {
          res.json({
            success: false,
            error: {
              message:
                'Upload failed. File is too large. The current limit is: ' + FILE_SIZE_LIMIT + 'B.',
            },
          });
        } else {
          res.json({
            success: false,
            error: {
              message: 'Upload failed:: ' + JSON.stringify(err),
            },
          });
        }
      } else {
        let file = req.file;
        try {
          var metaData = {
            // 'Content-Type': 'application/octet-stream',
            mimetype: file.mimetype,
            originalFilename: new URLSearchParams(file.originalname).toString(),
            userCreated: req.session.passport.user,
            fileOrdinal: req.params.fileOrdinal,
            idTestInstance: id_test_instance,
          };
          let newfilename = 'upload_' + req.params.fileOrdinal + '_' + file.originalname;
          let minioFilename = `${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${req.params.ordinal}/${newfilename}`;
          //console.log(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, minioFilename);
          await service.putObject(
            globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
            minioFilename,
            file.buffer,
            metaData
          );
          res.json({
            success: true,
            data: req.file,
          });
        } catch (error) {
          winston.error(error);
          res
            .status(500)
            .send({
              error: {
                message: `Upload failed: ${error.message}`,
              },
            })
            .end();
        }
      }
    });
  },
  deleteTestInstanceFile: async function (req, res, id_test_instance) {
    let prefix = `${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${req.params.ordinal}/`;
    let objectsStream = minioClient.listObjects(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, prefix, true);
    let toBeDeleted = [];
    objectsStream.on('error', async (err) => {
      winston.error(err);
      res.json({
        message: 'Error deleting files.',
        success: false,
      });
    });
    objectsStream.on('data', function (obj) {
      if (obj.name.indexOf(`upload_${req.params.fileOrdinal}_`) > 0) toBeDeleted.push(obj.name);
    });
    objectsStream.on('end', async () => {
      try {
        for (const filename of toBeDeleted) {
          await this.removeObject(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, filename);
        }
        winston.debug(`Deleted ${toBeDeleted.length} files from ${prefix}.`);
        res.json({
          success: true,
        });
      } catch (error) {
        winston.error(error);
        res.json({
          success: false,
          error: {
            message: JSON.stringify(error.message),
          },
        });
      }
    });
  },

  putObject: async (minioBucket, minioFilename, buffer, metaData) => {
    return minioClient.putObject(minioBucket, minioFilename, buffer, metaData);
  },
  removeObject: async (minioBucket, minioFilename) => {
    return minioClient.removeObject(minioBucket, minioFilename);
  },
  getObjectsInBucket: async (bucketName, prefix) => {
    return new Promise((resolve, reject) => {
      let files = [];
      try {
        let objectsStream = minioClient.listObjects(bucketName, prefix, true);
        objectsStream.on('error', function (err) {
          winston.error(err);
          resolve([]);
        });
        objectsStream.on('data', function (obj) {
          files.push(obj);
        });
        objectsStream.on('end', async () => {
          resolve(files);
        });
      } catch (err) {
        winston.error(`getObjectsInBucket for (${bucketName}, ${prefix})`);
        winston.error(err);
        resolve(files);
      }
    });
  },
  streamObject: (bucketName, objectName, destStream) => {
    return new Promise((resolve, reject) => {
      minioClient.getObject(bucketName, objectName, function (error, stream) {
        if (error) {
          if (objectName.startsWith('faces')) {
            // winston.debug(`streamObject error for (${bucketName}, ${objectName})`);
          } else {
            winston.error(`streamObject error for (${bucketName}, ${objectName})`);
          }
          reject(error);
        } else {
          stream.pipe(destStream);
          stream.on('end', async () => {
            resolve();
          });
          stream.on('error', async (err) => {
            reject(err);
          });
        }
      });
    });
  },
  streamFileFromZipObject: (bucketName, objectName, filterFileName, destStream) => {
    return new Promise((resolve, reject) => {
      minioClient.getObject(bucketName, objectName, function (error, stream) {
        // console.log("inside streamObject", error);
        if (error) {
          reject(error);
        } else {
          stream.pipe(unzipper.Parse()).pipe(
            Transform({
              objectMode: true,
              transform: function (entry, e, cb) {
                const fileName = entry.path;
                // console.log('fileName === filterFileName', fileName, filterFileName);
                // const type = entry.type; // 'Directory' or 'File'
                // const size = entry.vars.uncompressedSize; // There is also compressedSize;
                if (fileName === filterFileName) {
                  entry.pipe(destStream).on('finish', cb);
                } else {
                  entry.autodrain();
                  cb();
                }
              },
            })
          );
          stream.on('end', async () => {
            resolve();
          });
          stream.on('error', async (err) => {
            reject(err);
          });
        }
      });
    });
  },
  zipFilesInTheBucket: async function (bucketName, prefix, tmpDir) {
    let arrFilesInBucket = await this.getObjectsInBucket(bucketName, prefix);
    if (arrFilesInBucket && arrFilesInBucket.length) {
      for (const file of arrFilesInBucket) {
        let filename = file.name.split('/').pop();
        await minioClient.fGetObject(bucketName, file.name, path.join(tmpDir, filename));
      }
      let destFilename = utils.UUID.generate() + '.zip';
      // let yearFolder = new Date().getFullYear();
      let destZipFile = path.join(os.tmpdir(), destFilename);
      winston.info('Zipping dir ' + tmpDir + ' to ' + destZipFile);
      await utils.zipDirectory(tmpDir, destZipFile);
      fs.readdirSync(tmpDir).forEach((f) => fs.rmSync(`${tmpDir}/${f}`));
      return {
        destFilename,
        destZipFile,
        filesInBucket: arrFilesInBucket,
      };
    }
  },
  zipUploadedFiles: async function (id_test_instance, username, studAnswers) {
    let tmpDir;
    const appPrefix = `edgar-ti-${id_test_instance}-`;
    try {
      tmpDir = fs.mkdtempSync(path.join(os.tmpdir(), appPrefix));
      let prefixDir = `${globals.TI_UPLOAD_FOLDER}/${id_test_instance}`;
      for (var ord = studAnswers.length - 1; ord >= 0; --ord) {
        // reverse order, so that prefix 1 will not pick up 11, 111, etc.
        let prefix = `${prefixDir}/${ord + 1}`;
        let zippedFiles = await this.zipFilesInTheBucket(
          globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
          prefix,
          tmpDir
        );
        // console.log(tmpDir, prefixDir, arrFilesForQuestion);
        if (zippedFiles) {
          let yearFolder = new Date().getFullYear();
          let fpath = `${yearFolder}/${zippedFiles.destFilename}`;
          studAnswers[ord].uploaded_files = fpath;
          let metaData = {
            userCreated: username,
            idTestInstance: id_test_instance,
          };
          winston.info(
            'Uploading to minio: ' +
              JSON.stringify({
                bucket: globals.PUBLIC_TI_DOWNLOAD_FOLDER,
                filePath: fpath,
                fromFile: zippedFiles.destZipFile,
                metada: metaData,
              })
          );
          await minioClient.fPutObject(
            globals.PUBLIC_TI_DOWNLOAD_FOLDER,
            fpath,
            zippedFiles.destZipFile,
            metaData
          );
          // delete files from tmp-upload bucket
          for (const file of zippedFiles.filesInBucket) {
            winston.info('Removing from minio: ' + file.name);
            await minioClient.removeObject(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, file.name);
          }
        }
      }
    } catch (error) {
      winston.error('ERROR: zipUploadedFiles');
      winston.error(error);
    } finally {
      try {
        if (tmpDir) {
          winston.debug('Removing tmpdir: ' + tmpDir);
          fs.rmSync(tmpDir, {
            recursive: true,
          });
        }
      } catch (e) {
        console.error(
          `An error has occurred while removing the temp folder at ${tmpDir}. Please remove it manually. Error: ${e}`
        );
      }
    }
  },

  unzipToFolder: async (bucketName, fileName, path) => {
    return new Promise(async (resolve, reject) => {
      let stream = unzip
        .Extract({
          path,
        })
        .on('error', function (err) {
          winston.error('Unzipper died on file: ' + fileName);
          winston.error(err);
          reject(err);
        })
        .on('close', function () {
          winston.debug('finished unzipping');
          resolve();
        });
      try {
        await service.streamObject(bucketName, fileName, stream);
      } catch (e) {
        winston.error(e);
        reject(e);
      }
    });
  },
  downloadTestInstanceFile: async function (res, id_test_instance, question_ordinal, file_ordinal) {
    try {
      console.log(id_test_instance, question_ordinal, file_ordinal);
      let prefix = `${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${question_ordinal}`;
      console.log(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, prefix);
      let arrFilesForQuestion = await service.getObjectsInBucket(
        globals.MINIO_TMP_UPLOAD_BUCKET_NAME,
        prefix
      );

      let object = arrFilesForQuestion.find((obj) => {
        return obj.name && obj.name.indexOf('/upload_' + file_ordinal + '_') >= 0;
      });
      if (object) {
        let downloadFilename = object.name
          .split('/')
          .pop()
          .replace('upload_' + file_ordinal + '_', '');
        res.set('Content-Disposition', 'attachment;filename=' + downloadFilename);
        await service.streamObject(globals.MINIO_TMP_UPLOAD_BUCKET_NAME, object.name, res);
      } else {
        throw `Minio download, file (${id_test_instance}, ${question_ordinal}, ${file_ordinal}) not found in bucket ${globals.MINIO_TMP_UPLOAD_BUCKET_NAME}`;
      }
    } catch (err) {
      winston.error(err);
      res.sendStatus(404);
    }
  },

  zipObjects: async function (bucketNames, minioObjects, objectFilenames) {
    try {
      if (minioObjects && minioObjects.length) {
        var tmpMasterDir = tmp.dirSync();
        let folderName = `${tmpMasterDir.name}`;
        if (!fs.existsSync(folderName)) {
          fs.mkdirSync(folderName);
        }
        let Zip = new AdmZip();
        let errCount = 0;
        let errMessages = [];
        let arrFilenames = [];
        for (let i = 0; i < minioObjects.length; ++i) {
          try {
            let hasFilename = objectFilenames && objectFilenames[i];
            let filename = hasFilename ? objectFilenames[i] : minioObjects[i];
            filename = filename.trim().split(new RegExp('\\s+')).join('_');

            if (!hasFilename && filename.endsWith('.zip')) {
              let unzippedPath = path.join(folderName, 'unzipped');
              fs.mkdirSync(unzippedPath);
              await service.unzipToFolder(bucketNames[i], minioObjects[i], unzippedPath);
              let files = fs.readdirSync(unzippedPath);
              files.forEach((file) => {
                let filename = file.trim().split(new RegExp('\\s+')).join('_');
                // winston.debug('Zipping file: ' + path.join(unzippedPath, file) + ' as ' + filename);
                Zip.addLocalFile(path.join(unzippedPath, file), filename);
                arrFilenames.push(filename);
              });
            } else {
              await minioClient.fGetObject(
                bucketNames[i],
                minioObjects[i],
                path.join(folderName, filename)
              );
              // winston.debug('Zipping file: ' + path.join(folderName, filename) + ' as ' + filename);
              Zip.addLocalFile(path.join(folderName, filename));
              arrFilenames.push(filename);
            }
          } catch (error) {
            winston.error(error);
            ++errCount;
            errMessages.push(
              `Error occured while downloading ${objectFilenames[i]} to ${path.join(
                folderName,
                filename
              )}: ${error.message}`
            );
          }
        }
        if (errCount > 0) {
          errMessages.push(`---------------\nErrors while making package:`);
          fs.writeFileSync(`${tmpMasterDir.name}/errors.txt`, errMessages.join('\n'));
        }
        let rv = Zip.toBuffer().toString('base64');
        tmpMasterDir.removeCallback();
        return {
          filenames: arrFilenames,
          base64Zip: rv,
        };
      } else {
        return null;
      }
    } catch (error) {
      winston.error('Error preparing zip bundle.');
      winston.error(error);
    }
  },
};

module.exports = service;
