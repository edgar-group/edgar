'use strict';
var db = require('../db').db;
// var winston = require('winston');
// var utils = require('../common/utils');

var service = {
  getProgrammingLanguages: async function (idCourse) {
    return db.any(
      `SELECT DISTINCT programming_language.id, name, id_type, hello_world, programming_language.id as value
         FROM course_code_runner
         JOIN programming_language
           ON course_code_runner.id_programming_language = programming_language.id
          AND id_course = $(idCourse)
        ORDER BY programming_language.id`,
      {
        idCourse,
      }
    );
  },
  getNodes: async function (idCourse) {
    return db.many(
      `SELECT node.id,
                       node_name,
                       type_name
                FROM node
                JOIN node_type ON node.id_node_type = node_type.id
                WHERE node.id IN
                    (SELECT id_child
                     FROM v_roots_children
                     WHERE id_root =
                         (SELECT id_root_node
                          FROM course
                          WHERE id = $(idCourse)))
                ORDER BY type_name,
                         node_name `,
      {
        idCourse,
      }
    );
  },
  getCodeRunners: async function (idCourse) {
    return db.any(
      `SELECT DISTINCT code_runner.id as value, name
            FROM course_code_runner
            JOIN code_runner
              ON course_code_runner.id_code_runner = code_runner.id
            WHERE course_code_runner.id_course = $(idCourse)
            ORDER BY code_runner.id`,
      {
        idCourse,
      }
    );
  },
};

module.exports = service;
