// 'use strict';
// const db = require('../db').db;
// const mongoose = require('mongoose');
// const mongoDb = mongoose.connection;
// const TutorialProgress = require.main.require('./models/TutorialProgressModel');
// const questionEvalService = require('../services/questionEvalService');
// const utils = require('../common/utils');

// async function listTutorials(courseId, studentId, onlyActive = true) {
//   const activeFilter = onlyActive ? 'AND tutorial.is_active = true' : '';
//   return await db.any(
//     `SELECT tutorial.id, tutorial_title, tutorial.is_active, tutorial_desc,
//                 tutorial_student.ts_finished :: timestamp(0) :: varchar,
//                 CASE WHEN is_finished = TRUE THEN 'Yes' ELSE 'No' END AS finished,
//                 string_agg(DISTINCT course_name, ', ') AS courses,
//                 COUNT(DISTINCT tutorial_step.id) AS no_steps
//             FROM tutorial JOIN tutorial_course
//                     ON tutorial.id = tutorial_course.id_tutorial
//                 JOIN course
//                     ON tutorial_course.id_course = course.id
//                 LEFT JOIN tutorial_step
//                     ON tutorial.id = tutorial_step.id_tutorial
//                 LEFT JOIN tutorial_student
//                     ON tutorial.id = tutorial_student.id_tutorial
//                     AND tutorial_student.id_student = $(id_student)
//             WHERE tutorial_course.id_course = $(id_course)
//                 ${activeFilter}
//             GROUP BY tutorial.id, tutorial_title, tutorial_desc,
//                 tutorial_student.ts_finished, tutorial_student.is_finished
//             ORDER BY tutorial_title`,
//     {
//       id_course: courseId,
//       id_student: studentId,
//     }
//   );
// }

// async function start(courseId, studentId, tutorialId) {
//   const trackingId = parseInt(
//     (await db.func('start_tutorial', [courseId, studentId, tutorialId]))[0].start_tutorial
//   );

//   const status = await TutorialProgress.getTutorialProgress(tutorialId, courseId, studentId);

//   if (status) {
//     return status;
//   } else {
//     return {
//       latestStepOrdinal: 1,
//     };
//   }
// }

// function getTutorial(tutorialId) {
//   return Promise.all([
//     db.one(
//       `SELECT id, tutorial_title AS title, tutorial_desc AS descruption, allow_random_access
//             FROM tutorial
//             WHERE id = $(id_tutorial)`,
//       {
//         id_tutorial: tutorialId,
//       }
//     ),
//     db.many(
//       `SELECT ordinal, title
//             FROM tutorial_step
//             WHERE id_tutorial = $(id_tutorial)
//             ORDER BY ordinal ASC`,
//       {
//         id_tutorial: tutorialId,
//       }
//     ),
//   ]).then((data) => ({
//     ...data[0],
//     steps: data[1],
//   }));
// }

// async function getTutorialStep(tutorialId, stepOrdinal, courseId, studentId) {
//   return db
//     .task((t) =>
//       t.batch([
//         db.oneOrNone(
//           `SELECT id, id_question, ordinal, title, text
//             FROM tutorial_step
//             WHERE id_tutorial = $(id_tutorial)
//                 AND ordinal = $(step_ordinal)`,
//           {
//             id_tutorial: tutorialId,
//             step_ordinal: stepOrdinal,
//           }
//         ),
//         db.any(
//           `SELECT tutorial_step_hint.id, tutorial_step_hint.ordinal,
//                 hint_text as text
//             FROM tutorial_step_hint JOIN tutorial_step
//                 ON tutorial_step_hint.id_step = tutorial_step.id
//             WHERE tutorial_step.id_tutorial = $(id_tutorial)
//                 AND tutorial_step.ordinal = $(step_ordinal)
//                 AND is_active = true
//             ORDER BY tutorial_step_hint.ordinal ASC`,
//           {
//             id_tutorial: tutorialId,
//             step_ordinal: stepOrdinal,
//           }
//         ),
//       ])
//     )
//     .then(async (results) => {
//       let step = results[0],
//         stepHints = results[1];

//       step.html = await utils.md2Html(step.text);
//       stepHints = stepHints.map(async (sh) => ({
//         ...sh,
//         html: await utils.md2Html(sh.text),
//       }));

//       if (!results[0]) {
//         throw new Error('Tutorial step not found');
//       }

//       const question = await db.one(
//         `SELECT question.id, has_answers, type_name as type, question_text as text,
//                 id_question_type, answers_permutation
//             FROM question JOIN question_type
//                 ON question.id_question_type = question_type.id
//                 JOIN tutorial_student_question
//                 ON tutorial_student_question.id_question = question.id
//             WHERE question.id = $(id_question)
//                 AND id_tutorial = $(id_tutorial)
//                 AND id_course = $(id_course)
//                 AND id_student = $(id_student)`,
//         {
//           id_question: step.id_question,
//           id_tutorial: tutorialId,
//           id_course: courseId,
//           id_student: studentId,
//         }
//       );

//       question.html = await utils.md2Html(question.text);

//       let queries = [];

//       if (question && question.has_answers === true) {
//         queries.push(
//           db.any(
//             `SELECT id, answer_text, ordinal
//                                 FROM question_answer
//                             WHERE id_question = $(id_question)
//                             ORDER BY ordinal`,
//             {
//               id_question: question.id,
//             }
//           )
//         );
//       } else if (question && question.type.toUpperCase().indexOf('SQL') >= 0) {
//         queries.push(
//           db.any(
//             `SELECT sql_answer, mode_desc, sql_alt_assertion, sql_test_fixture,
//                     sql_alt_presentation_query, check_tuple_order, id_check_column_mode
//                             FROM sql_question_answer
//                             JOIN check_column_mode
//                                 ON sql_question_answer.id_check_column_mode = check_column_mode.id
//                         WHERE id_question = $(id_question)`,
//             {
//               id_question: question.id,
//             }
//           )
//         );
//       } else if (
//         question.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
//         question.type_name.toUpperCase().indexOf('JAVA') >= 0
//       ) {
//         queries.push(
//           t.any(
//             `SELECT c_prefix, c_suffix, c_source
//                             FROM c_question_answer
//                             -- LEFT JOIN c_question_test
//                             --   ON c_question_answer.id = c_question_test.c_question_answer_id
//                             WHERE id_question = $(id_question)`,
//             {
//               id_question: question.id,
//             }
//           )
//         );
//       }

//       queries.push(
//         question.id_question_type === 1
//           ? db.any(
//               `SELECT id, id_question_answer, hint_text as text
//                 FROM tutorial_question_hint
//                 WHERE id_step = $(id_step)
//                     AND is_active = true`,
//               {
//                 id_step: step.id,
//               }
//             )
//           : db.any(
//               `SELECT id, ordinal, answer_regex_trigger as trigger,
//                     hint_text as text
//                 FROM tutorial_code_question_hint
//                 WHERE id_step = $(id_step)
//                     AND is_active = true`,
//               {
//                 id_step: step.id,
//               }
//             )
//       );

//       return db
//         .task((t) => t.batch(queries))
//         .then(async (data) => {
//           let answers = data[0],
//             codeHints = !question.has_answers ? data[1] : [],
//             htmlAnswersPerm = [];

//           if (question.has_answers) {
//             for (
//               let i = 0;
//               question.answers_permutation && i < question.answers_permutation.length;
//               ++i
//             ) {
//               const permIdx = question.answers_permutation[i] - 1,
//                 hint = data[1].find((h) => h.id_question_answer == answers[permIdx].id);
//               htmlAnswersPerm.push({
//                 aHtml: await utils.md2Html(answers[permIdx].answer_text),
//                 ordinal: i + 1,
//                 hintHtml: hint ? await utils.md2Html(hint.text) : null,
//               });
//             }
//           } else if (codeHints.length) {
//             for (let i = 0; i < codeHints.length; ++i) {
//               codeHints[i] = {
//                 ordinal: codeHints[i].ordinal,
//                 trigger: codeHints[i].trigger,
//                 html: await utils.md2Html(codeHints[i].text),
//               };
//             }
//           }

//           delete step.id_question;
//           delete step.text;
//           delete question.id;
//           delete question.id_question_type;
//           delete question.text;
//           delete question.answers_permutation;

//           return await Promise.all(stepHints).then(
//             async (stepHints) =>
//               await Promise.all(codeHints).then(
//                 async (codeHints) =>
//                   await Promise.all(htmlAnswersPerm).then((hap) => ({
//                     ...step,
//                     hints: stepHints,
//                     question: {
//                       ...question,
//                       answers: question.has_answers ? hap : [],
//                       codeHints: codeHints,
//                       modeDesc: data[0].mode_desc,
//                       multiCorrect: null,
//                     },
//                   }))
//               )
//           );
//         });
//     });
// }

// async function getStudentAnswers(courseId, studentId, tutorialId) {
//   return await TutorialProgress.getTutorialProgress(courseId, studentId, tutorialId);
// }

// async function updateStudentAnswers(courseId, studentId, tutorialId, answers) {
//   return await TutorialProgress.updateTutorialAnswers(courseId, studentId, tutorialId, answers);
// }

// async function evaluateQuestionAnswer(
//   courseId,
//   studentId,
//   tutorialId,
//   questionId,
//   answer,
//   currStepOrdinal
// ) {
//   const abcEvalFunc = async function (questionId, answerOrdinal) {
//     return await questionEvalService.evlauateTutorialAbcQuestion(
//       courseId,
//       tutorialId,
//       studentId,
//       questionId,
//       answerOrdinal
//     );
//   };
//   const sqlEvalFunc = questionEvalService.evaluateSqlQuestion;
//   const cLangEvalFunc = questionEvalService.evaluateCLangQuestion; // to be implemented

//   const result = await questionEvalService.evaluateQuestion(
//     courseId,
//     questionId,
//     answer,
//     abcEvalFunc,
//     sqlEvalFunc,
//     cLangEvalFunc
//   );

//   const event = {
//     clientTs: new Date(),
//     eventName: 'Answer evaluation',
//     eventData: `step ordinal: ${currStepOrdinal}, correct: ${result.score.is_correct}`,
//   };

//   TutorialProgress.logEvent(courseId, studentId, tutorialId, event);

//   if (result.score.is_correct) {
//     progressToNextStep(courseId, studentId, tutorialId, currStepOrdinal);
//   }

//   let answers, codeAnswer;

//   if (result.question_type.toUpperCase().includes('ABC')) {
//     // ABC question
//     answers = answer || [];
//   } else if (
//     result.question_type.toUpperCase().includes('SQL') ||
//     result.question_type.toUpperCase().includes('C-LANG')
//   ) {
//     // SQL question
//     codeAnswer = answer || '';
//   }

//   const predicate = {
//     id_course: courseId,
//     id_student: studentId,
//     id_tutorial: tutorialId,
//     id_question: questionId,
//   };

//   await db.task((t) =>
//     t.batch([
//       db.none(
//         `UPDATE tutorial_student_question
//                 SET ${answers ? 'student_answers = $(answers),' : ''}
//                     ${codeAnswer ? 'student_answer_code = $(code_answer),' : ''}
//                     is_correct = $(is_correct)
//                 WHERE id_course = $(id_course)
//                     AND id_student = $(id_student)
//                     AND id_tutorial = $(id_tutorial)
//                     AND id_question = $(id_question)`,
//         {
//           ...predicate,
//           answers: answers,
//           code_answer: codeAnswer,
//           is_correct: result.score.is_correct,
//         }
//       ),
//       db.none(
//         `INSERT INTO tutorial_student_question_attempt (
//                     id_course, id_tutorial, id_student, id_question, is_correct)
//                     VALUES ($(id_course), $(id_tutorial), $(id_student), $(id_question),
//                     $(is_correct))`,
//         {
//           ...predicate,
//           is_correct: result.score.is_correct,
//         }
//       ),
//     ])
//   );

//   return result;
// }

// async function progressToNextStep(courseId, studentId, tutorialId, currentStepOrdinal) {
//   const steps = parseInt(
//     (
//       await db.one(
//         `SELECT COUNT(*) as steps
//             FROM tutorial_step
//             WHERE id_tutorial = $(id_tutorial)`,
//         {
//           id_tutorial: tutorialId,
//         }
//       )
//     ).steps
//   );

//   if (currentStepOrdinal < steps) {
//     return await TutorialProgress.updateLatestStepOrdinal(
//       courseId,
//       studentId,
//       tutorialId,
//       currentStepOrdinal + 1
//     );
//   } else if (currentStepOrdinal === steps) {
//     const previouslyFinished = await db.oneOrNone(
//       `SELECT ts_finished :: timestamp(0) :: varchar
//             FROM tutorial_student
//             WHERE id_course = $(id_course)
//                 AND id_student = $(id_student)
//                 AND id_tutorial = $(id_tutorial)`,
//       {
//         id_course: courseId,
//         id_student: studentId,
//         id_tutorial: tutorialId,
//       }
//     );

//     if (!(previouslyFinished && previouslyFinished.ts_finished)) {
//       const finishedTs = new Date();
//       db.none(
//         `UPDATE public.tutorial_student
//                 SET is_finished = true, ts_finished = $(ts_finished)
//                 WHERE id_course = $(id_course)
//                     AND id_student = $(id_student)
//                     AND id_tutorial = $(id_tutorial)`,
//         {
//           id_course: courseId,
//           id_student: studentId,
//           id_tutorial: tutorialId,
//           ts_finished: finishedTs,
//         }
//       );
//       return await TutorialProgress.markAsFinished(courseId, studentId, tutorialId, finishedTs);
//     }
//   }
// }

// async function logEvent(courseId, studentId, tutorialId, event) {
//   return await TutorialProgress.logEvent(courseId, studentId, tutorialId, event);
// }

// module.exports = {
//   listTutorials: listTutorials,
//   start: start,
//   getTutorial: getTutorial,
//   getTutorialStep: getTutorialStep,
//   getStudentAnswers: getStudentAnswers,
//   evaluateQuestionAnswer: evaluateQuestionAnswer,
//   updateStudentAnswers: updateStudentAnswers,
//   progressToNextStep: progressToNextStep,
//   logEvent: logEvent,
// };
