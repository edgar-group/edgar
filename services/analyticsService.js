'use strict';
var db = require('../db').db;
var plagiarismDetectionService = require('./plagiarismDetectionService');

async function getManualGradingExamDistribution(testId, resolution) {
  let data = await db.any(
    `
        WITH ranges as
          (SELECT step * $(resolution) AS range_min,
                  step * $(resolution) + $(resolution) - 1 AS range_max
            FROM generate_series(0, 100 / $(resolution)) step)

        SELECT range_min::int x, COUNT( * )::int y
        FROM student_course sc JOIN
            (
            SELECT ti.id_student,
                   ((CASE WHEN SUM(t.max_score) <> 0 THEN SUM(mg.score) / MAX(t.max_score) ELSE 0 END) * 100) AS score
              FROM test t 
              JOIN test_instance ti
                ON t.id = ti.id_test
              JOIN test_instance_question tiq
                ON ti.id = tiq.id_test_instance
              JOIN test_instance_question_manual_grade mg
                ON mg.id_test_instance_question = tiq.id
             WHERE t.id = $(id_test)                
            GROUP BY ti.id_student
            ) students
            ON sc.id_student = students.id_student
         JOIN test
           ON sc.id_course = test.id_course
          AND sc.id_academic_year = test.id_academic_year
          AND test.id = $(id_test)
         JOIN ranges
           ON students.score between range_min and range_max
        WHERE students.score BETWEEN 0 AND 100
          AND COALESCE(sc.class_group, '-') <> 'Teachers'
        GROUP BY range_min
        ORDER BY range_min `,
    {
      id_test: testId,
      resolution: resolution,
    }
  );

  const scoreCategories = [];
  for (let i = 0; i <= 100; i += resolution) {
    scoreCategories.push(i);
  }

  // add missing score categories with no students in it
  data.forEach((score, idx) => {
    const missing = [...new Set(scoreCategories.filter((x) => !data.map((s) => s.x).includes(x)))];
    missing.forEach((xVal) => {
      data.push({
        x: xVal,
        y: 0,
      });
    });
  });
  data = data.sort((a, b) => a.x - b.x);
  return data;
}
async function getScoreAnalyticsByAllGroups(
  academicYearId,
  courseId,
  testId,
  onlyScored,
  resolution
) {
  let data = await db.any(
    `
        WITH ranges as
            (SELECT step * $(resolution) AS range_min,
                step * $(resolution) + $(resolution) - 1 AS range_max FROM generate_series(0, 100 / $(resolution)) step) 
       SELECT range_min::int x, COUNT( * )::int y
        FROM student_course sc JOIN
            (
            SELECT ti.id_student,
                   (CASE WHEN SUM(t.max_score) <> 0 THEN SUM(ti.score) / SUM(t.max_score) ELSE 0 END) * 100 AS score
              FROM v_test_stats t 
              JOIN test_instance ti
                ON t.id = ti.id_test
             WHERE -- t.use_in_stats
                   t.id_course = $(id_course)
               AND t.id_academic_year = $(id_academic_year) ${
                 testId ? `AND t.id = $(id_test)` : ''
               } ${onlyScored ? 'AND test_score_ignored = false' : ''}
                AND score IS NOT NULL
            GROUP BY ti.id_student
            ) students
            ON sc.id_student = students.id_student
          JOIN ranges
            ON students.score between range_min and range_max
        WHERE sc.id_course = $(id_course)
          AND sc.id_academic_year = $(id_academic_year) 
          AND students.score BETWEEN 0 AND 100
          AND COALESCE(sc.class_group, '-') <> 'Teachers'
        GROUP BY range_min
        ORDER BY range_min `,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_test: testId,
      resolution: resolution,
    }
  );

  const scoreCategories = [];
  for (let i = 0; i <= 100; i += resolution) {
    scoreCategories.push(i);
  }

  // add missing score categories with no students in it
  data.forEach((score, idx) => {
    const missing = [...new Set(scoreCategories.filter((x) => !data.map((s) => s.x).includes(x)))];
    missing.forEach((xVal) => {
      data.push({
        x: xVal,
        y: 0,
      });
    });
  });

  // sort them
  data = data.sort((a, b) => a.x - b.x);

  return data;
}

async function getScoreAnalyticsThroughTime(academicYearId, courseId, studentId, onlyScored) {
  const data = await db.any(
    `SELECT
            CASE WHEN (t.title_abbrev IS NULL OR t.title_abbrev = '') THEN 'ID' || t.id ELSE t.title_abbrev END x,
            COUNT(ti.id) as n,
            ROUND(100 * MAX ( CASE WHEN (ti.id_student = $(id_student)) THEN COALESCE(ti.score_perc, 0) ELSE 0 END), 2) as y,
            ROUND(AVG(ti.score_perc * 100), 2) y2,
            (
                SELECT max(case when (ranked.id_student = $(id_student)) then ranked.rank else -1 end) as rank
                    FROM (
                    SELECT
                        sc.id_student,
                        RANK() OVER (ORDER BY SUM(ti.score) desc) rank
                    FROM v_test_stats tt
                    JOIN test_instance ti 
                      ON tt.id = ti.id_test
                    JOIN student_course sc 
                      ON sc.id_course = tt.id_course 
                     AND ti.id_student = sc.id_student
                     AND sc.id_academic_year = tt.id_academic_year
                    WHERE -- tt.use_in_stats
                          tt.id_course = $(id_course)
                      AND tt.id_academic_year = $(id_academic_year)
                      AND ti.score IS NOT NULL
                      AND COALESCE(class_group, '-') <> 'Teachers'
                      -- AND test_score_ignored = false
                      AND tt.test_ordinal <= t.test_ordinal
                    GROUP BY sc.id_student
                ) as ranked) as y3
        FROM v_test_stats t
        LEFT JOIN test_instance ti 
               ON t.id = ti.id_test
        WHERE --t.use_in_stats
                t.id_course = $(id_course)
            AND t.id_academic_year = $(id_academic_year) 
           -- AND sc.id_student = $(id_student)
            -- AND test_score_ignored = false
        GROUP BY x, t.id, t.test_ordinal
        ORDER BY t.test_ordinal`,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_student: studentId,
    }
  );

  return data.map((item) => ({
    ...item,
    y: parseFloat(item.y),
    y2: parseFloat(item.y2),
    y3: parseFloat(item.y3),
  }));
}

async function getQuestionScoreAnalyticsThroughTime(academicYearId, courseId, studentId, testId) {
  const data = await db.any(
    `WITH enrolledStudents AS (
                SELECT sc.id_student 
                FROM student_course sc 
                WHERE sc.id_course = $(id_course)
                AND sc.id_academic_year = $(id_academic_year)
                AND COALESCE(class_group, '-') <> 'Teachers'
                )
        SELECT    tiq.ordinal x
        , COALESCE( MAX (CASE WHEN (ti.id_student = $(id_student)) THEN tiq.score ELSE NULL END), 0) as y
        , ROUND(AVG(tiq.score), 2) as y2
        , (SELECT ROUND(AVG(tiq2.score), 2) 
            FROM test_instance_question tiq2 
            JOIN test_instance ti ON ti.id = tiq2.id_test_instance 
            JOIN v_test_stats t ON t.id = ti.id_test 
            JOIN enrolledStudents
              ON enrolledStudents.id_student = ti.id_student
            WHERE t.id_course = $(id_course)
            AND t.id = $(id_test)
            AND t.id_academic_year = $(id_academic_year) 
            AND tiq2.id_question = (SELECT id_question 
                FROM test_instance_question 
                JOIN test_instance ti ON ti.id = test_instance_question.id_test_instance 
                AND id_student = $(id_student)
                AND ordinal = tiq.ordinal 
                AND ti.id_test = $(id_test))
          ) as y3
            FROM test_instance_question tiq
            JOIN test_instance ti
              ON ti.id = tiq.id_test_instance
            JOIN v_test_stats t
              ON t.id = ti.id_test
            JOIN enrolledStudents
              ON enrolledStudents.id_student = ti.id_student
            WHERE t.id_course = $(id_course)
              AND t.id = $(id_test)
              AND t.id_academic_year = $(id_academic_year)                         
        GROUP BY tiq.ordinal
            ORDER BY x`,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_student: studentId,
      id_test: testId,
    }
  );

  return data;
}

async function getScoreAnalyticsByGroups(academicYearId, courseId, testId, onlyScored, resolution) {
  const data = await db.any(
    `
        WITH ranges as
            (SELECT step * $(resolution) AS range_min,
                step * $(resolution) + $(resolution) - 1 AS range_max FROM generate_series(0, 100 / $(resolution)) step)
    SELECT sc.class_group "group", ranges.range_min::int x, COUNT( * )::int y
        FROM student_course sc JOIN
            (
            SELECT ti.id_student,
                (CASE WHEN SUM(t.max_score) <> 0 THEN SUM(ti.score) / SUM(t.max_score) ELSE 0 END) * 100 AS score
            FROM v_test_stats t 
            JOIN test_instance ti
              ON t.id = ti.id_test
            WHERE 
                    t.id_course = $(id_course)
                AND t.id_academic_year = $(id_academic_year) ${
                  testId ? 'AND t.id = $(id_test)' : ''
                } ${onlyScored ? 'AND test_score_ignored = false' : ''}
                AND score is not null
            GROUP BY ti.id_student
            ) students
            ON sc.id_student = students.id_student
        JOIN ranges
          ON students.score between range_min and range_max
        WHERE sc.id_course = $(id_course)
          AND sc.id_academic_year = $(id_academic_year) 
        GROUP BY sc.class_group, range_min
        ORDER BY sc.class_group`,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_test: testId,
      resolution: resolution,
    }
  );
  const groups = [...new Set(data.map((x) => x.group))].filter((x) => x);

  const scoresPerGroup = groups.map((group) => ({
    group: group,
    scores: data.filter((score) => score.group == group && score.x >= 0 && score.x <= 100),
  }));
  const scoreCategories = [];
  for (let i = 0; i <= 100; i += resolution) {
    scoreCategories.push(i);
  }

  // add missing score categories with no students in it
  scoresPerGroup.forEach((groupScore, idx) => {
    const missing = [
      ...new Set(scoreCategories.filter((x) => !groupScore.scores.map((s) => s.x).includes(x))),
    ];
    missing.forEach((xVal) => {
      scoresPerGroup[idx].scores.push({
        x: xVal,
        y: 0,
      });
    });
    scoresPerGroup[idx].scores = scoresPerGroup[idx].scores.filter((s) => s.x >= 0 && s.x <= 100);
  });

  // sort them
  scoresPerGroup.forEach((_, idx) => {
    scoresPerGroup[idx].scores = scoresPerGroup[idx].scores.sort((a, b) => a.x - b.x);
  });

  return {
    scoreCategories: scoreCategories,
    scoresPerGroup: scoresPerGroup,
  };
}

async function getStudentScore(
  academicYearId,
  courseId,
  studentId,
  testId,
  onlyScored,
  decimals = 2
) {
  const data = await db.one(
    `SELECT ROUND(  (CASE WHEN SUM(t.max_score) <> 0 THEN SUM(ti.score) / SUM(t.max_score) ELSE 0 END) * 100, $(decimals)) score
           FROM v_test_stats t 
           JOIN test_instance ti
             ON t.id = ti.id_test
           JOIN student_course sc
             ON sc.id_course = t.id_course
            AND sc.id_academic_year = t.id_academic_year
            AND ti.id_student = sc.id_student
          WHERE -- t.use_in_stats
                t.id_course = $(id_course)
            AND COALESCE(sc.class_group, '-') <> 'Teachers'
            AND t.id_academic_year = $(id_academic_year)
            AND sc.id_student = $(id_student) ${testId ? 'AND t.id = $(id_test)' : ''} ${
      onlyScored ? 'AND test_score_ignored = false' : ''
    }`,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_student: studentId,
      id_test: testId,
      decimals: decimals,
    }
  );

  return parseFloat(data.score);
}

async function getStudentCourseScores(academicYearId, courseId, studentId, testId, decimals = 2) {
  const data = await db.one(
    `SELECT
            COUNT(ti.id) attendedtests,
            COUNT(t.id) heldtests,
            ROUND(MIN(ti.score_perc) * 100, ${decimals}) minscore,
            ROUND(MAX(ti.score_perc) * 100, ${decimals}) maxscore,
            ROUND(AVG(ti.score_perc) * 100, ${decimals}) avgscore,
            SUM(ti.score) winnedscore,
            -- SUM(t.max_score) maxwinnablescoretest,
            (   -- max possible score ANY student on the course could have achieved through written tests
                SELECT SUM(t.max_score) as max_score
                  FROM v_test_stats t
                  JOIN test_instance ti
                    ON t.id = ti.id_test
                 WHERE t.id_course = $(id_course)
                   AND t.id_academic_year = $(id_academic_year)
                   AND score IS NOT NULL
                   ${testId ? 'AND t.id = $(id_test)' : ''}
                 GROUP BY ti.id_student
                 ORDER BY 1 desc
                 LIMIT 1
            ) AS maxwinnablescore
        FROM v_test_stats t 
        LEFT JOIN test_instance ti
               ON t.id = ti.id_test
              AND ti.id_student = $(id_student)
        WHERE -- t.use_in_stats
                t.id_course = $(id_course)
            AND t.id_academic_year = $(id_academic_year)
            AND test_score_ignored = false ${testId ? 'AND t.id = $(id_test)' : ''}
        `,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_student: studentId,
      decimals: decimals,
      id_test: testId,
    }
  );

  return {
    attendedTests: parseFloat(data.attendedtests),
    heldTests: parseFloat(data.heldtests),
    minScore: parseFloat(data.minscore),
    maxScore: parseFloat(data.maxscore),
    avgScore: parseFloat(data.avgscore),
    winnedScore: parseFloat(data.winnedscore),
    maxWinnableScore: parseFloat(data.maxwinnablescore),
    // maxWinnableScoreTest: parseFloat(data.maxwinnablescoretest)
  };
}

async function getStudentRank(academicYearId, courseId, studentId, testId) {
  const data = await db.one(
    `SELECT 
                max(case when (ranked.id_student = $(id_student)) then ranked.rank else -1 end) as rank, 
                max(case when (ranked.id_student = $(id_student)) then 100 * ranked.percent_rank else -1 end) as percent_rank, 
                count(*) as maxrank
        FROM (
            SELECT
                RANK() OVER (ORDER BY SUM(ti.score) desc) rank,
                PERCENT_RANK() OVER (ORDER BY SUM(ti.score) ) percent_rank,
                COUNT(ti.*) maxrank,
                sc.id_student id_student
            FROM v_test_stats t
            JOIN test_instance ti 
              ON t.id = ti.id_test
            JOIN student_course sc 
              ON sc.id_course = t.id_course 
             AND ti.id_student = sc.id_student
             AND sc.id_academic_year = $(id_academic_year)
            WHERE -- t.use_in_stats
                  t.id_course = $(id_course)
              AND t.id_academic_year = $(id_academic_year)
              AND ti.score IS NOT NULL
              AND COALESCE(class_group, '-') <> 'Teachers'
              ${testId ? 'AND t.id = $(id_test)' : ''}
            GROUP BY sc.id_student
        ) as ranked
        `,
    {
      id_academic_year: academicYearId,
      id_course: courseId,
      id_student: studentId,
      id_test: testId,
    }
  );

  return {
    rank: parseFloat(data.rank),
    percentileRank: parseFloat(data.percent_rank),
    maxRank: parseFloat(data.maxrank),
  };
}

async function getPlagiarismData(examId, testId, cutoffPercentage, recalculate) {
  let rows = await db.any(
    `SELECT
            test_instance_question.id_question, test_instance_question.student_answer_code, 
            test_instance.id_student, student.first_name, student.last_name
        FROM test_instance_question 
        JOIN test_instance on test_instance_question.id_test_instance = test_instance.id
        JOIN student on test_instance.id_student = student.id
        WHERE id_test_instance in (SELECT id from test_instance where id_test = ${examId}) 
            AND is_unanswered = false
            AND student_answer_code is not null
        ORDER BY id_question, id_student;`
  );
  let data = {};
  let questions = [];
  let studentsPerQuestion = {};
  for (let i in rows) {
    if (!(rows[i]['id_question'] in data)) {
      data[rows[i]['id_question']] = {};
      questions.push(rows[i]['id_question']);
      studentsPerQuestion[rows[i]['id_question']] = [];
    }
    data[rows[i]['id_question']][rows[i]['id_student']] = rows[i]['student_answer_code'];
    studentsPerQuestion[rows[i]['id_question']].push(rows[i]['id_student']);
  }
  data = await plagiarismDetectionService.calculateSimilarity(
    data,
    questions,
    studentsPerQuestion,
    testId,
    cutoffPercentage,
    recalculate,
    examId,
    testId
  );
  data['examId'] = examId;
  return data;
}

module.exports = {
  getManualGradingExamDistribution: getManualGradingExamDistribution,
  getScoreAnalyticsByAllGroups: getScoreAnalyticsByAllGroups,
  getScoreAnalyticsByGroups: getScoreAnalyticsByGroups,
  getStudentScore: getStudentScore,
  // getStudentPercentile: getStudentPercentile,
  getStudentCourseScores: getStudentCourseScores,
  getStudentRank: getStudentRank,
  getScoreAnalyticsThroughTime: getScoreAnalyticsThroughTime,
  getQuestionScoreAnalyticsThroughTime: getQuestionScoreAnalyticsThroughTime,
  getPlagiarismData: getPlagiarismData,
};
