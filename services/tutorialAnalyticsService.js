'use strict';
var db = require('../db').db;
const TutorialProgress = require.main.require('./models/TutorialProgressModel');

const ordinalsSql = `SELECT ordinal
FROM tutorial_step
WHERE id_tutorial = $(id_tutorial)
ORDER BY ordinal ASC`;

async function byGroups(tutorialId, courseId, academicYearId) {
  const ordinals = await db.any(ordinalsSql, {
    id_tutorial: tutorialId,
  });

  if (ordinals.length === 0) {
    return {
      ordinals: [],
    };
  }

  const mainSql = `SELECT class_group, COUNT(ts.*) started,
        SUM(CASE WHEN ts.is_finished THEN 1 ELSE 0 END) finished,
        tutorial_step.ordinal,
        SUM(CASE WHEN tsq.is_correct THEN 1 ELSE 0 END) passed
    FROM student JOIN student_course
        ON student.id = student_course.id_student
        AND student_course.id_course = $(id_course)
        AND id_academic_year = $(id_academic_year)
    JOIN tutorial_student ts
        ON ts.id_tutorial = $(id_tutorial)
        AND ts.id_student = student.id
        AND ts.id_course = $(id_course)
    LEFT JOIN tutorial_student_question tsq
        ON tsq.id_student = student.id
        AND tsq.id_tutorial = $(id_tutorial)
        AND tsq.id_course = $(id_course)
    JOIN tutorial_step
        ON tutorial_step.id_question = tsq.id_question
    WHERE tsq.id_tutorial = $(id_tutorial)
        AND tsq.id_course = $(id_course)
    GROUP BY class_group, tutorial_step.ordinal
    ORDER BY class_group ASC, ordinal ASC, passed DESC`;

  const rv = await db.any(
    `SELECT *
        FROM crosstab($$
        ${mainSql}
        $$, $$
        ${ordinalsSql}
        $$) AS pivotTable(class_group text, started bigint, finished bigint, ${ordinals
          .map((o) => `"${o.ordinal}" text`)
          .join(', ')})`,
    {
      id_tutorial: tutorialId,
      id_course: courseId,
      id_academic_year: academicYearId,
    }
  );

  return {
    ordinals: ordinals,
    data: rv,
  };
}

async function byStudents(tutorialId, courseId, academicYearId) {
  const ordinals = await db.any(ordinalsSql, {
    id_tutorial: tutorialId,
  });

  if (ordinals.length === 0) {
    return {
      ordinals: [],
    };
  }

  const mainSql = `SELECT student.id as id_student, alt_id2, first_name, last_name,
        (first_name || ' ' || last_name || '<br />(' || alt_id2 || ')') student,
        class_group, ts.is_finished, ts.ts_finished :: timestamp(0) :: varchar, ts.ts_created :: timestamp(0) :: varchar,
        tutorial_step.ordinal,
        SUM(CASE WHEN tsqa.is_correct THEN 1 ELSE 0 END) || '/' || COUNT(tsqa.*)
            AS attempts
    FROM student JOIN student_course
        ON student.id = student_course.id_student
        AND student_course.id_course = $(id_course)
        AND id_academic_year = $(id_academic_year)
    JOIN tutorial_student ts
        ON ts.id_tutorial = $(id_tutorial)
        AND ts.id_student = student.id
        AND ts.id_course = $(id_course)
    LEFT JOIN tutorial_student_question tsq
        ON tsq.id_student = student.id
        AND tsq.id_course = $(id_course)
        AND tsq.id_tutorial = $(id_tutorial)
    LEFT JOIN tutorial_student_question_attempt tsqa
        ON tsq.id_question = tsqa.id_question
        AND tsq.id_student = tsqa.id_student
        AND tsq.id_tutorial = tsqa.id_tutorial
        AND tsq.id_course = tsqa.id_course
    LEFT JOIN tutorial_step
        ON tutorial_step.id_question = tsq.id_question
    --WHERE tsq.id_tutorial = $(id_tutorial)
    --    AND tsq.id_course = $(id_course)
    GROUP BY student.id, alt_id2, first_name, last_name, student, class_group,
        is_finished, ts_finished, ts.ts_created, tutorial_step.ordinal
    ORDER BY alt_id2, ordinal, attempts DESC`;

  const rv = await db.any(
    `SELECT *
        FROM crosstab($$
        ${mainSql}
        $$, $$
        ${ordinalsSql}
        $$) AS pivotTable(id_student int, alt_id2 text, first_name text, last_name text,
            student text, class_group text,
            is_finished boolean, ts_finished varchar, ts_created varchar, ${ordinals
              .map((o) => `"${o.ordinal}" text`)
              .join(', ')})
        ORDER BY alt_id2`,
    {
      id_tutorial: tutorialId,
      id_course: courseId,
      id_academic_year: academicYearId,
    }
  );

  return {
    ordinals: ordinals,
    data: rv,
  };
}

async function studentLog(tutorialId, courseId, studentId) {
  const rv = await TutorialProgress.getTutorialProgress(tutorialId, courseId, studentId);
  return rv;
}

module.exports = {
  byGroups: byGroups,
  byStudents: byStudents,
  studentLog: studentLog,
};
