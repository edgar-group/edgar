'use strict';
const db = require('../db').db;
const mongoose = require('mongoose');
var winston = require('winston');
const TutorialProgress = require.main.require('./models/TutorialProgressModel');
const questionEvalService = require('./questionEvalService2');
const utils = require('../common/utils');
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');

async function listTutorials(courseId, studentId, onlyActive = true) {
  const activeFilter = onlyActive ? 'AND tutorial.is_active = true' : '';
  let [mongo, pg, m2] = await Promise.all([
    TutorialProgress.getTutorialProgresses(courseId, studentId),
    db.any(
      `SELECT tutorial.id, tutorial_title, tutorial_desc, is_active,
                tutorial_student.ts_finished :: timestamp(0) :: varchar,
                CASE WHEN is_finished = TRUE THEN 'Yes' ELSE 'No' END AS finished,
                string_agg(DISTINCT course_name, ', ') AS courses,
                COUNT(DISTINCT tutorial_step.id) AS no_steps
            FROM tutorial JOIN tutorial_course
                    ON tutorial.id = tutorial_course.id_tutorial
                JOIN course
                    ON tutorial_course.id_course = course.id
                LEFT JOIN tutorial_step
                    ON tutorial.id = tutorial_step.id_tutorial
                LEFT JOIN tutorial_student
                    ON tutorial.id = tutorial_student.id_tutorial
                    AND tutorial_student.id_student = $(id_student)
                    AND tutorial_student.id_course = $(id_course)
            WHERE tutorial_course.id_course = $(id_course)
                ${activeFilter}
            GROUP BY tutorial.id, tutorial_title, tutorial_desc,
                tutorial_student.ts_finished, tutorial_student.is_finished
            ORDER BY tutorial_title`,
      {
        id_course: courseId,
        id_student: studentId,
      }
    ),
    TutorialProgress.getUsageReport(courseId, 65),
  ]);
  // console.log('m2', JSON.stringify(m2, null, 2));
  let lookup = {};
  mongo.forEach((tut) => {
    lookup[tut.tutorialId] = tut;
  });

  for (let i = 0; i < pg.length; i++) {
    let tut = lookup[pg[i].id];
    if (tut) {
      pg[i].latestStepOrdinal = tut.latestStepOrdinal;
      pg[i].startedTs = tut.startedTs;
      pg[i].finishedTs = tut.finishedTs;
    }
  }
  return pg;
}

async function start(courseId, studentId, tutorialId) {
  const trackingId = parseInt(
    (await db.func('start_tutorial', [courseId, studentId, tutorialId]))[0].start_tutorial
  );

  const status = await TutorialProgress.getTutorialProgress(tutorialId, courseId, studentId);

  if (status) {
    return status;
  } else {
    return {
      latestStepOrdinal: 1,
    };
  }
}

function getTutorial(tutorialId) {
  return Promise.all([
    db.one(
      `SELECT id, tutorial_title AS title, tutorial_desc AS description, allow_random_access
            FROM tutorial
            WHERE id = $(id_tutorial)`,
      {
        id_tutorial: tutorialId,
      }
    ),
    db.many(
      `SELECT ordinal, title
            FROM tutorial_step
            WHERE id_tutorial = $(id_tutorial)
            ORDER BY ordinal ASC`,
      {
        id_tutorial: tutorialId,
      }
    ),
  ]).then((data) => ({
    ...data[0],
    steps: data[1],
  }));
}

async function getTutorialQuestion(questionId, currCourseId) {
  let question, programming_languages, scale_items, attachments;
  winston.debug(`getting tutorial/question id = ${questionId}`);

  question = await db.one(
    `SELECT
            question.id,
            question_text,
            is_active,
            qt.type_name,
            has_answers,

            check_column_mode.mode_desc,
            sql_question_answer.sql_answer,
            text_question_answer.text_answer,
            c_question_answer.c_prefix,
            c_question_answer.c_suffix,
            c_question_answer.c_source,
            c_question_answer.id_programming_language,
            json_question_answer.json_answer,
            COALESCE (
                sql_question_answer.id_code_runner,
                course_code_runner.id_code_runner,
                json_question_answer.id_code_runner) as id_code_runner

            FROM question

        JOIN question_type qt
          ON question.id_question_type = qt.id

        LEFT JOIN sql_question_answer
               ON question.id = sql_question_answer.id_question
        LEFT JOIN check_column_mode
               ON sql_question_answer.id_check_column_mode = check_column_mode.id

            LEFT JOIN text_question_answer
            ON question.id = text_question_answer.id_question

            LEFT JOIN c_question_answer
            ON question.id = c_question_answer.id_question
            LEFT JOIN course_code_runner
                   ON c_question_answer.id_programming_language = course_code_runner.id_programming_language
                  AND course_code_runner.id_course = $(currCourseId)

            LEFT JOIN json_question_answer
            ON question.id = json_question_answer.id_question


        WHERE question.id = $(questionId)
          `,
    {
      questionId,
      currCourseId,
    }
  );
  let abcAnswers;
  if (question.has_answers === true) {
    abcAnswers = await db.any(
      `SELECT is_correct, answer_text, ordinal
          FROM question_answer
         WHERE id_question = $(id_question)
         ORDER BY ordinal`,
      {
        id_question: question.id,
      }
    );
  }
  let html = await utils.md2Html(question.question_text);
  //console.log("THIS IS THE QUESTION HTML ", html)

  if (question.has_answers === true) {
    var htmlAnswersPerm = [];
    // Rearange answers to current permutation:
    for (let i = 0; abcAnswers && i < abcAnswers.length; ++i) {
      htmlAnswersPerm.push({
        aHtml: await utils.md2Html(abcAnswers[i].answer_text),
        ordinal: i + 1,
        isCorrect: abcAnswers[i].is_correct,
      });
    }
  } else if (
    question.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
    question.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
    question.type_name.toUpperCase().indexOf('CODE') >= 0 ||
    question.type_name.toUpperCase().indexOf('SQL') >= 0
  ) {
    programming_languages = await db.any(
      `SELECT programming_language.id, programming_language.name, programming_language.hello_world
               FROM question_programming_language
               JOIN programming_language
                 ON id_programming_language = programming_language.id
                AND id_question = $(id_question)
              ORDER BY programming_language.id`,
      {
        id_question: question.id,
      }
    );
  } else if (question.type_name.toUpperCase().indexOf('SCALE') >= 0) {
    scale_items = await db.any(
      ` SELECT scale_item.value, label
              FROM question_scale
              JOIN scale
                ON question_scale.id_scale = scale.id
              JOIN scale_item
                ON scale.id = scale_item.id_scale
              WHERE id_question = $(id_question)
              ORDER BY ordinal`,
      {
        id_question: question.id,
      }
    );
  }
  attachments = await db.any(
    `SELECT filename, label, is_public
           FROM question_attachment
          WHERE id_question = $(id_question)`,
    {
      id_question: question.id,
    }
  );

  if (question.type_name.toUpperCase().indexOf('SQL') >= 0) {
    question.code_answer = question.sql_answer;
  } else if (question.type_name.toUpperCase().indexOf('JSON') >= 0) {
    question.code_answer = question.json_answer;
  } else if (question.type_name.toUpperCase().indexOf('FREE TEXT') >= 0) {
    question.code_answer = await utils.md2Html(question.text_answer);
  } else {
    question.code_answer = question.c_source;
    // Don't merge prefix and suffix, we don't want students to see that
  }

  return {
    success: true,
    id: question.id,
    type: question.type_name,
    content: question.is_active ? html : `<h1>Not an active question</h1>${html}`,
    answers: htmlAnswersPerm,
    id_code_runner: question.id_code_runner,
    correctAnswerCode: question.code_answer,
    programmingLanguages: programming_languages,
    modeDesc: question.mode_desc,
    multiCorrect: htmlAnswersPerm && htmlAnswersPerm.filter((x) => x.isCorrect).length > 1,
    c_eval_data: question.c_eval_data,
    attachments: attachments,
    scaleItems: question.type_name.toUpperCase().indexOf('SCALE') >= 0 ? scale_items : undefined,
  };
}
async function getTutorialCodeRunner(id_course, id_code_runner, langids) {
  let programming_languages;

  winston.debug(`getting tutorial/code-playground id = ${id_code_runner}`);
  try {
    let langFilter = '';
    if (langids) {
      let ids = [];
      langids
        .replace('(', '')
        .replace(')', '')
        .split(',')
        .forEach(function (id) {
          try {
            ids.push(parseInt(id));
          } catch (error) {}
        });
      if (ids.length) langFilter = ` AND programming_language.id IN (${ids.join(',')})`;
    }
    programming_languages = await db.any(
      `SELECT programming_language.id, programming_language.name, programming_language.hello_world
            FROM course_code_runner
            JOIN programming_language
                ON course_code_runner.id_programming_language = programming_language.id
                AND id_course = $(id_course)
                AND id_code_runner = $(id_code_runner)
                ${langFilter}
            ORDER BY programming_language.id`,
      {
        id_code_runner,
        id_course,
      }
    );
  } catch (error) {}

  return {
    success: true,
    id_code_runner,
    programmingLanguages: programming_languages,
  };
}
async function getTutorialStep(tutorialId, stepOrdinal, courseId, studentId) {
  return db
    .task((t) =>
      t.batch([
        db.oneOrNone(
          `SELECT id, ordinal, title, text
            FROM tutorial_step
            WHERE id_tutorial = $(id_tutorial)
                AND ordinal = $(step_ordinal)`,
          {
            id_tutorial: tutorialId,
            step_ordinal: stepOrdinal,
          }
        ),
        db.any(
          `SELECT tutorial_step_hint.id, tutorial_step_hint.ordinal,
                hint_text as text
            FROM tutorial_step_hint JOIN tutorial_step
                ON tutorial_step_hint.id_step = tutorial_step.id
            WHERE tutorial_step.id_tutorial = $(id_tutorial)
                AND tutorial_step.ordinal = $(step_ordinal)
                AND is_active = true
            ORDER BY tutorial_step_hint.ordinal ASC`,
          {
            id_tutorial: tutorialId,
            step_ordinal: stepOrdinal,
          }
        ),
      ])
    )
    .then(async (results) => {
      let step = results[0],
        stepHints = results[1];

      step.html = await utils.md2Html(step.text);

      for (let sh of stepHints) {
        sh.html = await utils.md2Html(sh.text);
      }
      if (!results[0]) {
        throw new Error('Tutorial step not found');
      }

      return {
        ...step,
        hints: stepHints,
      };
    });
}

async function getStudentAnswers(courseId, studentId, tutorialId) {
  return await TutorialProgress.getTutorialProgress(courseId, studentId, tutorialId);
}

async function updateStudentAnswers(courseId, studentId, tutorialId, answers) {
  return await TutorialProgress.updateTutorialAnswers(courseId, studentId, tutorialId, answers);
}

async function evaluateQuestionAnswer(
  courseId,
  studentId,
  tutorialId,
  questionId,
  answer,
  currStepOrdinal,
  id_programming_language
) {
  // const abcEvalFunc = async function(questionId, answerOrdinal) {
  //     return await questionEvalService.evlauateTutorialAbcQuestion(
  //         courseId,
  //         tutorialId,
  //         studentId,
  //         questionId,
  //         answerOrdinal
  //     );
  // };
  // const sqlEvalFunc = questionEvalService.evaluateSqlQuestion;
  // const cLangEvalFunc = questionEvalService.evaluateCLangQuestion; // to be implemented
  // console.log("YYY", courseId, studentId, tutorialId, questionId, answer, currStepOrdinal);
  const result = await questionEvalService.evaluateQuestion(
    courseId,
    questionId,
    answer,
    id_programming_language //, abcEvalFunc, sqlEvalFunc, cLangEvalFunc
  );

  const event = {
    clientTs: new Date(),
    eventName: 'Answer evaluation',
    eventData: `step ordinal: ${currStepOrdinal}, correct: ${result.score.is_correct}`,
  };

  TutorialProgress.logEvent(courseId, studentId, tutorialId, event);

  // if (result.score.is_correct) {
  //   progressToNextStep(courseId, studentId, tutorialId, currStepOrdinal);
  // }

  let answers, codeAnswer;

  if (result.question_type.toUpperCase().includes('ABC')) {
    // ABC question
    answers = answer || [];
  } else if (
    result.question_type.toUpperCase().includes('SQL') ||
    result.question_type.toUpperCase().includes('C-LANG')
  ) {
    // SQL question
    codeAnswer = answer || '';
  }

  const predicate = {
    id_course: courseId,
    id_student: studentId,
    id_tutorial: tutorialId,
    id_question: questionId,
  };

  await db.task((t) =>
    t.batch([
      db.none(
        `UPDATE tutorial_student_question
                SET ${answers ? 'student_answers = $(answers),' : ''}
                    ${codeAnswer ? 'student_answer_code = $(code_answer),' : ''}
                    is_correct = $(is_correct)
                WHERE id_course = $(id_course)
                    AND id_student = $(id_student)
                    AND id_tutorial = $(id_tutorial)
                    AND id_question = $(id_question)`,
        {
          ...predicate,
          answers: answers,
          code_answer: codeAnswer,
          is_correct: result.score.is_correct,
        }
      ),
      db.none(
        `INSERT INTO tutorial_student_question_attempt (
                    id_course, id_tutorial, id_student, id_question, is_correct)
                    VALUES ($(id_course), $(id_tutorial), $(id_student), $(id_question),
                    $(is_correct))`,
        {
          ...predicate,
          is_correct: result.score.is_correct,
        }
      ),
    ])
  );

  return result;
}
async function runPlaygroundCode(courseId, code, stdin, idCodeRunner, id_programming_language) {
  // TODO: log playground shananigans...
  const langType = (
    await db.one(
      `SELECT name
            FROM programming_language
            WHERE id = $(id_programming_language)
            `,
      {
        id_programming_language,
      }
    )
  ).name;

  if (langType.toUpperCase().includes('SQL')) {
    // SQL question
    return CodeRunnerService.execSQLCodeCodeRunner(idCodeRunner, code);
  } else if (langType.toUpperCase().includes('JSON')) {
    throw new Error('JSON not implemeted yet');
  } else {
    // j0 languages...
    return CodeRunnerService.execCustomCodeWithInput2(
      courseId,
      code,
      stdin,
      idCodeRunner,
      100,
      id_programming_language
    );
  }
}

async function updateTutorialStep(courseId, tutorialId, currentStepOrdinal, studentId) {
  const steps = parseInt(
    (
      await db.one(
        `SELECT COUNT(*) as steps
            FROM tutorial_step
            WHERE id_tutorial = $(id_tutorial)`,
        {
          id_tutorial: tutorialId,
        }
      )
    ).steps
  );
  await TutorialProgress.updateLatestStepOrdinal(
    courseId,
    studentId,
    tutorialId,
    currentStepOrdinal
  );

  if (currentStepOrdinal === steps) {
    const previouslyFinished = await db.one(
      `SELECT ts_finished :: timestamp(0) :: varchar
            FROM tutorial_student
            WHERE id_course = $(id_course)
                AND id_student = $(id_student)
                AND id_tutorial = $(id_tutorial)`,
      {
        id_course: courseId,
        id_student: studentId,
        id_tutorial: tutorialId,
      }
    );

    if (!previouslyFinished.ts_finished) {
      const finishedTs = new Date();
      db.none(
        `UPDATE public.tutorial_student
                SET is_finished = true, ts_finished = $(ts_finished)
                WHERE id_course = $(id_course)
                    AND id_student = $(id_student)
                    AND id_tutorial = $(id_tutorial)`,
        {
          id_course: courseId,
          id_student: studentId,
          id_tutorial: tutorialId,
          ts_finished: finishedTs,
        }
      );
      // Zašto ovo pohranjujem duplo? ts_finished i u realcijkoj i u mongu?
      return await TutorialProgress.markAsFinished(courseId, studentId, tutorialId, finishedTs);
    }
  }
}

async function logEvent(courseId, studentId, tutorialId, event) {
  return await TutorialProgress.logEvent(courseId, studentId, tutorialId, event);
}

module.exports = {
  listTutorials,
  start,
  getTutorial,
  getTutorialStep,
  getStudentAnswers,
  evaluateQuestionAnswer,
  updateStudentAnswers,
  // progressToNextStep,
  logEvent,
  getTutorialQuestion,
  getTutorialCodeRunner,
  runPlaygroundCode,
  updateTutorialStep,
};
