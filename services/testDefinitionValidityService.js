'use strict';
const db = require('../db').db;
const winston = require('winston');

const messageHelper = {
  quantifier: (number) => (number <= 1 ? 'is' : 'are'),
};

const DECIMALS = 4;

const roundToDecimals = (number, decimals) => {
  return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
};

const messageConstructor = {
  notEnoughQuestions: (added, required) =>
    `There ${messageHelper.quantifier(
      added
    )} ${added} question(s) added to this test, but you specified ${required}`,
  tooManyQuestions: (added, required) =>
    `There ${messageHelper.quantifier(
      added
    )} ${added} question(s) added to this test, but you specified ${required}`,
  sumOfMinQuestionsLargerThanNumOfQuestions: (sum, required) =>
    `The sum of minimum questions per all test parts is ${sum}, which is more than ${required}, the defined number of questions for the test`,
  sumOfMaxQuestionsLessThanNumOfQuestions: (sum, required) =>
    `The sum of maximum questions per all test parts is ${sum}, which is less than ${required}, the defined number of questions for the test`,
  notEnoughAvailableActiveQuestions: (available, required) =>
    `There ${messageHelper.quantifier(
      available
    )} totally ${available} question(s) available, but you requested ${required}`,
  invalidMinMax: (id_test_part, setMin, setMax) =>
    `Test part ${id_test_part} requires a minimum of ${setMin} question(s) which is larger than the maximum required questions (${setMax})`,
  noTestPartsDefined: () => `There are no test parts defined for this test.`,
  noTestQuestionsDefined: () => `There are no test questions defined for this test.`,
  invalidCompetitionFormula: () => `Competition formula doesn't seem to be valid.`,
  notEnoughMinActiveQuestionsForTestPart: (id_test_part, min, available) =>
    `There ${messageHelper.quantifier(
      available
    )} only ${available} available active question(s) for test part ${id_test_part}, but the required minimum was set to ${min}; consider lowering the minimum to ${available} or adding new questions`,
  notEnoughMaxActiveQuestionsForTestPart: (id_test_part, max, available) =>
    `There ${messageHelper.quantifier(
      available
    )} only ${available} available active question(s) for test part ${id_test_part}, but the required maximum was set to ${max}; consider lowering the maximum to ${available} or adding new questions`,
  minScoreExceedsMaxScore: (minScore, maxScore) =>
    `The minimum possible maxiumum score determined by the minimum questions per test part and its grading models is ${minScore} and exceed the maximum score of ${maxScore} in test definition; consider increasing the maximum score in test definition`,
  maxScoreNeverReachable: (reachable, defined) =>
    `The lower bound of the maximum reachable score is ${reachable}, but the test definition requires it to be ${defined}; try to adjust the minimum and maximum number of questions and grading models for each test part or lowering the defined maximum score`,
  maxScoreLowerBoundExceedsRequested: (calculatedLowerBound, reqNumOfuestions, reqMaxScore) =>
    `The calculated lower bound of the maximum score when requiring ${reqNumOfuestions} questions and using the selected grading models is ${calculatedLowerBound}, which exceeds ${reqMaxScore}, the requested maximum score`,
  maxScoreAndGradingModelLimitTheReachOfRequiredNumberOfQuestions: (
    maxScore,
    numOfQuestionsReached,
    numOfQuestionsRequired
  ) =>
    `The maximum score of ${maxScore} and the selected grading models combination limit the number of questions to ${numOfQuestionsReached} maximum, while requiring ${numOfQuestionsRequired} in the test definition`,
  nonDeterministicMaxScore: (low, high, defined) =>
    `The test definition allows to generate test instances with a non-deterministic maximum score, ranging from ${low} to ${high}, but requiring ${defined}`,
  calculatedMaxScoreTooLow: (reachable, defined) =>
    `Calculated max score too low! The calculated max score is ${reachable}, but the test definition requires it to be ${defined}!`,
  calculatedMaxScoreTooHigh: (reachable, defined) =>
    `Calculated max score too high! The calculated max score is ${reachable}, but the test definition requires it to be ${defined}!`,
};

let replaces = [
  new RegExp('\\s+', 'gi'),
  new RegExp('\\[t\\]', 'gi'),
  new RegExp('\\[n\\]', 'gi'),
  new RegExp('\\[bc\\]', 'gi'),
  new RegExp('[0-9]+[.]?[0-9]*', 'gi'),
  new RegExp('[+\\-*/()^%]', 'gi'),
  new RegExp('math\\.', 'gi'),
];

Object.getOwnPropertyNames(Math).forEach((prop) => {
  replaces.push(new RegExp(prop, 'gi'));
});

function isCompetitionFormulaValid(formula) {
  replaces.forEach((repl) => {
    formula = formula.replace(repl, '');
  });

  return !!!formula;
}

const service = {
  isCompetitionFormulaValid: isCompetitionFormulaValid,

  validate: function (test) {
    if (test.type_name === 'Peer assessment PHASE2 MASTER') {
      // TODO !!
      return new Promise((resolve, reject) => {
        resolve({
          valid: true,
        });
      });
    }

    // parse and round integers
    test.questions_no = parseInt(test.questions_no);
    test.max_score = roundToDecimals(parseFloat(test.max_score), DECIMALS);
    if (test.test_parts) {
      test.test_parts = test.test_parts.map((tp) => ({
        ...tp,
        id_test: parseInt(tp.id_test),
        id_test_part: parseInt(tp.id_test_part),
        id_grading_model: parseInt(tp.id_grading_model),
        min_questions: parseInt(tp.min_questions),
        max_questions: parseInt(tp.max_questions),
        pass_percentage: roundToDecimals(parseFloat(tp.pass_percentage), DECIMALS),
      }));
    }
    let errorMessages = [],
      warningMessages = [];

    if (
      test.is_competition === 'true' &&
      isCompetitionFormulaValid(test.eval_comp_score) === false
    ) {
      errorMessages.push(messageConstructor.invalidCompetitionFormula());
    }

    if (test.fixed_questions === true) {
      if (!test.test_questions) {
        errorMessages.push(messageConstructor.noTestQuestionsDefined());
        return new Promise((resolve, reject) => {
          resolve({
            valid: false,
            error: messageConstructor.noTestQuestionsDefined(),
          });
        });
      }

      return new Promise((resolve, reject) =>
        db
          .any(
            `SELECT MAX(ordinal) as max_ord FROM test_question
                        WHERE id_test = $(test_id)
                        GROUP BY id_test`,
            {
              test_id: test.id_test,
            }
          )
          .then(function (data) {
            // console.log(data[0].max_ord);
            if (data[0].max_ord < test.questions_no) {
              errorMessages.push(
                messageConstructor.notEnoughQuestions(data[0].max_ord, test.questions_no)
              );
            }

            if (data[0].max_ord > test.questions_no) {
              errorMessages.push(
                messageConstructor.tooManyQuestions(data[0].max_ord, test.questions_no)
              );
            }

            const usedGradingModelsIds = Array.from(
              new Set(test.test_questions.map((tq) => tq.id_grading_model))
            );
            return db.many(`
                        SELECT CAST(id AS integer), CAST(correct_score AS float)
                        FROM grading_model
                        WHERE id  = ANY ('{${usedGradingModelsIds.join(',')}}'::int[])`);
          })
          .then((gradingModels) => {
            // Create a map with grading model id as key and its correct score as value
            let correctScoresForGradingModelMap = {};
            gradingModels.forEach((d) => {
              correctScoresForGradingModelMap[d.id] = parseFloat(d.correct_score);
            });

            let maxReachableScore = 0;

            test.test_questions.forEach((tq) => {
              maxReachableScore += correctScoresForGradingModelMap[tq.id_grading_model];
            });

            if (
              maxReachableScore.toFixed(DECIMALS) < parseFloat(test.max_score).toFixed(DECIMALS)
            ) {
              errorMessages.push(
                messageConstructor.calculatedMaxScoreTooLow(maxReachableScore, test.max_score)
              );
            } else if (
              maxReachableScore.toFixed(DECIMALS) > parseFloat(test.max_score).toFixed(DECIMALS)
            ) {
              errorMessages.push(
                messageConstructor.calculatedMaxScoreTooHigh(maxReachableScore, test.max_score)
              );
            }

            if (errorMessages.length == 0) {
              resolve({
                valid: true,
              });
            } else {
              resolve({
                valid: false,
                error: `The test definition is not valid: <ul>${errorMessages
                  .map((e) => `<li>${e}</li>`)
                  .join('')}</ul>`,
              });
            }
          })
      );
    }

    if (!test.test_parts || test.test_parts.length == 0) {
      errorMessages.push(messageConstructor.noTestPartsDefined());
      return new Promise((resolve, reject) => {
        resolve({
          valid: false,
          error: messageConstructor.noTestPartsDefined(),
        });
      });
    }

    const sumOfMinQuestionsPerTestPart = test.test_parts.reduce(
      (sum, tp) => sum + tp.min_questions,
      0
    );

    // Check if the sum of minimun questions per test part
    // is larger than the number of questions defined for the test
    if (sumOfMinQuestionsPerTestPart > test.questions_no)
      errorMessages.push(
        messageConstructor.sumOfMinQuestionsLargerThanNumOfQuestions(
          sumOfMinQuestionsPerTestPart,
          test.questions_no
        )
      );

    const sumOfMaxQuestionsPerTestPart = test.test_parts.reduce(
      (sum, tp) => sum + tp.max_questions,
      0
    );

    // Check if the sum of maximum questions per test part
    // is less than the number of questions defined for the test
    if (sumOfMaxQuestionsPerTestPart < test.questions_no) {
      errorMessages.push(
        messageConstructor.sumOfMaxQuestionsLessThanNumOfQuestions(
          sumOfMaxQuestionsPerTestPart,
          test.questions_no
        )
      );
    }

    const testPartIds = test.test_parts.map((tp) => tp.id_test_part);

    return new Promise((resolve, reject) =>
      db
        .any(
          `
            SELECT 	test_part.id AS id_test_part
            , CAST(COUNT(*) AS integer) AS count
            FROM test_part LEFT JOIN node ON test_part.id_node = node.id
            LEFT JOIN question_node ON question_node.id_node = node.id
            LEFT JOIN question ON question.id = question_node.id_question
            WHERE is_active = true
            AND test_part.id = ANY ('{${testPartIds.join(',')}}'::int[])
            GROUP BY test_part.id`
        )
        .then(function (data) {
          const questionsAvailable = data.reduce((sum, row) => sum + parseInt(row.count), 0);

          // Check if there is enough questions available
          if (questionsAvailable < parseInt(test.questions_no)) {
            errorMessages.push(
              messageConstructor.notEnoughAvailableActiveQuestions(
                questionsAvailable,
                test.questions_no
              )
            );
          }

          // Check every test part for validity
          const testPartserrorMessages = test.test_parts
            .map((tp) => {
              if (tp.min_questions > tp.max_questions)
                return messageConstructor.invalidMinMax(
                  tp.id_test_part,
                  tp.min_questions,
                  tp.max_questions
                );

              // Is there enough questions available?
              var available = data.filter((x) => x.id_test_part == tp.id_test_part);

              if (
                (tp.min_questions > 0 && available.length == 0) ||
                parseInt(available.length > 0 ? available[0].count : '0') < tp.min_questions
              )
                return messageConstructor.notEnoughMinActiveQuestionsForTestPart(
                  tp.id_test_part,
                  tp.min_questions,
                  available.length > 0 ? available[0].count : 0
                );
              if (parseInt(available.length > 0 ? available[0].count : '0') < tp.max_questions)
                warningMessages.push(
                  messageConstructor.notEnoughMaxActiveQuestionsForTestPart(
                    tp.id_test_part,
                    tp.max_questions,
                    available.length > 0 ? available[0].count : 0
                  )
                );
              else return null;
            }) // Remove the nulls as these test parts are valid
            .filter((x) => x != null);

          if (testPartserrorMessages.length > 0) {
            errorMessages.push(
              `Some test parts are invalid: <ul>${testPartserrorMessages
                .map((e) => `<li>${e}</li>`)
                .join('')}</ul>`
            );
          }

          // Get a set of grading model ids that were used in test parts
          const usedGradingModelsIds = Array.from(
            new Set(test.test_parts.map((tp) => tp.id_grading_model))
          );
          return db.many(`
                    SELECT CAST(id AS integer), CAST(correct_score AS float)
                    FROM grading_model
                    WHERE id  = ANY ('{${usedGradingModelsIds.join(',')}}'::int[])`);
        })
        .then((gradingModels) => {
          // Create a map with grading model id as key and its correct score as value
          let correctScoresForGradingModelMap = {};
          gradingModels.forEach((d) => {
            correctScoresForGradingModelMap[d.id] = roundToDecimals(
              parseFloat(d.correct_score),
              DECIMALS
            );
          });

          // Check if the maximum score is theoretically and deterministically reachable
          // while not exceedng the number of questions
          let guaranteedMaxScore = 0,
            numOfQuestions = 0;

          // a) Sum number of questions and maximum score for each test part for min questions
          test.test_parts.forEach((tp) => {
            guaranteedMaxScore +=
              tp.min_questions * correctScoresForGradingModelMap[tp.id_grading_model];
            numOfQuestions += tp.min_questions;
          });

          // if it exceeds the defined maximum score, add error and skip all other checks
          if (
            roundToDecimals(guaranteedMaxScore, DECIMALS) >
            roundToDecimals(test.max_score, DECIMALS)
          ) {
            errorMessages.push(
              messageConstructor.minScoreExceedsMaxScore(guaranteedMaxScore, test.max_score)
            );
          } else {
            // b) Sum additional questions until the test has the required number of questions
            // while choosing the questions that have
            // 1. the smallest correct score - to get the lower bound of max score
            // 2. the biggest correct score - to get the upper bound of max score
            let leftGradingModelQuestions = test.test_parts.map((tp) => ({
              testPartId: tp.id_test_part,
              leftQuestionsForLower: tp.max_questions - tp.min_questions,
              leftQuestionsForUpper: tp.max_questions - tp.min_questions,
              correctScore: gradingModels.filter((x) => x.id == tp.id_grading_model)[0]
                .correct_score,
            }));

            let lowerMaxScore = guaranteedMaxScore,
              upperMaxScore = guaranteedMaxScore,
              numOfQuestionsForLower = numOfQuestions,
              numOfQuestionsForUpper = numOfQuestions;

            while (numOfQuestionsForLower < test.questions_no) {
              const availableTestPartsForLowerMaxScore = leftGradingModelQuestions
                .filter((x) => x.leftQuestionsForLower > 0)
                .sort((x, y) => x.correctScore > y.correctScore);

              if (availableTestPartsForLowerMaxScore.length == 0) break;

              const lowerChosenTestPart = availableTestPartsForLowerMaxScore[0];

              if (roundToDecimals(lowerMaxScore, DECIMALS) < test.max_score) {
                lowerMaxScore += lowerChosenTestPart.correctScore;
                leftGradingModelQuestions.filter(
                  (x) => x.testPartId == lowerChosenTestPart.testPartId
                )[0].leftQuestionsForLower--;
              } else {
                break;
              }

              numOfQuestionsForLower++;
            }

            while (numOfQuestionsForUpper < test.questions_no) {
              const availableTestPartsForUpperMaxScore = leftGradingModelQuestions
                .filter((x) => x.leftQuestionsForUpper > 0)
                .sort((x, y) => x.correctScore < y.correctScore);

              if (availableTestPartsForUpperMaxScore.length == 0) break;

              const upperChosenTestPart = availableTestPartsForUpperMaxScore[0];

              if (roundToDecimals(upperMaxScore, DECIMALS) < test.max_score) {
                upperMaxScore += upperChosenTestPart.correctScore;
                leftGradingModelQuestions.filter(
                  (x) => x.testPartId == upperChosenTestPart.testPartId
                )[0].leftQuestionsForUpper--;
              } else {
                break;
              }

              numOfQuestionsForUpper++;
            }

            if (numOfQuestionsForLower < test.questions_no) {
              errorMessages.push(
                messageConstructor.maxScoreAndGradingModelLimitTheReachOfRequiredNumberOfQuestions(
                  test.max_score,
                  numOfQuestionsForLower,
                  test.questions_no
                )
              );
            }

            if (roundToDecimals(lowerMaxScore, DECIMALS) < test.max_score) {
              errorMessages.push(
                messageConstructor.maxScoreNeverReachable(lowerMaxScore, test.max_score)
              );
            } else if (roundToDecimals(lowerMaxScore, DECIMALS) > test.max_score) {
              warningMessages.push(
                messageConstructor.maxScoreLowerBoundExceedsRequested(
                  lowerMaxScore,
                  test.questions_no,
                  test.max_score
                )
              );
            }

            if (
              roundToDecimals(lowerMaxScore, DECIMALS) != roundToDecimals(upperMaxScore, DECIMALS)
            ) {
              warningMessages.push(
                messageConstructor.nonDeterministicMaxScore(
                  lowerMaxScore,
                  upperMaxScore,
                  test.max_score
                )
              );
            }
          }

          let warning = warningMessages.map((w) => `<li>${w}</li>`);
          if (warning.length == 0) warning = null;

          if (errorMessages.length == 0) {
            resolve({
              valid: true,
              warning: warning,
            });
          } else {
            resolve({
              valid: false,
              error: `The test definition is not valid: <ul>${errorMessages
                .map((e) => `<li>${e}</li>`)
                .join('')}</ul>`,
            });
          }
        })
    );
  },
};

module.exports = service;
