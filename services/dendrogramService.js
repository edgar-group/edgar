const distance_methods = {
  1: productOfDistances,
  2: minDistance,
};

function productOfDistances(suspiciousStudents, questions, similarityPerQuestion) {
  let distances = [];
  for (let row = 0; row < suspiciousStudents.length; row++) {
    distances[row] = new Array(suspiciousStudents.length);
    for (let col = row + 1; col < suspiciousStudents.length; col++) {
      let firstStudent = suspiciousStudents[row];
      let secondStudent = suspiciousStudents[col];
      let key = `${firstStudent}+${secondStudent}`;
      let distance = 1;
      // calculate distance for those two students
      for (let question of questions) {
        if (similarityPerQuestion[question][key])
          distance = distance * (1 - similarityPerQuestion[question][key] / 100);
      }
      distances[row][col] = distance;
    }
  }
  return distances;
}

function minDistance(suspiciousStudents, questions, similarityPerQuestion) {
  let distances = [];
  for (let row = 0; row < suspiciousStudents.length; row++) {
    distances[row] = new Array(suspiciousStudents.length);
    for (let col = row + 1; col < suspiciousStudents.length; col++) {
      let firstStudent = suspiciousStudents[row];
      let secondStudent = suspiciousStudents[col];
      let key = `${firstStudent}+${secondStudent}`;
      let distance = 1;
      // calculate distance for those two students
      for (let question of questions) {
        if (
          similarityPerQuestion[question][key] &&
          distance > 1 - similarityPerQuestion[question][key] / 100
        )
          distance = 1 - similarityPerQuestion[question][key] / 100;
      }
      distances[row][col] = distance;
    }
  }
  return distances;
}

// the main clustering function
function clusterData(data) {
  let suspiciousStudents = data.suspiciousStudents;
  let questions = data.questions;
  let similarityPerQuestion = data.similarityPerQuestion;
  let studentsPerQuestion = data.studentsPerQuestion;
  let examId = data.examId;
  let distanceMethodId = data.distanceMethodId;

  // maybe will be needed later
  let dataPerQuestion = data.dataPerQuestion;

  // compute distance between each suspiciousStudents point and every other suspiciousStudents point
  // N x N matrix where N = suspiciousStudents.length
  // upper triangle matrix, row must be < col when getting suspiciousStudents!
  const distances = distance_methods[distanceMethodId](
    suspiciousStudents,
    questions,
    similarityPerQuestion
  );

  // initialize clusters to match suspiciousStudents
  const clusters = suspiciousStudents.map((datum, index) => ({
    n: datum,
    d: 0,
    indexes: [Number(index)],
  }));

  // iterate through suspiciousStudents
  for (let iteration = 0; iteration < suspiciousStudents.length; iteration++) {
    // dont find clusters to merge when only one cluster left
    if (iteration >= suspiciousStudents.length - 1) break;

    // initialize smallest distance
    let nearestDistance = Infinity;
    let nearestRow = 0;
    let nearestCol = 0;
    let nodeName = '';
    let linkExtension = '';

    // upper triangular matrix of clusters
    for (let row = 0; row < clusters.length; row++) {
      for (let col = row + 1; col < clusters.length; col++) {
        // calculate distance between clusters
        const { distance, firstIndex, secondIndex } = singleLinkClustering(
          clusters[row].indexes,
          clusters[col].indexes,
          distances
        );
        // update smallest distance
        if (distance < nearestDistance) {
          nearestDistance = distance;
          nearestRow = row;
          nearestCol = col;
          nodeName = `${suspiciousStudents[firstIndex]}+${suspiciousStudents[secondIndex]}`;
          linkExtension = `idFirstStudent=${suspiciousStudents[firstIndex]}&idSecondStudent=${suspiciousStudents[secondIndex]}`;
        }
      }
    }

    // merge nearestRow and nearestCol clusters together
    const newCluster = {
      n: nodeName,
      d: nearestDistance,
      link: `/analytics/plagiarismdetection/exam/${examId}?${linkExtension}`,
      c: [clusters[nearestRow], clusters[nearestCol]],
      indexes: [...clusters[nearestRow].indexes, ...clusters[nearestCol].indexes],
    };

    // remove nearestRow and nearestCol clusters
    // splice higher index first so it doesn't affect second splice
    clusters.splice(Math.max(nearestRow, nearestCol), 1);
    clusters.splice(Math.min(nearestRow, nearestCol), 1);

    // add new merged cluster
    clusters.push(newCluster);
  }

  // return useful information
  return clusters[0];
}

// single link clustering method (possible to add complete or average link in the future)
function singleLinkClustering(setA, setB, distances) {
  let distance = -1;
  let firstIndex = 0;
  let secondIndex = 0;
  let tmpDistance = 0;
  for (const a of setA) {
    for (const b of setB) {
      tmpDistance = distances[a][b] == undefined ? distances[b][a] : distances[a][b];
      if (distance == -1 || distance > tmpDistance) {
        distance = tmpDistance;
        if (a <= b) {
          firstIndex = a;
          secondIndex = b;
        } else {
          firstIndex = b;
          secondIndex = a;
        }
      }
    }
  }
  return { distance, firstIndex, secondIndex };
}

module.exports = { clusterData: clusterData };
