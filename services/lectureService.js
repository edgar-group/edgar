'use strict';
var db = require('../db').db;
var winston = require('winston');
var utils = require('../common/utils');
var Scorer = require('../lib/Scorer');

var service = {
  // TODO: remove duplication, there is already getQuestion method
  //getQuestion: function(ordinal, idTest, showCorrect, showInActiveQuestion) {
  getQuestion: async function (ordinal, idTest, showCorrect, showInActiveQuestion) {
    winston.debug(`getting /question/${ordinal}`);
    try {
      let question = await db.one(
        `SELECT question.id,
                       question_text,
                       is_active,
                       qt.type_name,
                       has_answers
                  FROM question
                  JOIN test_question
                    ON question.id = test_question.id_question
                  JOIN question_type qt
                    ON question.id_question_type = qt.id
                 WHERE id_test = $(id_test)
                   AND ordinal = $(ordinal)`,
        {
          id_test: idTest,
          ordinal: ordinal,
        }
      );
      let answers;
      if (question.has_answers === true) {
        answers = await db.any(
          `SELECT is_correct, answer_text, ordinal
                                        FROM question_answer
                                        WHERE id_question = $(id_question)
                                    ORDER BY ordinal`,
          {
            id_question: question.id,
          }
        );
      }
      var html = await utils.md2Html(question.question_text);
      var htmlAnswers = [];
      var correctAnswers = [];
      if (question.has_answers === true) {
        for (let i = 0; i < answers.length; i++) {
          if (answers[i].is_correct) {
            correctAnswers.push(answers[i].ordinal);
          }
          htmlAnswers.push({
            aHtml: await utils.md2Html(answers[i].answer_text),
            ordinal: i + 1,
          });
        }
      }

      return {
        success: true,
        id: question.id,
        ordinal: ordinal,
        type: question.type_name,
        content: question.is_active
          ? html
          : showInActiveQuestion
          ? `<h1>Not an active question</h1>${html}`
          : 'Not an active question',
        answers: question.is_active ? htmlAnswers : showInActiveQuestion ? htmlAnswers : [],
        correctAnswers: showCorrect ? correctAnswers : undefined,
        multiCorrect: correctAnswers.length > 1,
      };
    } catch (error) {
      winston.error(error);
      cb({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    }
  },

  updateTeacherLectureQuizInstance: function (room_name) {
    return db.tx(function (t) {
      let statements = [];
      statements.push(
        t.result(
          `UPDATE teacher_lecture_quiz_instance
                                        SET ts_ended = current_timestamp
                                        WHERE room_name = $1`,
          [room_name]
        )
      );

      return t.batch(statements);
    });
  },

  updateLectureQuizInstanceWithAnswers: function (id_lecture_quiz_instance, answers) {
    return db
      .many(
        `SELECT ordinal, has_answers
                        FROM test_instance_question tiq
                        JOIN question ON tiq.id_question = question.id
                        JOIN question_type ON question.id_question_type = question_type.id
                        WHERE id_test_instance = $(id_lecture_quiz_instance)`,
        {
          id_lecture_quiz_instance: id_lecture_quiz_instance,
        }
      )
      .then(function (answerTypes) {
        var lookup = {};
        for (let i = 0; i < answerTypes.length; ++i) {
          lookup[answerTypes[i].ordinal] = answerTypes[i].has_answers;
        }

        // transaction
        return db.tx(function (t) {
          let statements = [];

          statements.push(
            t.result(
              `UPDATE test_instance
                                 SET ts_submitted = current_timestamp
                               WHERE id =$1
                                 AND ts_submitted IS NULL`,
              [id_lecture_quiz_instance]
            )
          );

          for (var ord = 0; ord < answers.length; ++ord) {
            if (lookup[ord + 1]) {
              // if this question has answers
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answers = $1::integer[]
                                  WHERE ordinal = $2
                                    AND id_test_instance =$3`,
                  [answers[ord], ord + 1, id_lecture_quiz_instance]
                )
              );
            } else {
              // free text question
              statements.push(
                t.result(
                  `
                                 UPDATE test_instance_question
                                    SET student_answer_text = $1
                                  WHERE ordinal = $2
                                    AND id_test_instance =$3`,
                  // answers[ord] is an array with only 1 element, student answer
                  [
                    typeof answers[ord][0] === 'string' ? answers[ord][0] : '',
                    ord + 1,
                    id_lecture_quiz_instance,
                  ]
                )
              );
            }
          }
          return t.batch(statements);
        });
      });
  },

  updateTestScore: function (id_lecture_quiz_instance, lscore) {
    return db.tx(function (t) {
      let statements = [];
      statements.push(
        t.result(
          `
                                 UPDATE test_instance
                                    SET correct_no    = $1,
                                        incorrect_no  = $2,
                                        unanswered_no = $3,
                                        partial_no    = $4,
                                        score         = $5,
                                        passed        = $6,
                                        score_perc    = $7
                                  WHERE id =$8`,
          [
            lscore.correct_no,
            lscore.incorrect_no,
            lscore.unanswered_no,
            lscore.partial_no,
            lscore.score,
            lscore.passed,
            lscore.score_perc,
            id_lecture_quiz_instance,
          ]
        )
      );
      for (var ord = 0; ord < lscore.qscores.length; ++ord) {
        let qs = lscore.qscores[ord];
        statements.push(
          t.result(
            `
                                 UPDATE test_instance_question
                                    SET is_correct    = $1,
                                        is_incorrect  = $2,
                                        is_partial    = $3,
                                        is_unanswered = $4,
                                        score         = $5,
                                        score_perc    = $6,
                                        hint          = $7,
                                        c_eval_data   = $8
                                  WHERE id =$9`,
            [
              qs.is_correct,
              qs.is_incorrect,
              qs.is_partial,
              qs.is_unanswered,
              qs.score,
              qs.score_perc,
              qs.hint,
              JSON.stringify(qs.c_outcome), // do I need this?
              qs.id_test_instance_question,
            ]
          )
        );
      }
      return t.batch(statements);
    });
  },

  gradeLectureQuiz: function (req) {
    winston.debug('before gradeTest');
    var student_score;
    return Scorer.gradeLectureQuiz(req.session.id_lecture_quiz_instance, db)
      .then(function (lscore) {
        //respMsg.score = lscore.score;
        student_score = lscore;
        winston.debug('before updateTestScore ' + lscore);
        return service.updateTestScore(req.session.id_lecture_quiz_instance, lscore);
      })
      .then(function (score) {
        // Clear the test data for (all) tests
        // TODO: enable multiple ongoing tests?
        winston.debug('UpdateTestScore DONE! ');
        // winston.info('before clearTestData');
        // return OngoingTests.clearTestData(req.session);
      })
      .then(function () {
        // Finally, erase the session vars, and respond:
        // if (req.session.currAnswers) {
        //   req.session.currAnswers[req.session.id_test_instance] = undefined;
        // }
        if (req.session.anon_alt_id) {
          req.session.anon_alt_id = undefined;
        }
        req.session.id_lecture_quiz_instance = undefined;
        req.session.id_lecture_quiz = undefined;
        req.session.room_name = undefined;
        req.session.id_test_instance = undefined;
        return new Promise((resolve, reject) => {
          resolve({
            success: true,
            score: student_score,
            async_submit: false,
          });
        });
      });
  },
};

module.exports = service;
