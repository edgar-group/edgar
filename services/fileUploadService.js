'use strict';
var winston = require('winston');
var multer = require('multer');
var globals = require('../common/globals');
var fs = require('fs');
// TODO: finish moving stuff from file upload routes to this service

var storageTestInstancePrivate = multer.diskStorage({
  destination: function (req, file, cb) {
    var dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${globals.TI_UPLOAD_FOLDER}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${req.current_id_test_instance}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${req.params.ordinal}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, 'upload_' + req.params.fileOrdinal + '_' + file.originalname);
  },
});

const FILE_SIZE_LIMIT = 50 * 1024 * 1024; // 50MB;

var uploadTestInstancePrivate = multer({
  storage: storageTestInstancePrivate,
  limits: {
    fileSize: FILE_SIZE_LIMIT,
  },
}).single('testInstanceFile');

var service = {
  uploadTestInstanceFile: async function (req, res, id_test_instance) {
    req.current_id_test_instance = id_test_instance; // used in multer destination, see above
    uploadTestInstancePrivate(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        winston.error('error! ' + err);
        if (err.code === 'LIMIT_FILE_SIZE') {
          res.json({
            success: false,
            error: {
              message:
                'Upload failed. File is too large. The current limit is: ' + FILE_SIZE_LIMIT + 'B.',
            },
          });
        } else {
          res.json({
            success: false,
            error: {
              message: 'Upload failed:: ' + JSON.stringify(err),
            },
          });
        }
      } else {
        res.json({
          success: true,
          data: req.file,
        });
      }
    });
  },
  deleteTestInstanceFile: async function (req, res, id_test_instance) {
    var dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${req.params.ordinal}/`;
    if (fs.existsSync(dir)) {
      fs.readdir(dir, function (err, files) {
        //handling error
        if (err) {
          res.json({
            success: false,
            error: {
              message: 'Unable to scan directory ' + JSON.stringify(err),
            },
          });
        }
        for (let i = 0; files && i < files.length; ++i) {
          winston.debug(files[i]);
          if (files[i].startsWith('upload_' + req.params.fileOrdinal + '_')) {
            winston.debug('deleting: ' + dir + files[i]);
            fs.unlinkSync(dir + files[i]);
            break;
          }
        }
        res.json({
          success: true,
        });
      });
    }
  },
  downloadTestInstanceFile: async function (res, id_test_instance, question_ordinal, file_ordinal) {
    var dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/${globals.TI_UPLOAD_FOLDER}/${id_test_instance}/${question_ordinal}/`;
    if (fs.existsSync(dir)) {
      fs.readdir(dir, function (err, files) {
        //handling error
        if (err) {
          res.json({
            success: false,
            error: {
              message: 'Unable to scan directory ' + JSON.stringify(err),
            },
          });
        }
        for (let i = 0; files && i < files.length; ++i) {
          winston.debug(files[i]);
          if (files[i].startsWith('upload_' + file_ordinal + '_')) {
            winston.debug('downloading: ' + dir + files[i]);
            res.download(dir + files[i], files[i].replace('upload_' + file_ordinal + '_', ''));
            return;
          }
        }
        res.status(404).send();
      });
    }
  },
};

module.exports = service;
