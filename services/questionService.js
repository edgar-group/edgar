'use strict';
var db = require('../db').db;
const globals = require('../common/globals');

async function _getQuestionBasic(id_question, id_test_instance, ordinal) {
  return db.oneOrNone(
    `SELECT
                        ${
                          id_test_instance
                            ? ` test_instance.id_test, test_instance.id_student, `
                            : ''
                        }
                        question.*,
                        question.ts_created :: timestamp(0) :: varchar,
                        question.ts_modified :: timestamp(0) :: varchar,
                        (SELECT MAX(next.id) FROM question next  WHERE next.id_prev_question = question.id) as id_next_question,
                        type_name,
                        has_answers,
                        ucre.first_name || ' ' || ucre.last_name as user_created,
                        umod.first_name || ' ' ||  umod.last_name as user_modified,
                        (select string_agg(rpath, '<br/>' order by rpath)
                                from v_node_path
                                join question_node on question.id = id_question
                                where v_node_path.id = question_node.id_node
                                ) as paths
                    FROM question
                    JOIN question_type on id_question_type = question_type.id
                    JOIN app_user ucre on id_user_created = ucre.id
                    JOIN app_user umod on id_user_modified = umod.id

                    ${
                      id_test_instance
                        ? ` JOIN test_instance_question
                        ON test_instance_question.id_test_instance = $(id_test_instance)
                        AND test_instance_question.ordinal = $(ordinal)
                        AND test_instance_question.id_question = question.id
                        JOIN test_instance
                          ON test_instance_question.id_test_instance = test_instance.id
                        `
                        : ''
                    }
                    ${id_question ? ' WHERE question.id = $(id_question)' : ''}
                    `,
    {
      id_question,
      id_test_instance,
      ordinal,
    }
  );
}

module.exports = {
  getQuestionBasic: async function (id_question) {
    return _getQuestionBasic(id_question);
  },
  getQuestionBasicTestInstance: async function (id_test_instance, ordinal) {
    return _getQuestionBasic(undefined, id_test_instance, ordinal);
  },
  getQuestion: async function (questionId, courseId, username) {
    let promises = [];
    promises.push(
      db.any(
        `SELECT id as value, tag as name
                    FROM course_tag
                    WHERE id_course = $(id_course)
                    ORDER BY name
                    `,
        {
          id_course: courseId,
        }
      )
    );
    promises.push(
      db.any(
        `SELECT course_tag.id
                    FROM question_course_tag
                    JOIN course_tag
                        ON question_course_tag .id_course_tag = course_tag.id
                    WHERE id_question = $(id_question)
                        -- id_course = $(id_course), let the user SEE all tags, across courses
                    `,
        {
          id_course: courseId,
          id_question: questionId,
        }
      )
    );
    promises.push(
      db.oneOrNone(
        ` select question.*,
                        question.ts_created :: timestamp(0) :: varchar,
                        question.ts_modified :: timestamp(0) :: varchar,
                        (SELECT MAX(next.id) FROM question next  WHERE next.id_prev_question = question.id) as id_next_question,
                        type_name,
                        has_answers,
                        ucre.first_name || ' ' || ucre.last_name as user_created,
                        umod.first_name || ' ' ||  umod.last_name as user_modified,
                        (select string_agg(rpath, '<br/>' order by rpath)
                                from v_node_path
                                join question_node on question.id = id_question
                                where v_node_path.id = question_node.id_node
                                ) as paths,
                        last_used :: timestamp(0) :: varchar,
                        coalesce(count_used, 0) ::int as count_used
                    From question
                    join question_type on id_question_type = question_type.id
                    join app_user ucre on id_user_created = ucre.id
                    join app_user umod on id_user_modified = umod.id
                    left join (
                            select id_question, max(ts_submitted) as last_used, count(*) as count_used
                            from test_instance_question tiq
                                join test_instance ti on tiq.id_test_instance = ti.id
                                group by id_question) as usage
                    on usage.id_question = question.id
                    WHERE question.id = $(id_question)
                    AND chk_can_see_question($(username), $(id_question)) = true
                    `,
        {
          id_question: questionId,
          username: username,
        }
      )
    );
    let [tags, selectedTags, question] = await Promise.all(promises);
    let answers;
    if (question && question.has_answers === true) {
      answers = await db.any(
        `SELECT id, is_correct, answer_text, ordinal, threshold_weight, penalty_percentage::int
                            FROM question_answer
                        WHERE id_question = $(id_question)
                        ORDER BY ordinal`,
        {
          id_question: questionId,
        }
      );
    } else if (question && question.type_name.toUpperCase().indexOf('SQL') >= 0) {
      answers = await db.any(
        `SELECT sql_answer, sql_alt_assertion, sql_test_fixture, sql_alt_presentation_query, check_tuple_order, id_check_column_mode, code_runner.name as code_runner_name
                    FROM sql_question_answer
                    LEFT JOIN code_runner
                        ON sql_question_answer.id_code_runner = code_runner.id
                WHERE id_question = $(id_question)`,
        {
          id_question: questionId,
        }
      );
    }
    return {
      tags: tags,
      selectedTags: selectedTags.map((x) => x.id),
      retired: question.id_next_question ? true : false,
      question: question,
      answers: question.has_answers ? answers : [],
      sql_answer: question.type_name.toUpperCase().indexOf('SQL') >= 0 ? answers[0] : null,
      code_runner_name:
        question.type_name.toUpperCase().indexOf('SQL') >= 0
          ? answers[0].code_runner_name
          : undefined,
    };
  },
  prepareSaveFunctionArguments: function (req_body, appUserId) {
    let correctAnswers = [],
      answers = [],
      thresholds = [],
      penalty_percentages = [],
      // C-stuff:
      _c_prefix,
      _c_suffix,
      _c_source,
      _c_test_type_ids,
      _c_percentages,
      _c_allow_diff_orders,
      _c_allow_diff_letter_sizes,
      _c_trim_whitespaces,
      _c_comments,
      _c_regexs,
      _c_question_test_ids,
      _c_inputs,
      _c_outputs,
      _c_generator_test_file_ids,
      _c_argumentss,
      _c_random_test_type_ids,
      _c_low_bounds,
      _c_upper_bounds,
      _c_elem_counts;
    // /C

    for (let i = 0; i < 100; ++i) {
      if (req_body['answer' + i]) {
        correctAnswers.push(parseInt(req_body[`corrAnswerCheckboxes${i}`]) === i ? 1 : 0);
        answers.push(req_body['answer' + i]);
        thresholds.push(1);
        //thresholds.push(req_body['threshold_weight' + i]);
        penalty_percentages.push(req_body['penalty_percentage' + i]);
      } else {
        break;
      }
    }

    // C-stuff:
    _c_question_test_ids = [];
    _c_comments = [];
    _c_percentages = [];
    _c_allow_diff_orders = [];
    _c_allow_diff_letter_sizes = [];
    _c_trim_whitespaces = [];
    _c_regexs = [];
    _c_test_type_ids = [];
    _c_inputs = [];
    _c_outputs = [];
    _c_generator_test_file_ids = [];
    _c_argumentss = [];
    _c_random_test_type_ids = [];
    _c_low_bounds = [];
    _c_upper_bounds = [];
    _c_elem_counts = [];

    for (let i = 0; i < 100; ++i) {
      if (req_body['c_question_test_id' + i]) {
        _c_comments.push(req_body['comment' + i]);
        _c_test_type_ids.push(req_body['id_test_type' + i]);
        _c_allow_diff_orders.push(req_body['diff_order' + i] === 'on' ? 'true' : 'false');
        _c_allow_diff_letter_sizes.push(req_body['check_letters' + i] === 'on' ? 'true' : 'false');
        _c_question_test_ids.push(req_body['c_question_test_id' + i]);
        _c_trim_whitespaces.push(req_body['trim_whitespace' + i] === 'on' ? 'true' : 'false');
        _c_percentages.push(req_body['percent' + i]);
        _c_inputs.push(req_body['c_input' + i]);
        _c_outputs.push(req_body['c_output' + i]);
        _c_random_test_type_ids.push(req_body['randGenType' + i]);
        _c_low_bounds.push(req_body['lowBound' + i]);
        _c_upper_bounds.push(req_body['upperBound' + i]);
        _c_elem_counts.push(req_body['count' + i]);
        _c_generator_test_file_ids.push(req_body['generators' + i]);
        _c_argumentss.push(req_body['params' + i]);
        _c_regexs.push(req_body['regex_override' + i]);
      } else {
        break;
      }
    }
    return [
      req_body.id_question,
      req_body.is_active === 'true' ? true : false,
      req.body.upload_files_no,
      req.body.required_upload_files_no,
      req_body.question_text,
      null, // comment
      req_body.text_answer,
      correctAnswers.join(globals.CSV.DELIM),
      answers.join(globals.CSV.DELIM),
      thresholds.join(globals.CSV.DELIM),
      penalty_percentages.join(globals.CSV.DELIM),
      req_body.sql_answer,
      req_body.sql_alt_assertion,
      req_body.sql_test_fixture,
      req_body.sql_alt_presentation_query,
      req_body.check_tuple_order === 'true' ? true : false,
      req_body.id_check_column_mode,
      // C-stuff:
      req_body.prefix,
      req_body.suffix,
      req_body.c_answer,
      _c_test_type_ids.join(globals.CSV.DELIM),
      _c_percentages.join(globals.CSV.DELIM),
      _c_allow_diff_orders.join(globals.CSV.DELIM),
      _c_allow_diff_letter_sizes.join(globals.CSV.DELIM),
      _c_trim_whitespaces.join(globals.CSV.DELIM),
      _c_comments.join(globals.CSV.DELIM),
      _c_regexs.join(globals.CSV.DELIM),
      _c_question_test_ids.join(globals.CSV.DELIM),
      _c_inputs.join(globals.CSV.DELIM),
      _c_outputs.join(globals.CSV.DELIM),
      _c_generator_test_file_ids.join(globals.CSV.DELIM),
      _c_argumentss.join(globals.CSV.DELIM),
      _c_random_test_type_ids.join(globals.CSV.DELIM),
      _c_low_bounds.join(globals.CSV.DELIM),
      _c_upper_bounds.join(globals.CSV.DELIM),
      _c_elem_counts.join(globals.CSV.DELIM),
      // /C-stuff
      appUserId,
      req_body.question_tags ? req_body.question_tags.join(globals.CSV.DELIM) : null,
      // JSON stuff:
      req_body.json_answer,
      req_body.json_alt_assertion,
      req_body.json_test_fixture,
      req_body.json_alt_presentation_query,
      req_body.assert_deep === 'true' ? true : false,
      req_body.assert_strict === 'true' ? true : false,
      // C-stuff again, runtime constraints and multi-lang stuff
      // mocked for the time being, as tuts support only SQL so far
      JSON.stringify([]), // rtc constraints
      JSON.stringify([]), // rtc test constraints
      null, // langId: text
      '', // public tests
      null, // id_Scale
      null, // data_object
      null, // eval_scripot
      null, // req.body.id_script_programming_language,
      true, // bump_version
    ];
  },
};
