'use strict';
var db = require('../db').db;
const ExerciseProgress = require.main.require('./models/ExerciseProgressModel');

const questionIdsSql = `SELECT DISTINCT id_question
FROM exercise_student_question
WHERE id_exercise = $(id_exercise)
ORDER BY id_question`;

async function byGroups(exerciseId, courseId, academicYearId) {
  const questionIds = await db.any(questionIdsSql, {
    id_exercise: exerciseId,
  });

  if (questionIds.length === 0) {
    return {
      questionIds: [],
    };
  }

  const mainSql = `SELECT class_group,
        COUNT(es.*) started, 
        SUM(CASE WHEN es.is_finished THEN 1 ELSE 0 END) finished,
        id_question,
        SUM(CASE WHEN esq.is_correct THEN 1 ELSE 0 END) || '/' || COUNT(esq.*) attempts
    FROM student JOIN student_course
        ON student.id = student_course.id_student
        AND student_course.id_course = $(id_course)
        AND id_academic_year = $(id_academic_year)
    JOIN exercise_student es
        ON es.id_exercise = $(id_exercise)
        AND es.id_student = student.id
    LEFT JOIN exercise_student_question esq
        ON esq.id_student = student.id
        AND esq.id_exercise = $(id_exercise)
    GROUP BY class_group, id_question
    ORDER BY class_group ASC, id_question ASC, attempts DESC`;

  const rv = await db.any(
    `SELECT *
        FROM crosstab($$
        ${mainSql}
        $$, $$
        ${questionIdsSql}
        $$) AS pivotTable(class_group text, started bigint, finished bigint, ${questionIds
          .map((qid) => `"${qid.id_question}" text`)
          .join(', ')})`,
    {
      id_exercise: exerciseId,
      id_course: courseId,
      id_academic_year: academicYearId,
    }
  );

  return {
    questionIds: questionIds,
    data: rv,
  };
}

async function byStudents(exerciseId, courseId, academicYearId) {
  const questionIds = await db.any(questionIdsSql, {
    id_exercise: exerciseId,
  });

  if (questionIds.length === 0) {
    return {
      questionIds: [],
    };
  }

  const mainSql = `SELECT student.id as id_student, alt_id2, first_name, last_name,
        (first_name || ' ' || last_name || '<br />(' || alt_id2 || ')') student,
        class_group, es.is_finished, 
        es.ts_finished :: timestamp(0) :: varchar, esq.id_question,
        SUM(CASE WHEN esqa.is_correct THEN 1 ELSE 0 END) || '/' || COUNT(esqa.*) 
            AS attempts
    FROM student JOIN student_course
        ON student.id = student_course.id_student
        AND student_course.id_course = $(id_course)
        AND id_academic_year = $(id_academic_year)
    JOIN exercise_student es
        ON es.id_exercise = $(id_exercise)
        AND es.id_student = student.id
    LEFT JOIN exercise_student_question esq
        ON esq.id_exercise = $(id_exercise)
        AND esq.id_student = student.id
    LEFT JOIN exercise_student_question_attempt esqa
        ON esq.id_question = esqa.id_question
        AND esq.id_student = esqa.id_student
        AND esq.id_exercise = esqa.id_exercise
    WHERE esq.id_exercise = $(id_exercise)
    GROUP BY student.id, alt_id2, first_name, last_name, student, class_group, 
        is_finished, ts_finished, esq.id_question
    ORDER BY alt_id2, esq.id_question, attempts DESC`;

  const rv = await db.any(
    `SELECT *
        FROM crosstab($$
        ${mainSql}
        $$, $$
        ${questionIdsSql}
        $$) AS pivotTable(id_student int, alt_id2 text, first_name text, last_name text, 
            student text, class_group text, is_finished boolean, ts_finished varchar, ${questionIds
              .map((qid) => `"${qid.id_question}" text`)
              .join(', ')})
        ORDER BY alt_id2`,
    {
      id_exercise: exerciseId,
      id_course: courseId,
      id_academic_year: academicYearId,
    }
  );

  return {
    questionIds: questionIds,
    data: rv,
  };
}

async function studentLog(exerciseId, courseId, studentId) {
  const rv = await ExerciseProgress.getLog(exerciseId, courseId, studentId);
  return rv;
}

module.exports = {
  byGroups: byGroups,
  byStudents: byStudents,
  studentLog: studentLog,
};
