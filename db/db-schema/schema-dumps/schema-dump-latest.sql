--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Ubuntu 15.3-1.pgdg22.04+1)
-- Dumped by pg_dump version 15.3 (Ubuntu 15.3-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: audit; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS pg_cron;


ALTER SCHEMA audit OWNER TO postgres;

--
-- Name: SCHEMA audit; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA audit IS 'Out-of-table audit/history logging tables and trigger functions';


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: chk_can_see_instance_return_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.chk_can_see_instance_return_type AS (
	can_see boolean,
	show_solutions boolean
);


ALTER TYPE public.chk_can_see_instance_return_type OWNER TO postgres;

--
-- Name: chk_get_lecture_quiz_test_instance_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.chk_get_lecture_quiz_test_instance_type AS (
	err_code integer,
	id_student integer,
	alt_id_student character varying,
	id_room integer,
	id_test_instance integer
);


ALTER TYPE public.chk_get_lecture_quiz_test_instance_type OWNER TO postgres;

--
-- Name: chk_get_test_instance_return_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.chk_get_test_instance_return_type AS (
	err_code integer,
	id_test_instance integer,
	comment text
);


ALTER TYPE public.chk_get_test_instance_return_type OWNER TO postgres;

--
-- Name: create_teacher_lecture_quiz_test_instance_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.create_teacher_lecture_quiz_test_instance_type AS (
	err_code integer,
	id_teacher_lecture_instance integer,
	comment text
);


ALTER TYPE public.create_teacher_lecture_quiz_test_instance_type OWNER TO postgres;

--
-- Name: enum_ticket_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.enum_ticket_status AS ENUM (
    'reopened',
    'open',
    'closed'
);


ALTER TYPE public.enum_ticket_status OWNER TO postgres;

--
-- Name: audit_table(regclass); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit.audit_table(target_table regclass) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, BOOLEAN 't', BOOLEAN 't');
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass) OWNER TO postgres;

--
-- Name: FUNCTION audit_table(target_table regclass); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION audit.audit_table(target_table regclass) IS '
Add auditing support to the given table. Row-level changes will be logged with full client query text. No cols are ignored.
';


--
-- Name: audit_table(regclass, boolean, boolean); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, $2, $3, ARRAY[]::text[]);
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) OWNER TO postgres;

--
-- Name: audit_table(regclass, boolean, boolean, text[]); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  stm_targets text = 'INSERT OR UPDATE OR DELETE OR TRUNCATE';
  _q_txt text;
  _ignored_cols_snip text = '';
BEGIN
    EXECUTE 'DROP TRIGGER IF EXISTS stamp ON ' || quote_ident(target_table::TEXT); -- modified
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_row ON ' || quote_ident(target_table::TEXT);
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_stm ON ' || quote_ident(target_table::TEXT);

    IF audit_rows THEN
        IF array_length(ignored_cols,1) > 0 THEN
            _ignored_cols_snip = ', ' || quote_literal(ignored_cols);
        END IF;
        _q_txt = 'CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON ' ||
                 quote_ident(target_table::TEXT) ||
                 ' FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func(' ||
                 quote_literal(audit_query_text) || _ignored_cols_snip || ');';
        RAISE NOTICE '%',_q_txt;
        EXECUTE _q_txt;
        stm_targets = 'TRUNCATE';
    ELSE
    END IF;
    _q_txt = 'CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON ' ||    -- modified
             target_table ||
             ' FOR EACH ROW EXECUTE PROCEDURE audit.stamp()';
    RAISE NOTICE '%',_q_txt;
    EXECUTE _q_txt;

    _q_txt = 'CREATE TRIGGER audit_trigger_stm AFTER ' || stm_targets || ' ON ' ||
             target_table ||
             ' FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('||
             quote_literal(audit_query_text) || ');';
    RAISE NOTICE '%',_q_txt;
    EXECUTE _q_txt;

END;
$$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) OWNER TO postgres;

--
-- Name: FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) IS '
Add auditing support to a table.

Arguments:
   target_table:     Table name, schema qualified if not on search_path
   audit_rows:       Record each row change, or only audit at a statement level
   audit_query_text: Record the text of the client query that triggered the audit event?
   ignored_cols:     Columns to exclude from update diffs, ignore updates that change only ignored cols.
';


--
-- Name: if_modified_func(); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit.if_modified_func() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public'
    AS $$
DECLARE
    audit_row audit.logged_actions;
    include_values boolean;
    log_diffs boolean;
    h_old hstore;
    h_new hstore;
    excluded_cols text[] = ARRAY[]::text[];
BEGIN
    IF TG_WHEN <> 'AFTER' THEN
        RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
    END IF;

    audit_row = ROW(
        nextval('audit.logged_actions_event_id_seq'), -- event_id
        TG_TABLE_SCHEMA::text,                        -- schema_name
        TG_TABLE_NAME::text,                          -- table_name
        TG_RELID,                                     -- relation OID for much quicker searches
        session_user::text,                           -- session_user_name
        current_timestamp,                            -- action_tstamp_tx
        statement_timestamp(),                        -- action_tstamp_stm
        clock_timestamp(),                            -- action_tstamp_clk
        txid_current(),                               -- transaction ID
        current_setting('application_name'),          -- client application
        inet_client_addr(),                           -- client_addr
        inet_client_port(),                           -- client_port
        current_query(),                              -- top-level query or queries (if multistatement) from client
        substring(TG_OP,1,1),                         -- action
        NULL, NULL,                                   -- row_data, changed_fields
        'f'                                           -- statement_only
        );

    IF NOT TG_ARGV[0]::boolean IS DISTINCT FROM 'f'::boolean THEN
        audit_row.client_query = NULL;
    END IF;

    IF TG_ARGV[1] IS NOT NULL THEN
        excluded_cols = TG_ARGV[1]::text[];
    END IF;

    IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
        audit_row.changed_fields =  (hstore(NEW.*) - audit_row.row_data) - excluded_cols;
        IF audit_row.changed_fields = hstore('') THEN
            -- All changed fields are ignored. Skip this update.
            RETURN NULL;
        END IF;
    ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
    ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(NEW.*) - excluded_cols;
    ELSIF (TG_LEVEL = 'STATEMENT' AND TG_OP IN ('INSERT','UPDATE','DELETE','TRUNCATE')) THEN
        audit_row.statement_only = 't';
    ELSE
        RAISE EXCEPTION '[audit.if_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
        RETURN NULL;
    END IF;
    INSERT INTO audit.logged_actions VALUES (audit_row.*);
    RETURN NULL;
END;
$$;


ALTER FUNCTION audit.if_modified_func() OWNER TO postgres;

--
-- Name: FUNCTION if_modified_func(); Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON FUNCTION audit.if_modified_func() IS '
Track changes to a table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: boolean, whether to log the query text. Default ''t''.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a ''FOR EACH STATEMENT'' rather than ''FOR EACH ROW'' trigger if you do not
want to log row values.

Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger its self.
';


--
-- Name: stamp(); Type: FUNCTION; Schema: audit; Owner: postgres
--

CREATE FUNCTION audit.stamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            NEW.ts_created := current_timestamp;
            IF (NEW.user_created IS NULL) THEN
                NEW.user_created := current_user;
            END IF;
        END IF;
        NEW.ts_modified := current_timestamp;
        IF (NEW.user_modified IS NULL) THEN
            NEW.user_modified := current_user;
        END IF;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION audit.stamp() OWNER TO postgres;

--
-- Name: accept_all_answers(integer, integer, integer, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.accept_all_answers(_id_test integer, _id_question integer, _id_app_user integer, _reason text, _include_unanswered boolean) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$

DECLARE
    _row RECORD;
BEGIN
    FOR _row IN
        SELECT TIQ.id AS id_test_instance_question,
            CASE
                WHEN TIQ.is_incorrect = true THEN -GM.incorrect_score + GM.correct_score
                WHEN TIQ.is_unanswered = true THEN GM.correct_score
            END AS score_delta
        FROM test_instance_question AS TIQ
        JOIN grading_model AS GM
          ON TIQ.id_grading_model = GM.id
        JOIN test_instance AS TI
          ON TI.id = TIQ.id_test_instance
   LEFT JOIN test_correction AS TC
          ON TC.id_test_instance_question = TIQ.id
       WHERE TIQ.is_correct = false AND (TIQ.is_incorrect = true OR (TIQ.is_unanswered = true AND _include_unanswered = true))
         AND TI.id_test = _id_test AND TIQ.id_question = _id_question
         AND TC.id_test_instance_question IS NULL
    LOOP
        PERFORM save_correction(_reason, _row.score_delta, _id_app_user, ARRAY[_row.id_test_instance_question]);
    END LOOP;
    RETURN 0;
END;
$$;


ALTER FUNCTION public.accept_all_answers(_id_test integer, _id_question integer, _id_app_user integer, _reason text, _include_unanswered boolean) OWNER TO postgres;

--
-- Name: accept_answer(integer, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.accept_answer(_id_test_instance_question integer, _id_app_user integer, _reason text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _score_delta NUMERIC(10,3):= 0;
BEGIN

    SELECT
      CASE
           WHEN TIQ.is_incorrect = TRUE  THEN -GM.incorrect_score + GM.correct_score
           WHEN TIQ.is_unanswered = TRUE THEN GM.correct_score
           WHEN TIQ.is_partial = TRUE THEN GM.correct_score - TIQ.score
      END INTO _score_delta
    FROM test_instance_question AS TIQ
    JOIN grading_model AS GM
      ON TIQ.id_grading_model = GM.id
    LEFT JOIN test_correction AS TC
      ON TC.id_test_instance_question = TIQ.id
   WHERE TIQ.is_correct = FALSE
     AND TC.id_test_instance_question IS NULL
     AND TIQ.id = _id_test_instance_question;

    IF _score_delta > 0 THEN
        PERFORM save_correction(_reason, _score_delta, _id_app_user, ARRAY[_id_test_instance_question]);
    END IF;
 RETURN 0;
END;
$$;


ALTER FUNCTION public.accept_answer(_id_test_instance_question integer, _id_app_user integer, _reason text) OWNER TO postgres;

--
-- Name: add_c_question_test(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.add_c_question_test(_c_question_answer_id integer, _id_user_modified integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  new_id INTEGER;
BEGIN
    INSERT INTO c_question_test (c_question_answer_id , c_test_type_id , percentage , allow_diff_order , allow_diff_letter_size , trim_whitespace  )
    VALUES (_c_question_answer_id , 1 , 100.00 , FALSE , FALSE , TRUE )  RETURNING id INTO new_id;

    INSERT INTO fixed_test( c_question_test_id, input, output)
    VALUES (new_id, 'in', 'out');

    RETURN new_id;
END;
$$;


ALTER FUNCTION public.add_c_question_test(_c_question_answer_id integer, _id_user_modified integer) OWNER TO postgres;

--
-- Name: apply_manual_grading(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.apply_manual_grading(_id_test integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  i INTEGER;
BEGIN
    -- Potentially update grading model for classic/test_part tests
    UPDATE test_instance_question
        SET id_grading_model =  test_part.id_grading_model
        FROM test_instance, test_part
        WHERE test_instance_question.id_test_part = test_part.id
        AND test_instance_question.id_grading_model <> test_part.id_grading_model
        AND test_instance_question.id_test_instance = test_instance.id
        AND test_instance.id_test = _id_test;

    -- Potentially update grading model for fixed qs tests
    UPDATE test_instance_question
        SET id_grading_model =  test_question.id_grading_model
        FROM test_question, test_instance
        WHERE test_question.id_test = _id_test
        AND test_instance_question.id_test_instance = test_instance.id
        AND test_instance.id_test = _id_test
        AND test_instance_question.ordinal = test_question.ordinal
        AND test_instance_question.id_grading_model <> test_question.id_grading_model;


    -- Update on the question level:
    UPDATE test_instance_question
    SET score = coalesce(man_grade.score, 0),
        score_perc = CASE WHEN (grading_model.correct_score = 0) THEN 1 ELSE (coalesce(man_grade.score, 0)) / grading_model.correct_score END,
        hint = 'Score assigned manually to: ' ||
                coalesce(man_grade.score, 0) || ' ('  || ROUND(100 * CASE WHEN (grading_model.correct_score = 0) THEN 1 ELSE (coalesce(man_grade.score, 0)) / grading_model.correct_score END, 1)
                || '%)'
    FROM test_instance_question proxy  -- I'm introducing "proxy" to achieve left join
      JOIN grading_model
        ON proxy.id_grading_model = grading_model.id
      JOIN test_instance
        ON proxy.id_test_instance = test_instance.id
       AND test_instance.id_test = _id_test
      LEFT JOIN test_instance_question_manual_grade man_grade
        ON proxy.id = man_grade.id_test_instance_question
    WHERE test_instance_question.id = proxy.id;

   UPDATE test_instance_question
      SET is_correct = CASE WHEN (test_instance_question.score_perc = 1) THEN true ELSE false END,
          is_incorrect = CASE WHEN (test_instance_question.score_perc <= 0) THEN true ELSE false END,
          is_partial = CASE WHEN (test_instance_question.score_perc > 0 AND test_instance_question.score_perc < 1) THEN true ELSE false END,
          is_unanswered = false
     FROM test_instance
    WHERE test_instance_question.id_test_instance = test_instance.id
      AND test_instance.id_test = _id_test;

    -- And on the test level, first score:
    UPDATE test_instance
       SET score = (SELECT SUM(score)
                   FROM test_instance_question
                   WHERE id_test_instance = test_instance.id)
    WHERE id_test = _id_test;
    -- ... and then score perc:
    UPDATE test_instance
       SET score_perc = CASE WHEN (test.max_score = 0) THEN 1 ELSE score / test.max_score END
      FROM test
     WHERE test_instance.id_test = test.id
       AND test_instance.id_test = _id_test;

    RETURN 0;
END;
$$;


ALTER FUNCTION public.apply_manual_grading(_id_test integer) OWNER TO postgres;

--
-- Name: chk_can_see(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chk_can_see(_username character varying, _id_course integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _id_app_user INTEGER;
    _role_name VARCHAR(50);
BEGIN
    -- SELECT role.role_name, id_app_user
    --   INTO _role_name, _id_app_user
    --   FROM app_user
    --   JOIN app_user_role ON app_user.id = app_user_role.id_app_user
    --   JOIN role          ON app_user_role.id_role = role.id
    --  WHERE username = _username;
    IF ((SELECT COUNT(*)
        FROM perm_user_course puc
        JOIN app_user
          ON puc.id_user = app_user.id
       WHERE puc.id_course = _id_course
         AND app_user.username = _username) > 0) THEN
        RETURN true;
    ELSE
      RETURN (
      SELECT COUNT(*) > 0
       From student
       JOIN student_course
         ON student.id = student_course.id_student
       JOIN course
         ON student_course.id_course = course.id
      WHERE (alt_id = _username OR alt_id2 = _username)
        AND course.id = _id_course);
    END IF;
END;
$$;


ALTER FUNCTION public.chk_can_see(_username character varying, _id_course integer) OWNER TO postgres;

--
-- Name: chk_can_see_instance(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chk_can_see_instance(_username character varying, _id_test_instance integer) RETURNS public.chk_can_see_instance_return_type
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _show_solutions boolean;
BEGIN
    SELECT show_solutions
      INTO _show_solutions
      FROM test JOIN test_instance ON test.id = test_instance.id_test
      WHERE test_instance.id = _id_test_instance;
    -- First, check if it is a student or a super user? A student does not have a role.
    IF (( SELECT COUNT(*)
            FROM app_user
           WHERE username = _username) = 0) THEN
      -- a student can see only own test instances, and only for a configured time period (review_period_mins):
      RETURN ((
        SELECT (COUNT(*) > 0)
         From student
         JOIN test_instance
           ON student.id = test_instance.id_student
         JOIN test
           ON test_instance.id_test = test.id
        WHERE (alt_id = _username OR alt_id2 = _username)
          AND test_instance.id = _id_test_instance
          AND (EXTRACT(EPOCH FROM (current_timestamp- test_instance.ts_submitted)) < test.review_period_mins * 60)), _show_solutions);
    ELSE
      RETURN (chk_can_see (_username, (SELECT id_course From test_instance ti join test on ti.id_test = test.id WHERE ti.id = _id_test_instance)), _show_solutions);
    END IF;
END;
$$;


ALTER FUNCTION public.chk_can_see_instance(_username character varying, _id_test_instance integer) OWNER TO postgres;

--
-- Name: chk_can_see_question(character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chk_can_see_question(_username character varying, _id_question integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
    IF (( SELECT COUNT(*)
            FROM app_user
            --JOIN app_user_role ON app_user.id = app_user_role.id_app_user
           WHERE id_role IS NOT NULL
             AND username = _username) > 0) THEN
      -- Check if the question belongs to the course the user has permission to:
      RETURN (
        SELECT (COUNT(*) > 0)
         From question
         JOIN question_node on question.id = question_node.id_question
        WHERE id_question = _id_question
          AND id_node in
                (SELECT id_child from v_roots_children WHERE id_root IN
                    (SELECT id_root_node
                       FROM course
                       JOIN perm_user_course ON course.id = perm_user_course.id_course
                       JOIN app_user ON perm_user_course.id_user = app_user.id
                       WHERE username = _username)
                )
        );
    ELSE
    -- for the time being, this function is meant to be called only for app users
    -- if the app calls it for student, I'll return FALSE, which will bubble-up as a problem
    -- then, either fix the webapp, or implement studdent's permissions, if this policy had indeed changed
      RETURN false;
    END IF;
END;
$$;


ALTER FUNCTION public.chk_can_see_question(_username character varying, _id_question integer) OWNER TO postgres;

--
-- Name: chk_get_lecture_quiz_test_instance(character varying, character varying, boolean, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chk_get_lecture_quiz_test_instance(_ip_address character varying, _room_name character varying, _is_anon boolean, _id_student integer) RETURNS public.chk_get_lecture_quiz_test_instance_type
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _lecture_quiz_instance_row RECORD;
    _anon_id INTEGER;
    _alt_id CHARACTER VARYING;
    _test_instance_id INTEGER;
BEGIN

    SELECT id, id_test, allow_anonymous
    INTO _lecture_quiz_instance_row
    FROM teacher_lecture_quiz_instance
    WHERE room_name = _room_name AND ts_ended IS NULL;

    IF NOT FOUND THEN
        RETURN (1, -1, ''::varchar, -1, -1); -- no room with _room_name active at the moment
    END IF;



    IF _is_anon = true THEN

        IF _lecture_quiz_instance_row.allow_anonymous = false THEN
            RETURN (2, -1, ''::varchar, -1, -1); -- anonymous not allowed in the room
        END IF;


        -- generate random anon id
        SELECT id
        INTO _anon_id
        FROM student
        WHERE is_anonymous = true
        order by random() limit 1;

        SELECT alt_id
        INTO _alt_id
        FROM student
        WHERE id = _anon_id;

        _test_instance_id = gen_test_instance(_anon_id, _lecture_quiz_instance_row.id_test, _ip_address);

        INSERT INTO test_instance_room(id_test_instance, id_teacher_instance, room_name) VALUES (_test_instance_id, _lecture_quiz_instance_row.id, _room_name);

        RETURN (0,
                _anon_id,
                _alt_id,
                _lecture_quiz_instance_row.id,
                _test_instance_id
                );

    ELSE
        -- signed in entrance

        SELECT alt_id
        INTO _alt_id
        FROM student
        WHERE id = _id_student;

        _test_instance_id = gen_test_instance(_id_student, _lecture_quiz_instance_row.id_test, _ip_address);

        INSERT INTO test_instance_room(id_test_instance, id_teacher_instance, room_name) VALUES (_test_instance_id, _lecture_quiz_instance_row.id, _room_name);

        RETURN (0,
                _id_student,
                _alt_id,
                _lecture_quiz_instance_row.id,
                _test_instance_id
                );
    END IF;

END;
$$;


ALTER FUNCTION public.chk_get_lecture_quiz_test_instance(_ip_address character varying, _room_name character varying, _is_anon boolean, _id_student integer) OWNER TO postgres;

--
-- Name: chk_get_test_instance(integer, character varying, character varying, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chk_get_test_instance(_id_student integer, _ip_address character varying, _password character varying, _id_course integer, _is_pregenerate boolean DEFAULT false) RETURNS public.chk_get_test_instance_return_type
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _test_row RECORD;
    _has_run INTEGER;
    _existing_test_instance_id INTEGER;
    _rv chk_get_test_instance_return_type;
BEGIN


    SELECT test.id, id_course, id_academic_year, max_runs, is_global,
        CASE WHEN (CURRENT_TIMESTAMP BETWEEN ts_available_from AND ts_available_to)
          THEN true
          ELSE false
        END AS is_available
      INTO _test_row
      FROM test
      JOIN test_type
        ON test.id_test_type = test_type.id
     WHERE password = _password
       AND test_type.standalone = true;

    IF NOT FOUND THEN
        RETURN (1, -1, format('No test for password: %s.', _password));
    END IF;

    IF (_test_row.id_course <> _id_course) THEN
        RETURN (1, -1, 'You are in the wrong course. Please, change the course if you want to run this test.'::text);
    END IF;

    IF ((NOT _test_row.is_global) AND  (SELECT COUNT(*) FROM student_course
          WHERE id_academic_year = _test_row.id_academic_year
            AND id_course = _test_row.id_course
            AND id_student = _id_student) = 0) THEN
        -- Check to see it this is a teacher's alter ego:

      IF ((SELECT COUNT(*)
        FROM perm_user_course
        JOIN app_user ON perm_user_course.id_user = app_user.id
        JOIN student  ON app_user.id = student.id_app_user
       WHERE student.id = _id_student
         AND perm_user_course.id_course = _test_row.id_course) = 0) THEN

        RETURN (2, -1, format('Student (id = %s) has not enrolled the assigned test''s course (id = %s) in acdm year(id = %s).',
                  _id_student, _test_row.id_course, _test_row.id_academic_year));
      END IF;
    END IF;

    -- SET LOCK to prevent double test_instances when this function is run in parallel at the same time (ms apart)
    INSERT INTO public.test_instance_gen_lock(id_student, id_course, password, ts_created)
    VALUES (_id_student, _id_course, _password, CURRENT_TIMESTAMP);

    _rv = (0, 0, 'to-be-updated');

    SELECT SUM(CASE WHEN ts_submitted IS NULL THEN 1E6 ELSE 1 END)
      INTO _has_run
      from test_instance
     WHERE id_student = _id_student
       AND id_test = _test_row.id;

    IF (_has_run >= 1E6) THEN
        -- has run, and is in progress, return latest unsubmitted if available or prolonged:
        IF (_test_row.is_available = false) THEN
           -- try to find the latest prolonged:
            SELECT id
              INTO _existing_test_instance_id
              FROM test_instance
             WHERE id_student = _id_student
               AND id_test = _test_row.id
               AND ts_submitted IS NULL
               AND prolonged
          ORDER BY ts_generated DESC
             LIMIT 1;
            IF NOT FOUND THEN
                _rv = (3, -1, format('There are unsubmitted tests, but: (a) test is no more available and (b) test instance for student is NOT prolonged.'));
            ELSE
                _rv = RN (1, -1, format('Returning latest PROLONGED unsubmitted test (since the test itself is not available ie. time has passed).'));
            END IF;

        ELSE
            _rv = (0,
                    (SELECT id
                       FROM test_instance
                      WHERE id_student = _id_student
                        AND id_test = _test_row.id
                        AND ts_submitted IS NULL
                   ORDER BY ts_generated DESC
                      LIMIT 1),
                    'Returned latest unsubmitted ongoing test.'::text);
        END IF;

    ELSIF ( (_has_run > 0)
        AND (_test_row.max_runs > 0) -- 0 = unlimited, so ignore it.
        AND (_test_row.max_runs <= _has_run)) THEN
        _rv =  (4, -1, format('Student has already taken this exam (has_run = %s, max_runs = %s).', _has_run, _test_row.max_runs));

    ELSE
        -- this is where a new test instance should be generated, because:
        --  (a) student has NEVER run this test
        --  (b) student has run this test, but not exceeded the max_runs param
        IF (_test_row.is_available = false AND _is_pregenerate = false) THEN
            _rv =  (5, -1, format('Test is not available (the time has passed).'));
        ELSE
            _rv =  (0,
                    gen_test_instance(_id_student, _test_row.id, _ip_address),
                    'New test instance generated.'::text);
        END IF;
    END IF;

    DELETE FROM public.test_instance_gen_lock
    WHERE id_student  = _id_student
      AND id_course   = _id_course
      AND password    = _password;

    RETURN _rv;
END;
$$;


ALTER FUNCTION public.chk_get_test_instance(_id_student integer, _ip_address character varying, _password character varying, _id_course integer, _is_pregenerate boolean) OWNER TO postgres;

--
-- Name: clone_c_question_answer(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clone_c_question_answer(_old_question_id integer, _new_question_id integer) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _c_question_answer_id INTEGER;
  _c_question_test_new_id INTEGER;
  _rec_question_test RECORD;
  _cursor CURSOR (_id integer) FOR SELECT * FROM c_question_test WHERE c_question_answer_id = _id;
  _new_c_question_answer_id integer;
BEGIN
    -- Clone programming languages
    INSERT INTO question_programming_language (id_question, id_programming_language)
    SELECT _new_question_id, id_programming_language
    FROM question_programming_language
    WHERE id_question = _old_question_id;

    SELECT id
      INTO _c_question_answer_id
      FROM c_question_answer
     WHERE id_question = _old_question_id;

    INSERT INTO c_question_answer (id_question, c_prefix, c_suffix, c_source, id_programming_language)
    SELECT _new_question_id, c_prefix, c_suffix, c_source, id_programming_language
      FROM c_question_answer
      WHERE id_question = _old_question_id;

    SELECT id INTO _new_c_question_answer_id
      FROM c_question_answer
     WHERE id_question = _new_question_id;

    INSERT INTO runtime_constraint_question (id_question, id_programming_language, id_runtime_constraint, override_value)
    SELECT _new_question_id, id_programming_language, id_runtime_constraint, override_value
    FROM runtime_constraint_question
    WHERE id_question = _old_question_id;

    OPEN _cursor(_c_question_answer_id);
    LOOP
      FETCH _cursor INTO _rec_question_test;
      EXIT WHEN NOT FOUND;

      INSERT INTO c_question_test (c_question_answer_id , c_test_type_id , percentage, allow_diff_order, allow_diff_letter_size ,trim_whitespace , comment ,regex_override, is_public )
        VALUES ( _new_c_question_answer_id , _rec_question_test.c_test_type_id , _rec_question_test.percentage,
            _rec_question_test.allow_diff_order, _rec_question_test.allow_diff_letter_size ,
            _rec_question_test.trim_whitespace , _rec_question_test.comment ,_rec_question_test.regex_override, _rec_question_test.is_public)
      RETURNING id INTO _c_question_test_new_id;

      INSERT INTO fixed_test(c_question_test_id, input, output)
      SELECT _c_question_test_new_id, input, output
        FROM fixed_test
        WHERE c_question_test_id = _rec_question_test.id;

      INSERT INTO random_test(c_question_test_id, random_test_type_id, low_bound, upper_bound, elem_count)
      SELECT _c_question_test_new_id, random_test_type_id, low_bound, upper_bound, elem_count
        FROM random_test
        WHERE c_question_test_id = _rec_question_test.id;

      INSERT INTO generator_test(c_question_test_id, generator_test_file_id, arguments)
      SELECT _c_question_test_new_id, generator_test_file_id, arguments
        FROM generator_test
        WHERE c_question_test_id = _rec_question_test.id;

      -- Copy test constraints
      INSERT INTO runtime_constraint_test (id_c_question_test, id_programming_language, id_runtime_constraint, override_value)
      SELECT _c_question_test_new_id, id_programming_language, id_runtime_constraint, override_value
          FROM runtime_constraint_test
      WHERE id_c_question_test = _rec_question_test.id;

    END LOOP;
    CLOSE _cursor;
END;
$$;


ALTER FUNCTION public.clone_c_question_answer(_old_question_id integer, _new_question_id integer) OWNER TO postgres;

--
-- Name: clone_c_question_test(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clone_c_question_test(_id_c_question_test integer, _id_user_modified integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  new_id INTEGER;
BEGIN
    -- Copy "head"
    INSERT INTO c_question_test (c_question_answer_id , c_test_type_id , percentage , allow_diff_order , allow_diff_letter_size , trim_whitespace, comment, is_public)
    SELECT c_question_answer_id , c_test_type_id , percentage , allow_diff_order , allow_diff_letter_size , trim_whitespace, comment, is_public
      FROM c_question_test
    WHERE id = _id_c_question_test
    RETURNING id INTO new_id;
    --Copy details, 1st fixed_test:
    INSERT INTO fixed_test( c_question_test_id, input, output)
    SELECT new_id, input, output
      FROM fixed_test
    WHERE c_question_test_id = _id_c_question_test;
    --Copy details, 2nd random_test:
    INSERT INTO random_test( c_question_test_id, random_test_type_id, low_bound, upper_bound, elem_count)
    SELECT new_id, random_test_type_id, low_bound, upper_bound, elem_count
      FROM random_test
    WHERE c_question_test_id = _id_c_question_test;
    --Copy details, 3rd generator_test:
    INSERT INTO generator_test( c_question_test_id, generator_test_file_id, arguments)
    SELECT new_id, generator_test_file_id, arguments
      FROM generator_test
    WHERE c_question_test_id = _id_c_question_test;

    -- Copy test constraints
    INSERT INTO runtime_constraint_test (id_c_question_test, id_programming_language, id_runtime_constraint, override_value)
    SELECT new_id, id_programming_language, id_runtime_constraint, override_value
        FROM runtime_constraint_test
    WHERE id_c_question_test = _id_c_question_test;

    RETURN new_id;
END;
$$;


ALTER FUNCTION public.clone_c_question_test(_id_c_question_test integer, _id_user_modified integer) OWNER TO postgres;

--
-- Name: clone_question(integer, integer, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clone_question(_id integer, _id_user_created integer, _id_user_modified integer, _linktoparent boolean) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _has_answers boolean;
    new_id INTEGER;
    _id_question_type INTEGER;
BEGIN

    SELECT has_answers, id_question_type
      INTO _has_answers, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
     WHERE question.id = _id;

    -- Clone head:
    INSERT INTO question (version, is_active, id_prev_question, id_question_type, question_text, question_comment, id_user_created, id_user_modified
          , upload_files_no, required_upload_files_no, grader_object)
        SELECT CASE WHEN (_linkToParent) THEN version + 1 ELSE 0 END, is_active,
               CASE WHEN (_linkToParent) THEN _id ELSE null END,
               id_question_type, question_text, question_comment, _id_user_created, _id_user_modified
               , upload_files_no, required_upload_files_no, grader_object
          FROM question
         WHERE id = _id
        RETURNING id INTO new_id;

    -- Clone detail:
    -- (a) nodes:
    INSERT INTO question_node (id_question, id_node)
      SELECT new_id, id_node
        FROM question_node
       WHERE id_question = _id;
    -- (b) answers:
    IF (_has_answers) THEN

      INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage)
      SELECT new_id, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage
        FROM question_answer
       WHERE id_question = _id;
       IF (_id_question_type = 8) THEN
            INSERT INTO ordered_element_question (id_question, id_ordered_element_correctness_model, display_option)
            SELECT new_id, id_ordered_element_correctness_model, display_option
              FROM ordered_element_question
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL + C
        IF (_id_question_type = 2) THEN
            INSERT INTO sql_question_answer (id_question, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                    sql_alt_presentation_query, check_tuple_order, id_check_column_mode)
            SELECT new_id, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                   sql_alt_presentation_query, check_tuple_order, id_check_column_mode
              FROM sql_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 21) THEN
            INSERT INTO json_question_answer (id_question, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                    json_alt_presentation_query, assert_deep, assert_strict)
            SELECT new_id, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                   json_alt_presentation_query, assert_deep, assert_strict
              FROM json_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM clone_c_question_answer(_id, new_id);
        ELSIF (_id_question_type = 26) THEN
            INSERT INTO question_scale (id_question, id_scale)
            SELECT new_id, id_scale
              FROM question_scale
             WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            -- Free text question
            INSERT INTO text_question_answer (id_question, text_answer)
            SELECT new_id, text_answer
              FROM text_question_answer
             WHERE id_question = _id;
        ELSE
            RAISE EXCEPTION 'Unknown id_question_type';
        END IF;
    END IF;
    -- tags:
    INSERT INTO question_course_tag (id_question, id_course_tag)
         SELECT new_id, id_course_tag
           FROM question_course_tag
          WHERE id_question = _id;
    -- data objects:
    INSERT INTO question_data_object (id_question, data_object)
         SELECT new_id, data_object
           FROM question_data_object
          WHERE id_question = _id;
    -- scripts:
    INSERT INTO question_eval_script (id_question, eval_script, id_script_programming_language)
         SELECT new_id, eval_script, id_script_programming_language
           FROM question_eval_script
          WHERE id_question = _id;

    --INSERT INTO question_attachment (id_question, original_name, filename, label, is_public)
    --    SELECT new_id, original_name, filename, label, is_public
    --        FROM question_attachment
    --        WHERE id_question = _id;

    RETURN new_id;
END;
$$;


ALTER FUNCTION public.clone_question(_id integer, _id_user_created integer, _id_user_modified integer, _linktoparent boolean) OWNER TO postgres;

--
-- Name: clone_test(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clone_test(_id_test integer, _id_academic_year integer, _id_user_created integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$

DECLARE new_id INTEGER;
-- DECLARE _test_ordinal INTEGER;

BEGIN

-- Clone head:
  -- Test ordinal is no longer unique, so I'm dropping this part:
    -- IF (0 = (SELECT COUNT(*)
    --   FROM test JOIN test old_test
    --             ON test.id_course = old_test.id_course
    --             and test.test_ordinal = old_test.test_ordinal
    --             and old_test.id = _id_test
    --    AND test.id_academic_year = _id_academic_year)) THEN
    --   SELECT test_ordinal INTO _test_ordinal FROM test where id = _id_test;
    -- ELSE
    --   SELECT COALESCE(MAX(test_ordinal), 0) + 1 INTO _test_ordinal FROM test WHERE id_academic_year = _id_academic_year;
    -- END IF;



    INSERT INTO test (title, title_abbrev, id_course, id_academic_year, id_semester, id_test_type, id_user_created, test_ordinal,
    max_runs, show_solutions, hint_result, max_score, password, questions_no, duration_seconds, pass_percentage,
    ts_available_from, ts_available_to, review_period_mins, is_competition, eval_comp_score, async_submit,
    trim_clock,
    allow_anonymous,
    upload_file_limit,
    forward_only
    )
    SELECT title, title_abbrev, id_course, _id_academic_year, id_semester, id_test_type, _id_user_created, test_ordinal, max_runs,
        show_solutions, hint_result, max_score,
      (SELECT string_agg (substr('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', ceil (random() * 62)::integer, 1), '')
       FROM generate_series(1, 45)), questions_no,
                                     duration_seconds,
                                     pass_percentage,
                                     ts_available_from,
                                     ts_available_to,
                                     review_period_mins, is_competition, eval_comp_score, async_submit,
        trim_clock,
        allow_anonymous,
        upload_file_limit,
        forward_only
    FROM test
    WHERE id = _id_test RETURNING id INTO new_id;

    -- Clone detail:
    INSERT INTO test_part (id_test, id_node, id_grading_model, id_question_type, min_questions, max_questions, pass_percentage)
    SELECT new_id,
           id_node,
           id_grading_model,
           id_question_type,
           min_questions,
           max_questions,
           pass_percentage
    FROM test_part
    WHERE id_test = _id_test;


    INSERT INTO test_question (id_test, ordinal, id_question, id_grading_model)
    SELECT new_id,
           ordinal,
           id_question,
           id_grading_model
    FROM test_question
    WHERE id_test = _id_test;

    RETURN new_id;
END; $$;


ALTER FUNCTION public.clone_test(_id_test integer, _id_academic_year integer, _id_user_created integer) OWNER TO postgres;

--
-- Name: clone_tutorial(integer, integer, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clone_tutorial(_id_tutorial integer, _id_course integer, _id_user_created integer, _node_name text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _id_tutorials_course_root_node integer;
  _new_id_tutorial integer;
  _new_id_step integer;
  _new_id_question integer;
  _id_node_tutorial integer;
  _id_question_node integer;
  _step record;
  _answer record;
BEGIN
  -- Clone head:
  INSERT INTO public.tutorial(
            id_node, id_user_created, id_user_modified, tutorial_title, tutorial_desc,
            is_active)
  SELECT id_node, _id_user_created, _id_user_created, tutorial_title, tutorial_desc, false
  FROM tutorial
  WHERE id = _id_tutorial
  RETURNING id INTO _new_id_tutorial;

  -- Assign tutorial to course
  INSERT INTO public.tutorial_course(id_tutorial, id_course)
    VALUES(_new_id_tutorial, _id_course);

  -- Find the tutorials course root node id
	_id_tutorials_course_root_node = tutorials_course_root_node_id(
    _id_course
  );

	-- Create the tutorial node for questions used in steps of this tutorial
  INSERT INTO public.node(
                id_node_type, node_name)
            VALUES ((SELECT id FROM node_type WHERE type_name = 'Tutorial'), _node_name)
            RETURNING id INTO _id_node_tutorial;

  -- Assign tutorials course root node as its parent
  INSERT INTO public.node_parent(
        id_child, id_parent)
        VALUES (_id_node_tutorial, _id_tutorials_course_root_node);

  -- Update the tutorial node id for the cloned tutorial
  UPDATE tutorial
  SET id_node = _id_node_tutorial
  WHERE id = _new_id_tutorial;

  -- Clone detail:

	-- Assign tutorial to course
	INSERT INTO public.tutorial_course(
    id_course, id_tutorial)
      VALUES (_id_course, _new_id_tutorial);

  FOR _step IN
		SELECT *
		FROM tutorial_step
		WHERE id_tutorial = _id_tutorial
	LOOP

    -- question
    _new_id_question = clone_question(
      _step.id_question,
      _id_user_created,
      _id_user_created,
      false);

    -- question_node
    UPDATE question_node
    SET id_node = _id_node_tutorial
    WHERE id_question = _new_id_question;

    -- step
    INSERT INTO tutorial_step (
      id_tutorial,
      id_question,
      id_user_created,
      id_user_modified,
      ordinal,
      title,
      text) VALUES (
        _new_id_tutorial,
        _new_id_question,
        _step.id_user_created,
        _step.id_user_modified,
        _step.ordinal,
        _step.title,
        _step.text)
    RETURNING id INTO _new_id_step;

    -- step hint
    INSERT INTO tutorial_step_hint (
      id_step,
      id_user_created,
      id_user_modified,
      is_active,
      ordinal,
      hint_text
    )
    SELECT _new_id_step,
      id_user_created,
      id_user_modified,
      is_active,
      ordinal,
      hint_text
    FROM tutorial_step_hint
    WHERE id_step = _step.id;

    -- question hints
    FOR _answer IN
      SELECT *
      FROM question_answer
      WHERE id_question = _new_id_question
    LOOP
	    -- question hint
	    INSERT INTO tutorial_question_hint (
	      id_step,
	      id_question_answer,
	      id_user_created,
	      id_user_modified,
	      is_active,
	      hint_text
	    )
	    SELECT _new_id_step, _answer.id,
	      id_user_created,
	      id_user_modified,
	      is_active,
	      hint_text
	    FROM tutorial_question_hint
	    WHERE id_step = _step.id;
    END LOOP;

    -- code question hint
    INSERT INTO tutorial_code_question_hint (
      id_step,
      id_question,
      id_user_created,
      id_user_modified,
      is_active,
      ordinal,
      answer_regex_trigger,
      hint_text
    )
    SELECT _new_id_step, _new_id_question,
      id_user_created,
      id_user_modified,
      is_active,
      ordinal,
      answer_regex_trigger,
      hint_text
    FROM tutorial_code_question_hint
    WHERE id_question = _step.id_question;
  END LOOP;

  RETURN _new_id_tutorial;
END;
$$;


ALTER FUNCTION public.clone_tutorial(_id_tutorial integer, _id_course integer, _id_user_created integer, _node_name text) OWNER TO postgres;

--
-- Name: create_teacher_lecture_quiz_instance(integer, boolean, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.create_teacher_lecture_quiz_instance(_id_test integer, _allow_anon boolean, _id_app_user integer) RETURNS public.create_teacher_lecture_quiz_test_instance_type
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
    DECLARE
        _id_teacher_lecture_instance INTEGER;
    BEGIN
        INSERT INTO teacher_lecture_quiz_instance (id_test, id_app_user, room_name, current_question_ordinal, ts_started, allow_anonymous)
             VALUES (_id_test, _id_app_user, (substring(md5(random()::text) from 1 for 4)),
                    0, current_timestamp, _allow_anon); --current_question_ordinal = 0 => do not display any questions

            _id_teacher_lecture_instance = (SELECT currval('teacher_lecture_quiz_instance_id_seq'));

            RETURN (0, _id_teacher_lecture_instance, format('Teacher lecture quiz instance created!'));
    END
$$;


ALTER FUNCTION public.create_teacher_lecture_quiz_instance(_id_test integer, _allow_anon boolean, _id_app_user integer) OWNER TO postgres;

--
-- Name: deep_clone_test(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.deep_clone_test(_id_test integer, _id_academic_year integer, _id_user_created integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$

DECLARE new_id_test INTEGER;

BEGIN
    -- Leverage the basic clone
    new_id_test = clone_test(_id_test, _id_academic_year, _id_user_created);
    -- And then fix the questions:
    DELETE FROM test_question WHERE id_test = new_id_test;

    INSERT INTO test_question (id_test, ordinal, id_question, id_grading_model)
    SELECT new_id_test,
           ordinal,
           clone_question(id_question, _id_user_created, _id_user_created, false),  -- DEEP !!
           id_grading_model
    FROM test_question
    WHERE id_test = _id_test;

    UPDATE question SET question_text = 'DEEP-CLONED (fix node?): ' || question_text WHERE id IN (SELECT id_question FROM test_question WHERE id_test = new_id_test);

    RETURN new_id_test;
END; $$;


ALTER FUNCTION public.deep_clone_test(_id_test integer, _id_academic_year integer, _id_user_created integer) OWNER TO postgres;

--
-- Name: del_all_test_instances(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.del_all_test_instances(_id_test integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  DELETE FROM test_correction
   WHERE id_test_instance_question in
         (SELECT test_instance_question.id
                    FROM test_instance_question
                    JOIN test_instance
                      ON test_instance_question.id_test_instance = test_instance.id
                   WHERE test_instance.id_test = _id_test);

  DELETE FROM test_instance_question_generated
   WHERE id_test_instance_question IN
        (SELECT test_instance_question.id
           from test_instance_question
           JOIN test_instance
             ON test_instance_question.id_test_instance = test_instance.id
           WHERE id_test = _id_test);

  DELETE FROM test_instance_question_run
   WHERE id_test_instance_question IN
        (SELECT test_instance_question.id
           from test_instance_question
           JOIN test_instance
             ON test_instance_question.id_test_instance = test_instance.id
           WHERE id_test = _id_test);

  DELETE FROM ticket
   WHERE id_test_instance_question IN
        (SELECT test_instance_question.id
           from test_instance_question
           JOIN test_instance
             ON test_instance_question.id_test_instance = test_instance.id
           WHERE id_test = _id_test);

  DELETE FROM test_instance_question_manual_grade
   WHERE id_test_instance_question IN
        (SELECT test_instance_question.id
           from test_instance_question
           JOIN test_instance
             ON test_instance_question.id_test_instance = test_instance.id
           WHERE id_test = _id_test);

  DELETE FROM test_instance_question
   WHERE id_test_instance IN
        (SELECT id from test_instance WHERE id_test = _id_test);

  DELETE FROM test_instance
   WHERE id_test = _id_test;

  RETURN true;
END;
$$;


ALTER FUNCTION public.del_all_test_instances(_id_test integer) OWNER TO postgres;

--
-- Name: del_lecture_quiz_instance(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.del_lecture_quiz_instance(id_lecture_quiz_instance integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN

  DELETE FROM test_instance_question
   WHERE id_test_instance IN (SELECT id_test_instance FROM test_instance_room WHERE id_teacher_instance = id_lecture_quiz_instance);

  DELETE FROM test_instance_room
   WHERE id_teacher_instance = id_lecture_quiz_instance;

  DELETE FROM test_instance
   WHERE id IN (SELECT id_test_instance FROM test_instance_room WHERE id_teacher_instance = id_lecture_quiz_instance);

  DELETE FROM teacher_lecture_quiz_instance
   WHERE id = id_lecture_quiz_instance;

    RETURN true;
END;
$$;


ALTER FUNCTION public.del_lecture_quiz_instance(id_lecture_quiz_instance integer) OWNER TO postgres;

--
-- Name: del_test(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.del_test(_id_test integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  PERFORM del_test(id) from test where id_parent = _id_test;
  DELETE FROM test_part WHERE id_test = _id_test;
  DELETE FROM test_question WHERE id_test = _id_test;
  DELETE FROM test_message WHERE id_test = _id_test;
  DELETE FROM peer_test_group_question
	WHERE id_peer_test_group IN
	(SELECT peer_test_group.id
		FROM peer_test
		JOIN peer_test_group
		  ON peer_test.id = peer_test_group.id_peer_test
		WHERE peer_test.id_test_phase_2 = _id_test);
  DELETE FROM peer_test_group WHERE id_peer_test IN (SELECT id FROM peer_test WHERE id_test_phase_2 = _id_test);
  DELETE FROM peer_test WHERE id_test_phase_2 = _id_test;
  DELETE FROM peer_shuffle WHERE id_test_phase_2 = _id_test;
  DELETE FROM test      WHERE id      = _id_test;
  RETURN true;
END;
$$;


ALTER FUNCTION public.del_test(_id_test integer) OWNER TO postgres;

--
-- Name: del_test_instance(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.del_test_instance(_id_test_instance integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$

BEGIN
  DELETE FROM ticket
   WHERE id_test_instance_question in
         (SELECT id FROM test_instance_question WHERE id_test_instance = _id_test_instance);
  DELETE FROM test_correction
   WHERE id_test_instance_question in
         (SELECT id FROM test_instance_question WHERE id_test_instance = _id_test_instance);
  DELETE FROM test_instance_question_run
   WHERE id_test_instance_question IN
         (SELECT id FROM test_instance_question WHERE id_test_instance = _id_test_instance);
  DELETE FROM test_instance_question_generated
   WHERE id_test_instance_question IN
         (SELECT id FROM test_instance_question WHERE id_test_instance = _id_test_instance);
  DELETE FROM test_instance_question_manual_grade
   WHERE id_test_instance_question IN
         (SELECT id FROM test_instance_question WHERE id_test_instance = _id_test_instance);

  DELETE FROM test_instance_question
   WHERE id_test_instance = _id_test_instance;



  DELETE FROM test_instance
   WHERE id = _id_test_instance;

  RETURN true;
END;

$$;


ALTER FUNCTION public.del_test_instance(_id_test_instance integer) OWNER TO postgres;

--
-- Name: delete_c_question_test(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_c_question_test(_id integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
    -- IF (SELECT COUNT(*) FROM c_question_test WHERE id =  _id) = 0 THEN
    --   RETURN false;
    -- END IF;
    DELETE FROM fixed_test        WHERE c_question_test_id = _id;
    DELETE FROM generator_test    WHERE c_question_test_id = _id;
    DELETE FROM random_test       WHERE c_question_test_id = _id;

    DELETE FROM runtime_constraint_test WHERE id_c_question_test = _id;

    DELETE FROM c_question_test  WHERE id = _id;

    RETURN true;
END;
$$;


ALTER FUNCTION public.delete_c_question_test(_id integer) OWNER TO postgres;

--
-- Name: delete_exercise(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_exercise(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	-- delete exercise nodes
	DELETE FROM public.exercise_node
	WHERE id_exercise = _id;

	-- delete exercise
	DELETE FROM public.exercise
	WHERE id = _id;
END
$$;


ALTER FUNCTION public.delete_exercise(_id integer) OWNER TO postgres;

--
-- Name: delete_peer_test_question(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_peer_test_question(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        _deleted_q_ordinal INTEGER;
        _id_peer_test_group INTEGER;
        _max_ordinal INTEGER;
        _question_ordinal INTEGER;
    BEGIN
        _deleted_q_ordinal = (SELECT ordinal FROM peer_test_group_question WHERE id = _id);

        _id_peer_test_group = (SELECT id_peer_test_group FROM peer_test_group_question WHERE id = _id);

        DELETE FROM peer_test_group_question
            WHERE id = _id;

        _max_ordinal = (SELECT COALESCE(MAX(ordinal), 0) FROM peer_test_group_question WHERE id_peer_test_group = _id_peer_test_group);

        FOR i IN (_deleted_q_ordinal + 1).._max_ordinal LOOP
            UPDATE peer_test_group_question
            SET ordinal = ordinal - 1
            WHERE _id_peer_test_group = _id_peer_test_group
            AND ordinal = i;
        END LOOP;
    END
$$;


ALTER FUNCTION public.delete_peer_test_question(_id integer) OWNER TO postgres;

--
-- Name: delete_question(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_question(_id integer) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
BEGIN
    -- Script, if exists:
    DELETE FROM question_eval_script WHERE id_question = _id;
    -- Template, if exists:
    DELETE FROM question_data_object WHERE id_question = _id;
    -- Will not delete test_instance_question ON PURPOSE; such questions should not be deleted, ever.
    DELETE FROM question_node WHERE id_question = _id;
    -- ABC:
    DELETE FROM question_answer WHERE id_question = _id;
    -- SQL:
    DELETE FROM sql_question_answer WHERE id_question = _id;
    -- JSON:
    DELETE FROM json_question_answer WHERE id_question = _id;
    -- Tags:
    DELETE FROM question_course_tag WHERE id_question = _id;
    --Scale (questionnaire)
    DELETE FROM question_scale WHERE id_question = _id;
    -- C:
    DELETE FROM random_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
     DELETE FROM fixed_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
     DELETE FROM generator_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
    -- What about generator_test_file?

    -- Delete test level runtime constraints
    DELETE FROM runtime_constraint_test
        WHERE id_c_question_test IN (
            SELECT id FROM c_question_test WHERE c_question_answer_id IN (
                    SELECT id FROM c_question_answer WHERE id_question = _id));

    DELETE FROM c_question_test
      WHERE c_question_answer_id IN (
            SELECT id FROM c_question_answer WHERE id_question = _id
        );

    -- Delete question level runtime constraints
    DELETE FROM runtime_constraint_question WHERE id_question = _id;

    DELETE FROM question_programming_language WHERE id_question = _id;

    DELETE FROM c_question_answer WHERE id_question = _id;
    DELETE FROM question_review   WHERE id_question = _id;
    DELETE FROM text_question_answer  WHERE id_question = _id;
    DELETE FROM question_attachment  WHERE id_question = _id;
    ------------------------------------
    DELETE FROM ordered_element_question  WHERE id_question = _id;
    DELETE FROM connected_elements_question  WHERE id_question = _id;
    DELETE FROM connected_elements_question_answer  WHERE id_question = _id;
    DELETE FROM diagram_question_answer  WHERE id_question = _id;
    ------------------------------------
    -- Finally:
    DELETE FROM question WHERE id = _id;

    RETURN true;
END;
$$;


ALTER FUNCTION public.delete_question(_id integer) OWNER TO postgres;

--
-- Name: delete_test_question(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_test_question(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        _deleted_q_ordinal INTEGER;
        _id_test INTEGER;
        _max_ordinal INTEGER;
        _question_ordinal INTEGER;
    BEGIN
        _deleted_q_ordinal = (SELECT ordinal FROM test_question WHERE id=_id);

        _id_test = (SELECT id_test FROM test_question WHERE id=_id);

        DELETE FROM test_question
        WHERE id = _id;

        _max_ordinal = (SELECT COALESCE(MAX(ordinal), 0) FROM test_question WHERE id_test = _id_test);

        FOR i IN (_deleted_q_ordinal + 1).._max_ordinal LOOP
            UPDATE test_question
            SET ordinal = ordinal - 1
            WHERE id_test = _id_test AND ordinal = i;
        END LOOP;
    END
$$;


ALTER FUNCTION public.delete_test_question(_id integer) OWNER TO postgres;

--
-- Name: delete_tutorial(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_tutorial(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	_row record;
BEGIN
	FOR _row IN
		SELECT *
		FROM public.tutorial_step
		WHERE id_tutorial = _id
	LOOP
		PERFORM delete_tutorial_step(_row.id);
	END LOOP;

	DELETE FROM public.tutorial_course
	WHERE id_tutorial = _id;

	DELETE FROM public.tutorial
	WHERE id = _id;
END
$$;


ALTER FUNCTION public.delete_tutorial(_id integer) OWNER TO postgres;

--
-- Name: delete_tutorial_step(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_tutorial_step(_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	_id_question integer;
BEGIN
	DELETE FROM public.tutorial_step_hint
	WHERE id_step = _id;

	DELETE FROM public.tutorial_question_hint
	WHERE id_step = _id;

	SELECT id_question INTO _id_question
	FROM public.tutorial_step
	WHERE id = _id;

	DELETE FROM public.tutorial_step
	WHERE id = _id;

	PERFORM delete_question(_id_question);
END
$$;


ALTER FUNCTION public.delete_tutorial_step(_id integer) OWNER TO postgres;

--
-- Name: email_process_queue(text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.email_process_queue(_mail_from text, _batch_size integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    _test RECORD;
    _mail RECORD;
    _numProcessed INT;
BEGIN
     _numProcessed = 0;
    FOR _test IN
        SELECT  id,
                title,
                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from)) as elapsed,
                ROUND((100. * EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from))/EXTRACT(EPOCH FROM (ts_available_to - ts_available_from)))::numeric, 2) || '%' as elapsed_perc,
                ts_available_from::timestamp(0),
                ts_available_to::timestamp(0),
                MIN(threshold_not_started) as threshold
                FROM (
                        SELECT test.id, test.title, UNNEST(thresholds_not_started) as threshold_not_started, ts_available_from, ts_available_to
                          FROM test
                          JOIN email_reminder_scheme
                            ON test.id_email_reminder_scheme = email_reminder_scheme.id
                         WHERE CURRENT_TIMESTAMP between ts_available_from and ts_available_to
                ) AS t
                WHERE NOT EXISTS (SELECT *
                                    FROM email_handled_reminders
                                    WHERE id_test = t.id
                                      AND threshold = t.threshold_not_started AND reminder_type = 'not started')
                AND EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from))  >=  threshold_not_started * EXTRACT(EPOCH FROM (ts_available_to - ts_available_from))
                GROUP BY t.id, t.title, elapsed, elapsed_perc, ts_available_from, ts_available_to
    LOOP
        _numProcessed = _numProcessed +1;
        FOR _mail IN
                SELECT part, string_agg(email, ';') as mail_bcc
                FROM (
                        SELECT email, 1+ (row_number() over ()) /_batch_size  as part
                        FROM test
                        JOIN student_course
                          ON test.id_course = student_course.id_course
                         AND test.id_academic_year = student_course.id_academic_year
                        JOIN student
                          ON student_course.id_student = student.id
                       WHERE test.id = _test.id
                         AND email is not null
                         AND NOT EXISTS (
                                SELECT test_instance.id
                                 FROM test_instance
                                WHERE test_instance.id_test = test.id
                                  AND test_instance.id_student = student.id
                        )
                        ORDER BY last_name
                        ) as list
                GROUP BY part
                ORDER BY part
	LOOP
        INSERT INTO email_queue (
                mail_to,
                mail_cc,
                mail_bcc,
                mail_from,
                mail_subject,
                mail_text,
                ---------
                ts_created,
                ts_modified,
                user_modified
                ) VALUES (
                _mail_from,
                '',
                _mail.mail_bcc,
                _mail_from,
                'A gentle reminder from Edgar at ' || 100 * _test.threshold::decimal || '% threshold',
$$Dear user,

Please consider the following test:

  * Test name: $$ || _test.title || $$
  * Available from: $$ || _test.ts_available_from || $$
  * Available to: $$ || _test.ts_available_to || $$
    ------------------------------------
  * Elapsed: $$ || _test.elapsed_perc || $$
  * Reminder threshold: $$ || _test.threshold || $$
  * Your status: NOT STARTED

Please, do not reply to this message.

Best,

Edgar$$,
                ---------
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP,
                'Mailman'
                );
        END LOOP;
        INSERT INTO email_handled_reminders (id_test, threshold, reminder_type, ts_created, ts_modified, user_modified)
                VALUES (_test.id, _test.threshold, 'not started', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Mailman');
    END LOOP;

-- Repeat everything, but for the ONGOING tests:

    FOR _test IN
        SELECT  id,
                title,
                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from)) as elapsed,
                ROUND((100. * EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from))/EXTRACT(EPOCH FROM (ts_available_to - ts_available_from)))::numeric, 2) || '%' as elapsed_perc,
                ts_available_from::timestamp(0),
                ts_available_to::timestamp(0),
                MIN(threshold_ongoing) as threshold
                FROM (
                        SELECT test.id, test.title, UNNEST(thresholds_ongoing) as threshold_ongoing, ts_available_from, ts_available_to
                          FROM test
                          JOIN email_reminder_scheme
                            ON test.id_email_reminder_scheme = email_reminder_scheme.id
                         WHERE CURRENT_TIMESTAMP between ts_available_from and ts_available_to
                ) AS t
                WHERE NOT EXISTS (SELECT *
                                    FROM email_handled_reminders
                                    WHERE id_test = t.id
                                      AND threshold = t.threshold_ongoing AND reminder_type = 'ongoing')
                AND EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - ts_available_from))  >=  threshold_ongoing * EXTRACT(EPOCH FROM (ts_available_to - ts_available_from))
                GROUP BY t.id, t.title, elapsed, elapsed_perc, ts_available_from, ts_available_to
    LOOP
        _numProcessed = _numProcessed +1;
        FOR _mail IN
                SELECT part, string_agg(email, ';') as mail_bcc
                FROM (
                        SELECT email, 1+ (row_number() over ()) / _batch_size  as part
                        FROM test
                        JOIN student_course
                          ON test.id_course = student_course.id_course
                         AND test.id_academic_year = student_course.id_academic_year
                        JOIN student
                          ON student_course.id_student = student.id
                        JOIN test_instance
                          ON test_instance.id_test = test.id
                         AND test_instance.id_student = student.id
                         AND test_instance.ts_submitted IS NULL -- == started
                       WHERE test.id = _test.id
                         AND email is not null
                        ORDER BY last_name
                        ) as list
                GROUP BY part
                ORDER BY part
	LOOP
        INSERT INTO email_queue (
                mail_to,
                mail_cc,
                mail_bcc,
                mail_from,
                mail_subject,
                mail_text,
                ---------
                ts_created,
                ts_modified,
                user_modified
                ) VALUES (
                _mail_from,
                '',
                _mail.mail_bcc,
                _mail_from,
                'A gentle reminder from Edgar at ' || 100 * _test.threshold::decimal || '% threshold',
$$Dear user,

Please consider the following test:

  * Test name: $$ || _test.title || $$
  * Available from: $$ || _test.ts_available_from || $$
  * Available to: $$ || _test.ts_available_to || $$
    ------------------------------------
  * Elapsed: $$ || _test.elapsed_perc || $$
  * Reminder threshold: $$ || _test.threshold || $$
  * Your status: ONGOING

Please, do not reply to this message.

Best,

Edgar$$,
                ---------
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP,
                'Mailman'
                );
        END LOOP;
        INSERT INTO email_handled_reminders (id_test, threshold, reminder_type, ts_created, ts_modified, user_modified)
                VALUES (_test.id, _test.threshold, 'ongoing', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'Mailman');
    END LOOP;
    RETURN _numProcessed;
END;
$_$;


ALTER FUNCTION public.email_process_queue(_mail_from text, _batch_size integer) OWNER TO postgres;

--
-- Name: emptyarrayelem2zero(text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.emptyarrayelem2zero(_arr text[]) RETURNS text[]
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
    DECLARE _newArr TEXT ARRAY;
    DECLARE i INT;
BEGIN
    IF (array_length(_arr, 1) IS NULL OR array_length(_arr, 1) = 0) THEN
        RETURN _arr;
    END IF;
    FOR i in 1..array_length(_arr, 1) LOOP
        IF trim(_arr[i]) = '' THEN
            _newArr[i] = '0';
        ELSE
            _newArr[i] = _arr[i];
        END IF;
    END LOOP;
    RETURN _newArr;
END;
$$;


ALTER FUNCTION public.emptyarrayelem2zero(_arr text[]) OWNER TO postgres;

--
-- Name: exercise_student_assign_new_question(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.exercise_student_assign_new_question(_id_exercise integer, _id_student integer, _id_difficulty_level integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_question record;
	_answers_no integer;
  _latest_ordinal integer;
	_answers_permutation integer[];
BEGIN
	SELECT question.id, has_answers INTO _question
	FROM question JOIN question_type
		ON question.id_question_type = question_type.id
	JOIN v_exercise_question v_eq
		ON question.id = v_eq.id_question
	JOIN question_assigned_difficulty qad
		ON qad.id_question = v_eq.id_question
	WHERE id_exercise = _id_exercise
		AND id_difficulty_level = _id_difficulty_level
		AND v_eq.id_question NOT IN (
			SELECT esq.id_question
			FROM exercise_student_question esq
			WHERE esq.id_student = _id_student
				AND esq.id_exercise = _id_exercise
		)
	ORDER BY RANDOM()
	LIMIT 1;

	IF _question.id IS NULL THEN
		RETURN -1;
	END IF;

	SELECT COUNT(*) INTO _answers_no
	FROM question_answer
	WHERE id_question = _question.id;

  SELECT MAX(ordinal)::integer INTO _latest_ordinal
  FROM exercise_student_question
  WHERE id_student = _id_student
  AND id_exercise = _id_exercise;

	IF _latest_ordinal IS NULL THEN
		_latest_ordinal = 0;
	END IF;

	_answers_permutation = gen_answers_ordinal_permutation(_answers_no);

	INSERT INTO public.exercise_student_question(
	  id_exercise, id_student, id_question, ordinal, answers_permutation)
	  VALUES (_id_exercise, _id_student, _question.id, _latest_ordinal + 1, _answers_permutation);

	RETURN _question.id;
END
$$;


ALTER FUNCTION public.exercise_student_assign_new_question(_id_exercise integer, _id_student integer, _id_difficulty_level integer) OWNER TO postgres;

--
-- Name: gen_answers_ordinal_permutation(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.gen_answers_ordinal_permutation(_answers integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$
DECLARE
	_rv integer[];
BEGIN
	WITH RECURSIVE r(n,p,a,b)
	AS (SELECT (SELECT trunc(random() * (factorial(_answers)::integer)))::integer, '{}'::INTEGER[],
	    (select array_agg(i::integer) from generate_series(1, _answers) i), _answers :: INTEGER
	    UNION ALL
	    SELECT n / b, p || a[n % b + 1], a[1:n % b] || a[n % b + 2:b], b-1
	    FROM r
	    WHERE b > 0)
    SELECT p INTO _rv FROM r WHERE b=0;
	RETURN _rv;
END
$$;


ALTER FUNCTION public.gen_answers_ordinal_permutation(_answers integer) OWNER TO postgres;

--
-- Name: gen_test_instance(integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.gen_test_instance(_id_student integer, _id_test integer, _ip_address character varying) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _questions_no  SMALLINT ;
    _row RECORD;
    _new_test_instance_id INTEGER;
    _generated INTEGER;
    _fixed_questions BOOLEAN;
BEGIN
    _questions_no = (SELECT test.questions_no FROM test WHERE test.id = _id_test);
    --check if test type is Lecture quiz
    DROP TABLE if exists tiq;
    CREATE TEMP TABLE tiq (
        --id                SERIAL PRIMARY KEY NOT NULL,
        --id_test_instance  integer not NULL REFERENCES test_instance(id),
        id_test_part      integer,  -- fixed will not set this
        id_question       integer not NULL ,
        id_question_type  integer not NULL ,
        --ordinal           smallint not NULL CONSTRAINT ordinal_btw_0_and_100  CHECK (ordinal BETWEEN 0 and 100),
        id_grading_model  integer not NULL ,
        answers_permutation             INTEGER[],
        weights_permutation             INTEGER[],
        penalty_percentage_permutation  INTEGER[],
        correct_answers_permutation     INTEGER[],
        fixed_test_ordinal integer not null default 0,  -- only fixed test will set this, used when inserting at the bottom
        skip_permutations boolean not null default false -- lecture quiz and PA; dont want to shuffle them
    );
    _fixed_questions := false;

    IF (SELECT fixed_questions
          FROM test
          JOIN test_type
            ON test.id_test_type = test_type.id
          WHERE test.id = _id_test ) = true THEN
        _fixed_questions := true;
        -- for lecture quizzes and competitions:
        FOR _row IN (SELECT test_question.*, question.id_question_type, test_type.skip_permutations
                       FROM test_question
                       JOIN question
                         ON test_question.id_question = question.id
                       JOIN test
                         ON test_question.id_test = test.id
                       JOIN test_type
                         ON test.id_test_type = test_type.id
                      WHERE id_test = _id_test
                      ORDER BY ordinal)
        LOOP
          CASE
            WHEN _row.id_question_type = 9 THEN  -- Connected Elements
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                              correct_answers_permutation, penalty_percentage_permutation, weights_permutation
                              , fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM connected_elements_question_answer where id_question = _row.id_question),
                  (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = _row.id_question),
                  _row.ordinal,
                  _row.skip_permutations
                        );
            WHEN _row.id_question_type = 11  THEN
              INSERT INTO tiq (id_question, id_question_type, id_grading_model,
                      fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  _row.ordinal,
                  _row.skip_permutations
                        );

            ELSE
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                      correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
              VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                  (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  _row.ordinal,
                  _row.skip_permutations
              );
          END CASE;
        END LOOP;
        -- For PA#2 tests:
        FOR _row IN (
           SELECT peer_test_group_question.*, question.id_question_type
             FROM peer_test_group_question
             JOIN question
               ON peer_test_group_question.id_question = question.id
             JOIN peer_test_group
               ON peer_test_group.id = peer_test_group_question.id_peer_test_group
             JOIN peer_test
               ON peer_test_group.id_peer_test = peer_test.id
             JOIN test
               ON test.id = _id_test
              AND test.id_parent = peer_test.id_test_phase_2
             JOIN test_instance ti1
               ON ti1.id_test = peer_test.id_test_phase_1
              AND ti1.id_student = _id_student
             JOIN test_instance_question tiq1
               ON tiq1.ordinal = 1
              AND tiq1.id_test_instance = ti1.id
              AND peer_test_group.id_question = tiq1.id_question
            ORDER BY peer_test_group_question.ordinal)
        LOOP
            INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
            VALUES (
                    _row.id_question,
                    _row.id_question_type,
                    _row.id_grading_model,
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                    (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    _row.ordinal,
                    true
                    );
        END LOOP;
    ELSE
        -- Non-fixed test, let's generate the questions and insert them:
        DROP TABLE IF EXISTS tpart;
        CREATE TEMP table tpart AS
            SELECT test_part.id AS id_test_part, id_node, id_grading_model, min_questions, max_questions, min_questions AS gen_questions
            FROM   test_part
            WHERE  test_part.id_test = _id_test;

        -- #1. Fullfil min_questions request:
        _generated := 0;
        FOR _row IN SELECT min_questions, id_node, id_grading_model, id_test_part
                    FROM tpart
                    WHERE min_questions > 0
        LOOP
            INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
            SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                  CASE WHEN (question.id_question_type = 9)
                    THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                    ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                  END,
                  (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
            FROM   question JOIN question_node
                            ON question.id = question_node.id_question
                            AND is_active
            WHERE  question_node.id_node = _row.id_node
            AND    question.id NOT IN (SELECT id_question FROM tiq)
            ORDER BY random()
            LIMIT _row.min_questions;
            _generated = _generated + _row.min_questions; -- If there are less than that availale in the DB, this var will lie! Better than inf loop.
        END LOOP;

        -- #2. Add optional questions to achieve targer q.no:
        FOR _row IN
            -- skew the distribution (with max 20) towards those with bigger upper limit (max_questions):
            SELECT id_node, id_grading_model, id_test_part
            FROM  tpart
            JOIN  generate_series(0, 20) idx
            ON    (tpart.max_questions - tpart.min_questions) > idx
            ORDER BY random()
            LIMIT (_questions_no - _generated)
        LOOP
            INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
            SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                  CASE WHEN (question.id_question_type = 9)
                    THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                    ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                  END,
                  (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
            FROM   question JOIN question_node
                            ON question.id = question_node.id_question
                            AND is_active
            WHERE  question_node.id_node = _row.id_node
            AND    question.id NOT IN (SELECT id_question FROM tiq)
            ORDER BY random()
            LIMIT 1;
        END LOOP;

    END IF;

    -- #3. (a) Set permutations for those questions that have multiple answers:
    FOR _row IN
    (
      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg(cqa.ordinal) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   connected_elements_question_answer cqa
        ON   tiq.id_question = cqa.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type

      UNION

      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg( case when question_answer.is_correct THEN question_answer.ordinal ELSE NULL END) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   question_answer
        ON   tiq.id_question = question_answer.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type
    )
    LOOP
      UPDATE tiq
      SET    answers_permutation = (
            WITH RECURSIVE r(n,p,a,b)
              AS (SELECT (SELECT trunc(random() * (factorial(_row.no_answers)::integer)))::integer, '{}'::INTEGER[],
                (SELECT array_agg(i::integer) from generate_series(1, _row.no_answers) i), _row.no_answers :: INTEGER
                UNION ALL
                SELECT n / b, p || a[n % b + 1], a[1:n % b] || a[n % b + 2:b], b-1
                FROM r
                WHERE b > 0)
            SELECT p  FROM r WHERE b=0
           ),
           correct_answers_permutation = _row.correct_answers_permutation -- still, to be updated
      WHERE tiq.id_question = _row.id_question;

    END LOOP;

    -- #3. (b) Update correct answers and weights:
    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT    array_agg(perm.nr)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   unnest(tiq.correct_answers_permutation) correct(ordinal)
            ON   perm.ordinal = correct.ordinal
          AND   correct.ordinal IS NOT NULL)
    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type NOT IN (8, 9);  -- TODO: fix this

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by ordinal)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          )
    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 8;

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by perm.ordinal)
           FROM  unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
           JOIN  connected_elements_question_answer cqa
             ON  tiq.id_question = cqa.id_question
            AND (trim(answer_text_static) <> '') IS TRUE  -- only those that have static answers, bcs there can be more dynamic than static answers
            AND perm.ordinal = cqa.ordinal
          )

    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 9;  -- TODO: fix this

    UPDATE tiq
      SET    weights_permutation =

        (SELECT    array_agg(question_answer.threshold_weight)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false  -- skip lecture quizzes and PAs;
      AND tiq.id_question_type <> 9;  -- skip connected elements

    UPDATE tiq
      SET    penalty_percentage_permutation =

        (SELECT    array_agg(question_answer.penalty_percentage)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false;  -- skip lecture quizzes and PAs;

    -- **********************************************
    -- * Finally, save it:
    -- **********************************************
    INSERT INTO test_instance (id_test, id_student, ts_generated, ts_started, ip_address)
       VALUES (_id_test, _id_student, current_timestamp, NULL, _ip_address);

    _new_test_instance_id = (SELECT currval('test_instance_id_seq'));

    IF (_fixed_questions) THEN
        INSERT into test_instance_question (id_test_instance, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_question,
            (row_number() OVER (ORDER BY fixed_test_ordinal)) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        ORDER BY fixed_test_ordinal;

    ELSE
        INSERT into test_instance_question (id_test_instance, id_test_part, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_test_part,
            tiq.id_question,
            (row_number() OVER (ORDER BY test_part.ordinal, random())) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        JOIN test_part
            ON tiq.id_test_part = test_part.id
        ORDER BY test_part.ordinal, random();

        -- cleanup:
        DROP TABLE IF EXISTS tpart;
    END IF;

    -- cleanup:
    DROP TABLE if exists tiq;

    RETURN _new_test_instance_id;

END;
$$;


ALTER FUNCTION public.gen_test_instance(_id_student integer, _id_test integer, _ip_address character varying) OWNER TO postgres;

--
-- Name: generate_tutorial_steps_with_questions(integer, integer, integer, integer, smallint, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.generate_tutorial_steps_with_questions(_id_tutorial integer, _id_node_tutorial integer, _no_steps integer, _id_question_type integer, _answers_count smallint, _id_code_runner integer, _id_app_user integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  _generated integer := 0;
  _ordinal integer := 0;
  _new_question_id integer;
BEGIN
  LOOP
    EXIT WHEN _ordinal >= _no_steps;
    _ordinal = _ordinal + 1;

    _new_question_id = null;
    IF (_id_question_type IS NOT NULL) THEN
      -- insert question with answers
      SELECT new_question(
        _id_question_type,
        _id_code_runner,
        _answers_count,
        _id_app_user,
        intset(_id_node_tutorial),
        false, -- TODO: can tuts have these?
        false, -- TODO: can tuts have these?
        null   -- TODO: prog questions tuts
        )
      INTO _new_question_id;
    END IF;
    -- insert step
    INSERT INTO public.tutorial_step(
      id_tutorial,
      id_question,
      id_user_created,
      id_user_modified,
      ordinal,
      title,
      text)
    SELECT 	_id_tutorial,
      _new_question_id,
      _id_app_user,
      _id_app_user,
      _ordinal,
      'Generated step title, please update',
      'Generated step text, please update';
  END LOOP;
END
$$;


ALTER FUNCTION public.generate_tutorial_steps_with_questions(_id_tutorial integer, _id_node_tutorial integer, _no_steps integer, _id_question_type integer, _answers_count smallint, _id_code_runner integer, _id_app_user integer) OWNER TO postgres;

--
-- Name: get_question_runtime_constraints(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_question_runtime_constraints(_id_course integer, _id_question integer) RETURNS TABLE(programming_language_id integer, constraint_id integer, constraint_name text, constraint_value text, constraint_type text, j0_unit_type text, override_level text)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN

    RETURN QUERY SELECT pl.id, rtc.id, rtc.name,
                 	COALESCE (rtc_q.override_value,
                                         rtc_c.override_value,
                                         rtc.default_value),
                                         dt.type_name, j0ut.type_name,
                    CASE
                        WHEN (rtc_q.override_value IS NOT NULL) THEN 'question'
                        WHEN (rtc_c.override_value IS NOT NULL) THEN 'course'
                        ELSE 'default'
                    END AS override_level
                 FROM programming_language pl CROSS JOIN runtime_constraint rtc
                  LEFT JOIN runtime_constraint_course rtc_c
                            ON rtc.id = rtc_c.id_runtime_constraint
                 		   AND pl.id = rtc_c.id_programming_language
                           AND rtc_c.id_course = _id_course
                  LEFT JOIN runtime_constraint_question rtc_q
                            ON rtc.id = rtc_q.id_runtime_constraint
                 		   AND pl.id = rtc_q.id_programming_language
                            AND rtc_q.id_question = _id_question
                 LEFT JOIN j0_unit_type j0ut ON j0ut.id = rtc.id_j0_unit_type
                 JOIN data_type dt ON dt.id = rtc.id_data_type
                 WHERE pl.id IN (SELECT qpl.id_programming_language FROM question_programming_language qpl WHERE id_question = _id_question)
                 ORDER BY pl.id, rtc.id;
END;
$$;


ALTER FUNCTION public.get_question_runtime_constraints(_id_course integer, _id_question integer) OWNER TO postgres;

--
-- Name: get_question_tests_runtime_constraints(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_question_tests_runtime_constraints(_id_course integer, _id_question integer) RETURNS TABLE(c_question_test_id integer, programming_language_id integer, constraint_id integer, constraint_name text, constraint_value text, constraint_type text, j0_unit_type text, override_level text)
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    RETURN QUERY SELECT cqt.id, pl.id, rtc.id, rtc.name,
                                    COALESCE (    rtc_t.override_value,
                                                  rtc_q.override_value,
                                                  rtc_c.override_value,
                                                  rtc.default_value),
                                                  dt.type_name,
                                                  j0ut.type_name,
                                    CASE
                                        WHEN (rtc_t.override_value IS NOT NULL) THEN 'test'
                                        WHEN (rtc_q.override_value IS NOT NULL) THEN 'question'
                                        WHEN (rtc_c.override_value IS NOT NULL) THEN 'course'
                                        ELSE 'default'
                                    END AS override_level
                                  FROM c_question_test cqt CROSS JOIN programming_language pl CROSS JOIN runtime_constraint rtc
                                   LEFT JOIN runtime_constraint_course rtc_c
                                             ON rtc.id = rtc_c.id_runtime_constraint
                                  		   AND pl.id = rtc_c.id_programming_language
                                            AND rtc_c.id_course = _id_course
                                   LEFT JOIN runtime_constraint_question rtc_q
                                             ON rtc.id = rtc_q.id_runtime_constraint
                                  		   AND pl.id = rtc_q.id_programming_language
                                             AND rtc_q.id_question = _id_question
                                   LEFT JOIN runtime_constraint_test rtc_t
                                             ON rtc.id = rtc_t.id_runtime_constraint
                                             AND pl.id = rtc_t.id_programming_language
                                             AND rtc_t.id_c_question_test = cqt.id
                                  LEFT JOIN j0_unit_type j0ut ON j0ut.id = rtc.id_j0_unit_type
                                  JOIN data_type dt ON dt.id = rtc.id_data_type
                                  WHERE pl.id IN (SELECT qpl.id_programming_language FROM question_programming_language qpl WHERE id_question = _id_question)
                                  AND cqt.id IN (SELECT c_question_test.id FROM c_question_test
                                                         WHERE c_question_answer_id IN (SELECT c_question_answer.id FROM c_question_answer WHERE id_question = _id_question))
                 																	   order by cqt.id, pl.id, rtc.id;
END;
$$;


ALTER FUNCTION public.get_question_tests_runtime_constraints(_id_course integer, _id_question integer) OWNER TO postgres;

--
-- Name: get_tutorial_node_id(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_tutorial_node_id(_id_tutorial integer, _id_course integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_id_tutorials_root_node integer;
	_tutorial_title text;
	_rv integer;
BEGIN
	SELECT id_child INTO _id_tutorials_root_node
	FROM v_roots_children JOIN node
	    ON v_roots_children.id_child = node.id
	    JOIN node_type
		ON node.id_node_type = node_type.id
	WHERE type_name='Tutorial'
	    AND id_parent = _id_course;

	SELECT tutorial_title INTO _tutorial_title
	FROM tutorial
	WHERE id = _id_tutorial;

	SELECT id_child INTO _rv
	FROM v_roots_children
	WHERE id_parent = _id_tutorials_root_node;

	RETURN _rv;
END
$$;


ALTER FUNCTION public.get_tutorial_node_id(_id_tutorial integer, _id_course integer) OWNER TO postgres;

--
-- Name: ins_email_queue_for_ticket_subcription(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ins_email_queue_for_ticket_subcription() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
   --1. ticket nastane, status je 'open', id_assigned_user je NULL
   --2. ticket preuzme nastavnik, status je 'open', promijeni se id_assigned_user u NOT NULL
   --3. nastavnik odgovara, status je 'closed' --ovdje se jedino ne šalje ticket
   --4. student opet otvara ticket, status je 'open' --i dalje u krug

   DECLARE p_ticket_status VARCHAR(100);

   BEGIN
         --/*
	  IF (TG_OP = 'INSERT') THEN
         p_ticket_status = 'new - open';
      ELSE -- TG_OP = 'UPDATE' AND NEW.status = 'open' -ovo piše u WHEN
	     p_ticket_status = 'old - reopened';

		 IF (OLD.id_assigned_user IS NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je preuzimanje ticketa
		    SELECT 'assigned to user: ' || app_user.email INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
         ELSIF (OLD.id_assigned_user IS NOT NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je reopen ticketa i nalijepim usera da se odmah u mailu vidi treba li reagirati ili ignorirati
            SELECT 'old - reopened (previously assigned to user: ' || app_user.email || ')' INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
         END IF;

	  END IF;
	  		-- */
      INSERT INTO email_queue (user_modified, mail_to, mail_cc, mail_bcc, mail_from, mail_subject, mail_text)

       SELECT 'postgres'
            , 'edgar@fer.hr', ''
	       	, app_user.email
            , 'edgar@fer.hr'
	       	, 'Ticket raised (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' - test ' || test.id || ' - ' || TRIM(test.title)
            , 'Ticket raised (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' in test ' || test.id || ' - ' || TRIM(test.title) || '.' || chr(10) ||chr(10) ||
	       	'Access tickets for the test using this link: https://edgar.fer.hr/ticket/test/' || test.id || '# '|| chr(10) || chr(10) ||
	       	--'Ticket message: ' || (ticket.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	' You have received this email because you are subscribed for it.' || chr(10)||chr(10)||
	       	'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
        FROM test_instance_question, test_instance, test, ticket, ticket_subscription, app_user
       WHERE test_instance_question.id_test_instance = test_instance.id
	        AND ticket.id = NEW.id
	        AND test_instance_question.id = ticket.id_test_instance_question
	        AND test_instance.id_test = test.id
	        AND ticket_subscription.id_test = test_instance.id_test
	        AND ticket_subscription.id_question = test_instance_question.id_question
	        AND ticket_subscription.id_app_user = app_user.id
       UNION
       SELECT 'postgres'
            , 'edgar@fer.hr', ''
	       	, app_user.email
            , 'edgar@fer.hr'
	       	, 'Ticket raised (status: ' || p_ticket_status || ') - test ' || test.id || ' - ' || TRIM(test.title)
            , 'Ticket raised (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' in test ' || test.id || ' - ' || TRIM(test.title) || '.' || chr(10) ||chr(10) ||
	       	'Access tickets for the test using this link: https://edgar.fer.hr/ticket/test/' || test.id || '# '|| chr(10) || chr(10) ||
	       	--'Ticket message: ' || (ticket.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	' You have received this email because you are subscribed for it.' || chr(10)||chr(10)||
	       	'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
        FROM test_instance_question, test_instance, test, ticket, ticket_subscription, app_user
       WHERE test_instance_question.id_test_instance = test_instance.id
	        AND ticket.id = NEW.id
	        AND test_instance_question.id = ticket.id_test_instance_question
	        AND test_instance.id_test = test.id
	        AND ticket_subscription.id_test = test_instance.id_test
	        AND ticket_subscription.id_question IS NULL
	        AND ticket_subscription.id_app_user = app_user.id;

      RETURN NEW;
   END;
$$;


ALTER FUNCTION public.ins_email_queue_for_ticket_subcription() OWNER TO postgres;

--
-- Name: ins_email_queue_for_tutorial_ticket_subcription(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ins_email_queue_for_tutorial_ticket_subcription() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  --1. ticket nastane, status je 'open', id_assigned_user je NULL
  --2. ticket preuzme nastavnik, status je 'open', promijeni se id_assigned_user u NOT NULL
  --3. nastavnik odgovara, status je 'closed' --ovdje se jedino ne šalje ticket
  --4. student opet otvara ticket, status je 'open' --i dalje u krug
  DECLARE p_ticket_status VARCHAR(100);

  BEGIN
        --/*
   IF (TG_OP = 'INSERT') THEN
     p_ticket_status = 'NEW - Open';
   ELSE -- TG_OP = 'UPDATE' AND NEW.status = 'open' -ovo piše u WHEN
     -- p_ticket_status = 'OLD - Reopened';
     IF (OLD.id_assigned_user IS NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je preuzimanje ticketa
       SELECT 'ASSIGNED to user: ' || app_user.email INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
     ELSIF (OLD.id_assigned_user IS NOT NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je reopen ticketa i nalijepim usera da se odmah u mailu vidi treba li reagirati ili ignorirati
       SELECT 'OLD - REOPENED (previously assigned to user: ' || app_user.email || ')' INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
     END IF;
   END IF;
       -- */
     INSERT INTO email_queue (user_modified, mail_to, mail_cc, mail_bcc, mail_from, mail_subject, mail_text)
      SELECT 'postgres'
           , 'edgar@fer.hr'
           , ''
           , app_user.email || 'edgar@fer.hr'
           , 'edgar@fer.hr'
           , 'Tutorial ticket (status: ' || p_ticket_status || ') - ' || ' - tutorial ' || tutorial.id || ' - ' || TRIM(tutorial.tutorial_title)
           , 'Tutorial ticket (status: ' || p_ticket_status || ') - ' || ' in tutorial ' || tutorial.id || ' - ' || TRIM(tutorial.tutorial_title) || '.'
                || chr(10) ||chr(10) || 'Step: ' || tutorial_step.ordinal :: text || ': ' || tutorial_step.title || chr(10) ||chr(10) ||
                'Access tickets for the tutorial using this link: https://edgar.fer.hr/ticket/tutorial/' || tutorial.id || '# '|| chr(10) || chr(10) ||
                'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
                'You have received this email because you (' || COALESCE(app_user.email, 'mail not set!?') || ') are subscribed for it.' || chr(10)||chr(10)||
                'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
       FROM  tutorial_ticket
       JOIN  tutorial_step
         ON  tutorial_ticket.id_tutorial_step = tutorial_step.id
       JOIN  tutorial
         ON  tutorial_step.id_tutorial = tutorial.id
       JOIN  tutorial_ticket_subscription
         ON  tutorial_ticket_subscription.id_tutorial = tutorial.id
       JOIN  app_user
         ON  tutorial_ticket_subscription.id_app_user = app_user.id
      WHERE  tutorial_ticket.id = NEW.id;
     RETURN NEW;
  END;
$$;


ALTER FUNCTION public.ins_email_queue_for_tutorial_ticket_subcription() OWNER TO postgres;

--
-- Name: insert_anonymous_in_student(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insert_anonymous_in_student(num integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        _max_ordinal INTEGER;
    BEGIN
        _max_ordinal = (SELECT COALESCE(MAX(id), 0) FROM test_question student);
        FOR i IN _max_ordinal..num LOOP
            INSERT INTO student(alt_id, first_name, last_name, is_anonymous)
            VALUES ('anon'::VARCHAR || i::VARCHAR || '@fer.hr', 'Anonymous'::VARCHAR || i::VARCHAR, 'Anonymous'::VARCHAR || i::VARCHAR, true);
        END LOOP;
    END
$$;


ALTER FUNCTION public.insert_anonymous_in_student(num integer) OWNER TO postgres;

--
-- Name: is_virgin_tutorial(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.is_virgin_tutorial(_id_tutorial integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  _rows integer;
BEGIN
  SELECT COUNT(*) INTO _rows
  FROM tutorial_student
  WHERE id_tutorial = _id_tutorial;

	RETURN CASE WHEN _rows = 0 THEN true ELSE false END;
END
$$;


ALTER FUNCTION public.is_virgin_tutorial(_id_tutorial integer) OWNER TO postgres;

--
-- Name: new_exercise(integer, integer, integer, integer, integer[], text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.new_exercise(_id_course integer, _id_academic_year integer, _id_adaptivity_model integer, _id_app_user integer, _node_ids integer[], _title text, _description text, _is_active boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_id_exercise integer;
	i integer;
BEGIN
	-- insert exercise
	INSERT INTO public.exercise (id_course, id_academic_year, id_adaptivity_model,
		id_user_created, id_user_modified, title, description, is_active) VALUES(
		_id_course, _id_academic_year, _id_adaptivity_model, _id_app_user,
		_id_app_user, _title, _description, _is_active)
	RETURNING id INTO _id_exercise;

	-- insert exercise nodes
	FOR i IN 1 .. array_upper(_node_ids, 1)
	LOOP
	   INSERT INTO public.exercise_node (id_exercise, id_node)
		VALUES(_id_exercise, _node_ids[i]);
	END LOOP;

	RETURN _id_exercise;
END
$$;


ALTER FUNCTION public.new_exercise(_id_course integer, _id_academic_year integer, _id_adaptivity_model integer, _id_app_user integer, _node_ids integer[], _title text, _description text, _is_active boolean) OWNER TO postgres;

--
-- Name: new_question(integer, integer, smallint, smallint, smallint, integer, integer[], boolean, boolean, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.new_question(_id_question_type integer, _id_code_runner integer, _answers_count smallint, _cards_count1 smallint, _cards_count2 smallint, _id_user integer, _arr_node_ids integer[], use_template boolean, use_eval_script boolean, _id_programming_languages integer[] DEFAULT NULL::integer[]) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _new_id INTEGER;
    i integer;
    _new_answer_id INTEGER;
    _new_question_test_id INTEGER;
    _pl RECORD;
BEGIN
    INSERT INTO question (VERSION, is_active, id_question_type, question_text, id_user_created, id_user_modified)
    VALUES (0, FALSE, _id_question_type,
      CASE
        WHEN (_id_question_type = 1) THEN '2+2?'
        WHEN (_id_question_type = 7) THEN 'Is this statement true or false?'
        WHEN (_id_question_type = 8) THEN 'Please, use drag and drop to order the assigned objects.'
        WHEN (_id_question_type = 9) THEN 'Please, use drag and drop to connect the assigned objects.'
        WHEN (_id_question_type = 10) THEN 'Please, fill in the blanks.'
        WHEN (_id_question_type = 11) THEN 'Add the following elements to the B-tree: `1,10,20`.
You can try these diagram types too:
```
sequenceDiagram
Alice->>John: Hello John, how are you?
John-->>Alice: Great!
Alice-)John: See you later!
```
or this:
```
stateDiagram
[*] --> Still
Still --> [*]
Still --> Moving
Moving --> Still
Moving --> Crash
Crash --> [*]
Crash --> Still
```
See more at mermaid.js'
        WHEN (_id_question_type = 25) THEN 'This ''question'' is just a placeholder for the attachement, please attach the file that will be given to the student. Make sure you have enabled question attachments in the course settings.'
        ELSE 'Please, answer my clever question...'
      END, _id_user, _id_user)
    RETURNING id INTO _new_id;

    IF (use_template) THEN
        INSERT INTO question_data_object (id_question) VALUES (_new_id);
     END IF;

    IF (use_eval_script) THEN
        INSERT INTO question_eval_script (id_question) VALUES (_new_id);
     END IF;

    IF (_id_question_type = 1) THEN
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 1,
                 TRUE,
                 '4');
        FOR i IN 2.._answers_count LOOP
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 i,
                 FALSE,
                 'dunno');
         END LOOP;
   END IF;
   IF(_id_question_type = 7) THEN
     INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 1,
                 TRUE,
                 'True');
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 2,
                 FALSE,
                 'False');
   END IF;
   IF(_id_question_type = 8) THEN
        FOR i IN 1.._answers_count LOOP
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 i,
                 TRUE,
           i);
         END LOOP;
        INSERT INTO ordered_element_question (id_question) values (_new_id);
   END IF;
   IF(_id_question_type = 9) THEN
     IF(_cards_count1 <= _cards_count2) THEN
      FOR i IN 1.._cards_count2 LOOP
        INSERT INTO connected_elements_question_answer (id_question, ordinal, answer_text_static, answer_text_dynamic, ts_created, ts_modified, user_modified)
        VALUES (_new_id,
             i,
            CASE WHEN i <= _cards_count1 THEN i ELSE NULL END,
             i*11,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP,
             'postgres'
             );
       END LOOP;
     ELSE
       FOR i IN 1.._cards_count1 LOOP
        INSERT INTO connected_elements_question_answer (id_question, ordinal, answer_text_static, answer_text_dynamic, ts_created, ts_modified, user_modified)
        VALUES (_new_id,
             i,
             i,
             CASE WHEN i <= _cards_count2 THEN i*11 ELSE NULL END,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP,
             'postgres'
             );
       END LOOP;
     END IF;
     INSERT INTO connected_elements_question (id_question) values (_new_id);
   END IF;

   IF(_id_question_type = 11) THEN
     INSERT INTO diagram_question_answer (id_question, initial_diagram_answer, diagram_answer)
              VALUES (_new_id, 'graph
7-->3
7-->12
3-->2
3-->5,6
12-->8
12-->15,23',
           'graph
7-->3
7-->12,20
3-->1,2
3-->5,6
12,20-->8,10
12,20-->15
12,20-->23');
    END IF;



     --ELSE
     -- Handle additional question types here
     -- SQL + C
        IF(_id_question_type = 2) THEN
            -- Handle additional question types here
            INSERT INTO sql_question_answer (id_question, id_code_runner, sql_answer)
            VALUES (_new_id, _id_code_runner, 'SELECT * FROM student');

            FOR _pl IN
              SELECT DISTINCT id_programming_language
              FROM course_code_runner
              WHERE id_code_runner = _id_code_runner
            LOOP
              INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _pl.id_programming_language);
            END LOOP;

        END IF;
        IF(_id_question_type = 21) THEN
              INSERT INTO json_question_answer (id_question, id_code_runner, json_answer)
              VALUES (_new_id, _id_code_runner, 'db.collection.find({});');
        END IF;
        IF(_id_question_type = 20) THEN
              INSERT INTO text_question_answer (id_question, text_answer)
              VALUES (_new_id, '42.');
        END IF;
        IF(_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            INSERT INTO c_question_answer ( id_question, c_prefix, c_suffix, c_source, id_programming_language)
            VALUES (_new_id, '', '',
                CASE WHEN (array_length(_id_programming_languages, 1) = 1) THEN
                  (SELECT hello_world FROM programming_language WHERE id = _id_programming_languages[1])
                ELSE
                    ''
              END, _id_programming_languages[1]
              )
            RETURNING id INTO _new_answer_id;

            INSERT INTO c_question_test ( c_question_answer_id, c_test_type_id, percentage, allow_diff_order, allow_diff_letter_size, trim_whitespace)
            VALUES (_new_answer_id, 1, 100, FALSE, TRUE, TRUE)
            RETURNING id INTO _new_question_test_id;

            INSERT INTO fixed_test( c_question_test_id, input, output)
            VALUES (_new_question_test_id, '1', '1');

            FOR i in array_lower(_id_programming_languages, 1)..array_upper(_id_programming_languages, 1) LOOP
              INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _id_programming_languages[i]);
            END LOOP;
        END IF;
        IF(_id_question_type = 26 AND (SELECT COUNT(*) FROM scale) > 0) THEN
              INSERT INTO question_scale (id_question, id_scale)
              SELECT _new_id, max(id)
                FROM scale;
        END IF;
     -- END IF;

    FOR i in array_lower(_arr_node_ids, 1)..array_upper(_arr_node_ids, 1) loop
        INSERT INTO question_node (id_question, id_node) VALUES (_new_id, _arr_node_ids[i]);
    END LOOP;

    RETURN _new_id;
END;
$$;


ALTER FUNCTION public.new_question(_id_question_type integer, _id_code_runner integer, _answers_count smallint, _cards_count1 smallint, _cards_count2 smallint, _id_user integer, _arr_node_ids integer[], use_template boolean, use_eval_script boolean, _id_programming_languages integer[]) OWNER TO postgres;

--
-- Name: new_test(integer, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.new_test(_id_test_type integer, _title character varying, _id_course integer, _id_academic_year integer, _id_user_created integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  new_id INTEGER;
BEGIN
  INSERT INTO test (title, id_course, id_academic_year, id_semester, id_test_type, id_user_created, test_ordinal, max_runs, show_solutions, hint_result, max_score, password, questions_no, duration_seconds, pass_percentage, ts_available_from, ts_available_to, review_period_mins)
  VALUES ( _title, _id_course, _id_academic_year,
            (SELECT CASE WHEN (EXTRACT ('month'
                                        FROM CURRENT_DATE) BETWEEN 2 AND 8) THEN 2 ELSE 1 END), _id_test_type, _id_user_created,
            (SELECT COALESCE(max(test_ordinal), 0) + 1
             FROM test
             WHERE id_course = _id_course
               AND id_academic_year = _id_academic_year), 1, FALSE, TRUE, 100,
            (SELECT string_agg (substr('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', ceil (random() * 62)::INTEGER, 1), '')
             FROM generate_series(1, 45)), 10,
                                           600,
                                           0,
                                           CURRENT_TIMESTAMP,
                                           CURRENT_TIMESTAMP + '10 Minutes'::INTERVAL,
                                                               10 ) RETURNING id INTO new_id;
    RETURN new_id;
END;
$$;


ALTER FUNCTION public.new_test(_id_test_type integer, _title character varying, _id_course integer, _id_academic_year integer, _id_user_created integer) OWNER TO postgres;

--
-- Name: new_tutorial(text, text, boolean, integer, integer, integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.new_tutorial(_title text, _description text, _is_active boolean, _no_steps integer, _answers_count integer, _id_question_type integer, _id_code_runner integer, _id_course integer, _id_app_user integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_id_tutorial integer;
	_id_tutorials_course_root_node integer;
	_id_node_tutorial integer;
BEGIN

  -- Find the tutorials course root node id
  _id_tutorials_course_root_node = tutorials_course_root_node_id(_id_course);

	-- Create the tutorial node for questions used in steps of this tutorial
  INSERT INTO public.node(
                id_node_type, node_name)
            VALUES ((SELECT id FROM node_type WHERE type_name = 'Tutorial'), _title)
            RETURNING id INTO _id_node_tutorial;

  -- Assign tutorials course root node as its parent
  INSERT INTO public.node_parent(
        id_child, id_parent)
        VALUES (_id_node_tutorial, _id_tutorials_course_root_node);

  -- Update the tutorial node id for the cloned tutorial
  UPDATE tutorial
  SET id_node = _id_node_tutorial
  WHERE id = _id_tutorial;

	INSERT INTO public.tutorial(
            id_node, id_user_created, id_user_modified,
            tutorial_title, tutorial_desc, is_active)
            VALUES (_id_node_tutorial, _id_app_user, _id_app_user, _title, _description, _is_active)
            RETURNING id INTO _id_tutorial;

	-- Assign tutorial to course
	INSERT INTO public.tutorial_course(
                id_course, id_tutorial)
                VALUES (_id_course, _id_tutorial);

	PERFORM generate_tutorial_steps_with_questions(
		_id_tutorial,
		_id_node_tutorial,
		_no_steps,
		_id_question_type,
		_answers_count::smallint,
		_id_code_runner,
		_id_app_user);

	RETURN _id_tutorial;
END
$$;


ALTER FUNCTION public.new_tutorial(_title text, _description text, _is_active boolean, _no_steps integer, _answers_count integer, _id_question_type integer, _id_code_runner integer, _id_course integer, _id_app_user integer) OWNER TO postgres;

--
-- Name: run_upro_schedule_set_test_params(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.run_upro_schedule_set_test_params() RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    UPDATE public.test T
       SET password = V.test_password
      FROM public.upro_schedule_set_test_params V
     WHERE T.id = V.id_test
       AND TIMESTAMP 'now' BETWEEN V.ts_start and V.ts_end;

    UPDATE public.test_part TP
        SET min_questions = CASE WHEN N.node_name like CONCAT('%L_Z_G',V.group_no,'%') then 1 else 0 end,
            max_questions = CASE WHEN N.node_name like CONCAT('%L_Z_G',V.group_no,'%') then 1 else 0 end
        FROM  public.test T, public.node N, public.upro_schedule_set_test_params V
        WHERE T.id = TP.id_test
            AND T.id = V.id_test
            AND TP.id_node = N.id AND T.title_abbrev LIKE 'L_-C'
            AND TIMESTAMP 'now' BETWEEN V.ts_start and V.ts_end;

    RETURN 0;
END;
$$;


ALTER FUNCTION public.run_upro_schedule_set_test_params() OWNER TO postgres;

--
-- Name: save_correction(text, numeric, integer, integer[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.save_correction(_reason text, _score_delta numeric, _id_app_user integer, _arr_ids integer[]) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  i INTEGER;
BEGIN
    FOR i in array_lower(_arr_ids, 1)..array_upper(_arr_ids, 1) loop
        DELETE FROM test_correction WHERE id_test_instance_question = _arr_ids[i];
        INSERT INTO test_correction (id_test_instance_question, score_delta, id_user_created, reason)
            VALUES (
                    _arr_ids[i],
                    _score_delta,
                    _id_app_user,
                    _reason
                );
    END LOOP;

    -- Update on the question level
    UPDATE test_instance_question
    SET score = coalesce(score, 0) + _score_delta,
        score_perc = (coalesce(score, 0) + _score_delta) / correct_score
    FROM grading_model
    WHERE test_instance_question.id_grading_model = grading_model.id
      AND test_instance_question.id = ANY(_arr_ids);

    -- And on the test level, first score
    UPDATE test_instance
       SET score = (SELECT SUM(score)
                   FROM test_instance_question
                   WHERE id_test_instance = test_instance.id)
    WHERE id IN
        (SELECT id_test_instance
           FROM test_instance_question
          WHERE id = ANY(_arr_ids));

    -- ... and then score perc:
    UPDATE test_instance
       SET score_perc = score / test.max_score
      FROM test
     WHERE test_instance.id_test = test.id
       AND test_instance.id IN
        (SELECT id_test_instance
         FROM test_instance_question
         WHERE id = ANY(_arr_ids));

    RETURN 0;
END;
$$;


ALTER FUNCTION public.save_correction(_reason text, _score_delta numeric, _id_app_user integer, _arr_ids integer[]) OWNER TO postgres;

--
-- Name: save_exercise(integer, integer, integer, integer[], text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.save_exercise(_id_exercise integer, _id_adaptivity_model integer, _id_app_user integer, _node_ids integer[], _title text, _description text, _is_active boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
	i integer;
BEGIN
	-- update exercise
	UPDATE public.exercise
	SET 	id_adaptivity_model = _id_adaptivity_model,
		id_user_modified = _id_app_user,
		is_active = _is_active,
		title = _title,
		description = _description
	WHERE id = _id_exercise;

	-- delete old exercise nodes
	DELETE FROM public.exercise_node
	WHERE id_exercise = _id_exercise;

	-- insert new exercise nodes
	FOR i IN 1 .. array_upper(_node_ids, 1)
	LOOP
	   INSERT INTO public.exercise_node (id_exercise, id_node)
		VALUES(_id_exercise, _node_ids[i]);
	END LOOP;
END
$$;


ALTER FUNCTION public.save_exercise(_id_exercise integer, _id_adaptivity_model integer, _id_app_user integer, _node_ids integer[], _title text, _description text, _is_active boolean) OWNER TO postgres;

--
-- Name: save_in_place_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.save_in_place_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _initial_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer, _bump_version boolean) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _arr_answers text[];
    _arr_answers_static text[];
    _arr_answers_dynamic text[];
    _arr_correct_answers integer[];
    _arr_threshold_weights integer[];
    _arr_penalty_percentages DECIMAL(6, 2)[];
    i INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    _id_question_type INTEGER;
BEGIN
  SELECT has_answers, id_user_created, id_question_type
    INTO _has_answers, _id_user_created, _id_question_type
    FROM question
    JOIN question_type ON question.id_question_type = question_type.id
   WHERE question.id = _id;

    -- Update in place:
    UPDATE question
    SET question_text = _question_text,
        question_comment = _question_comment,
        upload_files_no = _upload_files_no,
        required_upload_files_no = _required_upload_files_no,
        is_active = _is_active,
        id_user_modified = _id_user_modified,
        grader_object = _grader_object,
        time_limit = _time_limit,
        version = CASE _bump_version
                    WHEN TRUE THEN version + 1
                    ELSE version
                  END
    WHERE id = _id;

    UPDATE question_data_object
       SET data_object = _data_object
    WHERE id_question = _id;

    UPDATE question_eval_script
       SET eval_script = _eval_script,
           id_script_programming_language = _id_script_programming_language
    WHERE id_question = _id;

    IF (_has_answers) THEN
        _arr_answers           = string_to_array(_answers_texts, '~~#~~');
        _arr_answers_static    = string_to_array(_answer_text_static, '~~#~~');
        _arr_answers_dynamic   = string_to_array(_answer_text_dynamic, '~~#~~');
        _arr_threshold_weights = string_to_array(_threshold_weights, '~~#~~');
        _arr_penalty_percentages = string_to_array(_penalty_percentages, '~~#~~');
        _arr_correct_answers   = string_to_array(_correct_answers, '~~#~~');

        IF (   array_lower(_arr_answers, 1) <> array_lower(_arr_threshold_weights, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_correct_answers, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_penalty_percentages, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_threshold_weights, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_correct_answers, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_penalty_percentages, 1)
            ) THEN
            RAISE EXCEPTION 'Bad arguments for SQL question (all arrays must be of the same size).';
        END IF;

        IF (_id_question_type = 9) THEN
            FOR i in array_lower(_arr_answers_static, 1)..array_upper(_arr_answers_static, 1) loop
                UPDATE connected_elements_question_answer
                   SET answer_text_static = _arr_answers_static[i]::TEXT,
                       answer_text_dynamic = _arr_answers_dynamic[i]::TEXT
                    --penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                WHERE id_question = _id
                AND ordinal = i;
            END LOOP;
            UPDATE connected_elements_question
               SET id_connected_elements_correctness_model = _id_connected_elements_correctness_model
             WHERE id_question = _id;
        ELSE
            FOR i in array_lower(_arr_answers, 1)..array_upper(_arr_answers, 1) loop
                UPDATE question_answer
                   SET answer_text       = _arr_answers[i]::TEXT,
                       threshold_weight  = _arr_threshold_weights[i]::SMALLINT,
                       penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                       is_correct        = _arr_correct_answers[i]::BOOLEAN
                 WHERE id_question = _id
                   AND ordinal = i;
            END LOOP;
        END IF;
        IF (_id_question_type = 8) THEN
            UPDATE ordered_element_question
               SET id_ordered_element_correctness_model = _id_ordered_element_correctness_model,
                   display_option = _display_option
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL (2) and C
        IF (_id_question_type = 2) THEN
            UPDATE sql_question_answer
            SET sql_answer = _sql_answer,
                sql_alt_assertion = _sql_alt_assertion,
                sql_test_fixture = _sql_test_fixture,
                sql_alt_presentation_query = _sql_alt_presentation_query,
                check_tuple_order = _check_tuple_order,
                id_check_column_mode = _id_check_column_mode
            WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM update_c_question(
                _id, _c_prefix, _c_suffix, _c_source, _c_test_type_ids,
                _c_percentages, _c_allow_diff_orders, _c_allow_diff_letter_sizes,
                _c_trim_whitespaces, _c_comments, _c_regexs, _c_question_test_ids,
                _c_inputs, _c_outputs, _c_generator_test_file_ids,
                _c_argumentss, _c_random_test_type_ids, _c_low_bounds,
                _c_upper_bounds, _c_elem_counts, _c_programming_language_id, _c_rtc_question,
                                                        _c_rtc_test, _c_is_public);
        ELSIF (_id_question_type = 11) THEN  -- Diagrams
            UPDATE diagram_question_answer
                SET initial_diagram_answer = _initial_diagram_answer,
                    diagram_answer = _diagram_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            UPDATE text_question_answer
                SET text_answer = _text_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 25) THEN
                -- it's ok, nothing more to do here...
        ELSIF (_id_question_type = 21) THEN
                -- Mongo, JSON
                UPDATE json_question_answer
                SET json_answer = _json_answer,
                    json_alt_assertion = _json_alt_assertion,
                    json_test_fixture = _json_test_fixture,
                    json_alt_presentation_query = _json_alt_presentation_query,
                    assert_deep = _assert_deep,
                    assert_strict = _assert_strict
                WHERE id_question = _id;
        ELSIF (_id_question_type = 26) THEN
                -- Scale
                DELETE FROM question_scale WHERE id_question = _id;
                INSERT INTO question_scale (id_question, id_scale) VALUES (_id, _id_scale);
        ELSE
                RAISE EXCEPTION 'Unknown question type';
        END IF;
    END IF;

  PERFORM set_question_tags(_id, _tags);
  RETURN _id;
END;
$$;


ALTER FUNCTION public.save_in_place_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _initial_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer, _bump_version boolean) OWNER TO postgres;

--
-- Name: save_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.save_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _inital_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    i INTEGER;
    _new_id INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    -- C stuff:
    _id_question_type INTEGER;
    -- /C stuff

BEGIN
    SELECT  has_answers,  id_user_created,  id_question_type
      INTO _has_answers, _id_user_created, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
    WHERE question.id = _id;

    IF (SELECT COUNT(*) FROM test_instance_question WHERE id_question = _id) = 0 THEN
    -- Virgin, update in place:
        RETURN public.save_in_place_question(
               _id,
               _is_active,
               _upload_files_no,
               _required_upload_files_no,
               _question_text,
               _display_option,
               _time_limit,
               _question_comment,
               _text_answer,
               _inital_diagram_answer,
               _diagram_answer,
               _correct_answers,
               _answers_texts,
               _answer_text_static,
               _answer_text_dynamic,
               _threshold_weights,
               _penalty_percentages,
               _sql_answer,
               _sql_alt_assertion,
               _sql_test_fixture,
               _sql_alt_presentation_query,
               _check_tuple_order,
               _id_check_column_mode,
               _c_prefix,
               _c_suffix,
               _c_source,
               _c_test_type_ids,
               _c_percentages,
               _c_allow_diff_orders,
               _c_allow_diff_letter_sizes,
               _c_trim_whitespaces,
               _c_comments,
               _c_regexs,
               _c_question_test_ids,
               _c_inputs,
               _c_outputs,
               _c_generator_test_file_ids,
               _c_argumentss,
               _c_random_test_type_ids,
               _c_low_bounds,
               _c_upper_bounds,
               _c_elem_counts,
               _id_user_modified,
               _tags,
               _json_answer,
               _json_alt_assertion,
               _json_test_fixture,
               _json_alt_presentation_query,
               _assert_deep,
               _assert_strict,

               _c_rtc_question,
               _c_rtc_test,
               _c_programming_language_id,
               _c_is_public,
               _id_scale,
               _data_object,
               _eval_script,
               _id_script_programming_language,
               _grader_object,
               _id_ordered_element_correctness_model,
               _id_connected_elements_correctness_model,
               FALSE
        );
    ELSE
    -- Referenced question, close it (active) and leave content (keep history) and instantiate new question:
        UPDATE question
        SET is_active = false,
            id_user_modified = _id_user_modified
        WHERE id = _id;

        SELECT public.clone_question(_id, _id_user_created, _id_user_modified, true) INTO _new_id;
        -- Update new (virgin) question:
        RETURN public.save_question(
            _new_id,
            _is_active,
            _upload_files_no,
            _required_upload_files_no,
            _question_text,
            _display_option,
            _time_limit,
            _question_comment,
            _text_answer,
            _inital_diagram_answer,
            _diagram_answer,
            _correct_answers, --CSVs
            _answers_texts,  -- CSVs
            _answer_text_static,
            _answer_text_dynamic,
            _threshold_weights,  -- CSVs
            _penalty_percentages, -- CSVs
            _sql_answer,
            _sql_alt_assertion,
            _sql_test_fixture,
            _sql_alt_presentation_query,
            _check_tuple_order,
            _id_check_column_mode,
            _c_prefix,
            _c_suffix,
            _c_source,
            _c_test_type_ids,
            _c_percentages,
            _c_allow_diff_orders,
            _c_allow_diff_letter_sizes,
            _c_trim_whitespaces,
            _c_comments,
            _c_regexs,
            --// TODO: this sucks, bcs there is no "ordinal" attribute in the c_qustion_test, I have to go out on a lim with order by ID
            (SELECT array_to_string(array(
              SELECT id
                FROM c_question_test
              WHERE c_question_answer_id = (SELECT id FROM c_question_answer WHERE id_question = _new_id)
              ORDER BY id), '~~#~~')),
            _c_inputs,
            _c_outputs,
            _c_generator_test_file_ids,
            _c_argumentss,
            _c_random_test_type_ids,
            _c_low_bounds,
            _c_upper_bounds,
            _c_elem_counts,
            _id_user_modified,
            _tags,
            _json_answer,
            _json_alt_assertion,
            _json_test_fixture,
            _json_alt_presentation_query,
            _assert_deep,
            _assert_strict,
            _c_rtc_question,
            _c_rtc_test,
            _c_programming_language_id,
            _c_is_public,
            _id_scale,
            _data_object,
            _eval_script,
            _id_script_programming_language,
            _grader_object,
            _id_ordered_element_correctness_model,
            _id_connected_elements_correctness_model
        );
    END IF;

END;
$$;


ALTER FUNCTION public.save_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _inital_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer) OWNER TO postgres;

--
-- Name: set_question_tags(integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_question_tags(_id integer, _tags text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  _arr_tagids int[];
  i INTEGER;
BEGIN

  DELETE FROM question_course_tag WHERE id_question = _id;
  _arr_tagids = string_to_array(_tags, '~~#~~');
  IF (_tags IS NOT NULL AND LENGTH(_tags) > 0) THEN
    FOR i in array_lower(_arr_tagids, 1)..array_upper(_arr_tagids, 1) loop
      INSERT INTO question_course_tag(id_question, id_course_tag) VALUES (_id, _arr_tagids[i]);
    END LOOP;
  END IF;
  RETURN i;
END;
$$;


ALTER FUNCTION public.set_question_tags(_id integer, _tags text) OWNER TO postgres;

--
-- Name: set_runtime_constraints(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.set_runtime_constraints(c_rtc_question text, c_rtc_tests text) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    _cursor_rtc_question CURSOR FOR SELECT * FROM json_to_recordset(c_rtc_question::JSON) as x(id_question int, id_language int, id_rtc int, rtc_value text);
    _cursor_rtc_tests CURSOR FOR SELECT * FROM json_to_recordset(c_rtc_tests::JSON) as y(id_test int, id_language int, id_rtc int, rtc_value text);
BEGIN

    FOR item IN _cursor_rtc_question LOOP
        DELETE FROM runtime_constraint_question WHERE id_question = item.id_question AND id_programming_language = item.id_language AND id_runtime_constraint = item.id_rtc;

        IF (item.rtc_value = '') IS NOT TRUE THEN
            INSERT INTO runtime_constraint_question (id_question, id_programming_language, id_runtime_constraint, override_value)
                VALUES (item.id_question, item.id_language, item.id_rtc, item.rtc_value);
        END IF;
    END LOOP;

    FOR item IN _cursor_rtc_tests LOOP
        DELETE FROM runtime_constraint_test WHERE id_c_question_test = item.id_test AND id_programming_language = item.id_language AND id_runtime_constraint = item.id_rtc;

        IF (item.rtc_value = '') IS NOT TRUE THEN
            INSERT INTO runtime_constraint_test (id_c_question_test, id_programming_language, id_runtime_constraint, override_value)
                VALUES (item.id_test, item.id_language, item.id_rtc, item.rtc_value);
        END if;
    END LOOP;
END;
$$;


ALTER FUNCTION public.set_runtime_constraints(c_rtc_question text, c_rtc_tests text) OWNER TO postgres;

--
-- Name: start_exercise(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.start_exercise(_id_exercise integer, _id_student integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_is_started boolean := false;
	_row record;
	_answers_no integer;
  _difficulty_level_id integer;
	_rv integer;
BEGIN
	SELECT * INTO _row
        FROM exercise_student
        WHERE id_exercise = _id_exercise
            AND id_student = _id_student;

	IF _row IS NULL THEN
    INSERT INTO public.exercise_student (id_exercise, id_student)
      VALUES (_id_exercise, _id_student)
      RETURNING id INTO _rv;
  END IF;

  SELECT * INTO _row
  FROM exercise_student_question
  WHERE id_exercise = _id_exercise
    AND id_student = _id_student
  ORDER BY ordinal ASC
  LIMIT 1;

  -- add first question (with the lowest difficulty level) if not present
  IF _row IS NULL THEN

    SELECT qdl.id INTO _difficulty_level_id
    FROM question_difficulty_level qdl JOIN adaptivity_model am
      ON qdl.id_adaptivity_model = am.id
    JOIN exercise
      ON exercise.id_adaptivity_model = am.id
    WHERE exercise.id = _id_exercise
    ORDER BY ordinal ASC
    LIMIT 1;

    _rv = exercise_student_assign_new_question(_id_exercise, _id_student, _difficulty_level_id);
	ELSE
		SELECT id INTO _rv
		FROM public.exercise_student
		WHERE id_exercise = _id_exercise
			AND id_student = _id_student;
	END IF;

	RETURN _rv;
END
$$;


ALTER FUNCTION public.start_exercise(_id_exercise integer, _id_student integer) OWNER TO postgres;

--
-- Name: start_tutorial(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.start_tutorial(_id_course integer, _id_student integer, _id_tutorial integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	_is_started boolean := false;
	_row record;
	_step record;
	_answers_no integer;
	_answers_permutation integer[];
	_rv integer;
BEGIN
	SELECT * INTO _row
        FROM tutorial_student
        WHERE id_course = _id_course
            AND id_student = _id_student
            AND id_tutorial = _id_tutorial;

	IF _row IS NULL THEN
		INSERT INTO public.tutorial_student (id_course, id_student, id_tutorial, is_finished)
			VALUES (_id_course, _id_student, _id_tutorial, false)
			RETURNING id INTO _rv;

		-- insert question answer permutations
		FOR _step IN
		  SELECT id_question
		  FROM tutorial_step
		  WHERE id_tutorial = _id_tutorial
        AND id_question IS NOT NULL
		LOOP
		  SELECT COUNT(*) INTO _answers_no
		  FROM question_answer
		  WHERE id_question = _step.id_question;

		  _answers_permutation = gen_answers_ordinal_permutation(_answers_no);

		  INSERT INTO public.tutorial_student_question(
		    id_course, id_tutorial, id_student, id_question, answers_permutation)
		    VALUES (_id_course, _id_tutorial, _id_student, _step.id_question, _answers_permutation);

		END LOOP;
	ELSE
		SELECT id INTO _rv
		FROM public.tutorial_student
		WHERE id_course = _id_course
			AND id_student = _id_student
			AND id_tutorial = _id_tutorial;
	END IF;

	RETURN _rv;
END
$$;


ALTER FUNCTION public.start_tutorial(_id_course integer, _id_student integer, _id_tutorial integer) OWNER TO postgres;

--
-- Name: ticket_reminder_for_students(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ticket_reminder_for_students() RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE _ticket RECORD;
BEGIN
    FOR _ticket IN
        SELECT DISTINCT test.title, ticket_message.ts_created,  -- distinct bcs max_runs>1
               CURRENT_TIMESTAMP- ticket_message.ts_created as diff,
               email, ticket_message.ordinal
         FROM ticket_message
         JOIN test_instance
           ON ticket_message.id_test = test_instance.id_test
          AND ticket_message.id_student = test_instance.id_student
         JOIN test
           ON test_instance.id_test = test.id
         JOIN student
           ON ticket_message.id_student = student.id
        WHERE  ts_sent is null
          AND (CURRENT_TIMESTAMP- ticket_message.ts_created) between '10 minutes'::interval and '20 minutes'::interval
        ORDER by diff
    LOOP
        INSERT INTO email_queue (
                mail_to,
                mail_cc,
                mail_bcc,
                mail_from,
                mail_subject,
                mail_text,
                ---------
                ts_created,
                ts_modified,
                user_modified
                ) VALUES (
                _ticket.email,
                '',
                'igor.mekterovic@fer.hr',
                'edgar@fer.hr',
                'Ticket closed!',
$$Dear user,

Your ticket has been closed:

  * Exam name: $$ || _ticket.title || $$
  * Question ordinal: $$ || _ticket.ordinal || $$
  * Ticked closed before: $$ || _ticket.diff || $$

Please, do not reply to this message.

Best,

Edgar$$,
                ---------
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP,
                'Mailman'
                );
    END LOOP;
    RETURN 0;
END;
$_$;


ALTER FUNCTION public.ticket_reminder_for_students() OWNER TO postgres;

--
-- Name: translate_substring(integer, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.translate_substring(qid integer, old_substring text, new_substring text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	UPDATE c_question_answer
		SET c_source = REPLACE(c_source, old_substring, new_substring),
		    c_prefix = REPLACE(c_prefix, old_substring, new_substring),
		    c_suffix = REPLACE(c_suffix, old_substring, new_substring)
		WHERE id_question = qid;

	UPDATE fixed_test
		SET output = REPLACE(output, old_substring, new_substring)
		FROM c_question_test AS CQT
		INNER JOIN c_question_answer AS CQA ON CQA.id = CQT.c_question_answer_id
		WHERE id_question = qid AND CQT.id = fixed_test.c_question_test_id;

	old_substring := REPLACE(old_substring, ' ', '·');
	new_substring := REPLACE(new_substring, ' ', '·');

	UPDATE question SET question_text = REPLACE(question_text, old_substring, new_substring ) WHERE id = qid;

END
$$;


ALTER FUNCTION public.translate_substring(qid integer, old_substring text, new_substring text) OWNER TO postgres;

--
-- Name: tutorials_course_root_node_id(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.tutorials_course_root_node_id(_id_course integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    _id_root_node integer;
BEGIN
    -- Check if the root tutorials node is present for the course
    SELECT id_child INTO _id_root_node
    FROM v_roots_children JOIN node
        ON v_roots_children.id_child = node.id
        JOIN node_type
        ON node.id_node_type = node_type.id
    WHERE type_name='Tutorial'
        AND id_parent = (SELECT id_root_node FROM course WHERE id = _id_course);

  IF _id_root_node IS NULL THEN
        INSERT INTO public.node(
            id_node_type, node_name, description)
            VALUES ((SELECT id FROM node_type WHERE type_name = 'Tutorial'),
            'Tutorials', 'Parent node for all questions used in tutorials')
            RETURNING id INTO _id_root_node;
        -- Assign course root node as a parent to the created tutorials root node
        INSERT INTO public.node_parent(
                        id_child, id_parent)
                        VALUES (_id_root_node, _id_course);
    END IF;

    RETURN _id_root_node;
END
$$;


ALTER FUNCTION public.tutorials_course_root_node_id(_id_course integer) OWNER TO postgres;

--
-- Name: undo_correction(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.undo_correction(_id_test_instance_question integer) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    IF (1 = (SELECT COUNT(*) FROM test_instance_question where id = _id_test_instance_question AND is_partial)) THEN
        RAISE EXCEPTION 'Cannot undo PARTIALLY successful question bcs I can not determine the score from the grading model. Do it by hand (using score delta).';
    END IF;
 -- Update on the question level
    UPDATE test_instance_question AS TIQ
    SET score = CASE
            WHEN TIQ.is_incorrect = true THEN GM.incorrect_score
            WHEN TIQ.is_unanswered = true THEN GM.unanswered_score
            WHEN TIQ.is_correct = true THEN GM.correct_score
        END,
        score_perc = CASE
            WHEN TIQ.is_incorrect = true THEN GM.incorrect_score / GM.correct_score
            WHEN TIQ.is_unanswered = true THEN GM.unanswered_score / GM.correct_score
            WHEN TIQ.is_correct = true THEN 1
        END
    FROM grading_model AS GM
    WHERE TIQ.id_grading_model = GM.id
      AND TIQ.id = _id_test_instance_question;

    -- And on the test level, first score
    UPDATE test_instance
       SET score = (SELECT SUM(score)
                   FROM test_instance_question
                   WHERE id_test_instance = test_instance.id)
     WHERE id IN
        (SELECT id_test_instance
           FROM test_instance_question
          WHERE id = _id_test_instance_question);

    -- ... and then score perc:
    UPDATE test_instance
       SET score_perc = score / test.max_score
      FROM test
     WHERE test_instance.id_test = test.id
       AND test_instance.id IN
        (SELECT id_test_instance
         FROM test_instance_question
         WHERE id = _id_test_instance_question);

 -- delete from correction
    DELETE FROM test_correction
     WHERE id_test_instance_question = _id_test_instance_question;

    RETURN 0;
END;
$$;


ALTER FUNCTION public.undo_correction(_id_test_instance_question integer) OWNER TO postgres;

--
-- Name: update_c_question(integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_c_question(_id integer, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _c_programming_language_id text, _c_rtc_question text, _c_rtc_test text, _c_is_public text) RETURNS integer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
    -- C stuff:
    _id_question_type INTEGER;
    _c_question_answer_id INTEGER;
    _arr_c_question_test_ids integer[];
    _arr_c_percentages numeric[];
    _arr_c_allow_diff_orders boolean[];
    _arr_c_allow_diff_letter_sizes boolean[];
    _arr_c_trim_whitespaces boolean[];
    _arr_c_comments text[];
    _arr_c_regexs text[];
    _arr_c_test_type_ids integer[];
    _arr_c_inputs text[];
    _arr_c_outputs text[];
    _arr_c_generator_test_file_ids integer[];
    _arr_c_argumentss text[];
    _arr_c_random_test_type_ids integer[];
    _arr_c_low_bounds numeric[];
    _arr_c_upper_bounds numeric[];
    _arr_c_elem_counts integer[];
    old_test_type_id integer;
    _arr_c_is_public boolean[];
    -- /C stuff

BEGIN
    -- C stuff
    UPDATE c_question_answer
        SET c_prefix = _c_prefix,
            c_suffix = _c_suffix,
            c_source = _c_source,
            id_programming_language = _c_programming_language_id::INTEGER
    WHERE id_question = _id;

    SELECT id INTO _c_question_answer_id
        FROM c_question_answer
    WHERE c_question_answer.id_question = _id;

    _arr_c_question_test_ids = string_to_array(_c_question_test_ids, '~~#~~');
    _arr_c_percentages = emptyArrayElem2Zero(string_to_array(_c_percentages, '~~#~~'));
    _arr_c_allow_diff_orders = string_to_array(_c_allow_diff_orders, '~~#~~');
    _arr_c_allow_diff_letter_sizes = string_to_array(_c_allow_diff_letter_sizes, '~~#~~');
    _arr_c_trim_whitespaces = string_to_array(_c_trim_whitespaces, '~~#~~');
    _arr_c_comments = string_to_array(_c_comments, '~~#~~');
    _arr_c_regexs = string_to_array(_c_regexs, '~~#~~');
    _arr_c_test_type_ids = string_to_array(_c_test_type_ids, '~~#~~');
    _arr_c_inputs = string_to_array(_c_inputs, '~~#~~');
    _arr_c_outputs  = string_to_array(_c_outputs, '~~#~~');
    _arr_c_generator_test_file_ids  = emptyArrayElem2Zero(string_to_array(_c_generator_test_file_ids, '~~#~~'));
    _arr_c_argumentss  = string_to_array(_c_argumentss, '~~#~~');
    _arr_c_random_test_type_ids  = emptyArrayElem2Zero(string_to_array(_c_random_test_type_ids, '~~#~~'));
    _arr_c_low_bounds  = emptyArrayElem2Zero(string_to_array(_c_low_bounds, '~~#~~'));
    _arr_c_upper_bounds  = emptyArrayElem2Zero(string_to_array(_c_upper_bounds, '~~#~~'));
    _arr_c_elem_counts  = emptyArrayElem2Zero(string_to_array(_c_elem_counts, '~~#~~'));
    _arr_c_is_public = string_to_array(_c_is_public, '~~#~~');

    FOR i in 1..array_length(_arr_c_question_test_ids, 1) LOOP
        SELECT c_test_type_id INTO old_test_type_id
            FROM c_question_test
        WHERE id = _arr_c_question_test_ids[i]::INTEGER ;

        IF old_test_type_id = _arr_c_test_type_ids[i]::INTEGER THEN

            CASE
            WHEN (_arr_c_test_type_ids[i]::INTEGER = 1) THEN
                UPDATE fixed_test
                    SET input =  _arr_c_inputs[i]::TEXT ,
                        output = _arr_c_outputs[i]::TEXT
                    WHERE c_question_test_id =  _arr_c_question_test_ids[i]::INTEGER;

            WHEN (_arr_c_test_type_ids[i]::INTEGER = 2) THEN
                UPDATE random_test
                    SET random_test_type_id = _arr_c_random_test_type_ids[i]::INTEGER,
                        low_bound =  _arr_c_low_bounds[i]::NUMERIC(13,3),
                        upper_bound = _arr_c_upper_bounds[i]::NUMERIC(13,3),
                        elem_count =  _arr_c_elem_counts[i]::INTEGER
                    WHERE c_question_test_id =  _arr_c_question_test_ids[i]::INTEGER;

            WHEN (_arr_c_test_type_ids[i]::INTEGER = 3) THEN
                UPDATE random_test
                    SET  generator_test_file_id = _arr_c_generator_test_file_ids[i]::INTEGER,
                            arguments = _arr_c_argumentss[i]::TEXT
                    WHERE c_question_test_id =  _arr_c_question_test_ids[i]::INTEGER;
            ELSE
                RAISE EXCEPTION '_arr_c_test_type_ids[i] is not a valid id ';
            END CASE;
        ELSE
            CASE
            WHEN (old_test_type_id = 1) THEN
                DELETE FROM fixed_test WHERE c_question_test_id = _arr_c_question_test_ids[i]::INTEGER ;
            WHEN (old_test_type_id = 2) THEN
                DELETE FROM random_test WHERE c_question_test_id = _arr_c_question_test_ids[i]::INTEGER ;
            WHEN (old_test_type_id = 3) THEN
                DELETE FROM generator_test WHERE c_question_test_id = _arr_c_question_test_ids[i]::INTEGER ;
            END CASE;
            CASE
            WHEN (_arr_c_test_type_ids[i]::INTEGER = 1) THEN
                INSERT INTO fixed_test (c_question_test_id , input , output  )
                VALUES (_arr_c_question_test_ids[i]::INTEGER  , _arr_c_inputs[i]::TEXT , _arr_c_outputs[i]::TEXT ) ;
            WHEN (_arr_c_test_type_ids[i]::INTEGER = 2) THEN
                INSERT INTO random_test (c_question_test_id , random_test_type_id , low_bound , upper_bound , elem_count  )
                VALUES (_arr_c_question_test_ids[i]::INTEGER  , _arr_c_random_test_type_ids[i]::INTEGER ,
                    _arr_c_low_bounds[i]::NUMERIC(13,3) , _arr_c_upper_bounds[i]::NUMERIC(13,3) ,
                    _arr_c_elem_counts[i]::INTEGER  );
            WHEN (_arr_c_test_type_ids[i]::INTEGER = 3) THEN
                INSERT INTO generator_test (c_question_test_id , generator_test_file_id , arguments  )
                VALUES (_arr_c_question_test_ids[i]::INTEGER ,
                    _arr_c_generator_test_file_ids[i]::INTEGER , _arr_c_argumentss[i]::TEXT  ) ;
            ELSE
                RAISE EXCEPTION '_arr_c_test_type_ids[i] is not a valid id  ';
            END CASE;
        END IF;

        UPDATE c_question_test
            SET c_test_type_id = _arr_c_test_type_ids[i]::INTEGER,
                percentage = _arr_c_percentages[i]::NUMERIC(6,2),
                allow_diff_order = _arr_c_allow_diff_orders[i]::BOOLEAN,
                allow_diff_letter_size = _arr_c_allow_diff_letter_sizes[i]::BOOLEAN,
                trim_whitespace = _arr_c_trim_whitespaces[i]::BOOLEAN,
                comment = _arr_c_comments[i]::TEXT,
                regex_override = _arr_c_regexs[i]::TEXT,
                is_public = _arr_c_is_public[i]::BOOLEAN
        WHERE  id = _arr_c_question_test_ids[i]::INTEGER;
    END LOOP;
    PERFORM set_runtime_constraints(_c_rtc_question, _c_rtc_test);
    RETURN _id;
END;
$$;


ALTER FUNCTION public.update_c_question(_id integer, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _c_programming_language_id text, _c_rtc_question text, _c_rtc_test text, _c_is_public text) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: logged_actions; Type: TABLE; Schema: audit; Owner: postgres
--

CREATE TABLE audit.logged_actions (
    event_id bigint NOT NULL,
    schema_name text NOT NULL,
    table_name text NOT NULL,
    relid oid NOT NULL,
    session_user_name text,
    action_tstamp_tx timestamp with time zone NOT NULL,
    action_tstamp_stm timestamp with time zone NOT NULL,
    action_tstamp_clk timestamp with time zone NOT NULL,
    transaction_id bigint,
    application_name text,
    client_addr inet,
    client_port integer,
    client_query text,
    action text NOT NULL,
    row_data public.hstore,
    changed_fields public.hstore,
    statement_only boolean NOT NULL,
    CONSTRAINT logged_actions_action_check CHECK ((action = ANY (ARRAY['I'::text, 'D'::text, 'U'::text, 'T'::text])))
);


ALTER TABLE audit.logged_actions OWNER TO postgres;

--
-- Name: TABLE logged_actions; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON TABLE audit.logged_actions IS 'History of auditable actions on audited tables, from audit.if_modified_func()';


--
-- Name: COLUMN logged_actions.event_id; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.event_id IS 'Unique identifier for each auditable event';


--
-- Name: COLUMN logged_actions.schema_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.schema_name IS 'Database schema audited table for this event is in';


--
-- Name: COLUMN logged_actions.table_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.table_name IS 'Non-schema-qualified table name of table event occured in';


--
-- Name: COLUMN logged_actions.relid; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.relid IS 'Table OID. Changes with drop/create. Get with ''tablename''::regclass';


--
-- Name: COLUMN logged_actions.session_user_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.session_user_name IS 'Login / session user whose statement caused the audited event';


--
-- Name: COLUMN logged_actions.action_tstamp_tx; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_tx IS 'Transaction start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_stm; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_stm IS 'Statement start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_clk; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_clk IS 'Wall clock time at which audited event''s trigger call occurred';


--
-- Name: COLUMN logged_actions.transaction_id; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.transaction_id IS 'Identifier of transaction that made the change. May wrap, but unique paired with action_tstamp_tx.';


--
-- Name: COLUMN logged_actions.application_name; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.application_name IS 'Application name set when this audit event occurred. Can be changed in-session by client.';


--
-- Name: COLUMN logged_actions.client_addr; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.client_addr IS 'IP address of client that issued query. Null for unix domain socket.';


--
-- Name: COLUMN logged_actions.client_port; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.client_port IS 'Remote peer IP port address of client that issued query. Undefined for unix socket.';


--
-- Name: COLUMN logged_actions.client_query; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.client_query IS 'Top-level query that caused this auditable event. May be more than one statement.';


--
-- Name: COLUMN logged_actions.action; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.action IS 'Action type; I = insert, D = delete, U = update, T = truncate';


--
-- Name: COLUMN logged_actions.row_data; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.row_data IS 'Record value. Null for statement-level trigger. For INSERT this is the new tuple. For DELETE and UPDATE it is the old tuple.';


--
-- Name: COLUMN logged_actions.changed_fields; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.changed_fields IS 'New values of fields changed by UPDATE. Null except for row-level UPDATE events.';


--
-- Name: COLUMN logged_actions.statement_only; Type: COMMENT; Schema: audit; Owner: postgres
--

COMMENT ON COLUMN audit.logged_actions.statement_only IS '''t'' if audit event is from an FOR EACH STATEMENT trigger, ''f'' for FOR EACH ROW';


--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE; Schema: audit; Owner: postgres
--

CREATE SEQUENCE audit.logged_actions_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.logged_actions_event_id_seq OWNER TO postgres;

--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: postgres
--

ALTER SEQUENCE audit.logged_actions_event_id_seq OWNED BY audit.logged_actions.event_id;


--
-- Name: academic_year; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.academic_year (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    date_start date,
    date_end date,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.academic_year OWNER TO postgres;

--
-- Name: academic_year_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.academic_year_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.academic_year_id_seq OWNER TO postgres;

--
-- Name: academic_year_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.academic_year_id_seq OWNED BY public.academic_year.id;


--
-- Name: adaptivity_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.adaptivity_model (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.adaptivity_model OWNER TO postgres;

--
-- Name: adaptivity_model_behaviour; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.adaptivity_model_behaviour (
    id integer NOT NULL,
    id_adaptivity_model integer NOT NULL,
    id_question_difficulty_level integer NOT NULL,
    level_up_questions_to_track smallint NOT NULL,
    level_up_correct_questions smallint NOT NULL,
    level_down_questions_to_track smallint NOT NULL,
    level_down_incorrect_questions smallint NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.adaptivity_model_behaviour OWNER TO postgres;

--
-- Name: adaptivity_model_behaviour_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.adaptivity_model_behaviour ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.adaptivity_model_behaviour_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: adaptivity_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.adaptivity_model ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.adaptivity_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: answer_regex_trigger_template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answer_regex_trigger_template (
    id integer NOT NULL,
    ordinal integer NOT NULL,
    is_active boolean NOT NULL,
    title character varying(128) NOT NULL,
    description text,
    regex text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.answer_regex_trigger_template OWNER TO postgres;

--
-- Name: answer_regex_trigger_template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.answer_regex_trigger_template ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.answer_regex_trigger_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: app_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user (
    id integer NOT NULL,
    id_role integer NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    username character varying(50) NOT NULL,
    alt_id character varying(50),
    alt_id2 character varying(50),
    provider character varying(50) DEFAULT 'saml.aai'::character varying NOT NULL,
    google_translate_key text,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.app_user OWNER TO postgres;

--
-- Name: app_user_course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user_course (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_teacher integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.app_user_course OWNER TO postgres;

--
-- Name: app_user_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_user_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_course_id_seq OWNER TO postgres;

--
-- Name: app_user_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_user_course_id_seq OWNED BY public.app_user_course.id;


--
-- Name: app_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_id_seq OWNER TO postgres;

--
-- Name: app_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_user_id_seq OWNED BY public.app_user.id;


--
-- Name: app_user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_user_role (
    id integer NOT NULL,
    id_app_user integer,
    id_role integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.app_user_role OWNER TO postgres;

--
-- Name: app_user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_user_role_id_seq OWNER TO postgres;

--
-- Name: app_user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_user_role_id_seq OWNED BY public.app_user_role.id;


--
-- Name: c_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.c_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    c_prefix text,
    c_source text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    c_suffix text,
    id_programming_language integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.c_question_answer OWNER TO postgres;

--
-- Name: c_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.c_question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.c_question_answer_id_seq OWNER TO postgres;

--
-- Name: c_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.c_question_answer_id_seq OWNED BY public.c_question_answer.id;


--
-- Name: c_question_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.c_question_test (
    id integer NOT NULL,
    c_question_answer_id integer NOT NULL,
    c_test_type_id integer NOT NULL,
    percentage numeric(6,2) DEFAULT 100.00 NOT NULL,
    allow_diff_order boolean DEFAULT false NOT NULL,
    allow_diff_letter_size boolean DEFAULT true NOT NULL,
    allow_subset boolean DEFAULT false NOT NULL,
    comment text,
    regex_override text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    trim_whitespace boolean DEFAULT true NOT NULL,
    is_public boolean DEFAULT false NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.c_question_test OWNER TO postgres;

--
-- Name: c_question_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.c_question_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.c_question_test_id_seq OWNER TO postgres;

--
-- Name: c_question_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.c_question_test_id_seq OWNED BY public.c_question_test.id;


--
-- Name: c_test_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.c_test_type (
    id integer NOT NULL,
    test_type_name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.c_test_type OWNER TO postgres;

--
-- Name: check_column_mode; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.check_column_mode (
    id smallint NOT NULL,
    mode_desc character varying(100) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.check_column_mode OWNER TO postgres;

--
-- Name: code_runner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.code_runner (
    id integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    name character varying(100) NOT NULL,
    host character varying(500) NOT NULL,
    port integer NOT NULL,
    path character varying(500) NOT NULL,
    db_schema text,
    timeout integer,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.code_runner OWNER TO postgres;

--
-- Name: code_runner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.code_runner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.code_runner_id_seq OWNER TO postgres;

--
-- Name: code_runner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.code_runner_id_seq OWNED BY public.code_runner.id;


--
-- Name: code_snippet; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.code_snippet (
    id integer NOT NULL,
    title text NOT NULL,
    code text NOT NULL,
    id_code_runner integer NOT NULL,
    id_student integer NOT NULL,
    id_course integer NOT NULL,
    is_public boolean DEFAULT false NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.code_snippet OWNER TO postgres;

--
-- Name: code_snippet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.code_snippet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.code_snippet_id_seq OWNER TO postgres;

--
-- Name: code_snippet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.code_snippet_id_seq OWNED BY public.code_snippet.id;


--
-- Name: computer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.computer (
    ip character varying(15) NOT NULL,
    room character varying(20) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL,
    ts_created timestamp with time zone,
    ts_modified timestamp with time zone,
    user_modified character varying(50)
);


ALTER TABLE public.computer OWNER TO postgres;

--
-- Name: computer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.computer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.computer_id_seq OWNER TO postgres;

--
-- Name: computer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.computer_id_seq OWNED BY public.computer.id;


--
-- Name: connected_elements_correctness_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.connected_elements_correctness_model (
    id integer NOT NULL,
    model_name text NOT NULL,
    correctness integer[],
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.connected_elements_correctness_model OWNER TO postgres;

--
-- Name: connected_elements_correctness_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.connected_elements_correctness_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.connected_elements_correctness_model_id_seq OWNER TO postgres;

--
-- Name: connected_elements_correctness_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.connected_elements_correctness_model_id_seq OWNED BY public.connected_elements_correctness_model.id;


--
-- Name: connected_elements_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.connected_elements_question (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_connected_elements_correctness_model integer DEFAULT 1 NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.connected_elements_question OWNER TO postgres;

--
-- Name: connected_elements_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.connected_elements_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    ordinal smallint NOT NULL,
    answer_text_static text,
    answer_text_dynamic text NOT NULL,
    penalty_percentage numeric(6,2) DEFAULT '100'::numeric NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified time with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.connected_elements_question_answer OWNER TO postgres;

--
-- Name: connected_elements_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.connected_elements_question_answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.connected_elements_question_answer_id_seq OWNER TO postgres;

--
-- Name: connected_elements_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.connected_elements_question_answer_id_seq OWNED BY public.connected_elements_question_answer.id;


--
-- Name: connected_elements_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.connected_elements_question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.connected_elements_question_id_seq OWNER TO postgres;

--
-- Name: connected_elements_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.connected_elements_question_id_seq OWNED BY public.connected_elements_question.id;


--
-- Name: course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course (
    id integer NOT NULL,
    course_name character varying(128) NOT NULL,
    course_desc text,
    ects_credits numeric(5,1),
    course_acronym character varying(15) NOT NULL,
    course_url character varying(128),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_root_node integer,
    uses_mobile_app boolean DEFAULT false NOT NULL,
    show_event_log boolean DEFAULT true NOT NULL,
    show_codemirror boolean DEFAULT true NOT NULL,
    show_attachments boolean DEFAULT false NOT NULL,
    show_runtime_constraints boolean DEFAULT false NOT NULL,
    is_competitive boolean DEFAULT false NOT NULL,
    uses_ticketing boolean DEFAULT true NOT NULL,
    data_object text,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.course OWNER TO postgres;

--
-- Name: course_code_runner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course_code_runner (
    id_course integer NOT NULL,
    id_programming_language integer NOT NULL,
    id_code_runner integer NOT NULL,
    id_type integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.course_code_runner OWNER TO postgres;

--
-- Name: playground_code_runner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.playground_code_runner (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_code_runner integer NOT NULL,
    ordinal smallint NOT NULL,
    student_can_see boolean DEFAULT false NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.playground_code_runner OWNER TO postgres;

--
-- Name: course_code_runner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_code_runner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_code_runner_id_seq OWNER TO postgres;

--
-- Name: course_code_runner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_code_runner_id_seq OWNED BY public.playground_code_runner.id;


--
-- Name: course_code_runner_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_code_runner_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_code_runner_id_seq1 OWNER TO postgres;

--
-- Name: course_code_runner_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_code_runner_id_seq1 OWNED BY public.course_code_runner.id;


--
-- Name: course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_id_seq OWNER TO postgres;

--
-- Name: course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_id_seq OWNED BY public.course.id;


--
-- Name: course_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course_tag (
    id integer NOT NULL,
    id_course integer NOT NULL,
    tag text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.course_tag OWNER TO postgres;

--
-- Name: course_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_tag_id_seq OWNER TO postgres;

--
-- Name: course_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_tag_id_seq OWNED BY public.course_tag.id;


--
-- Name: data_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_type (
    id integer NOT NULL,
    type_name text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.data_type OWNER TO postgres;

--
-- Name: data_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_type_id_seq OWNER TO postgres;

--
-- Name: data_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_type_id_seq OWNED BY public.data_type.id;


--
-- Name: dendrogram_distance_algorithm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dendrogram_distance_algorithm (
    id integer NOT NULL,
    name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.dendrogram_distance_algorithm OWNER TO postgres;

--
-- Name: diagram_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.diagram_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    initial_diagram_answer text NOT NULL,
    diagram_answer text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.diagram_question_answer OWNER TO postgres;

--
-- Name: diagram_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.diagram_question_answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diagram_question_answer_id_seq OWNER TO postgres;

--
-- Name: diagram_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.diagram_question_answer_id_seq OWNED BY public.diagram_question_answer.id;


--
-- Name: email_handled_reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_handled_reminders (
    id integer NOT NULL,
    id_test integer NOT NULL,
    threshold real NOT NULL,
    reminder_type text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT chk_reminder_type CHECK ((reminder_type = ANY (ARRAY['ongoing'::text, 'not started'::text])))
);


ALTER TABLE public.email_handled_reminders OWNER TO postgres;

--
-- Name: email_handled_reminders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_handled_reminders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_handled_reminders_id_seq OWNER TO postgres;

--
-- Name: email_handled_reminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_handled_reminders_id_seq OWNED BY public.email_handled_reminders.id;


--
-- Name: email_queue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_queue (
    id integer NOT NULL,
    mail_to text NOT NULL,
    mail_cc text NOT NULL,
    mail_bcc text NOT NULL,
    mail_from text NOT NULL,
    mail_subject text NOT NULL,
    mail_text text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.email_queue OWNER TO postgres;

--
-- Name: email_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_queue_id_seq OWNER TO postgres;

--
-- Name: email_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_queue_id_seq OWNED BY public.email_queue.id;


--
-- Name: email_reminder_scheme; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_reminder_scheme (
    id integer NOT NULL,
    name text NOT NULL,
    thresholds_ongoing real[],
    thresholds_not_started real[],
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.email_reminder_scheme OWNER TO postgres;

--
-- Name: email_reminder_scheme_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_reminder_scheme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_reminder_scheme_id_seq OWNER TO postgres;

--
-- Name: email_reminder_scheme_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_reminder_scheme_id_seq OWNED BY public.email_reminder_scheme.id;


--
-- Name: exercise; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exercise (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_academic_year integer NOT NULL,
    id_adaptivity_model integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    title character varying(128) NOT NULL,
    description text,
    is_active boolean NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.exercise OWNER TO postgres;

--
-- Name: exercise_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.exercise ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.exercise_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: exercise_node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exercise_node (
    id integer NOT NULL,
    id_exercise integer NOT NULL,
    id_node integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.exercise_node OWNER TO postgres;

--
-- Name: exercise_node_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.exercise_node ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.exercise_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: exercise_student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exercise_student (
    id integer NOT NULL,
    id_exercise integer NOT NULL,
    id_student integer NOT NULL,
    is_finished boolean DEFAULT false NOT NULL,
    ts_finished timestamp with time zone,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.exercise_student OWNER TO postgres;

--
-- Name: exercise_student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.exercise_student ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.exercise_student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: exercise_student_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exercise_student_question (
    id integer NOT NULL,
    id_exercise integer NOT NULL,
    id_student integer NOT NULL,
    id_question integer NOT NULL,
    ordinal integer NOT NULL,
    answers_permutation integer[],
    student_answers integer[],
    student_answer_code text,
    is_correct boolean,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.exercise_student_question OWNER TO postgres;

--
-- Name: exercise_student_question_attempt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exercise_student_question_attempt (
    id integer NOT NULL,
    id_exercise integer NOT NULL,
    id_student integer NOT NULL,
    id_question integer NOT NULL,
    is_correct boolean NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.exercise_student_question_attempt OWNER TO postgres;

--
-- Name: exercise_student_question_attempt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.exercise_student_question_attempt ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.exercise_student_question_attempt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: exercise_student_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.exercise_student_question ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.exercise_student_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: feedback; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.feedback (
    id integer NOT NULL,
    id_academic_year integer NOT NULL,
    id_course integer NOT NULL,
    id_student integer NOT NULL,
    id_exercise integer,
    exercise_question_ordinal integer,
    id_tutorial integer,
    id_tutorial_step integer,
    id_question integer,
    comment text,
    rating real,
    max_rating real,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT feedback_rating_max_rating_present CHECK (((rating >= (0)::double precision) AND (rating <= max_rating) AND (((rating IS NOT NULL) AND (max_rating IS NOT NULL)) OR ((rating IS NULL) AND (max_rating IS NULL)))))
);


ALTER TABLE public.feedback OWNER TO postgres;

--
-- Name: feedback_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.feedback ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.feedback_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: fixed_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fixed_test (
    id integer NOT NULL,
    c_question_test_id integer NOT NULL,
    input text NOT NULL,
    output text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.fixed_test OWNER TO postgres;

--
-- Name: fixed_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fixed_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fixed_test_id_seq OWNER TO postgres;

--
-- Name: fixed_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fixed_test_id_seq OWNED BY public.fixed_test.id;


--
-- Name: generator_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.generator_test (
    id integer NOT NULL,
    c_question_test_id integer NOT NULL,
    generator_test_file_id integer NOT NULL,
    arguments text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.generator_test OWNER TO postgres;

--
-- Name: generator_test_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.generator_test_file (
    id integer NOT NULL,
    description text,
    file text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.generator_test_file OWNER TO postgres;

--
-- Name: generator_test_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.generator_test_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.generator_test_file_id_seq OWNER TO postgres;

--
-- Name: generator_test_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.generator_test_file_id_seq OWNED BY public.generator_test_file.id;


--
-- Name: generator_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.generator_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.generator_test_id_seq OWNER TO postgres;

--
-- Name: generator_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.generator_test_id_seq OWNED BY public.generator_test.id;


--
-- Name: global_data_object; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.global_data_object (
    id integer NOT NULL,
    data_object text DEFAULT '{}'::text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.global_data_object OWNER TO postgres;

--
-- Name: grading_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grading_model (
    id integer NOT NULL,
    model_name character varying(50) NOT NULL,
    correct_score numeric(10,3) NOT NULL,
    incorrect_score numeric(10,3) NOT NULL,
    unanswered_score numeric(10,3) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    threshold smallint DEFAULT 1 NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT abs_incorrect_score_less_than_correct CHECK ((abs(incorrect_score) <= correct_score)),
    CONSTRAINT correct_score_btw_0_and_100 CHECK (((correct_score >= (0)::numeric) AND (correct_score <= (100)::numeric))),
    CONSTRAINT threshold_between_0_and_50 CHECK (((threshold >= 0) AND (threshold <= 50))),
    CONSTRAINT unanswered_score_abs_less_than_correct CHECK ((abs(unanswered_score) <= correct_score))
);


ALTER TABLE public.grading_model OWNER TO postgres;

--
-- Name: grading_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grading_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grading_model_id_seq OWNER TO postgres;

--
-- Name: grading_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grading_model_id_seq OWNED BY public.grading_model.id;


--
-- Name: j0_unit_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.j0_unit_type (
    id integer NOT NULL,
    type_name text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.j0_unit_type OWNER TO postgres;

--
-- Name: j0_unit_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.j0_unit_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.j0_unit_type_id_seq OWNER TO postgres;

--
-- Name: j0_unit_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.j0_unit_type_id_seq OWNED BY public.j0_unit_type.id;


--
-- Name: json_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.json_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    json_answer text NOT NULL,
    json_alt_assertion text,
    json_test_fixture text,
    json_alt_presentation_query text,
    assert_deep boolean DEFAULT false NOT NULL,
    assert_strict boolean DEFAULT false NOT NULL,
    id_code_runner integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.json_question_answer OWNER TO postgres;

--
-- Name: json_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.json_question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.json_question_answer_id_seq OWNER TO postgres;

--
-- Name: json_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.json_question_answer_id_seq OWNED BY public.json_question_answer.id;


--
-- Name: local_provider; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.local_provider (
    id_student integer NOT NULL,
    username character varying(50),
    password character varying(70),
    id integer NOT NULL,
    user_created character varying(50) NOT NULL,
    ts_created timestamp with time zone,
    ts_modified timestamp with time zone,
    user_modified character varying(50)
);


ALTER TABLE public.local_provider OWNER TO postgres;

--
-- Name: local_provider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.local_provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.local_provider_id_seq OWNER TO postgres;

--
-- Name: local_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.local_provider_id_seq OWNED BY public.local_provider.id;


--
-- Name: node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node (
    id integer NOT NULL,
    id_node_type integer NOT NULL,
    node_name character varying(255) NOT NULL,
    description text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.node OWNER TO postgres;

--
-- Name: node_hierarchy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node_hierarchy (
    id integer NOT NULL,
    name character varying(50),
    description text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.node_hierarchy OWNER TO postgres;

--
-- Name: node_hierarchy_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_hierarchy_id_seq OWNER TO postgres;

--
-- Name: node_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_hierarchy_id_seq OWNED BY public.node_hierarchy.id;


--
-- Name: node_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_id_seq OWNER TO postgres;

--
-- Name: node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_id_seq OWNED BY public.node.id;


--
-- Name: node_parent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node_parent (
    id integer NOT NULL,
    id_child integer NOT NULL,
    id_parent integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT parent_is_different_from_child CHECK ((id_parent <> id_child))
);


ALTER TABLE public.node_parent OWNER TO postgres;

--
-- Name: node_parent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_parent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_parent_id_seq OWNER TO postgres;

--
-- Name: node_parent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_parent_id_seq OWNED BY public.node_parent.id;


--
-- Name: node_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node_type (
    id integer NOT NULL,
    type_name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    color text DEFAULT 'aliceblue'::text NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.node_type OWNER TO postgres;

--
-- Name: node_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_type_id_seq OWNER TO postgres;

--
-- Name: node_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_type_id_seq OWNED BY public.node_type.id;


--
-- Name: ordered_element_correctness_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ordered_element_correctness_model (
    id integer NOT NULL,
    model_name text NOT NULL,
    correctness integer[],
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.ordered_element_correctness_model OWNER TO postgres;

--
-- Name: ordered_element_correctness_model_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ordered_element_correctness_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ordered_element_correctness_model_id_seq OWNER TO postgres;

--
-- Name: ordered_element_correctness_model_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ordered_element_correctness_model_id_seq OWNED BY public.ordered_element_correctness_model.id;


--
-- Name: ordered_element_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ordered_element_question (
    id integer NOT NULL,
    id_question integer NOT NULL,
    display_option text DEFAULT 'horizontal'::text NOT NULL,
    id_ordered_element_correctness_model integer DEFAULT 1 NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT check_display_option_is_hor_or_vert CHECK ((display_option = ANY (ARRAY['horizontal'::text, 'vertical'::text])))
);


ALTER TABLE public.ordered_element_question OWNER TO postgres;

--
-- Name: ordered_element_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ordered_element_question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ordered_element_question_id_seq OWNER TO postgres;

--
-- Name: ordered_element_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ordered_element_question_id_seq OWNED BY public.ordered_element_question.id;


--
-- Name: pace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pace (
    id integer NOT NULL,
    pace_name text NOT NULL,
    timeouts integer[] NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.pace OWNER TO postgres;

--
-- Name: pace_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pace_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pace_id_seq OWNER TO postgres;

--
-- Name: pace_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pace_id_seq OWNED BY public.pace.id;


--
-- Name: peer_shuffle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_shuffle (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    id_test_phase_2 integer NOT NULL,
    jobs integer[],
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.peer_shuffle OWNER TO postgres;

--
-- Name: peer_shuffle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_shuffle_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_shuffle_id_seq OWNER TO postgres;

--
-- Name: peer_shuffle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_shuffle_id_seq OWNED BY public.peer_shuffle.id;


--
-- Name: peer_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_test (
    id integer NOT NULL,
    id_test_phase_1 integer NOT NULL,
    id_test_phase_2 integer NOT NULL,
    assessments_no smallint NOT NULL,
    calib_assessments_no smallint,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT calib_assessments_is_null_or_less_than_assessments_no CHECK (((calib_assessments_no IS NULL) OR (calib_assessments_no < assessments_no)))
);


ALTER TABLE public.peer_test OWNER TO postgres;

--
-- Name: peer_test_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_test_group (
    id integer NOT NULL,
    id_peer_test integer NOT NULL,
    id_question integer NOT NULL,
    id_calib_assessments_node integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.peer_test_group OWNER TO postgres;

--
-- Name: peer_test_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_test_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_test_group_id_seq OWNER TO postgres;

--
-- Name: peer_test_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_test_group_id_seq OWNED BY public.peer_test_group.id;


--
-- Name: peer_test_group_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_test_group_question (
    id integer NOT NULL,
    id_peer_test_group integer NOT NULL,
    ordinal smallint NOT NULL,
    id_question integer NOT NULL,
    id_grading_model integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.peer_test_group_question OWNER TO postgres;

--
-- Name: peer_test_group_question_calib; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_test_group_question_calib (
    id integer NOT NULL,
    id_peer_test_group_question integer NOT NULL,
    id_question_calib integer NOT NULL,
    id_scale_item integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.peer_test_group_question_calib OWNER TO postgres;

--
-- Name: peer_test_group_question_calib_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_test_group_question_calib_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_test_group_question_calib_id_seq OWNER TO postgres;

--
-- Name: peer_test_group_question_calib_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_test_group_question_calib_id_seq OWNED BY public.peer_test_group_question_calib.id;


--
-- Name: peer_test_group_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_test_group_question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_test_group_question_id_seq OWNER TO postgres;

--
-- Name: peer_test_group_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_test_group_question_id_seq OWNED BY public.peer_test_group_question.id;


--
-- Name: peer_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_test_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_test_id_seq OWNER TO postgres;

--
-- Name: peer_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_test_id_seq OWNED BY public.peer_test.id;


--
-- Name: perm_user_course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.perm_user_course (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_course integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.perm_user_course OWNER TO postgres;

--
-- Name: perm_user_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.perm_user_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perm_user_course_id_seq OWNER TO postgres;

--
-- Name: perm_user_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.perm_user_course_id_seq OWNED BY public.perm_user_course.id;


--
-- Name: plag_detection_algorithm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plag_detection_algorithm (
    id integer NOT NULL,
    name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.plag_detection_algorithm OWNER TO postgres;

--
-- Name: plag_detection_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plag_detection_data (
    id integer NOT NULL,
    id_plag_detection_algorithm integer NOT NULL,
    id_test integer NOT NULL,
    similarity_per_question text,
    data_per_question text,
    students_per_question text,
    questions text,
    color_per_question text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.plag_detection_data OWNER TO postgres;

--
-- Name: plag_detection_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.plag_detection_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plag_detection_data_id_seq OWNER TO postgres;

--
-- Name: plag_detection_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.plag_detection_data_id_seq OWNED BY public.plag_detection_data.id;


--
-- Name: programming_language; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.programming_language (
    id integer NOT NULL,
    name text NOT NULL,
    judge0_id smallint,
    hello_world text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    compiler_options text,
    extension character varying(10),
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.programming_language OWNER TO postgres;

--
-- Name: programming_language_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.programming_language_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programming_language_id_seq OWNER TO postgres;

--
-- Name: programming_language_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.programming_language_id_seq OWNED BY public.programming_language.id;


--
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question (
    id integer NOT NULL,
    version smallint NOT NULL,
    is_active boolean NOT NULL,
    id_prev_question integer,
    id_question_type integer NOT NULL,
    question_text text NOT NULL,
    id_user_created integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_user_modified integer NOT NULL,
    question_comment text,
    grader_object text,
    time_limit integer,
    user_created character varying(50) NOT NULL,
    upload_files_no smallint DEFAULT 0 NOT NULL,
    required_upload_files_no smallint DEFAULT 0 NOT NULL,
    CONSTRAINT chk_required_upload_files_no_less_than_upload_files_no CHECK (((required_upload_files_no >= 0) AND (required_upload_files_no <= upload_files_no))),
    CONSTRAINT chk_upload_files_no_beween_0_and_5 CHECK (((upload_files_no >= 0) AND (upload_files_no <= 5)))
);


ALTER TABLE public.question OWNER TO postgres;

--
-- Name: question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    ordinal smallint NOT NULL,
    is_correct boolean NOT NULL,
    answer_text text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    threshold_weight smallint DEFAULT 1 NOT NULL,
    penalty_percentage numeric(6,2) DEFAULT 100.00 NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT threshold_weight_between_0_and_10 CHECK (((threshold_weight >= 0) AND (threshold_weight <= 10)))
);


ALTER TABLE public.question_answer OWNER TO postgres;

--
-- Name: question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_answer_id_seq OWNER TO postgres;

--
-- Name: question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_answer_id_seq OWNED BY public.question_answer.id;


--
-- Name: question_assigned_difficulty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_assigned_difficulty (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_adaptivity_model integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    id_difficulty_level integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_assigned_difficulty OWNER TO postgres;

--
-- Name: question_assigned_difficulty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.question_assigned_difficulty ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.question_assigned_difficulty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: question_attachment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_attachment (
    id_question integer,
    original_name text NOT NULL,
    filename text NOT NULL,
    label text,
    is_public boolean DEFAULT false,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_attachment OWNER TO postgres;

--
-- Name: question_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_attachment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_attachment_id_seq OWNER TO postgres;

--
-- Name: question_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_attachment_id_seq OWNED BY public.question_attachment.id;


--
-- Name: question_computed_difficulty; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_computed_difficulty (
    id integer NOT NULL,
    id_question_difficulty_computation integer NOT NULL,
    id_question integer NOT NULL,
    difficulty real NOT NULL,
    difficulty_std_err real,
    discriminating_power real,
    discriminating_power_std_err real,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_computed_difficulty OWNER TO postgres;

--
-- Name: question_computed_difficulty_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.question_computed_difficulty ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.question_computed_difficulty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: question_course_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_course_tag (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_course_tag integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_course_tag OWNER TO postgres;

--
-- Name: question_course_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_course_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_course_tag_id_seq OWNER TO postgres;

--
-- Name: question_course_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_course_tag_id_seq OWNED BY public.question_course_tag.id;


--
-- Name: question_data_object; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_data_object (
    id integer NOT NULL,
    id_question integer NOT NULL,
    data_object text DEFAULT '
{
  init() {
      this.x = this.randomInt(13, 37);
  }
}'::text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_data_object OWNER TO postgres;

--
-- Name: question_data_object_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_data_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_data_object_id_seq OWNER TO postgres;

--
-- Name: question_data_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_data_object_id_seq OWNED BY public.question_data_object.id;


--
-- Name: question_difficulty_computation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_difficulty_computation (
    id integer NOT NULL,
    id_exercise integer NOT NULL,
    id_user_created integer,
    no_model_params integer NOT NULL,
    no_questions integer,
    ts_started timestamp with time zone NOT NULL,
    ts_finished timestamp with time zone,
    min_difficulty real,
    min_discriminating_power real,
    mean_difficulty real,
    mean_discriminating_power real,
    max_difficulty real,
    max_discriminating_power real,
    results_json text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_difficulty_computation OWNER TO postgres;

--
-- Name: question_difficulty_computation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.question_difficulty_computation ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.question_difficulty_computation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: question_difficulty_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_difficulty_level (
    id integer NOT NULL,
    id_adaptivity_model integer NOT NULL,
    name character varying(64) NOT NULL,
    ordinal integer NOT NULL,
    rgb_color character(6) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_difficulty_level OWNER TO postgres;

--
-- Name: question_difficulty_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.question_difficulty_level ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.question_difficulty_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: question_eval_script; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_eval_script (
    id integer NOT NULL,
    id_question integer NOT NULL,
    eval_script text DEFAULT '{
  getScore() {
    return this.score;
  }
}'::text NOT NULL,
    id_script_programming_language integer DEFAULT 1,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_eval_script OWNER TO postgres;

--
-- Name: question_eval_script_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_eval_script_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_eval_script_id_seq OWNER TO postgres;

--
-- Name: question_eval_script_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_eval_script_id_seq OWNED BY public.question_eval_script.id;


--
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_id_seq OWNER TO postgres;

--
-- Name: question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_id_seq OWNED BY public.question.id;


--
-- Name: question_node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_node (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_node integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_node OWNER TO postgres;

--
-- Name: question_node_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_node_id_seq OWNER TO postgres;

--
-- Name: question_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_node_id_seq OWNED BY public.question_node.id;


--
-- Name: question_programming_language; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_programming_language (
    id_question integer NOT NULL,
    id_programming_language integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_programming_language OWNER TO postgres;

--
-- Name: question_programming_language_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_programming_language_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_programming_language_id_seq OWNER TO postgres;

--
-- Name: question_programming_language_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_programming_language_id_seq OWNED BY public.question_programming_language.id;


--
-- Name: question_review; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_review (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_user_reviewed integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_review OWNER TO postgres;

--
-- Name: question_review_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_review_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_review_id_seq OWNER TO postgres;

--
-- Name: question_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_review_id_seq OWNED BY public.question_review.id;


--
-- Name: question_scale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_scale (
    id integer NOT NULL,
    id_question integer NOT NULL,
    id_scale integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_scale OWNER TO postgres;

--
-- Name: question_scale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_scale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_scale_id_seq OWNER TO postgres;

--
-- Name: question_scale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_scale_id_seq OWNED BY public.question_scale.id;


--
-- Name: question_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question_type (
    id integer NOT NULL,
    type_name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    has_answers boolean DEFAULT false NOT NULL,
    has_permutations boolean DEFAULT false NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.question_type OWNER TO postgres;

--
-- Name: random_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.random_test (
    id integer NOT NULL,
    c_question_test_id integer NOT NULL,
    random_test_type_id integer NOT NULL,
    low_bound numeric(13,3),
    upper_bound numeric(13,3),
    elem_count integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.random_test OWNER TO postgres;

--
-- Name: random_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.random_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.random_test_id_seq OWNER TO postgres;

--
-- Name: random_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.random_test_id_seq OWNED BY public.random_test.id;


--
-- Name: random_test_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.random_test_type (
    id integer NOT NULL,
    random_test_type_name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.random_test_type OWNER TO postgres;

--
-- Name: real_time_plag_detection_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.real_time_plag_detection_data (
    id integer NOT NULL,
    id_plag_detection_algorithm integer NOT NULL,
    id_test integer NOT NULL,
    id_question integer NOT NULL,
    similarities text,
    students text,
    data text,
    colors text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.real_time_plag_detection_data OWNER TO postgres;

--
-- Name: real_time_plag_detection_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.real_time_plag_detection_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.real_time_plag_detection_data_id_seq OWNER TO postgres;

--
-- Name: real_time_plag_detection_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.real_time_plag_detection_data_id_seq OWNED BY public.real_time_plag_detection_data.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id integer NOT NULL,
    role_name character varying(50) NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: runtime_constraint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.runtime_constraint (
    id integer NOT NULL,
    name text NOT NULL,
    default_value text NOT NULL,
    id_data_type integer NOT NULL,
    id_j0_unit_type integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.runtime_constraint OWNER TO postgres;

--
-- Name: runtime_constraint_course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.runtime_constraint_course (
    id_course integer NOT NULL,
    id_programming_language integer NOT NULL,
    id_runtime_constraint integer NOT NULL,
    override_value text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.runtime_constraint_course OWNER TO postgres;

--
-- Name: runtime_constraint_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.runtime_constraint_course_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runtime_constraint_course_id_seq OWNER TO postgres;

--
-- Name: runtime_constraint_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.runtime_constraint_course_id_seq OWNED BY public.runtime_constraint_course.id;


--
-- Name: runtime_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.runtime_constraint_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runtime_constraint_id_seq OWNER TO postgres;

--
-- Name: runtime_constraint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.runtime_constraint_id_seq OWNED BY public.runtime_constraint.id;


--
-- Name: runtime_constraint_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.runtime_constraint_question (
    id_question integer NOT NULL,
    id_programming_language integer NOT NULL,
    id_runtime_constraint integer NOT NULL,
    override_value text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.runtime_constraint_question OWNER TO postgres;

--
-- Name: runtime_constraint_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.runtime_constraint_question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runtime_constraint_question_id_seq OWNER TO postgres;

--
-- Name: runtime_constraint_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.runtime_constraint_question_id_seq OWNED BY public.runtime_constraint_question.id;


--
-- Name: runtime_constraint_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.runtime_constraint_test (
    id_c_question_test integer NOT NULL,
    id_programming_language integer NOT NULL,
    id_runtime_constraint integer NOT NULL,
    override_value text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.runtime_constraint_test OWNER TO postgres;

--
-- Name: runtime_constraint_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.runtime_constraint_test_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runtime_constraint_test_id_seq OWNER TO postgres;

--
-- Name: runtime_constraint_test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.runtime_constraint_test_id_seq OWNED BY public.runtime_constraint_test.id;


--
-- Name: scale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scale (
    id integer NOT NULL,
    name text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.scale OWNER TO postgres;

--
-- Name: scale_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scale_id_seq OWNER TO postgres;

--
-- Name: scale_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.scale_id_seq OWNED BY public.scale.id;


--
-- Name: scale_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scale_item (
    id integer NOT NULL,
    id_scale integer NOT NULL,
    ordinal smallint NOT NULL,
    value integer NOT NULL,
    label text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.scale_item OWNER TO postgres;

--
-- Name: scale_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scale_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scale_item_id_seq OWNER TO postgres;

--
-- Name: scale_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.scale_item_id_seq OWNED BY public.scale_item.id;


--
-- Name: semester; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.semester (
    id smallint NOT NULL,
    title character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.semester OWNER TO postgres;

--
-- Name: sql_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sql_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    sql_answer text NOT NULL,
    sql_alt_assertion text,
    sql_test_fixture text,
    sql_alt_presentation_query text,
    check_tuple_order boolean DEFAULT false NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_code_runner integer NOT NULL,
    id_check_column_mode smallint DEFAULT 4 NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.sql_question_answer OWNER TO postgres;

--
-- Name: sql_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sql_question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sql_question_answer_id_seq OWNER TO postgres;

--
-- Name: sql_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sql_question_answer_id_seq OWNED BY public.sql_question_answer.id;


--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student (
    id integer NOT NULL,
    alt_id character varying(50),
    alt_id2 character varying(50),
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    email character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_app_user integer,
    group_name character varying(30),
    provider character varying(50) DEFAULT 'saml.aai'::character varying,
    is_anonymous boolean DEFAULT false NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.student OWNER TO postgres;

--
-- Name: student_course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student_course (
    id integer NOT NULL,
    id_academic_year integer NOT NULL,
    id_course integer NOT NULL,
    id_student integer NOT NULL,
    id_teacher integer,
    class_group character varying(10),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.student_course OWNER TO postgres;

--
-- Name: student_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.student_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_course_id_seq OWNER TO postgres;

--
-- Name: student_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.student_course_id_seq OWNED BY public.student_course.id;


--
-- Name: student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_id_seq OWNER TO postgres;

--
-- Name: student_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.student_id_seq OWNED BY public.student.id;


--
-- Name: student_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student_token (
    id integer NOT NULL,
    student_id integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_valid_until timestamp with time zone NOT NULL,
    token text NOT NULL,
    pin text NOT NULL,
    is_activated boolean DEFAULT false NOT NULL,
    user_created character varying(50) NOT NULL,
    ts_modified timestamp with time zone,
    user_modified character varying(50)
);


ALTER TABLE public.student_token OWNER TO postgres;

--
-- Name: student_token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.student_token ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.student_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: teacher_lecture_quiz_instance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.teacher_lecture_quiz_instance (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_app_user integer NOT NULL,
    room_name character varying(50) NOT NULL,
    current_question_ordinal integer NOT NULL,
    ts_started timestamp with time zone NOT NULL,
    ts_ended timestamp with time zone,
    allow_anonymous boolean NOT NULL,
    user_created character varying(50) NOT NULL,
    ts_created timestamp with time zone,
    ts_modified timestamp with time zone,
    user_modified character varying(50)
);


ALTER TABLE public.teacher_lecture_quiz_instance OWNER TO postgres;

--
-- Name: teacher_lecture_quiz_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.teacher_lecture_quiz_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teacher_lecture_quiz_instance_id_seq OWNER TO postgres;

--
-- Name: teacher_lecture_quiz_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.teacher_lecture_quiz_instance_id_seq OWNED BY public.teacher_lecture_quiz_instance.id;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    id_course integer NOT NULL,
    id_academic_year integer NOT NULL,
    id_semester smallint NOT NULL,
    id_test_type integer NOT NULL,
    id_user_created integer NOT NULL,
    test_ordinal integer NOT NULL,
    max_runs integer NOT NULL,
    show_solutions boolean NOT NULL,
    max_score numeric(5,2) NOT NULL,
    password character varying(50) NOT NULL,
    questions_no smallint NOT NULL,
    duration_seconds integer NOT NULL,
    pass_percentage numeric(5,1) NOT NULL,
    ts_available_from timestamp with time zone NOT NULL,
    ts_available_to timestamp with time zone NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    review_period_mins integer DEFAULT 10 NOT NULL,
    hint_result boolean DEFAULT false NOT NULL,
    test_score_ignored boolean DEFAULT false NOT NULL,
    title_abbrev character varying(15),
    async_submit boolean DEFAULT true NOT NULL,
    trim_clock boolean DEFAULT false NOT NULL,
    id_email_reminder_scheme integer,
    allow_anonymous boolean DEFAULT false NOT NULL,
    is_competition boolean DEFAULT false,
    eval_comp_score text,
    upload_file_limit integer DEFAULT 0 NOT NULL,
    forward_only boolean DEFAULT false NOT NULL,
    id_parent integer,
    allow_anonymous_stalk boolean DEFAULT false NOT NULL,
    use_in_stats boolean DEFAULT true NOT NULL,
    is_global boolean DEFAULT false NOT NULL,
    is_public boolean DEFAULT false NOT NULL,
    id_plag_detection_algorithm integer,
    id_ticket_policy integer DEFAULT 2 NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT duration_seconds_btw_5_and_100days CHECK (((duration_seconds >= 5) AND (duration_seconds <= (((100 * 60) * 60) * 24)))),
    CONSTRAINT max_run_is_unsigned CHECK ((max_runs >= 0)),
    CONSTRAINT max_score_is_ok CHECK (((max_score >= (0)::numeric) AND (max_score <= (1000)::numeric))),
    CONSTRAINT pass_percentage_btw_0_and_1 CHECK (((pass_percentage >= (0)::numeric) AND (pass_percentage <= (1)::numeric))),
    CONSTRAINT password_is_at_least_4_chars CHECK ((length((password)::text) > 3)),
    CONSTRAINT questions_no_btw_0_and_1000 CHECK (((questions_no >= 1) AND (questions_no <= 1000))),
    CONSTRAINT review_period_mins CHECK ((review_period_mins >= 0)),
    CONSTRAINT test_ordinal_btw_0_and_200 CHECK (((test_ordinal >= 0) AND (test_ordinal <= 200))),
    CONSTRAINT ts_available_from_year_gt_2000 CHECK ((date_part('year'::text, ts_available_from) >= (2000)::double precision)),
    CONSTRAINT ts_available_to_gt_from CHECK ((ts_available_to > ts_available_from))
);


ALTER TABLE public.test OWNER TO postgres;

--
-- Name: test_correction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_correction (
    id_test_instance_question integer NOT NULL,
    score_delta numeric(10,3),
    id_user_created integer NOT NULL,
    reason text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id integer NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_correction OWNER TO postgres;

--
-- Name: test_correction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_correction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_correction_id_seq OWNER TO postgres;

--
-- Name: test_correction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_correction_id_seq OWNED BY public.test_correction.id;


--
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_id_seq OWNER TO postgres;

--
-- Name: test_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_id_seq OWNED BY public.test.id;


--
-- Name: test_instance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_student integer NOT NULL,
    ts_generated timestamp with time zone NOT NULL,
    ts_started timestamp with time zone,
    ts_submitted timestamp with time zone,
    ip_address character varying(50),
    correct_no smallint,
    incorrect_no smallint,
    unanswered_no smallint,
    partial_no smallint,
    score numeric(10,3),
    score_perc numeric(10,3),
    passed boolean,
    interupted boolean,
    prolonged boolean,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    comp_username text,
    user_created character varying(50) NOT NULL,
    CONSTRAINT correct_no_btw_0_and_100 CHECK (((correct_no >= 0) AND (correct_no <= 100))),
    CONSTRAINT incorrect_no_btw_0_and_100 CHECK (((incorrect_no >= 0) AND (incorrect_no <= 100))),
    CONSTRAINT partial_no_btw_0_and_100 CHECK (((partial_no >= 0) AND (partial_no <= 100))),
    CONSTRAINT score_perc_btw_neg200_and_200 CHECK (((score_perc >= ('-200'::integer)::numeric) AND (score_perc <= (200)::numeric))),
    CONSTRAINT unanswered_no_btw_0_and_100 CHECK (((unanswered_no >= 0) AND (unanswered_no <= 100)))
);


ALTER TABLE public.test_instance OWNER TO postgres;

--
-- Name: test_instance_gen_lock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_gen_lock (
    id integer NOT NULL,
    id_student integer,
    id_course integer,
    password character varying,
    ts_created timestamp with time zone NOT NULL
);


ALTER TABLE public.test_instance_gen_lock OWNER TO postgres;

--
-- Name: test_instance_gen_lock_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_gen_lock_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_gen_lock_id_seq OWNER TO postgres;

--
-- Name: test_instance_gen_lock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_gen_lock_id_seq OWNED BY public.test_instance_gen_lock.id;


--
-- Name: test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_id_seq OWNER TO postgres;

--
-- Name: test_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_id_seq OWNED BY public.test_instance.id;


--
-- Name: test_instance_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_question (
    id integer NOT NULL,
    id_test_instance integer NOT NULL,
    id_test_part integer,
    id_question integer NOT NULL,
    ordinal smallint NOT NULL,
    id_grading_model integer NOT NULL,
    answers_permutation integer[],
    weights_permutation integer[],
    correct_answers_permutation integer[],
    student_answers integer[],
    student_answer_code text,
    is_correct boolean,
    is_incorrect boolean,
    is_unanswered boolean,
    is_partial boolean,
    score numeric(10,3),
    score_perc numeric(10,3),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    hint text,
    c_eval_data text,
    student_answer_text text,
    penalty_percentage_permutation numeric(6,2)[],
    student_answer_code_pl integer,
    uploaded_files text,
    ts_started timestamp with time zone,
    user_created character varying(50) NOT NULL,
    CONSTRAINT ordinal_btw_0_and_100 CHECK (((ordinal >= 0) AND (ordinal <= 100))),
    CONSTRAINT score_perc_btw_neg200_and_200 CHECK (((score_perc >= ('-200'::integer)::numeric) AND (score_perc <= (200)::numeric)))
);


ALTER TABLE public.test_instance_question OWNER TO postgres;

--
-- Name: test_instance_question_comp_score; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_question_comp_score (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    base_score numeric(10,3),
    comp_score numeric(10,3),
    time_passed integer,
    number_of_runs integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_instance_question_comp_score OWNER TO postgres;

--
-- Name: test_instance_question_comp_score_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_question_comp_score_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_question_comp_score_id_seq OWNER TO postgres;

--
-- Name: test_instance_question_comp_score_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_question_comp_score_id_seq OWNED BY public.test_instance_question_comp_score.id;


--
-- Name: test_instance_question_generated; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_question_generated (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    data_object text NOT NULL,
    question_text_html text NOT NULL,
    question_abc_answers_html text[],
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_instance_question_generated OWNER TO postgres;

--
-- Name: test_instance_question_generated_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_question_generated_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_question_generated_id_seq OWNER TO postgres;

--
-- Name: test_instance_question_generated_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_question_generated_id_seq OWNED BY public.test_instance_question_generated.id;


--
-- Name: test_instance_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_question_id_seq OWNER TO postgres;

--
-- Name: test_instance_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_question_id_seq OWNED BY public.test_instance_question.id;


--
-- Name: test_instance_question_manual_grade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_question_manual_grade (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    score numeric(10,3),
    comment text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_app_user integer,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_instance_question_manual_grade OWNER TO postgres;

--
-- Name: test_instance_question_manual_grade_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_question_manual_grade_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_question_manual_grade_id_seq OWNER TO postgres;

--
-- Name: test_instance_question_manual_grade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_question_manual_grade_id_seq OWNED BY public.test_instance_question_manual_grade.id;


--
-- Name: test_instance_question_run; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_question_run (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    base_score numeric(10,3),
    is_correct boolean,
    ts_submitted timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_instance_question_run OWNER TO postgres;

--
-- Name: test_instance_question_run_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_question_run_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_question_run_id_seq OWNER TO postgres;

--
-- Name: test_instance_question_run_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_question_run_id_seq OWNED BY public.test_instance_question_run.id;


--
-- Name: test_instance_room; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_instance_room (
    id integer NOT NULL,
    id_test_instance integer NOT NULL,
    id_teacher_instance integer NOT NULL,
    room_name character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    ts_created timestamp with time zone,
    ts_modified timestamp with time zone,
    user_modified character varying(50)
);


ALTER TABLE public.test_instance_room OWNER TO postgres;

--
-- Name: test_instance_room_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_instance_room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_instance_room_id_seq OWNER TO postgres;

--
-- Name: test_instance_room_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_instance_room_id_seq OWNED BY public.test_instance_room.id;


--
-- Name: test_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_message (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_student integer NOT NULL,
    message_title text NOT NULL,
    message_body text NOT NULL,
    ts_sent timestamp with time zone,
    ts_displayed timestamp with time zone,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_message OWNER TO postgres;

--
-- Name: test_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_message_id_seq OWNER TO postgres;

--
-- Name: test_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_message_id_seq OWNED BY public.test_message.id;


--
-- Name: test_pace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_pace (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_pace_test integer,
    id_pace_question integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT test_or_question_pace_is_not_null CHECK (((id_pace_test IS NOT NULL) OR (id_pace_question IS NOT NULL)))
);


ALTER TABLE public.test_pace OWNER TO postgres;

--
-- Name: test_pace_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_pace_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_pace_id_seq OWNER TO postgres;

--
-- Name: test_pace_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_pace_id_seq OWNED BY public.test_pace.id;


--
-- Name: test_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_part (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_node integer NOT NULL,
    id_grading_model integer,
    min_questions integer NOT NULL,
    max_questions integer NOT NULL,
    pass_percentage numeric(5,1) DEFAULT 0 NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    id_question_type integer,
    ordinal smallint DEFAULT 0 NOT NULL,
    user_created character varying(50) NOT NULL,
    CONSTRAINT max_questions_is_gt_or_eq_min_and_lt_101 CHECK (((max_questions >= min_questions) AND (max_questions <= 101))),
    CONSTRAINT min_questions_btw_0_and_100 CHECK (((min_questions >= 0) AND (min_questions <= 100))),
    CONSTRAINT pass_percentage_btw_0_and_100 CHECK (((pass_percentage >= (0)::numeric) AND (pass_percentage <= (100)::numeric)))
);


ALTER TABLE public.test_part OWNER TO postgres;

--
-- Name: test_part_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_part_id_seq OWNER TO postgres;

--
-- Name: test_part_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_part_id_seq OWNED BY public.test_part.id;


--
-- Name: test_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_question (
    id integer NOT NULL,
    ordinal smallint NOT NULL,
    id_test integer NOT NULL,
    id_question integer NOT NULL,
    id_grading_model integer,
    ts_created timestamp with time zone,
    ts_modified timestamp with time zone,
    user_modified character varying(50),
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_question OWNER TO postgres;

--
-- Name: test_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_question_id_seq OWNER TO postgres;

--
-- Name: test_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_question_id_seq OWNED BY public.test_question.id;


--
-- Name: test_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test_type (
    id integer NOT NULL,
    type_name character varying(50),
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    standalone boolean DEFAULT true NOT NULL,
    fixed_questions boolean DEFAULT false NOT NULL,
    skip_permutations boolean DEFAULT false NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.test_type OWNER TO postgres;

--
-- Name: test_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.test_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_type_id_seq OWNER TO postgres;

--
-- Name: test_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.test_type_id_seq OWNED BY public.test_type.id;


--
-- Name: text_question_answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.text_question_answer (
    id integer NOT NULL,
    id_question integer NOT NULL,
    text_answer text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.text_question_answer OWNER TO postgres;

--
-- Name: text_question_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.text_question_answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.text_question_answer_id_seq OWNER TO postgres;

--
-- Name: text_question_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.text_question_answer_id_seq OWNED BY public.text_question_answer.id;


--
-- Name: ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket (
    id integer NOT NULL,
    id_test_instance_question integer NOT NULL,
    comments json[] DEFAULT '{}'::json[] NOT NULL,
    status public.enum_ticket_status DEFAULT 'open'::public.enum_ticket_status NOT NULL,
    id_assigned_user integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.ticket OWNER TO postgres;

--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_id_seq OWNER TO postgres;

--
-- Name: ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_id_seq OWNED BY public.ticket.id;


--
-- Name: ticket_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_message (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_student integer NOT NULL,
    ordinal integer NOT NULL,
    ts_sent timestamp with time zone,
    ts_displayed timestamp with time zone,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.ticket_message OWNER TO postgres;

--
-- Name: ticket_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_message_id_seq OWNER TO postgres;

--
-- Name: ticket_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_message_id_seq OWNED BY public.ticket_message.id;


--
-- Name: ticket_policy; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_policy (
    id integer NOT NULL,
    policy_name text,
    uses_tickets_exam boolean NOT NULL,
    uses_tickets_review boolean NOT NULL,
    uses_tickets_tutorial boolean DEFAULT false NOT NULL,
    uses_tickets_exercise boolean DEFAULT false NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.ticket_policy OWNER TO postgres;

--
-- Name: ticket_subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_subscription (
    id integer NOT NULL,
    id_test integer NOT NULL,
    id_question integer,
    id_app_user integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.ticket_subscription OWNER TO postgres;

--
-- Name: ticket_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_subscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_subscription_id_seq OWNER TO postgres;

--
-- Name: ticket_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_subscription_id_seq OWNED BY public.ticket_subscription.id;


--
-- Name: tutorial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial (
    id integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    id_node integer NOT NULL,
    tutorial_title character varying(128) NOT NULL,
    tutorial_desc text,
    is_active boolean NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    allow_random_access boolean DEFAULT true NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial OWNER TO postgres;

--
-- Name: tutorial_code_question_hint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_code_question_hint (
    id integer NOT NULL,
    id_step integer NOT NULL,
    id_question integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    is_active boolean NOT NULL,
    ordinal integer NOT NULL,
    answer_regex_trigger text NOT NULL,
    hint_text text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_code_question_hint OWNER TO postgres;

--
-- Name: tutorial_code_question_hint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_code_question_hint ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_code_question_hint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_course (
    id integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_course integer NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_course OWNER TO postgres;

--
-- Name: tutorial_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_course ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_question_hint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_question_hint (
    id integer NOT NULL,
    id_step integer NOT NULL,
    id_question_answer integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    is_active boolean NOT NULL,
    hint_text text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_question_hint OWNER TO postgres;

--
-- Name: tutorial_question_hint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_question_hint ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_question_hint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_step; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_step (
    id integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_question integer,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    ordinal integer NOT NULL,
    title text,
    text text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    id_code_runner integer,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_step OWNER TO postgres;

--
-- Name: tutorial_step_hint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_step_hint (
    id integer NOT NULL,
    id_step integer NOT NULL,
    id_user_created integer NOT NULL,
    id_user_modified integer NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    ordinal integer NOT NULL,
    hint_text text,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_step_hint OWNER TO postgres;

--
-- Name: tutorial_step_hint_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_step_hint ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_step_hint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_step_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_step ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_student (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_student integer NOT NULL,
    is_finished boolean DEFAULT false NOT NULL,
    ts_finished timestamp with time zone,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_student OWNER TO postgres;

--
-- Name: tutorial_student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_student ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_student_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_student_question (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_student integer NOT NULL,
    id_question integer NOT NULL,
    answers_permutation integer[],
    student_answers integer[],
    student_answer_code text,
    is_correct boolean,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_student_question OWNER TO postgres;

--
-- Name: tutorial_student_question_attempt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_student_question_attempt (
    id integer NOT NULL,
    id_course integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_question integer NOT NULL,
    id_student integer NOT NULL,
    is_correct boolean NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(64) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_student_question_attempt OWNER TO postgres;

--
-- Name: tutorial_student_question_attempt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_student_question_attempt ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_student_question_attempt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_student_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.tutorial_student_question ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tutorial_student_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tutorial_ticket; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_ticket (
    id integer NOT NULL,
    id_tutorial_step integer NOT NULL,
    id_student integer NOT NULL,
    comments json[] DEFAULT '{}'::json[] NOT NULL,
    status public.enum_ticket_status DEFAULT 'open'::public.enum_ticket_status NOT NULL,
    id_assigned_user integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_ticket OWNER TO postgres;

--
-- Name: tutorial_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tutorial_ticket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutorial_ticket_id_seq OWNER TO postgres;

--
-- Name: tutorial_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tutorial_ticket_id_seq OWNED BY public.tutorial_ticket.id;


--
-- Name: tutorial_ticket_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_ticket_message (
    id integer NOT NULL,
    id_tutorial_ticket integer NOT NULL,
    ts_sent timestamp with time zone,
    ts_displayed timestamp with time zone,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_ticket_message OWNER TO postgres;

--
-- Name: tutorial_ticket_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tutorial_ticket_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutorial_ticket_message_id_seq OWNER TO postgres;

--
-- Name: tutorial_ticket_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tutorial_ticket_message_id_seq OWNED BY public.tutorial_ticket_message.id;


--
-- Name: tutorial_ticket_subscription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutorial_ticket_subscription (
    id integer NOT NULL,
    id_tutorial integer NOT NULL,
    id_app_user integer,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.tutorial_ticket_subscription OWNER TO postgres;

--
-- Name: tutorial_ticket_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tutorial_ticket_subscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutorial_ticket_subscription_id_seq OWNER TO postgres;

--
-- Name: tutorial_ticket_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tutorial_ticket_subscription_id_seq OWNED BY public.tutorial_ticket_subscription.id;


--
-- Name: upro_schedule_set_test_params; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.upro_schedule_set_test_params (
    id integer NOT NULL,
    id_test integer NOT NULL,
    group_no smallint NOT NULL,
    ts_start timestamp without time zone NOT NULL,
    ts_end timestamp without time zone NOT NULL,
    test_password text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.upro_schedule_set_test_params OWNER TO postgres;

--
-- Name: upro_schedule_set_test_params_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.upro_schedule_set_test_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.upro_schedule_set_test_params_id_seq OWNER TO postgres;

--
-- Name: upro_schedule_set_test_params_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.upro_schedule_set_test_params_id_seq OWNED BY public.upro_schedule_set_test_params.id;


--
-- Name: v_calib_question_answer; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_calib_question_answer AS
 SELECT ptgqc.id_question_calib,
    ptgq.ordinal,
    scale_item.value
   FROM ((public.peer_test_group_question_calib ptgqc
     JOIN public.peer_test_group_question ptgq ON ((ptgqc.id_peer_test_group_question = ptgq.id)))
     JOIN public.scale_item ON ((ptgqc.id_scale_item = scale_item.id)));


ALTER TABLE public.v_calib_question_answer OWNER TO postgres;

--
-- Name: v_d3tree; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_d3tree AS
 WITH RECURSIVE d3tree(id, id_ext, id_parent, id_parent_ext, node_name, depth, id_course) AS (
         SELECT node.id,
            (node.id)::text AS id,
            NULL::integer AS int4,
            ''::text AS text,
            node.node_name,
            1 AS depth,
            course.id AS id_course
           FROM (public.node
             JOIN public.course ON ((node.id = course.id_root_node)))
        UNION ALL
         SELECT np.id_child,
            ((d3tree_1.id_ext || '.'::text) || np.id_child),
            np.id_parent,
            d3tree_1.id_ext,
            node.node_name,
            (d3tree_1.depth + 1),
            d3tree_1.id_course
           FROM ((public.node_parent np
             JOIN public.node ON ((np.id_child = node.id)))
             JOIN d3tree d3tree_1 ON ((np.id_parent = d3tree_1.id)))
        )
 SELECT d3tree.id,
    d3tree.id_ext,
    d3tree.id_parent,
    d3tree.id_parent_ext,
    d3tree.node_name,
    d3tree.depth,
    d3tree.id_course
   FROM d3tree;


ALTER TABLE public.v_d3tree OWNER TO postgres;

--
-- Name: v_exercise_question; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_exercise_question AS
 SELECT DISTINCT exercise_node.id_exercise,
    question_node.id_question
   FROM ((public.question_node
     JOIN public.node ON ((node.id = question_node.id_node)))
     JOIN public.exercise_node ON ((exercise_node.id_node = node.id)));


ALTER TABLE public.v_exercise_question OWNER TO postgres;

--
-- Name: v_node_path; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_node_path AS
 WITH RECURSIVE search_node(id, id_parent, name, depth, path, cycle, last, name2) AS (
         SELECT n.id,
            np.id_parent,
            ARRAY[n.node_name] AS "array",
            1 AS "?column?",
            ARRAY[n.id] AS "array",
            false AS bool,
                CASE
                    WHEN (np.id_parent IS NULL) THEN true
                    ELSE false
                END AS "case",
            ARRAY[n.node_name] AS "array"
           FROM (public.node n
             LEFT JOIN public.node_parent np ON ((n.id = np.id_child)))
        UNION ALL
         SELECT sn.id,
            np.id_parent,
            ((sn.name || n.node_name))::character varying(255)[] AS "varchar",
            (sn.depth + 1),
            (sn.path || n.id),
            (n.id = ANY (sn.path)),
                CASE
                    WHEN (np.id_parent IS NULL) THEN true
                    ELSE false
                END AS "case",
            ((n.node_name || sn.name))::character varying(255)[] AS "varchar"
           FROM ((public.node n
             JOIN search_node sn ON ((n.id = sn.id_parent)))
             LEFT JOIN public.node_parent np ON ((n.id = np.id_child)))
          WHERE (NOT sn.cycle)
        )
 SELECT search_node.id,
    array_to_string(search_node.name, ' <- '::text) AS rpath,
    array_to_string(search_node.name2, ' -> '::text) AS path,
    search_node.depth
   FROM search_node
  WHERE (search_node.last = true);


ALTER TABLE public.v_node_path OWNER TO postgres;

--
-- Name: v_node_tree; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_node_tree AS
 WITH RECURSIVE node_tree(id, id_ext, id_parent, id_parent_ext, node_name, description, depth, id_course, id_node_type, type_name, color) AS (
         SELECT node.id,
            (node.id)::text AS id,
            NULL::integer AS int4,
            ''::text AS text,
            node.node_name,
            node.description,
            1 AS depth,
            course.id AS id_course,
            node.id_node_type,
            node_type.type_name,
            node_type.color
           FROM ((public.node
             JOIN public.course ON ((node.id = course.id_root_node)))
             JOIN public.node_type ON ((node.id_node_type = node_type.id)))
        UNION ALL
         SELECT np.id_child,
            ((node_tree_1.id_ext || '.'::text) || np.id_child),
            np.id_parent,
            node_tree_1.id_ext,
            node.node_name,
            node.description,
            (node_tree_1.depth + 1),
            node_tree_1.id_course,
            node.id_node_type,
            node_type.type_name,
            node_type.color
           FROM (((public.node_parent np
             JOIN public.node ON ((np.id_child = node.id)))
             JOIN node_tree node_tree_1 ON ((np.id_parent = node_tree_1.id)))
             JOIN public.node_type ON ((node.id_node_type = node_type.id)))
        )
 SELECT node_tree.id,
    node_tree.id_ext,
    node_tree.id_parent,
    node_tree.id_parent_ext,
    node_tree.node_name,
    node_tree.description,
    node_tree.depth,
    node_tree.id_course,
    node_tree.id_node_type,
    node_tree.type_name,
    node_tree.color
   FROM node_tree;


ALTER TABLE public.v_node_tree OWNER TO postgres;

--
-- Name: v_peer_shuffle; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_peer_shuffle AS
 SELECT ps.id AS id_peer_shuffle,
    s1.id AS id_student1,
    s1.first_name AS first_name1,
    s1.last_name AS last_name1,
    ps.id_test_instance_question AS id_test_instance_question1,
    s2.id AS id_student2,
    s2.first_name AS first_name2,
    s2.last_name AS last_name2,
    a.id_test_instance_question2,
    tiq2.id_question AS id_question_pa1,
    ti2.id AS id_test_instance2,
    a.ordinal2,
    t1.id AS id_test_phase_1,
    ps.id_test_phase_2,
    pa_test.id AS id_pa_test
   FROM (((((((((((public.peer_shuffle ps
     JOIN LATERAL unnest(ps.jobs) WITH ORDINALITY a(id_test_instance_question2, ordinal2) ON (true))
     JOIN public.test_instance_question tiq1 ON ((ps.id_test_instance_question = tiq1.id)))
     JOIN public.test_instance ti1 ON ((tiq1.id_test_instance = ti1.id)))
     JOIN public.test t1 ON ((ti1.id_test = t1.id)))
     JOIN public.student s1 ON ((ti1.id_student = s1.id)))
     JOIN public.test_instance_question tiq2 ON ((a.id_test_instance_question2 = tiq2.id)))
     JOIN public.test_instance ti2 ON ((tiq2.id_test_instance = ti2.id)))
     JOIN public.test t2 ON ((ti2.id_test = t2.id)))
     JOIN public.student s2 ON ((ti2.id_student = s2.id)))
     JOIN public.test pa_master ON ((pa_master.id = ps.id_test_phase_2)))
     LEFT JOIN public.test pa_test ON (((pa_test.id_parent = ps.id_test_phase_2) AND (pa_test.test_ordinal = (a.ordinal2 + pa_master.test_ordinal)))));


ALTER TABLE public.v_peer_shuffle OWNER TO postgres;

--
-- Name: v_roots_children; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_roots_children AS
 WITH RECURSIVE roots_children(id_child, id_parent, id_root, depth) AS (
         SELECT node.id,
            NULL::integer AS int4,
            node.id,
            1 AS "?column?"
           FROM (public.node
             JOIN public.node_type ON ((node.id_node_type = node_type.id)))
        UNION ALL
         SELECT np.id_child,
            np.id_parent,
            rc.id_root,
            (rc.depth + 1)
           FROM (public.node_parent np
             JOIN roots_children rc ON ((np.id_parent = rc.id_child)))
        )
 SELECT roots_children.id_child,
    roots_children.id_parent,
    roots_children.id_root,
    roots_children.depth
   FROM roots_children;


ALTER TABLE public.v_roots_children OWNER TO postgres;

--
-- Name: v_student_question_instance_answer; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_student_question_instance_answer AS
 SELECT test_instance.id_student,
    test_instance_question.id_question,
        CASE
            WHEN test_instance_question.is_correct THEN 1
            ELSE 0
        END AS is_correct
   FROM ((public.test_instance_question
     JOIN public.test_instance ON ((test_instance_question.id_test_instance = test_instance.id)))
     JOIN public.student ON ((student.id = test_instance.id_student)));


ALTER TABLE public.v_student_question_instance_answer OWNER TO postgres;

--
-- Name: v_student_question_instance_correct; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_student_question_instance_correct AS
 SELECT test_instance.id_student,
    test_instance_question.id_question,
        CASE
            WHEN test_instance_question.is_correct THEN 1
            ELSE 0
        END AS is_correct
   FROM ((public.test_instance_question
     JOIN public.test_instance ON ((test_instance_question.id_test_instance = test_instance.id)))
     JOIN public.student ON ((student.id = test_instance.id_student)));


ALTER TABLE public.v_student_question_instance_correct OWNER TO postgres;

--
-- Name: v_student_total_score; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_student_total_score AS
 SELECT test.id_course,
    test_instance.id_student,
    sum(test_instance.score) AS score
   FROM public.test,
    public.test_instance
  WHERE (test.id = test_instance.id_test)
  GROUP BY test.id_course, test_instance.id_student;


ALTER TABLE public.v_student_total_score OWNER TO postgres;

--
-- Name: v_test_stats; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_test_stats AS
 SELECT test.id,
    test.title,
    test.id_course,
    test.id_academic_year,
    test.id_semester,
    test.id_test_type,
    test.id_user_created,
    test.test_ordinal,
    test.max_runs,
    test.show_solutions,
    test.max_score,
    test.password,
    test.questions_no,
    test.duration_seconds,
    test.pass_percentage,
    test.ts_available_from,
    test.ts_available_to,
    test.ts_created,
    test.ts_modified,
    test.user_modified,
    test.review_period_mins,
    test.hint_result,
    test.test_score_ignored,
    test.title_abbrev,
    test.async_submit,
    test.trim_clock,
    test.id_email_reminder_scheme,
    test.allow_anonymous,
    test.is_competition,
    test.eval_comp_score,
    test.upload_file_limit,
    test.forward_only,
    test.id_parent,
    test.allow_anonymous_stalk,
    test.use_in_stats,
    test.is_global,
    test.is_public
   FROM (public.test
     JOIN public.test_type ON (((test.id_test_type = test_type.id) AND ((test_type.type_name)::text <> 'Lecture quiz'::text))))
  WHERE ((NOT test.test_score_ignored) AND test.use_in_stats AND (test.ts_available_from <= CURRENT_TIMESTAMP));


ALTER TABLE public.v_test_stats OWNER TO postgres;

--
-- Name: v_tmp; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.v_tmp AS
 SELECT node.node_name,
    vrc.id_child,
    vrc.id_parent,
    vrc.depth,
    sum(
        CASE
            WHEN (question.is_active = true) THEN 1
            ELSE 0
        END) AS cnt_active,
    sum(
        CASE
            WHEN (question.is_active = false) THEN 1
            ELSE 0
        END) AS cnt_inactive,
    vrc.id_root
   FROM (((public.v_roots_children vrc
     JOIN public.node ON ((vrc.id_child = node.id)))
     LEFT JOIN public.question_node ON ((question_node.id_node = vrc.id_child)))
     LEFT JOIN public.question ON ((question_node.id_question = question.id)))
  WHERE (vrc.id_root = ( SELECT course.id_root_node
           FROM public.course
          WHERE (course.id = 155)))
  GROUP BY node.node_name, vrc.id_child, vrc.id_parent, vrc.depth, vrc.id_root
  ORDER BY vrc.depth;


ALTER TABLE public.v_tmp OWNER TO postgres;

--
-- Name: versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.versions (
    id smallint NOT NULL,
    tag text NOT NULL,
    ts_created timestamp with time zone NOT NULL,
    ts_modified timestamp with time zone NOT NULL,
    user_modified character varying(50) NOT NULL,
    user_created character varying(50) NOT NULL
);


ALTER TABLE public.versions OWNER TO postgres;

--
-- Name: logged_actions event_id; Type: DEFAULT; Schema: audit; Owner: postgres
--

ALTER TABLE ONLY audit.logged_actions ALTER COLUMN event_id SET DEFAULT nextval('audit.logged_actions_event_id_seq'::regclass);


--
-- Name: academic_year id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.academic_year ALTER COLUMN id SET DEFAULT nextval('public.academic_year_id_seq'::regclass);


--
-- Name: app_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user ALTER COLUMN id SET DEFAULT nextval('public.app_user_id_seq'::regclass);


--
-- Name: app_user_course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_course ALTER COLUMN id SET DEFAULT nextval('public.app_user_course_id_seq'::regclass);


--
-- Name: app_user_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_role ALTER COLUMN id SET DEFAULT nextval('public.app_user_role_id_seq'::regclass);


--
-- Name: c_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_answer ALTER COLUMN id SET DEFAULT nextval('public.c_question_answer_id_seq'::regclass);


--
-- Name: c_question_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_test ALTER COLUMN id SET DEFAULT nextval('public.c_question_test_id_seq'::regclass);


--
-- Name: code_runner id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_runner ALTER COLUMN id SET DEFAULT nextval('public.code_runner_id_seq'::regclass);


--
-- Name: code_snippet id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_snippet ALTER COLUMN id SET DEFAULT nextval('public.code_snippet_id_seq'::regclass);


--
-- Name: computer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.computer ALTER COLUMN id SET DEFAULT nextval('public.computer_id_seq'::regclass);


--
-- Name: connected_elements_correctness_model id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_correctness_model ALTER COLUMN id SET DEFAULT nextval('public.connected_elements_correctness_model_id_seq'::regclass);


--
-- Name: connected_elements_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question ALTER COLUMN id SET DEFAULT nextval('public.connected_elements_question_id_seq'::regclass);


--
-- Name: connected_elements_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question_answer ALTER COLUMN id SET DEFAULT nextval('public.connected_elements_question_answer_id_seq'::regclass);


--
-- Name: course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course ALTER COLUMN id SET DEFAULT nextval('public.course_id_seq'::regclass);


--
-- Name: course_code_runner id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner ALTER COLUMN id SET DEFAULT nextval('public.course_code_runner_id_seq1'::regclass);


--
-- Name: course_tag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_tag ALTER COLUMN id SET DEFAULT nextval('public.course_tag_id_seq'::regclass);


--
-- Name: data_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_type ALTER COLUMN id SET DEFAULT nextval('public.data_type_id_seq'::regclass);


--
-- Name: diagram_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diagram_question_answer ALTER COLUMN id SET DEFAULT nextval('public.diagram_question_answer_id_seq'::regclass);


--
-- Name: email_handled_reminders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_handled_reminders ALTER COLUMN id SET DEFAULT nextval('public.email_handled_reminders_id_seq'::regclass);


--
-- Name: email_queue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_queue ALTER COLUMN id SET DEFAULT nextval('public.email_queue_id_seq'::regclass);


--
-- Name: email_reminder_scheme id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_reminder_scheme ALTER COLUMN id SET DEFAULT nextval('public.email_reminder_scheme_id_seq'::regclass);


--
-- Name: fixed_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fixed_test ALTER COLUMN id SET DEFAULT nextval('public.fixed_test_id_seq'::regclass);


--
-- Name: generator_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test ALTER COLUMN id SET DEFAULT nextval('public.generator_test_id_seq'::regclass);


--
-- Name: generator_test_file id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test_file ALTER COLUMN id SET DEFAULT nextval('public.generator_test_file_id_seq'::regclass);


--
-- Name: grading_model id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grading_model ALTER COLUMN id SET DEFAULT nextval('public.grading_model_id_seq'::regclass);


--
-- Name: j0_unit_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.j0_unit_type ALTER COLUMN id SET DEFAULT nextval('public.j0_unit_type_id_seq'::regclass);


--
-- Name: json_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.json_question_answer ALTER COLUMN id SET DEFAULT nextval('public.json_question_answer_id_seq'::regclass);


--
-- Name: local_provider id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.local_provider ALTER COLUMN id SET DEFAULT nextval('public.local_provider_id_seq'::regclass);


--
-- Name: node id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node ALTER COLUMN id SET DEFAULT nextval('public.node_id_seq'::regclass);


--
-- Name: node_hierarchy id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_hierarchy ALTER COLUMN id SET DEFAULT nextval('public.node_hierarchy_id_seq'::regclass);


--
-- Name: node_parent id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_parent ALTER COLUMN id SET DEFAULT nextval('public.node_parent_id_seq'::regclass);


--
-- Name: node_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_type ALTER COLUMN id SET DEFAULT nextval('public.node_type_id_seq'::regclass);


--
-- Name: ordered_element_correctness_model id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_correctness_model ALTER COLUMN id SET DEFAULT nextval('public.ordered_element_correctness_model_id_seq'::regclass);


--
-- Name: ordered_element_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_question ALTER COLUMN id SET DEFAULT nextval('public.ordered_element_question_id_seq'::regclass);


--
-- Name: pace id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pace ALTER COLUMN id SET DEFAULT nextval('public.pace_id_seq'::regclass);


--
-- Name: peer_shuffle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_shuffle ALTER COLUMN id SET DEFAULT nextval('public.peer_shuffle_id_seq'::regclass);


--
-- Name: peer_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test ALTER COLUMN id SET DEFAULT nextval('public.peer_test_id_seq'::regclass);


--
-- Name: peer_test_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group ALTER COLUMN id SET DEFAULT nextval('public.peer_test_group_id_seq'::regclass);


--
-- Name: peer_test_group_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question ALTER COLUMN id SET DEFAULT nextval('public.peer_test_group_question_id_seq'::regclass);


--
-- Name: peer_test_group_question_calib id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib ALTER COLUMN id SET DEFAULT nextval('public.peer_test_group_question_calib_id_seq'::regclass);


--
-- Name: perm_user_course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perm_user_course ALTER COLUMN id SET DEFAULT nextval('public.perm_user_course_id_seq'::regclass);


--
-- Name: plag_detection_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plag_detection_data ALTER COLUMN id SET DEFAULT nextval('public.plag_detection_data_id_seq'::regclass);


--
-- Name: playground_code_runner id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playground_code_runner ALTER COLUMN id SET DEFAULT nextval('public.course_code_runner_id_seq'::regclass);


--
-- Name: programming_language id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language ALTER COLUMN id SET DEFAULT nextval('public.programming_language_id_seq'::regclass);


--
-- Name: question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question ALTER COLUMN id SET DEFAULT nextval('public.question_id_seq'::regclass);


--
-- Name: question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_answer ALTER COLUMN id SET DEFAULT nextval('public.question_answer_id_seq'::regclass);


--
-- Name: question_attachment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_attachment ALTER COLUMN id SET DEFAULT nextval('public.question_attachment_id_seq'::regclass);


--
-- Name: question_course_tag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_course_tag ALTER COLUMN id SET DEFAULT nextval('public.question_course_tag_id_seq'::regclass);


--
-- Name: question_data_object id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_data_object ALTER COLUMN id SET DEFAULT nextval('public.question_data_object_id_seq'::regclass);


--
-- Name: question_eval_script id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_eval_script ALTER COLUMN id SET DEFAULT nextval('public.question_eval_script_id_seq'::regclass);


--
-- Name: question_node id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_node ALTER COLUMN id SET DEFAULT nextval('public.question_node_id_seq'::regclass);


--
-- Name: question_programming_language id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_programming_language ALTER COLUMN id SET DEFAULT nextval('public.question_programming_language_id_seq'::regclass);


--
-- Name: question_review id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_review ALTER COLUMN id SET DEFAULT nextval('public.question_review_id_seq'::regclass);


--
-- Name: question_scale id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_scale ALTER COLUMN id SET DEFAULT nextval('public.question_scale_id_seq'::regclass);


--
-- Name: random_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.random_test ALTER COLUMN id SET DEFAULT nextval('public.random_test_id_seq'::regclass);


--
-- Name: real_time_plag_detection_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.real_time_plag_detection_data ALTER COLUMN id SET DEFAULT nextval('public.real_time_plag_detection_data_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: runtime_constraint id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint ALTER COLUMN id SET DEFAULT nextval('public.runtime_constraint_id_seq'::regclass);


--
-- Name: runtime_constraint_course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_course ALTER COLUMN id SET DEFAULT nextval('public.runtime_constraint_course_id_seq'::regclass);


--
-- Name: runtime_constraint_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_question ALTER COLUMN id SET DEFAULT nextval('public.runtime_constraint_question_id_seq'::regclass);


--
-- Name: runtime_constraint_test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_test ALTER COLUMN id SET DEFAULT nextval('public.runtime_constraint_test_id_seq'::regclass);


--
-- Name: scale id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scale ALTER COLUMN id SET DEFAULT nextval('public.scale_id_seq'::regclass);


--
-- Name: scale_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scale_item ALTER COLUMN id SET DEFAULT nextval('public.scale_item_id_seq'::regclass);


--
-- Name: sql_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sql_question_answer ALTER COLUMN id SET DEFAULT nextval('public.sql_question_answer_id_seq'::regclass);


--
-- Name: student id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student ALTER COLUMN id SET DEFAULT nextval('public.student_id_seq'::regclass);


--
-- Name: student_course id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course ALTER COLUMN id SET DEFAULT nextval('public.student_course_id_seq'::regclass);


--
-- Name: teacher_lecture_quiz_instance id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher_lecture_quiz_instance ALTER COLUMN id SET DEFAULT nextval('public.teacher_lecture_quiz_instance_id_seq'::regclass);


--
-- Name: test id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test ALTER COLUMN id SET DEFAULT nextval('public.test_id_seq'::regclass);


--
-- Name: test_correction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_correction ALTER COLUMN id SET DEFAULT nextval('public.test_correction_id_seq'::regclass);


--
-- Name: test_instance id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance ALTER COLUMN id SET DEFAULT nextval('public.test_instance_id_seq'::regclass);


--
-- Name: test_instance_gen_lock id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_gen_lock ALTER COLUMN id SET DEFAULT nextval('public.test_instance_gen_lock_id_seq'::regclass);


--
-- Name: test_instance_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question ALTER COLUMN id SET DEFAULT nextval('public.test_instance_question_id_seq'::regclass);


--
-- Name: test_instance_question_comp_score id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_comp_score ALTER COLUMN id SET DEFAULT nextval('public.test_instance_question_comp_score_id_seq'::regclass);


--
-- Name: test_instance_question_generated id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_generated ALTER COLUMN id SET DEFAULT nextval('public.test_instance_question_generated_id_seq'::regclass);


--
-- Name: test_instance_question_manual_grade id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_manual_grade ALTER COLUMN id SET DEFAULT nextval('public.test_instance_question_manual_grade_id_seq'::regclass);


--
-- Name: test_instance_question_run id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_run ALTER COLUMN id SET DEFAULT nextval('public.test_instance_question_run_id_seq'::regclass);


--
-- Name: test_instance_room id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_room ALTER COLUMN id SET DEFAULT nextval('public.test_instance_room_id_seq'::regclass);


--
-- Name: test_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_message ALTER COLUMN id SET DEFAULT nextval('public.test_message_id_seq'::regclass);


--
-- Name: test_pace id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_pace ALTER COLUMN id SET DEFAULT nextval('public.test_pace_id_seq'::regclass);


--
-- Name: test_part id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part ALTER COLUMN id SET DEFAULT nextval('public.test_part_id_seq'::regclass);


--
-- Name: test_question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_question ALTER COLUMN id SET DEFAULT nextval('public.test_question_id_seq'::regclass);


--
-- Name: test_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_type ALTER COLUMN id SET DEFAULT nextval('public.test_type_id_seq'::regclass);


--
-- Name: text_question_answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.text_question_answer ALTER COLUMN id SET DEFAULT nextval('public.text_question_answer_id_seq'::regclass);


--
-- Name: ticket id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket ALTER COLUMN id SET DEFAULT nextval('public.ticket_id_seq'::regclass);


--
-- Name: ticket_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_message ALTER COLUMN id SET DEFAULT nextval('public.ticket_message_id_seq'::regclass);


--
-- Name: ticket_subscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_subscription ALTER COLUMN id SET DEFAULT nextval('public.ticket_subscription_id_seq'::regclass);


--
-- Name: tutorial_ticket id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket ALTER COLUMN id SET DEFAULT nextval('public.tutorial_ticket_id_seq'::regclass);


--
-- Name: tutorial_ticket_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_message ALTER COLUMN id SET DEFAULT nextval('public.tutorial_ticket_message_id_seq'::regclass);


--
-- Name: tutorial_ticket_subscription id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_subscription ALTER COLUMN id SET DEFAULT nextval('public.tutorial_ticket_subscription_id_seq'::regclass);


--
-- Name: upro_schedule_set_test_params id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upro_schedule_set_test_params ALTER COLUMN id SET DEFAULT nextval('public.upro_schedule_set_test_params_id_seq'::regclass);


--
-- Name: logged_actions logged_actions_pkey; Type: CONSTRAINT; Schema: audit; Owner: postgres
--

ALTER TABLE ONLY audit.logged_actions
    ADD CONSTRAINT logged_actions_pkey PRIMARY KEY (event_id);


--
-- Name: academic_year academic_year_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.academic_year
    ADD CONSTRAINT academic_year_pkey PRIMARY KEY (id);


--
-- Name: adaptivity_model_behaviour adaptivity_model_behaviour_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adaptivity_model_behaviour
    ADD CONSTRAINT adaptivity_model_behaviour_pkey PRIMARY KEY (id);


--
-- Name: adaptivity_model adaptivity_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adaptivity_model
    ADD CONSTRAINT adaptivity_model_pkey PRIMARY KEY (id);


--
-- Name: answer_regex_trigger_template answer_regex_trigger_template_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer_regex_trigger_template
    ADD CONSTRAINT answer_regex_trigger_template_ordinal_uniq UNIQUE (ordinal);


--
-- Name: answer_regex_trigger_template answer_regex_trigger_template_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer_regex_trigger_template
    ADD CONSTRAINT answer_regex_trigger_template_pkey PRIMARY KEY (id);


--
-- Name: app_user_course app_user_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_course
    ADD CONSTRAINT app_user_course_pkey PRIMARY KEY (id);


--
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);


--
-- Name: app_user_role app_user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_role
    ADD CONSTRAINT app_user_role_pkey PRIMARY KEY (id);


--
-- Name: c_question_answer c_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_answer
    ADD CONSTRAINT c_question_answer_pkey PRIMARY KEY (id);


--
-- Name: c_test_type c_test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_test_type
    ADD CONSTRAINT c_test_type_pkey PRIMARY KEY (id);


--
-- Name: check_column_mode check_column_mode_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.check_column_mode
    ADD CONSTRAINT check_column_mode_pkey PRIMARY KEY (id);


--
-- Name: code_runner code_runner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_runner
    ADD CONSTRAINT code_runner_pkey PRIMARY KEY (id);


--
-- Name: code_snippet code_snippet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_snippet
    ADD CONSTRAINT code_snippet_pkey PRIMARY KEY (id);


--
-- Name: computer computer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.computer
    ADD CONSTRAINT computer_pkey PRIMARY KEY (ip);


--
-- Name: connected_elements_correctness_model connected_elements_correctness_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_correctness_model
    ADD CONSTRAINT connected_elements_correctness_model_pkey PRIMARY KEY (id);


--
-- Name: connected_elements_question_answer connected_elements_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question_answer
    ADD CONSTRAINT connected_elements_question_answer_pkey PRIMARY KEY (id);


--
-- Name: connected_elements_question connected_elements_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question
    ADD CONSTRAINT connected_elements_question_pkey PRIMARY KEY (id);


--
-- Name: playground_code_runner course_code_runner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playground_code_runner
    ADD CONSTRAINT course_code_runner_pkey PRIMARY KEY (id);


--
-- Name: course_code_runner course_code_runner_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner
    ADD CONSTRAINT course_code_runner_pkey1 PRIMARY KEY (id_course, id_programming_language, id_code_runner);


--
-- Name: course course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_pkey PRIMARY KEY (id);


--
-- Name: course_tag course_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_tag
    ADD CONSTRAINT course_tag_pkey PRIMARY KEY (id);


--
-- Name: data_type data_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_type
    ADD CONSTRAINT data_type_pkey PRIMARY KEY (id);


--
-- Name: data_type data_type_type_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_type
    ADD CONSTRAINT data_type_type_name_key UNIQUE (type_name);


--
-- Name: dendrogram_distance_algorithm dendrogram_distance_algorithm_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dendrogram_distance_algorithm
    ADD CONSTRAINT dendrogram_distance_algorithm_pkey PRIMARY KEY (id);


--
-- Name: diagram_question_answer diagram_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diagram_question_answer
    ADD CONSTRAINT diagram_question_answer_pkey PRIMARY KEY (id);


--
-- Name: email_handled_reminders email_handled_reminders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_handled_reminders
    ADD CONSTRAINT email_handled_reminders_pkey PRIMARY KEY (id);


--
-- Name: email_queue email_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_queue
    ADD CONSTRAINT email_queue_pkey PRIMARY KEY (id);


--
-- Name: email_reminder_scheme email_reminder_scheme_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_reminder_scheme
    ADD CONSTRAINT email_reminder_scheme_pkey PRIMARY KEY (id);


--
-- Name: exercise_node exercise_node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_node
    ADD CONSTRAINT exercise_node_pkey PRIMARY KEY (id);


--
-- Name: exercise exercise_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_pkey PRIMARY KEY (id);


--
-- Name: exercise_student exercise_student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student
    ADD CONSTRAINT exercise_student_pkey PRIMARY KEY (id);


--
-- Name: exercise_student_question_attempt exercise_student_question_attempt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question_attempt
    ADD CONSTRAINT exercise_student_question_attempt_pkey PRIMARY KEY (id);


--
-- Name: exercise_student_question exercise_student_question_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question
    ADD CONSTRAINT exercise_student_question_ordinal_uniq UNIQUE (id_exercise, id_student, id_question, ordinal);


--
-- Name: exercise_student_question exercise_student_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question
    ADD CONSTRAINT exercise_student_question_pkey PRIMARY KEY (id);


--
-- Name: feedback feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);


--
-- Name: fixed_test fixed_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fixed_test
    ADD CONSTRAINT fixed_test_pkey PRIMARY KEY (id);


--
-- Name: generator_test_file generator_test_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test_file
    ADD CONSTRAINT generator_test_file_pkey PRIMARY KEY (id);


--
-- Name: generator_test generator_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test
    ADD CONSTRAINT generator_test_pkey PRIMARY KEY (id);


--
-- Name: global_data_object global_data_object_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.global_data_object
    ADD CONSTRAINT global_data_object_pkey PRIMARY KEY (id);


--
-- Name: grading_model grading_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grading_model
    ADD CONSTRAINT grading_model_pkey PRIMARY KEY (id);


--
-- Name: j0_unit_type j0_unit_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.j0_unit_type
    ADD CONSTRAINT j0_unit_type_pkey PRIMARY KEY (id);


--
-- Name: j0_unit_type j0_unit_type_type_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.j0_unit_type
    ADD CONSTRAINT j0_unit_type_type_name_key UNIQUE (type_name);


--
-- Name: json_question_answer json_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.json_question_answer
    ADD CONSTRAINT json_question_answer_pkey PRIMARY KEY (id);


--
-- Name: local_provider local_provider_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.local_provider
    ADD CONSTRAINT local_provider_pkey PRIMARY KEY (id_student);


--
-- Name: node_hierarchy node_hierarchy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_hierarchy
    ADD CONSTRAINT node_hierarchy_pkey PRIMARY KEY (id);


--
-- Name: node_parent node_parent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_parent
    ADD CONSTRAINT node_parent_pkey PRIMARY KEY (id);


--
-- Name: node node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT node_pkey PRIMARY KEY (id);


--
-- Name: node_type node_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_type
    ADD CONSTRAINT node_type_pkey PRIMARY KEY (id);


--
-- Name: ordered_element_correctness_model ordered_element_correctness_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_correctness_model
    ADD CONSTRAINT ordered_element_correctness_model_pkey PRIMARY KEY (id);


--
-- Name: ordered_element_question ordered_element_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_question
    ADD CONSTRAINT ordered_element_question_pkey PRIMARY KEY (id);


--
-- Name: pace pace_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pace
    ADD CONSTRAINT pace_pkey PRIMARY KEY (id);


--
-- Name: peer_shuffle peer_shuffle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_shuffle
    ADD CONSTRAINT peer_shuffle_pkey PRIMARY KEY (id);


--
-- Name: peer_test_group peer_test_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group
    ADD CONSTRAINT peer_test_group_pkey PRIMARY KEY (id);


--
-- Name: peer_test_group_question peer_test_group_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question
    ADD CONSTRAINT peer_test_group_question_pkey PRIMARY KEY (id);


--
-- Name: peer_test peer_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test
    ADD CONSTRAINT peer_test_pkey PRIMARY KEY (id);


--
-- Name: perm_user_course perm_user_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perm_user_course
    ADD CONSTRAINT perm_user_course_pkey PRIMARY KEY (id);


--
-- Name: c_question_test pk_c_question_test; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_test
    ADD CONSTRAINT pk_c_question_test PRIMARY KEY (id);


--
-- Name: peer_test_group_question_calib pk_peer_test_group_question_calib; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib
    ADD CONSTRAINT pk_peer_test_group_question_calib PRIMARY KEY (id);


--
-- Name: plag_detection_algorithm plag_detection_algorithm_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plag_detection_algorithm
    ADD CONSTRAINT plag_detection_algorithm_pkey PRIMARY KEY (id);


--
-- Name: plag_detection_data plag_detection_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plag_detection_data
    ADD CONSTRAINT plag_detection_data_pkey PRIMARY KEY (id);


--
-- Name: programming_language programming_language_judge0_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language
    ADD CONSTRAINT programming_language_judge0_id_key UNIQUE (judge0_id);


--
-- Name: programming_language programming_language_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language
    ADD CONSTRAINT programming_language_name_key UNIQUE (name);


--
-- Name: programming_language programming_language_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programming_language
    ADD CONSTRAINT programming_language_pkey PRIMARY KEY (id);


--
-- Name: question_answer question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_answer
    ADD CONSTRAINT question_answer_pkey PRIMARY KEY (id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_pkey PRIMARY KEY (id);


--
-- Name: question_attachment question_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_attachment
    ADD CONSTRAINT question_attachment_pkey PRIMARY KEY (filename);


--
-- Name: question_computed_difficulty question_computed_difficulty_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_computed_difficulty
    ADD CONSTRAINT question_computed_difficulty_pkey PRIMARY KEY (id);


--
-- Name: question_course_tag question_course_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_course_tag
    ADD CONSTRAINT question_course_tag_pkey PRIMARY KEY (id);


--
-- Name: question_data_object question_data_object_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_data_object
    ADD CONSTRAINT question_data_object_pkey PRIMARY KEY (id);


--
-- Name: question_difficulty_computation question_difficulty_computation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_computation
    ADD CONSTRAINT question_difficulty_computation_pkey PRIMARY KEY (id);


--
-- Name: question_difficulty_level question_difficulty_level_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_level
    ADD CONSTRAINT question_difficulty_level_ordinal_uniq UNIQUE (id_adaptivity_model, ordinal);


--
-- Name: question_difficulty_level question_difficulty_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_level
    ADD CONSTRAINT question_difficulty_level_pkey PRIMARY KEY (id);


--
-- Name: question_eval_script question_eval_script_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_eval_script
    ADD CONSTRAINT question_eval_script_pkey PRIMARY KEY (id);


--
-- Name: question_node question_node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_node
    ADD CONSTRAINT question_node_pkey PRIMARY KEY (id);


--
-- Name: question question_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_pkey1 PRIMARY KEY (id);


--
-- Name: question_programming_language question_programming_language_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_programming_language
    ADD CONSTRAINT question_programming_language_pkey PRIMARY KEY (id_question, id_programming_language);


--
-- Name: question_review question_review_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_review
    ADD CONSTRAINT question_review_pkey PRIMARY KEY (id);


--
-- Name: question_scale question_scale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_scale
    ADD CONSTRAINT question_scale_pkey PRIMARY KEY (id);


--
-- Name: question_type question_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_type
    ADD CONSTRAINT question_type_pkey PRIMARY KEY (id);


--
-- Name: random_test random_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.random_test
    ADD CONSTRAINT random_test_pkey PRIMARY KEY (id);


--
-- Name: random_test_type random_test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.random_test_type
    ADD CONSTRAINT random_test_type_pkey PRIMARY KEY (id);


--
-- Name: real_time_plag_detection_data real_time_plag_detection_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.real_time_plag_detection_data
    ADD CONSTRAINT real_time_plag_detection_data_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: runtime_constraint_course runtime_constraint_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_course
    ADD CONSTRAINT runtime_constraint_course_pkey PRIMARY KEY (id_course, id_programming_language, id_runtime_constraint);


--
-- Name: runtime_constraint runtime_constraint_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint
    ADD CONSTRAINT runtime_constraint_name_key UNIQUE (name);


--
-- Name: runtime_constraint runtime_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint
    ADD CONSTRAINT runtime_constraint_pkey PRIMARY KEY (id);


--
-- Name: runtime_constraint_question runtime_constraint_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_question
    ADD CONSTRAINT runtime_constraint_question_pkey PRIMARY KEY (id_question, id_programming_language, id_runtime_constraint);


--
-- Name: runtime_constraint_test runtime_constraint_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_test
    ADD CONSTRAINT runtime_constraint_test_pkey PRIMARY KEY (id_c_question_test, id_programming_language, id_runtime_constraint);


--
-- Name: scale_item scale_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scale_item
    ADD CONSTRAINT scale_item_pkey PRIMARY KEY (id);


--
-- Name: scale scale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scale
    ADD CONSTRAINT scale_pkey PRIMARY KEY (id);


--
-- Name: semester semester_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.semester
    ADD CONSTRAINT semester_pkey PRIMARY KEY (id);


--
-- Name: sql_question_answer sql_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sql_question_answer
    ADD CONSTRAINT sql_question_answer_pkey PRIMARY KEY (id);


--
-- Name: student student_alt_id2_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_alt_id2_key UNIQUE (alt_id2);


--
-- Name: student student_alt_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_alt_id_key UNIQUE (alt_id);


--
-- Name: student_course student_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course
    ADD CONSTRAINT student_course_pkey PRIMARY KEY (id);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (id);


--
-- Name: student_token student_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_token
    ADD CONSTRAINT student_token_pkey PRIMARY KEY (id);


--
-- Name: teacher_lecture_quiz_instance teacher_lecture_quiz_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher_lecture_quiz_instance
    ADD CONSTRAINT teacher_lecture_quiz_instance_pkey PRIMARY KEY (id);


--
-- Name: test_correction test_correction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_correction
    ADD CONSTRAINT test_correction_pkey PRIMARY KEY (id_test_instance_question);


--
-- Name: test_instance_gen_lock test_instance_gen_lock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_gen_lock
    ADD CONSTRAINT test_instance_gen_lock_pkey PRIMARY KEY (id);


--
-- Name: test_instance test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance
    ADD CONSTRAINT test_instance_pkey PRIMARY KEY (id);


--
-- Name: test_instance_question_comp_score test_instance_question_comp_score_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_comp_score
    ADD CONSTRAINT test_instance_question_comp_score_pkey PRIMARY KEY (id);


--
-- Name: test_instance_question_generated test_instance_question_generated_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_generated
    ADD CONSTRAINT test_instance_question_generated_pkey PRIMARY KEY (id);


--
-- Name: test_instance_question_manual_grade test_instance_question_manual_grade_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_manual_grade
    ADD CONSTRAINT test_instance_question_manual_grade_pkey PRIMARY KEY (id);


--
-- Name: test_instance_question test_instance_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_pkey PRIMARY KEY (id);


--
-- Name: test_instance_question_run test_instance_question_run_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_run
    ADD CONSTRAINT test_instance_question_run_pkey PRIMARY KEY (id);


--
-- Name: test_instance_room test_instance_room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_room
    ADD CONSTRAINT test_instance_room_pkey PRIMARY KEY (id);


--
-- Name: test_message test_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_message
    ADD CONSTRAINT test_message_pkey PRIMARY KEY (id);


--
-- Name: test_pace test_pace_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_pace
    ADD CONSTRAINT test_pace_pkey PRIMARY KEY (id);


--
-- Name: test_part test_part_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part
    ADD CONSTRAINT test_part_pkey PRIMARY KEY (id);


--
-- Name: test test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_pkey PRIMARY KEY (id);


--
-- Name: test_question test_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_question
    ADD CONSTRAINT test_question_pkey PRIMARY KEY (id);


--
-- Name: test_type test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_type
    ADD CONSTRAINT test_type_pkey PRIMARY KEY (id);


--
-- Name: text_question_answer text_question_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.text_question_answer
    ADD CONSTRAINT text_question_answer_pkey PRIMARY KEY (id);


--
-- Name: ticket_message ticket_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_message
    ADD CONSTRAINT ticket_message_pkey PRIMARY KEY (id);


--
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (id);


--
-- Name: ticket_policy ticket_policy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_policy
    ADD CONSTRAINT ticket_policy_pkey PRIMARY KEY (id);


--
-- Name: ticket_subscription ticket_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_subscription
    ADD CONSTRAINT ticket_subscription_pkey PRIMARY KEY (id);


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_ordinal_uniq UNIQUE (id_step, ordinal) DEFERRABLE;


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_pkey PRIMARY KEY (id);


--
-- Name: tutorial_course tutorial_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_course
    ADD CONSTRAINT tutorial_course_pkey PRIMARY KEY (id);


--
-- Name: tutorial tutorial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial
    ADD CONSTRAINT tutorial_pkey PRIMARY KEY (id);


--
-- Name: tutorial_question_hint tutorial_question_hint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_question_hint
    ADD CONSTRAINT tutorial_question_hint_pkey PRIMARY KEY (id);


--
-- Name: tutorial_step_hint tutorial_step_hint_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step_hint
    ADD CONSTRAINT tutorial_step_hint_ordinal_uniq UNIQUE (id_step, ordinal) DEFERRABLE;


--
-- Name: tutorial_step_hint tutorial_step_hint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step_hint
    ADD CONSTRAINT tutorial_step_hint_pkey PRIMARY KEY (id);


--
-- Name: tutorial_step tutorial_step_ordinal_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_ordinal_uniq UNIQUE (id_tutorial, ordinal) DEFERRABLE;


--
-- Name: tutorial_step tutorial_step_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_pkey PRIMARY KEY (id);


--
-- Name: tutorial_student tutorial_student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student
    ADD CONSTRAINT tutorial_student_pkey PRIMARY KEY (id);


--
-- Name: tutorial_student_question_attempt tutorial_student_question_attempt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question_attempt
    ADD CONSTRAINT tutorial_student_question_attempt_pkey PRIMARY KEY (id);


--
-- Name: tutorial_student_question tutorial_student_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question
    ADD CONSTRAINT tutorial_student_question_pkey PRIMARY KEY (id);


--
-- Name: tutorial_ticket_message tutorial_ticket_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_message
    ADD CONSTRAINT tutorial_ticket_message_pkey PRIMARY KEY (id);


--
-- Name: tutorial_ticket tutorial_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket
    ADD CONSTRAINT tutorial_ticket_pkey PRIMARY KEY (id);


--
-- Name: tutorial_ticket_subscription tutorial_ticket_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_subscription
    ADD CONSTRAINT tutorial_ticket_subscription_pkey PRIMARY KEY (id);


--
-- Name: peer_test uidx_peer_test; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test
    ADD CONSTRAINT uidx_peer_test UNIQUE (id_test_phase_1, id_test_phase_2);


--
-- Name: peer_test_group_question_calib uipeer_test_group_question_calib; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib
    ADD CONSTRAINT uipeer_test_group_question_calib UNIQUE (id_peer_test_group_question, id_question_calib);


--
-- Name: upro_schedule_set_test_params upro_schedule_set_test_params_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upro_schedule_set_test_params
    ADD CONSTRAINT upro_schedule_set_test_params_pkey PRIMARY KEY (id);


--
-- Name: versions versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: logged_actions_action_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_action_idx ON audit.logged_actions USING btree (action);


--
-- Name: logged_actions_action_tstamp_tx_stm_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_action_tstamp_tx_stm_idx ON audit.logged_actions USING btree (action_tstamp_stm);


--
-- Name: logged_actions_relid_idx; Type: INDEX; Schema: audit; Owner: postgres
--

CREATE INDEX logged_actions_relid_idx ON audit.logged_actions USING btree (relid);


--
-- Name: app_user_alt_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX app_user_alt_id_idx ON public.app_user USING btree (alt_id);


--
-- Name: idx_course_tag; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_course_tag ON public.course_tag USING btree (id_course, tag);


--
-- Name: idx_test_message; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_test_message ON public.test_message USING btree (id_test, id_student);


--
-- Name: idx_ticket_message; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_ticket_message ON public.ticket_message USING btree (id_test, id_student);


--
-- Name: question_text_trgm_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX question_text_trgm_idx ON public.question USING gin (question_text public.gin_trgm_ops);


--
-- Name: test_instance_question_manual_gra_id_test_instance_question_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX test_instance_question_manual_gra_id_test_instance_question_idx ON public.test_instance_question_manual_grade USING btree (id_test_instance_question);


--
-- Name: test_instance_question_run_id_test_instance_question_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX test_instance_question_run_id_test_instance_question_idx ON public.test_instance_question_run USING btree (id_test_instance_question);


--
-- Name: uidx_app_user_alt_id2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_app_user_alt_id2 ON public.app_user USING btree (alt_id2);


--
-- Name: uidx_app_user_course; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_app_user_course ON public.student_course USING btree (id_course, id_teacher);


--
-- Name: uidx_app_user_role; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_app_user_role ON public.app_user_role USING btree (id_app_user, id_role);


--
-- Name: uidx_course_code_runner; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_course_code_runner ON public.playground_code_runner USING btree (id_course, ordinal);


--
-- Name: uidx_node_child_parent; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_node_child_parent ON public.node_parent USING btree (id_child, id_parent);


--
-- Name: uidx_peer_test_group_question; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_peer_test_group_question ON public.peer_test_group_question USING btree (id_peer_test_group, id_question);


--
-- Name: uidx_peer_test_group_question_ordinal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_peer_test_group_question_ordinal ON public.peer_test_group_question USING btree (id_peer_test_group, ordinal);


--
-- Name: uidx_perm_user_course; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_perm_user_course ON public.perm_user_course USING btree (id_user, id_course);


--
-- Name: uidx_plag_detection_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_plag_detection_data ON public.plag_detection_data USING btree (id_test, id_plag_detection_algorithm);


--
-- Name: uidx_question_node; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_question_node ON public.question_node USING btree (id_question, id_node);


--
-- Name: uidx_question_scale; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_question_scale ON public.question_scale USING btree (id_question);


--
-- Name: uidx_real_time_plag_detection_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_real_time_plag_detection_data ON public.real_time_plag_detection_data USING btree (id_test, id_question, id_plag_detection_algorithm);


--
-- Name: uidx_sql_question_answer_id_question; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_sql_question_answer_id_question ON public.sql_question_answer USING btree (id_question);


--
-- Name: uidx_student_app_user; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_student_app_user ON public.student USING btree (id_app_user);


--
-- Name: uidx_student_course; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_student_course ON public.student_course USING btree (id_academic_year, id_course, id_student);


--
-- Name: uidx_test_instance_gen_lock; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_test_instance_gen_lock ON public.test_instance_gen_lock USING btree (id_student, id_course, password);


--
-- Name: uidx_test_instance_ordinal; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_test_instance_ordinal ON public.test_instance_question USING btree (id_test_instance, ordinal);


--
-- Name: uidx_test_instance_question; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_test_instance_question ON public.test_instance_question USING btree (id_test_instance, id_question);


--
-- Name: uidx_test_instance_question_generated; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_test_instance_question_generated ON public.test_instance_question_generated USING btree (id_test_instance_question);


--
-- Name: uidx_test_password; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_test_password ON public.test USING btree (password);


--
-- Name: uidx_text_question_answer; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_text_question_answer ON public.text_question_answer USING btree (id);


--
-- Name: uidx_ticket; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_ticket ON public.ticket USING btree (id_test_instance_question);


--
-- Name: uidx_tutorial_ticket; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uidx_tutorial_ticket ON public.tutorial_ticket USING btree (id_tutorial_step, id_student);


--
-- Name: academic_year audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.academic_year FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: adaptivity_model audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.adaptivity_model FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: adaptivity_model_behaviour audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.adaptivity_model_behaviour FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: answer_regex_trigger_template audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.answer_regex_trigger_template FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.app_user FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user_course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.app_user_course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user_role audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.app_user_role FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.c_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_question_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.c_question_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_test_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.c_test_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: check_column_mode audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.check_column_mode FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: code_runner audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.code_runner FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: code_snippet audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.code_snippet FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: computer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.computer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_correctness_model audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.connected_elements_correctness_model FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.connected_elements_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.connected_elements_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course_code_runner audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.course_code_runner FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course_tag audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.course_tag FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: data_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.data_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: dendrogram_distance_algorithm audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.dendrogram_distance_algorithm FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: diagram_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.diagram_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_handled_reminders audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.email_handled_reminders FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_queue audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.email_queue FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_reminder_scheme audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.email_reminder_scheme FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.exercise FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_node audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.exercise_node FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.exercise_student FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.exercise_student_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student_question_attempt audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.exercise_student_question_attempt FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: feedback audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.feedback FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: fixed_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.fixed_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: generator_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.generator_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: generator_test_file audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.generator_test_file FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: global_data_object audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.global_data_object FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: grading_model audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.grading_model FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: j0_unit_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.j0_unit_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: json_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.json_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: local_provider audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.local_provider FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.node FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_hierarchy audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.node_hierarchy FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_parent audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.node_parent FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.node_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ordered_element_correctness_model audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ordered_element_correctness_model FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ordered_element_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ordered_element_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: pace audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.pace FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_shuffle audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.peer_shuffle FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.peer_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.peer_test_group FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.peer_test_group_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group_question_calib audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.peer_test_group_question_calib FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: perm_user_course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.perm_user_course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: plag_detection_algorithm audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.plag_detection_algorithm FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: plag_detection_data audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.plag_detection_data FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: playground_code_runner audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.playground_code_runner FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: programming_language audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.programming_language FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true', '{question_text}');


--
-- Name: question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_assigned_difficulty audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_assigned_difficulty FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_attachment audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_attachment FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_computed_difficulty audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_computed_difficulty FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_course_tag audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_course_tag FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_data_object audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_data_object FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_difficulty_computation audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_difficulty_computation FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_difficulty_level audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_difficulty_level FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_eval_script audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_eval_script FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_node audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_node FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_programming_language audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_programming_language FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_review audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_review FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_scale audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_scale FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.question_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: random_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.random_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: random_test_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.random_test_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: real_time_plag_detection_data audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.real_time_plag_detection_data FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: role audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.role FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.runtime_constraint FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.runtime_constraint_course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.runtime_constraint_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.runtime_constraint_test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: scale audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.scale FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: scale_item audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.scale_item FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: semester audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.semester FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: sql_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.sql_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.student FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student_course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.student_course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student_token audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.student_token FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: teacher_lecture_quiz_instance audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.teacher_lecture_quiz_instance FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_correction audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_correction FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_comp_score audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_question_comp_score FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_generated audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_question_generated FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_manual_grade audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_question_manual_grade FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_run audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_question_run FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_room audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_instance_room FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_message audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_message FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_pace audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_pace FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_part audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_part FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_type audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.test_type FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: text_question_answer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.text_question_answer FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ticket FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_message audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ticket_message FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_policy audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ticket_policy FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_subscription audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.ticket_subscription FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_code_question_hint audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_code_question_hint FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_course audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_course FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_question_hint audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_question_hint FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_step audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_step FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_step_hint audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_step_hint FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_student FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student_question audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_student_question FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student_question_attempt audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_student_question_attempt FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_ticket FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket_message audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_ticket_message FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket_subscription audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.tutorial_ticket_subscription FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: upro_schedule_set_test_params audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.upro_schedule_set_test_params FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: versions audit_trigger_row; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.versions FOR EACH ROW EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: academic_year audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.academic_year FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: adaptivity_model audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.adaptivity_model FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: adaptivity_model_behaviour audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.adaptivity_model_behaviour FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: answer_regex_trigger_template audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.answer_regex_trigger_template FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.app_user FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user_course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.app_user_course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: app_user_role audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.app_user_role FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.c_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_question_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.c_question_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: c_test_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.c_test_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: check_column_mode audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.check_column_mode FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: code_runner audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.code_runner FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: code_snippet audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.code_snippet FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: computer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.computer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_correctness_model audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.connected_elements_correctness_model FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.connected_elements_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: connected_elements_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.connected_elements_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course_code_runner audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.course_code_runner FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: course_tag audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.course_tag FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: data_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.data_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: dendrogram_distance_algorithm audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.dendrogram_distance_algorithm FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: diagram_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.diagram_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_handled_reminders audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.email_handled_reminders FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_queue audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.email_queue FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: email_reminder_scheme audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.email_reminder_scheme FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.exercise FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_node audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.exercise_node FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.exercise_student FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.exercise_student_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: exercise_student_question_attempt audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.exercise_student_question_attempt FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: feedback audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.feedback FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: fixed_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.fixed_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: generator_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.generator_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: generator_test_file audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.generator_test_file FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: global_data_object audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.global_data_object FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: grading_model audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.grading_model FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: j0_unit_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.j0_unit_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: json_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.json_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: local_provider audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.local_provider FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.node FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_hierarchy audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.node_hierarchy FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_parent audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.node_parent FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: node_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.node_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ordered_element_correctness_model audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ordered_element_correctness_model FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ordered_element_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ordered_element_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: pace audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.pace FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_shuffle audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.peer_shuffle FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.peer_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.peer_test_group FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.peer_test_group_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: peer_test_group_question_calib audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.peer_test_group_question_calib FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: perm_user_course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.perm_user_course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: plag_detection_algorithm audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.plag_detection_algorithm FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: plag_detection_data audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.plag_detection_data FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: playground_code_runner audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.playground_code_runner FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: programming_language audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.programming_language FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_assigned_difficulty audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_assigned_difficulty FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_attachment audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_attachment FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_computed_difficulty audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_computed_difficulty FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_course_tag audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_course_tag FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_data_object audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_data_object FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_difficulty_computation audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_difficulty_computation FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_difficulty_level audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_difficulty_level FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_eval_script audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_eval_script FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_node audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_node FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_programming_language audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_programming_language FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_review audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_review FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_scale audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_scale FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: question_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.question_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: random_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.random_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: random_test_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.random_test_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: real_time_plag_detection_data audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.real_time_plag_detection_data FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: role audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.role FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.runtime_constraint FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.runtime_constraint_course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.runtime_constraint_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: runtime_constraint_test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.runtime_constraint_test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: scale audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.scale FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: scale_item audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.scale_item FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: semester audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.semester FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: sql_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.sql_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.student FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student_course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.student_course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: student_token audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.student_token FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: teacher_lecture_quiz_instance audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.teacher_lecture_quiz_instance FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_correction audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_correction FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_comp_score audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_question_comp_score FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_generated audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_question_generated FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_manual_grade audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_question_manual_grade FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_question_run audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_question_run FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_instance_room audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_instance_room FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_message audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_message FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_pace audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_pace FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_part audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_part FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: test_type audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.test_type FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: text_question_answer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.text_question_answer FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ticket FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_message audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ticket_message FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_policy audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ticket_policy FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket_subscription audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.ticket_subscription FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_code_question_hint audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_code_question_hint FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_course audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_course FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_question_hint audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_question_hint FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_step audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_step FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_step_hint audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_step_hint FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_student FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student_question audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_student_question FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_student_question_attempt audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_student_question_attempt FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_ticket FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket_message audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_ticket_message FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: tutorial_ticket_subscription audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.tutorial_ticket_subscription FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: upro_schedule_set_test_params audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.upro_schedule_set_test_params FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: versions audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.versions FOR EACH STATEMENT EXECUTE FUNCTION audit.if_modified_func('true');


--
-- Name: ticket ins_email_queue_for_ticket_subscription; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ins_email_queue_for_ticket_subscription AFTER INSERT OR UPDATE ON public.ticket FOR EACH ROW WHEN ((new.status <> 'closed'::public.enum_ticket_status)) EXECUTE FUNCTION public.ins_email_queue_for_ticket_subcription();


--
-- Name: tutorial_ticket ins_email_queue_for_tutorial_ticket_subscription; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER ins_email_queue_for_tutorial_ticket_subscription AFTER INSERT OR UPDATE ON public.tutorial_ticket FOR EACH ROW WHEN ((new.status <> 'closed'::public.enum_ticket_status)) EXECUTE FUNCTION public.ins_email_queue_for_tutorial_ticket_subcription();


--
-- Name: academic_year stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.academic_year FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: adaptivity_model stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.adaptivity_model FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: adaptivity_model_behaviour stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.adaptivity_model_behaviour FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: answer_regex_trigger_template stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.answer_regex_trigger_template FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: app_user stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.app_user FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: app_user_course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.app_user_course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: app_user_role stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.app_user_role FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: c_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.c_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: c_question_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.c_question_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: c_test_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.c_test_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: check_column_mode stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.check_column_mode FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: code_runner stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.code_runner FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: code_snippet stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.code_snippet FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: computer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.computer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: connected_elements_correctness_model stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.connected_elements_correctness_model FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: connected_elements_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.connected_elements_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: connected_elements_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.connected_elements_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: course_code_runner stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.course_code_runner FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: course_tag stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.course_tag FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: data_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.data_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: dendrogram_distance_algorithm stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.dendrogram_distance_algorithm FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: diagram_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.diagram_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: email_handled_reminders stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.email_handled_reminders FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: email_queue stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.email_queue FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: email_reminder_scheme stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.email_reminder_scheme FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: exercise stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.exercise FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: exercise_node stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.exercise_node FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: exercise_student stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.exercise_student FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: exercise_student_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.exercise_student_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: exercise_student_question_attempt stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.exercise_student_question_attempt FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: feedback stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.feedback FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: fixed_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.fixed_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: generator_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.generator_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: generator_test_file stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.generator_test_file FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: global_data_object stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.global_data_object FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: grading_model stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.grading_model FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: j0_unit_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.j0_unit_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: json_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.json_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: local_provider stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.local_provider FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: node stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.node FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: node_hierarchy stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.node_hierarchy FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: node_parent stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.node_parent FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: node_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.node_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ordered_element_correctness_model stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ordered_element_correctness_model FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ordered_element_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ordered_element_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: pace stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.pace FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: peer_shuffle stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.peer_shuffle FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: peer_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.peer_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: peer_test_group stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.peer_test_group FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: peer_test_group_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.peer_test_group_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: peer_test_group_question_calib stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.peer_test_group_question_calib FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: perm_user_course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.perm_user_course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: plag_detection_algorithm stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.plag_detection_algorithm FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: plag_detection_data stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.plag_detection_data FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: playground_code_runner stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.playground_code_runner FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: programming_language stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.programming_language FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_assigned_difficulty stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_assigned_difficulty FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_attachment stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_attachment FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_computed_difficulty stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_computed_difficulty FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_course_tag stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_course_tag FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_data_object stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_data_object FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_difficulty_computation stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_difficulty_computation FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_difficulty_level stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_difficulty_level FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_eval_script stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_eval_script FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_node stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_node FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_programming_language stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_programming_language FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_review stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_review FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_scale stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_scale FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: question_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.question_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: random_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.random_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: random_test_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.random_test_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: real_time_plag_detection_data stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.real_time_plag_detection_data FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: role stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.role FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: runtime_constraint stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.runtime_constraint FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: runtime_constraint_course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.runtime_constraint_course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: runtime_constraint_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.runtime_constraint_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: runtime_constraint_test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.runtime_constraint_test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: scale stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.scale FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: scale_item stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.scale_item FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: semester stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.semester FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: sql_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.sql_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: student stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.student FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: student_course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.student_course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: student_token stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.student_token FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: teacher_lecture_quiz_instance stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.teacher_lecture_quiz_instance FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_correction stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_correction FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_question_comp_score stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_question_comp_score FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_question_generated stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_question_generated FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_question_manual_grade stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_question_manual_grade FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_question_run stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_question_run FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_instance_room stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_instance_room FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_message stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_message FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_pace stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_pace FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_part stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_part FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: test_type stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.test_type FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: text_question_answer stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.text_question_answer FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ticket stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ticket FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ticket_message stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ticket_message FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ticket_policy stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ticket_policy FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: ticket_subscription stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.ticket_subscription FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_code_question_hint stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_code_question_hint FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_course stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_course FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_question_hint stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_question_hint FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_step stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_step FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_step_hint stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_step_hint FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_student stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_student FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_student_question stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_student_question FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_student_question_attempt stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_student_question_attempt FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_ticket stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_ticket FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_ticket_message stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_ticket_message FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: tutorial_ticket_subscription stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.tutorial_ticket_subscription FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: upro_schedule_set_test_params stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.upro_schedule_set_test_params FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: versions stamp; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER stamp BEFORE INSERT OR UPDATE ON public.versions FOR EACH ROW EXECUTE FUNCTION audit.stamp();


--
-- Name: adaptivity_model_behaviour adaptivity_model_behaviour_id_adaptivity_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adaptivity_model_behaviour
    ADD CONSTRAINT adaptivity_model_behaviour_id_adaptivity_model_fkey FOREIGN KEY (id_adaptivity_model) REFERENCES public.adaptivity_model(id);


--
-- Name: adaptivity_model_behaviour adaptivity_model_behaviour_id_question_difficulty_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adaptivity_model_behaviour
    ADD CONSTRAINT adaptivity_model_behaviour_id_question_difficulty_level_fkey FOREIGN KEY (id_question_difficulty_level) REFERENCES public.question_difficulty_level(id);


--
-- Name: app_user_course app_user_course_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_course
    ADD CONSTRAINT app_user_course_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: app_user_course app_user_course_id_teacher_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_course
    ADD CONSTRAINT app_user_course_id_teacher_fkey FOREIGN KEY (id_teacher) REFERENCES public.app_user(id);


--
-- Name: app_user app_user_id_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user
    ADD CONSTRAINT app_user_id_role_fkey FOREIGN KEY (id_role) REFERENCES public.role(id);


--
-- Name: app_user_role app_user_role_id_app_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_role
    ADD CONSTRAINT app_user_role_id_app_user_fkey FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: app_user_role app_user_role_id_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_user_role
    ADD CONSTRAINT app_user_role_id_role_fkey FOREIGN KEY (id_role) REFERENCES public.role(id);


--
-- Name: c_question_answer c_question_answer_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_answer
    ADD CONSTRAINT c_question_answer_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: c_question_answer c_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_answer
    ADD CONSTRAINT c_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: c_question_test c_question_test_c_question_answer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_test
    ADD CONSTRAINT c_question_test_c_question_answer_id_fkey FOREIGN KEY (c_question_answer_id) REFERENCES public.c_question_answer(id);


--
-- Name: c_question_test c_question_test_c_test_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.c_question_test
    ADD CONSTRAINT c_question_test_c_test_type_id_fkey FOREIGN KEY (c_test_type_id) REFERENCES public.c_test_type(id);


--
-- Name: code_snippet code_snippet_id_code_runner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_snippet
    ADD CONSTRAINT code_snippet_id_code_runner_fkey FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: code_snippet code_snippet_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_snippet
    ADD CONSTRAINT code_snippet_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: code_snippet code_snippet_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.code_snippet
    ADD CONSTRAINT code_snippet_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: connected_elements_question_answer connected_elements_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question_answer
    ADD CONSTRAINT connected_elements_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: connected_elements_question connected_elements_question_id_connected_elements_correctn_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question
    ADD CONSTRAINT connected_elements_question_id_connected_elements_correctn_fkey FOREIGN KEY (id_connected_elements_correctness_model) REFERENCES public.connected_elements_correctness_model(id);


--
-- Name: connected_elements_question connected_elements_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.connected_elements_question
    ADD CONSTRAINT connected_elements_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: playground_code_runner course_code_runner_id_code_runner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playground_code_runner
    ADD CONSTRAINT course_code_runner_id_code_runner_fkey FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: course_code_runner course_code_runner_id_code_runner_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner
    ADD CONSTRAINT course_code_runner_id_code_runner_fkey1 FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: playground_code_runner course_code_runner_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playground_code_runner
    ADD CONSTRAINT course_code_runner_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: course_code_runner course_code_runner_id_course_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner
    ADD CONSTRAINT course_code_runner_id_course_fkey1 FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: course_code_runner course_code_runner_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner
    ADD CONSTRAINT course_code_runner_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: course_code_runner course_code_runner_id_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_code_runner
    ADD CONSTRAINT course_code_runner_id_type_fkey FOREIGN KEY (id_type) REFERENCES public.question_type(id);


--
-- Name: course_tag course_tag_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course_tag
    ADD CONSTRAINT course_tag_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: diagram_question_answer diagram_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diagram_question_answer
    ADD CONSTRAINT diagram_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: email_handled_reminders email_handled_reminders_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_handled_reminders
    ADD CONSTRAINT email_handled_reminders_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: exercise exercise_id_academic_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_id_academic_year_fkey FOREIGN KEY (id_academic_year) REFERENCES public.academic_year(id);


--
-- Name: exercise exercise_id_adaptivity_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_id_adaptivity_model_fkey FOREIGN KEY (id_adaptivity_model) REFERENCES public.adaptivity_model(id);


--
-- Name: exercise exercise_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: exercise exercise_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: exercise exercise_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise
    ADD CONSTRAINT exercise_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: exercise_node exercise_node_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_node
    ADD CONSTRAINT exercise_node_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: exercise_node exercise_node_id_node_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_node
    ADD CONSTRAINT exercise_node_id_node_fkey FOREIGN KEY (id_node) REFERENCES public.node(id);


--
-- Name: exercise_student exercise_student_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student
    ADD CONSTRAINT exercise_student_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: exercise_student exercise_student_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student
    ADD CONSTRAINT exercise_student_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: exercise_student_question_attempt exercise_student_question_attempt_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question_attempt
    ADD CONSTRAINT exercise_student_question_attempt_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: exercise_student_question_attempt exercise_student_question_attempt_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question_attempt
    ADD CONSTRAINT exercise_student_question_attempt_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: exercise_student_question_attempt exercise_student_question_attempt_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question_attempt
    ADD CONSTRAINT exercise_student_question_attempt_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: exercise_student_question exercise_student_question_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question
    ADD CONSTRAINT exercise_student_question_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: exercise_student_question exercise_student_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question
    ADD CONSTRAINT exercise_student_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: exercise_student_question exercise_student_question_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.exercise_student_question
    ADD CONSTRAINT exercise_student_question_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: feedback feedback_exercise_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_exercise_question_fkey FOREIGN KEY (id_student, id_exercise, id_question, exercise_question_ordinal) REFERENCES public.exercise_student_question(id_student, id_exercise, id_question, ordinal);


--
-- Name: feedback feedback_id_academic_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_academic_year_fkey FOREIGN KEY (id_academic_year) REFERENCES public.academic_year(id);


--
-- Name: feedback feedback_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: feedback feedback_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: feedback feedback_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: feedback feedback_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: feedback feedback_id_tutorial_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.feedback
    ADD CONSTRAINT feedback_id_tutorial_step_fkey FOREIGN KEY (id_tutorial_step) REFERENCES public.tutorial_step(id);


--
-- Name: fixed_test fk_fixed_test_c_question_test; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fixed_test
    ADD CONSTRAINT fk_fixed_test_c_question_test FOREIGN KEY (c_question_test_id) REFERENCES public.c_question_test(id);


--
-- Name: generator_test fk_generator_test_c_question_test; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test
    ADD CONSTRAINT fk_generator_test_c_question_test FOREIGN KEY (c_question_test_id) REFERENCES public.c_question_test(id);


--
-- Name: random_test fk_random_test_c_question_test; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.random_test
    ADD CONSTRAINT fk_random_test_c_question_test FOREIGN KEY (c_question_test_id) REFERENCES public.c_question_test(id);


--
-- Name: random_test fk_random_test_random_test_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.random_test
    ADD CONSTRAINT fk_random_test_random_test_type FOREIGN KEY (random_test_type_id) REFERENCES public.random_test_type(id);


--
-- Name: generator_test generator_test_generator_test_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.generator_test
    ADD CONSTRAINT generator_test_generator_test_file_id_fkey FOREIGN KEY (generator_test_file_id) REFERENCES public.generator_test_file(id);


--
-- Name: json_question_answer json_question_answer_id_code_runner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.json_question_answer
    ADD CONSTRAINT json_question_answer_id_code_runner_fkey FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: json_question_answer json_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.json_question_answer
    ADD CONSTRAINT json_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: node node_id_node_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT node_id_node_type_fkey FOREIGN KEY (id_node_type) REFERENCES public.node_type(id);


--
-- Name: node_parent node_parent_id_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_parent
    ADD CONSTRAINT node_parent_id_child_fkey FOREIGN KEY (id_child) REFERENCES public.node(id);


--
-- Name: node_parent node_parent_id_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_parent
    ADD CONSTRAINT node_parent_id_parent_fkey FOREIGN KEY (id_parent) REFERENCES public.node(id);


--
-- Name: ordered_element_question ordered_element_question_id_ordered_element_correctness_mo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_question
    ADD CONSTRAINT ordered_element_question_id_ordered_element_correctness_mo_fkey FOREIGN KEY (id_ordered_element_correctness_model) REFERENCES public.ordered_element_correctness_model(id);


--
-- Name: ordered_element_question ordered_element_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ordered_element_question
    ADD CONSTRAINT ordered_element_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: peer_shuffle peer_shuffle_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_shuffle
    ADD CONSTRAINT peer_shuffle_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: peer_shuffle peer_shuffle_id_test_phase_2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_shuffle
    ADD CONSTRAINT peer_shuffle_id_test_phase_2_fkey FOREIGN KEY (id_test_phase_2) REFERENCES public.test(id);


--
-- Name: peer_test_group peer_test_group_id_calib_assessments_node_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group
    ADD CONSTRAINT peer_test_group_id_calib_assessments_node_fkey FOREIGN KEY (id_calib_assessments_node) REFERENCES public.node(id);


--
-- Name: peer_test_group peer_test_group_id_peer_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group
    ADD CONSTRAINT peer_test_group_id_peer_test_fkey FOREIGN KEY (id_peer_test) REFERENCES public.peer_test(id);


--
-- Name: peer_test_group peer_test_group_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group
    ADD CONSTRAINT peer_test_group_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: peer_test_group_question peer_test_group_question_id_peer_test_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question
    ADD CONSTRAINT peer_test_group_question_id_peer_test_group_fkey FOREIGN KEY (id_peer_test_group) REFERENCES public.peer_test_group(id);


--
-- Name: peer_test peer_test_id_test_phase_1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test
    ADD CONSTRAINT peer_test_id_test_phase_1_fkey FOREIGN KEY (id_test_phase_1) REFERENCES public.test(id);


--
-- Name: peer_test peer_test_id_test_phase_2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test
    ADD CONSTRAINT peer_test_id_test_phase_2_fkey FOREIGN KEY (id_test_phase_2) REFERENCES public.test(id);


--
-- Name: peer_test_group_question_calib peer_tgq_calib_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib
    ADD CONSTRAINT peer_tgq_calib_id_question_fkey FOREIGN KEY (id_question_calib) REFERENCES public.question(id);


--
-- Name: peer_test_group_question_calib peer_tgq_calib_id_scale_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib
    ADD CONSTRAINT peer_tgq_calib_id_scale_item_fkey FOREIGN KEY (id_scale_item) REFERENCES public.scale_item(id);


--
-- Name: peer_test_group_question_calib peer_tgq_calib_peer_tgq_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_test_group_question_calib
    ADD CONSTRAINT peer_tgq_calib_peer_tgq_id_course_fkey FOREIGN KEY (id_peer_test_group_question) REFERENCES public.peer_test_group_question(id);


--
-- Name: perm_user_course perm_user_course_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perm_user_course
    ADD CONSTRAINT perm_user_course_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: perm_user_course perm_user_course_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perm_user_course
    ADD CONSTRAINT perm_user_course_id_user_fkey FOREIGN KEY (id_user) REFERENCES public.app_user(id);


--
-- Name: plag_detection_data plag_detection_data_id_plag_detection_algorithm_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plag_detection_data
    ADD CONSTRAINT plag_detection_data_id_plag_detection_algorithm_fkey FOREIGN KEY (id_plag_detection_algorithm) REFERENCES public.plag_detection_algorithm(id);


--
-- Name: plag_detection_data plag_detection_data_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plag_detection_data
    ADD CONSTRAINT plag_detection_data_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: question_answer question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_answer
    ADD CONSTRAINT question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_id_adaptivity_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_id_adaptivity_model_fkey FOREIGN KEY (id_adaptivity_model) REFERENCES public.adaptivity_model(id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_id_difficulty_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_id_difficulty_level_fkey FOREIGN KEY (id_difficulty_level) REFERENCES public.question_difficulty_level(id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: question_assigned_difficulty question_assigned_difficulty_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_assigned_difficulty
    ADD CONSTRAINT question_assigned_difficulty_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: question_attachment question_attachment_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_attachment
    ADD CONSTRAINT question_attachment_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_computed_difficulty question_computed_difficulty_id_question_difficulty_computation; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_computed_difficulty
    ADD CONSTRAINT question_computed_difficulty_id_question_difficulty_computation FOREIGN KEY (id_question_difficulty_computation) REFERENCES public.question_difficulty_computation(id);


--
-- Name: question_computed_difficulty question_computed_difficulty_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_computed_difficulty
    ADD CONSTRAINT question_computed_difficulty_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_course_tag question_course_tag_id_course_tag_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_course_tag
    ADD CONSTRAINT question_course_tag_id_course_tag_fkey FOREIGN KEY (id_course_tag) REFERENCES public.course_tag(id);


--
-- Name: question_course_tag question_course_tag_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_course_tag
    ADD CONSTRAINT question_course_tag_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_data_object question_data_object_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_data_object
    ADD CONSTRAINT question_data_object_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_difficulty_computation question_difficulty_computation_id_exercise_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_computation
    ADD CONSTRAINT question_difficulty_computation_id_exercise_fkey FOREIGN KEY (id_exercise) REFERENCES public.exercise(id);


--
-- Name: question_difficulty_computation question_difficulty_computation_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_computation
    ADD CONSTRAINT question_difficulty_computation_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: question_difficulty_level question_difficulty_level_id_adaptivity_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_difficulty_level
    ADD CONSTRAINT question_difficulty_level_id_adaptivity_model_fkey FOREIGN KEY (id_adaptivity_model) REFERENCES public.adaptivity_model(id);


--
-- Name: question_eval_script question_eval_script_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_eval_script
    ADD CONSTRAINT question_eval_script_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_eval_script question_eval_script_id_script_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_eval_script
    ADD CONSTRAINT question_eval_script_id_script_programming_language_fkey FOREIGN KEY (id_script_programming_language) REFERENCES public.programming_language(id);


--
-- Name: question question_id_prev_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_id_prev_question_fkey FOREIGN KEY (id_prev_question) REFERENCES public.question(id);


--
-- Name: question question_id_question_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_id_question_type_fkey FOREIGN KEY (id_question_type) REFERENCES public.question_type(id);


--
-- Name: question question_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: question question_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: question_node question_node_id_node_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_node
    ADD CONSTRAINT question_node_id_node_fkey FOREIGN KEY (id_node) REFERENCES public.node(id);


--
-- Name: question_node question_node_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_node
    ADD CONSTRAINT question_node_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_programming_language question_programming_language_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_programming_language
    ADD CONSTRAINT question_programming_language_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: question_programming_language question_programming_language_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_programming_language
    ADD CONSTRAINT question_programming_language_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_review question_review_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_review
    ADD CONSTRAINT question_review_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_review question_review_id_user_reviewed_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_review
    ADD CONSTRAINT question_review_id_user_reviewed_fkey FOREIGN KEY (id_user_reviewed) REFERENCES public.app_user(id);


--
-- Name: question_scale question_scale_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_scale
    ADD CONSTRAINT question_scale_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: question_scale question_scale_id_scale_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question_scale
    ADD CONSTRAINT question_scale_id_scale_fkey FOREIGN KEY (id_scale) REFERENCES public.scale(id);


--
-- Name: real_time_plag_detection_data real_time_plag_detection_data_id_plag_detection_algorithm_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.real_time_plag_detection_data
    ADD CONSTRAINT real_time_plag_detection_data_id_plag_detection_algorithm_fkey FOREIGN KEY (id_plag_detection_algorithm) REFERENCES public.plag_detection_algorithm(id);


--
-- Name: real_time_plag_detection_data real_time_plag_detection_data_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.real_time_plag_detection_data
    ADD CONSTRAINT real_time_plag_detection_data_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: real_time_plag_detection_data real_time_plag_detection_data_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.real_time_plag_detection_data
    ADD CONSTRAINT real_time_plag_detection_data_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: runtime_constraint_course runtime_constraint_course_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_course
    ADD CONSTRAINT runtime_constraint_course_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: runtime_constraint_course runtime_constraint_course_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_course
    ADD CONSTRAINT runtime_constraint_course_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: runtime_constraint_course runtime_constraint_course_id_runtime_constraint_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_course
    ADD CONSTRAINT runtime_constraint_course_id_runtime_constraint_fkey FOREIGN KEY (id_runtime_constraint) REFERENCES public.runtime_constraint(id);


--
-- Name: runtime_constraint runtime_constraint_id_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint
    ADD CONSTRAINT runtime_constraint_id_data_type_fkey FOREIGN KEY (id_data_type) REFERENCES public.data_type(id);


--
-- Name: runtime_constraint runtime_constraint_id_j0_unit_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint
    ADD CONSTRAINT runtime_constraint_id_j0_unit_type_fkey FOREIGN KEY (id_j0_unit_type) REFERENCES public.j0_unit_type(id);


--
-- Name: runtime_constraint_question runtime_constraint_question_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_question
    ADD CONSTRAINT runtime_constraint_question_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: runtime_constraint_question runtime_constraint_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_question
    ADD CONSTRAINT runtime_constraint_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: runtime_constraint_question runtime_constraint_question_id_runtime_constraint_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_question
    ADD CONSTRAINT runtime_constraint_question_id_runtime_constraint_fkey FOREIGN KEY (id_runtime_constraint) REFERENCES public.runtime_constraint(id);


--
-- Name: runtime_constraint_test runtime_constraint_test_id_c_question_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_test
    ADD CONSTRAINT runtime_constraint_test_id_c_question_test_fkey FOREIGN KEY (id_c_question_test) REFERENCES public.c_question_test(id);


--
-- Name: runtime_constraint_test runtime_constraint_test_id_programming_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_test
    ADD CONSTRAINT runtime_constraint_test_id_programming_language_fkey FOREIGN KEY (id_programming_language) REFERENCES public.programming_language(id);


--
-- Name: runtime_constraint_test runtime_constraint_test_id_runtime_constraint_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime_constraint_test
    ADD CONSTRAINT runtime_constraint_test_id_runtime_constraint_fkey FOREIGN KEY (id_runtime_constraint) REFERENCES public.runtime_constraint(id);


--
-- Name: scale_item scale_item_id_scale_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scale_item
    ADD CONSTRAINT scale_item_id_scale_fkey FOREIGN KEY (id_scale) REFERENCES public.scale(id);


--
-- Name: sql_question_answer sql_question_answer_id_check_column_mode_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sql_question_answer
    ADD CONSTRAINT sql_question_answer_id_check_column_mode_fkey FOREIGN KEY (id_check_column_mode) REFERENCES public.check_column_mode(id);


--
-- Name: sql_question_answer sql_question_answer_id_code_runner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sql_question_answer
    ADD CONSTRAINT sql_question_answer_id_code_runner_fkey FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: sql_question_answer sql_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sql_question_answer
    ADD CONSTRAINT sql_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: student_course student_course_id_academic_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course
    ADD CONSTRAINT student_course_id_academic_year_fkey FOREIGN KEY (id_academic_year) REFERENCES public.academic_year(id);


--
-- Name: student_course student_course_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course
    ADD CONSTRAINT student_course_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: student_course student_course_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course
    ADD CONSTRAINT student_course_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: student_course student_course_id_teacher_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_course
    ADD CONSTRAINT student_course_id_teacher_fkey FOREIGN KEY (id_teacher) REFERENCES public.app_user(id);


--
-- Name: student student_id_app_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_id_app_user_fkey FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: student_token student_token_student_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student_token
    ADD CONSTRAINT student_token_student_id_fkey FOREIGN KEY (student_id) REFERENCES public.student(id);


--
-- Name: teacher_lecture_quiz_instance teacher_lecture_quiz_instance_id_app_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher_lecture_quiz_instance
    ADD CONSTRAINT teacher_lecture_quiz_instance_id_app_user_fk FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: teacher_lecture_quiz_instance teacher_lecture_quiz_instance_id_test_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.teacher_lecture_quiz_instance
    ADD CONSTRAINT teacher_lecture_quiz_instance_id_test_fk FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: test_correction test_correction_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_correction
    ADD CONSTRAINT test_correction_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: test_correction test_correction_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_correction
    ADD CONSTRAINT test_correction_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: test test_id_academic_year_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_academic_year_fkey FOREIGN KEY (id_academic_year) REFERENCES public.academic_year(id);


--
-- Name: test test_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: test test_id_email_reminder_scheme_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_email_reminder_scheme_fkey FOREIGN KEY (id_email_reminder_scheme) REFERENCES public.email_reminder_scheme(id);


--
-- Name: test test_id_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_parent_fkey FOREIGN KEY (id_parent) REFERENCES public.test(id);


--
-- Name: test test_id_plag_detection_algorithm_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_plag_detection_algorithm_fkey FOREIGN KEY (id_plag_detection_algorithm) REFERENCES public.plag_detection_algorithm(id);


--
-- Name: test test_id_semester_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_semester_fkey FOREIGN KEY (id_semester) REFERENCES public.semester(id);


--
-- Name: test test_id_test_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_test_type_fkey FOREIGN KEY (id_test_type) REFERENCES public.test_type(id);


--
-- Name: test test_id_ticket_policy_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_ticket_policy_fkey FOREIGN KEY (id_ticket_policy) REFERENCES public.ticket_policy(id);


--
-- Name: test test_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: test_instance test_instance_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance
    ADD CONSTRAINT test_instance_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: test_instance test_instance_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance
    ADD CONSTRAINT test_instance_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: test_instance_question_comp_score test_instance_question_comp_scor_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_comp_score
    ADD CONSTRAINT test_instance_question_comp_scor_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: test_instance_question_generated test_instance_question_generated_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_generated
    ADD CONSTRAINT test_instance_question_generated_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: test_instance_question test_instance_question_id_grading_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_id_grading_model_fkey FOREIGN KEY (id_grading_model) REFERENCES public.grading_model(id);


--
-- Name: test_instance_question test_instance_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: test_instance_question test_instance_question_id_test_instance_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_id_test_instance_fkey FOREIGN KEY (id_test_instance) REFERENCES public.test_instance(id);


--
-- Name: test_instance_question test_instance_question_id_test_part_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_id_test_part_fkey FOREIGN KEY (id_test_part) REFERENCES public.test_part(id);


--
-- Name: test_instance_question_manual_grade test_instance_question_manual_gr_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_manual_grade
    ADD CONSTRAINT test_instance_question_manual_gr_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: test_instance_question_manual_grade test_instance_question_manual_grade_id_app_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_manual_grade
    ADD CONSTRAINT test_instance_question_manual_grade_id_app_user_fkey FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: test_instance_question_run test_instance_question_run_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question_run
    ADD CONSTRAINT test_instance_question_run_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: test_instance_question test_instance_question_student_answer_code_pl_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_question
    ADD CONSTRAINT test_instance_question_student_answer_code_pl_fkey FOREIGN KEY (student_answer_code_pl) REFERENCES public.programming_language(id);


--
-- Name: test_instance_room test_instance_room_id_teacher_instance_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_room
    ADD CONSTRAINT test_instance_room_id_teacher_instance_fk FOREIGN KEY (id_teacher_instance) REFERENCES public.teacher_lecture_quiz_instance(id);


--
-- Name: test_instance_room test_instance_room_id_test_instance_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_instance_room
    ADD CONSTRAINT test_instance_room_id_test_instance_fk FOREIGN KEY (id_test_instance) REFERENCES public.test_instance(id);


--
-- Name: test_message test_message_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_message
    ADD CONSTRAINT test_message_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: test_message test_message_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_message
    ADD CONSTRAINT test_message_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: test_pace test_pace_id_pace_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_pace
    ADD CONSTRAINT test_pace_id_pace_question_fkey FOREIGN KEY (id_pace_question) REFERENCES public.pace(id);


--
-- Name: test_pace test_pace_id_pace_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_pace
    ADD CONSTRAINT test_pace_id_pace_test_fkey FOREIGN KEY (id_pace_test) REFERENCES public.pace(id);


--
-- Name: test_pace test_pace_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_pace
    ADD CONSTRAINT test_pace_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: test_part test_part_id_grading_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part
    ADD CONSTRAINT test_part_id_grading_model_fkey FOREIGN KEY (id_grading_model) REFERENCES public.grading_model(id);


--
-- Name: test_part test_part_id_node_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part
    ADD CONSTRAINT test_part_id_node_fkey FOREIGN KEY (id_node) REFERENCES public.node(id);


--
-- Name: test_part test_part_id_question_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part
    ADD CONSTRAINT test_part_id_question_type_fkey FOREIGN KEY (id_question_type) REFERENCES public.question_type(id);


--
-- Name: test_part test_part_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_part
    ADD CONSTRAINT test_part_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: test_question test_question_id_grading_model_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_question
    ADD CONSTRAINT test_question_id_grading_model_fk FOREIGN KEY (id_grading_model) REFERENCES public.grading_model(id);


--
-- Name: test_question test_question_id_question_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_question
    ADD CONSTRAINT test_question_id_question_fk FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: test_question test_question_id_test_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test_question
    ADD CONSTRAINT test_question_id_test_fk FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: text_question_answer text_question_answer_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.text_question_answer
    ADD CONSTRAINT text_question_answer_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: ticket ticket_id_assigned_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_id_assigned_user_fkey FOREIGN KEY (id_assigned_user) REFERENCES public.app_user(id);


--
-- Name: ticket ticket_id_test_instance_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket
    ADD CONSTRAINT ticket_id_test_instance_question_fkey FOREIGN KEY (id_test_instance_question) REFERENCES public.test_instance_question(id);


--
-- Name: ticket_message ticket_message_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_message
    ADD CONSTRAINT ticket_message_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: ticket_message ticket_message_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_message
    ADD CONSTRAINT ticket_message_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: ticket_subscription ticket_subscription_id_app_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_subscription
    ADD CONSTRAINT ticket_subscription_id_app_user_fkey FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: ticket_subscription ticket_subscription_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_subscription
    ADD CONSTRAINT ticket_subscription_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: ticket_subscription ticket_subscription_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_subscription
    ADD CONSTRAINT ticket_subscription_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_id_step; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_id_step FOREIGN KEY (id_step) REFERENCES public.tutorial_step(id);


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: tutorial_code_question_hint tutorial_code_question_hint_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_code_question_hint
    ADD CONSTRAINT tutorial_code_question_hint_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: tutorial_course tutorial_course_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_course
    ADD CONSTRAINT tutorial_course_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: tutorial_course tutorial_course_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_course
    ADD CONSTRAINT tutorial_course_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: tutorial tutorial_id_node_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial
    ADD CONSTRAINT tutorial_id_node_fkey FOREIGN KEY (id_node) REFERENCES public.node(id);


--
-- Name: tutorial tutorial_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial
    ADD CONSTRAINT tutorial_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: tutorial tutorial_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial
    ADD CONSTRAINT tutorial_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: tutorial_question_hint tutorial_question_hint_id_answer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_question_hint
    ADD CONSTRAINT tutorial_question_hint_id_answer_fkey FOREIGN KEY (id_question_answer) REFERENCES public.question_answer(id);


--
-- Name: tutorial_question_hint tutorial_question_hint_id_step; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_question_hint
    ADD CONSTRAINT tutorial_question_hint_id_step FOREIGN KEY (id_step) REFERENCES public.tutorial_step(id);


--
-- Name: tutorial_question_hint tutorial_question_hint_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_question_hint
    ADD CONSTRAINT tutorial_question_hint_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: tutorial_question_hint tutorial_question_hint_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_question_hint
    ADD CONSTRAINT tutorial_question_hint_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: tutorial_step_hint tutorial_step_hint_id_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step_hint
    ADD CONSTRAINT tutorial_step_hint_id_step_fkey FOREIGN KEY (id_step) REFERENCES public.tutorial_step(id);


--
-- Name: tutorial_step_hint tutorial_step_hint_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step_hint
    ADD CONSTRAINT tutorial_step_hint_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: tutorial_step_hint tutorial_step_hint_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step_hint
    ADD CONSTRAINT tutorial_step_hint_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: tutorial_step tutorial_step_id_code_runner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_id_code_runner_fkey FOREIGN KEY (id_code_runner) REFERENCES public.code_runner(id);


--
-- Name: tutorial_step tutorial_step_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: tutorial_step tutorial_step_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: tutorial_step tutorial_step_id_user_created_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_id_user_created_fkey FOREIGN KEY (id_user_created) REFERENCES public.app_user(id);


--
-- Name: tutorial_step tutorial_step_id_user_modified_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_step
    ADD CONSTRAINT tutorial_step_id_user_modified_fkey FOREIGN KEY (id_user_modified) REFERENCES public.app_user(id);


--
-- Name: tutorial_student tutorial_student_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student
    ADD CONSTRAINT tutorial_student_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: tutorial_student tutorial_student_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student
    ADD CONSTRAINT tutorial_student_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: tutorial_student tutorial_student_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student
    ADD CONSTRAINT tutorial_student_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: tutorial_student_question_attempt tutorial_student_question_attempt_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question_attempt
    ADD CONSTRAINT tutorial_student_question_attempt_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: tutorial_student_question_attempt tutorial_student_question_attempt_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question_attempt
    ADD CONSTRAINT tutorial_student_question_attempt_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: tutorial_student_question_attempt tutorial_student_question_attempt_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question_attempt
    ADD CONSTRAINT tutorial_student_question_attempt_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: tutorial_student_question_attempt tutorial_student_question_attempt_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question_attempt
    ADD CONSTRAINT tutorial_student_question_attempt_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: tutorial_student_question tutorial_student_question_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question
    ADD CONSTRAINT tutorial_student_question_id_course_fkey FOREIGN KEY (id_course) REFERENCES public.course(id);


--
-- Name: tutorial_student_question tutorial_student_question_id_question_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question
    ADD CONSTRAINT tutorial_student_question_id_question_fkey FOREIGN KEY (id_question) REFERENCES public.question(id);


--
-- Name: tutorial_student_question tutorial_student_question_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question
    ADD CONSTRAINT tutorial_student_question_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: tutorial_student_question tutorial_student_question_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_student_question
    ADD CONSTRAINT tutorial_student_question_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: tutorial_ticket tutorial_ticket_id_assigned_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket
    ADD CONSTRAINT tutorial_ticket_id_assigned_user_fkey FOREIGN KEY (id_assigned_user) REFERENCES public.app_user(id);


--
-- Name: tutorial_ticket tutorial_ticket_id_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket
    ADD CONSTRAINT tutorial_ticket_id_student_fkey FOREIGN KEY (id_student) REFERENCES public.student(id);


--
-- Name: tutorial_ticket tutorial_ticket_id_tutorial_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket
    ADD CONSTRAINT tutorial_ticket_id_tutorial_step_fkey FOREIGN KEY (id_tutorial_step) REFERENCES public.tutorial_step(id);


--
-- Name: tutorial_ticket_message tutorial_ticket_message_id_tutorial_ticket_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_message
    ADD CONSTRAINT tutorial_ticket_message_id_tutorial_ticket_fkey FOREIGN KEY (id_tutorial_ticket) REFERENCES public.tutorial_ticket(id);


--
-- Name: tutorial_ticket_subscription tutorial_ticket_subscription_id_app_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_subscription
    ADD CONSTRAINT tutorial_ticket_subscription_id_app_user_fkey FOREIGN KEY (id_app_user) REFERENCES public.app_user(id);


--
-- Name: tutorial_ticket_subscription tutorial_ticket_subscription_id_tutorial_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutorial_ticket_subscription
    ADD CONSTRAINT tutorial_ticket_subscription_id_tutorial_fkey FOREIGN KEY (id_tutorial) REFERENCES public.tutorial(id);


--
-- Name: upro_schedule_set_test_params upro_schedule_set_test_params_id_test_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.upro_schedule_set_test_params
    ADD CONSTRAINT upro_schedule_set_test_params_id_test_fkey FOREIGN KEY (id_test) REFERENCES public.test(id);


--
-- PostgreSQL database dump complete
--

