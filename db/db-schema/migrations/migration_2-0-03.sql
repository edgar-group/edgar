BEGIN WORK;


create or replace function save_in_place_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint,
    _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _initial_diagram_answer text
    , _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text,
    _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text
    , _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text
    , _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text,
    _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text,
     _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text,
      _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text,
       _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer,
        _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer, _setup_script text, _test_cases jsonb, _bump_version boolean) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _arr_answers text[];
    _arr_answers_static text[];
    _arr_answers_dynamic text[];
    _arr_correct_answers integer[];
    _arr_threshold_weights integer[];
    _arr_penalty_percentages DECIMAL(6, 2)[];
    i INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    _id_question_type INTEGER;
    DELIMITER CONSTANT TEXT := '~~#~~';
BEGIN
  SELECT has_answers, id_user_created, id_question_type
    INTO _has_answers, _id_user_created, _id_question_type
    FROM question
    JOIN question_type ON question.id_question_type = question_type.id
   WHERE question.id = _id;

    -- Update in place:
    UPDATE question
    SET question_text = _question_text,
        question_comment = _question_comment,
        upload_files_no = _upload_files_no,
        required_upload_files_no = _required_upload_files_no,
        is_active = _is_active,
        id_user_modified = _id_user_modified,
        grader_object = _grader_object,
        time_limit = _time_limit,
        version = CASE _bump_version
                    WHEN TRUE THEN version + 1
                    ELSE version
                  END
    WHERE id = _id;

    DELETE FROM question_data_object WHERE id_question = _id;
    IF (LENGTH(TRIM(_data_object)) > 0) THEN
      INSERT INTO question_data_object (id_question, data_object) VALUES (_id, _data_object);
    END IF;

    DELETE FROM question_eval_script WHERE id_question = _id;
    IF (LENGTH(TRIM(_eval_script)) > 0) THEN
      INSERT INTO question_eval_script (id_question, id_script_programming_language, eval_script)
      VALUES (_id, _id_script_programming_language, _eval_script);
    END IF;


    IF (_has_answers) THEN
        _arr_answers           = string_to_array(_answers_texts, DELIMITER);
        _arr_answers_static    = string_to_array(_answer_text_static, DELIMITER);
        _arr_answers_dynamic   = string_to_array(_answer_text_dynamic, DELIMITER);
        _arr_threshold_weights = string_to_array(_threshold_weights, DELIMITER);
        _arr_penalty_percentages = string_to_array(_penalty_percentages, DELIMITER);
        _arr_correct_answers   = string_to_array(_correct_answers, DELIMITER);

        IF (   array_lower(_arr_answers, 1) <> array_lower(_arr_threshold_weights, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_correct_answers, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_penalty_percentages, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_threshold_weights, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_correct_answers, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_penalty_percentages, 1)
            ) THEN
            RAISE EXCEPTION 'Bad arguments for multiple choice question (all arrays must be of the same size).';
        END IF;

        IF (_id_question_type = 9) THEN -- Connected elements
            DELETE FROM connected_elements_question_answer WHERE id_question = _id;
            FOR i in array_lower(_arr_answers_static, 1)..array_upper(_arr_answers_static, 1) loop
                INSERT INTO connected_elements_question_answer(id_question, ordinal, answer_text_static, answer_text_dynamic)
                     VALUES (_id, i,_arr_answers_static[i]::TEXT, _arr_answers_dynamic[i]::TEXT);
            END LOOP;
            UPDATE connected_elements_question
               SET id_connected_elements_correctness_model = _id_connected_elements_correctness_model
             WHERE id_question = _id;
        ELSE  -- Multiple choice and ordered elements
            DELETE FROM question_answer WHERE id_question = _id;
            FOR i in array_lower(_arr_answers, 1)..array_upper(_arr_answers, 1) loop
                INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage)
                     VALUES (_id, i, _arr_correct_answers[i]::BOOLEAN, _arr_answers[i]::TEXT, _arr_threshold_weights[i]::SMALLINT, _arr_penalty_percentages[i]::DECIMAL(6,2));
            END LOOP;
        END IF;
        IF (_id_question_type = 8) THEN -- Additionally for ordered elements
            UPDATE ordered_element_question
               SET id_ordered_element_correctness_model = _id_ordered_element_correctness_model,
                   display_option = _display_option
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL (2) and C
        IF (_id_question_type = 2) THEN  -- Evaluated SQL question
            UPDATE sql_question_answer
            SET sql_answer = _sql_answer,
                sql_alt_assertion = _sql_alt_assertion,
                sql_test_fixture = _sql_test_fixture,
                sql_alt_presentation_query = _sql_alt_presentation_query,
                check_tuple_order = _check_tuple_order,
                id_check_column_mode = _id_check_column_mode
            WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN  -- Code questions, 3 and 22 are legacy
            PERFORM update_c_question(
                _id, _c_prefix, _c_suffix, _c_source, _c_test_type_ids,
                _c_percentages, _c_allow_diff_orders, _c_allow_diff_letter_sizes,
                _c_trim_whitespaces, _c_comments, _c_regexs, _c_question_test_ids,
                _c_inputs, _c_outputs, _c_generator_test_file_ids,
                _c_argumentss, _c_random_test_type_ids, _c_low_bounds,
                _c_upper_bounds, _c_elem_counts, _c_programming_language_id, _c_rtc_question,
                                                        _c_rtc_test, _c_is_public);
        ELSIF (_id_question_type = 11) THEN
            -- Diagrams
            UPDATE diagram_question_answer
                SET initial_diagram_answer = _initial_diagram_answer,
                    diagram_answer = _diagram_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            -- Free text
            UPDATE text_question_answer
                SET text_answer = _text_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 25) THEN -- Peer assessment calibration question
                -- it's ok, nothing more to do here...
        ELSIF (_id_question_type = 21) THEN
                -- JSON result question
                UPDATE json_question_answer
                SET json_answer = _json_answer,
                    json_alt_assertion = _json_alt_assertion,
                    json_test_fixture = _json_test_fixture,
                    json_alt_presentation_query = _json_alt_presentation_query,
                    assert_deep = _assert_deep,
                    assert_strict = _assert_strict
                WHERE id_question = _id;
        ELSIF (_id_question_type = 26) THEN
                -- Scale
                DELETE FROM question_scale WHERE id_question = _id;
                INSERT INTO question_scale (id_question, id_scale) VALUES (_id, _id_scale);
        ELSIF (_id_question_type = 30) THEN
                -- Complex
                UPDATE complex_question
                   SET setup_script = _setup_script,
                       test_cases = _test_cases
                 WHERE id_question = _id;
        ELSE
                RAISE EXCEPTION 'Unknown question type';
        END IF;
    END IF;

  PERFORM set_question_tags(_id, _tags);
  RETURN _id;
END;
$$;

create or replace function update_c_question(_id integer, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text,
    _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text
    , _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _c_programming_language_id text, _c_rtc_question text, _c_rtc_test text, _c_is_public text) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    -- C stuff:
    _id_question_type INTEGER;
    _c_question_answer_id INTEGER;
    -- _arr_c_question_test_ids integer[];
    _arr_c_percentages numeric[];
    _arr_c_allow_diff_orders boolean[];
    _arr_c_allow_diff_letter_sizes boolean[];
    _arr_c_trim_whitespaces boolean[];
    _arr_c_comments text[];
    _arr_c_regexs text[];
    _arr_c_test_type_ids integer[];
    _arr_c_inputs text[];
    _arr_c_outputs text[];
    -- _arr_c_generator_test_file_ids integer[];
    -- _arr_c_argumentss text[];
    _arr_c_random_test_type_ids integer[];
    _arr_c_low_bounds numeric[];
    _arr_c_upper_bounds numeric[];
    _arr_c_elem_counts integer[];
    -- old_test_type_id integer;
    _arr_c_is_public boolean[];
    -- /C stuff
    _new_c_question_test_id INTEGER;
    c_rtc_tests_ids int[];
BEGIN
    -- C stuff
    UPDATE c_question_answer
        SET c_prefix = _c_prefix,
            c_suffix = _c_suffix,
            c_source = _c_source,
            id_programming_language = _c_programming_language_id::INTEGER
    WHERE id_question = _id;

    SELECT id INTO _c_question_answer_id
      FROM c_question_answer
    WHERE c_question_answer.id_question = _id;
    -- _c_question_test_ids IS IGNORED, KEPT IT FOR BACKWARD COMPATIBILITY
    -- _arr_c_question_test_ids = string_to_array(_c_question_test_ids, '~~#~~');
    _arr_c_percentages = emptyArrayElem2Zero(string_to_array(_c_percentages, '~~#~~'));
    _arr_c_allow_diff_orders = string_to_array(_c_allow_diff_orders, '~~#~~');
    _arr_c_allow_diff_letter_sizes = string_to_array(_c_allow_diff_letter_sizes, '~~#~~');
    _arr_c_trim_whitespaces = string_to_array(_c_trim_whitespaces, '~~#~~');
    _arr_c_comments = string_to_array(_c_comments, '~~#~~');
    _arr_c_regexs = string_to_array(_c_regexs, '~~#~~');
    _arr_c_test_type_ids = string_to_array(_c_test_type_ids, '~~#~~');
    _arr_c_inputs = string_to_array(_c_inputs, '~~#~~');
    _arr_c_outputs  = string_to_array(_c_outputs, '~~#~~');
    -- _arr_c_generator_test_file_ids  = emptyArrayElem2Zero(string_to_array(_c_generator_test_file_ids, '~~#~~'));
    -- _arr_c_argumentss  = string_to_array(_c_argumentss, '~~#~~');
    _arr_c_random_test_type_ids  = emptyArrayElem2Zero(string_to_array(_c_random_test_type_ids, '~~#~~'));
    _arr_c_low_bounds  = emptyArrayElem2Zero(string_to_array(_c_low_bounds, '~~#~~'));
    _arr_c_upper_bounds  = emptyArrayElem2Zero(string_to_array(_c_upper_bounds, '~~#~~'));
    _arr_c_elem_counts  = emptyArrayElem2Zero(string_to_array(_c_elem_counts, '~~#~~'));
    _arr_c_is_public = string_to_array(_c_is_public, '~~#~~');

    -- delete all tests, details:
    DELETE FROM fixed_test  WHERE c_question_test_id IN (SELECT id FROM c_question_test WHERE c_question_answer_id = _c_question_answer_id);
    DELETE FROM random_test WHERE c_question_test_id IN (SELECT id FROM c_question_test WHERE c_question_answer_id = _c_question_answer_id);
    DELETE FROM runtime_constraint_test WHERE id_c_question_test IN (SELECT id FROM c_question_test WHERE c_question_answer_id = _c_question_answer_id);
    -- delete all tests, head:
    DELETE FROM c_question_test WHERE c_question_answer_id = _c_question_answer_id;  -- delete all tests

    FOR i in 1..array_length(_arr_c_is_public, 1) LOOP
        INSERT INTO c_question_test (c_question_answer_id, c_test_type_id, percentage,
                                      allow_diff_order, allow_diff_letter_size, allow_subset, comment,
                                      regex_override, trim_whitespace, is_public, ordinal)
          VALUES(_c_question_answer_id, _arr_c_test_type_ids[i]::INTEGER, _arr_c_percentages[i]::NUMERIC(6,2),
                _arr_c_allow_diff_orders[i]::BOOLEAN, _arr_c_allow_diff_letter_sizes[i]::BOOLEAN, false, _arr_c_comments[i]::TEXT,
                _arr_c_regexs[i]::TEXT, _arr_c_trim_whitespaces[i]::BOOLEAN, _arr_c_is_public[i]::BOOLEAN, i)
        RETURNING id INTO _new_c_question_test_id;
        c_rtc_tests_ids = c_rtc_tests_ids || _new_c_question_test_id;
        CASE
          WHEN (_arr_c_test_type_ids[i]::INTEGER = 1) THEN
              INSERT INTO fixed_test (c_question_test_id , input , output  )
              VALUES (_new_c_question_test_id, _arr_c_inputs[i]::TEXT , _arr_c_outputs[i]::TEXT ) ;
          WHEN (_arr_c_test_type_ids[i]::INTEGER = 2) THEN
              INSERT INTO random_test (c_question_test_id , random_test_type_id , low_bound , upper_bound , elem_count  )
              VALUES (_new_c_question_test_id, _arr_c_random_test_type_ids[i]::INTEGER ,
                  _arr_c_low_bounds[i]::NUMERIC(13,3) , _arr_c_upper_bounds[i]::NUMERIC(13,3) ,
                  _arr_c_elem_counts[i]::INTEGER  );
          ELSE
              RAISE EXCEPTION 'Unknown type of _arr_c_test_type_ids[i] is not a valid id  ';
        END CASE;


    END LOOP;
    PERFORM set_runtime_constraints2(_c_rtc_question, _c_rtc_test, _id, c_rtc_tests_ids);
    RETURN _id;
END;
$$;


create or replace function set_runtime_constraints2(c_rtc_question text, c_rtc_tests text, _id_question integer, c_rtc_tests_ids integer[]) returns void
    security definer
    language plpgsql
as
$$
DECLARE
    _cursor_rtc_question CURSOR FOR SELECT * FROM json_to_recordset(c_rtc_question::JSON) as x(id_question int, id_language int, id_rtc int, rtc_value text);
    _cursor_rtc_tests CURSOR FOR SELECT * FROM json_to_recordset(c_rtc_tests::JSON) as y(id_test int, id_language int, id_rtc int, rtc_value text, ordinal int);
BEGIN
    DELETE FROM runtime_constraint_question WHERE id_question = _id_question; -- AND id_programming_language = item.id_language AND id_runtime_constraint = item.id_rtc;
    FOR item IN _cursor_rtc_question LOOP
        IF (item.rtc_value = '') IS NOT TRUE THEN
            INSERT INTO runtime_constraint_question (id_question, id_programming_language, id_runtime_constraint, override_value)
                VALUES (_id_question, item.id_language, item.id_rtc, item.rtc_value);
        END IF;
    END LOOP;

    DELETE FROM runtime_constraint_test   -- delete them all
        WHERE id_c_question_test
          IN (SELECT c_question_test.id from c_question_test JOIN c_question_answer ON c_question_test.c_question_answer_id = c_question_answer.id
          where id_question = _id_question);  -- = item.id_test AND id_programming_language = item.id_language AND id_runtime_constraint = item.id_rtc;
    FOR item IN _cursor_rtc_tests LOOP
        IF (item.rtc_value = '') IS NOT TRUE THEN
            INSERT INTO runtime_constraint_test (id_c_question_test, id_programming_language, id_runtime_constraint, override_value)
                VALUES (c_rtc_tests_ids[item.ordinal], item.id_language, item.id_rtc, item.rtc_value);
        END if;
    END LOOP;
END;
$$;

alter function set_runtime_constraints2(text, text, integer, integer[]) owner to postgres;


alter table programming_language add id_question_type int not null default 23 references question_type(id);

COMMIT WORK;
