BEGIN WORK;


ALTER TABLE question ADD COLUMN upload_files_no SMALLINT DEFAULT 0 NOT NULL;
ALTER TABLE question ADD COLUMN required_upload_files_no SMALLINT DEFAULT 0 NOT NULL;
UPDATE question SET upload_files_no = 1 WHERE can_upload = TRUE;
ALTER TABLE question drop COLUMN can_upload;

ALTER TABLE question ADD CONSTRAINT chk_upload_files_no_beween_0_and_5 CHECK (upload_files_no BETWEEN 0 and 5);
ALTER TABLE question ADD CONSTRAINT chk_required_upload_files_no_less_than_upload_files_no CHECK (required_upload_files_no BETWEEN 0 AND upload_files_no);

UPDATE test SET upload_file_limit = 0 WHERE upload_file_no = 0;
ALTER TABLE test ALTER COLUMN upload_file_limit SET DEFAULT 0;

DROP VIEW public.v_test_stats;
CREATE VIEW public.v_test_stats (id, title, id_course, id_academic_year, id_semester, id_test_type, id_user_created, test_ordinal, max_runs,
             show_solutions, max_score, password, questions_no, duration_seconds, pass_percentage, ts_available_from,
             ts_available_to, ts_created, ts_modified, user_modified, review_period_mins, hint_result,
             test_score_ignored, title_abbrev, async_submit, trim_clock, id_email_reminder_scheme, allow_anonymous,
             is_competition, eval_comp_score, upload_file_limit, forward_only, id_parent,
             allow_anonymous_stalk, use_in_stats, is_global, is_public)
as
SELECT test.id,
       test.title,
       test.id_course,
       test.id_academic_year,
       test.id_semester,
       test.id_test_type,
       test.id_user_created,
       test.test_ordinal,
       test.max_runs,
       test.show_solutions,
       test.max_score,
       test.password,
       test.questions_no,
       test.duration_seconds,
       test.pass_percentage,
       test.ts_available_from,
       test.ts_available_to,
       test.ts_created,
       test.ts_modified,
       test.user_modified,
       test.review_period_mins,
       test.hint_result,
       test.test_score_ignored,
       test.title_abbrev,
       test.async_submit,
       test.trim_clock,
       test.id_email_reminder_scheme,
       test.allow_anonymous,
       test.is_competition,
       test.eval_comp_score,
       test.upload_file_limit,
       test.forward_only,
       test.id_parent,
       test.allow_anonymous_stalk,
       test.use_in_stats,
       test.is_global,
       test.is_public
FROM test
JOIN test_type ON test.id_test_type = test_type.id
 AND test_type.type_name::text <> 'Lecture quiz'::text
WHERE NOT test.test_score_ignored
  AND test.use_in_stats
  AND test.ts_available_from <= CURRENT_TIMESTAMP;

GRANT SELECT on public.v_test_stats to edgar;
GRANT SELECT on public.v_test_stats to ro;

ALTER TABLE test drop COLUMN upload_file_no;

DROP FUNCTION save_question(integer, boolean, boolean, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer);

CREATE FUNCTION save_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _inital_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    i INTEGER;
    _new_id INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    -- C stuff:
    _id_question_type INTEGER;
    -- /C stuff

BEGIN
    SELECT  has_answers,  id_user_created,  id_question_type
      INTO _has_answers, _id_user_created, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
    WHERE question.id = _id;

    IF (SELECT COUNT(*) FROM test_instance_question WHERE id_question = _id) = 0 THEN
    -- Virgin, update in place:
        RETURN public.save_in_place_question(
               _id,
               _is_active,
               _upload_files_no,
               _required_upload_files_no,
               _question_text,
               _display_option,
               _time_limit,
               _question_comment,
               _text_answer,
               _inital_diagram_answer,
               _diagram_answer,
               _correct_answers,
               _answers_texts,
               _answer_text_static,
               _answer_text_dynamic,
               _threshold_weights,
               _penalty_percentages,
               _sql_answer,
               _sql_alt_assertion,
               _sql_test_fixture,
               _sql_alt_presentation_query,
               _check_tuple_order,
               _id_check_column_mode,
               _c_prefix,
               _c_suffix,
               _c_source,
               _c_test_type_ids,
               _c_percentages,
               _c_allow_diff_orders,
               _c_allow_diff_letter_sizes,
               _c_trim_whitespaces,
               _c_comments,
               _c_regexs,
               _c_question_test_ids,
               _c_inputs,
               _c_outputs,
               _c_generator_test_file_ids,
               _c_argumentss,
               _c_random_test_type_ids,
               _c_low_bounds,
               _c_upper_bounds,
               _c_elem_counts,
               _id_user_modified,
               _tags,
               _json_answer,
               _json_alt_assertion,
               _json_test_fixture,
               _json_alt_presentation_query,
               _assert_deep,
               _assert_strict,

               _c_rtc_question,
               _c_rtc_test,
               _c_programming_language_id,
               _c_is_public,
               _id_scale,
               _data_object,
               _eval_script,
               _id_script_programming_language,
               _grader_object,
               _id_ordered_element_correctness_model,
               _id_connected_elements_correctness_model,
               FALSE
        );
    ELSE
    -- Referenced question, close it (active) and leave content (keep history) and instantiate new question:
        UPDATE question
        SET is_active = false,
            id_user_modified = _id_user_modified
        WHERE id = _id;

        SELECT public.clone_question(_id, _id_user_created, _id_user_modified, true) INTO _new_id;
        -- Update new (virgin) question:
        RETURN public.save_question(
            _new_id,
            _is_active,
            _upload_files_no,
            _required_upload_files_no,
            _question_text,
            _display_option,
            _time_limit,
            _question_comment,
            _text_answer,
            _inital_diagram_answer,
            _diagram_answer,
            _correct_answers, --CSVs
            _answers_texts,  -- CSVs
            _answer_text_static,
            _answer_text_dynamic,
            _threshold_weights,  -- CSVs
            _penalty_percentages, -- CSVs
            _sql_answer,
            _sql_alt_assertion,
            _sql_test_fixture,
            _sql_alt_presentation_query,
            _check_tuple_order,
            _id_check_column_mode,
            _c_prefix,
            _c_suffix,
            _c_source,
            _c_test_type_ids,
            _c_percentages,
            _c_allow_diff_orders,
            _c_allow_diff_letter_sizes,
            _c_trim_whitespaces,
            _c_comments,
            _c_regexs,
            --// TODO: this sucks, bcs there is no "ordinal" attribute in the c_qustion_test, I have to go out on a lim with order by ID
            (SELECT array_to_string(array(
              SELECT id
                FROM c_question_test
              WHERE c_question_answer_id = (SELECT id FROM c_question_answer WHERE id_question = _new_id)
              ORDER BY id), '~~#~~')),
            _c_inputs,
            _c_outputs,
            _c_generator_test_file_ids,
            _c_argumentss,
            _c_random_test_type_ids,
            _c_low_bounds,
            _c_upper_bounds,
            _c_elem_counts,
            _id_user_modified,
            _tags,
            _json_answer,
            _json_alt_assertion,
            _json_test_fixture,
            _json_alt_presentation_query,
            _assert_deep,
            _assert_strict,
            _c_rtc_question,
            _c_rtc_test,
            _c_programming_language_id,
            _c_is_public,
            _id_scale,
            _data_object,
            _eval_script,
            _id_script_programming_language,
            _grader_object,
            _id_ordered_element_correctness_model,
            _id_connected_elements_correctness_model
        );
    END IF;

END;
$$;
GRANT EXECUTE ON FUNCTION save_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer) to edgar;





DROP FUNCTION save_in_place_question(integer, boolean, boolean, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer, boolean);
CREATE FUNCTION save_in_place_question(_id integer, _is_active boolean,  _upload_files_no smallint, _required_upload_files_no smallint, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text, _initial_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer, _bump_version boolean) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _arr_answers text[];
    _arr_answers_static text[];
    _arr_answers_dynamic text[];
    _arr_correct_answers integer[];
    _arr_threshold_weights integer[];
    _arr_penalty_percentages DECIMAL(6, 2)[];
    i INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    _id_question_type INTEGER;
BEGIN
  SELECT has_answers, id_user_created, id_question_type
    INTO _has_answers, _id_user_created, _id_question_type
    FROM question
    JOIN question_type ON question.id_question_type = question_type.id
   WHERE question.id = _id;

    -- Update in place:
    UPDATE question
    SET question_text = _question_text,
        question_comment = _question_comment,
        upload_files_no = _upload_files_no,
        required_upload_files_no = _required_upload_files_no,
        is_active = _is_active,
        id_user_modified = _id_user_modified,
        grader_object = _grader_object,
        time_limit = _time_limit,
        version = CASE _bump_version
                    WHEN TRUE THEN version + 1
                    ELSE version
                  END
    WHERE id = _id;

    UPDATE question_data_object
       SET data_object = _data_object
    WHERE id_question = _id;

    UPDATE question_eval_script
       SET eval_script = _eval_script,
           id_script_programming_language = _id_script_programming_language
    WHERE id_question = _id;

    IF (_has_answers) THEN
        _arr_answers           = string_to_array(_answers_texts, '~~#~~');
        _arr_answers_static    = string_to_array(_answer_text_static, '~~#~~');
        _arr_answers_dynamic   = string_to_array(_answer_text_dynamic, '~~#~~');
        _arr_threshold_weights = string_to_array(_threshold_weights, '~~#~~');
        _arr_penalty_percentages = string_to_array(_penalty_percentages, '~~#~~');
        _arr_correct_answers   = string_to_array(_correct_answers, '~~#~~');

        IF (   array_lower(_arr_answers, 1) <> array_lower(_arr_threshold_weights, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_correct_answers, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_penalty_percentages, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_threshold_weights, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_correct_answers, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_penalty_percentages, 1)
            ) THEN
            RAISE EXCEPTION 'Bad arguments for SQL question (all arrays must be of the same size).';
        END IF;

        IF (_id_question_type = 9) THEN
            FOR i in array_lower(_arr_answers_static, 1)..array_upper(_arr_answers_static, 1) loop
                UPDATE connected_elements_question_answer
                   SET answer_text_static = _arr_answers_static[i]::TEXT,
                       answer_text_dynamic = _arr_answers_dynamic[i]::TEXT
                    --penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                WHERE id_question = _id
                AND ordinal = i;
            END LOOP;
            UPDATE connected_elements_question
               SET id_connected_elements_correctness_model = _id_connected_elements_correctness_model
             WHERE id_question = _id;
        ELSE
            FOR i in array_lower(_arr_answers, 1)..array_upper(_arr_answers, 1) loop
                UPDATE question_answer
                   SET answer_text       = _arr_answers[i]::TEXT,
                       threshold_weight  = _arr_threshold_weights[i]::SMALLINT,
                       penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                       is_correct        = _arr_correct_answers[i]::BOOLEAN
                 WHERE id_question = _id
                   AND ordinal = i;
            END LOOP;
        END IF;
        IF (_id_question_type = 8) THEN
            UPDATE ordered_element_question
               SET id_ordered_element_correctness_model = _id_ordered_element_correctness_model,
                   display_option = _display_option
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL (2) and C
        IF (_id_question_type = 2) THEN
            UPDATE sql_question_answer
            SET sql_answer = _sql_answer,
                sql_alt_assertion = _sql_alt_assertion,
                sql_test_fixture = _sql_test_fixture,
                sql_alt_presentation_query = _sql_alt_presentation_query,
                check_tuple_order = _check_tuple_order,
                id_check_column_mode = _id_check_column_mode
            WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM update_c_question(
                _id, _c_prefix, _c_suffix, _c_source, _c_test_type_ids,
                _c_percentages, _c_allow_diff_orders, _c_allow_diff_letter_sizes,
                _c_trim_whitespaces, _c_comments, _c_regexs, _c_question_test_ids,
                _c_inputs, _c_outputs, _c_generator_test_file_ids,
                _c_argumentss, _c_random_test_type_ids, _c_low_bounds,
                _c_upper_bounds, _c_elem_counts, _c_programming_language_id, _c_rtc_question,
                                                        _c_rtc_test, _c_is_public);
        ELSIF (_id_question_type = 11) THEN  -- Diagrams
            UPDATE diagram_question_answer
                SET initial_diagram_answer = _initial_diagram_answer,
                    diagram_answer = _diagram_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            UPDATE text_question_answer
                SET text_answer = _text_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 25) THEN
                -- it's ok, nothing more to do here...
        ELSIF (_id_question_type = 21) THEN
                -- Mongo, JSON
                UPDATE json_question_answer
                SET json_answer = _json_answer,
                    json_alt_assertion = _json_alt_assertion,
                    json_test_fixture = _json_test_fixture,
                    json_alt_presentation_query = _json_alt_presentation_query,
                    assert_deep = _assert_deep,
                    assert_strict = _assert_strict
                WHERE id_question = _id;
        ELSIF (_id_question_type = 26) THEN
                -- Scale
                DELETE FROM question_scale WHERE id_question = _id;
                INSERT INTO question_scale (id_question, id_scale) VALUES (_id, _id_scale);
        ELSE
                RAISE EXCEPTION 'Unknown question type';
        END IF;
    END IF;

  PERFORM set_question_tags(_id, _tags);
  RETURN _id;
END;
$$;

GRANT EXECUTE on function save_in_place_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer, boolean) to edgar;



CREATE OR REPLACE FUNCTION clone_question(_id integer, _id_user_created integer, _id_user_modified integer, _linktoparent boolean) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _has_answers boolean;
    new_id INTEGER;
    _id_question_type INTEGER;
BEGIN

    SELECT has_answers, id_question_type
      INTO _has_answers, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
     WHERE question.id = _id;

    -- Clone head:
    INSERT INTO question (version, is_active, id_prev_question, id_question_type, question_text, question_comment, id_user_created, id_user_modified
          , upload_files_no, required_upload_files_no, grader_object)
        SELECT CASE WHEN (_linkToParent) THEN version + 1 ELSE 0 END, is_active,
               CASE WHEN (_linkToParent) THEN _id ELSE null END,
               id_question_type, question_text, question_comment, _id_user_created, _id_user_modified
               , upload_files_no, required_upload_files_no, grader_object
          FROM question
         WHERE id = _id
        RETURNING id INTO new_id;

    -- Clone detail:
    -- (a) nodes:
    INSERT INTO question_node (id_question, id_node)
      SELECT new_id, id_node
        FROM question_node
       WHERE id_question = _id;
    -- (b) answers:
    IF (_has_answers) THEN

      INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage)
      SELECT new_id, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage
        FROM question_answer
       WHERE id_question = _id;
       IF (_id_question_type = 8) THEN
            INSERT INTO ordered_element_question (id_question, id_ordered_element_correctness_model, display_option)
            SELECT new_id, id_ordered_element_correctness_model, display_option
              FROM ordered_element_question
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL + C
        IF (_id_question_type = 2) THEN
            INSERT INTO sql_question_answer (id_question, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                    sql_alt_presentation_query, check_tuple_order, id_check_column_mode)
            SELECT new_id, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                   sql_alt_presentation_query, check_tuple_order, id_check_column_mode
              FROM sql_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 21) THEN
            INSERT INTO json_question_answer (id_question, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                    json_alt_presentation_query, assert_deep, assert_strict)
            SELECT new_id, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                   json_alt_presentation_query, assert_deep, assert_strict
              FROM json_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM clone_c_question_answer(_id, new_id);
        ELSIF (_id_question_type = 26) THEN
            INSERT INTO question_scale (id_question, id_scale)
            SELECT new_id, id_scale
              FROM question_scale
             WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            -- Free text question
            INSERT INTO text_question_answer (id_question, text_answer)
            SELECT new_id, text_answer
              FROM text_question_answer
             WHERE id_question = _id;
        ELSE
            RAISE EXCEPTION 'Unknown id_question_type';
        END IF;
    END IF;
    -- tags:
    INSERT INTO question_course_tag (id_question, id_course_tag)
         SELECT new_id, id_course_tag
           FROM question_course_tag
          WHERE id_question = _id;
    -- data objects:
    INSERT INTO question_data_object (id_question, data_object)
         SELECT new_id, data_object
           FROM question_data_object
          WHERE id_question = _id;
    -- scripts:
    INSERT INTO question_eval_script (id_question, eval_script, id_script_programming_language)
         SELECT new_id, eval_script, id_script_programming_language
           FROM question_eval_script
          WHERE id_question = _id;

    --INSERT INTO question_attachment (id_question, original_name, filename, label, is_public)
    --    SELECT new_id, original_name, filename, label, is_public
    --        FROM question_attachment
    --        WHERE id_question = _id;

    RETURN new_id;
END;
$$;

ALTER FUNCTION clone_question(integer, integer, integer, boolean) owner to postgres;
GRANT EXECUTE on function clone_question(integer, integer, integer, boolean) to edgar;


create or replace function clone_test(_id_test integer, _id_academic_year integer, _id_user_created integer) returns integer
    security definer
    language plpgsql
as
$$

DECLARE new_id INTEGER;
-- DECLARE _test_ordinal INTEGER;

BEGIN

-- Clone head:
  -- Test ordinal is no longer unique, so I'm dropping this part:
    -- IF (0 = (SELECT COUNT(*)
    --   FROM test JOIN test old_test
    --             ON test.id_course = old_test.id_course
    --             and test.test_ordinal = old_test.test_ordinal
    --             and old_test.id = _id_test
    --    AND test.id_academic_year = _id_academic_year)) THEN
    --   SELECT test_ordinal INTO _test_ordinal FROM test where id = _id_test;
    -- ELSE
    --   SELECT COALESCE(MAX(test_ordinal), 0) + 1 INTO _test_ordinal FROM test WHERE id_academic_year = _id_academic_year;
    -- END IF;



    INSERT INTO test (title, title_abbrev, id_course, id_academic_year, id_semester, id_test_type, id_user_created, test_ordinal,
    max_runs, show_solutions, hint_result, max_score, password, questions_no, duration_seconds, pass_percentage,
    ts_available_from, ts_available_to, review_period_mins, is_competition, eval_comp_score, async_submit,
    trim_clock,
    allow_anonymous,
    upload_file_limit,
    forward_only
    )
    SELECT title, title_abbrev, id_course, _id_academic_year, id_semester, id_test_type, _id_user_created, test_ordinal, max_runs,
        show_solutions, hint_result, max_score,
      (SELECT string_agg (substr('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', ceil (random() * 62)::integer, 1), '')
       FROM generate_series(1, 45)), questions_no,
                                     duration_seconds,
                                     pass_percentage,
                                     ts_available_from,
                                     ts_available_to,
                                     review_period_mins, is_competition, eval_comp_score, async_submit,
        trim_clock,
        allow_anonymous,
        upload_file_limit,
        forward_only
    FROM test
    WHERE id = _id_test RETURNING id INTO new_id;

    -- Clone detail:
    INSERT INTO test_part (id_test, id_node, id_grading_model, id_question_type, min_questions, max_questions, pass_percentage)
    SELECT new_id,
           id_node,
           id_grading_model,
           id_question_type,
           min_questions,
           max_questions,
           pass_percentage
    FROM test_part
    WHERE id_test = _id_test;


    INSERT INTO test_question (id_test, ordinal, id_question, id_grading_model)
    SELECT new_id,
           ordinal,
           id_question,
           id_grading_model
    FROM test_question
    WHERE id_test = _id_test;

    RETURN new_id;
END; $$;

alter function clone_test(integer, integer, integer) owner to postgres;

grant execute on function clone_test(integer, integer, integer) to edgar;



COMMIT WORK;
