BEGIN WORK;

alter table test_part add id_question int references question(id);



create or replace function gen_test_instance(_id_student integer, _id_test integer, _ip_address character varying) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _questions_no  SMALLINT ;
    _row RECORD;
    _new_test_instance_id INTEGER;
    _generated INTEGER;
    _fixed_questions BOOLEAN;
    _retry integer = 0;
BEGIN
    _questions_no = (SELECT test.questions_no FROM test WHERE test.id = _id_test);
    --check if test type is Lecture quiz
    DROP TABLE if exists tiq;
    CREATE TEMP TABLE tiq (
        --id                SERIAL PRIMARY KEY NOT NULL,
        --id_test_instance  integer not NULL REFERENCES test_instance(id),
        id_test_part      integer,  -- fixed will not set this
        id_question       integer not NULL ,
        id_question_type  integer not NULL ,
        --ordinal           smallint not NULL CONSTRAINT ordinal_btw_0_and_100  CHECK (ordinal BETWEEN 0 and 100),
        id_grading_model  integer not NULL ,
        answers_permutation             INTEGER[],
        weights_permutation             INTEGER[],
        penalty_percentage_permutation  INTEGER[],
        correct_answers_permutation     INTEGER[],
        fixed_test_ordinal integer not null default 0,  -- only fixed test will set this, used when inserting at the bottom
        skip_permutations boolean not null default false -- lecture quiz and PA; dont want to shuffle them
    );
    _fixed_questions := false;

    -- This will soon be obsolete, legacy option:
    IF (SELECT fixed_questions
          FROM test
          JOIN test_type
            ON test.id_test_type = test_type.id
          WHERE test.id = _id_test ) = true THEN
        _fixed_questions := true;
        -- for lecture quizzes and competitions:
        FOR _row IN (SELECT test_question.*, question.id_question_type, test_type.skip_permutations
                       FROM test_question
                       JOIN question
                         ON test_question.id_question = question.id
                       JOIN test
                         ON test_question.id_test = test.id
                       JOIN test_type
                         ON test.id_test_type = test_type.id
                      WHERE id_test = _id_test
                      ORDER BY ordinal)
        LOOP
          CASE
            WHEN _row.id_question_type = 9 THEN  -- Connected Elements
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                              correct_answers_permutation, penalty_percentage_permutation, weights_permutation
                              , fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM connected_elements_question_answer where id_question = _row.id_question),
                  (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = _row.id_question),
                  _row.ordinal,
                  _row.skip_permutations
                        );
            WHEN _row.id_question_type = 11  THEN
              INSERT INTO tiq (id_question, id_question_type, id_grading_model,
                      fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  _row.ordinal,
                  _row.skip_permutations
                        );

            ELSE
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                      correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
              VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                  (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  _row.ordinal,
                  _row.skip_permutations
              );
          END CASE;
        END LOOP;
        -- For PA#2 tests:
        FOR _row IN (
           SELECT peer_test_group_question.*, question.id_question_type
             FROM peer_test_group_question
             JOIN question
               ON peer_test_group_question.id_question = question.id
             JOIN peer_test_group
               ON peer_test_group.id = peer_test_group_question.id_peer_test_group
             JOIN peer_test
               ON peer_test_group.id_peer_test = peer_test.id
             JOIN test
               ON test.id = _id_test
              AND test.id_parent = peer_test.id_test_phase_2
             JOIN test_instance ti1
               ON ti1.id_test = peer_test.id_test_phase_1
              AND ti1.id_student = _id_student
             JOIN test_instance_question tiq1
               ON tiq1.ordinal = 1
              AND tiq1.id_test_instance = ti1.id
              AND peer_test_group.id_question = tiq1.id_question
            ORDER BY peer_test_group_question.ordinal)
        LOOP
            INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
            VALUES (
                    _row.id_question,
                    _row.id_question_type,
                    _row.id_grading_model,
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                    (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    _row.ordinal,
                    true
                    );
        END LOOP;
    ELSE
        -- This will soon be the ONLY option: mixed mode!
        -- Non-fixed test, let's generate the questions and insert them:
        DROP TABLE IF EXISTS tpart;
        CREATE TEMP table tpart AS
            SELECT test_part.id AS id_test_part,
                   id_node,
                   id_grading_model,
                   CASE WHEN (id_question IS NOT NULL) THEN 1 ELSE min_questions END,
                   CASE WHEN (id_question IS NOT NULL) THEN 1 ELSE max_questions END,
                   min_questions AS gen_questions,
                   id_question as id_fixed_question,  -- new stuff
                   test_type.skip_permutations,
                   question.id_question_type
            FROM   test_part
            JOIN   test
              ON   test_part.id_test = test.id
            JOIN   test_type
              ON   test.id_test_type = test_type.id
            LEFT JOIN question
                   ON test_part.id_question = question.id
            WHERE  test_part.id_test = _id_test;

        -- #1. Fullfil min_questions request:
        _generated := 0;
        FOR _row IN SELECT min_questions, id_node, id_grading_model, id_test_part, id_fixed_question, skip_permutations, id_question_type
                    FROM tpart
                    WHERE min_questions > 0
        LOOP
            IF (_row.id_fixed_question IS NOT NULL) THEN
              INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model,
                              answers_permutation, correct_answers_permutation, weights_permutation,
                              penalty_percentage_permutation, skip_permutations)
              VALUES (
                  _row.id_test_part,
                  _row.id_fixed_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  -- answers_permutation
                  CASE WHEN _row.id_question_type = 9 THEN
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_fixed_question order by ordinal) as sorted_questions)
                  ELSE
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_fixed_question order by ordinal) as sorted_questions)
                  END,
                  -- correct_answers_permutation
                  CASE WHEN _row.id_question_type = 9 THEN
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_fixed_question order by ordinal) as sorted_questions)
                  ELSE
                    (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_fixed_question and is_correct)
                  END,
                  -- weights_permutation
                  CASE WHEN _row.id_question_type = 9 THEN
                    (SELECT array_agg(ordinal) FROM connected_elements_question_answer where id_question = _row.id_fixed_question)
                  ELSE
                    (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_fixed_question order by ordinal) as sorted_questions)
                  END,
                  -- penalty_percentage_permutation
                  CASE WHEN _row.id_question_type = 9 THEN
                    (SELECT array_agg(ordinal) FROM connected_elements_question_answer where id_question = _row.id_fixed_question)
                  ELSE
                    (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_fixed_question order by ordinal) as sorted_questions)
                  END,
                  _row.skip_permutations
              );
            ELSE
              INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation,
                          weights_permutation, penalty_percentage_permutation)
              SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                    (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                    (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                    CASE WHEN (question.id_question_type = 9)
                      THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                      ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                    END,
                    (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
              FROM   question JOIN question_node
                              ON question.id = question_node.id_question
                              AND is_active
              WHERE  question_node.id_node = _row.id_node
              AND    question.id NOT IN (SELECT id_question FROM tiq)
              ORDER BY random()
              LIMIT _row.min_questions;
            END IF;
            _generated = _generated + _row.min_questions; -- If there are less than that availale in the DB, this var will lie! Better than inf loop.
        END LOOP;

        -- #2. Add optional questions to achieve targer q.no:
        -- if there are qs in multiple nodes then the question uniqueness criteria may cause less than LIMIT (_questions_no - _generated) to be inserted
        --   and the I'll try again few more times. NB: it might be impossible to reach the goal, that is why we have _retry counter.
        WHILE ((_retry < 5) AND ((SELECT COUNT(*) from tiq) < _questions_no)) LOOP
            FOR _row IN
                -- skew the distribution (with max 20) towards those with bigger upper limit (max_questions):
                SELECT id_node, id_grading_model, id_test_part
                FROM  tpart
                JOIN  generate_series(0, 20) idx
                ON    (tpart.max_questions - tpart.min_questions) > idx
                ORDER BY random()
                LIMIT (_questions_no - _generated)
            LOOP
                INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
                SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                      (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                      (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                      CASE WHEN (question.id_question_type = 9)
                        THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                        ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                      END,
                      (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
                FROM   question JOIN question_node
                                ON question.id = question_node.id_question
                                AND is_active
                WHERE  question_node.id_node = _row.id_node
                AND    question.id NOT IN (SELECT id_question FROM tiq)
                ORDER BY random()
                LIMIT 1;
                IF (_retry > 0) THEN
                    EXIT WHEN ((SELECT COUNT(*) from tiq) = _questions_no);
                END IF;
            END LOOP;
            _retry = _retry + 1;
        END LOOP;

    END IF;

    -- #3. (a) Set permutations for those questions that have multiple answers:
    FOR _row IN
    (
      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg(cqa.ordinal) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   connected_elements_question_answer cqa
        ON   tiq.id_question = cqa.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type

      UNION

      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg( case when question_answer.is_correct THEN question_answer.ordinal ELSE NULL END) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   question_answer
        ON   tiq.id_question = question_answer.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type
    )
    LOOP
      UPDATE tiq
      SET    answers_permutation = (
            WITH RECURSIVE r(n,p,a,b)
              AS (SELECT (SELECT trunc(random() * (factorial(_row.no_answers)::integer)))::integer, '{}'::INTEGER[],
                (SELECT array_agg(i::integer) from generate_series(1, _row.no_answers) i), _row.no_answers :: INTEGER
                UNION ALL
                SELECT n / b, p || a[n % b + 1], a[1:n % b] || a[n % b + 2:b], b-1
                FROM r
                WHERE b > 0)
            SELECT p  FROM r WHERE b=0
           ),
           correct_answers_permutation = _row.correct_answers_permutation -- still, to be updated
      WHERE tiq.id_question = _row.id_question;

    END LOOP;

    -- #3. (b) Update correct answers and weights:
    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT    array_agg(perm.nr)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   unnest(tiq.correct_answers_permutation) correct(ordinal)
            ON   perm.ordinal = correct.ordinal
          AND   correct.ordinal IS NOT NULL)
    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type NOT IN (8, 9);  -- TODO: fix this

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by ordinal)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          )
    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 8;

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by perm.ordinal)
           FROM  unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
           JOIN  connected_elements_question_answer cqa
             ON  tiq.id_question = cqa.id_question
            AND (trim(answer_text_static) <> '') IS TRUE  -- only those that have static answers, bcs there can be more dynamic than static answers
            AND perm.ordinal = cqa.ordinal
          )

    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 9;  -- TODO: fix this

    UPDATE tiq
      SET    weights_permutation =

        (SELECT    array_agg(question_answer.threshold_weight)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false  -- skip lecture quizzes and PAs;
      AND tiq.id_question_type <> 9;  -- skip connected elements

    UPDATE tiq
      SET    penalty_percentage_permutation =

        (SELECT    array_agg(question_answer.penalty_percentage)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false;  -- skip lecture quizzes and PAs;

    -- **********************************************
    -- * Finally, save it:
    -- **********************************************
    INSERT INTO test_instance (id_test, id_student, ts_generated, ts_started, ip_address)
       VALUES (_id_test, _id_student, current_timestamp, NULL, _ip_address);

    _new_test_instance_id = (SELECT currval('test_instance_id_seq'));

    IF (_fixed_questions) THEN
        INSERT into test_instance_question (id_test_instance, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_question,
            (row_number() OVER (ORDER BY fixed_test_ordinal)) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        ORDER BY fixed_test_ordinal;

    ELSE
        INSERT into test_instance_question (id_test_instance, id_test_part, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_test_part,
            tiq.id_question,
            (row_number() OVER (ORDER BY test_part.ordinal, random())) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        JOIN test_part
            ON tiq.id_test_part = test_part.id
        ORDER BY test_part.ordinal, random();

        -- cleanup:
        DROP TABLE IF EXISTS tpart;
    END IF;

    -- cleanup:
    DROP TABLE if exists tiq;

    RETURN _new_test_instance_id;

END;
$$;



COMMIT WORK;
