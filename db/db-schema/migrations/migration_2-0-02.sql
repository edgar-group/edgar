BEGIN WORK;

CREATE TABLE public.complex_question (
    id            serial        primary key,
    id_question   integer       not null     references public.question,
    setup_script  text          not null,
    test_cases    JSONB         not null,
    ----------------------
    ts_created    timestamp with time zone not null,
    ts_modified   timestamp with time zone not null,
    user_created  varchar(50)              not null,
    user_modified varchar(50)              not null
);
SELECT audit.audit_table('complex_question');


create or replace function new_question(_id_question_type integer, _id_code_runner integer, _answers_count smallint,
_cards_count1 smallint, _cards_count2 smallint, _id_user integer, _arr_node_ids integer[], use_template boolean, use_eval_script boolean,
_id_programming_languages integer[] DEFAULT NULL::integer[]) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _new_id INTEGER;
    i integer;
    _new_answer_id INTEGER;
    _new_question_test_id INTEGER;
    _pl RECORD;
BEGIN
    INSERT INTO question (VERSION, is_active, id_question_type, question_text, id_user_created, id_user_modified)
    VALUES (0, FALSE, _id_question_type,
      CASE
        WHEN (_id_question_type = 1) THEN '2+2?'
        WHEN (_id_question_type = 7) THEN 'Is this statement true or false?'
        WHEN (_id_question_type = 8) THEN 'Please, use drag and drop to order the assigned objects.'
        WHEN (_id_question_type = 9) THEN 'Please, use drag and drop to connect the assigned objects.'
        WHEN (_id_question_type = 10) THEN 'Please, fill in the blanks.'
        WHEN (_id_question_type = 11) THEN 'Add the following elements to the B-tree: `1,10,20`.
You can try these diagram types too:
```
sequenceDiagram
Alice->>John: Hello John, how are you?
John-->>Alice: Great!
Alice-)John: See you later!
```
or this:
```
stateDiagram
[*] --> Still
Still --> [*]
Still --> Moving
Moving --> Still
Moving --> Crash
Crash --> [*]
Crash --> Still
```
See more at mermaid.js'
        WHEN (_id_question_type = 25) THEN 'This ''question'' is just a placeholder for the attachement, please attach the file that will be given to the student. Make sure you have enabled question attachments in the course settings.'
        WHEN (_id_question_type = 30) THEN 'Create a project .... upload it as a single zip.'
        ELSE 'Please, answer my clever question...'
      END, _id_user, _id_user)
    RETURNING id INTO _new_id;

    IF (use_template) THEN
        INSERT INTO question_data_object (id_question) VALUES (_new_id);
     END IF;

    IF (use_eval_script) THEN
        INSERT INTO question_eval_script (id_question) VALUES (_new_id);
     END IF;

    IF (_id_question_type = 1) THEN
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 1,
                 TRUE,
                 '4');
        FOR i IN 2.._answers_count LOOP
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 i,
                 FALSE,
                 'dunno');
         END LOOP;
   END IF;
   IF(_id_question_type = 7) THEN
     INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 1,
                 TRUE,
                 'True');
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 2,
                 FALSE,
                 'False');
   END IF;
   IF(_id_question_type = 8) THEN
        FOR i IN 1.._answers_count LOOP
        INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text)
        VALUES (_new_id,
                 i,
                 TRUE,
           i);
         END LOOP;
        INSERT INTO ordered_element_question (id_question) values (_new_id);
   END IF;
   IF(_id_question_type = 9) THEN
     IF(_cards_count1 <= _cards_count2) THEN
      FOR i IN 1.._cards_count2 LOOP
        INSERT INTO connected_elements_question_answer (id_question, ordinal, answer_text_static, answer_text_dynamic, ts_created, ts_modified, user_modified)
        VALUES (_new_id,
             i,
            CASE WHEN i <= _cards_count1 THEN i ELSE NULL END,
             i*11,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP,
             'postgres'
             );
       END LOOP;
     ELSE
       FOR i IN 1.._cards_count1 LOOP
        INSERT INTO connected_elements_question_answer (id_question, ordinal, answer_text_static, answer_text_dynamic, ts_created, ts_modified, user_modified)
        VALUES (_new_id,
             i,
             i,
             CASE WHEN i <= _cards_count2 THEN i*11 ELSE NULL END,
             CURRENT_TIMESTAMP,
             CURRENT_TIMESTAMP,
             'postgres'
             );
       END LOOP;
     END IF;
     INSERT INTO connected_elements_question (id_question) values (_new_id);
   END IF;

   IF(_id_question_type = 11) THEN
     INSERT INTO diagram_question_answer (id_question, initial_diagram_answer, diagram_answer)
              VALUES (_new_id, 'graph
7-->3
7-->12
3-->2
3-->5,6
12-->8
12-->15,23',
           'graph
7-->3
7-->12,20
3-->1,2
3-->5,6
12,20-->8,10
12,20-->15
12,20-->23');
    END IF;



     --ELSE
     -- Handle additional question types here
     -- SQL + C
        IF(_id_question_type = 2) THEN
            -- Handle additional question types here
            INSERT INTO sql_question_answer (id_question, id_code_runner, sql_answer)
            VALUES (_new_id, _id_code_runner, 'SELECT * FROM student');

            -- FOR _pl IN
            --   SELECT DISTINCT id_programming_language
            --   FROM course_code_runner
            --   WHERE id_code_runner = _id_code_runner
            -- LOOP
            --   INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _pl.id_programming_language);
            -- END LOOP;

        END IF;
        IF(_id_question_type = 21) THEN
              INSERT INTO json_question_answer (id_question, id_code_runner, json_answer)
              VALUES (_new_id, _id_code_runner, 'db.collection.find({});');
        END IF;
        IF(_id_question_type = 20) THEN
              INSERT INTO text_question_answer (id_question, text_answer)
              VALUES (_new_id, '42.');
        END IF;
        IF(_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            INSERT INTO c_question_answer ( id_question, c_prefix, c_suffix, c_source, id_programming_language)
            VALUES (_new_id, '', '',
                CASE WHEN (array_length(_id_programming_languages, 1) = 1) THEN
                  (SELECT hello_world FROM programming_language WHERE id = _id_programming_languages[1])
                ELSE
                    ''
              END, _id_programming_languages[1]
              )
            RETURNING id INTO _new_answer_id;

            INSERT INTO c_question_test ( c_question_answer_id, c_test_type_id, percentage, allow_diff_order, allow_diff_letter_size, trim_whitespace)
            VALUES (_new_answer_id, 1, 100, FALSE, TRUE, TRUE)
            RETURNING id INTO _new_question_test_id;

            INSERT INTO fixed_test( c_question_test_id, input, output)
            VALUES (_new_question_test_id, '1', '1');

            -- FOR i in array_lower(_id_programming_languages, 1)..array_upper(_id_programming_languages, 1) LOOP
            --   INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _id_programming_languages[i]);
            -- END LOOP;
        END IF;
        IF(_id_question_type = 26 AND (SELECT COUNT(*) FROM scale) > 0) THEN
              INSERT INTO question_scale (id_question, id_scale)
              SELECT _new_id, max(id)
                FROM scale;
        END IF;
        IF(_id_question_type = 30) THEN
              -- FOR i in array_lower(_id_programming_languages, 1)..array_upper(_id_programming_languages, 1) LOOP
              --   INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _id_programming_languages[i]);
              -- END LOOP;
              INSERT INTO complex_question (id_question, setup_script, test_cases)
              SELECT _new_id,
'#!/bin/bash

# parsing cmd line args and setting env vars... one argument = one variable
# eg. ./main.sh -var1 value1 -var2 value2 will produce:
# export var1=value1
# export var2=value2
# maybe keep separalely?          #TODO
while [[ $# -gt 0 ]]; do
    case "$1" in
        -*)
            var_name="${1:1}"
            shift
            if [[ $# -gt 0 ]]; then
                export "$var_name"="$1"
                shift
            else
                echo "Error: Value for $var_name not provided"
                exit 1
            fi
            ;;
        *)
            echo "Error: Invalid argument $1"
            exit 1
            ;;
    esac
done

# re-scripts.zip contains scripts for running and evaluating the project
unzip -qq re-scripts.zip -d scripts
chmod +x scripts/*

# submission.zip contains student project
unzip -qq submission.zip -d submission


# read from stdina the names (paths) of the script to run
# results are stored to the file <script_name>.out
read -r script_name

if [[ -x "$script_name" ]]; then
   output_file="${script_name}.out"
   ./"$script_name" > "$output_file" 2>&1

   eval_script_file="${script_name}.eval.sh"
   result_file="${script_name}.result"
   ./"$eval_script_file"> "$result_file" 2>&1
   base64 "$result_file"
else
   echo "Error: ''$script_name'' is not executable or does not exist."
fi
', '[
    {
      "run_script": "run1.sh",
      "eval_script": "eval1.sh",
      "comment": ""
    }
  ]';
        END IF;

    FOR i in array_lower(_arr_node_ids, 1)..array_upper(_arr_node_ids, 1) loop
        INSERT INTO question_node (id_question, id_node) VALUES (_new_id, _arr_node_ids[i]);
    END LOOP;
    IF (cardinality(_id_programming_languages) > 0) THEN
      FOR i in array_lower(_id_programming_languages, 1)..array_upper(_id_programming_languages, 1) LOOP
        INSERT INTO question_programming_language (id_question, id_programming_language) VALUES (_new_id, _id_programming_languages[i]);
      END LOOP;
    END IF;

    RETURN _new_id;
END;
$$;


create or replace function delete_question(_id integer) returns boolean
    security definer
    language plpgsql
as
$$
DECLARE
BEGIN
    -- Script, if exists:
    DELETE FROM question_eval_script WHERE id_question = _id;
    -- Template, if exists:
    DELETE FROM question_data_object WHERE id_question = _id;
    -- Will not delete test_instance_question ON PURPOSE; such questions should not be deleted, ever.
    DELETE FROM question_node WHERE id_question = _id;
    -- ABC:
    DELETE FROM question_answer WHERE id_question = _id;
    -- SQL:
    DELETE FROM sql_question_answer WHERE id_question = _id;
    -- JSON:
    DELETE FROM json_question_answer WHERE id_question = _id;
    -- Tags:
    DELETE FROM question_course_tag WHERE id_question = _id;
    --Scale (questionnaire)
    DELETE FROM question_scale WHERE id_question = _id;
    -- C:
    DELETE FROM random_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
     DELETE FROM fixed_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
     DELETE FROM generator_test
     WHERE c_question_test_id IN (
        SELECT c_question_test.id
          FROM c_question_test JOIN c_question_answer
                                 ON c_question_test.c_question_answer_id = c_question_answer.id
         WHERE c_question_answer.id_question = _id);
    -- What about generator_test_file?

    -- Delete test level runtime constraints
    DELETE FROM runtime_constraint_test
        WHERE id_c_question_test IN (
            SELECT id FROM c_question_test WHERE c_question_answer_id IN (
                    SELECT id FROM c_question_answer WHERE id_question = _id));

    DELETE FROM c_question_test
      WHERE c_question_answer_id IN (
            SELECT id FROM c_question_answer WHERE id_question = _id
        );

    -- Delete question level runtime constraints
    DELETE FROM runtime_constraint_question WHERE id_question = _id;

    DELETE FROM question_programming_language WHERE id_question = _id;

    DELETE FROM c_question_answer WHERE id_question = _id;
    DELETE FROM question_review   WHERE id_question = _id;
    DELETE FROM text_question_answer  WHERE id_question = _id;
    DELETE FROM question_attachment  WHERE id_question = _id;
    ------------------------------------
    DELETE FROM ordered_element_question  WHERE id_question = _id;
    DELETE FROM connected_elements_question  WHERE id_question = _id;
    DELETE FROM connected_elements_question_answer  WHERE id_question = _id;
    DELETE FROM diagram_question_answer  WHERE id_question = _id;
    ------------------------------------
    DELETE FROM complex_question  WHERE id_question = _id;
    ------------------------------------
    -- Finally:
    DELETE FROM question WHERE id = _id;

    RETURN true;
END;
$$;

grant execute on function delete_question(integer) to edgar;






drop function save_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer);

create function save_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint
, _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text
, _inital_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text
, _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text
, _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint
, _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text
, _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text
, _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text
, _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text
, _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean
, _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text
, _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text
, _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer
, _setup_script text, _test_cases jsonb
) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    i INTEGER;
    _new_id INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    -- C stuff:
    _id_question_type INTEGER;
    -- /C stuff

BEGIN
    SELECT  has_answers,  id_user_created,  id_question_type
      INTO _has_answers, _id_user_created, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
    WHERE question.id = _id;

    IF (SELECT COUNT(*) FROM test_instance_question WHERE id_question = _id) = 0 THEN
    -- Virgin, update in place:
        RETURN public.save_in_place_question(
               _id,
               _is_active,
               _upload_files_no,
               _required_upload_files_no,
               _question_text,
               _display_option,
               _time_limit,
               _question_comment,
               _text_answer,
               _inital_diagram_answer,
               _diagram_answer,
               _correct_answers,
               _answers_texts,
               _answer_text_static,
               _answer_text_dynamic,
               _threshold_weights,
               _penalty_percentages,
               _sql_answer,
               _sql_alt_assertion,
               _sql_test_fixture,
               _sql_alt_presentation_query,
               _check_tuple_order,
               _id_check_column_mode,
               _c_prefix,
               _c_suffix,
               _c_source,
               _c_test_type_ids,
               _c_percentages,
               _c_allow_diff_orders,
               _c_allow_diff_letter_sizes,
               _c_trim_whitespaces,
               _c_comments,
               _c_regexs,
               _c_question_test_ids,
               _c_inputs,
               _c_outputs,
               _c_generator_test_file_ids,
               _c_argumentss,
               _c_random_test_type_ids,
               _c_low_bounds,
               _c_upper_bounds,
               _c_elem_counts,
               _id_user_modified,
               _tags,
               _json_answer,
               _json_alt_assertion,
               _json_test_fixture,
               _json_alt_presentation_query,
               _assert_deep,
               _assert_strict,

               _c_rtc_question,
               _c_rtc_test,
               _c_programming_language_id,
               _c_is_public,
               _id_scale,
               _data_object,
               _eval_script,
               _id_script_programming_language,
               _grader_object,
               _id_ordered_element_correctness_model,
               _id_connected_elements_correctness_model,
               _setup_script,
               _test_cases,
               FALSE
        );
    ELSE
    -- Referenced question, close it (active) and leave content (keep history) and instantiate new question:
        UPDATE question
        SET is_active = false,
            id_user_modified = _id_user_modified
        WHERE id = _id;

        SELECT public.clone_question(_id, _id_user_created, _id_user_modified, true) INTO _new_id;
        -- Update new (virgin) question:
        RETURN public.save_question(
            _new_id,
            _is_active,
            _upload_files_no,
            _required_upload_files_no,
            _question_text,
            _display_option,
            _time_limit,
            _question_comment,
            _text_answer,
            _inital_diagram_answer,
            _diagram_answer,
            _correct_answers, --CSVs
            _answers_texts,  -- CSVs
            _answer_text_static,
            _answer_text_dynamic,
            _threshold_weights,  -- CSVs
            _penalty_percentages, -- CSVs
            _sql_answer,
            _sql_alt_assertion,
            _sql_test_fixture,
            _sql_alt_presentation_query,
            _check_tuple_order,
            _id_check_column_mode,
            _c_prefix,
            _c_suffix,
            _c_source,
            _c_test_type_ids,
            _c_percentages,
            _c_allow_diff_orders,
            _c_allow_diff_letter_sizes,
            _c_trim_whitespaces,
            _c_comments,
            _c_regexs,
            --// TODO: this sucks, bcs there is no "ordinal" attribute in the c_qustion_test, I have to go out on a lim with order by ID
            (SELECT array_to_string(array(
              SELECT id
                FROM c_question_test
              WHERE c_question_answer_id = (SELECT id FROM c_question_answer WHERE id_question = _new_id)
              ORDER BY id), '~~#~~')),
            _c_inputs,
            _c_outputs,
            _c_generator_test_file_ids,
            _c_argumentss,
            _c_random_test_type_ids,
            _c_low_bounds,
            _c_upper_bounds,
            _c_elem_counts,
            _id_user_modified,
            _tags,
            _json_answer,
            _json_alt_assertion,
            _json_test_fixture,
            _json_alt_presentation_query,
            _assert_deep,
            _assert_strict,
            _c_rtc_question,
            _c_rtc_test,
            _c_programming_language_id,
            _c_is_public,
            _id_scale,
            _data_object,
            _eval_script,
            _id_script_programming_language,
            _grader_object,
            _id_ordered_element_correctness_model,
            _id_connected_elements_correctness_model,
            _setup_script,
            _test_cases
        );
    END IF;

END;
$$;


grant execute on function save_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer, text, jsonb) to edgar;



drop function save_in_place_question(integer, boolean, smallint, smallint, text, text, integer, text, text, text, text, text, text, text, text, text, text, text, text, text, text, boolean, smallint, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, integer, text, text, text, text, text, boolean, boolean, text, text, text, text, integer, text, text, integer, text, integer, integer, boolean);
create function save_in_place_question(_id integer, _is_active boolean, _upload_files_no smallint, _required_upload_files_no smallint,
 _question_text text, _display_option text, _time_limit integer, _question_comment text, _text_answer text,
 _initial_diagram_answer text, _diagram_answer text, _correct_answers text, _answers_texts text, _answer_text_static text,
 _answer_text_dynamic text, _threshold_weights text, _penalty_percentages text, _sql_answer text, _sql_alt_assertion text,
 _sql_test_fixture text, _sql_alt_presentation_query text, _check_tuple_order boolean, _id_check_column_mode smallint,
 _c_prefix text, _c_suffix text, _c_source text, _c_test_type_ids text, _c_percentages text, _c_allow_diff_orders text,
 _c_allow_diff_letter_sizes text, _c_trim_whitespaces text, _c_comments text, _c_regexs text, _c_question_test_ids text,
 _c_inputs text, _c_outputs text, _c_generator_test_file_ids text, _c_argumentss text, _c_random_test_type_ids text,
 _c_low_bounds text, _c_upper_bounds text, _c_elem_counts text, _id_user_modified integer, _tags text, _json_answer text,
 _json_alt_assertion text, _json_test_fixture text, _json_alt_presentation_query text, _assert_deep boolean,
 _assert_strict boolean, _c_rtc_question text, _c_rtc_test text, _c_programming_language_id text, _c_is_public text,
 _id_scale integer, _data_object text, _eval_script text, _id_script_programming_language integer, _grader_object text,
 _id_ordered_element_correctness_model integer, _id_connected_elements_correctness_model integer
 , _setup_script text, _test_cases jsonb
 , _bump_version boolean) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _arr_answers text[];
    _arr_answers_static text[];
    _arr_answers_dynamic text[];
    _arr_correct_answers integer[];
    _arr_threshold_weights integer[];
    _arr_penalty_percentages DECIMAL(6, 2)[];
    i INTEGER;
    _has_answers boolean;
    _id_user_created integer;
    _id_question_type INTEGER;
BEGIN
  SELECT has_answers, id_user_created, id_question_type
    INTO _has_answers, _id_user_created, _id_question_type
    FROM question
    JOIN question_type ON question.id_question_type = question_type.id
   WHERE question.id = _id;

    -- Update in place:
    UPDATE question
    SET question_text = _question_text,
        question_comment = _question_comment,
        upload_files_no = _upload_files_no,
        required_upload_files_no = _required_upload_files_no,
        is_active = _is_active,
        id_user_modified = _id_user_modified,
        grader_object = _grader_object,
        time_limit = _time_limit,
        version = CASE _bump_version
                    WHEN TRUE THEN version + 1
                    ELSE version
                  END
    WHERE id = _id;

    UPDATE question_data_object
       SET data_object = _data_object
    WHERE id_question = _id;

    UPDATE question_eval_script
       SET eval_script = _eval_script,
           id_script_programming_language = _id_script_programming_language
    WHERE id_question = _id;

    IF (_has_answers) THEN
        _arr_answers           = string_to_array(_answers_texts, '~~#~~');
        _arr_answers_static    = string_to_array(_answer_text_static, '~~#~~');
        _arr_answers_dynamic   = string_to_array(_answer_text_dynamic, '~~#~~');
        _arr_threshold_weights = string_to_array(_threshold_weights, '~~#~~');
        _arr_penalty_percentages = string_to_array(_penalty_percentages, '~~#~~');
        _arr_correct_answers   = string_to_array(_correct_answers, '~~#~~');

        IF (   array_lower(_arr_answers, 1) <> array_lower(_arr_threshold_weights, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_correct_answers, 1)
            OR array_lower(_arr_answers, 1) <> array_lower(_arr_penalty_percentages, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_threshold_weights, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_correct_answers, 1)
            OR array_upper(_arr_answers, 1) <> array_upper(_arr_penalty_percentages, 1)
            ) THEN
            RAISE EXCEPTION 'Bad arguments for SQL question (all arrays must be of the same size).';
        END IF;

        IF (_id_question_type = 9) THEN
            FOR i in array_lower(_arr_answers_static, 1)..array_upper(_arr_answers_static, 1) loop
                UPDATE connected_elements_question_answer
                   SET answer_text_static = _arr_answers_static[i]::TEXT,
                       answer_text_dynamic = _arr_answers_dynamic[i]::TEXT
                    --penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                WHERE id_question = _id
                AND ordinal = i;
            END LOOP;
            UPDATE connected_elements_question
               SET id_connected_elements_correctness_model = _id_connected_elements_correctness_model
             WHERE id_question = _id;
        ELSE
            FOR i in array_lower(_arr_answers, 1)..array_upper(_arr_answers, 1) loop
                UPDATE question_answer
                   SET answer_text       = _arr_answers[i]::TEXT,
                       threshold_weight  = _arr_threshold_weights[i]::SMALLINT,
                       penalty_percentage  = _arr_penalty_percentages[i]::DECIMAL(6,2),
                       is_correct        = _arr_correct_answers[i]::BOOLEAN
                 WHERE id_question = _id
                   AND ordinal = i;
            END LOOP;
        END IF;
        IF (_id_question_type = 8) THEN
            UPDATE ordered_element_question
               SET id_ordered_element_correctness_model = _id_ordered_element_correctness_model,
                   display_option = _display_option
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL (2) and C
        IF (_id_question_type = 2) THEN
            UPDATE sql_question_answer
            SET sql_answer = _sql_answer,
                sql_alt_assertion = _sql_alt_assertion,
                sql_test_fixture = _sql_test_fixture,
                sql_alt_presentation_query = _sql_alt_presentation_query,
                check_tuple_order = _check_tuple_order,
                id_check_column_mode = _id_check_column_mode
            WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM update_c_question(
                _id, _c_prefix, _c_suffix, _c_source, _c_test_type_ids,
                _c_percentages, _c_allow_diff_orders, _c_allow_diff_letter_sizes,
                _c_trim_whitespaces, _c_comments, _c_regexs, _c_question_test_ids,
                _c_inputs, _c_outputs, _c_generator_test_file_ids,
                _c_argumentss, _c_random_test_type_ids, _c_low_bounds,
                _c_upper_bounds, _c_elem_counts, _c_programming_language_id, _c_rtc_question,
                                                        _c_rtc_test, _c_is_public);
        ELSIF (_id_question_type = 11) THEN  -- Diagrams
            UPDATE diagram_question_answer
                SET initial_diagram_answer = _initial_diagram_answer,
                    diagram_answer = _diagram_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            UPDATE text_question_answer
                SET text_answer = _text_answer
                WHERE id_question = _id;
        ELSIF (_id_question_type = 25) THEN
                -- it's ok, nothing more to do here...
        ELSIF (_id_question_type = 21) THEN
                -- Mongo, JSON
                UPDATE json_question_answer
                SET json_answer = _json_answer,
                    json_alt_assertion = _json_alt_assertion,
                    json_test_fixture = _json_test_fixture,
                    json_alt_presentation_query = _json_alt_presentation_query,
                    assert_deep = _assert_deep,
                    assert_strict = _assert_strict
                WHERE id_question = _id;
        ELSIF (_id_question_type = 26) THEN
                -- Scale
                DELETE FROM question_scale WHERE id_question = _id;
                INSERT INTO question_scale (id_question, id_scale) VALUES (_id, _id_scale);
        ELSIF (_id_question_type = 30) THEN
                -- Complex
                UPDATE complex_question
                  SET setup_script = _setup_script,
                      test_cases = _test_cases
                  WHERE id_question = _id;
        ELSE
                RAISE EXCEPTION 'Unknown question type';
        END IF;
    END IF;

  PERFORM set_question_tags(_id, _tags);
  RETURN _id;
END;
$$;
grant execute on function save_in_place_question to edgar;


create or replace function clone_question(_id integer, _id_user_created integer, _id_user_modified integer, _linktoparent boolean) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _has_answers boolean;
    new_id INTEGER;
    _id_question_type INTEGER;
BEGIN

    SELECT has_answers, id_question_type
      INTO _has_answers, _id_question_type
      FROM question
      JOIN question_type ON question.id_question_type = question_type.id
     WHERE question.id = _id;

    -- Clone head:
    INSERT INTO question (version, is_active, id_prev_question, id_question_type, question_text, question_comment, id_user_created, id_user_modified
          , upload_files_no, required_upload_files_no, grader_object)
        SELECT CASE WHEN (_linkToParent) THEN version + 1 ELSE 0 END, is_active,
               CASE WHEN (_linkToParent) THEN _id ELSE null END,
               id_question_type, question_text, question_comment, _id_user_created, _id_user_modified
               , upload_files_no, required_upload_files_no, grader_object
          FROM question
         WHERE id = _id
        RETURNING id INTO new_id;

    -- Clone detail:
    -- (a) nodes:
    INSERT INTO question_node (id_question, id_node)
      SELECT new_id, id_node
        FROM question_node
       WHERE id_question = _id;
    -- (b) answers:
    IF (_has_answers) THEN

      INSERT INTO question_answer (id_question, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage)
      SELECT new_id, ordinal, is_correct, answer_text, threshold_weight, penalty_percentage
        FROM question_answer
       WHERE id_question = _id;
       IF (_id_question_type = 8) THEN
            INSERT INTO ordered_element_question (id_question, id_ordered_element_correctness_model, display_option)
            SELECT new_id, id_ordered_element_correctness_model, display_option
              FROM ordered_element_question
             WHERE id_question = _id;
        END IF;
    ELSE
        -- Handle additional question types here
        -- SQL + C
        IF (_id_question_type = 2) THEN
            INSERT INTO sql_question_answer (id_question, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                    sql_alt_presentation_query, check_tuple_order, id_check_column_mode)
            SELECT new_id, id_code_runner, sql_answer, sql_alt_assertion, sql_test_fixture,
                   sql_alt_presentation_query, check_tuple_order, id_check_column_mode
              FROM sql_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 21) THEN
            INSERT INTO json_question_answer (id_question, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                    json_alt_presentation_query, assert_deep, assert_strict)
            SELECT new_id, id_code_runner, json_answer, json_alt_assertion, json_test_fixture,
                   json_alt_presentation_query, assert_deep, assert_strict
              FROM json_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 3 OR _id_question_type = 22 OR _id_question_type = 23) THEN
            PERFORM clone_c_question_answer(_id, new_id);
        ELSIF (_id_question_type = 26) THEN
            INSERT INTO question_scale (id_question, id_scale)
            SELECT new_id, id_scale
              FROM question_scale
             WHERE id_question = _id;
        ELSIF (_id_question_type = 20) THEN
            -- Free text question
            INSERT INTO text_question_answer (id_question, text_answer)
            SELECT new_id, text_answer
              FROM text_question_answer
             WHERE id_question = _id;
        ELSIF (_id_question_type = 30) THEN
            -- Free text question
            INSERT INTO complex_question (id_question, setup_script, test_cases)
            SELECT new_id, setup_script, test_cases
              FROM complex_question
             WHERE id_question = _id;
        ELSE
            RAISE EXCEPTION 'Unknown id_question_type';
        END IF;
    END IF;
    -- tags:
    INSERT INTO question_course_tag (id_question, id_course_tag)
         SELECT new_id, id_course_tag
           FROM question_course_tag
          WHERE id_question = _id;
    -- data objects:
    INSERT INTO question_data_object (id_question, data_object)
         SELECT new_id, data_object
           FROM question_data_object
          WHERE id_question = _id;
    -- scripts:
    INSERT INTO question_eval_script (id_question, eval_script, id_script_programming_language)
         SELECT new_id, eval_script, id_script_programming_language
           FROM question_eval_script
          WHERE id_question = _id;

    --INSERT INTO question_attachment (id_question, original_name, filename, label, is_public)
    --    SELECT new_id, original_name, filename, label, is_public
    --        FROM question_attachment
    --        WHERE id_question = _id;

    RETURN new_id;
END;
$$;


-- Bugfix:
--    if there are qs in multiple nodes then the question uniqueness criteria may cause less than LIMIT (_questions_no - _generated) to be inserted
--      and the I'll try again few more times. NB: it might be impossible to reach the goal, that is why we have _retry counter.
create or replace function gen_test_instance(_id_student integer, _id_test integer, _ip_address character varying) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
    _questions_no  SMALLINT ;
    _row RECORD;
    _new_test_instance_id INTEGER;
    _generated INTEGER;
    _fixed_questions BOOLEAN;
    _retry integer = 0;
BEGIN
    _questions_no = (SELECT test.questions_no FROM test WHERE test.id = _id_test);
    --check if test type is Lecture quiz
    DROP TABLE if exists tiq;
    CREATE TEMP TABLE tiq (
        --id                SERIAL PRIMARY KEY NOT NULL,
        --id_test_instance  integer not NULL REFERENCES test_instance(id),
        id_test_part      integer,  -- fixed will not set this
        id_question       integer not NULL ,
        id_question_type  integer not NULL ,
        --ordinal           smallint not NULL CONSTRAINT ordinal_btw_0_and_100  CHECK (ordinal BETWEEN 0 and 100),
        id_grading_model  integer not NULL ,
        answers_permutation             INTEGER[],
        weights_permutation             INTEGER[],
        penalty_percentage_permutation  INTEGER[],
        correct_answers_permutation     INTEGER[],
        fixed_test_ordinal integer not null default 0,  -- only fixed test will set this, used when inserting at the bottom
        skip_permutations boolean not null default false -- lecture quiz and PA; dont want to shuffle them
    );
    _fixed_questions := false;

    IF (SELECT fixed_questions
          FROM test
          JOIN test_type
            ON test.id_test_type = test_type.id
          WHERE test.id = _id_test ) = true THEN
        _fixed_questions := true;
        -- for lecture quizzes and competitions:
        FOR _row IN (SELECT test_question.*, question.id_question_type, test_type.skip_permutations
                       FROM test_question
                       JOIN question
                         ON test_question.id_question = question.id
                       JOIN test
                         ON test_question.id_test = test.id
                       JOIN test_type
                         ON test.id_test_type = test_type.id
                      WHERE id_test = _id_test
                      ORDER BY ordinal)
        LOOP
          CASE
            WHEN _row.id_question_type = 9 THEN  -- Connected Elements
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                              correct_answers_permutation, penalty_percentage_permutation, weights_permutation
                              , fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from connected_elements_question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM connected_elements_question_answer where id_question = _row.id_question),
                  (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = _row.id_question),
                  _row.ordinal,
                  _row.skip_permutations
                        );
            WHEN _row.id_question_type = 11  THEN
              INSERT INTO tiq (id_question, id_question_type, id_grading_model,
                      fixed_test_ordinal, skip_permutations)
                    VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  _row.ordinal,
                  _row.skip_permutations
                        );

            ELSE
              INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                      correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
              VALUES (
                  _row.id_question,
                  _row.id_question_type,
                  _row.id_grading_model,
                  (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                  (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                  _row.ordinal,
                  _row.skip_permutations
              );
          END CASE;
        END LOOP;
        -- For PA#2 tests:
        FOR _row IN (
           SELECT peer_test_group_question.*, question.id_question_type
             FROM peer_test_group_question
             JOIN question
               ON peer_test_group_question.id_question = question.id
             JOIN peer_test_group
               ON peer_test_group.id = peer_test_group_question.id_peer_test_group
             JOIN peer_test
               ON peer_test_group.id_peer_test = peer_test.id
             JOIN test
               ON test.id = _id_test
              AND test.id_parent = peer_test.id_test_phase_2
             JOIN test_instance ti1
               ON ti1.id_test = peer_test.id_test_phase_1
              AND ti1.id_student = _id_student
             JOIN test_instance_question tiq1
               ON tiq1.ordinal = 1
              AND tiq1.id_test_instance = ti1.id
              AND peer_test_group.id_question = tiq1.id_question
            ORDER BY peer_test_group_question.ordinal)
        LOOP
            INSERT INTO tiq (id_question, id_question_type, id_grading_model, answers_permutation,
                correct_answers_permutation, weights_permutation, penalty_percentage_permutation, fixed_test_ordinal, skip_permutations)
            VALUES (
                    _row.id_question,
                    _row.id_question_type,
                    _row.id_grading_model,
                    (SELECT array_agg(ordinal) FROM (SELECT ordinal from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(ordinal) FROM question_answer where id_question = _row.id_question and is_correct),
                    (SELECT array_agg(threshold_weight) FROM (SELECT threshold_weight from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    (SELECT array_agg(penalty_percentage) FROM (SELECT penalty_percentage from question_answer where id_question = _row.id_question order by ordinal) as sorted_questions),
                    _row.ordinal,
                    true
                    );
        END LOOP;
    ELSE
        -- Non-fixed test, let's generate the questions and insert them:
        DROP TABLE IF EXISTS tpart;
        CREATE TEMP table tpart AS
            SELECT test_part.id AS id_test_part, id_node, id_grading_model, min_questions, max_questions, min_questions AS gen_questions
            FROM   test_part
            WHERE  test_part.id_test = _id_test;

        -- #1. Fullfil min_questions request:
        _generated := 0;
        FOR _row IN SELECT min_questions, id_node, id_grading_model, id_test_part
                    FROM tpart
                    WHERE min_questions > 0
        LOOP
            INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
            SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                  (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                  CASE WHEN (question.id_question_type = 9)
                    THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                    ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                  END,
                  (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
            FROM   question JOIN question_node
                            ON question.id = question_node.id_question
                            AND is_active
            WHERE  question_node.id_node = _row.id_node
            AND    question.id NOT IN (SELECT id_question FROM tiq)
            ORDER BY random()
            LIMIT _row.min_questions;
            _generated = _generated + _row.min_questions; -- If there are less than that availale in the DB, this var will lie! Better than inf loop.
        END LOOP;

        -- #2. Add optional questions to achieve targer q.no:
        -- if there are qs in multiple nodes then the question uniqueness criteria may cause less than LIMIT (_questions_no - _generated) to be inserted
        --   and the I'll try again few more times. NB: it might be impossible to reach the goal, that is why we have _retry counter.
        WHILE ((_retry < 5) AND ((SELECT COUNT(*) from tiq) < _questions_no)) LOOP
            FOR _row IN
                -- skew the distribution (with max 20) towards those with bigger upper limit (max_questions):
                SELECT id_node, id_grading_model, id_test_part
                FROM  tpart
                JOIN  generate_series(0, 20) idx
                ON    (tpart.max_questions - tpart.min_questions) > idx
                ORDER BY random()
                LIMIT (_questions_no - _generated)
            LOOP
                INSERT INTO tiq (id_test_part, id_question, id_question_type, id_grading_model, answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
                SELECT _row.id_test_part, question.id, question.id_question_type, _row.id_grading_model,
                      (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id),
                      (SELECT array_agg(ordinal order by ordinal) FROM question_answer where id_question = question.id and is_correct),
                      CASE WHEN (question.id_question_type = 9)
                        THEN (SELECT array_agg(1) FROM connected_elements_question_answer where id_question = question.id)
                        ELSE (SELECT array_agg(threshold_weight order by ordinal) FROM question_answer where id_question = question.id )
                      END,
                      (SELECT array_agg(penalty_percentage order by ordinal) from question_answer where id_question = question.id )
                FROM   question JOIN question_node
                                ON question.id = question_node.id_question
                                AND is_active
                WHERE  question_node.id_node = _row.id_node
                AND    question.id NOT IN (SELECT id_question FROM tiq)
                ORDER BY random()
                LIMIT 1;
                IF (_retry > 0) THEN
                    EXIT WHEN ((SELECT COUNT(*) from tiq) = _questions_no);
                END IF;
            END LOOP;
            _retry = _retry + 1;
        END LOOP;

    END IF;

    -- #3. (a) Set permutations for those questions that have multiple answers:
    FOR _row IN
    (
      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg(cqa.ordinal) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   connected_elements_question_answer cqa
        ON   tiq.id_question = cqa.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type

      UNION

      SELECT tiq.id_question,
             tiq.id_question_type,
             COUNT(*) AS no_answers,
             array_agg( case when question_answer.is_correct THEN question_answer.ordinal ELSE NULL END) AS correct_answers_permutation
      FROM   tiq
      JOIN   question_type
        ON   tiq.id_question_type = question_type.id
      JOIN   question_answer
        ON   tiq.id_question = question_answer.id_question
      WHERE  has_permutations = true
       AND  skip_permutations = false
      GROUP BY tiq.id_question, tiq.id_question_type
    )
    LOOP
      UPDATE tiq
      SET    answers_permutation = (
            WITH RECURSIVE r(n,p,a,b)
              AS (SELECT (SELECT trunc(random() * (factorial(_row.no_answers)::integer)))::integer, '{}'::INTEGER[],
                (SELECT array_agg(i::integer) from generate_series(1, _row.no_answers) i), _row.no_answers :: INTEGER
                UNION ALL
                SELECT n / b, p || a[n % b + 1], a[1:n % b] || a[n % b + 2:b], b-1
                FROM r
                WHERE b > 0)
            SELECT p  FROM r WHERE b=0
           ),
           correct_answers_permutation = _row.correct_answers_permutation -- still, to be updated
      WHERE tiq.id_question = _row.id_question;

    END LOOP;

    -- #3. (b) Update correct answers and weights:
    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT    array_agg(perm.nr)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   unnest(tiq.correct_answers_permutation) correct(ordinal)
            ON   perm.ordinal = correct.ordinal
          AND   correct.ordinal IS NOT NULL)
    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type NOT IN (8, 9);  -- TODO: fix this

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by ordinal)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          )
    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 8;

    UPDATE tiq
      SET    correct_answers_permutation =

        (SELECT  array_agg(perm.nr order by perm.ordinal)
           FROM  unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
           JOIN  connected_elements_question_answer cqa
             ON  tiq.id_question = cqa.id_question
            AND (trim(answer_text_static) <> '') IS TRUE  -- only those that have static answers, bcs there can be more dynamic than static answers
            AND perm.ordinal = cqa.ordinal
          )

    WHERE tiq.answers_permutation IS NOT NULL
      AND skip_permutations = false -- skip lecture quizzes and PAs;
      AND tiq.id_question_type = 9;  -- TODO: fix this

    UPDATE tiq
      SET    weights_permutation =

        (SELECT    array_agg(question_answer.threshold_weight)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false  -- skip lecture quizzes and PAs;
      AND tiq.id_question_type <> 9;  -- skip connected elements

    UPDATE tiq
      SET    penalty_percentage_permutation =

        (SELECT    array_agg(question_answer.penalty_percentage)
          FROM   unnest(tiq.answers_permutation) WITH ORDINALITY perm(ordinal, nr)
          JOIN   question_answer
            ON   perm.ordinal = question_answer.ordinal
          AND   question_answer.id_question = tiq.id_question)

    WHERE tiq.answers_permutation IS NOT NULL
      AND  skip_permutations = false;  -- skip lecture quizzes and PAs;

    -- **********************************************
    -- * Finally, save it:
    -- **********************************************
    INSERT INTO test_instance (id_test, id_student, ts_generated, ts_started, ip_address)
       VALUES (_id_test, _id_student, current_timestamp, NULL, _ip_address);

    _new_test_instance_id = (SELECT currval('test_instance_id_seq'));

    IF (_fixed_questions) THEN
        INSERT into test_instance_question (id_test_instance, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_question,
            (row_number() OVER (ORDER BY fixed_test_ordinal)) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        ORDER BY fixed_test_ordinal;

    ELSE
        INSERT into test_instance_question (id_test_instance, id_test_part, id_question, ordinal, id_grading_model,
            answers_permutation, correct_answers_permutation, weights_permutation, penalty_percentage_permutation)
        SELECT _new_test_instance_id,
            tiq.id_test_part,
            tiq.id_question,
            (row_number() OVER (ORDER BY test_part.ordinal, random())) :: smallint,
            tiq.id_grading_model,
            answers_permutation,
            correct_answers_permutation,
            weights_permutation,
            penalty_percentage_permutation
        FROM tiq
        JOIN test_part
            ON tiq.id_test_part = test_part.id
        ORDER BY test_part.ordinal, random();

        -- cleanup:
        DROP TABLE IF EXISTS tpart;
    END IF;

    -- cleanup:
    DROP TABLE if exists tiq;

    RETURN _new_test_instance_id;

END;
$$;

alter function gen_test_instance(integer, integer, varchar) owner to postgres;

grant execute on function gen_test_instance(integer, integer, varchar) to edgar;



create or replace function apply_manual_grading(_id_test integer) returns integer
    security definer
    language plpgsql
as
$$
DECLARE
  i INTEGER;
BEGIN
    -- Potentially update grading model for classic/test_part tests
    UPDATE test_instance_question
        SET id_grading_model =  test_part.id_grading_model
        FROM test_instance, test_part
        WHERE test_instance_question.id_test_part = test_part.id
        AND test_instance_question.id_grading_model <> test_part.id_grading_model
        AND test_instance_question.id_test_instance = test_instance.id
        AND test_instance.id_test = _id_test;

    -- Potentially update grading model for fixed qs tests
    UPDATE test_instance_question
        SET id_grading_model =  test_question.id_grading_model
        FROM test_question, test_instance
        WHERE test_question.id_test = _id_test
        AND test_instance_question.id_test_instance = test_instance.id
        AND test_instance.id_test = _id_test
        AND test_instance_question.ordinal = test_question.ordinal
        AND test_instance_question.id_grading_model <> test_question.id_grading_model;


    -- Update on the question level:
    UPDATE test_instance_question
    SET score = coalesce(man_grade.score, 0),
        score_perc = CASE WHEN (grading_model.correct_score = 0) THEN 1 ELSE (coalesce(man_grade.score, 0)) / grading_model.correct_score END,
        hint = 'Score assigned manually to: ' ||
                coalesce(man_grade.score, 0) || ' ('  || ROUND(100 * CASE WHEN (grading_model.correct_score = 0) THEN 1 ELSE (coalesce(man_grade.score, 0)) / grading_model.correct_score END, 1)
                || '%)'
    FROM test_instance_question proxy  -- I'm introducing "proxy" to achieve left join
      JOIN grading_model
        ON proxy.id_grading_model = grading_model.id
      JOIN test_instance
        ON proxy.id_test_instance = test_instance.id
       AND test_instance.id_test = _id_test
      LEFT JOIN test_instance_question_manual_grade man_grade
        ON proxy.id = man_grade.id_test_instance_question
    WHERE test_instance_question.id = proxy.id;

   UPDATE test_instance_question
      SET is_correct = CASE WHEN (test_instance_question.score_perc = 1) THEN true ELSE false END,
          is_incorrect = CASE WHEN (test_instance_question.score_perc < (gm.unanswered_score / (gm.correct_score - gm.incorrect_score))) THEN true ELSE false END,
          is_partial = CASE WHEN (test_instance_question.score_perc > (gm.unanswered_score / (gm.correct_score - gm.incorrect_score)) AND test_instance_question.score_perc < 1) THEN true ELSE false END,
          is_unanswered =  CASE WHEN (test_instance_question.score_perc = (gm.unanswered_score / (gm.correct_score - gm.incorrect_score))) THEN true ELSE false END
     FROM test_instance
     JOIN grading_model gm
       ON (gm.correct_score - gm.incorrect_score) > 0 -- further joined below, for some reason pg does not allow it here
    WHERE test_instance_question.id_test_instance = test_instance.id
      AND test_instance_question.id_grading_model = gm.id
      AND test_instance.id_test = _id_test;

    -- And on the test level, first score:
    UPDATE test_instance
       SET score = (SELECT SUM(score)
                   FROM test_instance_question
                   WHERE id_test_instance = test_instance.id)
    WHERE id_test = _id_test;
    -- ... and then score perc:
    UPDATE test_instance
       SET score_perc = CASE WHEN (test.max_score = 0) THEN 1 ELSE score / test.max_score END
      FROM test
     WHERE test_instance.id_test = test.id
       AND test_instance.id_test = _id_test;

    RETURN 0;
END;
$$;


create unique index uidx_eval_script_question on question_eval_script(id_question);

alter table c_question_test add ordinal smallint not null default 1;

CREATE TABLE public.homescreen_category (
    id            serial        primary key,
    id_course     integer       not null references course(id),
    category      text          not null,
    max_items_visible           smallint not null,
    ordinal       smallint      not null,
    ----------------------
    ts_created    timestamp with time zone not null,
    ts_modified   timestamp with time zone not null,
    user_created  varchar(50)              not null,
    user_modified varchar(50)              not null
);
SELECT audit.audit_table('homescreen_category');

CREATE TABLE public.homescreen_exam_category (
    id            serial        primary key,
    id_test       integer       not null references test(id),
    id_homescreen_category   integer       not null references homescreen_category(id),
    ordinal       smallint      not null,
    ----------------------
    ts_created    timestamp with time zone not null,
    ts_modified   timestamp with time zone not null,
    user_created  varchar(50)              not null,
    user_modified varchar(50)              not null
);
SELECT audit.audit_table('homescreen_exam_category');


--bugfix here:

create or replace function ins_email_queue_for_tutorial_ticket_subcription() returns trigger
    language plpgsql
as
$$
  --1. ticket nastane, status je 'open', id_assigned_user je NULL
  --2. ticket preuzme nastavnik, status je 'open', promijeni se id_assigned_user u NOT NULL
  --3. nastavnik odgovara, status je 'closed' --ovdje se jedino ne šalje ticket
  --4. student opet otvara ticket, status je 'open' --i dalje u krug
  DECLARE p_ticket_status VARCHAR(100);

  BEGIN
        --/*
   IF (TG_OP = 'INSERT') THEN
     p_ticket_status = 'NEW - Open';
   ELSE -- TG_OP = 'UPDATE' AND NEW.status = 'open' -ovo piše u WHEN
     -- p_ticket_status = 'OLD - Reopened';
     IF (OLD.id_assigned_user IS NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je preuzimanje ticketa
       SELECT 'ASSIGNED to user: ' || app_user.email INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
     ELSIF (OLD.id_assigned_user IS NOT NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je reopen ticketa i nalijepim usera da se odmah u mailu vidi treba li reagirati ili ignorirati
       SELECT 'OLD - REOPENED (previously assigned to user: ' || app_user.email || ')' INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
     END IF;
   END IF;
       -- */
     INSERT INTO email_queue (user_modified, mail_to, mail_cc, mail_bcc, mail_from, mail_subject, mail_text)
      SELECT 'postgres'
           , 'edgar@fer.hr'
           , ''
           , app_user.email
           , 'edgar@fer.hr'
           , 'Tutorial ticket (status: ' || p_ticket_status || ') - ' || ' - tutorial ' || tutorial.id || ' - ' || TRIM(tutorial.tutorial_title)
           , 'Tutorial ticket (status: ' || p_ticket_status || ') - ' || ' in tutorial ' || tutorial.id || ' - ' || TRIM(tutorial.tutorial_title) || '.'
                || chr(10) ||chr(10) || 'Step: ' || tutorial_step.ordinal :: text || ': ' || tutorial_step.title || chr(10) ||chr(10) ||
                'Access tickets for the tutorial using this link: https://edgar.fer.hr/tutorial/ticket/tutorial/' || tutorial.id || chr(10) || chr(10) ||
                'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
                'You have received this email because you (' || COALESCE(app_user.email, 'mail not set!?') || ') subscribed.' || chr(10)||chr(10)||
                'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
       FROM  tutorial_ticket
       JOIN  tutorial_step
         ON  tutorial_ticket.id_tutorial_step = tutorial_step.id
       JOIN  tutorial
         ON  tutorial_step.id_tutorial = tutorial.id
       JOIN  tutorial_ticket_subscription
         ON  tutorial_ticket_subscription.id_tutorial = tutorial.id
       JOIN  app_user
         ON  tutorial_ticket_subscription.id_app_user = app_user.id
      WHERE  tutorial_ticket.id = NEW.id;
     RETURN NEW;
  END;
$$;

alter function ins_email_queue_for_tutorial_ticket_subcription() owner to postgres;

create or replace function ins_email_queue_for_ticket_subcription() returns trigger
    language plpgsql
as
$$
   --1. ticket nastane, status je 'open', id_assigned_user je NULL
   --2. ticket preuzme nastavnik, status je 'open', promjeni se id_assigned_user u NOT NULL
   --3. nastavnik odgovara, status je 'closed' --ovdje se jedino ne šalje ticket
   --4. student opet otvara ticket, status je 'open' --i dalje u krug

   DECLARE p_ticket_status VARCHAR(100);

   BEGIN
         --/*
	  IF (TG_OP = 'INSERT') THEN
         p_ticket_status = 'new - open';
      ELSE -- TG_OP = 'UPDATE' AND NEW.status = 'open' -ovo piše u WHEN
	     p_ticket_status = 'old - reopened';

		 IF (OLD.id_assigned_user IS NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je preuzimanje ticketa
		    SELECT 'assigned to user: ' || app_user.email INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
         ELSIF (OLD.id_assigned_user IS NOT NULL AND NEW.id_assigned_user IS NOT NULL) THEN --ovo je reopen ticketa i nalijepim usera da se odmah u mailu vidi treba li reagirati ili ignorirati
            SELECT 'old - reopened (previously assigned to user: ' || app_user.email || ')' INTO p_ticket_status FROM app_user WHERE id = NEW.id_assigned_user;
         END IF;

	  END IF;
	  		-- */
      INSERT INTO email_queue (user_modified, mail_to, mail_cc, mail_bcc, mail_from, mail_subject, mail_text)

       SELECT 'postgres'
            , 'edgar@fer.hr', ''
	       	, app_user.email
            , 'edgar@fer.hr'
	       	, 'Ticket (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' - test ' || test.id || ' - ' || TRIM(test.title)
            , 'Ticket (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' in test ' || test.id || ' - ' || TRIM(test.title) || '.' || chr(10) ||chr(10) ||
	       	'Course: ' || course.course_name || chr(10) || chr(10) ||
            'Access tickets for the test using this link: https://edgar.fer.hr/ticket/test/' || test.id || '# '|| chr(10) || chr(10) ||
	       	'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	'You have received this email because you subscribed.' || chr(10)||chr(10)||
	       	'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
        FROM test_instance_question, test_instance, test, ticket, ticket_subscription, app_user, course
       WHERE test_instance_question.id_test_instance = test_instance.id
	        AND ticket.id = NEW.id
	        AND test_instance_question.id = ticket.id_test_instance_question
	        AND test_instance.id_test = test.id
	        AND ticket_subscription.id_test = test_instance.id_test
	        AND ticket_subscription.id_question = test_instance_question.id_question
	        AND ticket_subscription.id_app_user = app_user.id
            AND test.id_course = course.id
       UNION
       SELECT 'postgres'
            , 'edgar@fer.hr', ''
	       	, app_user.email
            , 'edgar@fer.hr'
	       	, 'Ticket (status: ' || p_ticket_status || ') - test ' || test.id || ' - ' || TRIM(test.title)
            , 'Ticket (status: ' || p_ticket_status || ') - question ' || test_instance_question.id_question || ' in test ' || test.id || ' - ' || TRIM(test.title) || '.' || chr(10) ||chr(10) ||
	       	'Course: ' || course.course_name || chr(10) || chr(10) ||
            'Access tickets for the test using this link: https://edgar.fer.hr/ticket/test/' || test.id || '# '|| chr(10) || chr(10) ||
	       	--'Ticket message: ' || (ticket.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	'Ticket message: ' || (NEW.comments[array_upper(comments, 1)]->'description')::text || chr(10) || chr(10) ||
	       	'You have received this email because you subscribed.' || chr(10)||chr(10)||
	       	'Please, do not reply to this message.' || chr(10)|| chr(10)|| 'Best,' ||chr(10)|| 'Edgar'
        FROM test_instance_question, test_instance, test, ticket, ticket_subscription, app_user, course
       WHERE test_instance_question.id_test_instance = test_instance.id
	        AND ticket.id = NEW.id
	        AND test_instance_question.id = ticket.id_test_instance_question
	        AND test_instance.id_test = test.id
	        AND ticket_subscription.id_test = test_instance.id_test
	        AND ticket_subscription.id_question IS NULL
	        AND ticket_subscription.id_app_user = app_user.id
            AND test.id_course = course.id;

      RETURN NEW;
   END;
$$;

COMMIT WORK;
