db.createCollection('testlogdetails', {
  capped: true,
  size: 20737418240, // 10GB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

db.createCollection('templatelogs', {
  capped: true,
  size: 10485760, // 10MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});
db.createCollection('scriptlogs', {
  capped: true,
  size: 10485760, // 10MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

db.createCollection('logentries', {
  capped: true,
  size: 10485760, // 10MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

db.createCollection('accesslog', {
  capped: true,
  size: 20971520, // 20MB
  storageEngine: {
    wiredTiger: { configString: 'block_compressor=zlib' },
  },
});

// In case you missed creating the collections and they were created as regular collections, you can convert them to capped collections:

// db.runCommand({
//   convertToCapped: 'accesslog',
//   size: 20971520,
//   writeConcern: { w: 0, wtimeout: 1000 },
//   comment: 'Convert 20971520 to capped collection of 20MB',
// });
// db.runCommand({
//   convertToCapped: 'logentries',
//   size: 10485760,
//   writeConcern: { w: 0, wtimeout: 1000 },
//   comment: 'Convert logentries to capped collection of 10MB',
// });
// db.runCommand({
//   convertToCapped: 'scriptlogs',
//   size: 10485760,
//   writeConcern: { w: 0, wtimeout: 1000 },
//   comment: 'Convert scriptlogs to capped collection of 10MB',
// });
// db.runCommand({
//   convertToCapped: 'templatelogs',
//   size: 10485760,
//   writeConcern: { w: 0, wtimeout: 1000 },
//   comment: 'Convert templatelogs to capped collection of 10MB',
// });
// db.runCommand({
//   convertToCapped: 'testlogdetails',
//   size: 20737418240,
//   writeConcern: { w: 0, wtimeout: 1000 },
//   comment: 'Convert testlogdetails to capped collection of 20GB',
// });
