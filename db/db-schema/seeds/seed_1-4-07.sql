INSERT INTO role VALUES (1, 'Admin');
INSERT INTO role VALUES (2, 'Teacher');

INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (1, 'Classic (ABC) question', true, true);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (2, 'Evaluated SQL question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (3, 'Evaluated C-lang question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (4, 'Classic (ABC) question without permutations', true, true);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (5, 'Homework question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (7, 'True/False question', true, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (8, 'OrderedElements', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (9, 'ConnectedElements', true, true);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (11, 'Diagram question', true, true);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (20, 'Free text', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (21, 'JSON result question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (22, 'Java question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (23, 'Code question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (25, 'Peer assessment calibration question', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (26, 'Scale (questionnaire)', false, false);
INSERT INTO question_type (id, type_name, has_answers, has_permutations) VALUES (27, 'R/RMD script', false, false);

INSERT INTO ticket_policy (id, policy_name, uses_tickets_exam, uses_tickets_review, uses_tickets_tutorial, uses_tickets_exercise) VALUES (1, 'Exam and review', true, true, false, false);
INSERT INTO ticket_policy (id, policy_name, uses_tickets_exam, uses_tickets_review, uses_tickets_tutorial, uses_tickets_exercise) VALUES (2, 'Exam only', true, false, false, false);
INSERT INTO ticket_policy (id, policy_name, uses_tickets_exam, uses_tickets_review, uses_tickets_tutorial, uses_tickets_exercise) VALUES (3, 'None (tickets disabled)', false, false, false, false);

INSERT INTO code_runner(name, host, port, path, db_schema) VALUES ('Code Runner', 'edgar_code_runner', 10084, '/run', NULL);
INSERT INTO code_runner(name, host, port, path, db_schema) VALUES ('Edgar', 'edgar_pg_runner_1', 10080, '/run', NULL);
INSERT INTO code_runner(name, host, port, path, db_schema) VALUES ('Chinook', 'edgar_pg_runner_2', 10080, '/run', 'chinook');
INSERT INTO code_runner(name, host, port, path, db_schema) VALUES ('SportsDB', 'edgar_pg_runner_2', 10080, '/run', 'sportsdb');
INSERT INTO code_runner(name, host, port, path, db_schema) VALUES ('Retail Analytics', 'edgar_pg_runner_2', 10080, '/run', 'retail_analytics');

INSERT INTO academic_year (id, title, date_start, date_end) VALUES (2023, '2023/2024', '2023-10-01', '2024-09-30');

INSERT INTO semester (id, title) VALUES (1, 'Winter semester');
INSERT INTO semester (id, title) VALUES (2, 'Spring semester');

INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (1, 'Classic ABC questions', true, false, false);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (8, 'Code questions (Java, C, Python, ...)', true, false, false);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (9, 'Competitive programming questions', true, false, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (10, 'Competition', true, true, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (12, 'Random, mixed questions type, exam', true, false, false);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (15, 'Evaluated SQL questions', true, false, false);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (16, 'Lecture quiz', false, true, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (21, 'Mongo queries (JSON)', true, false, false);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (30, 'Peer assessment PHASE1', true, false, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (31, 'Peer assessment PHASE2 MASTER', true, true, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (32, 'Peer assessment PHASE2', true, true, true);
INSERT INTO test_type (id, type_name, standalone, fixed_questions, skip_permutations) VALUES (33, 'Fixed questions exam', true, true, false);

INSERT INTO node_type (id, type_name, color) VALUES (3, 'Course', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (4, 'Module', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (5, 'Unit', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (7, 'Tutorial', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (8, 'Demo', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (10, 'TODO', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (11, 'Lecture quizz', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (12, 'Project', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (14, 'Manually graded exam', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (15, 'Homework', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (17, 'Midterm exam', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (18, 'Final exam', 'aliceblue');
INSERT INTO node_type (id, type_name, color) VALUES (19, 'Exam', 'aliceblue');

INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (0, '0/0/0', 0, 0, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (1, '15/0/-5', 15, -5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (2, '1/0/0', 1, 0, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (3, '10/0/3', 10, -3, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (4, '13/0/-3', 13, -3, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (5, '15/0/-4', 15, -4, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (6, '25/0/-5', 25, -5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (7, '2/0/-0,5', 2, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (8, '1/0/-0,25', 1, -0.25, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (9, '1/0/-0,2', 1, -0.2, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (10, '2,5/0/-0,5', 2.5, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (12, '0,25/0/-0,05', 0.25, -0.05, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (13, '0,3/0/-0,1', 0.3, -0.1, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (14, '0,5/0/-0,1', 0.5, -0.1, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (16, '1,2/0/-0,2', 1.2, -0.2, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (18, '1/0/-0,3', 1, -0.3, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (19, '0,5/0/0', 0.5, 0, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (20, '0,4/0/-0,15', 0.4, -0.15, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (21, '2,5/0/-0,5', 2.5, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (22, '1,5/0/-0,4', 1.5, -0.4, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (23, '1,5/0/-0,5', 1.5, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (25, '1/0/-0,5', 1, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (27, '1,25/0/-0,3', 1.25, -0.3, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (29, '3/0/-0,5', 3, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (30, '1,75/0/-0,5', 1.75, -0.5, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (32, '0,6/0/-0,12', 0.6, -0.12, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (33, '0,5/0/-0,2', 0.5, -0.2, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (36, '6/0/-1', 6, -1, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (37, '5/0/-1', 5, -1, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (38, '1,33/0/-0,33', 1.33, -0.33, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (39, '2/0/-0,6', 2, -0.6, 0);
INSERT INTO grading_model (id, model_name, correct_score, incorrect_score, unanswered_score) VALUES (40, '2/0/0', 2, 0, 0);

INSERT INTO app_user (id_role, first_name, last_name, username, alt_id, alt_id2, email, provider) VALUES (2, 'Edgar', 'Codd', 'edgar', 'edgar', 'edgar', '', 'local');
INSERT INTO student (first_name, last_name, alt_id, alt_id2, provider, id_app_user) VALUES ('Edgar', 'Student', 'edgar', 'edgar', 'local', 1);
INSERT INTO local_provider VALUES (1, 'edgar', '$2a$09$XTIfVbSDCyMFeGAbUTSZHu1bYvnOVPZvRdEbhjLX3jqL7aWGeff/i'); --- Password "edgar" generated with https://bcrypt-generator.com

INSERT INTO node (id_node_type, node_name, description) VALUES (3, 'ALCHEMY', 'root node for course Alchemy');
INSERT INTO course (course_name, course_acronym, ects_credits, id_root_node) VALUES ('Alchemy', 'ALC', 4, 1);

INSERT INTO perm_user_course (id_user, id_course) VALUES (1, 1);
INSERT INTO app_user_role(id_app_user, id_role) VALUES (1, 2);

INSERT INTO student_course (id_academic_year, id_course, id_student) VALUES (2023, 1, 1);

INSERT INTO c_test_type (id, test_type_name) VALUES (1, 'Fixed');
INSERT INTO c_test_type (id, test_type_name) VALUES (2, 'Random');
INSERT INTO c_test_type (id, test_type_name) VALUES (3, 'Generator');

INSERT INTO check_column_mode (id, mode_desc) VALUES (1, '1. STRICT: both column names and order must match.');
INSERT INTO check_column_mode (id, mode_desc) VALUES (2, '2. SQL: column order must match (column names ignored).');
INSERT INTO check_column_mode (id, mode_desc) VALUES (3, '3. RELALG: column names must match (column order ignored).');
INSERT INTO check_column_mode (id, mode_desc) VALUES (4, '4. PERMISSIVE: try 3 (to match by names); if not - try 2 (use column order).');

INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (0, 'Unknown (placeholder), do not delete!', null, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (1, 'Javascript', null, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (42, 'SQL', null, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (45, 'Assembly (NASM 2.14.02)', 45, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (46, 'Bash (5.0.0)', 46, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (47, 'Basic (FBC 1.07.1)', 47, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (75, 'C (Clang 7.0.1)', 75, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (76, 'C++ (Clang 7.0.1)', 76, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (48, 'C (GCC 7.4.0)', 48, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (52, 'C++ (GCC 7.4.0)', 52, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (49, 'C (GCC 8.3.0)', 49, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (53, 'C++ (GCC 8.3.0)', 53, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (50, 'C (GCC 9.2.0)', 50, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (54, 'C++ (GCC 9.2.0)', 54, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (86, 'Clojure (1.10.1)', 86, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (51, 'C# (Mono 6.6.0.161)', 51, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (77, 'COBOL (GnuCOBOL 2.2)', 77, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (55, 'Common Lisp (SBCL 2.0.0)', 55, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (56, 'D (DMD 2.089.1)', 56, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (57, 'Elixir (1.9.4)', 57, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (58, 'Erlang (OTP 22.2)', 58, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (44, 'Executable', 44, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (87, 'F# (.NET Core SDK 3.1.202)', 87, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (59, 'Fortran (GFortran 9.2.0)', 59, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (60, 'Go (1.13.5)', 60, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (88, 'Groovy (3.0.3)', 88, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (61, 'Haskell (GHC 8.8.1)', 61, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (62, 'Java (OpenJDK 13.0.1)', 62, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (63, 'JavaScript (Node.js 12.14.0)', 63, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (78, 'Kotlin (1.3.70)', 78, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (64, 'Lua (5.3.5)', 64, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (89, 'Multi-file program', 89, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (79, 'Objective-C (Clang 7.0.1)', 79, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (65, 'OCaml (4.09.0)', 65, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (66, 'Octave (5.1.0)', 66, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (67, 'Pascal (FPC 3.0.4)', 67, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (85, 'Perl (5.28.1)', 85, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (68, 'PHP (7.4.1)', 68, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (43, 'Plain Text', 43, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (69, 'Prolog (GNU Prolog 1.4.5)', 69, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (70, 'Python (2.7.17)', 70, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (71, 'Python (3.8.1)', 71, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (80, 'R (4.0.0)', 80, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (72, 'Ruby (2.7.0)', 72, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (73, 'Rust (1.40.0)', 73, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (81, 'Scala (2.13.2)', 81, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (82, 'SQL (SQLite 3.27.2)', 82, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (83, 'Swift (5.2.3)', 83, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (74, 'TypeScript (3.7.4)', 74, '');
INSERT INTO programming_language (id, name, judge0_id, hello_world) VALUES (84, 'Visual Basic.Net (vbnc 0.0.0.5943)', 84, '');

INSERT INTO course_code_runner (id_course, id_programming_language, id_code_runner, id_type)
SELECT 1, id, 1, 23
FROM programming_language
WHERE judge0_id IS NOT NULL
UNION
SELECT 1, 42, id, 2
FROM code_runner
WHERE id NOT IN (1, 2);

INSERT INTO playground_code_runner (id_course, id_code_runner, ordinal, student_can_see)
SELECT 1, 2, 2, false
UNION
SELECT 1, id, id, true
FROM code_runner
WHERE id NOT IN (1, 2);

INSERT INTO pace (pace_name, timeouts) VALUES ('q20: 10x0, 5x20, 5x60', '{0,0,0,0,0,0,0,0,0,0,20,20,20,20,20,60,60,60,60,60,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('t100: 50x0, 25x60, 25x120', '{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('Dummy: 0, 1, 2, ... 10, inf', '{0,1,2,3,4,5,6,7,8,9,10,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('UPRO exam pace (10x1, 10x3, 20x5)(mins)', '{60,60,60,60,60,60,60,60,60,60,180,180,180,180,180,180,180,180,180,180,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('(test) UPRO exam pace (10x1, 10x3, 20x5)(mins)', '{6,6,6,6,6,6,6,6,6,6,18,18,18,18,18,18,18,18,18,18,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('UPRO exam pace (24x5)(mins)', '{300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,300,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('t-test-mi: 10x0, 10x20, 10x60, 75x120', '{0,0,0,0,0,0,0,0,0,0,20,20,20,20,20,20,20,20,20,20,60,60,60,60,60,60,60,60,60,60,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,10000000}');
INSERT INTO pace (pace_name, timeouts) VALUES ('4 mins (4, 4, ..)', '{240}');
INSERT INTO pace (pace_name, timeouts) VALUES ('5 x 5 min', '{0,300,300,300,300,1000000}');

INSERT INTO email_reminder_scheme (name, thresholds_ongoing, thresholds_not_started) VALUES ('It''s alive (0% for not started)', '{}', '{0.00000000}');
INSERT INTO email_reminder_scheme (name, thresholds_ongoing, thresholds_not_started) VALUES ('Conservative (75%) + not started at 50% and 75%', '{0.75000000}', '{0.50000000,0.75000000}');
INSERT INTO email_reminder_scheme (name, thresholds_ongoing, thresholds_not_started) VALUES ('Default (75%, 90%) + not started at 50%', '{0.75000000,0.8999999761581421}', '{0.50000000,0.75000000,0.8999999761581421}');
INSERT INTO email_reminder_scheme (name, thresholds_ongoing, thresholds_not_started) VALUES ('Eager (75%, 90%) + not started at 0%, 50%', '{0.75000000,0.8999999761581421}', '{0.00000000,0.50000000,0.75000000,0.8999999761581421}');
INSERT INTO email_reminder_scheme (name, thresholds_ongoing, thresholds_not_started) VALUES ('Lazy (90%, 95%, 98%) + not started at 80%', '{0.8999999761581421,0.949999988079071,0.9800000190734863}', '{0.800000011920929,0.8999999761581421,0.949999988079071,0.9800000190734863}');

INSERT INTO plag_detection_algorithm (id, name) VALUES (1, 'Levenshtein distance');
INSERT INTO plag_detection_algorithm (id, name) VALUES (2, 'Jaro–Winkler distance');
INSERT INTO plag_detection_algorithm (id, name) VALUES (3, 'Sorensen–Dice coefficient');
INSERT INTO plag_detection_algorithm (id, name) VALUES (4, 'Longest substring');
INSERT INTO plag_detection_algorithm (id, name) VALUES (5, 'Bag distance');
INSERT INTO plag_detection_algorithm (id, name) VALUES (6, 'Jaccard distance');

INSERT INTO random_test_type (id, random_test_type_name) VALUES (1, 'intNumber');
INSERT INTO random_test_type (id, random_test_type_name) VALUES (2, 'floatNumber');
INSERT INTO random_test_type (id, random_test_type_name) VALUES (3, 'fixAlphaText');
INSERT INTO random_test_type (id, random_test_type_name) VALUES (4, 'fixAllText');
INSERT INTO random_test_type (id, random_test_type_name) VALUES (5, 'rangeAlphaText');
INSERT INTO random_test_type (id, random_test_type_name) VALUES (6, 'rangeAllText');

INSERT INTO global_data_object (id, data_object) VALUES (1, e'
{
  randomFloat: function(min, max) {
    return Math.random() * (max - min) + min;
  },

  randomInt: function(minInt, maxInt) {
    return Math.floor(Math.random() * (maxInt - minInt + 1)) + minInt;
  },

  currYear: function() {
    return (new Date()).getFullYear();
  },

  randomBoolean: function() {
    return !!this.randomInt(0, 1);
  },

  shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1)); // random index FROM 0 to i
      [array[i], array[j]] = [array[j], array[i]];
    }
  },

  getIncorrectScore(hint) {
    return {
      is_correct: false,
      is_incorrect: true,
      score: this.record.gradingModel.incorrect_score,
      score_perc: this.record.gradingModel.correct_score != 0 ? this.record.gradingModel.incorrect_score / this.record.gradingModel.correct_score : 0,
      hint,
      c_outcome: this.score.c_outcome
    };

  },

  getUnansweredScore(hint) {
    return {
      is_correct: false,
      is_incorrect: false,
      is_unanswered: true,
      score: this.record.gradingModel.unanswered_score,
      score_perc: this.record.gradingModel.correct_score != 0 ? this.record.gradingModel.unanswered_score / this.record.gradingModel.correct_score : 0,
      hint,
      c_outcome: this.score.c_outcome
    };

  },

  // removes C-style comments, single and multi-line
  removeComments(code) {
    return code.replace(/\\/\\*[\\s\\S]*?\\*\\/|\\/\\/.*/g, "");
  },

  removeWhitespace(code) {
    return code.replace(/\\s/g, "");
  },

  getCodeAnswer() {
    return this.record.student_answer_code || "";
  },

  getFreeTextAnswer() {
    return this.record.student_answer_text || "";
  },

  getAnswer() {
    return this.getCodeAnswer() + this.getFreeTextAnswer();
  }
}

');

INSERT INTO connected_elements_correctness_model (model_name, correctness) VALUES ('Strict, all or nothing', '{0}');
INSERT INTO connected_elements_correctness_model (model_name, correctness) VALUES ('100%, 75%, 50%, 0%', '{75,50}');
INSERT INTO connected_elements_correctness_model (model_name, correctness) VALUES ('100%, 90%, 70%, 0%', '{90,70}');
INSERT INTO connected_elements_correctness_model (model_name, correctness) VALUES ('100%, 50%, 0%', '{50}');

INSERT INTO adaptivity_model (name) VALUES ('easy > +3/5 -3/4 < normal > +4/7 -2/5 < hard');

INSERT INTO question_difficulty_level (id_adaptivity_model, name, ordinal, rgb_color) VALUES (1, 'easy', 1, '009900');
INSERT INTO question_difficulty_level (id_adaptivity_model, name, ordinal, rgb_color) VALUES (1, 'normal', 2, 'cccc00');
INSERT INTO question_difficulty_level (id_adaptivity_model, name, ordinal, rgb_color) VALUES (1, 'hard', 3, 'cc0000');

INSERT INTO adaptivity_model_behaviour (id_adaptivity_model, id_question_difficulty_level, level_up_questions_to_track, level_up_correct_questions, level_down_questions_to_track, level_down_incorrect_questions) VALUES (1, 1, 5, 3, 5, 5);
INSERT INTO adaptivity_model_behaviour (id_adaptivity_model, id_question_difficulty_level, level_up_questions_to_track, level_up_correct_questions, level_down_questions_to_track, level_down_incorrect_questions) VALUES (1, 2, 7, 4, 4, 3);
INSERT INTO adaptivity_model_behaviour (id_adaptivity_model, id_question_difficulty_level, level_up_questions_to_track, level_up_correct_questions, level_down_questions_to_track, level_down_incorrect_questions) VALUES (1, 3, 5, 5, 5, 2);

INSERT INTO ordered_element_correctness_model (model_name, correctness) VALUES ('Strict, all or nothing', '{0}');
INSERT INTO ordered_element_correctness_model (model_name, correctness) VALUES ('100%, 80%, 40%, 0%', '{80,40}');
INSERT INTO ordered_element_correctness_model (model_name, correctness) VALUES ('100%, 90%, 70%, 0%', '{90,70}');
INSERT INTO ordered_element_correctness_model (model_name, correctness) VALUES ('50%, 0%', '{50}');

INSERT INTO versions VALUES (147, '1.4.07');
