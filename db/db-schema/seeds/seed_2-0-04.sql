
update global_data_object
set data_object = '
{
  randomFloat: function(min, max) {
    return Math.random() * (max - min) + min;
  },
  randomInt: function(minInt, maxInt) {
    return Math.floor(Math.random() * (maxInt - minInt + 1)) + minInt;
  },
  currYear: function() {
    return (new Date()).getFullYear();
  },
  randomBoolean: function() {
    return !!this.randomInt(0, 1);
  },
  shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
      [array[i], array[j]] = [array[j], array[i]];
    }
  },
  getIncorrectScore(hint) {
    let correctScore =
        this.record.gradingModel.correct_score != undefined
            ? this.record.gradingModel.correct_score
            : this.record.gradingModel.correctScore;
    let incorrectScore =
        this.record.gradingModel.incorrect_score != undefined
            ? this.record.gradingModel.incorrect_score
            : this.record.gradingModel.incorrectScore;
    return {
        is_correct: false,
        is_incorrect: true,
        score: incorrectScore,
        score_perc: correctScore != 0 ? incorrectScore / correctScore : 0,
        hint,
        c_outcome: this.score.c_outcome,
    };
  },
  getUnansweredScore(hint) {
    let correctScore =
        this.record.gradingModel.correct_score != undefined
            ? this.record.gradingModel.correct_score
            : this.record.gradingModel.correctScore;
    let unansweredScore =
        this.record.gradingModel.unanswered_score != undefined
            ? this.record.gradingModel.unanswered_score
            : this.record.gradingModel.unansweredScore;
    return {
        is_correct: false,
        is_incorrect: false,
        is_unanswered: true,
        score: unansweredScore,
        score_perc: correctScore != 0 ? unansweredScore / correctScore : 0,
        hint,
        c_outcome: this.score.c_outcome,
    };
  },
  // removes C-style comments, single and multi-line
  removeComments(code) {
    return code.replace(/\/\*[\s\S]*?\*\/|\/\/.*/g, "");
  },
  removeWhitespace(code) {
    return code.replace(/\s/g, "");
  },
  getCodeAnswer() {
    return this.record.student_answer_code || this.record.studentAnswerCode || "";
  },
  getFreeTextAnswer() {
    return this.record.student_answer_text || this.record.studentAnswerText || "";
  },
  getAnswer() {
    return this.getCodeAnswer() + this.getFreeTextAnswer();
  }
}' where 1=1;

INSERT INTO versions VALUES (2004, '2.0.04');

