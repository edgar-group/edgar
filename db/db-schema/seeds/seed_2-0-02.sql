INSERT into question_type (id, type_name, has_answers, has_permutations) values (30, 'Complex (project upload) question', false, false);
--insert into programming_language (id, name, judge0_id, extension) values (46, 'Bash (5.0.0.)', 46, 'sh');
UPDATE programming_language SET extension = 'sh' WHERE id = 46;

insert into programming_language (id, name, hello_world) values (2, 'Mongo MR', '');

insert into question_programming_language (id_question, id_programming_language)
select id, 2 from question where id_question_type = 21
   and not exists (select * From question_programming_language where id_question = question.id);

insert into question_programming_language (id_question, id_programming_language)
select id, 42 from question where id_question_type = 2
   and not exists (select * From question_programming_language where id_question = question.id);

update c_question_test set ordinal = 1
   + (select count(*) from c_question_test tinner
        where tinner.c_question_answer_id = c_question_test.c_question_answer_id
          and tinner.id < c_question_test.id)
where 1 = 1;


INSERT INTO versions VALUES (2002, '2.0.02');

