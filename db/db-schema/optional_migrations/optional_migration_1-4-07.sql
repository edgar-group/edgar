-- Sets the "edgar" password for students that have an app_user associated with them (eg alterago student of teachers).
CREATE OR REPLACE FUNCTION insert_into_local_provider()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.id_app_user IS NOT NULL) THEN
        INSERT INTO local_provider (id_student, username, password) VALUES (NEW.id, (SELECT username FROM app_user WHERE id = NEW.id_app_user), '$2a$09$XTIfVbSDCyMFeGAbUTSZHu1bYvnOVPZvRdEbhjLX3jqL7aWGeff/i');
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_insert_into_local_provider AFTER INSERT ON student FOR EACH ROW EXECUTE FUNCTION insert_into_local_provider();
