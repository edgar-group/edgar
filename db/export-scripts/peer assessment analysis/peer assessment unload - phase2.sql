-- *******************************************************************************************************************
-- *******************************************************************************************************************
-- *******************************************************************************************************************
-- ********************************************** PHASE 2  ***********************************************************
-- *******************************************************************************************************************
-- *******************************************************************************************************************
-- *******************************************************************************************************************
-- drop table submitted;
-- drop table pa2;
-- drop table pa3;
-- drop table pa4;
-- drop table pa5;
-- drop table pa6;
-- drop table pa7;
-- drop table pa8;

--create view v_calib_question_answer as 
--select id_question_calib, ptgq.ordinal, value
--  from peer_test_group_question_calib ptgqc 
--  join peer_test_group_question ptgq on ptgqc.id_peer_test_group_question = ptgq.id
--  JOIN scale_item
--    on ptgqc.id_scale_item = scale_item.id;

-- DROP TABLE PTG;
create TEMP table ptg as 
select 'G' || dense_rank() over( order by id_question ) as group, 0 as QCNT, * 
  From peer_Test_group 
 where id_peer_test = (
select pt.id
  from peer_test pt    
 where pt.id_test_phase_1 =  12963);


update ptg
set qcnt = (select count(*) from peer_Test_group_question where id_peer_Test_group = ptg.id); --- 2 ;-- izbacujem free text i cjelokupni dojam

CREATE TEMP TABLE SUBMITTED AS 
select distinct id_student, last_name, ptg2.*
  from test_instance
  join student
     on test_instance.id_Student = student.id

JOIN (

select distinct id_Student1, ptg.*
 from v_peer_shuffle
 join ptg
   on v_peer_shuffle.id_question_pa1 = ptg.id_question
   )  ptg2
  on id_Student = id_student1
  
  where ts_submitted is not null
   and id_test in  (select id from test where id_parent = 12968);


-- PA2
create temp table pa2 as 
select submitted.*, question_node.id_question as id_calib_question 
, (SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY ordinal2
	) as rank, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_Student1 = submitted.id_student
    AND v_peer_shuffle.id_test_phase_2 = 12968) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = submitted.id_student
WHERE reviews.rank = 1 ) as ti21
, (SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY ordinal2
	) as rank, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_Student1 = submitted.id_student
    AND v_peer_shuffle.id_test_phase_2 = 12968) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = submitted.id_student
WHERE reviews.rank = 2 ) as ti22
, (SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY ordinal2
	) as rank, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_Student1 = submitted.id_student
    AND v_peer_shuffle.id_test_phase_2 = 12968) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = submitted.id_student
WHERE reviews.rank = 3 ) as ti23
, (SELECT ti.id
    FROM
	(SELECT dense_rank() OVER(				    
	    ORDER BY ordinal2
	) as rank, id_pa_test
	    FROM v_peer_shuffle
	WHERE id_Student1 = submitted.id_student
    AND v_peer_shuffle.id_test_phase_2 = 12968) reviews
JOIN test_instance ti
ON ti.id_test = reviews.id_pa_test
AND ti.id_student = submitted.id_student
WHERE reviews.rank = 4 ) as ti24
from submitted
  join question_node
    on submitted.id_calib_assessments_node = question_node.id_node;
  

-- where submitted.id_student = 5488
drop table ptg;
drop table submitted;

-- PA3:
create temp table pa3 as
select 
  case 
    when jobs[1] < 0 then 1
    when jobs[2] < 0 then 2
    when jobs[3] < 0 then 3
    when jobs[4] < 0 then 4
    when jobs[5] < 0 then 5
  end as calib_job_ordinal,
  jobs, pa2.* 
 from pa2 
 join peer_test
   on pa2.id_peer_test = peer_test.id
 join test_instance ti1
   on ti1.id_test = id_test_phase_1
   and ti1.id_student = pa2.id_student
 join test_instance_question tiq1
   on ti1.id = tiq1.id_test_instance
 join peer_shuffle 
   on peer_shuffle.id_test_instance_question = tiq1.id;

drop table pa2;

create temp table pa4 as 
select pa3.*, test_instance.id as ti_calib
 From pa3
  join test 
    on test.id_parent = 12968
   and test_ordinal = (select test_ordinal from test where id = 12968) + calib_job_ordinal
  left join test_instance
    on test_instance.id_Test = test.id
    and test_instance.id_Student = pa3.id_Student;

  drop table pa3;

-- PA5:
  create temp table pa5 as
select pa4.*
-- 1:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 1 ) as q1_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 2 ) as q2_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 3 ) as q3_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 4 ) as q4_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 5 ) as q5_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 6 ) as q6_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 7 ) as q7_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 8 ) as q8_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 9 ) as q9_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 10 ) as q10_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 11 ) as q11_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 12 ) as q12_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 13 ) as q13_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 14 ) as q14_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 15 ) as q15_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 16 ) as q16_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 17 ) as q17_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 18 ) as q18_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 19 ) as q19_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 20 ) as q20_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 21 ) as q21_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 22 ) as q22_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 23 ) as q23_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 24 ) as q24_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 25 ) as q25_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 26 ) as q26_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 27 ) as q27_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 28 ) as q28_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 29 ) as q29_1
  , (select student_answers[1] From test_instance_question where id_test_instance = ti21 and ordinal = 30 ) as q30_1
  -- 2:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 1 ) as q1_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 2 ) as q2_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 3 ) as q3_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 4 ) as q4_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 5 ) as q5_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 6 ) as q6_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 7 ) as q7_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 8 ) as q8_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 9 ) as q9_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 10 ) as q10_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 11 ) as q11_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 12 ) as q12_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 13 ) as q13_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 14 ) as q14_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 15 ) as q15_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 16 ) as q16_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 17 ) as q17_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 18 ) as q18_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 19 ) as q19_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 20 ) as q20_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 21 ) as q21_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 22 ) as q22_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 23 ) as q23_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 24 ) as q24_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 25 ) as q25_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 26 ) as q26_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 27 ) as q27_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 28 ) as q28_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 29 ) as q29_2
  , (select student_answers[1] From test_instance_question where id_test_instance = ti22 and ordinal = 30 ) as q30_2
  -- 3:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 1 ) as q1_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 2 ) as q2_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 3 ) as q3_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 4 ) as q4_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 5 ) as q5_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 6 ) as q6_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 7 ) as q7_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 8 ) as q8_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 9 ) as q9_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 10 ) as q10_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 11 ) as q11_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 12 ) as q12_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 13 ) as q13_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 14 ) as q14_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 15 ) as q15_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 16 ) as q16_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 17 ) as q17_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 18 ) as q18_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 19 ) as q19_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 20 ) as q20_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 21 ) as q21_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 22 ) as q22_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 23 ) as q23_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 24 ) as q24_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 25 ) as q25_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 26 ) as q26_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 27 ) as q27_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 28 ) as q28_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 29 ) as q29_3
  , (select student_answers[1] From test_instance_question where id_test_instance = ti23 and ordinal = 30 ) as q30_3
  -- 2:
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 1 ) as q1_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 2 ) as q2_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 3 ) as q3_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 4 ) as q4_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 5 ) as q5_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 6 ) as q6_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 7 ) as q7_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 8 ) as q8_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 9 ) as q9_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 10 ) as q10_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 11 ) as q11_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 12 ) as q12_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 13 ) as q13_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 14 ) as q14_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 15 ) as q15_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 16 ) as q16_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 17 ) as q17_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 18 ) as q18_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 19 ) as q19_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 20 ) as q20_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 21 ) as q21_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 22 ) as q22_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 23 ) as q23_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 24 ) as q24_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 25 ) as q25_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 26 ) as q26_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 27 ) as q27_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 28 ) as q28_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 29 ) as q29_4
  , (select student_answers[1] From test_instance_question where id_test_instance = ti24 and ordinal = 30 ) as q30_4
--   
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 1 ) as q1_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 2 ) as q2_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 3 ) as q3_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 4 ) as q4_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 5 ) as q5_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 6 ) as q6_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 7 ) as q7_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 8 ) as q8_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 9 ) as q9_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 10 ) as q10_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 11 ) as q11_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 12 ) as q12_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 13 ) as q13_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 14 ) as q14_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 15 ) as q15_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 16 ) as q16_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 17 ) as q17_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 18 ) as q18_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 19 ) as q19_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 20 ) as q20_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 21 ) as q21_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 22 ) as q22_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 23 ) as q23_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 24 ) as q24_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 25 ) as q25_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 26 ) as q26_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 27 ) as q27_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 28 ) as q28_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 29 ) as q29_calib
  , (select student_answers[1] From test_instance_question where id_test_instance = ti_calib and ordinal = 30 ) as q30_calib
--
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 1 ) as q1_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 2 ) as q2_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 3 ) as q3_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 4 ) as q4_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 5 ) as q5_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 6 ) as q6_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 7 ) as q7_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 8 ) as q8_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 9 ) as q9_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 10 ) as q10_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 11 ) as q11_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 12 ) as q12_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 13 ) as q13_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 14 ) as q14_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 15 ) as q15_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 16 ) as q16_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 17 ) as q17_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 18 ) as q18_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 19 ) as q19_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 20 ) as q20_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 21 ) as q21_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 22 ) as q22_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 23 ) as q23_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 24 ) as q24_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 25 ) as q25_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 26 ) as q26_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 27 ) as q27_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 28 ) as q28_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 29 ) as q29_calib_crct
  , (select value From v_calib_question_answer where id_calib_question = id_question_calib and ordinal = 30 ) as q30_calib_crct

  , 0 as opgrade1
  , 0 as opgrade2
  , 0 as opgrade3
  , 0 as opgrade4
  , 0 as opgrade_calib
  , 0 as opgrade_calib_crct
  
 from pa4;


drop table pa4;
--  "G1";21
-- "G2";23
-- "G3";21
-- "G4";25
-- "G5";21
-- "G6";22
-- "G7";23
-- "G8";24
-- "G9";22
-- "G10";19
-- kpiram "cjelokupni dojam" u posebno pitanje
UPDATE pa5 SET opgrade1 = 
CASE 
    WHEN qcnt = 30 THEN q30_1
    WHEN qcnt = 29 THEN q29_1
    WHEN qcnt = 28 THEN q28_1
    WHEN qcnt = 27 THEN q27_1
    WHEN qcnt = 26 THEN q26_1
    WHEN qcnt = 25 THEN q25_1
    WHEN qcnt = 24 THEN q24_1
    WHEN qcnt = 23 THEN q23_1
    WHEN qcnt = 22 THEN q22_1
    WHEN qcnt = 21 THEN q21_1
    WHEN qcnt = 20 THEN q20_1
    WHEN qcnt = 19 THEN q19_1
    WHEN qcnt = 18 THEN q18_1
    WHEN qcnt = 17 THEN q17_1
    WHEN qcnt = 16 THEN q16_1
    WHEN qcnt = 15 THEN q15_1
    WHEN qcnt = 14 THEN q14_1
    WHEN qcnt = 13 THEN q13_1
    WHEN qcnt = 12 THEN q12_1
    WHEN qcnt = 11 THEN q11_1
    WHEN qcnt = 10 THEN q10_1
    WHEN qcnt = 9 THEN q9_1
    WHEN qcnt = 8 THEN q8_1
    WHEN qcnt = 7 THEN q7_1
    WHEN qcnt = 6 THEN q6_1
    WHEN qcnt = 5 THEN q5_1
    WHEN qcnt = 4 THEN q4_1
    WHEN qcnt = 3 THEN q3_1
    WHEN qcnt = 2 THEN q2_1
    WHEN qcnt = 1 THEN q1_1
END;
UPDATE pa5 SET opgrade2 = 
CASE 
    WHEN qcnt = 30 THEN q30_2
    WHEN qcnt = 29 THEN q29_2
    WHEN qcnt = 28 THEN q28_2
    WHEN qcnt = 27 THEN q27_2
    WHEN qcnt = 26 THEN q26_2
    WHEN qcnt = 25 THEN q25_2
    WHEN qcnt = 24 THEN q24_2
    WHEN qcnt = 23 THEN q23_2
    WHEN qcnt = 22 THEN q22_2
    WHEN qcnt = 21 THEN q21_2
    WHEN qcnt = 20 THEN q20_2
    WHEN qcnt = 19 THEN q19_2
    WHEN qcnt = 18 THEN q18_2
    WHEN qcnt = 17 THEN q17_2
    WHEN qcnt = 16 THEN q16_2
    WHEN qcnt = 15 THEN q15_2
    WHEN qcnt = 14 THEN q14_2
    WHEN qcnt = 13 THEN q13_2
    WHEN qcnt = 12 THEN q12_2
    WHEN qcnt = 11 THEN q11_2
    WHEN qcnt = 10 THEN q10_2
    WHEN qcnt = 9 THEN q9_2
    WHEN qcnt = 8 THEN q8_2
    WHEN qcnt = 7 THEN q7_2
    WHEN qcnt = 6 THEN q6_2
    WHEN qcnt = 5 THEN q5_2
    WHEN qcnt = 4 THEN q4_2
    WHEN qcnt = 3 THEN q3_2
    WHEN qcnt = 2 THEN q2_2
    WHEN qcnt = 1 THEN q1_2
END;
UPDATE pa5 SET opgrade3 = 
CASE 
    WHEN qcnt = 30 THEN q30_3
    WHEN qcnt = 29 THEN q29_3
    WHEN qcnt = 28 THEN q28_3
    WHEN qcnt = 27 THEN q27_3
    WHEN qcnt = 26 THEN q26_3
    WHEN qcnt = 25 THEN q25_3
    WHEN qcnt = 24 THEN q24_3
    WHEN qcnt = 23 THEN q23_3
    WHEN qcnt = 22 THEN q22_3
    WHEN qcnt = 21 THEN q21_3
    WHEN qcnt = 20 THEN q20_3
    WHEN qcnt = 19 THEN q19_3
    WHEN qcnt = 18 THEN q18_3
    WHEN qcnt = 17 THEN q17_3
    WHEN qcnt = 16 THEN q16_3
    WHEN qcnt = 15 THEN q15_3
    WHEN qcnt = 14 THEN q14_3
    WHEN qcnt = 13 THEN q13_3
    WHEN qcnt = 12 THEN q12_3
    WHEN qcnt = 11 THEN q11_3
    WHEN qcnt = 10 THEN q10_3
    WHEN qcnt = 9 THEN q9_3
    WHEN qcnt = 8 THEN q8_3
    WHEN qcnt = 7 THEN q7_3
    WHEN qcnt = 6 THEN q6_3
    WHEN qcnt = 5 THEN q5_3
    WHEN qcnt = 4 THEN q4_3
    WHEN qcnt = 3 THEN q3_3
    WHEN qcnt = 2 THEN q2_3
    WHEN qcnt = 1 THEN q1_3
END;
UPDATE pa5 SET opgrade4 = 
CASE 
    WHEN qcnt = 30 THEN q30_4
    WHEN qcnt = 29 THEN q29_4
    WHEN qcnt = 28 THEN q28_4
    WHEN qcnt = 27 THEN q27_4
    WHEN qcnt = 26 THEN q26_4
    WHEN qcnt = 25 THEN q25_4
    WHEN qcnt = 24 THEN q24_4
    WHEN qcnt = 23 THEN q23_4
    WHEN qcnt = 22 THEN q22_4
    WHEN qcnt = 21 THEN q21_4
    WHEN qcnt = 20 THEN q20_4
    WHEN qcnt = 19 THEN q19_4
    WHEN qcnt = 18 THEN q18_4
    WHEN qcnt = 17 THEN q17_4
    WHEN qcnt = 16 THEN q16_4
    WHEN qcnt = 15 THEN q15_4
    WHEN qcnt = 14 THEN q14_4
    WHEN qcnt = 13 THEN q13_4
    WHEN qcnt = 12 THEN q12_4
    WHEN qcnt = 11 THEN q11_4
    WHEN qcnt = 10 THEN q10_4
    WHEN qcnt = 9 THEN q9_4
    WHEN qcnt = 8 THEN q8_4
    WHEN qcnt = 7 THEN q7_4
    WHEN qcnt = 6 THEN q6_4
    WHEN qcnt = 5 THEN q5_4
    WHEN qcnt = 4 THEN q4_4
    WHEN qcnt = 3 THEN q3_4
    WHEN qcnt = 2 THEN q2_4
    WHEN qcnt = 1 THEN q1_4
END;



-- update pa5 set opgrade_calib = q21_calib where pa5.group = 'G1';

UPDATE pa5 SET opgrade_calib = 
CASE 
    WHEN qcnt = 30 THEN q30_calib
    WHEN qcnt = 29 THEN q29_calib
    WHEN qcnt = 28 THEN q28_calib
    WHEN qcnt = 27 THEN q27_calib
    WHEN qcnt = 26 THEN q26_calib
    WHEN qcnt = 25 THEN q25_calib
    WHEN qcnt = 24 THEN q24_calib
    WHEN qcnt = 23 THEN q23_calib
    WHEN qcnt = 22 THEN q22_calib
    WHEN qcnt = 21 THEN q21_calib
    WHEN qcnt = 20 THEN q20_calib
    WHEN qcnt = 19 THEN q19_calib
    WHEN qcnt = 18 THEN q18_calib
    WHEN qcnt = 17 THEN q17_calib
    WHEN qcnt = 16 THEN q16_calib
    WHEN qcnt = 15 THEN q15_calib
    WHEN qcnt = 14 THEN q14_calib
    WHEN qcnt = 13 THEN q13_calib
    WHEN qcnt = 12 THEN q12_calib
    WHEN qcnt = 11 THEN q11_calib
    WHEN qcnt = 10 THEN q10_calib
    WHEN qcnt = 9 THEN q9_calib
    WHEN qcnt = 8 THEN q8_calib
    WHEN qcnt = 7 THEN q7_calib
    WHEN qcnt = 6 THEN q6_calib
    WHEN qcnt = 5 THEN q5_calib
    WHEN qcnt = 4 THEN q4_calib
    WHEN qcnt = 3 THEN q3_calib
    WHEN qcnt = 2 THEN q2_calib
    WHEN qcnt = 1 THEN q1_calib
END;
UPDATE pa5 SET opgrade_calib_crct = 
CASE 
    WHEN qcnt = 30 THEN q30_calib_crct
    WHEN qcnt = 29 THEN q29_calib_crct
    WHEN qcnt = 28 THEN q28_calib_crct
    WHEN qcnt = 27 THEN q27_calib_crct
    WHEN qcnt = 26 THEN q26_calib_crct
    WHEN qcnt = 25 THEN q25_calib_crct
    WHEN qcnt = 24 THEN q24_calib_crct
    WHEN qcnt = 23 THEN q23_calib_crct
    WHEN qcnt = 22 THEN q22_calib_crct
    WHEN qcnt = 21 THEN q21_calib_crct
    WHEN qcnt = 20 THEN q20_calib_crct
    WHEN qcnt = 19 THEN q19_calib_crct
    WHEN qcnt = 18 THEN q18_calib_crct
    WHEN qcnt = 17 THEN q17_calib_crct
    WHEN qcnt = 16 THEN q16_calib_crct
    WHEN qcnt = 15 THEN q15_calib_crct
    WHEN qcnt = 14 THEN q14_calib_crct
    WHEN qcnt = 13 THEN q13_calib_crct
    WHEN qcnt = 12 THEN q12_calib_crct
    WHEN qcnt = 11 THEN q11_calib_crct
    WHEN qcnt = 10 THEN q10_calib_crct
    WHEN qcnt = 9 THEN q9_calib_crct
    WHEN qcnt = 8 THEN q8_calib_crct
    WHEN qcnt = 7 THEN q7_calib_crct
    WHEN qcnt = 6 THEN q6_calib_crct
    WHEN qcnt = 5 THEN q5_calib_crct
    WHEN qcnt = 4 THEN q4_calib_crct
    WHEN qcnt = 3 THEN q3_calib_crct
    WHEN qcnt = 2 THEN q2_calib_crct
    WHEN qcnt = 1 THEN q1_calib_crct
END;

-- zatim poništavam ta zadnja dva pitanja:
-- Prvo zadnje:
UPDATE pa5 SET q30_4 = null, q30_3 = null, q30_2 = null, q30_1 = null where qcnt = 30;
UPDATE pa5 SET q29_4 = null, q29_3 = null, q29_2 = null, q29_1 = null where qcnt = 29;
UPDATE pa5 SET q28_4 = null, q28_3 = null, q28_2 = null, q28_1 = null where qcnt = 28;
UPDATE pa5 SET q27_4 = null, q27_3 = null, q27_2 = null, q27_1 = null where qcnt = 27;
UPDATE pa5 SET q26_4 = null, q26_3 = null, q26_2 = null, q26_1 = null where qcnt = 26;
UPDATE pa5 SET q25_4 = null, q25_3 = null, q25_2 = null, q25_1 = null where qcnt = 25;
UPDATE pa5 SET q24_4 = null, q24_3 = null, q24_2 = null, q24_1 = null where qcnt = 24;
UPDATE pa5 SET q23_4 = null, q23_3 = null, q23_2 = null, q23_1 = null where qcnt = 23;
UPDATE pa5 SET q22_4 = null, q22_3 = null, q22_2 = null, q22_1 = null where qcnt = 22;
UPDATE pa5 SET q21_4 = null, q21_3 = null, q21_2 = null, q21_1 = null where qcnt = 21;
UPDATE pa5 SET q20_4 = null, q20_3 = null, q20_2 = null, q20_1 = null where qcnt = 20;
UPDATE pa5 SET q19_4 = null, q19_3 = null, q19_2 = null, q19_1 = null where qcnt = 19;
UPDATE pa5 SET q18_4 = null, q18_3 = null, q18_2 = null, q18_1 = null where qcnt = 18;
UPDATE pa5 SET q17_4 = null, q17_3 = null, q17_2 = null, q17_1 = null where qcnt = 17;
UPDATE pa5 SET q16_4 = null, q16_3 = null, q16_2 = null, q16_1 = null where qcnt = 16;
UPDATE pa5 SET q15_4 = null, q15_3 = null, q15_2 = null, q15_1 = null where qcnt = 15;
UPDATE pa5 SET q14_4 = null, q14_3 = null, q14_2 = null, q14_1 = null where qcnt = 14;
UPDATE pa5 SET q13_4 = null, q13_3 = null, q13_2 = null, q13_1 = null where qcnt = 13;
UPDATE pa5 SET q12_4 = null, q12_3 = null, q12_2 = null, q12_1 = null where qcnt = 12;
UPDATE pa5 SET q11_4 = null, q11_3 = null, q11_2 = null, q11_1 = null where qcnt = 11;
UPDATE pa5 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 10;
UPDATE pa5 SET q9_4 = null, q9_3 = null, q9_2 = null, q9_1 = null where qcnt = 9;
UPDATE pa5 SET q8_4 = null, q8_3 = null, q8_2 = null, q8_1 = null where qcnt = 8;
UPDATE pa5 SET q7_4 = null, q7_3 = null, q7_2 = null, q7_1 = null where qcnt = 7;
UPDATE pa5 SET q6_4 = null, q6_3 = null, q6_2 = null, q6_1 = null where qcnt = 6;
UPDATE pa5 SET q5_4 = null, q5_3 = null, q5_2 = null, q5_1 = null where qcnt = 5;
UPDATE pa5 SET q4_4 = null, q4_3 = null, q4_2 = null, q4_1 = null where qcnt = 4;
UPDATE pa5 SET q3_4 = null, q3_3 = null, q3_2 = null, q3_1 = null where qcnt = 3;
UPDATE pa5 SET q2_4 = null, q2_3 = null, q2_2 = null, q2_1 = null where qcnt = 2;
-- zatim predzadnje:
-- ovo zapravo ne treba - to je text pitanje i ono je null 
-- osim ako Pintar ne poreda krivo pitanja, pa stavi text na kraj, onda ipak treba:
UPDATE pa5 SET q29_4 = null, q29_3 = null, q29_2 = null, q29_1 = null where qcnt = 30;
UPDATE pa5 SET q28_4 = null, q28_3 = null, q28_2 = null, q28_1 = null where qcnt = 29;
UPDATE pa5 SET q27_4 = null, q27_3 = null, q27_2 = null, q27_1 = null where qcnt = 28;
UPDATE pa5 SET q26_4 = null, q26_3 = null, q26_2 = null, q26_1 = null where qcnt = 27;
UPDATE pa5 SET q25_4 = null, q25_3 = null, q25_2 = null, q25_1 = null where qcnt = 26;
UPDATE pa5 SET q24_4 = null, q24_3 = null, q24_2 = null, q24_1 = null where qcnt = 25;
UPDATE pa5 SET q23_4 = null, q23_3 = null, q23_2 = null, q23_1 = null where qcnt = 24;
UPDATE pa5 SET q22_4 = null, q22_3 = null, q22_2 = null, q22_1 = null where qcnt = 23;
UPDATE pa5 SET q21_4 = null, q21_3 = null, q21_2 = null, q21_1 = null where qcnt = 22;
UPDATE pa5 SET q20_4 = null, q20_3 = null, q20_2 = null, q20_1 = null where qcnt = 21;
UPDATE pa5 SET q19_4 = null, q19_3 = null, q19_2 = null, q19_1 = null where qcnt = 20;
UPDATE pa5 SET q18_4 = null, q18_3 = null, q18_2 = null, q18_1 = null where qcnt = 19;
UPDATE pa5 SET q17_4 = null, q17_3 = null, q17_2 = null, q17_1 = null where qcnt = 18;
UPDATE pa5 SET q16_4 = null, q16_3 = null, q16_2 = null, q16_1 = null where qcnt = 17;
UPDATE pa5 SET q15_4 = null, q15_3 = null, q15_2 = null, q15_1 = null where qcnt = 16;
UPDATE pa5 SET q14_4 = null, q14_3 = null, q14_2 = null, q14_1 = null where qcnt = 15;
UPDATE pa5 SET q13_4 = null, q13_3 = null, q13_2 = null, q13_1 = null where qcnt = 14;
UPDATE pa5 SET q12_4 = null, q12_3 = null, q12_2 = null, q12_1 = null where qcnt = 13;
UPDATE pa5 SET q11_4 = null, q11_3 = null, q11_2 = null, q11_1 = null where qcnt = 12;
UPDATE pa5 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 11;
UPDATE pa5 SET q10_4 = null, q10_3 = null, q10_2 = null, q10_1 = null where qcnt = 11;
UPDATE pa5 SET q9_4 = null, q9_3 = null, q9_2 = null, q9_1 = null where qcnt = 10;
UPDATE pa5 SET q8_4 = null, q8_3 = null, q8_2 = null, q8_1 = null where qcnt = 9;
UPDATE pa5 SET q7_4 = null, q7_3 = null, q7_2 = null, q7_1 = null where qcnt = 8;
UPDATE pa5 SET q6_4 = null, q6_3 = null, q6_2 = null, q6_1 = null where qcnt = 7;
UPDATE pa5 SET q5_4 = null, q5_3 = null, q5_2 = null, q5_1 = null where qcnt = 6;
UPDATE pa5 SET q4_4 = null, q4_3 = null, q4_2 = null, q4_1 = null where qcnt = 5;
UPDATE pa5 SET q3_4 = null, q3_3 = null, q3_2 = null, q3_1 = null where qcnt = 4;
UPDATE pa5 SET q2_4 = null, q2_3 = null, q2_2 = null, q2_1 = null where qcnt = 3;

--Zatim i kalibracijske:

UPDATE pa5 SET q30_calib = null, q30_calib_crct = null where qcnt = 30;
UPDATE pa5 SET q29_calib = null, q29_calib_crct = null where qcnt = 29;
UPDATE pa5 SET q28_calib = null, q28_calib_crct = null where qcnt = 28;
UPDATE pa5 SET q27_calib = null, q27_calib_crct = null where qcnt = 27;
UPDATE pa5 SET q26_calib = null, q26_calib_crct = null where qcnt = 26;
UPDATE pa5 SET q25_calib = null, q25_calib_crct = null where qcnt = 25;
UPDATE pa5 SET q24_calib = null, q24_calib_crct = null where qcnt = 24;
UPDATE pa5 SET q23_calib = null, q23_calib_crct = null where qcnt = 23;
UPDATE pa5 SET q22_calib = null, q22_calib_crct = null where qcnt = 22;
UPDATE pa5 SET q21_calib = null, q21_calib_crct = null where qcnt = 21;
UPDATE pa5 SET q20_calib = null, q20_calib_crct = null where qcnt = 20;
UPDATE pa5 SET q19_calib = null, q19_calib_crct = null where qcnt = 19;
UPDATE pa5 SET q18_calib = null, q18_calib_crct = null where qcnt = 18;
UPDATE pa5 SET q17_calib = null, q17_calib_crct = null where qcnt = 17;
UPDATE pa5 SET q16_calib = null, q16_calib_crct = null where qcnt = 16;
UPDATE pa5 SET q15_calib = null, q15_calib_crct = null where qcnt = 15;
UPDATE pa5 SET q14_calib = null, q14_calib_crct = null where qcnt = 14;
UPDATE pa5 SET q13_calib = null, q13_calib_crct = null where qcnt = 13;
UPDATE pa5 SET q12_calib = null, q12_calib_crct = null where qcnt = 12;
UPDATE pa5 SET q11_calib = null, q11_calib_crct = null where qcnt = 11;
UPDATE pa5 SET q10_calib = null, q10_calib_crct = null where qcnt = 10;
UPDATE pa5 SET q9_calib = null, q9_calib_crct = null  where qcnt = 9;
UPDATE pa5 SET q8_calib = null, q8_calib_crct = null  where qcnt = 8;
UPDATE pa5 SET q7_calib = null, q7_calib_crct = null  where qcnt = 7;
UPDATE pa5 SET q6_calib = null, q6_calib_crct = null  where qcnt = 6;
UPDATE pa5 SET q5_calib = null, q5_calib_crct = null  where qcnt = 5;
UPDATE pa5 SET q4_calib = null, q4_calib_crct = null  where qcnt = 4;
UPDATE pa5 SET q3_calib = null, q3_calib_crct = null  where qcnt = 3;
UPDATE pa5 SET q2_calib = null, q2_calib_crct = null  where qcnt = 2;
-- i predzadnje kalibracijske:

UPDATE pa5 SET q29_calib = null, q29_calib_crct = null where qcnt = 30  ;
UPDATE pa5 SET q28_calib = null, q28_calib_crct = null where qcnt = 29  ;
UPDATE pa5 SET q27_calib = null, q27_calib_crct = null where qcnt = 28  ;
UPDATE pa5 SET q26_calib = null, q26_calib_crct = null where qcnt = 27  ;
UPDATE pa5 SET q25_calib = null, q25_calib_crct = null where qcnt = 26  ;
UPDATE pa5 SET q24_calib = null, q24_calib_crct = null where qcnt = 25  ;
UPDATE pa5 SET q23_calib = null, q23_calib_crct = null where qcnt = 24  ;
UPDATE pa5 SET q22_calib = null, q22_calib_crct = null where qcnt = 23  ;
UPDATE pa5 SET q21_calib = null, q21_calib_crct = null where qcnt = 22  ;
UPDATE pa5 SET q20_calib = null, q20_calib_crct = null where qcnt = 21  ;
UPDATE pa5 SET q19_calib = null, q19_calib_crct = null where qcnt = 20  ;
UPDATE pa5 SET q18_calib = null, q18_calib_crct = null where qcnt = 19  ;
UPDATE pa5 SET q17_calib = null, q17_calib_crct = null where qcnt = 18  ;
UPDATE pa5 SET q16_calib = null, q16_calib_crct = null where qcnt = 17  ;
UPDATE pa5 SET q15_calib = null, q15_calib_crct = null where qcnt = 16  ;
UPDATE pa5 SET q14_calib = null, q14_calib_crct = null where qcnt = 15  ;
UPDATE pa5 SET q13_calib = null, q13_calib_crct = null where qcnt = 14  ;
UPDATE pa5 SET q12_calib = null, q12_calib_crct = null where qcnt = 13  ;
UPDATE pa5 SET q11_calib = null, q11_calib_crct = null where qcnt = 12  ;
UPDATE pa5 SET q10_calib = null, q10_calib_crct = null where qcnt = 11  ;
UPDATE pa5 SET q9_calib = null, q9_calib_crct = null  where qcnt =  10  ;
UPDATE pa5 SET q8_calib = null, q8_calib_crct = null  where qcnt = 9;
UPDATE pa5 SET q7_calib = null, q7_calib_crct = null  where qcnt = 8;
UPDATE pa5 SET q6_calib = null, q6_calib_crct = null  where qcnt = 7;
UPDATE pa5 SET q5_calib = null, q5_calib_crct = null  where qcnt = 6;
UPDATE pa5 SET q4_calib = null, q4_calib_crct = null  where qcnt = 5;
UPDATE pa5 SET q3_calib = null, q3_calib_crct = null  where qcnt = 4;
UPDATE pa5 SET q2_calib = null, q2_calib_crct = null  where qcnt = 3;



 
create temp table pa6 as 
select pa5.*
  ,
  coalesce(q1_1,0) + 
  coalesce(q2_1,0) + 
  coalesce(q3_1,0) + 
  coalesce(q4_1,0) + 
  coalesce(q5_1,0) + 
  coalesce(q6_1,0) + 
  coalesce(q7_1,0) + 
  coalesce(q8_1,0) + 
  coalesce(q9_1,0) + 
  coalesce(q10_1,0) + 
  coalesce(q11_1,0) + 
  coalesce(q12_1,0) + 
  coalesce(q13_1,0) + 
  coalesce(q14_1,0) + 
  coalesce(q15_1,0) + 
  coalesce(q16_1,0) + 
  coalesce(q17_1,0) + 
  coalesce(q18_1,0) + 
  coalesce(q19_1,0) + 
  coalesce(q20_1,0) + 
  coalesce(q21_1,0) + 
  coalesce(q22_1,0) + 
  coalesce(q23_1,0) + 
  coalesce(q24_1,0) + 
  coalesce(q25_1,0) + 
  coalesce(q26_1,0) + 
  coalesce(q27_1,0) + 
  coalesce(q28_1,0) + 
  coalesce(q29_1,0) + 
  coalesce(q30_1,0)  
  as q1t
  ,
  coalesce(q1_2, 0) + 
  coalesce(q2_2, 0) + 
  coalesce(q3_2, 0) + 
  coalesce(q4_2, 0) + 
  coalesce(q5_2, 0) + 
  coalesce(q6_2, 0) + 
  coalesce(q7_2, 0) + 
  coalesce(q8_2, 0) + 
  coalesce(q9_2, 0) + 
  coalesce(q10_2, 0) + 
  coalesce(q11_2, 0) + 
  coalesce(q12_2, 0) + 
  coalesce(q13_2, 0) + 
  coalesce(q14_2, 0) + 
  coalesce(q15_2, 0) + 
  coalesce(q16_2, 0) + 
  coalesce(q17_2, 0) + 
  coalesce(q18_2, 0) + 
  coalesce(q19_2, 0) + 
  coalesce(q20_2, 0) + 
  coalesce(q21_2, 0) + 
  coalesce(q22_2, 0) + 
  coalesce(q23_2, 0) + 
  coalesce(q24_2, 0) + 
  coalesce(q25_2, 0) +
  coalesce(q26_2, 0) +
  coalesce(q27_2, 0) +
  coalesce(q28_2, 0) +
  coalesce(q29_2, 0) +
  coalesce(q30_2, 0) 
  as q2t
  ,
  coalesce(q1_3, 0) + 
  coalesce(q2_3, 0) + 
  coalesce(q3_3, 0) + 
  coalesce(q4_3, 0) + 
  coalesce(q5_3, 0) + 
  coalesce(q6_3, 0) + 
  coalesce(q7_3, 0) + 
  coalesce(q8_3, 0) + 
  coalesce(q9_3, 0) + 
  coalesce(q10_3, 0) + 
  coalesce(q11_3, 0) + 
  coalesce(q12_3, 0) + 
  coalesce(q13_3, 0) + 
  coalesce(q14_3, 0) + 
  coalesce(q15_3, 0) + 
  coalesce(q16_3, 0) + 
  coalesce(q17_3, 0) + 
  coalesce(q18_3, 0) + 
  coalesce(q19_3, 0) + 
  coalesce(q20_3, 0) + 
  coalesce(q21_3, 0) + 
  coalesce(q22_3, 0) + 
  coalesce(q23_3, 0) + 
  coalesce(q24_3, 0) + 
  coalesce(q25_3, 0) +
  coalesce(q26_3, 0) +
  coalesce(q27_3, 0) +
  coalesce(q28_3, 0) +
  coalesce(q29_3, 0) +
  coalesce(q30_3, 0) 
  as q3t
  ,
  coalesce(q1_4,0) + 
  coalesce(q2_4,0) + 
  coalesce(q3_4,0) + 
  coalesce(q4_4,0) + 
  coalesce(q5_4,0) + 
  coalesce(q6_4,0) + 
  coalesce(q7_4,0) + 
  coalesce(q8_4,0) + 
  coalesce(q9_4,0) + 
  coalesce(q10_4,0) + 
  coalesce(q11_4,0) + 
  coalesce(q12_4,0) + 
  coalesce(q13_4,0) + 
  coalesce(q14_4,0) + 
  coalesce(q15_4,0) + 
  coalesce(q16_4,0) + 
  coalesce(q17_4,0) + 
  coalesce(q18_4,0) + 
  coalesce(q19_4,0) + 
  coalesce(q20_4,0) + 
  coalesce(q21_4,0) + 
  coalesce(q22_4,0) + 
  coalesce(q23_4,0) + 
  coalesce(q24_4,0) + 
  coalesce(q25_4,0) +
  coalesce(q26_4,0) +
  coalesce(q27_4,0) +
  coalesce(q28_4,0) +
  coalesce(q29_4,0) +
  coalesce(q30_4,0)  
  as q4t
  ,
  coalesce(q1_calib,0) + 
  coalesce(q2_calib,0) + 
  coalesce(q3_calib,0) + 
  coalesce(q4_calib,0) + 
  coalesce(q5_calib,0) + 
  coalesce(q6_calib,0) + 
  coalesce(q7_calib,0) + 
  coalesce(q8_calib,0) + 
  coalesce(q9_calib,0) + 
  coalesce(q10_calib,0) + 
  coalesce(q11_calib,0) + 
  coalesce(q12_calib,0) + 
  coalesce(q13_calib,0) + 
  coalesce(q14_calib,0) + 
  coalesce(q15_calib,0) + 
  coalesce(q16_calib,0) + 
  coalesce(q17_calib,0) + 
  coalesce(q18_calib,0) + 
  coalesce(q19_calib,0) + 
  coalesce(q20_calib,0) + 
  coalesce(q21_calib,0) + 
  coalesce(q22_calib,0) + 
  coalesce(q23_calib,0) + 
  coalesce(q24_calib,0) + 
  coalesce(q25_calib,0) +
  coalesce(q26_calib,0) +
  coalesce(q27_calib,0) +
  coalesce(q28_calib,0) +
  coalesce(q29_calib,0) +
  coalesce(q30_calib,0) 
  as qcalibt
  ,
  coalesce(q1_calib_crct,0) + 
  coalesce(q2_calib_crct,0) + 
  coalesce(q3_calib_crct,0) + 
  coalesce(q4_calib_crct,0) + 
  coalesce(q5_calib_crct,0) + 
  coalesce(q6_calib_crct,0) + 
  coalesce(q7_calib_crct,0) + 
  coalesce(q8_calib_crct,0) + 
  coalesce(q9_calib_crct,0) + 
  coalesce(q10_calib_crct,0) + 
  coalesce(q11_calib_crct,0) + 
  coalesce(q12_calib_crct,0) + 
  coalesce(q13_calib_crct,0) + 
  coalesce(q14_calib_crct,0) + 
  coalesce(q15_calib_crct,0) + 
  coalesce(q16_calib_crct,0) + 
  coalesce(q17_calib_crct,0) + 
  coalesce(q18_calib_crct,0) + 
  coalesce(q19_calib_crct,0) + 
  coalesce(q20_calib_crct,0) + 
  coalesce(q21_calib_crct,0) + 
  coalesce(q22_calib_crct,0) + 
  coalesce(q23_calib_crct,0) + 
  coalesce(q24_calib_crct,0) + 
  coalesce(q25_calib_crct,0) +
  coalesce(q26_calib_crct,0) +
  coalesce(q27_calib_crct,0) +
  coalesce(q28_calib_crct,0) +
  coalesce(q29_calib_crct,0) +
  coalesce(q30_calib_crct,0) 
  as qcct
  ,
  1.0 * (
  case when (q1_1 is null) then 0 else 1 end +
  case when (q2_1 is null) then 0 else 1 end +
  case when (q3_1 is null) then 0 else 1 end +
  case when (q4_1 is null) then 0 else 1 end +
  case when (q5_1 is null) then 0 else 1 end +
  case when (q6_1 is null) then 0 else 1 end +
  case when (q7_1 is null) then 0 else 1 end +
  case when (q8_1 is null) then 0 else 1 end +
  case when (q9_1 is null) then 0 else 1 end +
  case when (q10_1 is null) then 0 else 1 end +
  case when (q11_1 is null) then 0 else 1 end +
  case when (q12_1 is null) then 0 else 1 end +
  case when (q13_1 is null) then 0 else 1 end +
  case when (q14_1 is null) then 0 else 1 end +
  case when (q15_1 is null) then 0 else 1 end +
  case when (q16_1 is null) then 0 else 1 end +
  case when (q17_1 is null) then 0 else 1 end +
  case when (q18_1 is null) then 0 else 1 end +
  case when (q19_1 is null) then 0 else 1 end +
  case when (q20_1 is null) then 0 else 1 end +
  case when (q21_1 is null) then 0 else 1 end +
  case when (q22_1 is null) then 0 else 1 end +
  case when (q23_1 is null) then 0 else 1 end +
  case when (q24_1 is null) then 0 else 1 end +
  case when (q25_1 is null) then 0 else 1 end +
  case when (q26_1 is null) then 0 else 1 end +
  case when (q27_1 is null) then 0 else 1 end +
  case when (q28_1 is null) then 0 else 1 end +
  case when (q29_1 is null) then 0 else 1 end +
  case when (q30_1 is null) then 0 else 1 end 
  ) / (qcnt - 2) as support1,

  1.0 * (
  case when (q1_2 is null) then 0 else 1 end +
  case when (q2_2 is null) then 0 else 1 end +
  case when (q3_2 is null) then 0 else 1 end +
  case when (q4_2 is null) then 0 else 1 end +
  case when (q5_2 is null) then 0 else 1 end +
  case when (q6_2 is null) then 0 else 1 end +
  case when (q7_2 is null) then 0 else 1 end +
  case when (q8_2 is null) then 0 else 1 end +
  case when (q9_2 is null) then 0 else 1 end +
  case when (q10_2 is null) then 0 else 1 end +
  case when (q11_2 is null) then 0 else 1 end +
  case when (q12_2 is null) then 0 else 1 end +
  case when (q13_2 is null) then 0 else 1 end +
  case when (q14_2 is null) then 0 else 1 end +
  case when (q15_2 is null) then 0 else 1 end +
  case when (q16_2 is null) then 0 else 1 end +
  case when (q17_2 is null) then 0 else 1 end +
  case when (q18_2 is null) then 0 else 1 end +
  case when (q19_2 is null) then 0 else 1 end +
  case when (q20_2 is null) then 0 else 1 end +
  case when (q21_2 is null) then 0 else 1 end +
  case when (q22_2 is null) then 0 else 1 end +
  case when (q23_2 is null) then 0 else 1 end +
  case when (q24_2 is null) then 0 else 1 end +
  case when (q25_2 is null) then 0 else 1 end +
  case when (q26_2 is null) then 0 else 1 end +
  case when (q27_2 is null) then 0 else 1 end +
  case when (q28_2 is null) then 0 else 1 end +
  case when (q29_2 is null) then 0 else 1 end +
  case when (q30_2 is null) then 0 else 1 end 
  ) / (qcnt - 2) as support2,

  1.0 * (
  case when (q1_3 is null) then 0 else 1 end +
  case when (q2_3 is null) then 0 else 1 end +
  case when (q3_3 is null) then 0 else 1 end +
  case when (q4_3 is null) then 0 else 1 end +
  case when (q5_3 is null) then 0 else 1 end +
  case when (q6_3 is null) then 0 else 1 end +
  case when (q7_3 is null) then 0 else 1 end +
  case when (q8_3 is null) then 0 else 1 end +
  case when (q9_3 is null) then 0 else 1 end +
  case when (q10_3 is null) then 0 else 1 end +
  case when (q11_3 is null) then 0 else 1 end +
  case when (q12_3 is null) then 0 else 1 end +
  case when (q13_3 is null) then 0 else 1 end +
  case when (q14_3 is null) then 0 else 1 end +
  case when (q15_3 is null) then 0 else 1 end +
  case when (q16_3 is null) then 0 else 1 end +
  case when (q17_3 is null) then 0 else 1 end +
  case when (q18_3 is null) then 0 else 1 end +
  case when (q19_3 is null) then 0 else 1 end +
  case when (q20_3 is null) then 0 else 1 end +
  case when (q21_3 is null) then 0 else 1 end +
  case when (q22_3 is null) then 0 else 1 end +
  case when (q23_3 is null) then 0 else 1 end +
  case when (q24_3 is null) then 0 else 1 end +
  case when (q25_3 is null) then 0 else 1 end +
  case when (q26_3 is null) then 0 else 1 end +
  case when (q27_3 is null) then 0 else 1 end +
  case when (q28_3 is null) then 0 else 1 end +
  case when (q29_3 is null) then 0 else 1 end +
  case when (q30_3 is null) then 0 else 1 end 
  ) / (qcnt - 2) as support3,


  1.0 * (
  case when (q1_4 is null) then 0 else 1 end +
  case when (q2_4 is null) then 0 else 1 end +
  case when (q3_4 is null) then 0 else 1 end +
  case when (q4_4 is null) then 0 else 1 end +
  case when (q5_4 is null) then 0 else 1 end +
  case when (q6_4 is null) then 0 else 1 end +
  case when (q7_4 is null) then 0 else 1 end +
  case when (q8_4 is null) then 0 else 1 end +
  case when (q9_4 is null) then 0 else 1 end +
  case when (q10_4 is null) then 0 else 1 end +
  case when (q11_4 is null) then 0 else 1 end +
  case when (q12_4 is null) then 0 else 1 end +
  case when (q13_4 is null) then 0 else 1 end +
  case when (q14_4 is null) then 0 else 1 end +
  case when (q15_4 is null) then 0 else 1 end +
  case when (q16_4 is null) then 0 else 1 end +
  case when (q17_4 is null) then 0 else 1 end +
  case when (q18_4 is null) then 0 else 1 end +
  case when (q19_4 is null) then 0 else 1 end +
  case when (q20_4 is null) then 0 else 1 end +
  case when (q21_4 is null) then 0 else 1 end +
  case when (q22_4 is null) then 0 else 1 end +
  case when (q23_4 is null) then 0 else 1 end +
  case when (q24_4 is null) then 0 else 1 end +
  case when (q25_4 is null) then 0 else 1 end +
  case when (q26_4 is null) then 0 else 1 end +
  case when (q27_4 is null) then 0 else 1 end +
  case when (q28_4 is null) then 0 else 1 end +
  case when (q29_4 is null) then 0 else 1 end +
  case when (q30_4 is null) then 0 else 1 end 
  ) / (qcnt - 2) as support4,



  1.0 * (
  case when (q1_calib is null) then 0 else 1 end +
  case when (q2_calib is null) then 0 else 1 end +
  case when (q3_calib is null) then 0 else 1 end +
  case when (q4_calib is null) then 0 else 1 end +
  case when (q5_calib is null) then 0 else 1 end +
  case when (q6_calib is null) then 0 else 1 end +
  case when (q7_calib is null) then 0 else 1 end +
  case when (q8_calib is null) then 0 else 1 end +
  case when (q9_calib is null) then 0 else 1 end +
  case when (q10_calib is null) then 0 else 1 end +
  case when (q11_calib is null) then 0 else 1 end +
  case when (q12_calib is null) then 0 else 1 end +
  case when (q13_calib is null) then 0 else 1 end +
  case when (q14_calib is null) then 0 else 1 end +
  case when (q15_calib is null) then 0 else 1 end +
  case when (q16_calib is null) then 0 else 1 end +
  case when (q17_calib is null) then 0 else 1 end +
  case when (q18_calib is null) then 0 else 1 end +
  case when (q19_calib is null) then 0 else 1 end +
  case when (q20_calib is null) then 0 else 1 end +
  case when (q21_calib is null) then 0 else 1 end +
  case when (q22_calib is null) then 0 else 1 end +
  case when (q23_calib is null) then 0 else 1 end +
  case when (q24_calib is null) then 0 else 1 end +
  case when (q25_calib is null) then 0 else 1 end +
  case when (q26_calib is null) then 0 else 1 end +
  case when (q27_calib is null) then 0 else 1 end +
  case when (q28_calib is null) then 0 else 1 end +
  case when (q29_calib is null) then 0 else 1 end +
  case when (q30_calib is null) then 0 else 1 end 
  ) / (qcnt - 2) as supportcalib,

  1.0 * (
  case when (q1_calib_crct is null) then 0 else 1 end +
  case when (q2_calib_crct is null) then 0 else 1 end +
  case when (q3_calib_crct is null) then 0 else 1 end +
  case when (q4_calib_crct is null) then 0 else 1 end +
  case when (q5_calib_crct is null) then 0 else 1 end +
  case when (q6_calib_crct is null) then 0 else 1 end +
  case when (q7_calib_crct is null) then 0 else 1 end +
  case when (q8_calib_crct is null) then 0 else 1 end +
  case when (q9_calib_crct is null) then 0 else 1 end +
  case when (q10_calib_crct is null) then 0 else 1 end +
  case when (q11_calib_crct is null) then 0 else 1 end +
  case when (q12_calib_crct is null) then 0 else 1 end +
  case when (q13_calib_crct is null) then 0 else 1 end +
  case when (q14_calib_crct is null) then 0 else 1 end +
  case when (q15_calib_crct is null) then 0 else 1 end +
  case when (q16_calib_crct is null) then 0 else 1 end +
  case when (q17_calib_crct is null) then 0 else 1 end +
  case when (q18_calib_crct is null) then 0 else 1 end +
  case when (q19_calib_crct is null) then 0 else 1 end +
  case when (q20_calib_crct is null) then 0 else 1 end +
  case when (q21_calib_crct is null) then 0 else 1 end +
  case when (q22_calib_crct is null) then 0 else 1 end +
  case when (q23_calib_crct is null) then 0 else 1 end +
  case when (q24_calib_crct is null) then 0 else 1 end +
  case when (q25_calib_crct is null) then 0 else 1 end +
  case when (q26_calib_crct is null) then 0 else 1 end +
  case when (q27_calib_crct is null) then 0 else 1 end +
  case when (q28_calib_crct is null) then 0 else 1 end +
  case when (q29_calib_crct is null) then 0 else 1 end +
  case when (q30_calib_crct is null) then 0 else 1 end 
  ) / (qcnt - 2) as supportcalibcrct


from pa5;

drop table pa5;


create temp table pa7 as 
select pa6.*,
case when (q1_calib is null)  then null else q1_calib - q1_calib_crct   end as q1_bias, 
case when (q2_calib is null)  then null else q2_calib - q2_calib_crct   end as q2_bias, 
case when (q3_calib is null)  then null else q3_calib - q3_calib_crct   end as q3_bias, 
case when (q4_calib is null)  then null else q4_calib - q4_calib_crct   end as q4_bias, 
case when (q5_calib is null)  then null else q5_calib - q5_calib_crct   end as q5_bias, 
case when (q6_calib is null)  then null else q6_calib - q6_calib_crct   end as q6_bias, 
case when (q7_calib is null)  then null else q7_calib - q7_calib_crct   end as q7_bias, 
case when (q8_calib is null)  then null else q8_calib - q8_calib_crct   end as q8_bias, 
case when (q9_calib is null)  then null else q9_calib - q9_calib_crct   end as q9_bias, 
case when (q10_calib is null) then null else q10_calib - q10_calib_crct   end as q10_bias, 
case when (q11_calib is null) then null else q11_calib - q11_calib_crct   end as q11_bias, 
case when (q12_calib is null) then null else q12_calib - q12_calib_crct   end as q12_bias, 
case when (q13_calib is null) then null else q13_calib - q13_calib_crct   end as q13_bias, 
case when (q14_calib is null) then null else q14_calib - q14_calib_crct   end as q14_bias, 
case when (q15_calib is null) then null else q15_calib - q15_calib_crct   end as q15_bias, 
case when (q16_calib is null) then null else q16_calib - q16_calib_crct   end as q16_bias, 
case when (q17_calib is null) then null else q17_calib - q17_calib_crct   end as q17_bias, 
case when (q18_calib is null) then null else q18_calib - q18_calib_crct   end as q18_bias, 
case when (q19_calib is null) then null else q19_calib - q19_calib_crct   end as q19_bias, 
case when (q20_calib is null) then null else q20_calib - q20_calib_crct   end as q20_bias, 
case when (q21_calib is null) then null else q21_calib - q21_calib_crct   end as q21_bias, 
case when (q22_calib is null) then null else q22_calib - q22_calib_crct   end as q22_bias, 
case when (q23_calib is null) then null else q23_calib - q23_calib_crct   end as q23_bias, 
case when (q24_calib is null) then null else q24_calib - q24_calib_crct   end as q24_bias, 
case when (q25_calib is null) then null else q25_calib - q25_calib_crct   end as q25_bias,
case when (q26_calib is null) then null else q26_calib - q26_calib_crct   end as q26_bias,
case when (q27_calib is null) then null else q27_calib - q27_calib_crct   end as q27_bias,
case when (q28_calib is null) then null else q28_calib - q28_calib_crct   end as q28_bias,
case when (q29_calib is null) then null else q29_calib - q29_calib_crct   end as q29_bias,
case when (q30_calib is null) then null else q30_calib - q30_calib_crct   end as q30_bias

from pa6;

drop table pa6;

create temp table pa8 as 
select pa7.*, 
case when (q1_bias is null) then 0 else q1_bias end +
case when (q2_bias is null) then 0 else q2_bias end +
case when (q3_bias is null) then 0 else q3_bias end +
case when (q4_bias is null) then 0 else q4_bias end +
case when (q5_bias is null) then 0 else q5_bias end +
case when (q6_bias is null) then 0 else q6_bias end +
case when (q7_bias is null) then 0 else q7_bias end +
case when (q8_bias is null) then 0 else q8_bias end +
case when (q9_bias is null) then 0 else q9_bias end +
case when (q10_bias is null) then 0 else q10_bias end +
case when (q11_bias is null) then 0 else q11_bias end +
case when (q12_bias is null) then 0 else q12_bias end +
case when (q13_bias is null) then 0 else q13_bias end +
case when (q14_bias is null) then 0 else q14_bias end +
case when (q15_bias is null) then 0 else q15_bias end +
case when (q16_bias is null) then 0 else q16_bias end +
case when (q17_bias is null) then 0 else q17_bias end +
case when (q18_bias is null) then 0 else q18_bias end +
case when (q19_bias is null) then 0 else q19_bias end +
case when (q20_bias is null) then 0 else q20_bias end +
case when (q21_bias is null) then 0 else q21_bias end +
case when (q22_bias is null) then 0 else q22_bias end +
case when (q23_bias is null) then 0 else q23_bias end +
case when (q24_bias is null) then 0 else q24_bias end +
case when (q25_bias is null) then 0 else q25_bias end +
case when (q26_bias is null) then 0 else q26_bias end +
case when (q27_bias is null) then 0 else q27_bias end +
case when (q28_bias is null) then 0 else q28_bias end +
case when (q29_bias is null) then 0 else q29_bias end +
case when (q30_bias is null) then 0 else q30_bias end 
as biastt,

case when (q1_bias < 0) then q1_bias else 0 end +
case when (q2_bias < 0) then q2_bias else 0 end +
case when (q3_bias < 0) then q3_bias else 0 end +
case when (q4_bias < 0) then q4_bias else 0 end +
case when (q5_bias < 0) then q5_bias else 0 end +
case when (q6_bias < 0) then q6_bias else 0 end +
case when (q7_bias < 0) then q7_bias else 0 end +
case when (q8_bias < 0) then q8_bias else 0 end +
case when (q9_bias < 0) then q9_bias else 0 end +
case when (q10_bias < 0) then q10_bias else 0 end +
case when (q11_bias < 0) then q11_bias else 0 end +
case when (q12_bias < 0) then q12_bias else 0 end +
case when (q13_bias < 0) then q13_bias else 0 end +
case when (q14_bias < 0) then q14_bias else 0 end +
case when (q15_bias < 0) then q15_bias else 0 end +
case when (q16_bias < 0) then q16_bias else 0 end +
case when (q17_bias < 0) then q17_bias else 0 end +
case when (q18_bias < 0) then q18_bias else 0 end +
case when (q19_bias < 0) then q19_bias else 0 end +
case when (q20_bias < 0) then q20_bias else 0 end +
case when (q21_bias < 0) then q21_bias else 0 end +
case when (q22_bias < 0) then q22_bias else 0 end +
case when (q23_bias < 0) then q23_bias else 0 end +
case when (q24_bias < 0) then q24_bias else 0 end +
case when (q25_bias < 0) then q25_bias else 0 end +
case when (q26_bias < 0) then q26_bias else 0 end +
case when (q27_bias < 0) then q27_bias else 0 end +
case when (q28_bias < 0) then q28_bias else 0 end +
case when (q29_bias < 0) then q29_bias else 0 end +
case when (q30_bias < 0) then q30_bias else 0 end 
as biastneg,

case when (q1_bias > 0) then q1_bias else 0 end +
case when (q2_bias > 0) then q2_bias else 0 end +
case when (q3_bias > 0) then q3_bias else 0 end +
case when (q4_bias > 0) then q4_bias else 0 end +
case when (q5_bias > 0) then q5_bias else 0 end +
case when (q6_bias > 0) then q6_bias else 0 end +
case when (q7_bias > 0) then q7_bias else 0 end +
case when (q8_bias > 0) then q8_bias else 0 end +
case when (q9_bias > 0) then q9_bias else 0 end +
case when (q10_bias > 0) then q10_bias else 0 end +
case when (q11_bias > 0) then q11_bias else 0 end +
case when (q12_bias > 0) then q12_bias else 0 end +
case when (q13_bias > 0) then q13_bias else 0 end +
case when (q14_bias > 0) then q14_bias else 0 end +
case when (q15_bias > 0) then q15_bias else 0 end +
case when (q16_bias > 0) then q16_bias else 0 end +
case when (q17_bias > 0) then q17_bias else 0 end +
case when (q18_bias > 0) then q18_bias else 0 end +
case when (q19_bias > 0) then q19_bias else 0 end +
case when (q20_bias > 0) then q20_bias else 0 end +
case when (q21_bias > 0) then q21_bias else 0 end +
case when (q22_bias > 0) then q22_bias else 0 end +
case when (q23_bias > 0) then q23_bias else 0 end +
case when (q24_bias > 0) then q24_bias else 0 end +
case when (q25_bias > 0) then q25_bias else 0 end +
case when (q26_bias > 0) then q26_bias else 0 end +
case when (q27_bias > 0) then q27_bias else 0 end +
case when (q28_bias > 0) then q28_bias else 0 end +
case when (q29_bias > 0) then q29_bias else 0 end +
case when (q30_bias > 0) then q30_bias else 0 end 
as biastpos
from pa7;

drop table pa7;

-- edit 2022:
update pa8 set qcnt = qcnt - 2;

COPY pa8 TO '/tmp/pa_phase_2.csv' WITH (FORMAT CSV, HEADER);