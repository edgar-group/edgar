1. promijeniti @@ parametre u unload phase 2, obaviti
    -> skripta proizvodi pa_phase_2.csv na serveru, preuzeti (npr. FTP), snimiti u r projekt folder
2. pokrenuti pa2.rmd, ona čita csv iz #1
    -> skripta proizvodi biases.csv i out.pa2.csv    
    -> Ljilja: na temelju out.pa2.csv se generiraju SQL narede tipa:

	update test_instance 
		set score = 5,
		score_perc = 1,
		passed = true
	where id_test = id_prvog_od_5_pa2_testova
	and id_Student in (odabrati one koji imaju <1 SD)
 	itd. za ostale, 4 boda, ...

3. napraviti temp tablicu od biases.csv (vidi create table stud_bias), lakše je u editoru nego upload na server pa copy.
4. promijeniti @@ parametre u unload phase 1, obaviti
    -> skripta proizvodi pa_phase_1_bias.csv
5. pokrenuti pa1.bias.rmd, ona čita csv iz #4
    -> skripta proizvodi out.pa1.csv
    -> Ljilja: na temelju out.pa1.csv generirati naredbe tipa:

    -- Update bodova na testu
    update test_instance set score = 1  , score_perc = 1 /10.0 where id_test = 13501 and id_student = 6126;

    -- Info o ocjenjivanju na drugom dijelu PA, malo je glupo što ovu info kačimo na prvi test, ali na drugi GUI zasad ne podražava, tj nemam gdje prikazati!
    update test_instance_question 
       set hint = 'Postotni bias kod ocjenjivanja kalibracijskog testa je 15,38% što je unutar 1 standardne devijacije' 
       where ordinal = 1 
         and id_test_instance = (select id from test_instance where id_test = 13501 and id_student = 5657);
     

    

Naravno, koraci 2 i 5 proizvode fajlove (pokrenuti KNIT) sa grafovim koji će poslužiti za ocjenjivanje.
Dalje LJilja... :)
