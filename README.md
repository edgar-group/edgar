![](https://edgar.fer.hr/images/edgar-sitting-small.png)

# Edgar &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/facebook/react/blob/master/LICENSE)

Edgar is a comprehensive automated programming assessment system.
It supports various text-based programming languages (C, C++, Java, Pyhton, SQL, etc.), multi-correct multiple-choice questions, rich exam logging and monitoring infrastructure and data visualization.
Edgar can be deployed on all major operating systems and can scale to a custom fit.
It is in active use today (1000+ students per semester, courses in C, Java, SQL, ...) and will be for the foreseeable future at [FER](https://www.fer.unizg.hr/).

## Demo

You can try Edgar at: https://edgar.fer.hr:8080/
Use one of the following 100 usernames: student00, student01, ... student98, student99.
(this is to avoid same students writing the same test at the same time)
The password is: "edgardemo"

## Docker setup

Perform all Docker setups from one "root" directory where you are sure to have RW permissions, e.g. on Windows `c:\User\Username\EdgarProject`.

### Main webapp (Edgar)

- `git clone git@gitlab.com:edgar-group/edgar.git`
- `cp ./edgar/config/development-config-docker.js ./edgar/config/development-config.js`
- `cp ./edgar/config/development-config-docker.js ./edgar/config/production-config.js`
- `docker-compose -f ./edgar/docker-compose.yml up -d`

Or, if you prefer a one-liner:

- `git clone git@gitlab.com:edgar-group/edgar.git && cp ./edgar/config/development-config-docker.js ./edgar/config/development-config.js && cp ./edgar/config/development-config-docker.js ./edgar/config/production-config.js && docker-compose -f ./edgar/docker-compose.yml up -d`

Now might be a good time to grab a ☕, this will take few minutes...

Once it is done, open: http://localhost:1337/ and login using "Dummy login" as:

- user "edgar"
- with password that is at least 10 chars and ends with "@node.js".

(This might not work on the first run/install because the DB might not finish seeding data before the webapp starts to connect.
Just restart the webapp container and you should be fine. It will not happen again on subsequent restarts.)

(If you are still using docker toolbox on Windows, you'll have to use VirtualBox's IP: http://192.168.99.100/. Also, change .yml file to remove `127.0.0.1:` in the ports mapping section)

This will only setup the minimal environment needed to run multiple-choice questions.
If you wish to include SQL and code questions, please add the following:

### SQL questions

- `git clone git@gitlab.com:edgar-group/pg-runner.git`
- `cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/development-config.js`
- `cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/production-config.js`
- `docker-compose -f ./pg-runner/docker-compose.yml up -d`

Or, if you prefer a one-liner:

- `git clone git@gitlab.com:edgar-group/pg-runner.git &&  cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/development-config.js &&  cp ./pg-runner/config/development-config-docker.js ./pg-runner/config/production-config.js &&  docker-compose -f ./pg-runner/docker-compose.yml up -d`

Check if everything is working:

- Questions -> New question
  - "New SQL Question", having code runner: "docker: pg sql"
- In the question edit form, edit the answer to "SELECT \* FROM film" and hit Run.
  - you should get a list of films from the Sakila database that is shipped with this image
    - For more info on Sakila, see: https://wiki.postgresql.org/wiki/Sample_Database

### Code questions (C, C++, Java, etc.)

Edgar uses the wonderful Judge0 (https://judge0.com/) online code execution system to run unstrusted code in the sandbox.
So, first install Judge0 according to their instructions:

- https://github.com/judge0/judge0/blob/master/CHANGELOG.md#deployment-procedure

I'd rather not change Judge0's original docker-compose.yml file (but you are welcomed to do so.)
Note that it will map its API to port 80, so be careful if that port is already taken.
Check if you can reach it on: http://localhost/

After it has started, we have to add it to our "edgarnet" network:

- `docker container ls | grep server` to find the API's container ID, and then:
- `docker network connect --alias j0server edgarnet <server_container_id>`

Repeat for worker:

- `docker container ls | grep worker` to find the API's container ID, and then:
- `docker network connect edgarnet <server_container_id>`

Then:

- `git clone git@gitlab.com:edgar-group/code-runner.git`
- `cp ./code-runner/config/development-config-docker.js ./code-runner/config/development-config.js`
- `cp ./code-runner/config/development-config-docker.js ./code-runner/config/production-config.js`
- `docker-compose -f ./code-runner/docker-compose.yml up -d`

Or, if you prefer a one-liner:

- `git clone git@gitlab.com:edgar-group/code-runner.git && cp ./code-runner/config/development-config-docker.js ./code-runner/config/development-config.js && cp ./code-runner/config/development-config-docker.js ./code-runner/config/production-config.js &&  docker-compose -f ./code-runner/docker-compose.yml up -d`

Check if everything is working:

- Questions -> New question
  - "New C (GCC\*) question", having code runner: "docker: generic code runner"
- In the question edit form, edit the "Code(main)" section to:

```
#include <stdio.h>
int main () {
  printf("1");
  return 0;
}
```

      * and hit "Run all tests"

## Docker setup for developers (anonymized DB)

- `git clone git@gitlab.com:edgar-group/edgar.git`
- `cp ./edgar/config/development-config-docker.js ./edgar/config/development-config.js`
- `cp ./edgar/config/development-config-docker.js ./edgar/config/production-config.js`

Or, if you prefer a one-liner:

- `git clone git@gitlab.com:edgar-group/edgar.git && cp ./edgar/config/development-config-docker.js ./edgar/config/development-config.js && cp ./edgar/config/development-config-docker.js ./edgar/config/production-config.js`

**IMPORTANT**: Before starting the containers, make sure to have the .dmp file named **adump_latest.dmp** in `./edgar/dumps/anonymous`

If you did everything correctly, you are nearly there. You should now be able to run the containers with:
- `docker-compose -f ./edgar/docker-compose-dumped.yml up`

Now, keep watch for a message in the log that goes like this: *Dump file found, restoring...*
 
After that message the database should be ready in under a minute (just be patient).
When the databse is ready, the log window (the one you started docker-compose in) should notify you that the database is ready to accept connections.
When you see that message you can restart the webapp container by getting it's id with `docker ps` and executing `docker restart <container_id_here>`.
Alternatively, you can connect to the webapp container with `docker exec -it <container_id_here> sh`, and run the following command:
- `kill -INT 1 && gulp dev &`

Same rules apply as in the **Docker setup -> Main webapp (Edgar)** section after the containers boot up.

If you forgot to add the dump file before starting the containers, don't worry, you aren't alone. Here's a handy fix:
- `docker-compose -f ./edgar/docker-compose-dumped.yml down`
- `docker rmi edgar-postgres:latest`
- `docker volume rm edgar_postgres-volume`

Or, if you prefer a one-liner:
- `docker-compose -f ./edgar/docker-compose-dumped.yml down && docker rmi edgar-postgres:latest && docker volume rm edgar_postgres-volume`

After doing this, you can start from the begining of this chapter (and don't forget to add the file this time).

### Additional notes
If you have an old database dump, after following the instructions above do the following:
1. Connect to the database in the container with your preferred database explorer
    - if you haven't changed anything in the docker-compose files, the server should be available @ `127.0.0.1:5432`
2. Find the version tag of the database schema with: `SELECT * FROM versions ORDER BY ts_created DESC;`
3. Go to the './edgar/db/db-schema' folder then find and apply all schema versions that your dumped database is missing
by using the tried and tested Copy-Paste method from the schema files into your preferred database explorer query tool
and execute them **in ascending order** (of course) one by one
    - Example: if your database version tag is 1.4.0 and the newest database schema version is 1.4.6 then you should apply
      schemas from 'edgar_db_schema_1-4-1.sql' to 'edgar_db_schema_1-4-6.sql' **in ascending order**

If you get any errors when executing scripts try to solve them by reading error messages. Here are some common errors and solutions:
- Role edgar does not exist
    - solution: run `CREATE ROLE edgar;` before executing the schema update script
- Function or table does not exist
    - solution: find the line of the script that you ran that offends the 'existance constraint' (such as a `DROP FUNCTION ...` or `DROP TABLE ...`)
      and change them to `DROP FUNCTION IF EXISTS ...` and/or `DROP TABLE IF EXISTS ...`

## "Classical" Setup:

- Install latest Node.js, npm, gulp, typescript
- Install latest PostgreSQL RDBMS (9.6.+)
  - See instructions below (in the Setup Edgar project section) on how to create and populate the DB
- Install latest MongoDB

### Setup Edgar main project

- `git clone git@gitlab.com:edgar-group/edgar.git`
- npm install
- Create files (by copying and adjusting `config/development-config-TEMPLATE.js`):
  - `config/development-config.js`
  - `config/production-config.js`
- Create file (by copying and adjusting `common/mailer-TEMPLATE.js`):
  - `common/mailer.js`
  - update email address to yours in gulpfile.js
- Compile the ts code to js:
  - `cd lib`
  - `tsc`
- Prepare database
  - Create database `edgar` with owner `postgres` (you might want to use different locales, that's alright):
    ```
       CREATE DATABASE edgar
       WITH OWNER = postgres
           ENCODING = 'UTF8'
           TABLESPACE = pg_default
           LC_COLLATE = 'en_US.UTF-8'
           LC_CTYPE = 'en_US.UTF-8'
           CONNECTION LIMIT = -1;
     GRANT CONNECT, TEMPORARY ON DATABASE edgar TO public;
     GRANT ALL ON DATABASE edgar TO postgres;
    ```
  - Create "edgar" database tables using latest `db/db-schema/edgar-schema-dump*.sql`
  - Execute SQL dump file, e.g.:
    - Command line (DoS/Windows):
    ```
    "C:\Program Files\PostgreSQL\9.5\bin\"psql -U postgres < edgar-schema-dump-someversionhere.sql -d edgar
    ```
    - Terminal (Unix/Linux/Mac)
    ```bash
    sudo -i -u postgres psql -f /path/to/sql/file/edgar-schema-dump-someversionhere.sql edgar
    ```
  - Seed database using `db/db-schema/seed-edgar.sql`
  - Check which is the last database schema version in `versions` table
    - if there are any new db schema versions in `db/db-schema`, apply them
      (using same procedure or from any SQL client)
  - Any mismatch in the present `db/db-schema` folder and the `versions` table
    will trigger an error message and stop the app
- Run edgar with: `gulp dev` or `gulp dev-lint`
- login at http://localhost:1337/ using (u,p) = (edgar, whatever@node.js)

#### Question difficulty computation using R (optional)

- install `R` and make sure you can run .R files using `Rscript` from your command line interface
- run the `R` environment as administrator to install packages globally
  eg. Linux

```R
sudo R
```

- install `R` packages `ltm` and `RJSONIO`

```R
install.packages('ltm')
install.packages('RJSONIO')
```

- set the `tmpPath` configuration parameter in the `/config/<environment>-config.js` file to a path where PostgreSQL has write permissions and R has read permissions
  (typically the temporary files folder, set to `/tmp` for Linux, differs for other operating systems)

### Setup pg-runner project (optional)

This project is needed if you want to run SQL questions in Edgar.

- See installation instructions in the pg-runner project

### Setup code-runner project (optional)

This project is needed if you want to test C, C++, Java, etc. questions.

- See installation instructions in the code-runner project

### Setup edgar-mobile project (optional)

- See installation instructions in the edgar-mobile project
