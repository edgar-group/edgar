var mailer = require('./common/mailer');
var winston = require('winston');
var config = require.main.require('./config/config');
var os = require('os');
var osInfo = require('./common/osInfo');
const { format } = require('winston');
const { errors } = format;

let winstonFormat;
let level;
if (process.env.NODE_ENV === 'production') {
  level = 'info';
  winstonFormat = winston.format.json();
} else {
  level = 'debug';
  winstonFormat = winston.format.printf(({ level, message, timestamp, stack }) => {
    if (stack) {
      return `${timestamp} ${level}: ${message} - ${stack}`;
    } else {
      return `${timestamp} ${level}: ${message}`;
    }
  });
}

winston.configure({
  //u formatu je dodan timestamp, i stavljen je json radi strukture
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    errors({
      stack: true,
    }),
    winstonFormat
  ),
  defaultMeta: {
    hostname: osInfo.getHostName(),
    script: osInfo.getScriptName(),
    appInstance: osInfo.appInstance(),
    source: 'edgar',
  },
  transports: [new winston.transports.Console()],
});
winston.level = level;

process.on('unhandledRejection', function (error) {
  console.error('UNHANDLED REJECTION', error.stack);
  winston.error('UNHANDLED REJECTION', error);
  winston.error('EXITing app (for pm2 to restart me...)', error);
  process.exit();
});

// winston.remove(winston.transports.Console);
// winston.add(winston.transports.Console, { 'timestamp': true });

winston.info(
  `***********************************************************************************************************`
);
winston.info(
  `*******************   Starting mailer dameon (postman.js)...     ******************************************`
);
winston.info(
  `***********************************************************************************************************`
);
winston.info(`process.env.NODE_ENV = ${process.env.NODE_ENV}`);
winston.info(`log level is ${winston.level}`);

setInterval(mailer.processQueue, 1000 * config.mail.intervalSeconds);

mailer.sendmail(`Postman started on ${os.hostname()}!`);
