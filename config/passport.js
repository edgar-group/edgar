const passport = require('passport');
const winston = require('winston');
const config = require('./config');

module.exports = function (app) {
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser(function (user, done) {
    winston.debug('Passport serializeUser: ' + user);
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  var providers = config.providers;

  if (providers.dummy) {
    var simpleStrategy = require('./strategies/simple.strategy');
    simpleStrategy.attachStrategy(passport);
    passport.webLogoutStrategy = simpleStrategy.logoutStrategy;
  }

  if (providers.local) {
    var localStrategy = require('./strategies/local.strategy');
    localStrategy.attachStrategy(passport);
    passport.webLogoutStrategy = localStrategy.logoutStrategy;
  }

  if (providers.facebook) {
    var facebookStrategy = require('./strategies/facebook.strategy');
    facebookStrategy.attachStrategy(passport);
  }

  if (providers.twitter) {
    var twitterStrategy = require('./strategies/twitter.strategy');
    twitterStrategy.attachStrategy(passport);
  }

  if (providers.google) {
    var googleStrategy = require('./strategies/google.strategy');
    googleStrategy.attachStrategy(passport);
  }

  if (providers.aai) {
    var samlAAIStrategy = require('./strategies/saml.aai.strategy');
    samlAAIStrategy.attachStrategy(passport);
    // For now, change when user logs in
    // TODO, mislim da ovo nije dobro, zar nije ovo globalna postavke, neovisna o useru?
    passport.webLogoutStrategy = samlAAIStrategy.logoutStrategy;
  }

  // Attach a token strategy
  var tokenStrategy = require('./strategies/token.strategy');
  tokenStrategy.attachStrategy(passport);
  passport.mobileLogoutStrategy = tokenStrategy.logoutStrategy;
};
