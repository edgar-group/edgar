var config = {
  development: require('./development-config.js'),
  production: require('./production-config.js'),
};
module.exports = config[(process.env.NODE_ENV || 'development').trim()];
