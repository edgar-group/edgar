'use strict';
const winston = require('winston');
const errors = require.main.require('./common/errors');
const globals = require.main.require('./common/globals');
const db = require.main.require('./db').db;
const utils = require.main.require('./common/utils');
const AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
const RunningInstances = require.main.require('./common/runningInstances');

var initSessionDataAppUser = function (req, username, rolename, cb) {
  winston.debug(`initSessionDataAppUser.username = $(username)`);
  var promises = [];
  promises.push(
    db.any(`SELECT id, title
              FROM academic_year
              ORDER BY date_start DESC
              LIMIT 10`)
  );
  if (rolename === 'Admin') {
    promises.push(
      db.any(
        `SELECT
              course.id,
              course_name,
              coalesce(course_acronym, 'null') as course_acronym
          FROM course
          ORDER BY course_name ASC`
      )
    );
  } else {
    promises.push(
      db.any(
        `SELECT
              course.id,
              course_name,
              coalesce(course_acronym, 'null') as course_acronym
        FROM perm_user_course puc
        JOIN course ON puc.id_course = course.id
        JOIN app_user ON puc.id_user = app_user.id
        WHERE username = $(username)
        ORDER BY course_name ASC`,
        {
          username: username,
        }
      )
    );
  }
  promises.push(
    db.any(
      `SELECT app_user.id AS id_app_user, student.id AS id_student
        FROM student
        JOIN app_user
          ON student.id_app_user = app_user.id
        WHERE app_user.username = $(username)`,
      {
        username: username,
      }
    )
  );

  promises.push(AppUserSettings.getProps(username));

  promises.push(
    db.one(
      `SELECT TRIM(first_name) || ' ' || TRIM(last_name) as full_name, alt_id2
        FROM app_user
        WHERE username = $(username)`,
      {
        username: username,
      }
    )
  );
  // promises.push(OngoingTests.getOngoingTests(req.session, username));

  Promise.all(promises).then(
    async function (args) {
      // returned data is in arguments[0], arguments[1], ... arguments[n]
      // you can process it here
      var years, courses, userSettings, ids;

      if (args[0].length === 0) {
        cb(null, 'There are no academic_year records in the DB.');
      } else {
        years = args[0];
      }
      if (args[1].length === 0) {
        cb(null, 'User has zero(0) course permissions.');
      } else {
        courses = args[1];
      }
      if (args[2].length === 0) {
        cb(null, 'User does not have a parallel alter-ego student.');
      } else {
        ids = args[2];
      }
      if (!args[3]) {
        userSettings = {
          currAcademicYearId: years[0].id,
          currCourseId: courses[0].id,
        };
        AppUserSettings.saveProps(username, userSettings); // I'm not awaiting this, moving on...
      } else {
        userSettings = args[3];
        let idexists = false;
        let save = false;
        for (let i = 0; i < courses.length; ++i) {
          if (courses[i].id === userSettings.currCourseId) {
            idexists = true;
            break;
          }
        }
        if (!idexists) {
          userSettings.currCourseId = courses[0].id;
          save = true;
        }
        idexists = false;
        for (let i = 0; i < years.length; ++i) {
          if (years[i].id === userSettings.currAcademicYearId) {
            idexists = true;
            break;
          }
        }
        if (!idexists) {
          userSettings.currAcademicYearId = years[0].id;
          save = true;
        }
        if (save) {
          try {
            winston.debug('User settings adjusted/reset, probably permissions change occured.');
            await userSettings.saveProps(username, userSettings);
          } catch (error) {
            winston.error(error);
          }
        }
      }

      winston.debug('0 rolename ' + rolename);
      winston.debug('1 fullName ' + args[4].full_name);
      //winston.info('2 years', years);
      //winston.info('3 courses', courses);
      //winston.info('4 userSettings', userSettings);

      req.session.fullname = args[4].full_name;
      req.session.imgSrc =
        '/images/faces/' + utils.getHashedImageName(args[4].alt_id2 + '.jpg', '.jpg');
      req.session.rolename = rolename;
      req.session.courses = courses;
      req.session.academicYears = years;
      req.session.appUserId = ids[0].id_app_user;
      req.session.studentId = ids[0].id_student;
      // Apply all saved user settings to the session
      for (let key of Object.keys(userSettings)) {
        req.session[key] = userSettings[key];
      }
      (async () => {
        try {
          await utils.setCourseParams(req.session);
          winston.debug(
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
          );
          winston.debug('Session setup for user: ');
          winston.debug(username);
          winston.debug(
            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
          );
          cb(null);
        } catch (e) {
          winston.error('ERROR');
        }
      })();
    },
    function (err) {
      winston.error(err);
      cb(new errors.DbError('An error occured in initSessionData: ' + err.message));
    }
  );
};

var initSessionDataStudent = function (req, username, cb) {
  var promises = [];
  promises.push(
    db.any(
      `SELECT DISTINCT academic_year.id, title
                            FROM academic_year
                            JOIN student_course ON academic_year.id = student_course.id_academic_year
                            JOIN student on student_course.id_student = student.id
                            WHERE alt_id = $(username)
                            ORDER BY id DESC`,
      {
        username: username,
      }
    )
  );

  promises.push(
    db.any(
      `SELECT DISTINCT course.id,
                           course_name,
                           coalesce(course_acronym, 'null') as course_acronym
                     From student
                     JOIN student_course
                       ON student.id = student_course.id_student
                     JOIN course
                       ON student_course.id_course = course.id
                    where alt_id = $(username)
                    ORDER BY course_name ASC`,
      {
        username: username,
      }
    )
  );
  promises.push(
    db.any(
      `SELECT id
                        FROM student
                        WHERE alt_id = $(username)`,
      {
        username: username,
      }
    )
  );
  // 3:
  promises.push(AppUserSettings.getProps(username));
  // 4:
  promises.push(
    db.one(
      `SELECT TRIM(first_name) || ' ' || TRIM(last_name) as full_name, alt_id2
                            FROM student
                           WHERE alt_id = $(username)`,
      {
        username: username,
      }
    )
  );
  // promises.push(OngoingTests.getOngoingTests(req.session, username));
  Promise.all(promises).then(
    async function (args) {
      // returned data is in arguments[0], arguments[1], ... arguments[n]
      // you can process it here
      var years, courses, userSettings, studentId;

      if (args[0].length === 0) {
        cb(null, 'There are no academic_year records in the DB.');
      } else {
        years = args[0];
      }
      if (args[1].length === 0) {
        cb(null, 'Student has zero(0) course permissions.');
      } else {
        courses = args[1];
      }
      if (args[2].length === 0) {
        cb(null, 'No student id for username/alt_id !?');
      } else {
        studentId = args[2][0].id;
      }
      if (!args[3]) {
        userSettings = {
          currAcademicYearId: years[0].id,
          currCourseId: courses[0].id,
        };
        AppUserSettings.saveProps(username, userSettings); // I'm not awaiting this, moving on...
      } else {
        userSettings = args[3];
        let idexists = false;
        let save = false;
        for (let i = 0; i < courses.length; ++i) {
          if (courses[i].id === userSettings.currCourseId) {
            idexists = true;
            break;
          }
        }
        if (!idexists) {
          userSettings.currCourseId = courses[0].id;
          save = true;
        }
        idexists = false;
        for (let i = 0; i < years.length; ++i) {
          if (years[i].id === userSettings.currAcademicYearId) {
            idexists = true;
            break;
          }
        }
        if (!idexists) {
          userSettings.currAcademicYearId = years[0].id;
          save = true;
        }
        if (save) {
          try {
            winston.info('User settings adjusted/reset, probably permissions change occured.');
            await userSettings.saveProps(username, userSettings);
          } catch (error) {
            winston.error(error);
          }
        }
      }
      req.session.rolename = globals.ROLES.STUDENT;
      req.session.fullname = args[4].full_name;
      req.session.imgSrc =
        '/images/faces/' + utils.getHashedImageName(args[4].alt_id2 + '.jpg', '.jpg');
      req.session.courses = courses;
      req.session.studentId = studentId;
      req.session.academicYears = years;

      // Apply all saved user settings to the session
      for (let key of Object.keys(userSettings)) {
        req.session[key] = userSettings[key];
      }
      (async () => {
        try {
          await utils.setCourseParams(req.session);
          winston.debug('student session ' + req.session);
          winston.info(`${req.session.fullname} (${username}) logged in at ${new Date()}`);
          cb(null);
        } catch (e) {
          winston.error(e);
        }
      })();
    },
    function (err) {
      winston.error(err);
      cb(new errors.DbError('An error occured while initSessionDataStudent: ' + err.message));
    }
  );
};

let isStudent = (username, provider) => {
  var checkForProvider = '';
  if (provider !== 'simple') {
    checkForProvider = 'AND provider = $(provider)';
  }
  return db
    .any(
      `
            SELECT count(*)::int as cnt
            FROM student
            WHERE alt_id = $(alt_id)` + checkForProvider,
      {
        alt_id: username,
        provider: provider,
      }
    )
    .then(function (res) {
      if (res.length === 1 && res[0].cnt === 1) {
        return true;
      } else {
        return false;
      }
    });
};

module.exports = {
  init: function (req, username, provider, done) {
    username = username.replace('aai-test.hr', 'fer.hr');
    // console.log('sessionInit.js for ' + username);
    // DO NOT use next 5 lines when working in dev mode SPA angular in order to impersonate user:
    if (process.env.NODE_ENV !== 'development') {
      winston.info('Session init for: ' + username + ' (deleting all ongoing exams and reviews).');
      RunningInstances.clearAll(req.session);
    }
    var checkForProvider = '';
    if (provider !== 'simple') {
      checkForProvider = 'AND provider = $(provider)';
    }
    // JOIN app_user_role ON app_user.id = app_user_role.id_app_user
    db.any(
      `
        SELECT role.role_name, app_user.first_name, app_user.last_name, app_user.email
          FROM app_user
          JOIN role ON app_user.id_role = role.id
         WHERE username = $(username)` + checkForProvider,
      {
        username: username,
        provider: provider,
      }
    )
      .then(function (users) {
        if (users.length > 1) {
          return done(null, false, req.flash('auth', `User ${username} has multiple roles!`));
        } else if (users.length === 1) {
          var user = users[0];
          initSessionDataAppUser(req, username, user.role_name, function (err, flashMsg) {
            if (err === null && flashMsg === undefined) {
              isStudent(username, provider)
                .then(function (isStud) {
                  req.session.mustChooseRole = isStud;
                  done(null, username);
                })
                .catch(function (error) {
                  winston.error(`An error occured while checking isStudent for ${username}`);
                  winston.error(error);
                  done(err);
                });
            } else if (err !== null) {
              done(err);
            } else {
              return done(null, false, req.flash('auth', flashMsg));
            }
          });
        } else {
          // Check if it is a student?
          return db
            .any(
              `
                        SELECT first_name, last_name, email
                        FROM student
                        WHERE alt_id = $(alt_id)` + checkForProvider,
              {
                alt_id: username,
                provider: provider,
              }
            )
            .then(function (students) {
              if (students.length === 1) {
                initSessionDataStudent(req, username, function (err, flashMsg) {
                  if (err) {
                    done(err);
                    return;
                  }
                  if (flashMsg) {
                    winston.error(flashMsg);
                    return done(null, false, req.flash('auth', flashMsg));
                  }
                  done(null, username);
                });
              } else {
                winston.error(`Unknown user ${username}. Query is: SELECT first_name, last_name, email
                        FROM student
                        WHERE alt_id = '${username}' ${checkForProvider}`);
                winston.info(`Unknown user ${username}.`);
                return done(
                  null,
                  false,
                  req.flash('auth', 'Unknown user or bad password, go figure.')
                );
              }
            });
        }
      })
      .catch(function (error) {
        winston.info(error);
        winston.error(
          new errors.DbError(
            `An error occured while authenticating ${username}. Error message: ${error.message}`
          )
        );
        done(new errors.DbError(`An error occured while authenticating ${username}.`));
      });
  },
  impersonate: initSessionDataStudent,
};
