'use strict';
var FacebookStrategy = require('passport-facebook').Strategy;
var winston = require('winston');
var db = require.main.require('./db').db;
var sessionInit = require.main.require('./config/strategies/sessionInit');
var config = require.main.require('./config/config');

var strategy = new FacebookStrategy(
  {
    clientID: config.facebookAuth.clientID,
    clientSecret: config.facebookAuth.clientSecret,
    callbackURL: config.facebookAuth.callbackURL,
    passReqToCallback: true,
    profileFields: ['id', 'displayName', 'photos'],
  },
  function (req, accessToken, refreshToken, profile, done) {
    // console.log(profile);
    winston.info('User signed in with facebook, id: ' + profile.id);
    if (req.session.registerStudentID) {
      db.any(
        `UPDATE student SET (alt_id, provider) = ($(alt_id), $(provider))
                WHERE id = $(id) RETURNING id_app_user`,
        {
          alt_id: profile.id,
          provider: 'facebook',
          id: req.session.registerStudentID,
        }
      )
        .then((data) => {
          if (data[0].id_app_user) {
            db.any(
              `UPDATE app_user SET (username, provider) = ($(username), $(provider))
                            WHERE id = $(id)`,
              {
                username: profile.id,
                provider: 'facebook',
                id: data[0].id_app_user,
              }
            )
              .then(() => {
                return sessionInit.init(req, profile.id, 'facebook', done);
              })
              .catch((err) => {
                winston.error('Error updateing user in app_user table');
                return done(err);
              });
          } else {
            return sessionInit.init(req, profile.id, 'facebook', done);
          }
        })
        .catch((err) => {
          winston.error('Error updateing user in student table');
          return done(err);
        });
    } else {
      return sessionInit.init(req, profile.id, 'facebook', done);
    }
  }
);

module.exports = {
  attachStrategy(passport) {
    winston.info('Attaching facebook strategy...');
    passport.use(strategy);
  },
  logoutStrategy(req, res) {
    winston.info('facebookLogout');
    req.session.destroy(function (err) {
      if (err) {
        winston.error(err);
      }
      res.status(200).send();
    });
  },
};
