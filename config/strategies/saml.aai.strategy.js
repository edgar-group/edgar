'use strict';
var SamlStrategy = require('passport-saml').Strategy;
var config = require.main.require('./config/config');
var winston = require('winston');
var db = require.main.require('./db').db;
var sessionInit = require.main.require('./config/strategies/sessionInit');
const moment = require('moment');

var strategy = new SamlStrategy(
  {
    path: config.samlAAIAuth.path,
    protocol: config.samlAAIAuth.protocol,
    entryPoint: config.samlAAIAuth.entryPoint,
    logoutUrl: config.samlAAIAuth.logoutUrl,
    logoutCallbackUrl: config.samlAAIAuth.logoutCallbackUrl,
    issuer: config.samlAAIAuth.issuer,
    cert: config.samlAAIAuth.cert,
    passReqToCallback: true,
  },
  function (req, profile, done) {
    winston.debug('👨👩  User logged in:', moment().format('YYYY-MM-DD, h:mm:ss'), profile);
    req.session.aaiprofile = profile;
    if (req.session.registerStudentID) {
      db.any(
        `UPDATE student SET (alt_id, provider) = ($(alt_id), $(provider))
                    WHERE id = $(id) RETURNING id_app_user`,
        {
          alt_id: profile.hrEduPersonUniqueID,
          provider: 'saml.aai',
          id: req.session.registerStudentID,
        }
      )
        .then((data) => {
          if (data[0].id_app_user) {
            db.any(
              `UPDATE app_user SET (username, provider) = ($(username), $(provider))
                            WHERE id = $(id)`,
              {
                username: profile.hrEduPersonUniqueID,
                provider: 'saml.aai',
                id: data[0].id_app_user,
              }
            )
              .then(() => {
                return sessionInit.init(req, profile.hrEduPersonUniqueID, 'saml.aai', done);
              })
              .catch((err) => {
                winston.error('Error updateing user in app_user table');
                return done(err);
              });
          } else {
            return sessionInit.init(req, profile.hrEduPersonUniqueID, 'saml.aai', done);
          }
        })
        .catch((err) => {
          winston.error('Error updateing user in student table');
          return done(err);
        });
    } else {
      return sessionInit.init(req, profile.hrEduPersonUniqueID, 'saml.aai', done);
    }
    /*return done(null,
          {
            id: profile.uid,
            email: profile.email,
            displayName: profile.cn,
            firstName: profile.givenName,
            lastName: profile.sn,
            aaiprofile: profile
          });*/
  }
);

module.exports = {
  attachStrategy: function (passport) {
    winston.info('Attaching SAML AAI strategy...');
    passport.use(strategy);
  },
  logoutStrategy(req, res) {
    //Here add the nameID and nameIDFormat to the user if you stored it someplace.
    //console.log(`SAML AAI logoutStrategy`);
    if (req.session.aaiprofile) {
      req.user = {
        id: req.session.aaiprofile.uid,
      };
      req.user.nameID = req.session.aaiprofile.nameID;
      req.user.nameIDFormat = req.session.aaiprofile.nameIDFormat;
      winston.debug(`${req.user.nameID} logging out...`);
      strategy.logout(req, function (err, request) {
        if (!err) {
          winston.debug('Successfully logged out of SAML IP.');
          // move this to logut callback in authroute
          req.session.destroy(function (err) {
            //res.redirect('/');
          });
          //redirect to the IdP Logout URL
          winston.debug('Logged out.');
          res.redirect(request);
        } else {
          res.errRender({
            err: 'Error logging out of SAML IP. Workaround: Close the browser to clear the cache!',
          });
        }
      });
    } else {
      req.session.destroy(function (err) {
        res.redirect('/');
      });
    }
  },
};
