'use strict';
var LocalStrategy = require('passport-local').Strategy;
var winston = require('winston');
var sessionInit = require.main.require('./config/strategies/sessionInit');

var strategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
  },
  function (req, username, password, done) {
    winston.info(username + ' ' + password);
    if (password.length > 10 && password.endsWith('@node.js')) {
      return sessionInit.init(req, username, 'simple', done);
    } else {
      winston.info(`Bad local password for user ${username}.`);
      done(null, false, req.flash('auth', 'Unknown user or bad password, go figure.'));
    }
  }
);

module.exports = {
  attachStrategy: function (passport) {
    winston.debug('Attaching simple strategy...');
    passport.use('simple', strategy);
  },
  logoutStrategy(req, res) {
    winston.debug('simpleLogout for ' + req.session.passport.user);
    req.session.destroy(function (err) {
      if (err) {
        req.flash('error', 'Error logging out: ' + JSON.stringify(err));
        winston.error(err);
      }
      res.redirect('/auth/logout');
      //res.status(200).send();
    });
  },
};
