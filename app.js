console.log('Booting... process.env.NODE_ENV = ' + process.env.NODE_ENV);
var express = require('express');
var cookieParser = require('cookie-parser');
const session = require('express-session');
var errors = require('./common/errors');
var mailer = require('./common/mailer');
var globals = require('./common/globals');
var utils = require('./common/utils');
const AccessLogModel = require('./models/AccessLogModel');
var winston = require('winston');
var moment = require('moment');
var favicon = require('serve-favicon');
var path = require('path');
const zip = require('express-easy-zip');
var fs = require('fs');
var OnlineUsers = require.main.require('./models/OnlineUsersModel.js');
const useragent = require('useragent');

useragent(true); // This will async load the database from the server and compile it to a proper JavaScript supported format

process.env.UV_THREADPOOL_SIZE = 128;

// var timeFormatFn = function() {
//     'use strict';
//     return moment().format('YYYY-MM-DD, kk:mm:ss');
// };

process.on('unhandledRejection', function (error, promise) {
  console.error(`(${moment().format('MMMM Do YYYY, h:mm:ss a')}) UNHANDLED REJECTION`, error.stack);
  winston.error(error);
  winston.error(`(${moment().format('MMMM Do YYYY, h:mm:ss a')} UNHANDLED REJECTION, exiting...`);
  process.exit(1);
});

var flash = require('connect-flash');
var config = require.main.require('./config/config');
const mongoose = require('mongoose');
winston.info(`Connecting to mongo: ${config.mongooseConnectionString}`);

mongoose.connect(config.mongooseConnectionString);
const MongoStore = require('connect-mongo')(session);

var app = express();

if (process.env.NODE_ENV === 'development') {
  winston.info('Development mode -> CORS is enabled for all routes!');
  var cors = require('cors');
  app.use(
    cors({
      credentials: true,
      origin: 'http://localhost:4200',
    })
  );
  app.options('*', cors());
}

app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
if (utils.useMinio()) {
  var publicMinioRoutes = require('./routes/publicMinioRoutes');
  app.use(`/`, publicMinioRoutes);
}

app.use(express.static('public'));
app.use(express.static('angular'));
app.use(express.json({ limit: '50mb', parameterLimit: 10000 }));
app.use(
  express.urlencoded({
    limit: '50mb',
    parameterLimit: 10000,
    extended: true,
  })
); // https://codereview.stackexchange.com/a/94554
app.use(cookieParser());
app.use(zip());

app.use(
  session({
    resave: false, // leave this as false!!
    saveUninitialized: true,
    secret: 'gradioso',
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    ttl: 5 * 24 * 60 * 60, // = 5 days. Default
  })
);
app.use(flash());
require('./config/passport')(app);

app.set('views', './views');
app.set('view engine', 'ejs');

// Handle non-existant images (faces)
app.use('/images/faces', function (req, res) {
  var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript',
  };
  var file = path.join(__dirname, 'public', 'images', 'unknown.png');
  var type = mime[path.extname(file).slice(1)] || 'text/plain';
  var s = fs.createReadStream(file);
  s.on('open', function () {
    res.set('Content-Type', type);
    s.pipe(res);
  });
});

app.use(function (req, res, next) {
  if (config.closedForMaintenance) {
    return res.redirect('/gonefishing.html');
  } else {
    next();
  }
});

app.use(function (req, res, next) {
  res.publicRender = function (viewname, viewdata) {
    viewdata = viewdata || {};
    viewdata.appVersion = globals.APP_VERSION;
    viewdata.partialsRoot = global.appRoot + '/views/partials';
    var msgs = req.flash('info');
    viewdata.infoFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    msgs = req.flash('error');
    viewdata.errorFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    res.render(viewname, viewdata);
  };
  res.errRender = function (viewdata) {
    if (typeof viewdata === 'string') {
      viewdata = { err: viewdata };
    } else if (!viewdata.err) {
      viewdata = {
        err: JSON.stringify(viewdata) || 'Error message not set?',
      };
    }
    viewdata.partialsRoot = global.appRoot + '/views/partials';
    var msgs = req.flash('info');
    viewdata.infoFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    msgs = req.flash('error');
    viewdata.errorFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    res.render('shared/error', viewdata);
  };
  next();
});

// I don't want to log public routes (images, etc.)
app.use(async (req, res, next) => {
  AccessLogModel.log(req, res); // do NOT await this, as we don't want to block the request
  next(); // Continue processing the request
});

var tinyUrlRoutes = require('./routes/tinyUrlRoutes');
app.use('/', tinyUrlRoutes);

var publicUrlRoutes = require('./routes/publicUrlRoutes');
app.use('/', publicUrlRoutes);

var lectureRoutes = require('./routes/lectureRoutes');
app.use('/lecture', lectureRoutes);

var authRoutes = require('./routes/authRoutes')(null);
app.use('/auth', authRoutes);

var registerRoutes = require('./routes/registerRoutes');
app.use('/register', registerRoutes);

// curl -X POST http://localhost:1337/atest/14446/re-evaluate/12345
// const curl = require('./routes/testRoutes');
// app.use('/atest', curl);

winston.info(`process.env.NODE_ENV = ${process.env.NODE_ENV}`);

// Potentially expose Prometheus metrics:
const EXPOSE_METRICS = !!process.env.METRICS;
//let metrics;
if (EXPOSE_METRICS) {
  winston.info('Exposing metrics on /metrics');
  const promMid = require('express-prometheus-middleware');

  app.use(
    promMid({
      metricsPath: '/metrics',
      collectDefaultMetrics: true,
      requestDurationBuckets: [0.1, 0.5, 1, 1.5],
    })
  );
}

// Catch all auth middleware:
app.use(function (req, res, next) {
  // var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  req.session.registerStudentID = '';
  if (!req.user) {
    winston.debug('Wanted to go: ' + req.url);
    winston.debug('** User is falsy: ' + req.user);

    if (
      config.redirectToOtherProxy &&
      config.redirectToOtherProxy.fromProxy === req.connection.remoteAddress
    ) {
      winston.info('Maybe redirect to other proxy...');
      for (let i = 0; i < config.redirectToOtherProxy.toProxies.length; ++i) {
        if (
          utils.randomIntInclusive(1, 100) <= config.redirectToOtherProxy.toProxies[i].percentage
        ) {
          let to = config.redirectToOtherProxy.toProxies[i].toProxyFullUrl + req.originalUrl;
          winston.info('REDIRECTING TO:', to);
          res.redirect(to);
          return;
        }
      }
    }

    winston.debug('** Unauthenticated request, redirecting to login.');
    if (req.url.indexOf('favicon.ico') === -1) {
      req.session.landingUrl = req.url;
    }
    // winston.info('app req = ', req.session);
    res.redirect('/auth/login');
  } else if (req.session.mustChooseRole) {
    res.redirect('/auth/chooserole');
  } else {
    next();
  }
});
//}

var appRender = function (parentViewName, req, res) {
  return function (viewname, viewdata, scriptname, cssname) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    viewdata = viewdata || {};
    viewdata.partialsRoot = global.appRoot + '/views/partials';
    viewdata.viewsRoot = global.appRoot + '/views';
    viewdata.ejspage = utils.getViewPagePath(viewname, req.baseUrl);
    viewdata.scriptpage = utils.getViewPagePath(scriptname, req.baseUrl);
    viewdata.csspage = utils.getViewPagePath(cssname, req.baseUrl);
    viewdata.session = req.session;
    var msgs = req.flash('info');
    viewdata.infoFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    msgs = req.flash('error');
    viewdata.errorFlash =
      msgs && msgs.length
        ? moment().format('MMMM Do YYYY, h:mm:ss a') + ': ' + msgs.join()
        : undefined;
    if (globals.WARNING_MESSAGE) {
      viewdata.globalWarningMessage = globals.WARNING_MESSAGE;
    }
    viewdata.ip = ip;
    viewdata.appVersion = globals.APP_VERSION;
    // console.log('From appRender, viewdata:', viewdata);
    res.render(parentViewName, viewdata);
  };
};

app.use(async function (req, res, next) {
  if (globals.ROLES.hasRole(req.session.rolename)) {
    if (req.session.rolename === globals.ROLES.STUDENT) {
      res.appRender = appRender('studentView', req, res);
    } else {
      res.appRender = appRender('appUserView', req, res);
      res.fullScreenRender = appRender('fullScreenView', req, res);
    }
    // res.customRender = function (viewName) {
    //     return appRender(viewName, req, res);
    // };
    next(); // req moved on down the pipeline...
    // async update of WHO'S ONLINE, don't care what happens with the request:
    let year = req.session.academicYears.find((x) => x.id === req.session.currAcademicYearId);
    let course = req.session.courses.find((x) => x.id === req.session.currCourseId);
    let updateUser = new OnlineUsers({
      _id: req.session.passport.user,
      username: req.session.passport.user,
      ts_modified: new Date(),
      imgSrc: req.session.imgSrc,
      rolename: req.session.rolename,
      fullname: req.session.fullname,
      currCourse: course ? course.course_name : '?',
      currAcademicYear: year ? year.title : '?',
      lastRoute: req.baseUrl,
    });
    try {
      await OnlineUsers.findByIdAndUpdate(updateUser._id, updateUser, { upsert: true });
    } catch (error) {
      winston.error('Error updating online users:', error);
    }
  } else {
    next(
      new errors.UnknownRoleSecurityError(
        'Unknown role for : ' + req.session &&
          req.session.passport &&
          JSON.stringify(req.session.passport.user)
      )
    );
  }
});

// Routes:
const questionRoutes = require('./routes/questionRoutes');
const acdmYearRoutes = require('./routes/acdmYearRoutes');
const courseRoutes = require('./routes/courseRoutes');
const testRoutes = require('./routes/testRoutes');
const examRunRoutes = require('./routes/examRunRoutes');
const examReviewRoutes = require('./routes/examReviewRoutes');
const ticketRoutes = require('./routes/ticketRoutes');
const tutorialTicketRoutes = require('./routes/tutorialTicketRoutes');
const testDefRoutes = require('./routes/testDefRoutes');
const uploadRoutes = require('./routes/uploadRoutes');
const prefsRoutes = require('./routes/prefsRoutes');
const adminRoutes = require('./routes/adminRoutes');
const analyticsRoutes = require('./routes/analyticsRoutes');
const tokenRoutes = require('./routes/tokenRoutes');
const nodeRoutes = require('./routes/nodeRoutes');
const helpRoutes = require('./routes/helpRoutes');
const playgroundRoutes = require('./routes/playgroundRoutes');
const exerciseRoutes = require('./routes/exerciseRoutes');
const exerciseAnalyticsRoutes = require('./routes/exerciseAnalyticsRoutes');
const exerciseDefRoutes = require('./routes/exerciseDefRoutes');
// const tutorialRoutes = require('./routes/tutorialRoutes');
const tutorialRoutes2 = require('./routes/tutorialRoutes2');
const lectureInstancesRoutes = require('./routes/lectureInstancesRoutes');
const lectureQuizDefRoutes = require('./routes/lectureQuizDefRoutes');
const mailRoutes = require('./routes/mailRoutes');
const tutorialAnalyticsRoutes = require('./routes/tutorialAnalyticsRoutes');
const tutorialDefRoutes = require('./routes/tutorialDefRoutes');
const feedbackRoutes = require('./routes/feedbackRoutes');
const homeRoutes = require('./routes/homeRoutes');

app.use('/acdmyear', acdmYearRoutes);
app.use('/course', courseRoutes);
app.use('/test', testRoutes);
app.use('/exam/run', examRunRoutes);
app.use('/exam/review', examReviewRoutes);
app.use('/test/def', testDefRoutes);
app.use('/ticket', ticketRoutes);
app.use('/question', questionRoutes);
app.use('/upload', uploadRoutes);
app.use('/prefs', prefsRoutes);
app.use('/admin', adminRoutes);
app.use('/analytics', analyticsRoutes);
app.use('/token', tokenRoutes);
app.use('/node', nodeRoutes);
app.use('/help', helpRoutes);
app.use('/playground', playgroundRoutes);
app.use('/exercise', exerciseRoutes);
app.use('/exercise/analytics', exerciseAnalyticsRoutes);
app.use('/exercise/def', exerciseDefRoutes);
// app.use('/tutorial', tutorialRoutes);
app.use('/tutorial', tutorialRoutes2);
app.use('/lecture/instances', lectureInstancesRoutes);
app.use('/lecture/def', lectureQuizDefRoutes);
app.use('/tutorial/analytics', tutorialAnalyticsRoutes);
app.use('/tutorial/def', tutorialDefRoutes);
app.use('/tutorial/ticket', tutorialTicketRoutes);
app.use('/feedback', feedbackRoutes);

app.use('/mail', mailRoutes);

app.use('/', homeRoutes);

// Catch all error handler:
app.use(function (err, req, res, next) {
  winston.error('*** Caught an error in global handler !!');
  winston.error(err);
  winston.error(err.stack);
  if (res) {
    if (process.env.NODE_ENV !== 'test') {
      let userinfo = 'Unknown user info\n';
      if (req.session && req.session.passport && req.session.passport.user)
        userinfo = `User: ${req.session.passport.user}\n`;
      if (res.app) {
        res.app.emit('sendmail', utils.getLogString(userinfo + err.message));
      } else {
        winston.error(
          "Missing node mailer - res.app is undefined. Can't send mail. Error is: " +
            utils.getLogString(userinfo + err.message)
        );
      }
    }
    if (res.headersSent) {
      winston.info("Headers already sent, delegating to express's default error handler.");
      return next(err);
    } else if (res.status) {
      winston.info('Sent a status, rendering error page.');
      res.status(err.statusCode || 500);
      res.render('shared/error', { err });
    }
  } else {
    winston.error('res NOT defined in global error handler?');
  }
});
// Mailer:
app.on('sendmail', mailer.sendmail);
module.exports = app;
