'use strict';
var edgar = edgar || {};

edgar.Grader = class {
  constructor(graderDivId, gradingObject, textAreaId, gradingModel, scoreInputId) {
    this.graderDivId = graderDivId;
    this.gradingObject = gradingObject;
    this.textAreaId = textAreaId;
    this.gradingModel = gradingModel;
    this.scoreInputId = scoreInputId;
    this.init();
  }
  init() {
    let div = document.getElementById(this.graderDivId);

    let allCorrectDiv = document.createElement('div');
    let acCheckLabel = document.createElement('label');
    acCheckLabel.innerHTML = `All correct`;

    var acCheckBox = document.createElement('input');
    acCheckBox.type = 'checkbox';
    acCheckBox.className = 'grader-allcorrect-option checkbox-2x mr-3';
    acCheckBox.onclick = () => this.calculateGrader();

    allCorrectDiv.appendChild(acCheckBox);
    allCorrectDiv.appendChild(acCheckLabel);

    div.appendChild(allCorrectDiv);

    let startFrom = 100;
    if ('startFrom' in this.gradingObject) {
      startFrom = this.gradingObject.startFrom;
    } else if (this.gradingObject.add && this.gradingObject.add.length) {
      startFrom = 0;
    }
    div.setAttribute('data-start-from', `${startFrom}`);

    let sfDiv = document.createElement('div');
    sfDiv.innerHTML = `<hr/>
      <div class="d-flex justify-content-between mb-3">
        <div>
          <h5>Starting from</h5>
          <div class="d-flex">
            <h3><span class="badge badge-primary mr-1">${startFrom}%</h3>
            <h3><span class="badge badge-primary">${
              this.gradingModel ? this.getPointsForPercentage(startFrom) : 0
            } pts</h3>
          </div>
        </div>
        <div>
          <h5>Current</h5>
          <div class="d-flex">
            <h3><span class="badge badge-primary mr-1" id="grader-current-perc-${
              this.graderDivId
            }">%</span></h3>
            <h3><span class="badge badge-primary" id="grader-current-pts-${
              this.graderDivId
            }"> pts</span></h3>
          </div>
        </div>
      </div>`;
    div.appendChild(sfDiv);
    let i = 0;
    if (this.gradingObject.add) {
      div.appendChild(document.createElement('hr'));
      for (let addOption of this.gradingObject.add) {
        ++i;

        let newCheckDiv = document.createElement('div');

        let checkBoxId = 'grader-add-option' + i;

        let newCheckLabel = document.createElement('label');
        newCheckLabel.htmlFor = checkBoxId;
        newCheckLabel.innerHTML = `+(${addOption.perc}%) ${addOption.desc}`;

        var newCheckBox = document.createElement('input');
        newCheckBox.type = 'checkbox';
        newCheckBox.id = checkBoxId;
        newCheckBox.className = 'grader-add-option checkbox-2x mr-3';
        newCheckBox.onclick = () => this.calculateGrader();
        newCheckBox.setAttribute('grader-idx', `${i - 1}`);

        newCheckDiv.appendChild(newCheckBox);
        newCheckDiv.appendChild(newCheckLabel);

        div.appendChild(newCheckDiv);
      }
    }
    if (this.gradingObject.deduct) {
      div.appendChild(document.createElement('hr'));
      i = 0;
      for (let deductOption of this.gradingObject.deduct) {
        ++i;

        let newCheckDiv = document.createElement('div');

        let checkBoxId = 'grader-deduct-option' + i;

        let newCheckLabel = document.createElement('label');
        newCheckLabel.htmlFor = checkBoxId;
        newCheckLabel.innerHTML = `-(${deductOption.perc}%) ${deductOption.desc}`;

        var newCheckBox = document.createElement('input');
        newCheckBox.type = 'checkbox';
        newCheckBox.id = checkBoxId;
        newCheckBox.className = 'grader-deduct-option checkbox-2x mr-3';
        newCheckBox.onclick = () => this.calculateGrader();
        newCheckBox.setAttribute('grader-idx', `${i - 1}`);

        newCheckDiv.appendChild(newCheckBox);
        newCheckDiv.appendChild(newCheckLabel);

        div.appendChild(newCheckDiv);
      }
    }
    if (!this.gradingModel) this.calculateGrader(); // do not init when manually grading - dont want to overwrite old comments!
  }
  getPointsForPercentage(perc) {
    return this.gradingModel
      ? parseFloat(
          (
            (perc * (this.gradingModel.correct_score - this.gradingModel.incorrect_score)) / 100 +
            this.gradingModel.incorrect_score
          ).toFixed(2)
        )
      : 0;
  }
  calculateGrader() {
    let graderDiv = document.getElementById(this.graderDivId);
    let startFrom = +graderDiv.getAttribute('data-start-from');
    let msg;
    let allCorrectOption = graderDiv.querySelectorAll('.grader-allcorrect-option:checked');
    if (allCorrectOption.length > 0) {
      msg = this.gradingObject.allCorrect;
      startFrom = 100;
      for (let checkbox of graderDiv.querySelectorAll('input.grader-add-option')) {
        checkbox.checked = false;
      }
      for (let checkbox of graderDiv.querySelectorAll('input.grader-deduct-option')) {
        checkbox.checked = false;
      }
    } else {
      let msgAdd = '';
      let msgDeduct = '';

      if (this.gradingObject.add && this.gradingObject.add.length) {
        for (let checkbox of graderDiv.querySelectorAll('input.grader-add-option:checked')) {
          let item = this.gradingObject.add[checkbox.getAttribute('grader-idx')];
          msgAdd += !!item.show ? `\n - ${item.desc}` : '';
          startFrom += item.perc;
        }
      }
      let hasDeduct = this.gradingObject.deduct && this.gradingObject.deduct.length;
      if (!hasDeduct && startFrom > 100) startFrom = 100;
      let hasError = false;
      if (hasDeduct) {
        for (let checkbox of graderDiv.querySelectorAll('input.grader-deduct-option:checked')) {
          let item = this.gradingObject.deduct[checkbox.getAttribute('grader-idx')];
          msgDeduct += item.show || !item.show ? `\n - ${item.desc}` : '';
          startFrom -= item.perc;
          hasError = true;
        }
      }
      if (startFrom < 0) startFrom = 0;
      msg =
        ((this.gradingObject.intro && this.gradingObject.intro.always) || '') +
        this.prep(msgAdd) +
        ((hasError && this.gradingObject.intro && this.prep(this.gradingObject.intro.error)) ||
          '') +
        this.prep(msgDeduct) +
        ((hasError && this.gradingObject.outro && this.prep(this.gradingObject.outro.error)) ||
          '') +
        ((this.gradingObject.outro && this.prep(this.gradingObject.outro.always)) || '');
    }
    document.getElementById(this.textAreaId).value = msg;
    let currPercSpan = document.getElementById('grader-current-perc-' + this.graderDivId);
    let currPtsSpan = document.getElementById('grader-current-pts-' + this.graderDivId);
    this.setScoreColors(currPercSpan, startFrom);
    this.setScoreColors(currPtsSpan, startFrom);
    currPercSpan.innerHTML = startFrom + '%';
    currPtsSpan.innerHTML = this.getPointsForPercentage(startFrom) + ' pts';

    if (this.gradingModel && this.scoreInputId) {
      let score = this.getPointsForPercentage(startFrom);
      try {
        let scoreElement = document.getElementById(this.scoreInputId);
        scoreElement.value = score;
        scoreElement.classList.remove('bg-orange');
        scoreElement.classList.add('bg-orange');
      } catch (error) {
        console.error(error);
      }
    }
  }
  setScoreColors(elem, perc) {
    elem.classList.remove('badge-primary');
    elem.classList.remove('badge-success');
    elem.classList.remove('badge-danger');
    elem.classList.remove('badge-warning');
    if (perc > 75) {
      elem.classList.add('badge-success');
    } else if (perc > 50) {
      elem.classList.add('badge-primary');
    } else if (perc > 25) {
      elem.classList.add('badge-warning');
    } else {
      elem.classList.add('badge-danger');
    }
  }
  prep(str) {
    return str && !str.startsWith('\n') ? `\n${str}` : str;
  }
};
