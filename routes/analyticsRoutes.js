'use strict';

var middleware = require('../common/middleware');
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var globals = require('../common/globals');
var db = require('../db').db;
var utils = require.main.require('./common/utils');
var winston = require('winston');
var analyticsService = require('../services/analyticsService');
var dendrogramService = require('../services/dendrogramService');
var plagiarismDetectionService = require('../services/plagiarismDetectionService');
var realTimePlagService = require.main.require('./services/realTimePlagService');

var http = require('http');
var ejs = require('ejs');
var fs = require('fs');
var path = require('path');
var Diff = require('text-diff');
const archiver = require('archiver');

var mongoose = require('mongoose');
var mongoDb = mongoose.connection;
var TestLog = require.main.require('./models/TestLogModel.js');

var timeIntervals = ['minute', 'hour', 'day', 'week', 'month', 'quarter', 'year'];

router.get(
  '/question/multiple/percentages',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    let filterString = '';
    const filters = [];

    const activity = req.query.activity;
    const type = req.query.type;
    const authorID = req.query.authorID;
    const testID = req.query.testID;
    const metrics = req.query.metrics;

    if (activity) {
      if (activity !== 'all') {
        filters.push(`question.is_active = ${activity}`);
      }
    }
    if (type) {
      if (type !== 'all') {
        filters.push(`question_type.id = ${type}`);
      }
    }
    if (authorID) {
      if (authorID !== 'all') {
        filters.push(`question.id_user_created = ${authorID}`);
      }
    }
    if (testID) {
      if (testID !== 'all') {
        filters.push(`test.id = ${testID}`);
      }
    }
    if (filters.length) {
      filterString = 'WHERE ' + filters.join(' AND ');
    }

    // let metricsQuery = 'round(SUM(CASE WHEN(is_correct = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_question), 2)';

    // if (metrics) {
    //     if (metrics !== 'correct') {
    //         metricsQuery = 'round(SUM(CASE WHEN(is_unanswered = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_question), 2)';
    //     }
    // }
    let metricsQuery =
      'ROUND(AVG(CASE WHEN(test_instance_question.score_perc) < 0 THEN 0 ELSE test_instance_question.score_perc END), 2)';
    db.any(
      `SELECT
                    percentage, question_count,
                    json_agg(json_build_object(
                        'id_question', id_question,
                        'question_text', question_text,
                        'is_active', is_active,
                        'question_type', type_name,
                        'id_author', id_user_created,
                        'author_first_name', first_name,
                        'author_last_name', last_name
                )) as questions,
                    json_agg(json_build_object(
                        'id/ver/act', id_ver_act,
                        'text', question_text,
                        'user/time', user_time,
                        'type', type_name,
                        'details', details
                )) as info
                    FROM
                    (SELECT
                        '<a target="_blank" href="/question/edit/' || id_question || '">' || id_question || '</a>' || '<br/>' || question.version || '<br>' || question.is_active as id_ver_act,
                        substring(app_user.first_name from 1 for 1) || '. ' || app_user.last_name || '<br/>' || question.ts_modified as user_time,
                        question.version, question.ts_modified,
                        '<a href="/analytics/questions/single/' || id_question || '" class="btn btn-info" role="button">Analytics</a>' as details,
                        test_instance_question.id_question, SUBSTRING(question.question_text from 1 for 20) || '...' as question_text, question.is_active, question_type.type_name,
                        question.id_user_created, app_user.first_name, app_user.last_name,
                        ${metricsQuery} as percentage,
                        COUNT(id_question) AS question_count
                    FROM test_instance_question
                        JOIN question
                        ON question.id = test_instance_question.id_question
                        JOIN test_instance
                        ON test_instance_question.id_test_instance = test_instance.id
                        JOIN test
                        ON test_instance.id_test = test.id AND test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}
                        JOIN question_type
                        ON question.id_question_type = question_type.id
                        JOIN app_user
                        ON app_user.id = question.id_user_created
                        ${filterString}
                        GROUP BY id_question, question_text, is_active, type_name,
                        question.id_user_created, app_user.first_name, app_user.last_name, question.version, question.ts_modified, details) as result
                    GROUP BY percentage, question_count
                    ORDER BY question_count`
    )
      .then((data) => {
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching question instances: ' + error.message)
        );
      });
  }
);

router.get(
  '/test/:id([0-9]{1,10})/instances',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT test_instance.*,
                to_json(ts_started) as ts_started,
                to_json(ts_submitted) as ts_submitted,
                test.ts_available_from, test.ts_available_to
                FROM test_instance
                    JOIN test
                    ON test_instance.id_test = test.id AND test.id = ${id}
                    WHERE test.id_course = ${id_course} AND test.id_academic_year = ${id_acmdYear}`
    )
      .then((data) => {
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching test instances: ' + error.message)
        );
      });
  }
);

router.get(
  '/highscore',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT dense_RANK () OVER (ORDER BY sum(score) desc) as rank, student.alt_id2, student.id
                    , student.first_name, student.last_name,
                    sc.class_group, count(*) as exam_no,
                    CASE WHEN (sum(score) is null ) then 0 else sum(score) END as total,
                    sum(max_score) as max,
                    CASE WHEN(sum(max_score) = 0) THEN 0 ELSE round(100.0 * sum(score) / sum(max_score), 2) END as perc
                  FROM test_instance ti
                  JOIN v_test_stats AS test
                    ON ti.id_test = test.id
                  JOIN student
                    on ti.id_student = student.id
                  JOIN student_course sc
                    ON sc.id_student = ti.id_student
                    AND sc.id_course = test.id_course
                    AND sc.id_academic_year = test.id_academic_year
                    AND COALESCE(sc.class_group, '-') <> 'Teachers'
                 WHERE test.id_course = ${id_course}
                   AND test.id_academic_year = ${id_acmdYear}
                 --  AND not test_score_ignored
                 --  AND test.use_in_stats
              GROUP BY student.alt_id2, student.first_name, student.last_name, student.id, sc.class_group
                 ORDER BY total DESC`
    )
      .then((data) => {
        res.appRender('analyticsHighScoreView', {
          title: 'High scores',
          list: {
            headers: [
              {
                rank: 'Rank',
              },
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return utils.getTinyImageHtml(row.alt_id2);
                },
              },
              {
                alt_id2: 'alt id2',
              },
              {
                first_name: 'First name',
              },
              {
                last_name: 'Last name',
              },
              {
                class_group: 'Group',
              },
              {
                exam_no: 'No of exams',
              },
              {
                total: 'Total',
              },
              {
                max: 'Max',
              },
              {
                perc: 'Perc',
              },
            ],
            links: [
              {
                label: 'Score',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return `/analytics/score/student/${row['id']}`;
                },
                __formatter: function (row) {
                  return `<i class="fas fa-chart-line"></i>`;
                },
              },
            ],
            rows: data,
          },
        });
      })
      .catch((error) => {
        return next(error);
      });
  }
);

router.get(
  '/test/:id([0-9]{1,10})/results',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT ROUND(perc, 2) as percentage, COUNT(ROUND(perc, 2)) as student_count
                FROM
                    (SELECT GREATEST(0, score_perc) as perc
                    FROM test_instance
                    JOIN test
                    ON test_instance.id_test = test.id AND test.id = ${id}
                    WHERE test.id_course = ${id_course} AND test.id_academic_year = ${id_acmdYear}) as result
                GROUP BY percentage`
    )
      .then((data) => {
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching test instances: ' + error.message)
        );
      });
  }
);

router.get(
  '/test/:id([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    const promises = [];

    promises.push(
      db.any(`SELECT
                    json_agg(json_build_object(
                        'ID/Title', id_title,
                        'User/Time', user_time,
                        'No. of Questions', questions_no,
                        'Pass Percentage', pass_percentage,
                        'Available From', ts_available_from,
                        'Available To', ts_available_to,
                        'Type', type_name
                )) as info
                    FROM
                    (SELECT
                        '<a target="_blank" href="/test/def/edit/' || test.id || '">' || test.id || '</a>' || '<br/>' || test.title as id_title,
                        substring(app_user.first_name from 1 for 1) || '. ' || app_user.last_name || '<br/>' || test.ts_modified as user_time,
                        test.questions_no, test.pass_percentage, test.ts_available_from, test.ts_available_to, test_type.type_name
                    FROM test
                        JOIN course
                        ON test.id_course = course.id
                        JOIN app_user
                        ON test.id_user_created = app_user.id
                        JOIN test_type
                        ON test.id_test_type = test_type.id
                        WHERE test.id = ${id}) as result`)
    );
    promises.push(
      db.any(`SELECT
                                test.title
                            FROM test
                            WHERE test.id = ${id}`)
    );
    Promise.all(promises)
      .then((data) => {
        res.json({
          info: data[0][0].info,
          title: data[1][0].title,
        });
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/questions/bulk',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const promises = [];
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    promises.push(
      db.any(
        `SELECT DISTINCT question_type.id, question_type.type_name
                FROM question_type
                JOIN question ON question_type.id = question.id_question_type
                JOIN question_node on question.id = question_node.id_question
                 AND id_node in
                            (SELECT id_child from v_roots_children WHERE id_root = (SELECT id_root_node FROM course WHERE id = $(id_course)))
                `,
        {
          id_course: req.session.currCourseId,
        }
      )
    );
    promises.push(
      db.any(`SELECT DISTINCT
                                app_user.id, first_name, last_name
                             FROM test_instance_question
                                JOIN question
                                ON question.id = test_instance_question.id_question
                                JOIN test_instance
                                ON test_instance_question.id_test_instance = test_instance.id
                                JOIN test
                                ON test_instance.id_test = test.id
                                JOIN app_user
                                ON app_user.id = question.id_user_created
                                WHERE test.id_course = ${id_course} AND test.id_academic_year = ${id_acmdYear}
                            ORDER BY last_name`)
    );
    promises.push(
      db.any(`SELECT test.id, title_abbrev, title
                                FROM test
                               WHERE test.id_course = ${id_course}
                                 AND test.id_academic_year = ${id_acmdYear}
                            ORDER BY test_ordinal`)
    );
    Promise.all(promises)
      .then((data) => {
        res.appRender(
          'analyticsQuestionsBulkView',
          {
            questionTypes: data[0],
            authors: data[1],
            tests: data[2],
          },
          'analyticsQuestionsBulkViewScript'
        );
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);

router.get(
  '/questions/single/:id([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const promises = [];
    const questionID = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    promises.push(
      db.any(`SELECT
                                question.id,
                                '<a target="_blank" href="/question/edit/' || question.id || '">' || question.id || '</a>' || '<br/>' || question.version || '<br>' || question.is_active as id_ver_act,
                                substring(app_user.first_name from 1 for 1) || '. ' || app_user.last_name || '<br/>' || question.ts_modified as user_time,
                                question_type.type_name as type, question.question_text as text
                            FROM question
                            JOIN question_type
                            ON question.id_question_type = question_type.id
                            JOIN app_user
                            ON app_user.id = question.id_user_created
                            WHERE question.id = ${questionID}`)
    );

    promises.push(
      db.any(`SELECT
                                round(SUM(CASE WHEN(is_correct = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_question), 2) AS correct,
                                round(SUM(CASE WHEN(is_incorrect = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_question), 2) AS incorrect,
                                round(SUM(CASE WHEN(is_unanswered = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_question), 2) AS unanswered
                            FROM test_instance_question
                                WHERE id_question = ${questionID}
                                GROUP BY id_question`)
    );

    promises.push(
      db.any(`SELECT
                                is_correct, is_incorrect, is_unanswered, ts_modified
                            FROM test_instance_question
                            WHERE id_question = ${questionID}`)
    );

    Promise.all(promises)
      .then((data) => {
        const percentages = data[1];
        const d = [
          {
            title: 'Correct',
            percentage: percentages[0].correct,
          },
          {
            title: 'Incorrect',
            percentage: percentages[0].incorrect,
          },
          {
            title: 'Unanswered',
            percentage: percentages[0].unanswered,
          },
        ];

        res.appRender(
          'analyticsQuestionsSingleView',
          {
            info: data[0],
            percentages: d,
            timeIntervals,
            instances: data[2],
          },
          'analyticsQuestionsSingleViewScript'
        );
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);

router.get(
  '/questions/single/:id([0-9]{1,10})/scores',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const questionID = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    let interval = req.query.interval;

    let intervalQuery = 'ts_modified';
    interval = interval.toLowerCase();
    if (timeIntervals.includes(interval)) {
      intervalQuery = `date_trunc('${interval}', ts_modified)`;
    }

    db.any(
      `SELECT
                    count(id) as instances_count, round(SUM(score_perc)::numeric/COUNT(id), 2) as average_score,
                    ${intervalQuery} as time
                FROM test_instance_question
                WHERE id_question = ${questionID} AND score IS NOT NULL
                GROUP BY time`
    )
      .then((data) => {
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);

router.get(
  '/tests/single',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT test.*
                FROM v_test_stats AS test
                    WHERE test.id_academic_year = ${id_acmdYear}
                    AND test.id_course = ${id_course}
                    --AND NOT test_score_ignored
                    --AND use_in_stats
                    `
    )
      .then((data) => {
        res.appRender(
          'analyticsTestsView',
          {
            tests: data,
          },
          'analyticsTestsViewScript'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/test/pass/percentages/:id([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id = req.params.id;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT
                    round(SUM(CASE WHEN(passed = true) THEN 1.0 ELSE 0 END)::numeric/COUNT(id_test), 2) AS passed
                FROM test_instance
                    JOIN test
                    ON test_instance.id_test = test.id
                    WHERE id_test = ${id} AND test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}
                    GROUP BY id_test`
    )
      .then((data) => {
        let d = [
          {
            title: 'Passed',
            percentage: 0,
          },
          {
            title: 'Failed',
            percentage: 0,
          },
        ];
        if (data.length !== 0) {
          const passed = data[0].passed;
          d[0].percentage = passed;
          d[1].percentage = (1 - passed).toFixed(2);
        }
        res.json(d);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching test instances: ' + error.message)
        );
      });
  }
);

router.get(
  '/test/pass/percentages',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT test.id, test.title, test.title_abbrev,
                    array_agg(ROUND(GREATEST(0, score_perc), 2)) AS scores
                FROM test_instance
                JOIN test
                ON test_instance.id_test = test.id
                WHERE test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}
                AND test.title_abbrev <> ''
                AND test.title_abbrev IS NOT NULL
                AND NOT test_score_ignored
                GROUP BY test.id, test.title
                ORDER BY test.test_ordinal`
    )
      .then((data) => {
        // console.log(data);
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching test instances: ' + error.message)
        );
      });
  }
);

router.get(
  '/question/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.any(
      `
               SELECT '<a target="_blank" href="/question/edit/' || question.id || '">' || question.id || '</a>' || '<br/>' || version || '<br>' || is_active as "id/ver/act",
                      substring(question_text from 1 for 45), substring(first_name from 1 for 1) || '. ' || last_name || '<br/>' || question.ts_modified as "user/time",
                      type_name as type
                 FROM question
                 JOIN question_type on id_question_type = question_type.id
                 JOIN app_user on id_user_created = app_user.id
                WHERE question.id = $(id_question)
                ORDER BY is_active desc, question.id
            `,
      {
        id_node: req.params.id_question,
      }
    )
      .then((data) => {
        res.json({
          qs: data,
        });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);

router.get(
  '/tests/bulk',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT test.*
                FROM test
                    WHERE test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}`
    )
      .then((data) => {
        res.appRender(
          'analyticsTestsBulkView',
          {
            tests: data,
          },
          'analyticsTestsBulkViewScript'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

// behaviour analytics route
router.get(
  '/test/:id_test([0-9]{1,10})/logs',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    const testID = req.params.id_test;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    db.any(
      `SELECT test_instance.id, test_instance.id_student
                FROM test_instance
                    JOIN test
                    ON test_instance.id_test = test.id
                    WHERE test_instance.id_test = ${testID} AND test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}`
    )
      .then((instancesIDs) => {
        let logsQueries = [];
        let dbg = [];
        instancesIDs.forEach((result) => {
          logsQueries.push(
            TestLog.find({
              _id: result.id,
            }).lean()
          );
          dbg.push(result.id);
        });
        Promise.all(logsQueries).then((logs) => {
          logs = logs.map((d) => d[0]);
          let dbgLogs = logs.map((x) => {
            if (x && x._id) return x._id;
            else return undefined;
          });
          let newLogs = logs
            .filter((x) => x && x._id)
            .map((log) => {
              let newLog = log;
              let studentID = instancesIDs.find((instance) => {
                return instance.id === newLog._id;
              }).id_student;
              newLog.id_student = studentID;
              return newLog;
            });
          res.json(newLogs);
        });
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/test/:id_test([0-9]{1,10})/participants',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    const testID = req.params.id_test;
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    db.any(
      `SELECT
            DISTINCT student.id, student.last_name, student.first_name
            FROM test_instance
                JOIN student
                ON test_instance.id_student = student.id
                JOIN test
                ON test_instance.id_test = test.id
                WHERE test_instance.id_test = ${testID} AND test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}`
    )
      .then((data) => {
        res.json(data);
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching test participants: ' + error.message)
        );
      });
  }
);

router.get(
  '/behaviour',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;
    db.any(
      `SELECT id, title
                FROM test
                    WHERE test.id_academic_year = ${id_acmdYear} AND test.id_course = ${id_course}`
    )
      .then((data) => {
        res.appRender(
          'analyticsStudentBehaviourView',
          {
            tests: data,
          },
          'analyticsStudentBehaviourViewScript'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/plagiarismdetection',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const promises = [];
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    promises.push(
      db.any(`SELECT title "name", id "value"
                    FROM test
                    WHERE test.id_academic_year = ${id_acmdYear}
                    AND test.id_course = ${id_course}
                    ORDER BY test.test_ordinal DESC`)
    );

    promises.push(db.any(`SELECT name, id "value" FROM plag_detection_algorithm;`));

    promises.push(db.any(`SELECT name, id "value" FROM dendrogram_distance_algorithm;`));

    Promise.all(promises)
      .then((data) => {
        res.appRender(
          'analyticsPlagiarismDetectionView',
          {
            exams: data[0],
            tests: data[1],
            distanceMethods: data[2],
          },
          'analyticsPlagiarismDetectionViewScript',
          'dendrogramViewCSS'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/plagiarismdetection/table-data',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      const data = await analyticsService.getPlagiarismData(
        req.query.examId,
        req.query.testId,
        req.query.cutoffPercentage,
        req.query.recalculate
      );
      let templateString = fs.readFileSync(
        `${path.dirname(
          require.main.filename
        )}/views/analytics/analyticsPlagiarismDetectionTable.ejs`,
        'utf-8'
      );
      res.end(ejs.render(templateString, data));
    } catch (err) {
      winston.error(err);
      winston.error('Failed to get plagiarism analytics data ' + JSON.stringify(err));
    }
  }
);

router.get(
  '/plagiarismdetection/dendrogram-data',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      const data = await analyticsService.getPlagiarismData(
        req.query.examId,
        req.query.testId,
        req.query.cutoffPercentage,
        req.query.recalculate
      );
      data.distanceMethodId = req.query.distanceMethodId;
      res.json(dendrogramService.clusterData(data));
    } catch (err) {
      winston.error(err);
      // winston.error('Failed to get plagiarism analytics data ' +
      //     JSON.stringify(err));
    }
  }
);

router.get(
  '/plagiarismdetection/cached',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    const data = await db.any(
      `SELECT id_plag_detection_algorithm FROM plag_detection_data
            WHERE id_plag_detection_algorithm = ${req.query.testId}
                AND id_test = ${req.query.examId};`
    );
    let isCached = data.length > 0;
    res.json(isCached);
  }
);

router.get(
  '/plagiarismdetection/exam/:id_exam?',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    const id_test = req.params.id_exam;
    const id_first_student = req.query.idFirstStudent;
    const id_second_student = req.query.idSecondStudent;

    let rows = await db.any(
      `SELECT
                test_instance_question.id_question,
                test_instance_question.student_answer_code,
                test_instance.id_student
            FROM test_instance_question
            JOIN test_instance on test_instance_question.id_test_instance = test_instance.id
            JOIN student on test_instance.id_student = student.id
            WHERE id_test_instance in (SELECT id from test_instance where id_test = ${id_test})
                AND (student.id = ${id_first_student} OR student.id = ${id_second_student})
                AND student_answer_code is not NULL
            ORDER by id_question, id_student;`
    );

    let firstStudent = await db.one(
      `SELECT id, first_name, last_name, alt_id2
            FROM student
            WHERE id = ${id_first_student}`
    );
    firstStudent.imgHtml = utils.getBigImageHtml(firstStudent.alt_id2);
    let secondStudent = await db.one(
      `SELECT id, first_name, last_name, alt_id2
            FROM student
            WHERE id = ${id_second_student}`
    );
    secondStudent.imgHtml = utils.getBigImageHtml(secondStudent.alt_id2);
    let examTitle = await db.one(`SELECT title FROM test WHERE id = ${id_test}`);
    examTitle = examTitle.title;

    let similarityMethodNames = await db.any(
      `SELECT name, id FROM plag_detection_algorithm ORDER BY id;`
    );
    let similarityMethods = plagiarismDetectionService.similarityMethods;

    let questions = {};
    for (let row of rows) {
      if (questions[row['id_question']] == undefined) {
        questions[row['id_question']] = {
          [firstStudent.id]: 'No answer',
          [secondStudent.id]: 'No answer',
        };
      }
      questions[row['id_question']][row['id_student']] =
        row['student_answer_code'] == undefined ||
        row['student_answer_code'].replace(/\s+/g, '') == ''
          ? 'No answer'
          : row['student_answer_code'];
    }
    for (let question of Object.keys(questions)) {
      let codeFirst = questions[question][firstStudent.id];
      let codeSecond = questions[question][secondStudent.id];

      if (codeFirst != 'No answer' && codeSecond != 'No answer') {
        var diff = new Diff();
        var textDiff = diff.main(codeFirst, codeSecond);
        questions[question]['difference'] = diff.prettyHtml(textDiff);
        questions[question].similarity = {};

        for (let method of similarityMethodNames) {
          let similarity = similarityMethods[method.id](
            codeFirst.replace(/\s+/g, ','),
            codeSecond.replace(/\s+/g, ',')
          );
          questions[question].similarity[method.id] =
            Math.round((similarity + Number.EPSILON) * 10000) / 100;
        }
      } else {
        questions[question]['difference'] = "Can't compare";
      }
    }
    res.appRender(
      'analyticsStudentsCompareView',
      {
        firstStudent,
        secondStudent,
        questions,
        examTitle,
        similarityMethodNames,
      },
      'analyticsStudentsCompareViewScript'
    );
  }
);

router.get(
  '/plagiarismdetection/jplagdata/:id_exam?',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const id_test = req.params.id_exam;
    const sort_by = req.query.sortby;
    res.writeHead(200, {
      'Content-Type': 'application/zip',
      'Content-disposition': `attachment; filename=exam-${id_test}-sorted_by=${sort_by}.zip`,
    });

    var zip = archiver('zip');
    zip.pipe(res);

    let questions = await db.any(
      `SELECT
                test_instance_question.id_question,
                test_instance_question.student_answer_code,
                test_instance.id_student, student.first_name,
                student.last_name,
                programming_language.extension
            FROM test_instance_question
            JOIN test_instance on test_instance_question.id_test_instance = test_instance.id
            JOIN student on test_instance.id_student = student.id
            JOIN question on test_instance_question.id_question = question.id
            JOIN question_programming_language on question_programming_language.id_question = question.id
            JOIN programming_language on question_programming_language.id_programming_language = programming_language.id
            WHERE id_test_instance in (SELECT id from test_instance where id_test = ${id_test})
                AND is_unanswered = false
                AND student_answer_code is not null
            ORDER by id_question, id_student;`
    );

    if (sort_by == 'student') {
      for (let question of questions) {
        zip.append(question.student_answer_code, {
          name: `${question.first_name}-${question.last_name}-${question.id_student}/question-${question.id_question}.${question.extension}`,
        });
      }
    } else {
      for (let question of questions) {
        zip.append(question.student_answer_code, {
          name: `question-${question.id_question}/${question.first_name}-${question.last_name}-${question.id_student}.${question.extension}`,
        });
      }
    }

    zip.finalize();
  }
);

router.get(
  '/time-dependent-plagiarism',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    const promises = [];
    const id_course = req.session.currCourseId;
    const id_acmdYear = req.session.currAcademicYearId;

    promises.push(
      db.any(`SELECT title "name", id "value"
                        FROM test
                        WHERE test.id_academic_year = ${id_acmdYear}
                        AND test.id_course = ${id_course}
                        ORDER BY test.test_ordinal DESC`)
    );

    promises.push(db.any(`SELECT name, id "value" FROM plag_detection_algorithm;`));

    Promise.all(promises)
      .then((data) => {
        res.appRender(
          'analyticsTimeDependentPlagView',
          {
            exams: data[0],
            tests: data[1],
          },
          'analyticsTimeDependentPlagViewScript'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching tests: ' + error.message));
      });
  }
);

router.get(
  '/time-dependent-plagiarism/calculate-data',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      await realTimePlagService.calculateDataAfterExam(req.query.examId, req.query.testId);
      res.json({ done: true });
    } catch (err) {
      winston.error(err);
      winston.error('Failed to get plagiarism analytics data ' + JSON.stringify(err));
    }
  }
);

router.get(
  '/time-dependent-plagiarism/compare/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let data = await realTimePlagService.getQuestionCompareData(
      req.params.id_test,
      req.params.id_question,
      req.query.idFirstStudent,
      req.query.idSecondStudent
    );
    res.appRender('analyticsTimeDependentQuestionCompareView', {
      ...data,
      autorefresh: req.query.ar,
    });
  }
);

router.get(
  '/time-dependent-plagiarism/compare/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let data = await realTimePlagService.getTestCompareData(
      req.params.id_test,
      req.query.idFirstStudent,
      req.query.idSecondStudent,
      req.query.id_plag_method
    );
    res.appRender('analyticsTimeDependentTestCompareView', {
      ...data,
      autorefresh: req.query.ar,
    });
  }
);

router.get(
  '/time-dependent-plagiarism/test/:id_test([0-9]{1,10})/questions',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let questions = await db.any(`
            SELECT DISTINCT id_question
            FROM test_instance_question
            JOIN test_instance ON test_instance.id = test_instance_question.id_test_instance
            WHERE id_test_instance in (SELECT id from test_instance where id_test = ${req.params.id_test})
            ORDER BY id_question;
        `);

    let urls = [];
    for (let question of questions)
      urls.push(
        `/analytics/time-dependent-plagiarism/test/${req.params.id_test}/question/${question.id_question}?id_plag_method=${req.query.id_plag_method}&cutoff=80`
      );

    res.appRender('analyticsOpenQuestionsCompare', {
      urls: urls,
    });
  }
);

// TODO: create table for this question
router.get(
  '/time-dependent-plagiarism/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let data = await realTimePlagService.getQuestionTableData(
      req.params.id_test,
      req.params.id_question,
      req.query.cutoff,
      req.query.id_plag_method
    );
    res.appRender(
      'analyticsTimeDependentPlagTable',
      {
        ...data,
        id_plag_method: req.query.id_plag_method,
        autorefresh: req.query.ar,
        cutoff: req.query.cutoff,
      },
      'analyticsTimeDependentPlagTableScript'
    );
  }
);

router.get(
  '/time-dependent-plagiarism/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    // let availableMethods = await db.any(`
    //     SELECT DISTINCT plag_detection_algorithm.name, plag_detection_algorithm.id "value"
    //     FROM real_time_plag_detection_data
    //     JOIN plag_detection_algorithm ON real_time_plag_detection_data.test_id = plag_detection_algorithm.id
    //     WHERE exam_id = ${req.params.id_test};
    // `)

    let data = await realTimePlagService.getExamTableData(
      req.params.id_test,
      req.query.cutoff,
      req.query.id_plag_method
    );
    res.appRender(
      'analyticsTimeDependentPlagTable',
      {
        ...data,
        id_plag_method: req.query.id_plag_method,
        autorefresh: req.query.ar,
        cutoff: req.query.cutoff,
      },
      'analyticsTimeDependentPlagTableScript'
    );
  }
);

router.get(
  '/time-dependent-plagiarism/cached',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    const data = await db.any(
      `SELECT id_plag_detection_algorithm FROM real_time_plag_detection_data
            WHERE id_plag_detection_algorithm = ${req.query.testId}
                AND id_test = ${req.query.examId};`
    );
    let isCached = data.length > 0;
    res.json(isCached);
  }
);

router.get('/score', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res) => {
  const id_academic_year = req.session.currAcademicYearId,
    id_course = req.session.currCourseId;

  try {
    const data = await db.task((t) =>
      t.batch([
        // students
        db.one(
          `SELECT COUNT(DISTINCT student.id) students
                        FROM student JOIN student_course sc
                            ON student.id = sc.id_student
                        WHERE sc.id_course = $(id_course)
                            AND sc.id_academic_year = $(id_academic_year)`,
          {
            id_course: id_course,
            id_academic_year: id_academic_year,
          }
        ),
        // tests
        db.any(
          `SELECT title "name", id "value"
                        FROM test t
                        WHERE t.id_academic_year = $(id_academic_year)
                            AND t.id_course = $(id_course)
                        ORDER BY t.test_ordinal DESC`,
          {
            id_academic_year: id_academic_year,
            id_course: id_course,
          }
        ),
      ])
    );

    let tests = data[1];
    tests.unshift({ value: -1, name: 'Only scored tests' });
    tests.unshift({ value: 0, name: 'All tests' });

    res.appRender(
      'analyticsScoreView',
      {
        students: data[0],
        tests: tests,
      },
      'analyticsScoreViewScript',
      'analyticsScoreViewCSS'
    );
  } catch (err) {
    winston.error('Failed to get score analytics ' + JSON.stringify(err));
  }
});

router.get(
  '/score/distribution/total',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let testId, onlyScored;
    if (req.query.testId) {
      testId = parseInt(req.query.testId);
      if (testId > 0 && req.session.rolename === globals.ROLES.STUDENT) {
        let studentId = getSafeIdStudent(req);
        if (!(await canStudentSeeExam(studentId, testId))) {
          res.json([]);
          return;
        }
      }
    }
    if (testId == 0) {
      //     testId = null;
      // } else if (testId == -1) {
      testId = null;
      onlyScored = true;
    }

    try {
      const data = await analyticsService.getScoreAnalyticsByAllGroups(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        testId,
        onlyScored,
        parseInt(req.query.resolution) || 20
      );
      res.json(data);
    } catch (err) {
      winston.error(err);
      winston.error('Failed to get total score analytics ' + JSON.stringify(err));
    }
  }
);
router.get(
  '/manualscore/distribution/',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    if (req.query.testId) {
      let testId = parseInt(req.query.testId);
      try {
        const data = await analyticsService.getManualGradingExamDistribution(
          testId,
          parseInt(req.query.resolution) || 20
        );
        res.json(data);
      } catch (err) {
        winston.error(err);
        res.send(500);
      }
    } else {
      res.send(400, 'Missing testId');
    }
  }
);

router.get(
  '/score/distribution/time',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let studentId = getSafeIdStudent(req);
    try {
      const data = await analyticsService.getScoreAnalyticsThroughTime(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        studentId,
        true
      );
      res.json(data);
    } catch (err) {
      winston.error('Failed to get total score distribution analytics', JSON.stringify(err));
    }
  }
);

router.get(
  '/score/distribution/timeQuestion',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let studentId = getSafeIdStudent(req);
    let testId = parseInt(req.query.testId) || 0;
    if (testId == 0) {
      res.json([]);
    } else if (
      testId > 0 &&
      req.session.rolename === globals.ROLES.STUDENT &&
      !(await canStudentSeeExam(studentId, testId))
    ) {
      res.json([]);
    } else {
      try {
        const data = await analyticsService.getQuestionScoreAnalyticsThroughTime(
          req.session.currAcademicYearId,
          req.session.currCourseId,
          studentId,
          testId,
          true
        );
        res.json(data);
      } catch (err) {
        winston.error('Failed to get question score distribution analytics' + JSON.stringify(err));
      }
    }
  }
);

router.get(
  '/score/distribution/groups',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    let testId, onlyScored;
    if (req.query.testId) {
      testId = parseInt(req.query.testId);
      if (testId > 0 && req.session.rolename === globals.ROLES.STUDENT) {
        let studentId = getSafeIdStudent(req);
        if (!(await canStudentSeeExam(studentId, testId))) {
          res.json([]);
          return;
        }
      }
    }
    if (testId == 0) {
      testId = null;
    } else if (testId == -1) {
      testId = null;
      onlyScored = true;
    }

    try {
      const data = await analyticsService.getScoreAnalyticsByGroups(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        testId,
        onlyScored,
        parseInt(req.query.resolution) || 20
      );
      res.json(data);
    } catch (err) {
      winston.error('Failed to get group score analytics ' + JSON.stringify(err));
    }
  }
);

router.get(
  '/score/student/:id_student?',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    const id_academic_year = req.session.currAcademicYearId;
    const id_course = req.session.currCourseId;
    const id_student = getSafeIdStudent(req);

    try {
      const data = await Promise.all([
        db.any(
          `SELECT t.title || ' (' || COALESCE(t.title_abbrev, '') || ')' as name
                        , t.id as value
                    FROM v_test_stats t
                    JOIN test_instance ti
                      ON t.id = ti.id_test
                     AND ti.id_student = $(id_student)
                    WHERE t.id_academic_year = $(id_academic_year)
                        AND t.id_course = $(id_course)
                    ORDER BY ti.ts_submitted DESC`,
          {
            id_academic_year: id_academic_year,
            id_course: id_course,
            id_student: id_student,
          }
        ),
        db.one(
          `SELECT *
                    FROM student
                    WHERE id = $(id_student)`,
          {
            id_student: id_student,
          }
        ),
      ]);

      let tests = data[0],
        student = data[1];

      res.appRender(
        'analyticsStudentScoreView',
        {
          tests: tests,
          student: student,
          imageHtml: utils.getImageHtml(student.alt_id2),
        },
        'analyticsStudentScoreViewScript'
      );
    } catch (err) {
      winston.error('Failed to get student score analytics ' + JSON.stringify(err));
    }
  }
);

router.get(
  '/score/student/:id_student/scores',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let studentId = getSafeIdStudent(req);
    let testId, onlyScored;

    if (req.query.testId) {
      testId = parseInt(req.query.testId);
      if (testId > 0 && req.session.rolename === globals.ROLES.STUDENT) {
        if (!(await canStudentSeeExam(studentId, testId))) {
          res.json([]);
          return;
        }
      }
    }
    if (testId == 0) {
      testId = null;
    } else if (testId == -1) {
      testId = null;
      onlyScored = true;
    }

    try {
      const score = await analyticsService.getStudentScore(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        studentId,
        testId,
        true,
        2
      );
      const {
        minScore,
        maxScore,
        avgScore,
        winnedScore,
        maxWinnableScore,
        // maxWinnableScoreTest,
        attendedTests,
        heldTests,
      } = await analyticsService.getStudentCourseScores(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        studentId,
        testId,
        2
      );
      const { rank, maxRank, percentileRank } = await analyticsService.getStudentRank(
        req.session.currAcademicYearId,
        req.session.currCourseId,
        studentId,
        testId
      );
      res.json({
        score: score,
        percentileRank: percentileRank,
        minScore: minScore,
        maxScore: maxScore,
        avgScore: avgScore,
        winnedScore: winnedScore,
        maxWinnableScore: maxWinnableScore,
        rank: rank,
        maxRank: maxRank,
        attendedTests: attendedTests,
        heldTests: heldTests,
      });
    } catch (err) {
      winston.error('Failed to get student profile analytics ' + JSON.stringify(err));
    }
  }
);

router.get(
  '/scatterstudents',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    const id_academic_year = req.session.currAcademicYearId;
    const id_course = req.session.currCourseId;
    try {
      const data = await db.any(
        `
                    SELECT 'All tests (use_stats=true, score_ignored=false)' as name, 0 as value, 1e9 as test_ordinal
                    UNION ALL
                    SELECT t.title || ' (' || COALESCE(t.title_abbrev, '') || ')' as name
                        , t.id as value, test_ordinal
                    FROM v_test_stats t
                    WHERE t.id_academic_year = $(id_academic_year)
                      AND t.id_course = $(id_course)
                      --AND NOT test_score_ignored
                      --AND use_in_stats
                    ORDER BY test_ordinal DESC`,
        {
          id_academic_year: id_academic_year,
          id_course: id_course,
        }
      );
      res.appRender(
        'analyticsScatterStudentsView',
        {
          tests: data,
        },
        'analyticsScatterStudentsViewScript'
      );
    } catch (err) {
      winston.error(err);
      winston.error('Failed to get student score analytics ' + JSON.stringify(err));
    }
  }
);
router.get(
  '/scatterstudents/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    const id_academic_year = req.session.currAcademicYearId;
    const id_course = req.session.currCourseId;
    try {
      const data = await db.any(
        `SELECT ti.id_student,
                            s.first_name || ' ' || s.last_name as full_name,
                            s.alt_id2,
                            COALESCE (SUM(ti.score), 0 ) as score
                        FROM v_test_stats t
                        JOIN test_instance ti
                          ON t.id = ti.id_test
                        JOIN student s
                          ON ti.id_student = s.id
                        JOIN student_course sc
                          ON sc.id_student = ti.id_student
                         AND sc.id_course = t.id_course
                         AND sc.id_academic_year = t.id_academic_year
                         AND COALESCE(sc.class_group, '-') <> 'Teachers'
                       WHERE
                       --      t.use_in_stats
                       --  AND t.test_score_ignored = false
                             t.id_course = $(id_course)
                         AND t.id_academic_year = $(id_academic_year)
                         AND (t.id = $(id_test) OR 0 = $(id_test) )
                       GROUP BY ti.id_student, s.alt_id2, full_name
                    ORDER BY score`,
        {
          id_academic_year: id_academic_year,
          id_course: id_course,
          id_test: req.params.id_test,
        }
      );
      res.json(
        data.map((item) => {
          return {
            ...item,
            img: utils.getImageHtml(item.alt_id2),
          };
        })
      );
    } catch (err) {
      winston.error('Failed to get scatterstudents ' + JSON.stringify(err));
      res.json([]);
    }
  }
);

let getSafeIdStudent = function (req) {
  if (req.session.rolename === globals.ROLES.TEACHER) {
    if (req.params.id_student) return parseInt(req.params.id_student);
    if (req.params.studentId) return parseInt(req.params.studentId);
    if (req.query.id_student) return parseInt(req.query.id_student);
    if (req.query.studentId) return parseInt(req.query.studentId);
  }
  return req.session.studentId;
};
let canStudentSeeExam = async function (studentId, testId) {
  const cansee = await db.one(
    'SELECT COUNT(*) as cnt FROM test_instance WHERE id_student = $(id_student) AND id_test = $(id_test)',
    {
      id_student: studentId,
      id_test: testId,
    }
  );
  if (cansee.cnt == 0) {
    winston.error(
      `Warning (security breach): student (${studentId}) trying to see test (${testId}) they have not written!`
    );
    return false;
  } else {
    return true;
  }
};
module.exports = router;
