'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var db = require('../db').db;
// var http = require('http').Server(express);
// var io = require('socket.io')(http);
var lectureService = require.main.require('./services/lectureService');
var TestLog = require.main.require('./models/TestLogModel.js');
var OngoingTests = require.main.require('./models/OngoingTests.js');

/* student routes */

router.get('/enter', function (req, res) {
  res.publicRender('lecture/enterLectureView');
});

router.post('/enter', function (req, res, next) {
  const room_name = req.body.room_name;

  req.session.id_lecture_quiz_instance = undefined;
  req.session.id_test_instance = undefined;
  req.session.anon_alt_id = undefined;
  req.session.room_name = undefined;

  // req.session.room_name = room_name;

  if (req.body.btn === 'anonymous') {
    res.redirect('/lecture/enter/anon/' + room_name);
  } else if (req.body.btn === 'login') {
    res.redirect('/lecture/enter/login/' + room_name);
  }
});

router.get('/enter/anon/:room', function (req, res, next) {
  db.func('chk_get_lecture_quiz_test_instance', [
    req.headers['x-forwarded-for'] || req.connection.remoteAddress,
    req.params.room,
    true,
    null,
  ])
    .then(function (data) {
      winston.debug('chk_get_lecture_quiz_test_instance returned: ', data);
      if (data[0].err_code === 0) {
        req.session.anon_alt_id = data[0].alt_id_student;
        req.session.id_lecture_quiz_instance = data[0].id_test_instance;
        req.session.id_test_instance = data[0].id_test_instance;

        db.one(
          `SELECT id_test, title
                                        FROM test_instance JOIN test on test_instance.id_test = test.id
                                        WHERE test_instance.id = $(id_test_instance)`,
          {
            //id_test_instance: req.session.id_lecture_quiz_instance
            id_test_instance: req.session.id_lecture_quiz_instance,
          }
        ).then(function (item) {
          req.session.id_lecture_quiz = item.id_test; // id_lecture_quiz is test id
          req.session.room_name = req.params.room;

          res.redirect('/lecture/startstudent');
        });
      } else {
        // error while getting lecture quiz instance
        res.redirect('/lecture/enter');
      }
    })
    .catch(function (error) {
      winston.error(error);
      res.redirect('/lecture/enter');
    });
});

router.get('/enter/login/:room', function (req, res, next) {
  if (!req.user) {
    winston.debug('** User is falsy: ' + req.user);
    winston.debug('** Unauthenticated request, redirecting to login.');

    req.session.landingUrl = '/lecture/enter/login/' + req.params.room;

    res.redirect('/auth/login');
  } else {
    // todo
    db.func('chk_get_lecture_quiz_test_instance', [
      req.headers['x-forwarded-for'] || req.connection.remoteAddress,
      req.params.room,
      false,
      req.session.studentId,
    ])
      .then(function (data) {
        winston.debug('chk_get_lecture_quiz_test_instance returned: ' + data);
        if (data[0].err_code === 0) {
          req.session.id_lecture_quiz_instance = data[0].id_test_instance;
          req.session.id_test_instance = data[0].id_test_instance;

          db.one(
            `SELECT id_test, title
                                        FROM test_instance JOIN test on test_instance.id_test = test.id
                                        WHERE test_instance.id = $(id_test_instance)`,
            {
              //id_test_instance: req.session.id_lecture_quiz_instance
              id_test_instance: req.session.id_lecture_quiz_instance,
            }
          ).then(function (item) {
            req.session.id_lecture_quiz = item.id_test; // id_lecture_quiz is test id
            req.session.room_name = req.params.room;

            res.redirect('/lecture/startstudent');
          });
        } else {
          // error while getting lecture quiz instance
          res.redirect('/lecture/enter');
        }
      })
      .catch(function (error) {
        winston.error(error);
        res.redirect('/lecture/enter');
      });
  }
});

router.get('/startstudent', function (req, res, next) {
  var lecture_data = { title: 'Lecture quiz' };
  res.publicRender('lecture/roomView', { data: lecture_data });
});

router.get(
  '/student/getquestion/:ordinal([0-9]{1,2})',
  // middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    var ordinal = parseInt(req.params.ordinal); // TODO: handle it?
    lectureService
      .getQuestion(ordinal, req.session.id_lecture_quiz, true, false) //show correct = true to get correct answers
      .then(function (q) {
        res.json(q);
      })
      .catch(function (q) {
        res.json(q);
      });
  }
);

router.get(
  '/student/gettest',
  // middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.one(
      `SELECT title,
                       questions_no,
                       password
                 FROM test
                 WHERE test.id = $(id_lecture)`,
      {
        id_lecture: req.session.id_lecture_quiz,
      }
    )
      .then(function (test) {
        res.json({
          title: test.title,
          noOfQuestions: test.questions_no,
          password: test.password,
          roomName: req.session.room_name,
        });
      })
      .catch(function (error) {
        winston.error(
          new errors.DbError('An error occured while fetching lecture test: ' + error.message)
        );
        res.json({
          success: false,
          error:
            process.env.NODE_ENV !== 'development'
              ? 'Logged at the server'
              : error.message || error,
        });
      });
  }
);

router.post('/submitanswers', function (req, res, next) {
  var answers = req.body.answers;

  db.one(
    `SELECT first_name, last_name
                FROM test_instance JOIN student ON test_instance.id_student = student.id
                WHERE test_instance.id = $(test_instance_id)`,
    { test_instance_id: req.session.id_lecture_quiz_instance }
  )
    .then(function (row) {
      lectureService
        .updateLectureQuizInstanceWithAnswers(req.session.id_lecture_quiz_instance, answers)
        .then(function (data) {
          // res.redirect('/lecture/enter');
          // return new Promise((resolve, reject) => {
          //     resolve({
          //         success: true
          //         // async_submit : false
          //     });
          // });

          return lectureService.gradeLectureQuiz(req);
        })
        .then(function (respMsg) {
          res.json({
            success: respMsg.success,
            first_name: row.first_name,
            last_name: row.last_name,
            score: respMsg.score.score,
            score_perc: respMsg.score.score_perc,
          });

          // console.log(respMsg);
          // res.json(respMsg);
        })
        .catch(function (error) {
          winston.error('Error while submitting answers! ');
          winston.error(error);
        });
    })
    .catch(function (error) {
      winston.error(new errors.DbError('An error occured while fetching score: ' + error.message));
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    });

  // lectureService.updateLectureQuizInstanceWithAnswers(req.session.id_lecture_quiz_instance, answers)
  //     .then(function(data) {
  //         // res.redirect('/lecture/enter');
  //         // return new Promise((resolve, reject) => {
  //         //     resolve({
  //         //         success: true
  //         //         // async_submit : false
  //         //     });
  //         // });
  //
  //         return lectureService.gradeLectureQuiz(req);
  //
  //     })
  //     .then(function(respMsg) {
  //         console.log(respMsg);
  //         res.json(respMsg);
  //     })
  //     .catch(function(error) {
  //         console.log("Error while submitting answers! " + error);
  //     })
});

router.post('/getscore', function (req, res, next) {
  db.one(
    `SELECT score, score_perc, first_name, last_name
                FROM test_instance JOIN student ON test_instance.id_student = student.id
                WHERE test_instance.id = $(test_instance_id)`,
    { test_instance_id: req.session.id_lecture_quiz_instance }
  )
    .then(function (row) {
      res.json({
        first_name: row.first_name,
        last_name: row.last_name,
        score: row.score,
        score_perc: row.score_perc,
      });
    })
    .catch(function (error) {
      winston.error(new errors.DbError('An error occured while fetching score: ' + error.message));
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    });
});

/* teacher routes */

router.get(
  '/startteacher',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    res.publicRender('lecture/newLectureQuizView');
  }
);

router.post(
  '/startteacher',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    const password = req.body.password;
    // const room_name = req.body.room_name;

    var allow_anon = false;
    // if box checked, allow anonymous
    if (req.body.allow_anon) {
      allow_anon = true;
    }

    const acceptsHTML = req.accepts('html');
    const acceptsJSON = req.accepts('json');

    if (!acceptsHTML && !acceptsJSON) {
      res.status(400).json({
        error: 'Unsupported Accept header value',
      });
      return;
    }

    db.one('SELECT * FROM test WHERE test.password=$(pass)', { pass: password })
      .then(function (item) {
        if (acceptsHTML) {
          //console.log(item);
          db.func('create_teacher_lecture_quiz_instance', [
            item.id,
            //room_name,
            allow_anon,
            req.session.appUserId,
          ])
            .then(function (data) {
              if (data[0].err_code === 0) {
                // console.log(data[0].create_teacher_lecture_quiz_instance);
                var lecture_data = { title: item.title };
                req.session.id_lecture = item.id; // id_lecture is test id
                req.session.id_lecture_instance = data[0].id_teacher_lecture_instance;
                // console.log(req.session);
                res.publicRender('lecture/teacherRoomView', {
                  data: lecture_data,
                });
              } else {
                // error while creating teacher lecture quiz instance
                res.redirect('/lecture/startteacher');
              }
            })
            .catch(function (error) {
              winston.error(error);
              res.redirect('/lecture/startteacher');
            });
        } else {
          res.status(400).send();
        }
      })
      .catch(function (error) {
        winston.error(error);
        res.redirect('/lecture/startteacher');
      });
  }
);

router.get(
  '/teacher/getquestion/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var ordinal = parseInt(req.params.ordinal); // TODO: handle it?
    lectureService
      .getQuestion(ordinal, req.session.id_lecture, true, false) //show correct = true to get correct answers
      .then(function (q) {
        // console.log(q);
        res.json(q);
      })
      .catch(function (q) {
        res.json(q);
      });
  }
);

router.get(
  '/teacher/gettest',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.one(
      `SELECT title,
                       questions_no,
                       password,
                       room_name
                 FROM test JOIN teacher_lecture_quiz_instance ON
                    (test.id = teacher_lecture_quiz_instance.id_test AND teacher_lecture_quiz_instance.id = $(id_lecture_instance))
                 WHERE test.id = $(id_lecture)`,
      {
        id_lecture: req.session.id_lecture,
        id_lecture_instance: req.session.id_lecture_instance,
      }
    )
      .then(function (test) {
        res.json({
          title: test.title,
          noOfQuestions: test.questions_no,
          password: test.password,
          roomName: test.room_name,
        });
      })
      .catch(function (error) {
        winston.error(
          new errors.DbError('An error occured while fetching lecture test: ' + error.message)
        );
        res.json({
          success: false,
          error:
            process.env.NODE_ENV !== 'development'
              ? 'Logged at the server'
              : error.message || error,
        });
      });
  }
);

router.post(
  '/endtest',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var room_name = req.body.room;

    lectureService
      .updateTeacherLectureQuizInstance(room_name)
      .then(function (result) {
        res.json(result);
      })
      .catch(function (result) {
        console.log('Error while updating teacher lecture quiz instance!');
      });
  }
);

router.post(
  '/getresults',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var room_name = req.body.room;

    db.many(
      `SELECT score
                FROM test_instance
                JOIN test_instance_room ON test_instance.id = test_instance_room.id_test_instance
                WHERE test_instance.id_test = $(id_lecture) AND room_name = $(room_name)`,
      {
        id_lecture: req.session.id_lecture,
        room_name: room_name,
      }
    )
      .then(function (score) {
        res.json(score);
      })
      .catch(function (error) {
        console.log("Error while fetching students' scores! " + error);
      });
  }
);

router.post(
  '/getstudanswers',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    var qordinal = req.body.qordinal;
    var room_name = req.body.room;
    var noOfAns = req.body.noOfAns;
    try {
      let students = await db.many(
        `SELECT alt_id, test_instance.id
                FROM student
                JOIN test_instance ON student.id = test_instance.id_student
                JOIN test_instance_room ON test_instance.id = test_instance_room.id_test_instance
                AND test_instance_room.room_name = $(room_name)`,
        {
          room_name: room_name,
        }
      );

      var promises = [];
      var textAnswers = [];

      var answersStat = [];
      for (var k = 0; k < noOfAns; k++) {
        answersStat.push(0);
      }
      for (var i = 0; i < students.length; i++) {
        promises.push(OngoingTests.getStudentAnswers(students[i].alt_id, students[i].id));
      }

      let answers = await Promise.all(promises);
      if (noOfAns > 0) {
        // console.log('answers', JSON.stringify(answers));
        // make an array containing the number of occurrences for every possible answer for given question ordinal
        for (var i = 0; i < answers[0].length; i++) {
          for (var j = 0; j < answers[0][i][qordinal - 1]; j++) {
            answersStat[answers[0][i][qordinal - 1][j] - 1]++;
          }
        }
        res.json(answersStat);
      } else {
        for (var i = 0; i < answers[0].length; i++) {
          textAnswers.push(answers[0][i][qordinal - 1][0]);
        }
        res.json(textAnswers);
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    }
  }
);

// logging route, implemented here to enable logging for anonymous users
router.post('/log', function (req, res, next) {
  var entry = req.body.e;
  var promises = [];
  let usernameToLog =
    req.session.impersonated_username ||
    (req.session.passport && req.session.passport.user) ||
    req.session.anon_alt_id;
  // save empty answers in the beginning
  // if (entry.eventName === 'Lecture started') {
  //   let ca = req.session.currAnswers || {};
  //   ca[req.session.id_test_instance] = JSON.parse(entry.eventData);
  //   req.session.currAnswers = ca; //JSON.parse(JSON.stringify(ca));
  //   promises.push(OngoingTests.updateCurrAnswer(req.session, usernameToLog)); // TODO: check if this is necessary
  // }

  // if (entry.eventName === 'Answer updated') {
  //   // stashing it also into the session object so that I can recover more easily
  //   let ca = req.session.currAnswers || {};
  //   ca[req.session.id_test_instance] = JSON.parse(entry.eventData);
  //   req.session.currAnswers = ca; //JSON.parse(JSON.stringify(ca));
  //   promises.push(OngoingTests.updateCurrAnswer(req.session, usernameToLog)); // TODO: check if this is necessary
  // }
  promises.push(TestLog.logEvent(req.session, entry));
  res.json({
    done: true,
  });
  Promise.all(promises).then(function (args) {
    winston.debug('OngoingTest and TestLog done');
  });
});

module.exports = router;
