'use strict';
var express = require('express');
var router = express.Router();
var testService = require.main.require('./services/testService');
var winston = require('winston');
var db = require('../db').db;
const utils = require.main.require('./common/utils');
router.get('/stalk/:courseAbbreviation/:testAbbreviation', async function (req, res) {
  try {
    let idTest = await db.oneOrNone(
      `SELECT test.id, id_academic_year
                FROM course
                JOIN test
                  ON test.id_course = course.id
               WHERE lower(course_acronym) = $(course_acronym)
                 AND lower(title_abbrev) = $(title_abbrev)
                 AND allow_anonymous_stalk
                ORDER BY id_academic_year desc
                LIMIT 1 `,
      {
        course_acronym: req.params.courseAbbreviation.toLowerCase(),
        title_abbrev: req.params.testAbbreviation.toLowerCase(),
      }
    );
    if (idTest && idTest.id && idTest.id > 0) {
      let stalkData = await testService.getStalkData(idTest.id, idTest.id_academic_year);
      let tabledata;
      if (req.query.view !== 'cards') {
        tabledata = {
          students: {
            headers: [
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return utils.getTinyImageHtml(row.alt_id2);
                },
              },
              {
                full_name: 'Name',
              },
              {
                ip_address: 'IP(s)',
              },
              {
                ts_started: 'Started',
              },
              {
                duration: 'Duration',
              },
              {
                room: 'Room',
              },
              {
                hbCount: 'HB count',
                __raw: true,
                __formatter: function (row) {
                  return `<strong><i class="fa fa-heart" style="color:${
                    row.hbColor
                  }"></i></strong>&nbsp;${row.hbCount ? row.hbCount : ''}`;
                },
              },
              {
                lastHbAgo: 'HB ago',
              },
              {
                progress: 'Progress',
              },
              {
                lfs: 'LF count',
              },
              {
                lfsTotal: 'LF total',
              },
              {
                lfsMax: 'LF max',
              },
              {
                lfsOn: 'LF since',
              },
            ],
            rows: stalkData.rs,
          },
        };
      }
      res.publicRender('public/stalkExam', {
        pageTitle: `Ongoing exams for ` + idTest.id,
        view: req.query.view || 'table',
        ...stalkData,
        ...tabledata,
        id_test: idTest.id,
      });
      // res.publicRender('public/stalkExam', stalkData);
    } else {
      res.status(404).send('Move along, nothing to see...');
    }
  } catch (error) {
    winston.error(error);
  }
});

var fs = require('fs');
var unzipper = require('unzipper');
var globals = require('../common/globals');

const unzipPromise = (from, to) => {
  return new Promise((resolve, reject) => {
    fs.createReadStream(from)
      .pipe(
        unzipper.Extract({
          path: to,
        })
      )
      .on('finish', function () {
        resolve();
      })
      .on('error', function (e) {
        reject(e);
      });
  });
};
router.get('/fromzip/:filename', async function (req, res) {
  if (utils.useMinio()) {
    try {
      let zipname = req.query.zip;
      let path = zipname.split('/');
      let bucketName = path.shift();
      path = path.join('/');
      console.log(bucketName, path);
      minioService.streamFileFromZipObject(
        bucketName, //globals.PUBLIC_TI_DOWNLOAD_FOLDER,
        path,
        req.params.filename,
        res
      );
    } catch (error) {
      winston.error('Error preparing download.');
      winston.error(error);
    }
  } else {
    try {
      let zipname = req.query.zip;
      let path = zipname.split('/');
      zipname = path.pop();
      const ziphash = zipname.replace('.zip', '');
      const subfolder = path.join('/');
      const filename = req.params.filename;
      let baseFolder = `${global.appRoot}/${globals.PUBLIC_FOLDER}/${subfolder}`.replace(
        /\/\/+/g,
        '/'
      );
      let folder = `${baseFolder}/mgrading-${ziphash}`.replace(/\/\/+/g, '/');

      if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
      }
      if (!fs.existsSync(`${folder}/${filename}`)) {
        //console.log("unzipping ", `${folder}/${zipname}`);
        await unzipPromise(`${baseFolder}/${zipname}`, folder);
      }
      if (!fs.existsSync(`${folder}/${filename}`)) {
        res.status(404);
      } else {
        let redirectUrl =
          req.protocol +
          '://' +
          req.get('host') +
          `${req.baseUrl}/${subfolder}//mgrading-${ziphash}/${filename}`;
        //console.log("redirecting to ", redirectUrl);
        res.redirect(redirectUrl);
      }
    } catch (error) {
      winston.error('Error preparing download.');
      winston.error(error);
    }
  }
});

module.exports = router;
