const utils = require.main.require('./common/utils');

module.exports = utils.useMinio()
  ? require('./uploadRoutesMinio.js')
  : require('./uploadRoutesFileSystem.js');
