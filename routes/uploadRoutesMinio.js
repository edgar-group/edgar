'use strict';

var express = require('express');
var router = express.Router();
var multer = require('multer');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var utils = require.main.require('./common/utils');
var db = require('../db').db;
var sanitize = require('sanitize-filename');
const minioService = require('../services/minioService');
const multiUploadImages = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  }, // 5MB
}).array('images', 5);
const minio = require('minio');
const config = require.main.require('./config/config');
const minioClient = new minio.Client(config.minioConfig);

// TUTORIALS_IMG_FOLDER

const uploadFunction = (id, IMG_FOLDER, IMG_FOLDER_PLACEHOLDER, req, res) => {
  multiUploadImages(req, res, async function (err) {
    if (err) {
      res
        .status(500)
        .send({
          error: {
            message: `Upload failed: ${err.message}`,
          },
        })
        .end();
      winston.error(err);
      return;
    }
    let images = [];
    for (let i = 0; i < req.files.length; i++) {
      let file = req.files[i];
      var metaData = {
        mimetype: file.mimetype,
        originalFilename: sanitize(file.originalname),
        userCreated: req.session.passport.user,
      };
      let newfilename = getHashedImageName(file.originalname, id);
      let path = `${IMG_FOLDER}/${id}/${newfilename}`;
      let arrPath = path.split('/');
      if (arrPath[0] === '') arrPath.shift();
      let minioBucket = arrPath[0];
      arrPath.shift();
      let minioFilename = arrPath.join('/');
      try {
        await minioService.putObject(minioBucket, minioFilename, file.buffer, metaData);
        images.push(
          `![Image\\${file.originalname}-${id}](${IMG_FOLDER_PLACEHOLDER}/${id}/${newfilename})`
        );
      } catch (error) {
        winston.error(error);
        images.push(`Error uploading to minio: ${error.message}`);
      }
    }
    res.json(images);
  });
};

router.post(
  '/tutorial/:id_tutorial([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  (req, res) => {
    uploadFunction(
      req.params.id_tutorial,
      globals.TUTORIALS_IMG_FOLDER,
      globals.TUTORIALS_IMG_FOLDER_PLACEHOLDER,
      req,
      res
    );
  }
);
router.post(
  '/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  (req, res) => {
    uploadFunction(
      req.params.id_question,
      globals.IMG_FOLDER,
      globals.IMG_FOLDER_PLACEHOLDER,
      req,
      res
    );
  }
);

/************* Question attachemnts *************** */

let questionAttachmentsBucketName = globals.QUESTIONS_ATTACHMENTS_FOLDER.replaceAll('_', '');

router.get(
  '/attachment/:filename(*)',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    return res.redirect(`/${globals.QUESTIONS_ATTACHMENTS_ROUTE}/${req.params.filename}`);
  }
);

router.delete(
  '/attachment/:filename(*)',
  middleware.requireRole(globals.ROLES.TEACHER),
  async (req, res) => {
    try {
      let result = await db.result(
        `
            DELETE FROM question_attachment
            WHERE filename = $(filename)
        `,
        {
          filename: req.params.filename,
        }
      );

      if (!result || result.rowCount !== 1) {
        return res.sendStatus(404);
      }

      minioClient.removeObject(questionAttachmentsBucketName, req.params.filename, function (e) {
        if (e) {
          winston.error(e);
          return res.sendStatus(500);
        }
        winston.info('Deleted ' + questionAttachmentsBucketName + '/' + req.params.filename);
        return res.sendStatus(200);
      });
    } catch (err) {
      winston.error(err);
      res.sendStatus(500);
    }
  }
);

var uploadAttachments = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 50 * 1024 * 1024,
  }, // 50MB
}).single('attachment');

router.post('/attachment', middleware.requireRole(globals.ROLES.TEACHER), (req, res) => {
  uploadAttachments(req, res, async function (err) {
    if (err) {
      res
        .status(500)
        .send({
          error: {
            message: `Upload failed: ${err.message}`,
          },
        })
        .end();
      winston.error(err);
      return;
    }
    let file = req.file;
    let idQuestion = req.body.id_question;
    let originalName = req.file.originalname; // what user uploaded
    let filename = utils.getRandomHash();
    let label = req.body.label; // input label
    let isPublic = req.body.is_public;
    var metaData = {
      mimetype: file.mimetype,
      originalFilename: sanitize(file.originalname),
      idQuestion,
      label: sanitize(label),
      isPublic,
    };
    try {
      await minioService.putObject(questionAttachmentsBucketName, filename, file.buffer, metaData);
      await db.none(
        `
                    INSERT INTO question_attachment(id_question, original_name, filename, label, is_public)
                    VALUES ($1, $2, $3, $4, $5)
                `,
        [idQuestion, originalName, filename, label || '', isPublic]
      );

      res.json({
        filename,
        label,
        isPublic,
      });
    } catch (error) {
      winston.error(error);
      res
        .status(500)
        .send({
          error: {
            message: `Upload failed: ${error.message}`,
          },
        })
        .end();
    }
  });
});

/************* Students' profile pics *************** */

var uploadFaces = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
}).array('images', 200);

const getHashedImageName = (originalname, prefix) => {
  let ext = originalname.substring(originalname.length - 4, originalname.length);
  return utils.getHashedImageName((prefix ? prefix + ':' : '') + originalname, ext);
};

router.post('/faces', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  uploadFaces(req, res, async function (err) {
    if (err) {
      res
        .status(500)
        .send({
          error: {
            message: `Upload failed: ${err.message}`,
          },
        })
        .end();
      winston.error(err);
      return;
    }

    let images = [];
    for (let i = 0; i < req.files.length; i++) {
      let file = req.files[i];
      var metaData = {
        mimetype: file.mimetype,
        originalFilename: sanitize(file.originalname),
        userCreated: req.session.passport.user,
      };
      let newFilename = getHashedImageName(file.originalname);
      let path = `${globals.IMG_FACES_FOLDER}/${newFilename}`;
      let arrPath = path.split('/');
      if (arrPath[0] === '') arrPath.shift();
      let minioBucket = arrPath[0];
      arrPath.shift();
      let minioFilename = arrPath.join('/');
      try {
        await minioService.putObject(minioBucket, minioFilename, file.buffer, metaData);
        images.push(path);
      } catch (error) {
        winston.error(error);
        images.push(`Error uploading to minio: ${error.message}`);
      }
    }
    winston.info(`${req.session.passport.user} uploaded  ${req.files.length} profile pics.`);
    res.appRender('~/admin/uploadFacesView', {
      hrefs: images,
    });
  });
});

const FILE_SIZE_LIMIT = 50 * 1024 * 1024; // 50MB;
var uploadTestInstancePrivate = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: FILE_SIZE_LIMIT, // 50MB
  },
}).single('testInstanceFile');

module.exports = router;
