var express = require('express');
var authRouter = express.Router();
var passport = require('passport');
var winston = require('winston');
var config = require.main.require('./config/config');
var sessionInit = require.main.require('./config/strategies/sessionInit');
const moment = require('moment');
const db = require('../db').db;
var middleware = require('../common/middleware');
var globals = require('../common/globals');

var router = function () {
  // let isStrategySetup = false;

  let tweakPassportStrategy = function () {
    return function (req, res, next) {
      if (config.passportStrategyName === 'saml') {
        //console.log("Tweaking saml.aai.strategy passport strategy issuer on POST login.");
        let map = config.samlAAIAuth && config.samlAAIAuth.proxyIssuerMap;
        if (map) {
          try {
            //console.log('before tweak', passport._strategies["saml"]._saml.options.issuer);
            //console.log('proxy is ', req.connection.remoteAddress);
            let issuer = map[req.connection.remoteAddress];
            winston.debug('mapped issuer is ', issuer);
            if (issuer) passport._strategies['saml']._saml.options.issuer = issuer;
            //console.log('after tweak', passport._strategies["saml"]._saml.options.issuer);
          } catch (error) {
            winston.error('Failed tweaking strategy', error);
          }
        }

        // isStrategySetup = true;
      }
      next();
    };
  };

  authRouter.route('/login').get(function (req, res) {
    // winston.info('login route, render view', 'req.session = ' + req.session);
    res.publicRender('loginView', {
      authMessage: req.flash('auth'),
      providers: config.providers,
    });
  });

  // Nije dobro, lbarisic hardkodirao 'local'
  // authRouter.route('/login')
  //     .post(function(req, res, next) {
  //         return passport.authenticate('local', {
  //               successRedirect: req.session.landingUrl || '/',
  //             failureRedirect: '/auth/login'
  //           })(req, res, next);
  //     });

  authRouter.route('/login').post(function (req, res, next) {
    return passport.authenticate(config.passportStrategyName, {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/aailogin').get(tweakPassportStrategy(), function (req, res, next) {
    return passport.authenticate('saml', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/google').get(function (req, res, next) {
    winston.info('get /auth/google');
    return passport.authenticate('google', {
      scope: ['profile'],
      prompt: 'select_account',
    })(req, res, next);
  });

  authRouter.route('/google/callback').get(function (req, res, next) {
    winston.info('get /auth/google/callback');
    return passport.authenticate('google', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/facebook').get(function (req, res, next) {
    winston.info('get /auth/facebook');
    return passport.authenticate('facebook', {
      prompt: 'select_account',
    })(req, res, next);
  });

  authRouter.route('/facebook/callback').get(function (req, res, next) {
    winston.info('get /auth/facebook/callback');
    return passport.authenticate('facebook', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/twitter').get(function (req, res, next) {
    winston.info('get /auth/twitter');
    return passport.authenticate('twitter')(req, res, next);
  });

  authRouter.route('/twitter/callback').get(function (req, res, next) {
    winston.info('get /auth/twitter/callback');
    return passport.authenticate('twitter', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/locallogin').post(function (req, res, next) {
    if (req.accepts('html')) {
      return passport.authenticate('local', {
        successRedirect: req.session.landingUrl || '/',
        failureRedirect: '/auth/login',
      })(req, res, next);
    } else if (req.accepts('json')) {
      return passport.authenticate('local', function (error, user, info) {
        if (error) {
          res.status(500).json({ error: error });
        } else if (info) {
          res.status(400).json({ error: info });
        } else if (user) {
          winston.debug('Successfull login for ' + user.username);
          winston.debug(req.url);
          req.login(user, function (err) {
            if (err) {
              return next(err);
            }
            res.status(201).json({ user: user });
          });
        } else {
          res.status(400).send();
        }
      })(req, res, next);
    }
    return passport.authenticate('local', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });

  authRouter.route('/simplelogin').post(function (req, res, next) {
    if (req.accepts('html')) {
      req.body.username = req.body.username.trim();
      return passport.authenticate('simple', {
        successRedirect: req.session.landingUrl || '/',
        failureRedirect: '/auth/login',
      })(req, res, next);
    } else if (req.accepts('json')) {
      return passport.authenticate('simple', function (error, user, info) {
        if (error) {
          res.status(500).json({ error: error });
        } else if (info) {
          res.status(400).json({ error: info });
        } else if (user) {
          winston.debug('Successfull login for ' + user.username);
          winston.debug(req.url);
          req.login(user, function (err) {
            if (err) {
              return next(err);
            }
            res.status(201).json({ user: user });
          });
        } else {
          res.status(400).send();
        }
      })(req, res, next);
    }
    return passport.authenticate('simple', {
      successRedirect: req.session.landingUrl || '/',
      failureRedirect: '/auth/login',
    })(req, res, next);
  });
  authRouter.route('/ngdevlogin').post(function (req, res, next) {
    console.log('ndevlgin', req.body);
    //req.body.username = 'igor@fer.hr';
    req.body.username = 'nk51530@fer.hr';
    //req.body.username = '"_mfertalj.fer.hr"';
    req.body.password = 'sdfsdfsdhrsdfsdfsd@node.js';
    return passport.authenticate('simple', {
      successRedirect: '/auth/ok',
      failureRedirect: '/auth/ngdevloginerr',
    })(req, res, next);
  });
  authRouter.get('/ok', function (req, res) {
    console.log(req.session.passport.user);
    res.status(200).send('');
  });
  authRouter.get('/ngdevloginerr', function (req, res) {
    console.log(req.session);
    res.status(200).send(req.session);
  });

  authRouter.route('/logout').post(function (req, res) {
    winston.info('Logout for ' + req.user);
    if (req.accepts('html')) {
      passport.webLogoutStrategy(req, res);
    } else {
      passport.mobileLogoutStrategy(req, res);
      res.status(401).json({ user: null });
    }
  });
  // SAML will redirect me here eventually:
  authRouter.route('/logout').get(function (req, res) {
    winston.debug('GET Logout for ' + req.user);
    if (req.accepts('html')) {
      res.publicRender('logoutView');
    } else {
      res.status(401).json({ user: null });
    }
  });
  authRouter.route('/profile').get(function (req, res) {
    if (!req.user) {
      if (req.accepts('html')) {
        res.redirect('/');
      } else {
        res.status(401).json({ user: null });
      }
    } else {
      res.json({ user: req.user });
    }
  });

  authRouter.route('/token-login').post((req, res, next) => {
    passport.authenticate('token', (error, user, info) => {
      if (error) {
        res.status(500).json({ error });
        return;
      } else if (info) {
        res.status(400).json({ error: info });
      } else if (user) {
        req.login(user, (err) => {
          if (err) {
            return next(err);
          }
          res.status(201).json({ user });
        });
      } else {
        res.status(400).send();
      }
    })(req, res, next);
  });

  authRouter.route('/token-register').post((req, res, next) => {
    const studentID = req.session.studentId;
    const pin = req.body.pin;

    const now = moment().toISOString();

    // Get student ID for given PIN
    db.any(
      `
        SELECT student_id
        FROM student_token
        WHERE pin = $(pin)
      `,
      { pin }
    )
      .then((response) => {
        if (response.length) {
          const studentID = response[0].student_id;

          // Find and activate token
          return db
            .any(
              `
            UPDATE student_token
            SET is_activated = TRUE
            WHERE student_id = $(studentID)
            AND ts_created = (
              SELECT max(ts_created)
              FROM student_token
              WHERE student_id = $(studentID)
            )
            AND pin = $(pin)
            AND ts_valid_until > $(now)
            AND is_activated = FALSE
            RETURNING token
          `,
              { studentID, now, pin }
            )
            .then((response) => {
              if (response.length) {
                const token = response[0].token;
                res.status(200).json({ token });
              } else {
                res.status(404).json({
                  error: 'Unknown PIN number',
                });
              }
            });
        } else {
          res.status(404).json({
            error: 'Unknown PIN number',
          });
        }
      })
      .catch((error) => {
        winston.error(
          new errors.DbError(
            'An error occured while generating fetching token for pin: ' + error.message
          )
        );
        res.status(500).send();
      });
  });

  authRouter.get(
    '/chooserole',
    middleware.requireRoles([globals.ROLES.TEACHER]),
    function (req, res) {
      res.publicRender('chooseRoleView', {
        authMessage: req.flash('auth'),
        providers: config.providers,
      });
    }
  );
  authRouter.post(
    '/chooserole',
    middleware.requireRoles([globals.ROLES.TEACHER]),
    function (req, res, next) {
      // authRouter.route('/chooserole')
      //     .post(function(req, res, next) {
      let role = req.body.role;
      let roleChosen = () => {
        winston.debug('Role chosen: ' + role);
        delete req.session.mustChooseRole;
        res.redirect(req.session.landingUrl || '/');
      };
      // A multi-role user is by default logged in as a teacher
      if (role.toLocaleLowerCase() === 'student') {
        sessionInit.impersonate(req, req.session.passport.user, function (err, flashMsg) {
          if (err === null && flashMsg === undefined) {
            roleChosen();
          } else if (err !== null) {
            return next(err);
          } else {
            req.flash('info', flashMsg);
            res.redirect('/auth/chooserole');
          }
        });
      } else {
        roleChosen();
      }
    }
  );

  return authRouter;
};

module.exports = router;
