'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const db = require('../db').db;
const winston = require('winston');
const tutorialService = require('../services/tutorialService2');
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');
var utils = require('../common/utils');
const { max } = require('moment');

// helper functions:

const getCurrTutorial = (req) => {
  // console.log(`idti: ${req.query.idti}  |  req.session.id_test_instance: ${req.session.id_test_instance}  |  All running instances: `, req.session.running_instances);
  let fallback = {
    currTutorialId: req.session.currTutorialId,
  };
  return req.query.idtut && req.session && req.session.running_tutorials
    ? req.session.running_tutorials.find((item) => {
        return item.currTutorialId == req.query.idtut;
      }) || fallback
    : fallback;
};
const getCurrTutorialId = (req) => {
  let rv = getCurrTutorial(req);
  rv = rv && rv.currTutorialId;
  return rv;
};

let getStepCodeRunner = async function (idTutorial, stepOrdinal, codeOrdinal) {
  const question = await db.one(
    `SELECT text
            FROM tutorial_step
            WHERE tutorial_step.id_tutorial = $(idTutorial)
                AND tutorial_step.ordinal = $(stepOrdinal)`,
    {
      idTutorial,
      stepOrdinal,
    }
  );
  // /(<!--\s*(code-playground|question|multichoice|code-question).*-->)((?!(<!-- \/code-playground|<!-- \/question|<!-- \/multichoice|<!-- \/code-question))[.\s\S])*<!--\s*\/(code-playground|question|multichoice|code-question)\s*-->/gi
  const regex =
    /<!--[ a-zA-Z]*(code-playground|question|multichoice|code-question)[ a-zA-Z=0-9{}(),]*-->/gs;
  const arrCodeRunnerLines = question.text.match(regex);

  if (arrCodeRunnerLines && arrCodeRunnerLines.length >= codeOrdinal) {
    // extract params from code-playground definition
    let line = arrCodeRunnerLines[codeOrdinal - 1]
      .replace('<!--', '')
      .replace('-->', '')
      .replace('code-playground', '')
      .replace('required', '');
    const re = /(\w+(?:\(\d+\))?)\s*=\s*(.*?)(?=(!|$|\w+(\(\d+\))?\s*=))/gs;

    // these can be crid = 123  or qid = 123
    let rv = {};
    for (let kv of line.match(re)) {
      const arrKv = kv.split('=');
      const key = arrKv[0].trim();
      const value = arrKv[1].trim();
      // const value = parseInt(arrKv[1].trim()) || arrKv[1].trim();
      rv[key] = value;
    }
    winston.debug('Params:' + JSON.stringify(rv));
    return rv;
  } else {
    throw new Error(
      `Tutorial ${idTutorial}, step ${stepOrdinal} does NOT have code-playground ordinal ${codeOrdinal}`
    );
  }
};

router.get('/', async (req, res) => {
  const data = await tutorialService.listTutorials(
    req.session.currCourseId,
    req.session.studentId,
    req.session.rolename === globals.ROLES.STUDENT
  );

  res.appRender('listTutorialsView', {
    tutorials: {
      headers: [
        {
          tutorial_title: 'Title',
        },
        {
          tutorial_desc: 'Description',
        },
        {
          courses: 'Course(s)',
        },
        {
          no_steps: 'Steps',
        },
        {
          finished: 'Finished',
        },
        {
          ts_finished: 'Finished at',
        },
      ],
      rows: data,
      buttons: [
        {
          label: 'Start',
          method: 'GET',
          action: (row) => `/tutorial/${row['id']}`,
        },
      ],
    },
  });
});

router.get(
  '/:id_tutorial([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let idTutorial = parseInt(req.params.id_tutorial);
    // const isOld = await db.one(
    //   `SELECT COUNT(*) as cnt
    //            FROM old_tutorials_tbd
    //           WHERE id_tutorial = $(idTutorial)`,
    //   {
    //     idTutorial,
    //   }
    // );
    // if (isOld.cnt == 1) {
    //   winston.info(`OLD tutorial id=${idTutorial} started. PLease, upgrade...`);
    //   res.redirect(`/tutorial/${idTutorial}`);
    // } else {

    // }
    const tutorial = await db.oneOrNone(
      `SELECT *
        FROM tutorial
      WHERE id = $(idTutorial)`,
      {
        idTutorial,
      }
    );
    if (tutorial) {
      req.session.currTutorialId = idTutorial;
      res.publicRender('tutorial/newTutorialView', {
        idTutorial: idTutorial,
        title: tutorial.tutorial_title,
      }); // break out of Layout - "full screen"
    } else {
      res.status(404).send('Missed it...');
    }
  }
);
router.get(
  '/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let idTutorial = parseInt(req.params.id_tutorial);

    const tutorial = await db.oneOrNone(
      `SELECT *
        FROM tutorial
      WHERE id = $(idTutorial)`,
      {
        idTutorial,
      }
    );
    if (tutorial) {
      req.session.currTutorialId = idTutorial;
      res.publicRender('tutorial/newTutorialView', {
        idTutorial: idTutorial,
        title: tutorial.tutorial_title,
      }); // break out of Layout - "full screen"
    } else {
      res.status(404).send('Missed it...');
    }
  }
);

// API routes
// ovu cu spojiti u donju:
// router.post('/api/start/:id_tutorial([0-9]{1,10})',
//     middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//     async(req, res, next) => {
//         try {
//             const status = await tutorialService.start(
//                 req.session.currCourseId,
//                 req.session.studentId,
//                 parseInt(req.params.id_tutorial)
//             );
//             req.session.currTutorialId = parseInt(req.params.id_tutorial);
//             winston.debug(`Student ${req.session.studentId} starts/continues tutorial at step ${status.latestStepOrdinal}`);
//             res.json(status);
//         } catch (error) {
//             req.flash('error', error);
//             res.status(500);
//         }
//     });

router.get(
  '/api/:id_tutorial([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res) => {
    const status = await tutorialService.start(
      req.session.currCourseId,
      req.session.studentId,
      parseInt(req.params.id_tutorial)
    );
    req.session.currTutorialId = parseInt(req.params.id_tutorial);
    winston.debug(
      `Student ${req.session.studentId} starts/continues tutorial at step ${status.latestStepOrdinal}`
    );
    delete status.events;

    const tutorial = await tutorialService.getTutorial(req.params.id_tutorial);
    req.session.currTutorialId = parseInt(req.params.id_tutorial);

    if (!req.session.running_tutorials) {
      req.session.running_tutorials = [];
    }
    let newArray = req.session.running_tutorials.filter(function (item) {
      return item.currTutorialId != req.session.currTutorialId;
    });
    newArray.push({
      currTutorialId: req.session.currTutorialId,
      started: new Date(),
    });
    winston.info(
      req.session.passport.user +
        ': SETTING req.session.running_tutorials to ' +
        JSON.stringify(newArray)
    );
    req.session.running_tutorials = newArray;

    res.json({
      ...tutorial,
      allowRandomAccess: tutorial.allow_random_access,
      currAnswers: status.currentAnswers,
      latestStepOrdinal: status.latestStepOrdinal,
      cmSkin: req.session.cmSkin,
    });
  }
);

router.get(
  '/question/:stepOrdinal([0-9]{1,2})/:questionOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    const idTutorial = parseInt(req.session.currTutorialId);
    const stepOrdinal = parseInt(req.params.stepOrdinal);
    const questionOrdinal = parseInt(req.params.questionOrdinal);
    // console.log(idTutorial, stepOrdinal, questionOrdinal);
    try {
      let params = await getStepCodeRunner(idTutorial, stepOrdinal, questionOrdinal);
      let q = await tutorialService.getTutorialQuestion(
        parseInt(params.qid),
        req.session.currCourseId
      );
      res.json(q);
    } catch (err) {
      winston.error(err);
      res.json(err);
    }
  }
);
router.get(
  '/coderunner/:stepOrdinal([0-9]{1,2})/:codeOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    // console.log("code runner route was called by ")
    const idTutorial = parseInt(req.session.currTutorialId);
    const stepOrdinal = parseInt(req.params.stepOrdinal);
    const codeOrdinal = parseInt(req.params.codeOrdinal);
    // console.log(idTutorial, stepOrdinal, codeOrdinal);
    try {
      let params = await getStepCodeRunner(idTutorial, stepOrdinal, codeOrdinal);
      // console.log("PARAMS ", params)
      let q = await tutorialService.getTutorialCodeRunner(
        parseInt(req.session.currCourseId),
        params.crid,
        params.langids
      );
      res.json(q);
    } catch (err) {
      winston.error(err);
      res.json(err);
    }
  }
);

router.get(
  '/api/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res) => {
    try {
      const step = await tutorialService.getTutorialStep(
        parseInt(req.params.id_tutorial),
        parseInt(req.params.step_ordinal),
        parseInt(req.session.currCourseId),
        parseInt(req.session.studentId)
      );
      res.json(step);
      await tutorialService.updateTutorialStep(
        parseInt(req.session.currCourseId),
        parseInt(req.params.id_tutorial),
        parseInt(req.params.step_ordinal),
        parseInt(req.session.studentId)
      );
    } catch (err) {
      winston.error('Error at fetching tutorial step ' + JSON.stringify(err));
      res.status(500);
    }
  }
);

router.get(
  '/api/:id_tutorial([0-9]{1,10})/answers',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res) => {
    try {
      const answers = await tutorialService.getStudentAnswers(
        req.session.currCourseId,
        req.session.studentId,
        parseInt(req.params.id_tutorial)
      );
      res.json({
        currAnswers: answers,
      });
    } catch (err) {
      winston.error('failed to get current student answers for tutorial ' + JSON.stringify(err));
      res.status(500);
    }
  }
);

router.get(
  '/api/start/:id_tutorial([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      const status = await tutorialService.start(
        req.session.currCourseId,
        req.session.studentId,
        req.params.id_tutorial
      );
      req.session.currTutorialId = req.params.id_tutorial;
      winston.debug(
        `Student ${req.session.studentId} starts/continues tutorial at step ${status.latestStepOrdinal}`
      );
      res.json(status);
    } catch (error) {
      req.flash('error', error);
      res.status(500);
    }
  }
);

router.put(
  '/api/:id_tutorial([0-9]{1,10})/answers',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res) => {
    try {
      // console.log("/api/:id_tutorial([0-9]{1,10})/answers", req.body.answers);
      // console.log("/api/:id_tutorial([0-9]{1,10})/answers", JSON.stringify(req.body.answers));
      await tutorialService.updateStudentAnswers(
        req.session.currCourseId,
        req.session.studentId,
        parseInt(req.params.id_tutorial),
        req.body.answers
      );
      res.json({
        success: true,
      });
    } catch (err) {
      winston.error('Failed to update current answers for tutorial ' + JSON.stringify(err));
      res.status(500);
    }
  }
);

router.post(
  '/api/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
  async (req, res, next) => {
    // get question id
    const questionId = parseInt(
      (
        await db.one(
          `SELECT id_question
            FROM tutorial_step
            WHERE id_tutorial = $(id_tutorial)
                AND ordinal = $(ordinal)`,
          {
            id_tutorial: req.params.id_tutorial,
            ordinal: req.params.step_ordinal,
          }
        )
      ).id_question
    );

    // check if answer correct
    try {
      // console.log("Before", req.body.answer);
      let convertedAnswer =
        req.body.answer.multichoice && req.body.answer.multichoice.length
          ? req.body.answer.multichoice
          : req.body.answer.codeAnswer.code;
      const result = await tutorialService.evaluateQuestionAnswer(
        req.session.currCourseId,
        req.session.studentId,
        parseInt(req.params.id_tutorial),
        questionId,
        convertedAnswer,
        parseInt(req.params.step_ordinal)
      );
      // console.log("After", result);
      res.json({
        correct: result.score.is_correct,
      });
    } catch (err) {
      res.json({
        success: false,
        error: {
          message: JSON.stringify(err.message),
        },
      });
    }
  }
);
const trunc = (code, max) => {
  return code && code.length > max ? code.substring(0, max) + '...(truncated and logged)...' : code;
};
router.post(
  '/run/step/:stepOrdinal([0-9]{1,2})/:codeOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    try {
      const idTutorial = parseInt(req.session.currTutorialId);
      const stepOrdinal = parseInt(req.params.stepOrdinal);
      const codeOrdinal = parseInt(req.params.codeOrdinal);
      let params = await getStepCodeRunner(idTutorial, stepOrdinal, codeOrdinal);
      req.body.code = trunc(req.body.code, 5000);
      req.body.stdin = trunc(req.body.stdin, 1000);
      if (params.qid) {
        // (courseId, studentId, tutorialId, questionId, answer, currStepOrdinal)
        const result = await tutorialService.evaluateQuestionAnswer(
          parseInt(req.session.currCourseId),
          req.session.studentId,
          idTutorial,
          params.qid,
          req.body.code,
          stepOrdinal,
          req.body.id_programming_language
        );
        //console.log(result)
        return res.json(result);
      } else if (params.crid) {
        const resp = await tutorialService.runPlaygroundCode(
          // courseId, code, stdin, idCodeRunner, id_programming_language
          parseInt(req.session.currCourseId),
          req.body.code,
          {
            id: 1,
            input: req.body.stdin || '_',
          },
          params.crid,
          req.body.id_programming_language
        );
        const langType = (
          await db.one(
            `SELECT name
              FROM programming_language
              WHERE id = $(id_programming_language)`,
            {
              id_programming_language: req.body.id_programming_language,
            }
          )
        ).name;

        if (langType.toUpperCase().includes('SQL')) {
          return res.json(resp);
        } else {
          let formattedHtmlResult = utils.judge0response2Html({
            resp,
            hideTestDetails: false,
            verboseTests: true,
            isPlayground: true,
          });
          return res.json({
            success: true,
            coderesult: formattedHtmlResult,
          });
        }
      } else {
        throw new Error(
          `Tutorial ${idTutorial}, step ${stepOrdinal}, code-playground ordinal ${codeOrdinal} unknown key: ${
            arrCodeRunnerLines[codeOrdinal - 1]
          }`
        );
      }
    } catch (err) {
      winston.error(
        `Failed to evalute question, step ${req.params.stepOrdinal}, code-playground ordinal ${req.params.codeOrdinal}, at tutorial ${req.session.currTutorialId}. ` +
          JSON.stringify(err)
      );
      winston.error(err);
      res.json({
        success: false,
        error: {
          message: `An error occured while evaluating question ${req.params.ordinal} (more info @server).`,
        },
      });
    }
  }
);

router.post(
  '/log',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res) {
    let event = req.body.e || req.body.entries[0];
    event.ip = req.ip;
    if (req.session.currTutorialId) {
      tutorialService.logEvent(
        req.session.currCourseId,
        req.session.studentId,
        parseInt(req.session.currTutorialId),
        event
      );

      res.json({
        done: true,
      });
    } else {
      res.json({
        done: true,
        message: 'Failed to log, unknown currTutorialId',
      });
    }
  }
);

module.exports = router;
