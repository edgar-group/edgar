var express = require('express');
var router = express.Router();
var AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
var winston = require('winston');

router.get('/:id(\\d+)/', async function (req, res) {
  try {
    req.session.currAcademicYearId = parseInt(req.params.id);
    await AppUserSettings.saveProps(req.user, {
      currAcademicYearId: req.session.currAcademicYearId,
    });
    winston.debug(`AppUserSettings for ${req.user} saved.`);
  } catch (error) {
    winston.error(error);
  }
  res.redirect('/');
});

module.exports = router;
