'use strict';

var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var db = require('../db').db;
var errors = require('../common/errors');
var sessionInit = require.main.require('./config/strategies/sessionInit');
var utils = require.main.require('./common/utils');
var OnlineUsers = require.main.require('./models/OnlineUsersModel.js');
var moment = require('moment');
var config = require.main.require('./config/config');

const { MongoClient } = require('mongodb');
router.get(
  '/session-data/:username',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    const client = new MongoClient(config.mongooseConnectionString);
    let result;
    try {
      await client.connect();
      const database = client.db('session_store');
      const collection = database.collection('sessions');

      const query = { session: { $regex: req.params.username } };
      result = await collection.find(query).toArray();
      result = result.map((x) => {
        return {
          ...x,
          session: JSON.parse(x.session),
        };
      });
    } catch (error) {
      console.error('Error:', error);
      result = error;
    } finally {
      await client.close();
      res.appRender('sessionDataView', {
        sessionData: result,
      });
    }
  }
);

router.get(
  '/onlineusers',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    OnlineUsers.getOnlineUsers()
      .then(function (data) {
        data.forEach(function (row) {
          row.img = `<div class="profilepictiny"><img width="35" height="35" src="${row.imgSrc}"></div>`;
        });
        res.appRender('onlineUsersView', {
          users: {
            headers: [
              {
                img: 'Image',
                __raw: true,
              },
              {
                username: 'Username',
              },
              {
                fullname: 'Full name',
              },
              {
                rolename: 'Role',
              },
              {
                currAcademicYear: 'Acdmc year',
              },
              {
                currCourse: 'Course',
              },
              {
                lastRoute: 'Last route',
              },
              {
                ts_modified: 'TS modified',
                __formatter: function (row) {
                  return moment(row.ts_modified).format('MMMM Do YYYY, h:mm:ss a');
                },
              },
              {
                _id: 'Minutes ago',
                __formatter: function (row) {
                  let now = moment(new Date());
                  let duration = moment.duration(now.diff(row.ts_modified));
                  let mins = duration.asMinutes();
                  return `${Math.round(mins)} minutes ago`;
                },
              },
            ],
            rows: data,
            links: [
              {
                label: 'Session',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return `/admin/session-data/${row['username']}`;
                },
                __formatter: function (row) {
                  return `<i class="fa fa-2 fa-eye"></i>`;
                },
              },
              {
                label: 'Accs.log',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return `/admin/access-logs/${row['username']}`;
                },
                __formatter: function (row) {
                  return `<i class="fa fa-2 fa-eye"></i>`;
                },
              },
            ],
          },
        });
      })
      .catch(function (error) {
        winston.error('Error fetching online users list. ' + error);
        return next(error);
      });
  }
);

router.get(
  '/student/impersonate/:id_student([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var IdStudent = parseInt(req.params.id_student);
    db.oneOrNone(
      ` select student.alt_id as username
                          FROM student
                         WHERE id = $(id_student)
                      `,
      {
        id_student: IdStudent,
      }
    )
      .then(function (stud) {
        if (stud) {
          sessionInit.impersonate(req, stud.username, function (err, flashMsg) {
            if (err === null && flashMsg === undefined) {
              req.session.impersonated_username = stud.username; // to correclty restore questions
              res.redirect('/');
            } else if (err !== null) {
              return next(err);
            } else {
              req.flash('info', flashMsg);
              res.redirect('/admin/student/list');
            }
          });
        } else {
          res.status(404).send('Missed it. Wanna try not guessing?');
        }
      })
      .catch(function (error) {
        winston.error(new errors.DbError('An error in student/impersonate: ' + error.message));
        return next(new errors.DbError('An error in student/impersonate: ' + error.message));
      });
  }
);

router.get('/student/faces', middleware.requireRole(globals.ROLES.TEACHER), function (req, res) {
  res.appRender('uploadFacesView', {});
});

router.get('/student/upload', middleware.requireRole(globals.ROLES.TEACHER), function (req, res) {
  res.appRender('uploadStudentsView', {}, 'uploadStudentsViewScript');
});

router.post(
  '/student/parse',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    var msg = '';
    var list = req.body.students.split('\n');
    var parsedlist = [];
    list.forEach(function (val, i) {
      //val = val.trim();
      if (!(val.trim() === '' || val.trim().startsWith('#'))) {
        var items = val.split('\t', -1);
        if (items.length !== 7) {
          msg += '<br>Row ignored, could not parse 7 columns: ' + JSON.stringify(items);
        } else if (!items[0] || !items[2] || !items[3]) {
          msg += 'Row ignored, obligatory column missing: ' + val;
        } else {
          parsedlist.push({
            alt_id: items[0].trim(),
            alt_id2: items[1],
            first_name: items[2].trim(),
            last_name: items[3].trim(),
            email: items[4],
            group_name: items[5],
            provider: items[6] ? items[6] : 'saml.aai',
          });
        }
      }
    });
    res.appRender(
      'uploadStudentsView',
      {
        usertxt: req.body.students,
        parsedlist: parsedlist,
        parsemsg: msg,
      },
      'uploadStudentsViewScript'
    );
  }
);

router.post('/student/commit', middleware.requireRole(globals.ROLES.TEACHER), function (req, res) {
  if (req.body.op === 'insert') {
    db.tx((t) => {
      var inserts = [];
      req.body.rows.forEach(function (row) {
        inserts.push(
          t.none(
            `insert into student(alt_id, alt_id2, first_name, last_name, email, provider)
                            SELECT $1, $2, $3, $4, $5, $6
                              WHERE NOT EXISTS (SELECT id FROM student WHERE alt_id = $1 OR alt_id2 = $2)`,
            [row.alt_id, row.alt_id2, row.first_name, row.last_name, row.email, row.provider]
          )
        );
        inserts.push(
          t.none(
            `insert into student_course(id_academic_year, id_course, id_student, class_group)
                            SELECT $1, $2, student.id, $3
                              FROM student
                             WHERE alt_id = $4 OR alt_id2 = $5
                               AND NOT EXISTS
                                    (SELECT * FROM student_course JOIN student s
                                               ON student_course.id_student = s.id
                                              AND s.alt_id = $4
                                              AND student_course.id_course = $2
                                              AND student_course.id_academic_year = $1)`,
            [
              req.session.currAcademicYearId,
              req.session.currCourseId,
              row.group_name,
              row.alt_id,
              row.alt_id2,
            ]
          )
        );
      });
      return t.batch(inserts);
    })
      .then((data) => {
        req.flash('info', data.length + ' rows affected.');
        res.redirect('/admin/student/list');
        // success;
      })
      .catch((error) => {
        req.flash('error', JSON.stringify(error));
        res.appRender('uploadStudentsView', {}, 'uploadStudentsViewScript');
      });
  } else {
    req.flash('error', 'Unsupported operation.');
    res.appRender('uploadStudentsView', {}, 'uploadStudentsViewScript');
  }
});

router.get(
  '/student/list',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.any(
      `SELECT student.*, class_group, provider, student.ts_created::timestamp(0)::varchar, student.ts_modified::timestamp(0)::varchar
                  FROM student_course
                  JOIN student ON student_course.id_Student = student.id
                  WHERE id_course = $(cId)
                    AND id_academic_year = $(yId)
          ORDER BY last_name, first_name`,
      {
        cId: req.session.currCourseId,
        yId: req.session.currAcademicYearId,
      }
    )
      .then((data) => {
        data.forEach(function (row) {
          row.img = utils.getTinyImageHtml(row.alt_id2); // `<div class="profilepic"><img src="/images/faces/${utils.getHashedImageName(row.alt_id2 + '.jpg', '.jpg')}"></div>`;
        });
        res.appRender('studentListView', {
          students: {
            headers: [
              {
                id: 'Id',
              },
              {
                img: 'Image',
                __raw: true,
              },
              {
                first_name: 'First name',
              },
              {
                last_name: 'Last name',
              },
              {
                alt_id: 'alt_id',
              },
              {
                alt_id2: 'alt_id2',
              },
              {
                email: 'Email',
              },
              {
                class_group: 'Class group name',
              },
              {
                provider: 'Provider',
              },
              {
                ts_created: 'ts_created',
              },
              {
                ts_modified: 'ts_modified',
              },
            ],
            rows: data,
            buttons: [
              {
                label: 'Delete',
                class: function (row) {
                  return 'btn btn-outline-danger';
                },
                method: 'POST',
                action: function (row) {
                  return '/admin/course/student/delete/' + row['id'];
                },
              },
              {
                label: 'Impersonate',
                method: 'GET',
                action: function (row) {
                  return '/admin/student/impersonate/' + row['id'];
                },
              },
              {
                label: 'Send register email',
                method: 'GET',
                action: function (row) {
                  return '/mail/onemail/' + row['id'];
                },
              },
            ],
            links: [
              {
                label: 'Score',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return `/analytics/score/student/${row['id']}`;
                },
                __formatter: function (row) {
                  return `<i class="fas fa-chart-line"></i>`;
                },
              },
              {
                label: 'Accs.log',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return `/admin/access-logs/${row['alt_id']}`;
                },
                __formatter: function (row) {
                  return `<i class="fa fa-2 fa-eye"></i>`;
                },
              },
            ],
          },
        });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching students: ' + error.message)
        );
      });
  }
);
router.get(
  '/teacher/list',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res, next) {
    let data = await Promise.all([
      db.any(
        `SELECT app_user.id, first_name, last_name, email, username, alt_id, alt_id2, provider,
                            app_user.ts_created::timestamp(0)::varchar, app_user.ts_modified::timestamp(0)::varchar
                  FROM perm_user_course
                  JOIN app_user
                    ON perm_user_course.id_user = app_user.id
                 WHERE id_course = $(cId)
                 ORDER BY last_name, first_name`,
        {
          cId: req.session.currCourseId,
        }
      ),
      db.any(
        `SELECT last_name || ' ' || first_name as name, id as value
                    FROM app_user
                    WHERE username is not null
                      AND NOT EXISTS (SELECT * FROM perm_user_course WHERE id_user = app_user.id AND id_course = $(cId) )
                    ORDER BY last_name`,
        {
          cId: req.session.currCourseId,
        }
      ),
    ]);
    data[0].forEach(function (row) {
      row.img = utils.getTinyImageHtml(row.alt_id2);
    });
    res.appRender('teacherListView', {
      app_users: data[1],
      teachers: {
        headers: [
          {
            id: 'Id',
          },
          {
            img: 'Image',
            __raw: true,
          },
          {
            first_name: 'First name',
          },
          {
            last_name: 'Last name',
          },
          {
            alt_id: 'alt_id',
          },
          {
            alt_id2: 'alt_id2',
          },
          {
            email: 'Email',
          },
          {
            username: 'Username',
          },
          {
            provider: 'Provider',
          },
          {
            ts_created: 'ts_created',
          },
          {
            ts_modified: 'ts_modified',
          },
        ],
        rows: data[0],
        buttons: [
          {
            label: 'Delete',
            class: function (row) {
              return 'btn btn-outline-danger';
            },
            method: 'POST',
            action: function (row) {
              return '/admin/course/teacher/delete/' + row['id'];
            },
          },
        ],
      },
    });
  }
);
router.get(
  '/user/list',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res, next) {
    let data = await db.any(
      `SELECT app_user.id, first_name, last_name, email, username, alt_id, alt_id2, provider,
                            app_user.ts_created::timestamp(0)::varchar, app_user.ts_modified::timestamp(0)::varchar,
                            (SELECT COUNT(*) FROM perm_user_course WHERE id_user = app_user.id) as course_cnt
                  FROM app_user
                 ORDER BY last_name, first_name`,
      {
        cId: req.session.currCourseId,
      }
    );
    data.forEach(function (row) {
      row.img = utils.getTinyImageHtml(row.alt_id2);
    });
    res.appRender('userListView', {
      teachers: {
        headers: [
          {
            id: 'Id',
          },
          {
            img: 'Image',
            __raw: true,
          },
          {
            first_name: 'First name',
          },
          {
            last_name: 'Last name',
          },
          {
            alt_id: 'alt_id',
          },
          {
            alt_id2: 'alt_id2',
          },
          {
            email: 'Email',
          },
          {
            username: 'Username',
          },
          {
            provider: 'Provider',
          },
          {
            ts_created: 'ts_created',
          },
          {
            ts_modified: 'ts_modified',
          },
          {
            course_cnt: 'Courses',
          },
        ],
        rows: data,
        buttons: [
          {
            label: 'Delete',
            class: function (row) {
              if (row.course_cnt > 0) {
                return 'd-none';
              } else {
                return 'btn btn-outline-danger';
              }
            },
            method: 'POST',
            action: function (row) {
              return '/admin/user/delete/' + row['id'];
            },
          },
        ],
      },
    });
  }
);
const LogEntry = require.main.require('./models/LogEntryModel.js');
router.get('/logs', middleware.requireRole(globals.ROLES.TEACHER), async function (req, res) {
  const rowLimit = req.query.rowLimit ? parseInt(req.query.rowLimit) : 1000;
  let now = moment(new Date());
  res.appRender('logsView', {
    date: req.query.date,
    rowLimit,
    logs: {
      headers: [
        {
          ts_collector: 'TS collector',
          __formatter: function (row) {
            let mins = moment.duration(now.diff(row.ts_collector)).asMinutes();
            return `<b>${moment(row.ts_collector).toISOString()}</b><br><i>${
              Math.round(mins) + ' minutes ago'
            }</i>`;
          },
          __raw: true,
        },
        {
          ts_log: 'TS log',
          __formatter: function (row) {
            if (row.ts_log) {
              let mins = moment.duration(now.diff(row.ts_log)).asMinutes();
              return `<b>${moment(row.ts_log).toISOString()}</b><br><i>${
                Math.round(mins) + ' minutes ago'
              }</i>`;
            } else {
              return 'not parsable';
            }
          },
          __raw: true,
        },
        {
          hostname: 'Hostname',
        },
        {
          logConfig: 'Log config',
          __formatter: function (row) {
            return `Label: <b>${row.logConfig.label}</b> <br>Type:<b>${row.logConfig.type}</b><br>Path:<small>${row.logConfig.path}</small>`;
          },
          __raw: true,
        },
        {
          appInstance: 'Inst.',
        },
        {
          level: 'Level',
        },
        {
          message: 'Message',
        },
        {
          stack: 'Stack trace',
        },
        {
          script: 'Script',
        },
      ],
      rows: await LogEntry.getLogEntries(rowLimit, req.query.date),
    },
  });
});
const AccessLogModel = require.main.require('./models/AccessLogModel.js');
router.get(
  '/access-logs/:username',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    const rowLimit = req.query.rowLimit ? parseInt(req.query.rowLimit) : 1000;
    let now = moment(new Date());
    res.appRender('accessLogsView', {
      date: req.query.date,
      username: req.params.username,
      rowLimit,
      logs: {
        headers: [
          {
            ts: 'Timestamp',
            __formatter: function (row) {
              let mins = moment.duration(now.diff(row.ts)).asMinutes();
              return `<b>${moment(row.ts).toISOString()}</b><br><i>${
                Math.round(mins) + ' minutes ago'
              }</i>`;
            },
            __raw: true,
          },
          {
            user: 'User',
          },
          {
            instance: 'Instance',
          },
          {
            url: 'Request',
            __formatter: function (row) {
              return `Method: <b>${row.method}</b> <br>URL: <b>${row.url}</b>`;
            },
            __raw: true,
          },
          {
            statusCode: 'Status Code',
          },
          {
            ip: 'IP',
          },
          {
            responeTime: 'Resp. time (ms)',
          },
          {
            userAgent: 'User agent',
          },
        ],
        rows: await AccessLogModel.getAccessLogEntries(
          rowLimit,
          req.query.date,
          req.params.username
        ),
      },
    });
  }
);

// router.get('/logs/nginx',
//     middleware.requireRole(globals.ROLES.TEACHER),
//     async function(req, res, next) {
//         mongoDb = mongoDb.useDb('error');
//         var msg = '';
//         var limit = req.query.numOfLogs ? parseInt(req.query.numOfLogs) : 100;
//         var date = undefined;
//         if (req.query.date != undefined && req.query.date != '') {
//             var currDate = new Date(req.query.date);
//             var nextDate = new Date(currDate.getTime() + 86400000);
//             date = {
//                 '@timestamp': {
//                     $gte: "\"" + req.query.date,
//                     $lt: "\"" + nextDate.toISOString().split('T')[0]
//                 }
//             };
//         }
//         var getData = async function() {
//             if (mongoDb === undefined || (await mongoDb).readyState !== 1) {
//                 if (config.mongooseConnectionStringForErrors !== null) {
//                     mongoDb = errorMongoose.createConnection(config.mongooseConnectionStringForErrors);
//                     mongoDb.on('error', function(err) {
//                         winston.error('connection to mongo failed ' + err);
//                     });
//                 }
//             }
//             if (mongoDb !== undefined && (await mongoDb).readyState === 1) {
//                 return mongoDb.collection("nginx_error").find(date).sort({ '@timestamp': -1 }).limit(limit).toArray();
//             }
//             msg = "Error connecting to MongoDB!"
//             return [];
//         }
//         var data = await getData();
//         var parseData = async function(data) {
//             var arr = [];
//             data.forEach(function(row) {
//                 row['nginx']['error']['timestamp'] = moment(row['@timestamp'].slice(1, -1)).format("YYYY-MM-DD hh:mm:ss");
//                 arr.push(row['nginx']['error']);
//             });
//             return arr;
//         };
//         var nginx = await parseData(data);
//         res.appRender('logsView', {
//             msg: msg,
//             date: req.query.date,
//             limit: limit,
//             type: 'nginx',
//             logs: {
//                 headers: [{
//                     'timestamp': 'Timestamp'
//                 }, {
//                     'tid': 'TID'
//                 }, {
//                     'level': 'Level'
//                 }, {
//                     'pid': 'PID'
//                 }, {
//                     'message': 'Message'
//                 }],
//                 rows: nginx
//             }
//         });

//     });

router.post(
  '/course/student/delete/:id_student([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.result(
      `
               DELETE FROM student_course
                WHERE id_student = $1
                  AND id_course = $2
                  AND id_academic_year = $3
            `,
      [parseInt(req.params.id_student), req.session.currCourseId, req.session.currAcademicYearId]
    )
      .then((result) => {
        if (result.rowCount === 1) {
          req.flash('info', 'Student deleted from the course.');
        } else {
          req.flash('info', result.rowCount + ' rows deleted !?');
        }
        res.redirect('/admin/student/list');
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);
router.post(
  '/course/teacher/delete/:id_teacher([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.result(
      `
               DELETE FROM perm_user_course
                WHERE id_user = $1
                  AND id_course = $2
            `,
      [parseInt(req.params.id_teacher), req.session.currCourseId]
    )
      .then((result) => {
        if (result.rowCount === 1) {
          req.flash('info', 'User(teacher) deleted from the course.');
        } else {
          req.flash('info', result.rowCount + ' rows deleted !?');
        }
        res.redirect('/admin/teacher/list');
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);

router.post(
  '/course/teacher/add',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    // db.result(`
    //         BEGIN WORK;
    //             INSERT INTO perm_user_course(id_user, id_course) VALUES ($(id_user), $(id_course));
    //             INSERT INTO student_course (id_academic_year, id_course, id_student, class_group)
    //                  SELECT $(id_academic_year), $(id_course), student.id , 'Teachers'
    //                    FROM student
    //                   WHERE id_app_user in (select id_user from perm_user_course where id_course = $(id_course))
    //                     AND id not in (select id_student from student_course where id_course = $(id_course) and id_academic_year =  $(id_academic_year));
    //         COMMIT WORK;
    //     `, {
    //         id_user: req.body.id_user,
    //         id_course: req.session.currCourseId,
    //         id_academic_year: req.session.currAcademicYearId
    //     })
    db.tx((t) => {
      // creating a sequence of transaction queries:
      let args = {
        id_user: req.body.id_user,
        id_course: req.session.currCourseId,
        id_academic_year: req.session.currAcademicYearId,
      };
      var q1 = t.result(
        `INSERT INTO perm_user_course(id_user, id_course) VALUES ($(id_user), $(id_course));`,
        args
      );
      var q2 = t.result(
        `INSERT INTO student_course (id_academic_year, id_course, id_student, class_group)
                          SELECT $(id_academic_year), $(id_course), student.id , 'Teachers'
                            FROM student
                           WHERE id_app_user = $(id_user)
                             AND id NOT IN (select id_student from student_course where id_course = $(id_course) and id_academic_year =  $(id_academic_year));`,
        args
      );
      return t.batch([q1, q2]);
    })
      .then((result) => {
        if (result && result.length) {
          req.flash(
            'info',
            `Teacher added to course. Yay!  (Added ${result[0].rowCount} teachers and ${result[1].rowCount} student alteregos to enrolled students.)`
          );
        } else {
          req.flash('info', 'Unexpected rows affected: ' + JSON.stringify(result));
        }
        res.redirect('/admin/teacher/list');
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);
router.post(
  '/appendteachers',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let result = await db.result(
        `
                INSERT INTO student_course(id_academic_year, id_course, id_student, class_group)
                SELECT $(id_academic_year), $(id_course), student.id, 'Teachers'
                    FROM perm_user_course
                    JOIN student
                    ON perm_user_course.id_user = student.id_app_user
                    WHERE id_course = $(id_course)
                    AND student.id NOT IN (
                            SELECT id_student FROM student_course WHERE id_academic_year = $(id_academic_year) and id_course = $(id_course)
                )
                `,
        {
          id_course: req.session.currCourseId,
          id_academic_year: req.session.currAcademicYearId,
        }
      );
      req.flash(
        'info',
        result.rowCount + ' rows added. Check the table for the "Teachers" class group name.'
      );
    } catch (error) {
      winston.error(error);
      req.flash('error', error);
    }
    res.redirect('/admin/student/list');
  }
);
router.post(
  '/user/add',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    let fName = req.body.first_name.trim();
    let lName = req.body.last_name.trim();
    let username = req.body.username.trim();
    let studUsername = '_' + username;
    db.tx((t) => {
      // 2, , req.body.email,

      var q1 = t.result(
        `
               INSERT INTO app_user(id_role, first_name, last_name, email, username, alt_id, alt_id2, provider) VALUES
                                    ($1,      $2,        $3,        $4,    $5,       $6,     $7,      $8)`,
        [
          2,
          fName,
          lName,
          req.body.email,
          username,
          req.body.alt_id,
          req.body.alt_id2,
          req.body.provider,
        ]
      );
      var q2 = t.result(
        `INSERT into student (alt_id, alt_id2, first_name, last_name, email, id_app_user, provider) VALUES
                          ($1, $2,  $3, $4, $5, (SELECT id FROM app_user WHERE username = $6), $7);`,
        [studUsername, req.body.alt_id2, fName, lName, req.body.email, username, req.body.provider]
      );
      return t.batch([q1, q2]);
    })
      .then((result) => {
        if (result && result.length) {
          req.flash('info', 'User added (including student alterego). Yay!');
        } else {
          req.flash('info', result.rowCount + ' rows affected !?');
        }
        res.redirect('/admin/user/list');
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);
router.post(
  '/user/delete/:id([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.tx((t) => {
      let args = {
        id_user: parseInt(req.params.id),
      };
      var q0 = t.result(
        `DELETE FROM student_course WHERE id_student IN (SELECT id FROM student WHERE id_app_user = $(id_user));`,
        args
      );
      var q1 = t.result(`DELETE FROM student WHERE id_app_user = $(id_user);`, args);
      var q2 = t.result(`DELETE FROM app_user_role WHERE id_app_user = $(id_user);`, args);
      var q3 = t.result(`DELETE FROM app_user WHERE id = $(id_user);`, args);
      return t.batch([q0, q1, q2, q3]);
    })
      .then((result) => {
        if (result && result.length) {
          req.flash('info', `Deleted.`);
        } else {
          req.flash('info', 'Unexpected rows affected: ' + JSON.stringify(result));
        }
        res.redirect('/admin/user/list');
      })
      .catch((error) => {
        winston.error(error, { username: req.session.passport.user });
        req.flash('error', 'Error (still referenced?): ' + JSON.stringify(error));
        res.redirect('/admin/user/list');
      });
  }
);

module.exports = router;
