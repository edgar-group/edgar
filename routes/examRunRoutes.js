// mounted at exam/run
'use strict';
var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var peerAssessmentService = require('../services/peerAssessmentService');
var utils = require('../common/utils');
const RunningInstances = require('../common/runningInstances');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
const moment = require('moment');

var TestLog = require.main.require('./models/TestLogModel.js');
var CodeRunLog = require.main.require('./models/CodeRunLogModel.js');
var TestLogDetails = require.main.require('./models/TestLogDetailsModel.js');
var OngoingTests = require.main.require('./models/OngoingTests.js');
var Heartbeat = require.main.require('./models/HeartbeatModel.js');

var testService = require.main.require('./services/testService');
var questionService = require.main.require('./services/questionService');
// var realTimePlagService = require.main.require(
//     "./services/realTimePlagService"
// );
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');
const HttpService = require('../lib/HttpServiceCommonJS');
const Scorer = require('../lib/Scorer');

const minioService = require('../services/minioService');
const fileUploadService = require('../services/fileUploadService');

// Serves the SPA (angular) app
router.get(
  '/:ordinal([0-9]{1,2})?',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res) {
    res.publicRender('exam/run/examRunView'); // break out of Layout - "full screen"
  }
);

// Checks and generatest the exam, if successful redirects to SPA exam app (above) - Game ON
router.post(
  '/new',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res, next) {
    req.session.id_test_instance = undefined;
    // delete anon_alt_id leftover from lecture quiz
    req.session.anon_alt_id = undefined;
    const password = req.body.password;
    const acceptsHTML = req.accepts('html');
    const acceptsJSON = req.accepts('json');
    const username = req.body.username;

    let course = await db.one(`SELECT is_competitive FROM course WHERE id = $1`, [
      req.session.currCourseId,
    ]);

    if (!acceptsHTML && !acceptsJSON) {
      res.status(400).json({
        error: 'Unsupported Accept header value',
      });
      return;
    }

    if (password.trim().toLowerCase() === 'friend' && acceptsHTML) {
      res.appRender('~/homeView', {
        errMessage: 'Almost there...',
        course: course,
      });
    } else if (password.trim().toLowerCase() === 'prijatelj' && acceptsHTML) {
      res.appRender('~/homeView', {
        errMessage: 'Nice try...',
        course: course,
      });
    } else if (password.trim().toLowerCase() === 'mellon' && acceptsHTML) {
      winston.info(
        `\n\n\n***************** ${req.session.fullname} SHALL NOT PASS!! ***************************\n\n\n`
      );
      res.appRender('~/homeView', {
        errMessage: req.session.fullname + ' SHALL NOT PASS!',
        ysnp: true,
        course: course,
      });
    } else {
      // db.tx((t) => {
      //     // var q1 = t.none(
      //     //     `START TRANSACTION ISOLATION LEVEL SERIALIZABLE;`
      //     // );
      //     var q2 = t.func(`chk_get_test_instance`, [
      //         req.session.studentId,
      //         req.headers["x-forwarded-for"] ||
      //             req.connection.remoteAddress,
      //         password,
      //         req.session.currCourseId,
      //     ]);
      //     return t.batch([q2]);
      // })
      db.func(`chk_get_test_instance`, [
        req.session.studentId,
        req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        password,
        req.session.currCourseId,
      ])
        .then(function (data) {
          winston.debug('chk_get_test_instance returned: ' + JSON.stringify(data));
          if (data[0].err_code === 0) {
            req.session.id_test_instance = data[0].id_test_instance;

            let promises = [];

            promises.push(
              db.one(
                `SELECT id_test, hint_result
                                FROM test_instance JOIN test on test_instance.id_test = test.id
                                WHERE test_instance.id = $(id_test_instance)`,
                {
                  id_test_instance: req.session.id_test_instance,
                }
              )
            );
            promises.push(
              db.none(
                `UPDATE test_instance SET ts_started = CURRENT_TIMESTAMP
                                    WHERE id = $1
                                    AND ts_started IS NULL`,
                [req.session.id_test_instance]
              )
            );
            if (username) {
              promises.push(
                db.none(`UPDATE test_instance SET comp_username = $1 WHERE id = $2`, [
                  username,
                  req.session.id_test_instance,
                ])
              );
            }
            Promise.all(promises).then(function ([item, _, __]) {
              // req.session.id_test = item.id_test;
              // req.session.hint_result = item.hint_result;
              RunningInstances.addExamInstance(req, item.id_test, item.hint_result);
              if (acceptsHTML) {
                res.redirect('/exam/run');
              } else if (acceptsJSON) {
                res.status(200).json({
                  test_instance: req.session.id_test_instance,
                });
              } else {
                res.status(400).send();
              }
            });
          } else {
            if (acceptsHTML) {
              res.appRender('~/homeView', {
                errMessage: data[0].comment,
                course: course,
              });
            } else if (acceptsJSON) {
              res.status(404).json({
                error: data[0].comment,
              });
            }
          }
        })
        .catch(function (error) {
          req.flash('error', error.toString());
          winston.error(error);
          res.appRender('~/homeView', {
            course: course,
          });
        });
    }
  }
);

// Web API used from the SPA NG exam app
router.get(
  '/instance',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let currTI = await RunningInstances.getCurrTestInstanceId(req);
      winston.info(
        `User ${
          req.session.impersonated_username || req.session.passport.user
        } fetching exam instance currTI =  ${currTI}...`
      );
      let test = await db.one(
        `SELECT title,
                       (SELECT COUNT(*) FROM test_instance_question WHERE id_test_instance = test_instance.id) AS questions_no,  -- bcs #PA2
                       CASE WHEN trim_clock THEN
                         EXTRACT(EPOCH FROM (ts_available_to - current_timestamp))::bigint
                       ELSE
                         (EXTRACT(EPOCH FROM (ts_started - current_timestamp)) + duration_seconds)::bigint
                       END as seconds_left,
                       prolonged,
                       student.first_name || ' ' || last_name  as student,
                       coalesce(alt_id, '') as alt_id,
                       coalesce(alt_id2, '') as alt_id2,
                       is_competition,
                       upload_file_limit,
                       forward_only,
                       test_type.type_name,
                       course.uses_ticketing AND ticket_policy.uses_tickets_exam as uses_ticketing
                  FROM test_instance
                  JOIN test on test_instance.id_test = test.id
                  JOIN test_type on test.id_test_type = test_type.id
                  JOIN student on test_instance.id_student =  student.id
                  JOIN course on test.id_course = course.id
                  JOIN ticket_policy on test.id_ticket_policy = ticket_policy.id
                 WHERE test_instance.id = $(id_test_instance)`,
        {
          id_test_instance: currTI,
        }
      );
      // on purpose, not using: req.session.impersonated_username || req.session.passport.user
      //   if the teacher is impesonating to fix someehting, I want them to start from question 1
      let props = (await OngoingTests.getCurrTestProperties(req.session, currTI)) || {};

      props.forward_only = test.forward_only; // overwriting if changed in the meanwhile
      props.last_ordinal = props.last_ordinal || 1; // careful not to delete last_ordinal!
      // tuck it back in:
      await OngoingTests.updateCurrTestProperties(req.session, props, currTI);
      // not persising with OngoingTests.updateCurrTestProperties, no need, it'll recover to these defaults

      const currAnswers = await OngoingTests.getCurrentAnswers(
        req.session.impersonated_username || req.session.passport.user,
        currTI
      );
      // winston.debug('*******************************************************************');
      // winston.debug('*******************************************************************');
      winston.info(
        'RECOVERED currAnswers for user ' +
          (req.session.impersonated_username || req.session.passport.user),
        currAnswers,
        currAnswers.length,
        typeof currAnswers
      );
      // winston.debug('*******************************************************************');
      // winston.debug('*******************************************************************');
      if (!(currAnswers && currAnswers.length)) {
        winston.info(
          `Test instance ID ${currTI} not found in currAnswers for user ${
            req.session.impersonated_username || req.session.passport.user
          }, setting it to empty array. Probably first time for this user and exam.`
        );
      }
      res.json({
        idTestInstance: currTI,
        title: test.title,
        noOfQuestions: test.questions_no,
        secondsLeft: test.seconds_left,
        prolonged: test.prolonged,
        currAnswers: currAnswers ? currAnswers : [],
        student: test.student,
        alt_id: test.alt_id,
        alt_id2: test.alt_id2,
        cmSkin: req.session.cmSkin,
        imgSrc: req.session.imgSrc,
        is_competition: test.is_competition,
        upload_file_limit: test.upload_file_limit,
        forward_only: test.forward_only,
        last_ordinal: props.last_ordinal,
        type_name: test.type_name,
        runStats: await testService.getRunStats(currTI),
        uses_ticketing: test.uses_ticketing,
      });
    } catch (error) {
      winston.error(
        'An error occured while fetching test instance (' +
          req.session.test_instance_id +
          '): ' +
          error.message
      );
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    }
  }
);

router.get(
  '/question/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req);

    var ordinal = parseInt(req.params.ordinal); // TODO: handle it?
    let ctp = await OngoingTests.getCurrTestProperties(req.session, currTI);
    let fwonly = !!(ctp && ctp.forward_only);
    if (fwonly) {
      let lastOrdinal = ctp.last_ordinal; // must be set || 1;
      if (ordinal < lastOrdinal) {
        winston.error(
          `Test instance ${req.session.id_test_instance} attempting to go back on a forward only test! Ordinal requested: ${ordinal}, last ordinal:${lastOrdinal}`
        );
        res
          .status(403)
          .send(
            "Hey, what do you think you're doing? Can't go back on a forward only test. Logged and reported."
          );
        return;
      } else if (ordinal > 1 + lastOrdinal) {
        winston.error(
          `Test instance ${req.session.id_test_instance} attempting to JUMP forward on a forward only test! Ordinal requested: ${ordinal}, last ordinal:${lastOrdinal}`
        );
        res
          .status(403)
          .send(
            "Hey, what do you think you're doing? Can't jump around on a forward only test. Logged and reported."
          );
        return;
      } else if (ordinal === 1 + lastOrdinal) {
        // this is fine, moving forward by 1
        ctp.last_ordinal = 1 + lastOrdinal;
        // console.log("Moving along! ", ctp);
        let item = await OngoingTests.updateCurrTestProperties(req.session, ctp, currTI);
        // console.log("Moving along - saved! ", item);
      } // else ordinal == lastOrdinal -> do nothing, user is just refreshing
    }
    try {
      let q = await testService.getQuestion(ordinal, currTI, false, false);
      res.json(q);
    } catch (err) {
      winston.error(err);
      res.json(err);
    }
  }
);

const respondAndLog = (req, res, ordinal, code, result, fwToPlagDetServer) => {
  res.json(result);
  (async () => {
    await TestLogDetails.logResult(
      await RunningInstances.getCurrTestInstanceId(req),
      ordinal,
      code,
      result
    );
  })();
  if (fwToPlagDetServer.examId) {
    HttpService.postJsonToUrl(
      '127.0.0.1', // TODO
      'code-run',
      '8888',
      fwToPlagDetServer
    ) // TODO  B64 encode code
      .catch((err) => {
        winston.error(err);
      })
      .finally(() => {
        winston.debug('Code run forwarded to PD server...');
      });
  }
};

router.post(
  '/question/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req);
    // winston.info("exam/run/question.currTI = " + currTI);
    var ordinal = parseInt(req.params.ordinal);
    var code = req.body.code;
    var baseScore, isCorrect;
    var timestampSubmitted = new Date();
    try {
      let q = await questionService.getQuestionBasicTestInstance(
        currTI, //req.session.id_test_instance,
        ordinal
      );
      // TODO: use params
      const id_plag_method = (
        await db.one(`SELECT id_plag_detection_algorithm FROM test WHERE id = (
                SELECT id_test FROM test_instance WHERE id = ${currTI}
            )`)
      ).id_plag_detection_algorithm;
      let rt_plag_det_obj = id_plag_method
        ? {
            examId: q.id_test,
            questionId: q.id,
            studentId: q.id_student,
            algorithmId: id_plag_method,
            code,
          }
        : {};
      if (
        q.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
        q.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
        q.type_name.toUpperCase().indexOf('CODE') >= 0
      ) {
        let runStats = await testService.getRunStats(
          currTI //req.session.id_test_instance
        );

        let thisRun = runStats.find((x) => x.ordinal === ordinal);

        if (thisRun && thisRun.can_run === false) {
          winston.error(
            `Hack attempted: test instance id ${
              currTI + '/' + req.session.id_test_instance
            } ignoring pace!`
          );
          res.status(403).json({
            success: false,
            error: {
              message: 'Hold your horses! Pace yourself.',
            },
          });
          return;
        }
        // let checkQuestionResult = await lib.checkSingleCQuestionTI(req.session.id_test_instance, ordinal, code, db,
        //     testService, req.session.currCourseId, id_programming_language,
        //     thisRun ? thisRun.qcnt * thisRun.tcnt : 1
        // );
        let scoreResult = await Scorer.scoreQuestionAtRuntime(
          currTI, //req.session.id_test_instance,
          ordinal,
          code,
          req.body.id_programming_language,
          thisRun ? thisRun.qcnt * thisRun.tcnt : 1
        );
        //var result;
        let j0 = scoreResult.j0;

        let result;

        if ((await RunningInstances.getCurrTestInstance(req)).hint_result) {
          // Kids mode
          //score = checkQuestionResult;
          //delete scoreResult.j0; // Don't send it back to client
          let formattedHtmlResult = utils.judge0response2Html({
            resp: j0,
            score: scoreResult.score,
            hideTestDetails: true,
            verboseTests: false,
          });
          delete scoreResult.score.c_outcome;
          result = {
            success: true,
            coderesult: formattedHtmlResult,
            score: scoreResult.score,
          };
        } else {
          result = {
            success: true,
            coderesult: utils.judge0response2Html({
              resp: j0,
              headerOnly: true,
              verboseTests: false,
            }),
            score: {
              hint: '-',
            },
          };
        }
        baseScore = scoreResult.score.score;
        isCorrect = scoreResult.score.is_correct;
        //result.question_runs = stats.question_runs;
        //result.test_instance_runs = stats.test_instance_runs;
        if (j0 && j0.status && j0.status.id === 14) {
          // Don't count pushbacks and various errors...
          await db.none(
            `INSERT INTO test_instance_question_run (
                                        id_test_instance_question, base_score, is_correct, ts_submitted)
                                    VALUES ((SELECT
                                            tiq.id
                                        FROM test_instance_question tiq
                                        WHERE tiq.id_test_instance = $(id_test_instance)
                                        AND tiq.ordinal = $(ordinal)), $(base_score), $(is_correct), $(ts_submitted))`,
            {
              id_test_instance: currTI,
              ordinal: ordinal,
              base_score: baseScore,
              is_correct: isCorrect,
              ts_submitted: timestampSubmitted,
            }
          );
          // refresh them stats:
          runStats = await testService.getRunStats(
            currTI //req.session.id_test_instance
          );
        }

        result.runStats = runStats;

        await respondAndLog(req, res, ordinal, code, result, {
          ...rt_plag_det_obj,
          score: result.score,
        });

        // no need to await, just move on?
        /*await*/

        CodeRunLog.logCodeRun(
          req.session.passport.user,
          req.session.studentId,
          (await RunningInstances.getCurrTestInstance(req)).id_test,
          code,
          q.id,
          ordinal,
          baseScore,
          isCorrect
        );
        // if (id_plag_method != null)
        //     realTimePlagService.updateSimilarity(
        //         req.session.studentId,
        //         getCurrTestInstance(req).id_test,
        //         q.id,
        //         code,
        //         id_plag_method
        //     );

        if (j0 && j0.status && j0.status.id === 14) {
          // Competition stuff:

          let test = await db.one(
            `SELECT eval_comp_score, is_competition
                            FROM test_instance JOIN test ON test_instance.id_test = test.id
                            WHERE test_instance.id = $(id_test_instance)
                        `,
            {
              id_test_instance: currTI,
            }
          );

          if (test.is_competition) {
            // check if test is_competitive
            // if not, proceed as you are
            // else insert into test_instance_question_run(easy) + test_instance_question_comp_score(comp_score, time_passed)
            // to calculate comp_score, fetch eval_comp_score, calculate time_passed, take last number of runs + 1, take base score
            let tiq = await db.one(
              `SELECT
                                    tiq.id
                                FROM test_instance_question tiq
                                WHERE tiq.id_test_instance = $(id_test_instance)
                                AND tiq.ordinal = $(ordinal)
                            `,
              {
                id_test_instance: currTI,
                ordinal: ordinal,
              }
            );

            let promises = []; // TODO put in list for parallel exec
            // now done for all, see upstairs:
            // await db.none(`INSERT INTO test_instance_question_run (id_test_instance_question, base_score, is_correct, ts_submitted)
            //                      VALUES ($1, $2, $3, $4)`, [tiq.id, baseScore, isCorrect, timestampSubmitted]);

            let { number_of_runs } = stats.question_runs;

            let { seconds_left, total_time } = await db.one(
              `SELECT CASE WHEN trim_clock THEN
                                EXTRACT(EPOCH FROM(ts_available_to - current_timestamp))::bigint
                            ELSE
                                (EXTRACT(EPOCH FROM(ts_started - current_timestamp)) + duration_seconds)::bigint
                            END as seconds_left,

                            CASE WHEN trim_clock THEN
                                EXTRACT(EPOCH FROM (ts_available_to - ts_available_from))::integer
                            ELSE
                                duration_seconds
                            END as total_time
                        FROM test_instance
                        JOIN test on test_instance.id_test = test.id
                        WHERE test_instance.id = $(id_test_instance)`,
              {
                id_test_instance: currTI,
              }
            );

            seconds_left = Math.max(seconds_left, 0);

            let formula = test.eval_comp_score;

            let comp_score = baseScore;

            if (formula) {
              formula = formula.replace(/\[bc\]/gi, baseScore);
              formula = formula.replace(/\[n\]/gi, number_of_runs);
              let timePassed = seconds_left / total_time; // [0, 1] -> starting from 1 approaching to 0 as time passes
              formula = formula.replace(/\[t\]/gi, timePassed);

              comp_score = eval(formula);
            }

            await db.none(
              `DELETE FROM test_instance_question_comp_score
                                            WHERE id_test_instance_question = $1`,
              [tiq.id]
            );

            await db.none(
              `INSERT INTO test_instance_question_comp_score
                                            (id_test_instance_question, base_score, comp_score, time_passed, number_of_runs)
                                            VALUES ($1, $2, $3, $4, $5)`,
              [tiq.id, baseScore, comp_score, total_time - seconds_left, number_of_runs]
            );
          }
        }
      } else if (q.type_name.toUpperCase().indexOf('JSON') >= 0) {
        // <JSON>
        let result;
        if ((await RunningInstances.getCurrTestInstance(req)).hint_result) {
          //promises.push(lib.checkSingleJSONQuestionTI(req.session.id_test_instance, ordinal, code, db, testService));
          result = await Scorer.scoreQuestionAtRuntime(
            currTI, //req.session.id_test_instance,
            ordinal,
            code
          );
        } else {
          result = await CodeRunnerService.execJSCodeTestInstance(
            currTI, // req.session.id_test_instance,
            ordinal,
            code,
            true,
            false
          );
        }
        let rv = {
          success: true,
          coderesult: result.success
            ? `<PRE>${JSON.stringify(result.data, null, 2)}</PRE>`
            : `${result.error.message} Position: ${result.error.position})`,
          score: result.score ? result.score : {},
        };
        await respondAndLog(req, res, ordinal, code, rv, {
          ...rt_plag_det_obj,
          score: rv.score,
        });
        baseScore = rv.score.score;
        isCorrect = rv.score.is_correct;

        // no need to await, just move on?
        /*await*/
        CodeRunLog.logCodeRun(
          req.session.passport.user,
          req.session.studentId,
          (await RunningInstances.getCurrTestInstance(req)).id_test,
          code,
          q.id,
          ordinal,
          baseScore,
          isCorrect
        );
        // if (id_plag_method != null)
        //     realTimePlagService.updateSimilarity(
        //         req.session.studentId,
        //         getCurrTestInstance(req).id_test,
        //         q.id,
        //         code,
        //         id_plag_method
        //     );

        // </JSON>
      } else if (q.type_name.toUpperCase().indexOf('SQL') >= 0) {
        // <SQL>
        code = `-- ${moment().format('YYYY-MM-DD, kk:mm:ss')} ${
          req.session.passport.user
        }\n\n${code}`;
        let result;
        if ((await RunningInstances.getCurrTestInstance(req)).hint_result) {
          result = await Scorer.scoreQuestionAtRuntime(
            currTI, // req.session.id_test_instance,
            ordinal,
            code
          );
        } else {
          result = await CodeRunnerService.execSQLCodeTestInstance(
            currTI, //req.session.id_test_instance,
            ordinal,
            code,
            true,
            false
          );
        }
        await respondAndLog(req, res, ordinal, code, result, {
          ...rt_plag_det_obj,
          score: result.score,
        });
        baseScore = result.score;
        isCorrect = result.is_correct;

        // no need to await, just move on?
        /*await*/
        CodeRunLog.logCodeRun(
          req.session.passport.user,
          req.session.studentId,
          (await RunningInstances.getCurrTestInstance(req)).id_test,
          code,
          q.id,
          ordinal,
          baseScore,
          isCorrect
        );
        // if (id_plag_method != null)
        //     realTimePlagService.updateSimilarity(
        //         req.session.studentId,
        //         getCurrTestInstance(req).id_test,
        //         q.id,
        //         code,
        //         id_plag_method
        //     );
        // </SQL>
      } else if (q.type_name.toUpperCase().startsWith('COMPLEX')) {
        let scoreResult = await Scorer.scoreQuestionAtRuntime(
          currTI, //req.session.id_test_instance,
          ordinal,
          code,
          req.body.id_programming_language,
          1
        );
        let j0 = scoreResult.j0;
        let result;

        let formattedHtmlResult = utils.judge0response2Html({
          resp: j0,
          score: scoreResult.score,
          hideTestDetails: true,
          verboseTests: false,
        });
        delete scoreResult.score.c_outcome;
        result = {
          success: true,
          coderesult: formattedHtmlResult,
          score: scoreResult.score,
        };
        baseScore = scoreResult.score.score;
        isCorrect = scoreResult.score.is_correct;

        await respondAndLog(req, res, ordinal, code, result, {
          ...rt_plag_det_obj,
          score: result.score,
        });

        CodeRunLog.logCodeRun(
          req.session.passport.user,
          req.session.studentId,
          (await RunningInstances.getCurrTestInstance(req)).id_test,
          code,
          q.id,
          ordinal,
          baseScore,
          isCorrect
        );
      }
      // console.table(await CodeRunLog.getCodeRuns(req.session.id_test, q.id));
      // // last two code runs:
      // console.table(await CodeRunLog.getCodeRunsTail(req.session.id_test, q.id, 2));
      // // code runs within last 10 seconds:
      // console.table(await CodeRunLog.getCodeRunsTail(req.session.id_test, q.id, undefined,10));
      // // code runs within last 10 seconds within last 5 code runs:
      // console.table(await CodeRunLog.getCodeRunsTail(req.session.id_test, q.id, 5, 10));
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: `An error occured (more info @server). (${
            currTI + '/' + req.session.id_test_instance
          }, ${ordinal})`,
        },
      });
    }
  }
);

router.post(
  '/log',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    const entries = req.body.entries;
    const promises = [];
    let serverSideCommand = undefined;
    const currTI = await RunningInstances.getCurrTestInstanceId(req, false); // I don't want logs to do the fold-in, they might arrive after the session has been cleared/submitted
    for (const entry of entries) {
      if (entry.eventName === 'Answer saved' || entry.eventName === 'Answer updated') {
        // stashing it also into the session object so that I can recover more easily
        // let ca = req.session.currAnswers || {};
        // if (!ca[currTI]) ca[currTI] = [];
        let payload = JSON.parse(entry.eventData);
        // ca[currTI][payload.ordinal - 1] = payload.answer; // req.session.id_test_instance
        // req.session.currAnswers = ca; //JSON.parse(JSON.stringify(ca));
        winston.debug(
          'updating: ' + (req.session.impersonated_username || req.session.passport.user)
        );
        promises.push(
          OngoingTests.updateCurrAnswer(
            req.session.impersonated_username || req.session.passport.user,
            currTI,
            payload.ordinal,
            payload.answer
          )
        );
      }
      if (!currTI) {
        if (utils.isAllowedAnonymousLogEvent(entry.eventName) && req.query.idti) {
          // I will allow 'Exam submitted' and 'Lost focus' to be logged even after the session vars have been cleared
          promises.push(TestLog.logEvent(req.session, entry, +req.query.idti));
        } else {
          serverSideCommand = {
            command: 'abort',
            message: 'Broken session, please restart the exam.',
          };
          promises.push(TestLog.logEvent(req.session, entry, +req.query.idti)); // log the original event
          promises.push(
            TestLog.logEvent(
              req.session,
              {
                eventName: 'Broken session',
                eventData: 'Broken session, ABORT command sent to the client.',
              },
              +req.query.idti
            )
          ); // log the original event
        }
      } else {
        promises.push(TestLog.logEvent(req.session, entry, currTI));
        if (!utils.isAllowedAnonymousLogEvent(entry.eventName)) {
          // Check if the exam is submitted:
          try {
            const tsSubmitted = await db.oneOrNone(
              'SELECT ts_submitted::varchar FROM test_instance WHERE id = $1',
              [currTI]
            );
            if (tsSubmitted && tsSubmitted.ts_submitted) {
              serverSideCommand = {
                command: 'abort',
                message: 'Exam submitted at ' + tsSubmitted.ts_submitted + '.',
              };
              promises.push(
                TestLog.logEvent(
                  req.session,
                  {
                    eventName: 'Exam already submitted.',
                    eventData:
                      'Exam already submitted at ' +
                      tsSubmitted.ts_submitted +
                      ', ABORT command sent to the client.',
                  },
                  +req.query.idti
                )
              );
            }
          } catch (error) {
            winston.error(error);
          }
        }
      }

      if (entry.eventName == 'Displayed test message.' && entry.eventData) {
        promises.push(
          db.none(
            `UPDATE test_message
                    SET ts_displayed = CURRENT_TIMESTAMP
                    WHERE id = $(id)
                      AND id_student = $(id_student)`,
            {
              id: entry.eventData,
              id_student: req.session.studentId,
            }
          )
        );
      }
      if (entry.eventName == 'Displayed ticket message.' && entry.eventData) {
        promises.push(
          db.none(
            `UPDATE ticket_message
                    SET ts_displayed = CURRENT_TIMESTAMP
                    WHERE id = $(id)
                      AND id_student = $(id_student)`,
            {
              id: entry.eventData,
              id_student: req.session.studentId,
            }
          )
        );
      }
    }
    res.json({
      done: true,
      command: serverSideCommand,
    });
    Promise.all(promises).then(function (args) {
      winston.debug('Logging done (' + promises.length + ' promises).');
    });
  }
);

router.post(
  '/hb',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req, false);
    winston.debug(
      `HB: ${moment().format('YYYY-MM-DD, h:mm:ss')} ${currTI} ${req.session.passport.user}`
    );
    if (currTI) {
      const id_test = (await RunningInstances.getCurrTestInstance(req)).id_test;
      Heartbeat.saveHeartbeat(
        currTI,
        id_test,
        req,
        await OngoingTests.getCurrentAnswers(
          req.session.impersonated_username || req.session.passport.user,
          currTI
        )
      );
      let msg = await db.oneOrNone(
        `SELECT id, message_title, message_body
                  FROM test_message
                  WHERE ts_sent is null
                    AND id_test = $(id_test)
                    AND id_student = $(id_student)
                  ORDER BY ts_created
                  LIMIT 1`,
        {
          id_test,
          id_student: req.session.studentId,
        }
      );
      res.json({
        done: true,
        msg: msg,
      });

      if (msg && msg.id) {
        await db.none(
          `UPDATE test_message
              SET ts_sent = CURRENT_TIMESTAMP
              WHERE id = $(id)`,
          {
            id: msg.id,
          }
        );
      }
    } else {
      // Apparently, this CAN happen. Not entirely sure how, maybe when users leaves the test open for a looong time and the session expires?
      winston.error(
        'Unknown current test instance id in HeartBeat, user: ' +
          req.session.passport.user +
          ' idti = ' +
          req.query.idti +
          ' running_instances = ' +
          JSON.stringify(req.session.running_instances)
      );
      res.json({
        done: true,
        error: 'Session expired? Please login and reload your exam.',
      });
    }
  }
);

router.post(
  '/file/upload/:ordinal([0-9]{1,3})/:fileOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req);
    // console.log('Uploading, useMinio = ' + utils.useMinio());
    if (currTI) {
      if (utils.useMinio()) {
        return minioService.uploadTestInstanceFile(req, res, currTI);
      } else {
        return fileUploadService.uploadTestInstanceFile(req, res, currTI);
      }
    } else {
      res.json({
        success: false,
        error: {
          message: 'Upload failed: who the hell are you? Session data not found.',
        },
      });
    }
  }
);
router.post(
  '/file/delete/:ordinal([0-9]{1,3})/:fileOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req);
    if (currTI) {
      if (utils.useMinio()) {
        return minioService.deleteTestInstanceFile(req, res, currTI);
      } else {
        return fileUploadService.deleteTestInstanceFile(req, res, currTI);
      }
    } else {
      res.json({
        success: false,
        error: {
          message: 'Delete failed: who the hell are you? Session data not found.',
        },
      });
    }
  }
);
router.get(
  '/file/download/:ordinal([0-9]{1,3})/:fileOrdinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let currTI = await RunningInstances.getCurrTestInstanceId(req, false);
    if (currTI) {
      if (utils.useMinio()) {
        return minioService.downloadTestInstanceFile(
          res,
          currTI,
          req.params.ordinal,
          req.params.fileOrdinal
        );
      } else {
        return fileUploadService.downloadTestInstanceFile(
          res,
          currTI,
          req.params.ordinal,
          req.params.fileOrdinal
        );
      }
    } else {
      res.json({
        success: false,
        error: {
          message: 'Delete failed: who the hell are you? Session data not found.',
        },
      });
    }
  }
);
router.post(
  '/startTheClock/:ordinal([0-9]{1,3})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let currTI = await RunningInstances.getCurrTestInstanceId(req);
      var ordinal = parseInt(req.params.ordinal);
      if (currTI) {
        const result = await db.result(
          `UPDATE test_instance_question
                    SET ts_started = CURRENT_TIMESTAMP
                    WHERE id_test_instance = $(id_test_instance)
                      AND ts_started IS NULL
                    AND ordinal = $(ordinal)`,
          {
            id_test_instance: currTI,
            ordinal: ordinal,
          }
        );
        res.json({
          success: true,
        });
        // if (result.rowCount === 1) {

        // } else {
        //   res.json({
        //     success: false,
        //     message: "Clock already started, cannot overwrite."
        //   });
        // }
      } else {
        res.json({
          success: false,
          error: {
            message: 'Update failed: who the hell are you? Session data not found.',
          },
        });
      }
    } catch (error) {
      winston.error(error);
      res.status(500).send('Error updating clock. See server logs...');
    }
  }
);

router.post(
  '/submit',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let currTI = await RunningInstances.getCurrTestInstanceId(req);
      var studAnswers = req.body.answers;
      if (studAnswers.length) {
        // Check whether it is OK to update?
        const ti = await db.one(
          `SELECT ts_submitted, async_submit, upload_file_limit
                        FROM test_instance JOIN test ON test.id = test_instance.id_test
                       WHERE test_instance.id = $(id_test_instance)`,
          {
            id_test_instance: currTI,
          }
        );
        if (ti) {
          if (ti.ts_submitted !== null) {
            winston.warn(new Error('Student cheating!? - Answers already submitted!'));
            res.json({
              success: false,
              error: 'Attempted re-submission of existing test!! Logged (cheating?).',
            });
          } else {
            winston.info(
              `!! Student submitting exam (user, instance, currTI) = (${req.session.passport.user}, ${req.session.id_test_instance}, ${currTI})`
            );

            if (ti.upload_file_limit > 0) {
              winston.info('Zipping files...');
              await testService.zipUploadedFiles(currTI, req.session.passport.user, studAnswers);
            }
            winston.debug('before updateTestInstanceWithSubmitValues');
            await testService.updateTestInstanceWithSubmitValues(currTI, studAnswers);
            await OngoingTests.clearTestData2(req.session, currTI);
            winston.info('Post-submit: clearing session data for currTI = ' + currTI);
            // Erase the session vars:
            // if (req.session.currAnswers) {
            //   req.session.currAnswers[currTI] = undefined;
            //   winston.info('Post-submit: cleared currAnswers.');
            // }
            RunningInstances.removeFromRunningTestInstances(req, currTI);
            // done with the session vars

            if (ti.async_submit) {
              // Browser gets a response:
              res.json({
                success: true,
                async_submit: true,
              });
              // ... and I carry on with the test evals:
              winston.info('Grade test async2, currTI:' + currTI);
              testService.gradeTest(currTI);
            } else {
              winston.info('Grade test sync2, currTI:' + currTI);
              const respMsg = await testService.gradeTest(currTI);
              res.json(respMsg);
            }
          }
        } else {
          winston.error(
            new Error(
              'Cannot find test_instance to update on submit? Re-submission (parallel) - cheating!? Logged.'
            )
          );
          res.json({
            success: false,
            error:
              'Cannot find test_instance to update on submit? Re-submission (parallel tabs) - cheating!? Logged.',
          });
        }
      }
    } catch (error) {
      winston.error('!!! An error occured while submitting exam: ' + error.message);
      winston.error(error);
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development'
            ? 'Error occured, sorry (keep calm, you answers are safe). Please notify admin.'
            : error.message || error,
      });
    }
  }
);

const request = require('request');
var url = require('url');
const runningInstances = require('../common/runningInstances');
router.get(
  '/peerattachment',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let currTI = await RunningInstances.getCurrTestInstanceId(req, false);
      let attachment = await peerAssessmentService.getPaAttachment(currTI);
      if (attachment.uploaded_files) {
        let newUrl = url.format({
          protocol: req.protocol,
          host: req.get('host'),
          pathname: `/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${attachment.uploaded_files}`,
        });
        let filename = attachment.uploaded_files.split(/\/|\\/).pop();
        res.setHeader('Content-Disposition', "attachment;filename*=UTF-8''" + filename);
        req.pipe(request.get(newUrl)).pipe(res);
      } else if (attachment.filename) {
        // Calibration question file - fake submission
        let pathname;
        if (utils.useMinio()) {
          pathname = `/${globals.QUESTIONS_ATTACHMENTS_ROUTE}/${attachment.filename}`;
        } else {
          pathname = `/upload/attachment/${attachment.filename}`;
        }
        let newUrl = url.format({
          protocol: req.protocol,
          host: req.get('host'),
          pathname,
        });
        req.pipe(request.get(newUrl)).pipe(res);
      } else {
        res.status(404).send('Missing attachement. Now what?');
      }
    } catch (error) {
      winston.error(error);
      res.status(404).send('Error finding attachement? See server logs...');
    }
  }
);

// *********************

module.exports = router;
