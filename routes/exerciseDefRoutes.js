'use strict';
const express = require('express');
const router = express.Router();
const errors = require('../common/errors');
const middleware = require('../common/middleware');
const utils = require('../common/utils');
const globals = require('../common/globals');
const db = require('../db').db;
const winston = require('winston');

const exerciseDefService = require('../services/exerciseDefService');

router.get('/', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res, next) => {
  const data = await exerciseDefService.listExercises(
    req.session.currCourseId,
    req.session.currAcademicYearId,
    req.session.studentId
  );

  data.forEach((row) => {
    row.analytics = `<a href="/exercise/analytics/${row['id']}/students">Students</a><br />
                <a href="/exercise/analytics/${row['id']}/groups">Groups</a><br />
                <a href="/feedback/?exerciseId[]=${row['id']}">Feedback</a>`;
  });

  res.appRender('~/exercise/listExercisesView', {
    exercises: {
      headers: [
        {
          title: 'Title',
        },
        {
          is_active: 'Active',
        },
        {
          description: 'Description',
        },
        {
          no_questions: 'Questions',
        },
        {
          no_students: 'Students',
        },
        {
          analytics: 'Analytics',
          __raw: true,
        },
      ],
      rows: data,
      buttons: [
        {
          label: 'Delete',
          method: 'POST',
          action: function (row) {
            return `/exercise/def/${row['id']}/delete`;
          },
          class: function (row) {
            return 'btn btn-outline-danger';
          },
          props: {
            edgar_confirm_text: 'Do you really want to delete this exercise?',
          },
        },
        {
          label: 'Edit',
          method: 'GET',
          action: (row) => `/exercise/def/${row['id']}/edit`,
        },
        {
          label: 'Questions',
          method: 'GET',
          action: (row) => `/exercise/def/${row['id']}/questions`,
          class: (row) => 'btn btn-outline-primary',
        },
      ],
    },
  });
});

router.get('/new', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res, next) => {
  db.task((t) =>
    t.batch([
      t.any(exerciseDefService.queries.adaptivityModels),
      t.any(exerciseDefService.queries.nodes, {
        id_course: req.session.currCourseId,
      }),
    ])
  ).then((data) => {
    res.appRender(
      'newExerciseDefView',
      {
        title: '',
        description: '',
        is_active: false,
        id_adaptivity_model: 0,
        adaptivity_models: data[0],
        nodes: data[1],
        selected_nodes: [],
      },
      '_exerciseEditorViewScript',
      '_exerciseEditorViewCSS'
    );
  });
});

router.post('/new', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res, next) => {
  db.func('new_exercise', [
    req.session.currCourseId,
    req.session.currAcademicYearId,
    req.body.id_adaptivity_model,
    req.session.appUserId,
    req.body.nodes.map((n) => parseInt(n)),
    req.body.title,
    req.body.description,
    req.body.is_active ? true : false,
  ])
    .then((data) => {
      req.flash('info', 'Exercise successfully created. You can continue editing if desired.');
      res.redirect(`/exercise/def/${data[0].new_exercise}/edit`);
    })
    .catch((err) => {
      req.flash('error', JSON.stringify(err));
      res.redirect('/exercise/def');
    });
});

router.get(
  '/:id_exercise([0-9]{1,10})/edit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await exerciseDefService.getExerciseData(
      req.params.id_exercise,
      req.session.currCourseId
    );

    res.appRender(
      'editExerciseDefView',
      {
        ...data,
        ...data.exercise,
      },
      '_exerciseEditorViewScript',
      '_exerciseEditorViewCSS'
    );
  }
);

router.post(
  '/:id_exercise([0-9]{1,10})/edit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    db.func('save_exercise', [
      req.params.id_exercise,
      req.body.id_adaptivity_model,
      req.session.appUserId,
      req.body.nodes.map((id) => parseInt(id)),
      req.body.title,
      req.body.description,
      req.body.is_active ? true : false,
    ])
      .then(() => {
        req.flash('info', `Exercise ${req.params.id_exercise} successfully updated.`);
        res.redirect(`/exercise/def/${req.params.id_exercise}/edit`);
      })
      .catch((err) => {
        req.flash('error', JSON.stringify(err));
        res.redirect(`/exercise/def/${req.params.id_exercise}/edit`);
      });
  }
);

router.post(
  '/:id_exercise([0-9]{1,10})/delete',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    db.func('delete_exercise', [req.params.id_exercise])
      .then((data) => {
        req.flash('info', `Exercise ${req.params.id_exercise} successfully deleted.`);
        res.redirect(`/exercise/def`);
      })
      .catch((err) => {
        req.flash('error', JSON.stringify(err));
        res.redirect('/exercise/def');
      });
  }
);

router.get(
  '/:id_exercise([0-9]{1,10})/questions',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      const data = await exerciseDefService.getExerciseQuestions(req.params.id_exercise);
      data.questions.forEach((row) => {
        row.id_ver_act = `<a href="/question/edit/${row.id}" target="_blank">${row.id}</a><br>${row.version}<br>${row.is_active}`;
      });

      res.appRender(
        'exerciseQuestionsView',
        {
          ...data,
        },
        'exerciseQuestionsViewScript',
        'exerciseQuestionsViewCSS'
      );
    } catch (err) {
      req.flash('error', JSON.stringify(err));
      res.redirect(`/exercise/def`);
    }
  }
);

router.post(
  '/:id_exercise([0-9]{1,10})/questions',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      if (req.body.save) {
        await exerciseDefService.saveAssignedQuestionDifficulties(
          req.params.id_exercise,
          req.body.difficulty,
          req.session.appUserId
        );
        req.flash(
          'info',
          `Assigned difficulties for ${req.body.difficulty.length} questions saved`
        );
      } else if (req.body.delete) {
        const deletedIds = await exerciseDefService.deleteQuestionDifficultyComputation(
          req.params.id_exercise,
          parseInt(req.body.delete)
        );

        if (deletedIds.length == 0) req.flash('info', 'Nothing to delete');
        else
          req.flash(
            'info',
            `Question difficulty computations (${deletedIds
              .map((x) => x.id)
              .join(', ')}) and their computed question difficulties successfully deleted.`
          );
      }
    } catch (err) {
      req.flash('error', JSON.stringify(err));
      res.redirect(`/exercise/def/${req.params.id_exercise}/questions`);
    }
    res.redirect(`/exercise/def/${req.params.id_exercise}/questions`);
  }
);

router.post(
  '/:id_exercise([0-9]{1,10})/questions/compute-difficulties',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      const computationId = await exerciseDefService.computeQuestionDifficulties(
        req.params.id_exercise,
        parseInt(req.body.min_answers) || 1,
        parseInt(req.body.no_model_params) || 1,
        req.session.appUserId
      );
      res.json({
        success: true,
        computationId: computationId,
      });
    } catch (err) {
      res.json({
        success: false,
        message: err.message,
      });
    }
  }
);

module.exports = router;
