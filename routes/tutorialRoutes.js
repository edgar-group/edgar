// 'use strict';
// const express = require('express');
// const router = express.Router();
// const middleware = require('../common/middleware');
// const globals = require('../common/globals');
// const db = require('../db').db;
// const winston = require('winston');
// const tutorialService = require('../services/tutorialService');
// const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');
// router.get('/', async (req, res) => {
//   const data = await tutorialService.listTutorials(
//     req.session.currCourseId,
//     req.session.studentId,
//     req.session.rolename === globals.ROLES.STUDENT
//   );

//   res.appRender('listTutorialsView', {
//     tutorials: {
//       headers: [
//         {
//           tutorial_title: 'Title',
//         },
//         {
//           tutorial_desc: 'Description',
//         },
//         {
//           courses: 'Course(s)',
//         },
//         {
//           no_steps: 'Steps',
//         },
//         {
//           finished: 'Finished',
//         },
//         {
//           ts_finished: 'Finished at',
//         },
//       ],
//       rows: data,
//       buttons: [
//         {
//           label: 'Start',
//           method: 'GET',
//           action: (row) => `/tutorial/${row['id']}`,
//         },
//       ],
//     },
//   });
// });

// router.get(
//   '/:id_tutorial([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
//   (req, res, next) => {
//     req.session.currTutorialId = parseInt(req.params.id_tutorial);
//     res.publicRender('tutorial/tutorialView'); // break out of Layout - "full screen"
//   }
// );

// // API routes

// router.post(
//   '/api/start/:id_tutorial([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     try {
//       const status = await tutorialService.start(
//         req.session.currCourseId,
//         req.session.studentId,
//         parseInt(req.params.id_tutorial)
//       );
//       req.session.currTutorialId = parseInt(req.params.id_tutorial);
//       winston.debug(
//         `Student ${req.session.studentId} starts/continues tutorial ${req.session.currTutorialId} at step ${status.latestStepOrdinal}`
//       );
//       res.json(status);
//     } catch (error) {
//       req.flash('error', error);
//       res.status(500);
//     }
//   }
// );

// router.get(
//   '/api/:id_tutorial([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     const tutorial = await tutorialService.getTutorial(req.params.id_tutorial);
//     req.session.currTutorialId = parseInt(req.params.id_tutorial);
//     res.json({
//       ...tutorial,
//       cmSkin: req.session.cmSkin,
//     });
//   }
// );

// router.get(
//   '/api/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     try {
//       const step = await tutorialService.getTutorialStep(
//         parseInt(req.params.id_tutorial),
//         parseInt(req.params.step_ordinal),
//         parseInt(req.session.currCourseId),
//         parseInt(req.session.studentId)
//       );
//       res.json(step);
//     } catch (err) {
//       winston.error('Error at fetching tutorial step ' + JSON.stringify(err));
//       res.status(500);
//     }
//   }
// );

// router.get(
//   '/api/:id_tutorial([0-9]{1,10})/answers',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     try {
//       const answers = await tutorialService.getStudentAnswers(
//         req.session.currCourseId,
//         req.session.studentId,
//         parseInt(req.params.id_tutorial)
//       );
//       res.json({
//         currAnswers: answers,
//       });
//     } catch (err) {
//       winston.error('failed to get current student answers for tutorial ' + JSON.stringify(err));
//       res.status(500);
//     }
//   }
// );

// router.get(
//   '/api/start/:id_tutorial([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     try {
//       const status = await tutorialService.start(
//         req.session.currCourseId,
//         req.session.studentId,
//         req.params.id_tutorial
//       );
//       req.session.currTutorialId = req.params.id_tutorial;
//       winston.debug(
//         `Student ${req.session.studentId} starts/continues tutorial at step ${status.latestStepOrdinal}`
//       );
//       res.json(status);
//     } catch (error) {
//       req.flash('error', error);
//       res.status(500);
//     }
//   }
// );

// router.put(
//   '/api/:id_tutorial([0-9]{1,10})/answers',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     try {
//       await tutorialService.updateStudentAnswers(
//         req.session.currCourseId,
//         req.session.studentId,
//         parseInt(req.params.id_tutorial),
//         req.body.answers
//       );
//       res.json({
//         success: true,
//       });
//     } catch (err) {
//       winston.error('failed to update current answers for tutorial ' + JSON.stringify(err));
//       res.status(500);
//     }
//   }
// );

// router.post(
//   '/api/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})',
//   middleware.requireRoles([globals.ROLES.STUDENT, globals.ROLES.TEACHER]),
//   async (req, res, next) => {
//     // get question id
//     const questionId = parseInt(
//       (
//         await db.one(
//           `SELECT id_question
//             FROM tutorial_step
//             WHERE id_tutorial = $(id_tutorial)
//                 AND ordinal = $(ordinal)`,
//           {
//             id_tutorial: req.params.id_tutorial,
//             ordinal: req.params.step_ordinal,
//           }
//         )
//       ).id_question
//     );

//     // check if answer correct
//     try {
//       // console.log("Before", req.body.answer);
//       let convertedAnswer =
//         req.body.answer.multichoice && req.body.answer.multichoice.length
//           ? req.body.answer.multichoice
//           : req.body.answer.codeAnswer.code;
//       const result = await tutorialService.evaluateQuestionAnswer(
//         req.session.currCourseId,
//         req.session.studentId,
//         parseInt(req.params.id_tutorial),
//         questionId,
//         convertedAnswer,
//         parseInt(req.params.step_ordinal)
//       );
//       // console.log("After", result);
//       res.json({
//         correct: result.score.is_correct,
//       });
//     } catch (err) {
//       res.json({
//         success: false,
//         error: {
//           message: JSON.stringify(err.message),
//         },
//       });
//     }
//   }
// );

// router.post(
//   '/question/exec/:ordinal([0-9]{1,2})',
//   middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
//   async (req, res) => {
//     try {
//       const question = await db.one(
//         `SELECT question.*
//                 FROM question JOIN tutorial_step
//                     ON question.id = tutorial_step.id_question
//                 WHERE tutorial_step.id_tutorial = $(id_tutorial)
//                     AND tutorial_step.ordinal = $(ordinal)`,
//         {
//           id_tutorial: parseInt(req.session.currTutorialId),
//           ordinal: parseInt(req.params.ordinal),
//         }
//       );
//       // (courseId, studentId, tutorialId, questionId, answer, currStepOrdinal)
//       const result = await tutorialService.evaluateQuestionAnswer(
//         parseInt(req.session.currCourseId),
//         req.session.studentId,
//         parseInt(req.session.currTutorialId),
//         parseInt(question.id),
//         req.body.code,
//         parseInt(req.params.ordinal)
//       );

//       return res.json(result);
//     } catch (err) {
//       winston.error(
//         `Failed to evalute question (ordinal = ${req.params.ordinal}) at tutorial ${req.session.currTutorialId}. ` +
//           JSON.stringify(err)
//       );
//       res.json({
//         success: false,
//         error: {
//           message: `An error occured while evaluating question ${req.params.ordinal} (more info @server).`,
//         },
//       });
//     }
//   }
// );

// router.post(
//   '/code/exec/:ordinal([0-9]{1,2})',
//   middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
//   async (req, res) => {
//     try {
//       const question = await db.one(
//         `SELECT id_code_runner
//                 FROM tutorial_step
//                 WHERE tutorial_step.id_tutorial = $(id_tutorial)
//                     AND tutorial_step.ordinal = $(ordinal)`,
//         {
//           id_tutorial: parseInt(req.session.currTutorialId),
//           ordinal: parseInt(req.params.ordinal),
//         }
//       );

//       res.json(
//         await CodeRunnerService.execSQLCodeCodeRunner(question.id_code_runner, req.body.code)
//       );
//     } catch (err) {
//       winston.error(
//         `Failed to evalute step code (ordinal = ${req.params.ordinal}) at tutorial ` +
//           JSON.stringify(err)
//       );
//       res.json({
//         success: false,
//         error: {
//           message: `An error occured while evaluating step code ${req.params.ordinal} (more info @server).`,
//         },
//       });
//     }
//   }
// );

// router.post(
//   '/log',
//   middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
//   function (req, res, next) {
//     let event = req.body.e;
//     event.ip = req.ip;
//     if (req.session.currTutorialId) {
//       tutorialService.logEvent(
//         req.session.currCourseId,
//         req.session.studentId,
//         parseInt(req.session.currTutorialId),
//         event
//       );

//       res.json({
//         done: true,
//       });
//     } else {
//       res.json({
//         done: true,
//         message: 'Failed to log, unknown currTutorialId',
//       });
//     }
//   }
// );

// module.exports = router;
