'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var db = require('../db').db;
// var lectureService = require.main.require('./services/lectureService');
// var TestLog = require.main.require('./models/TestLogModel.js');
// var OngoingTests = require.main.require('./models/OngoingTests.js');
var utils = require('../common/utils');
router.get('/list', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res, next) {
  db.any(
    `SELECT teacher_lecture_quiz_instance.id,
                        (ts_started :: timestamp) :: varchar as ts_start,
                        (ts_ended :: timestamp) :: varchar as ts_end,
                        test.title,
                        test.max_score,
                        left(test.password, 3) || '***' as spassword,
                        test.questions_no,
                        (first_name || ' ' || last_name) as user_started,
                        count(*) as students_no
                 FROM teacher_lecture_quiz_instance
                 JOIN test ON test.id = teacher_lecture_quiz_instance.id_test
                 JOIN app_user ON teacher_lecture_quiz_instance.id_app_user = app_user.id
                 LEFT JOIN test_instance_room
                        ON teacher_lecture_quiz_instance.id = test_instance_room.id_teacher_instance
                WHERE app_user.id =  $(app_user_id)
                  AND test.id_course = $(id_course)
                  AND test.id_academic_year = $(id_academic_year)
                GROUP BY teacher_lecture_quiz_instance.id,
                                    ts_start,
                                    ts_end,
                                    test.title,
                                    test.max_score,
                                    spassword,
                                    test.questions_no,
                                    user_started`,
    {
      app_user_id: req.session.appUserId,
      id_course: req.session.currCourseId,
      id_academic_year: req.session.currAcademicYearId,
    }
  )
    .then(function (data) {
      res.appRender('lectureTeacherInstancesView', {
        instances: {
          headers: [
            {
              id: 'Id',
            },
            {
              title: 'Title',
            },
            {
              user_started: 'User started',
            },
            {
              room_name: 'Room name',
            },
            {
              max_score: 'Max score',
            },
            {
              spassword: 'Password',
            },
            {
              questions_no: 'No of qs',
            },
            {
              ts_start: 'Started',
            },
            {
              ts_end: 'Finished',
            },
            {
              students_no: 'Students',
            },
          ],
          rows: data,
          buttons: [
            {
              label: 'Delete',
              method: 'POST',
              action: function (row) {
                return '/lecture/instances/delete/' + row['id'];
              },
              class: function (row) {
                return 'btn btn-outline-danger';
              },
              props: {
                edgar_confirm_text:
                  "Do you really want to delete this test (quizz) instances? (this will NOT delete test definition, only students' answers)",
              },
            },
            {
              label: 'Results',
              method: 'GET',
              action: function (row) {
                return '/lecture/instances/results/' + row['id'];
              },
            },
          ],
        },
      });
    })
    .catch((error) => {
      return next(
        new errors.DbError(
          'An error occured while fetching teacher lecture quiz instances: ' + error.message
        )
      );
    });
});

router.post(
  '/delete/:id_lecture_quiz_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_lecture_quiz_instance) {
      db.func('del_lecture_quiz_instance', [req.params.id_lecture_quiz_instance])
        .then(function (data) {
          winston.debug('del_lecture_quiz_instance returned: ' + data);
          req.flash(
            'info',
            `Lecture quiz instance ${req.params.id_lecture_quiz_instance} deleted.`
          );
          res.redirect(`/lecture/instances/list`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/lecture/instances/list`);
        });
    } else {
      req.flash('error', 'id_lecture_quiz_instance id NOT set.');
      res.redirect(`/lecture/instances/list`);
    }
  }
);

router.get(
  '/results/:id([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var rv = {};
    db.any(
      `SELECT test_instance_room.id,
                        student.first_name,
                        student.last_name,
                        test_instance.score,
                        test_instance.score_perc * 100 as score_perc
                FROM test_instance_room JOIN test_instance ON test_instance_room.id_test_instance = test_instance.id
                JOIN student ON test_instance.id_student = student.id
                WHERE test_instance_room.id_teacher_instance = $(id_teacher_instance)`,
      { id_teacher_instance: req.params.id }
    )
      .then(function (results) {
        var percScores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        var roundPerc;
        for (let i = 0; i < results.length; i++) {
          roundPerc = parseInt((results[i].score_perc / 10).toString());
          percScores[roundPerc]++;
        }
        rv.rows = results;
        rv.percScores = percScores;
        return db.any(
          `SELECT tiq.ordinal, tiq.id_question, question_text, tiq.answers_permutation, tiq.correct_answers_permutation, type_name,
                                string_to_array(string_agg(array_to_string(student_answers, ','), ','), ',')  as stud_abc_answers,
                                array_agg(student_answer_text order by tiq.id) as stud_text_answers,
                                array_agg(student.alt_id      order by tiq.id) as stud_text_authors,
                                (SELECT array_agg(answer_text ORDER BY question_answer.ordinal) FROM question_answer WHERE  tiq.id_question= question_answer.id_question) as abc_answers
                          FROM test_instance_room tir
                          JOIN test_instance ti
                            ON tir.id_test_instance = ti.id
                          JOIN test_instance_question tiq
                            ON ti.id = tiq.id_test_instance
                          JOIN question on tiq.id_question = question.id
                          JOIN question_type
                            ON question.id_question_type = question_type.id
                          JOIN student
                            ON ti.id_student = student.id
                         WHERE tir.id_teacher_instance = $(id_teacher_instance)
                         GROUP BY tiq.ordinal, tiq.id_question, question_text, tiq.answers_permutation, tiq.correct_answers_permutation, type_name
                         ORDER BY tiq.ordinal `,
          {
            id_teacher_instance: req.params.id,
          }
        );
      })
      .then(async function (_qs) {
        var qs = _qs;
        for (let g = 0; g < qs.length; g++) {
          var q = qs[g];

          q.html = await utils.md2Html(q.question_text);

          // TODO igor here
          var htmlAnswersPerm = [];
          if (q.abc_answers) {
            // Rearange answers to current permutation:
            for (let i = 0; q.answers_permutation && i < q.answers_permutation.length; ++i) {
              let permIdx = q.answers_permutation[i] - 1;
              htmlAnswersPerm.push({
                aHtml: await utils.md2Html(q.abc_answers[permIdx]),
                ordinal: String.fromCharCode(97 + i),
              });
            }
            let corrAnswers = [];
            for (
              let i = 0;
              q.correct_answers_permutation && i < q.correct_answers_permutation.length;
              ++i
            ) {
              corrAnswers.push(String.fromCharCode(97 + q.correct_answers_permutation[i] - 1));
            }
            q.correct_answers_permutation = corrAnswers.join(', ');
            q.abc_html = htmlAnswersPerm;
            let distrib = {};
            for (let i = 0; q.stud_abc_answers && i < q.stud_abc_answers.length; ++i) {
              let label = q.stud_abc_answers[i]
                ? String.fromCharCode(97 + parseInt(q.stud_abc_answers[i]) - 1)
                : '?';

              if (distrib[label]) {
                distrib[label] += 1;
              } else {
                distrib[label] = 1;
              }
            }
            q.abc_distrib = distrib;
          }
        }
        rv.qs = qs;
        // console.log(JSON.stringify(rv));
        res.appRender('lectureResultsView', rv, 'lectureResultsViewScript');
      })
      .catch((error) => {
        return next(
          new errors.DbError(
            'An error occured while fetching teacher lecture quiz instances: ' + error.message
          )
        );
      });
  }
);

router.get(
  '/getresults',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var lectureId = req.session.id_lecture_instance;
    req.session.id_lecture = undefined;
    req.session.id_lecture_instance = undefined;
    res.redirect('/lecture/instances/results/' + lectureId);
  }
);

module.exports = router;
