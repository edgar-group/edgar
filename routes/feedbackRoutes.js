'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const db = require('../db').db;
const utils = require('../common/utils');
const winston = require('winston');

router.get('/', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res, next) => {
  let filters = [];
  let selectedInfo = [];
  let selectedExercises = req.query.exerciseId
      ? req.query.exerciseId.map((id) => parseInt(id))
      : [],
    selectedTutorials = req.query.tutorialId ? req.query.tutorialId.map((id) => parseInt(id)) : [],
    selectedTutorialSteps = req.query.tutorialStepId
      ? req.query.tutorialStepId.map((id) => parseInt(id))
      : [],
    selectedStudents = req.query.studentId ? req.query.studentId.map((id) => parseInt(id)) : [],
    selectedClassGroups = req.query.classGroup || [],
    selectedQuestions = req.query.questionId ? req.query.questionId.map((id) => parseInt(id)) : [];

  let tableColumns = [
    {
      id: 'Id',
    },
    {
      student: 'Student',
      __raw: true,
    },
    {
      class_group: 'Class group',
    },
    {
      exercise: 'Exercise',
      __raw: true,
    },
    {
      tutorial: 'Tutorial',
      __raw: true,
    },
    {
      question: 'Question',
      __raw: true,
    },
    {
      comment: 'Comment',
    },
    {
      rating: 'Rating',
    },
    {
      ts_created: 'Timestamp',
    },
  ];

  if (req.query.exerciseId) {
    filters.push(`exercise.id = ANY ('{${selectedExercises}}'::int[])`);
    tableColumns = tableColumns.filter((tc) => !tc.exercise);
  }

  if (req.query.tutorialId) {
    filters.push(`tutorial.id = ANY ('{${selectedTutorials}}'::int[])`);
  }

  if (req.query.tutorialStepId) {
    filters.push(`tutorial_step.id = ANY ('{${selectedTutorialSteps}}'::int[])`);
  }

  if (req.query.questionId) {
    filters.push(`question.id = ANY ('{${selectedQuestions}}'::int[])`);
  }

  if (req.query.classGroup) {
    filters.push(`class_group =  ANY ('{${selectedClassGroups}}'::text[])`);
    tableColumns = tableColumns.filter((tc) => !tc.class_group);
  }

  if (req.query.studentId) {
    filters.push(`student.id = ANY ('{${selectedStudents}}'::int[])`);
    tableColumns = tableColumns.filter((tc) => !tc.student);
  }

  try {
    const data = await db.task((t) =>
      t.batch([
        db.any(
          `SELECT DISTINCT title as name, id as value
                        FROM exercise
                        WHERE id_course = $(id_course)
                            AND id_academic_year = $(id_academic_year)`,
          {
            id_course: req.session.currCourseId,
            id_academic_year: req.session.currAcademicYearId,
          }
        ),
        db.any(
          `SELECT DISTINCT tutorial_title as name, tutorial.id as value
                        FROM tutorial JOIN tutorial_course
                            ON tutorial.id = tutorial_course.id_tutorial
                        WHERE id_course = $(id_course)`,
          {
            id_course: req.session.currCourseId,
          }
        ),
        db.any(
          `SELECT DISTINCT tutorial_step.title as name, tutorial_step.id as value
                        FROM tutorial_step JOIN tutorial
                            ON tutorial.id = tutorial_step.id_tutorial
                        JOIN tutorial_course
                            ON tutorial.id = tutorial_course.id_tutorial
                        WHERE id_course = $(id_course)`,
          {
            id_course: req.session.currCourseId,
          }
        ),
        db.any(
          `SELECT DISTINCT class_group as name, class_group as value
                        FROM student JOIN student_course
                            ON student.id = student_course.id_student
                            AND student_course.id_academic_year = $(id_academic_year)
                        WHERE id_course = $(id_course)
                        ORDER BY class_group`,
          {
            id_course: req.session.currCourseId,
            id_academic_year: req.session.currAcademicYearId,
          }
        ),
        db.any(
          `SELECT DISTINCT first_name || ' ' || last_name as name, student.id as value
                        FROM student JOIN student_course
                            ON student.id = student_course.id_student
                            AND student_course.id_academic_year = $(id_academic_year)
                        WHERE id_course = $(id_course)`,
          {
            id_course: req.session.currCourseId,
            id_academic_year: req.session.currAcademicYearId,
          }
        ),
        db.any(
          `SELECT feedback.id,
                            student.id student_id,
                            student.alt_id2 student_alt_id2,
                            first_name || ' ' || last_name || ' (' || alt_id2 || ')' student_label,
                            --string_agg(DISTINCT class_group, ', ') class_group,
                            class_group,
                            exercise.id exercise_id,
                            exercise.title exercise_title,
                            tutorial.id tutorial_id,
                            tutorial_title,
                            tutorial_step.id tutorial_step_id,
                            tutorial_step.ordinal tutorial_step_ordinal,
                            tutorial_step.title tutorial_step_title,
                            question.id question_id,
                            LEFT(question_text, 64) question_text,
                            comment,
                            ROUND(rating / max_rating * 100) || '% (' || rating || '/' || max_rating || ')' rating,
                            feedback.ts_created :: timestamp(0) :: varchar
                        FROM feedback LEFT JOIN student
                            ON feedback.id_student = student.id 
                        LEFT JOIN student_course
                            ON student.id = student_course.id_student
                            AND student_course.id_course = $(id_course)
                            AND student_course.id_academic_year = $(id_academic_year)
                        LEFT JOIN exercise
                            ON feedback.id_exercise = exercise.id
                        LEFT JOIN exercise_student_question esq
                            ON feedback.exercise_question_ordinal = esq.ordinal
                            AND feedback.id_student = esq.id_student
                            AND feedback.id_exercise = esq.id_exercise
                            AND feedback.id_question = esq.id_question
                        LEFT JOIN tutorial
                            ON feedback.id_tutorial = tutorial.id
                        LEFT JOIN tutorial_step
                            ON feedback.id_tutorial_step = tutorial_step.id
                        LEFT JOIN question
                            ON feedback.id_question = question.id
                        WHERE feedback.id_academic_year = $(id_academic_year)
                            AND feedback.id_course = $(id_course)
                            ${filters.map((f) => `AND ${f}`).join(' ')}
                        /*GROUP BY feedback.id,
                            student_id, student_alt_id2, student_label, class_group,
                            exercise_id, exercise_title, tutorial_id, tutorial_title, 
                            tutorial_step_id, tutorial_step_ordinal, tutorial_step_title,
                            question_id, question_text, comment, rating
                        */`,
          {
            id_academic_year: req.session.currAcademicYearId,
            id_course: req.session.currCourseId,
            id_student: parseInt(req.query.studentId),
            id_exercise: parseInt(req.query.exerciseId),
            exercise_question_ordinal: parseInt(req.exerciseQuestionOrdinal),
            id_tutorial: parseInt(req.query.tutorialId),
            id_tutorial_step: parseInt(req.query.tutorialStepId),
            id_question: parseInt(req.query.questionId),
            rating: parseInt(req.query.rating),
          }
        ),
      ])
    );

    const exercises = data[0],
      tutorials = data[1],
      tutorialSteps = data[2],
      classGroups = data[3],
      students = data[4];
    let feedbacks = data[5];

    feedbacks.forEach((fb) => {
      if (fb.exercise_id) {
        fb.exercise = `
                    <a href="/exercise/def/${fb.exercise_id}/edit">${fb.exercise_id}: ${fb.exercise_title}</a>`;
      }
      if (fb.tutorial_id) {
        fb.tutorial = `
                    <a href="/tutorial/def/${fb.tutorial_id}">Tutorial ${fb.tutorial_id}: ${
          fb.tutorial_title
        }</a>${
          fb.tutorial_step_id
            ? `<br /><a href="/tutorial/def/${fb.tutorial_id}/step/${fb.tutorial_step_id}/edit">
                        Step #${fb.tutorial_step_ordinal}: ${fb.tutorial_step_title}`
            : ''
        }`;
      }
      if (fb.question_id) {
        fb.question = `
                    <a href="/question/edit/${fb.question_id}">${fb.question_id}</a>: 
                    ${fb.question_text}`;
      }
      fb.student = utils.getImageWithNameHtml(fb.student_alt_id2, fb.student_label);
    });

    res.appRender(
      'listFeedbacksView',
      {
        feedbacks: {
          headers: tableColumns,
          rows: feedbacks,
          buttons: [],
        },
        selectedInfo: selectedInfo,
        exercises: exercises,
        selectedExercises: selectedExercises,
        tutorials: tutorials,
        selectedTutorials: selectedTutorials,
        tutorialSteps: tutorialSteps,
        selectedTutorialSteps: selectedTutorialSteps,
        classGroups: classGroups,
        selectedClassGroups: selectedClassGroups,
        students: students,
        selectedStudents: selectedStudents,
      },
      'listFeedbacksViewScripts',
      'listFeedbacksViewCSS'
    );
  } catch (err) {
    req.flash('error', 'Failed to fetch feedbacks', JSON.stringify(err));
  }
});

// API routes

router.get(
  '/api',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let eid, eqo, tid, tsid;

    if (req.query.eid != 'undefined') {
      eid = parseInt(req.query.eid);
    }
    if (req.query.eqo != 'undefined') {
      eqo = parseInt(req.query.eqo);
    }
    if (req.query.tid != 'undefined') {
      tid = parseInt(req.query.tid);
    }
    if (req.query.tsid != 'undefined') {
      tsid = parseInt(req.query.tsid);
    }

    try {
      const feedback = await db.any(
        `SELECT id_exercise "exerciseId", 
                exercise_question_ordinal "exerciseQuestionOrdinal", 
                id_tutorial "tutorialId", id_tutorial_step "tutorialStepId", 
                    comment, rating, max_rating "maxRating", 
                    ts_created :: timestamp(0) :: varchar "timestamp"
                FROM feedback
                WHERE id_academic_year = $(id_academic_year)
                    AND id_course = $(id_course)
                    AND id_student = $(id_student)
                    ${eid ? 'AND id_exercise = $(eid)' : ''}
                    ${eqo ? 'AND exercise_question_ordinal = $(eqo)' : ''}
                    ${tid ? 'AND id_tutorial = $(tid)' : ''}
                    ${tsid ? 'AND id_tutorial_step = $(tsid)' : ''}
                ORDER BY ts_created DESC`,
        {
          id_academic_year: req.session.currAcademicYearId,
          id_course: req.session.currCourseId,
          id_student: req.session.studentId,
          eid: eid,
          eqo: eqo,
          tid: tid,
          tsid: tsid,
        }
      );

      if (feedback.length) {
        res.json(feedback);
      } else {
        res.status(404);
      }
    } catch (err) {
      winston.error('Failed to fetch feedback ' + JSON.stringify(err));
      res.status(500);
    }
  }
);

router.post(
  '/api',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    try {
      let questionId;

      if (req.body.exerciseId && req.body.exerciseQuestionOrdinal) {
        questionId = parseInt(
          (
            await db.one(
              `SELECT id_question
                    FROM exercise_student_question
                    WHERE id_exercise = $(id_exercise)
                        AND id_student = $(id_student)
                        AND ordinal = $(ordinal)`,
              {
                id_exercise: req.body.exerciseId,
                id_student: req.session.studentId,
                ordinal: req.body.exerciseQuestionOrdinal,
              }
            )
          ).id_question
        );
      } else if (req.body.tutorialStepId) {
        questionId = parseInt(
          (
            await db.one(
              `SELECT id_question
                    FROM tutorial_step
                    WHERE id = $(id_step)`,
              {
                id_step: req.body.tutorialStepId,
              }
            )
          ).id_question
        );
      }

      const resp = await db.one(
        `INSERT INTO public.feedback(
                    id_academic_year, id_course, id_student,
                    id_exercise, exercise_question_ordinal, id_tutorial, id_tutorial_step, id_question, comment, rating, max_rating)
            VALUES ($(id_academic_year), $(id_course), $(id_student), $(id_exercise), $(exercise_question_ordinal),
                $(id_tutorial), $(id_tutorial_step), $(id_question), $(comment), $(rating), $(max_rating))
                RETURNING id`,
        {
          id_academic_year: req.session.currAcademicYearId,
          id_course: req.session.currCourseId,
          id_student: req.session.studentId,
          id_question: questionId,
          id_exercise: req.body.exerciseId,
          exercise_question_ordinal: req.body.exerciseQuestionOrdinal,
          id_tutorial: req.body.tutorialId,
          id_tutorial_step: req.body.tutorialStepId,
          comment: req.body.comment,
          rating: req.body.rating,
          max_rating: req.body.maxRating,
        }
      );
      winston.debug(`Feedback for student ${req.session.studentId} saved with id ${resp.id}`);
      res.send({
        success: true,
        id: resp.id,
      });
    } catch (err) {
      winston.error(`Failed to save feedback: ${JSON.stringify(err)}`);
      res.status(500).send({
        success: false,
      });
    }
  }
);

module.exports = router;
