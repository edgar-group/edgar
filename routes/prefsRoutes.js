'use strict';

var express = require('express');
var router = express.Router();
var winston = require('winston');
var crypto = require('crypto');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
router.get(
  '/cmskin',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res) {
    if (req.session.showCodeMirror) {
      res.appRender('cmSkinView', {}, 'cmSkinViewScript', '~/shared/cmSkinViewCSS');
    } else {
      res.status(404).send('Course does not use code mirror.');
    }
  }
);

router.post(
  '/cmskin',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    if (req.session.showCodeMirror) {
      req.session.cmSkin = req.body.cmSkin;
      await AppUserSettings.saveProps(req.session.passport.user, {
        cmSkin: req.body.cmSkin,
      });
      req.flash('info', `Skin ${req.session.cmSkin} set.`);
      res.appRender('cmSkinView', {}, 'cmSkinViewScript', '~/shared/cmSkinViewCSS');
    } else {
      res.status(404).send('Course does not use code mirror.');
    }
  }
);

router.post(
  '/save/:paramName',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      if (
        req.params.paramName === 'markWhitespace' ||
        req.params.paramName === 'showQuestionHeader'
      ) {
        let paramValue = req.body.value || false;
        req.session.AppUserSettings = req.session.AppUserSettings || {};
        req.session.AppUserSettings[req.params.paramName] = paramValue;
        let mongoObj = {};
        mongoObj[req.params.paramName] = paramValue;
        await AppUserSettings.saveProps(req.session.passport.user, mongoObj);
      }
      res.json({
        success: true,
      });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: 'An error occured: ' + JSON.stringify(error) + '.',
        },
      });
    }
  }
);
module.exports = router;
