'use strict';
var express = require('express');
const { attach } = require('pg-monitor');
var router = express.Router();
var winston = require('winston');
const globals = require.main.require('./common/globals');
const minioService = require('../services/minioService');
var db = require('../db').db;

const appendSlash = (url) => {
  return url.startsWith('/') ? url : '/' + url;
};

let arrPath = `${globals.IMG_FOLDER}`.split('/');
if (arrPath[0] === '') arrPath.shift();
let minioBucketQuestionImages = arrPath[0];
arrPath.shift();
let minioPrefixQuestionImages = arrPath.join('/');

let toMount = appendSlash(`${globals.IMG_FOLDER}/:qid/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async function (req, res) {
  try {
    await minioService.streamObject(
      minioBucketQuestionImages,
      `${minioPrefixQuestionImages}/${req.params.qid}/${req.params.filename}`,
      res
    );
  } catch (error) {
    winston.error(error);
    return res.status(500).send(error);
  }
});

arrPath = `${globals.TUTORIALS_IMG_FOLDER}`.split('/');
if (arrPath[0] === '') arrPath.shift();
let minioBucketTutorialImages = arrPath[0];
arrPath.shift();
let minioPrefixTutorialImages = arrPath.join('/');
toMount = appendSlash(`${globals.TUTORIALS_IMG_FOLDER}/:tutid/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async function (req, res) {
  try {
    await minioService.streamObject(
      minioBucketTutorialImages,
      `${minioPrefixTutorialImages}/${req.params.tutid}/${req.params.filename}`,
      res
    );
  } catch (error) {
    winston.error(error);
    return res.status(500).send(error);
  }
});

toMount = appendSlash(`/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/:year?/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async function (req, res) {
  try {
    await minioService.streamObject(
      globals.PUBLIC_TI_DOWNLOAD_FOLDER,
      `${req.params.year ? req.params.year + '/' : ''}${req.params.filename}`,
      res
    );
  } catch (error) {
    winston.error(error);
    return res.status(500).send(error);
  }
});

toMount = appendSlash(`/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/resolved/:year?/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async function (req, res) {
  try {
    let attachment = await db.oneOrNone(
      `
                    SELECT last_name || '_' || first_name || '_' || coalesce(student.alt_id2, cast(student.id as text)) as filename
                      FROM test_instance_question
                      JOIN test_instance
                        ON test_instance_question.id_test_instance = test_instance.id
                      JOIN student
                        ON test_instance.id_student = student.id
                     WHERE uploaded_files = $(uploaded_files)

        `,
      {
        uploaded_files: req.params.year + '/' + req.params.filename,
      }
    );
    if (!attachment) {
      return res.sendStatus(404);
    }
    var newFileName = encodeURIComponent(attachment.filename + '_' + req.params.filename);
    res.setHeader('Content-Disposition', "attachment;filename*=UTF-8''" + newFileName);
    await minioService.streamObject(
      globals.PUBLIC_TI_DOWNLOAD_FOLDER,
      `${req.params.year ? req.params.year + '/' : ''}${req.params.filename}`,
      res
    );
  } catch (error) {
    winston.error(error);
    return res.status(500).send(error);
  }
});

arrPath = `${globals.IMG_FACES_FOLDER}`.split('/');
if (arrPath[0] === '') arrPath.shift();
let minioBucketFacesImages = arrPath[0];
arrPath.shift();
let minioPrefixFacesImages = arrPath.join('/');
toMount = appendSlash(`${globals.IMG_FACES_FOLDER}/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async function (req, res) {
  try {
    let path = `${minioPrefixFacesImages}/${req.params.filename}`;
    // winston.debug(`Getting from minio (${minioBucketFacesImages}, ${`${path}`})`);
    await minioService.streamObject(minioBucketFacesImages, `${path}`, res);
  } catch (error) {
    if (error.code === 'NoSuchKey') {
      return res.redirect('/images/unknown.png');
    } else {
      winston.error(error);
      return res.status(500).send(error);
    }
  }
});

let questionAttachmentsBucketName = globals.QUESTIONS_ATTACHMENTS_FOLDER.replaceAll('_', '');
toMount = appendSlash(`${globals.QUESTIONS_ATTACHMENTS_ROUTE.replaceAll('_', '')}/:filename`);
winston.info('Minio mounting ' + toMount);
router.get(toMount, async (req, res) => {
  try {
    let attachment = await db.oneOrNone(
      `
            SELECT filename, original_name
              FROM question_attachment
             WHERE filename = $(filename)
        `,
      {
        filename: req.params.filename,
      }
    );
    if (!attachment) {
      return res.sendStatus(404);
    }
    let downloadName = attachment.original_name.trim().split(new RegExp('\\s+')).join('_'); // + '.' + attachment.original_name.split('.').pop();
    winston.debug(
      `Getting from minio (${questionAttachmentsBucketName}, ${`${req.params.filename}`})`
    );
    res.set('Content-Disposition', 'attachment;filename=' + downloadName);
    await minioService.streamObject(questionAttachmentsBucketName, req.params.filename, res);
  } catch (err) {
    winston.error(err);
    res.sendStatus(500);
  }
});

module.exports = router;
