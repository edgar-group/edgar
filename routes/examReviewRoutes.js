// mounted at exam/run
'use strict';
var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var utils = require('../common/utils');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
var peerAssessmentService = require('../services/peerAssessmentService');
const Scorer = require('../lib/Scorer');
var testService = require.main.require('./services/testService');
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');
const RunningInstances = require('../common/runningInstances');
router.get(
  '/',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res) {
    res.redirect('/');
  }
);

// exam/review
router.get(
  '/:id_test_instance([0-9]{1,10})/:ordinal([0-9]{1,2})?',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),

  function (req, res) {
    const acceptsHTML = req.accepts('html');
    const acceptsJSON = req.accepts('json');

    const id_test_instance_rev = Number.parseInt(req.params.id_test_instance);
    db.func('chk_can_see_instance', [req.session.passport.user, id_test_instance_rev])
      .then(function (data) {
        if (data[0].can_see === true) {
          req.session.id_test_instance_rev = id_test_instance_rev;
          // req.session.show_solutions = data[0].show_solutions;

          RunningInstances.addReviewInstance(req, data[0].show_solutions);

          if (acceptsHTML) {
            res.publicRender('exam/review/examReviewView', {
              id_test_instance: id_test_instance_rev,
            }); // not appRender, bcs FullScreen
          } else if (acceptsJSON) {
            res.status(200).json({});
          } else {
            res.status(400).send();
          }
        } else {
          winston.error(
            `Security breach: user ${req.session.passport.user} is trying to access test_instance.id = ${id_test_instance_rev}`
          );
          res
            .status(403)
            .send(
              'Forbidden, you do not have permissions to access this test instance. Logged and noted!'
            );
        }
      })
      .catch(function (error) {
        winston.error(
          `An error occured while fetching test instance (req.session.passport.user=${req.session.passport.user}, id_test_instance_rev=${id_test_instance_rev}): ` +
            error.message
        );
        winston.error(error);
        res
          .status(500)
          .send(
            'An error occured while fetching test instance: ' +
              (process.env.NODE_ENV !== 'development')
              ? 'Logged at the server'
              : error.message || error
          );
      });
  }
);

router.get(
  '/instance',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    db.one(
      `SELECT ti.id AS id_test_instance,
                       title,
                       (SELECT COUNT(*) FROM test_instance_question WHERE id_test_instance = ti.id) AS questions_no,  -- bcs #PA2
                       (EXTRACT(EPOCH FROM (ts_started - ts_submitted)) + duration_seconds)::integer as seconds_left,
                       prolonged,
                       correct_no,
                       incorrect_no,
                       unanswered_no,
                       partial_no,
                       ti.score,
                       ti.score_perc,
                       ti.passed,
                       '[' || replace(replace(
                        (SELECT string_agg(outcome, ',') from (SELECT CASE WHEN (is_correct) THEN '"correct"'
                                                                           WHEN (is_incorrect) THEN '"incorrect"'
                                                                           WHEN (is_unanswered) THEN '"unanswered"'
                                                                           WHEN (is_partial) THEN '"partial"'
                                                                           ELSE '"incorrect"'
                                                                END as outcome
                                                                FROM test_instance_question
                                                                WHERE id_test_instance = ti.id order by ordinal) as x)
                        , '{', '[')
                        , '}', ']') || ']' as answers_outcome,
                        student.first_name || ' ' || last_name  as student,
                        coalesce(alt_id, '') as alt_id,
                        coalesce(alt_id2, '') as alt_id2,
                        show_solutions,
                        test_type.type_name,
                        course.uses_ticketing AND ticket_policy.uses_tickets_review as uses_ticketing
                  FROM test_instance ti
                  JOIN test
                    ON ti.id_test = test.id
                  JOIN course
                    ON test.id_course = course.id
                  JOIN test_type
                    ON test.id_test_type = test_type.id
                  JOIN student on ti.id_student =  student.id
                  JOIN ticket_policy
                    ON test.id_ticket_policy = ticket_policy.id
                 WHERE ti.id = $(id_test_instance)
                   AND ts_submitted IS NOT NULL
                   AND score IS NOT NULL`,
      {
        id_test_instance: req.session.id_test_instance_rev,
      }
    )
      .then(async function (test) {
        test.cmSkin = req.session.cmSkin;
        test.imgSrc = req.session.imgSrc;
        test.pa_no = 0;
        if (test.type_name === 'Peer assessment PHASE1') {
          let row = await db.one(
            `SELECT
                                COUNT(DISTINCT id_Student1) as no_reviews,
                                COUNT(DISTINCT ptgq.id_question) as no_review_questions
                            FROM v_peer_shuffle vps
                            JOIN peer_Test
                            on vps.id_test_phase_2 = peer_Test.id_test_phase_2
                            JOIN peer_Test_group ptg
                            ON ptg.id_peer_Test = peer_test.id
                            and ptg.id_question = vps.id_question_pa1
                            JOIN peer_Test_group_question ptgq
                            ON ptgq.id_peer_Test_group = ptg.id
                        WHERE id_test_instance2 =  $(id_test_instance)`,
            {
              id_test_instance: req.session.id_test_instance_rev,
            }
          );
          test.no_reviews = parseInt(row.no_reviews);
          test.no_review_questions = parseInt(row.no_review_questions);
        }
        res.json(utils.objectToCamelCase(test));
      })
      .catch(function (error) {
        winston.error(error);
        res.json({
          success: false,
          error:
            process.env.NODE_ENV !== 'development'
              ? 'Logged at the server'
              : error.message || error,
        });
      });
  }
);

// + retrive code to prepend / append to student answer
//  C gets executed with diff random inputs every time, and evaluated (calls : checkSingleCQuestionTI)
// returns evaluated data
router.post(
  '/exec/student/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      var ordinal = parseInt(req.params.ordinal);
      let currTI = await RunningInstances.getCurrIdTestInstanceRev(req);
      let q = await db.one(
        `SELECT    question.id,
                       student_answer_code,
                       student_answer_code_pl,
                       qt.type_name
                    FROM test_instance ti
                     JOIN test_instance_question tiq     ON ti.id             = tiq.id_test_instance
                     LEFT JOIN sql_question_answer sqlqa ON sqlqa.id_question = tiq.id_question
                     LEFT JOIN c_question_answer cqa     ON cqa.id_question   = tiq.id_question
                     JOIN question          ON question.id = tiq.id_question
                     JOIN question_type qt  ON question.id_question_type = qt.id
                    WHERE id_test_instance = $(id_test_instance)
                      AND ordinal = $(ordinal)
                    `,
        {
          id_test_instance: req.session.id_test_instance_rev,
          ordinal: ordinal,
        }
      );

      if (
        q.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
        q.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
        q.type_name.toUpperCase().indexOf('CODE') >= 0
      ) {
        // (id_test_instance, ordinal, code: string,
        let result = await Scorer.scoreQuestionAtRuntime(
          currTI,
          ordinal,
          q.student_answer_code,
          q.student_answer_code_pl
        );
        let j0 = result.j0;
        let codeResult = utils.judge0response2Html({
          resp: j0,
          score: result.score,
          showExpected: true,
          hideTestDetails: false,
        });
        delete result.score.c_outcome;
        res.json({
          success: true,
          codeResult,
          score: result.score,
        });
      } else if (q.type_name.toUpperCase().indexOf('SQL') >= 0) {
        res.json(
          await CodeRunnerService.execSQLCodeQuestion(
            q.id,
            q.student_answer_code,
            true,
            true // append presentation code!
          )
        );
      } else if (q.type_name.toUpperCase().indexOf('JSON') >= 0) {
        let result = await CodeRunnerService.execJSCodeQuestion(
          q.id,
          q.student_answer_code,
          true,
          true // append presentation code!
        );
        let rv = {
          success: true,
          codeResult: result.success
            ? `<PRE>${JSON.stringify(result.data, null, 2)}</PRE>`
            : `${result.error.message} Position: ${result.error.position})`,
          score: result.score,
        };
        res.json(rv);
      } else {
        throw 'Unknown question type.';
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: 'An error occured (more info @server).',
        },
      });
    }
  }
);
// + for C: if there is correct solution then it gets executed and results are returned (uses new random inputs)
//          else fixed outputs are returned
router.post(
  '/exec/correct/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      var ordinal = parseInt(req.params.ordinal);
      let currTI = await RunningInstances.getCurrIdTestInstanceRev(req);
      let q = await db.one(
        `SELECT question.id,
                           sql_answer,
                           c_source,
                           id_programming_language,
                           json_answer,
                           qt.type_name
                    FROM test_instance
                    JOIN test_instance_question ON test_instance.id = test_instance_question.id_test_instance
                    LEFT JOIN sql_question_answer ON sql_question_answer.id_question = test_instance_question.id_question
                    LEFT JOIN json_question_answer ON json_question_answer.id_question = test_instance_question.id_question
                    LEFT JOIN c_question_answer ON c_question_answer.id_question = test_instance_question.id_question
                    JOIN question ON question.id = test_instance_question.id_question
                    JOIN question_type qt  ON question.id_question_type = qt.id
                    WHERE test_instance.id = $(id_test_instance)
                      AND ordinal = $(ordinal)`,
        {
          id_test_instance: currTI, // req.session.id_test_instance_rev,
          ordinal: ordinal,
        }
      );

      if (
        q.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
        q.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
        q.type_name.toUpperCase().indexOf('CODE') >= 0
      ) {
        // Wwe'll send correct code as  code - this should always evaluate to is_correct
        // Otherwise, it might prove emarassing for the teacher :)
        let result = await Scorer.scoreQuestionNoExam(
          req.session.currCourseId,
          q.id,
          q.c_source,
          q.id_programming_language
        );
        let j0 = result.j0;
        let codeResult = utils.judge0response2Html({
          resp: j0,
          score: result.score,
          showExpected: true,
          hideTestDetails: false,
        });
        delete result.score.c_outcome;
        res.json({
          success: true,
          codeResult,
          score: result.score,
        });
      } else if (q.type_name.toUpperCase().indexOf('SQL') >= 0) {
        res.json(
          await CodeRunnerService.execSQLCodeQuestion(
            q.id,
            q.sql_answer,
            true,
            true // append presentation code!
          )
        );
      } else if (q.type_name.toUpperCase().indexOf('JSON') >= 0) {
        let result = await CodeRunnerService.execJSCodeQuestion(
          q.id,
          q.json_answer,
          true,
          true // append presentation code!
        );
        let rv = {
          success: true,
          codeResult: result.success
            ? `<PRE>${JSON.stringify(result.data, null, 2)}</PRE>`
            : `${result.error.message} Position: ${result.error.position})`,
          score: result.score,
        };
        res.json(rv);
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: 'An error occured (more info @server).',
        },
      });
    }
  }
);

router.get(
  '/question/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    var ordinal = parseInt(req.params.ordinal); // TODO: handle it?
    let currTI = await RunningInstances.getCurrIdTestInstanceRev(req);
    testService
      .getQuestion(ordinal, currTI, true, req.session.rolename === globals.ROLES.TEACHER)
      .then(async function (q) {
        q.studentAnswerCode = q.studentAnswerCode || '';
        if (q.type.toUpperCase().indexOf('JSON') >= 0) {
          q.c_eval_data = q.hint; // TODO: refactor this, both in C and JSON this should be renamted to logged_eval_data or similar
        }
        if (req.session.rolename === globals.ROLES.TEACHER) {
          q.content = `Id: <a href="/question/edit/${q.id}" target="_blank">${q.id}</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/question/instances/${q.id}" target="_blank">See instances (and accept?)</a><br>${q.content}`;
        } else {
          q.content = `Id: ${q.id}<br>${q.content}`;
        }
        if (
          q.correctAnswerCode &&
          !(await RunningInstances.getCurrTestInstanceRev(req)).show_solutions
        ) {
          q.correctAnswerCode = `This exam does not have "show solutions" option enabled.`;
        }
        // TODO: eventually, change this at the source...

        q.uploadedFiles = q.uploaded_files;
        delete q.uploaded_files;
        q.codeEvalData = q.c_eval_data;
        delete q.c_eval_data;

        res.json(q);
      })
      .catch(function (err) {
        winston.error(err);
        res.json({
          success: false,
          error: {
            message: 'An error occured (more info @server).',
          },
        });
      });
  }
);

router.get(
  '/peerquestions/:peerordinal([0-9]{1,2})/count/:count([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let count = parseInt(req.params.count);
    let peerOrdinal = parseInt(req.params.peerordinal);
    let currTI = await RunningInstances.getCurrIdTestInstanceRev(req);
    try {
      let instanceId = await db.oneOrNone(
        `SELECT ti.id
                                    FROM
                                        (SELECT dense_rank() OVER(
                                            ORDER BY id_Student1
                                        ) as rank, id_student1, id_pa_test
                                            FROM v_peer_shuffle
                                        WHERE id_test_instance2 = $(id_test_instance_rev)) reviews
                            JOIN test_instance ti
                            ON ti.id_test = reviews.id_pa_test
                            AND ti.id_student = reviews.id_student1
                            WHERE reviews.rank = $(peerOrdinal)
                            `,
        {
          peerOrdinal: peerOrdinal,
          id_test_instance_rev: currTI,
        }
      );
      if (instanceId && instanceId.id) {
        let promises = [];
        for (let ordinal = 1; ordinal <= count; ++ordinal) {
          promises.push(
            testService.getQuestion(
              ordinal,
              instanceId.id,
              true,
              req.session.rolename === globals.ROLES.TEACHER
            )
          );
        }
        let qs = await Promise.all(promises);
        for (let q of qs) {
          q.studentAnswerCode = q.studentAnswerCode || '';
        }
        res.json(qs);
      } else {
        // review pending
        res.json({
          success: false,
          error: {
            message: 'Review pending.',
          },
        });
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: 'An error occured (more info @server).',
        },
      });
    }
  }
);

router.get(
  '/peerattachment',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let currTI = await RunningInstances.getCurrIdTestInstanceRev(req);
      let attachment = await peerAssessmentService.getPaAttachment(currTI);
      if (attachment.uploaded_files) {
        res.redirect(`/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${attachment.uploaded_files}`);
      } else if (attachment.filename) {
        // Calibration question file - fake submission
        if (utils.useMinio()) {
          res.redirect(`/${globals.QUESTIONS_ATTACHMENTS_ROUTE}/${attachment.filename}`);
        } else {
          res.redirect(`/upload/attachment/${attachment.filename}`);
        }
      } else {
        res.status(404).send('Missing attachement. Now what?');
      }
    } catch (error) {
      winston.error(error);
      res.status(404).send('Error finding attachement? See server logs...');
    }
  }
);

// ********************
module.exports = router;
