'use strict';
var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var utils = require('../common/utils');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
const moment = require('moment');

// TODO: this is duplicted, dedupolicate
const getCurrTutorial = (req) => {
  let fallback = {
    currTutorialId: req.session.currTutorialId,
  };
  return req.query.idtut && req.session && req.session.running_tutorials
    ? req.session.running_tutorials.find((item) => {
        return item.currTutorialId == req.query.idtut;
      }) || fallback
    : fallback;
};
const getCurrTutorialId = (req) => {
  let rv = getCurrTutorial(req);
  rv = rv && rv.currTutorialId;
  return rv;
};

router.get(
  '/isresolved',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    winston.debug(
      `Tutorial ticket resolved?: ${moment().format('YYYY-MM-DD, h:mm:ss')} ${
        req.session.passport.user
      }`
    );
    let id_tutorial = getCurrTutorialId(req);
    if (id_tutorial) {
      let ticket = await db.oneOrNone(
        `SELECT tutorial_ticket_message.id, tutorial_step.ordinal
                    FROM tutorial_ticket_message
                    JOIN tutorial_ticket
                      ON tutorial_ticket_message.id_tutorial_ticket = tutorial_ticket.id
                    JOIN tutorial_step
                      ON tutorial_ticket.id_tutorial_step = tutorial_step.id
                   WHERE tutorial_ticket_message.ts_sent is null
                     AND tutorial_ticket.id_student  = $(id_student)
                     AND tutorial_step.id_tutorial = $(id_tutorial)
                   ORDER BY tutorial_ticket_message.ts_created
                   LIMIT 1`,
        {
          id_tutorial: id_tutorial,
          id_student: req.session.studentId,
        }
      );

      res.json({
        done: true,
        ticket_step_ordinal: ticket && ticket.ordinal,
      });

      if (ticket && ticket.id) {
        await db.none(
          `UPDATE tutorial_ticket_message
                        SET ts_sent = CURRENT_TIMESTAMP
                        WHERE id = $(id)`,
          {
            id: ticket.id,
          }
        );
      }
    } else {
      // Apparently, this CAN happen. Not entirely sure how, maybe when users leaves the test open for a looong time and the session expires?
      winston.error('Unknown id_tutorial, user: ' + req.session.passport.user);
      res.json({
        done: true,
        error: 'Session expired? Please login and reload your test.',
      });
    }
  }
);
router.post(
  '/raise',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let ticket = req.body;
      let id_tutorial = getCurrTutorialId(req);
      if (ticket.ordinal && ticket.description) {
        ticket.ts_created = new Date();
        //ticket.description = html.encode(ticket.description);
        let existing = await db.one(
          `SELECT tutorial_ticket.id, tutorial_step.id as id_tutorial_step
                                               FROM tutorial_step
                                          LEFT JOIN tutorial_ticket
                                                 ON tutorial_step.id = tutorial_ticket.id_tutorial_step
                                                AND id_student = $(id_student)
                                              WHERE id_tutorial = $(id_tutorial)
                                                AND ordinal = $(ordinal)
                                        `,
          {
            id_tutorial,
            ordinal: ticket.ordinal,
            id_student: req.session.studentId,
          }
        );
        let recAff;
        if (existing.id) {
          recAff = await db.result(
            `UPDATE tutorial_ticket
                                                 SET status = 'reopened',
                                                     comments = array_append(comments, $(entry)::json)
                                               WHERE id = $(id_tutorial_ticket)
                                               `,
            {
              id_tutorial_ticket: existing.id,
              entry: JSON.stringify(ticket),
            }
          );
        } else {
          recAff = await db.result(
            `INSERT INTO tutorial_ticket (id_tutorial_step, id_student, comments)
                                    VALUES (
                                         $(id_tutorial_step),
                                         $(id_student),
                                        array[$(entry)]::json[])
                                        `,
            {
              id_tutorial_step: existing.id_tutorial_step,
              id_student: req.session.studentId,
              entry: JSON.stringify(ticket),
            }
          );
        }
        res.json({
          success: recAff.rowCount === 1,
        });
      } else {
        winston.error('Invalid ticket payload:' + JSON.stringify(ticket));
        res.json({
          success: false,
          error: 'Ticket payload is invalid. Please provide the description of the issue.',
        });
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: JSON.stringify(error),
      });
    }
  }
);
router.get(
  '/statuses',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    let id_tutorial = getCurrTutorialId(req);
    try {
      let test = await db.one(
        `SELECT ARRAY_AGG(tutorial_ticket.status::text ORDER BY ordinal)  as ticket_statuses
                            FROM tutorial_step
                            LEFT JOIN tutorial_ticket
                                   ON id_student = $(id_student)
                                  AND tutorial_step.id = tutorial_ticket.id_tutorial_step
                           WHERE tutorial_step.id_tutorial = $(id_tutorial)
                  `,
        {
          id_tutorial,
          id_student: req.session.studentId,
        }
      );
      res.json({
        done: true,
        ticket_statuses: JSON.stringify(test.ticket_statuses),
      });
    } catch (error) {
      winston.error(
        'An error occured while fetching ticketstatuses (id_tutorial = ' +
          id_tutorial +
          '): ' +
          error.message
      );
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    }
  }
);
router.get(
  '/conversation/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let id_tutorial = getCurrTutorialId(req);
    try {
      let ticket = await db.any(
        `
                SELECT tutorial_ticket.comments
                  FROM tutorial_step
                  JOIN tutorial_ticket
                    ON tutorial_ticket.id_tutorial_step = tutorial_step.id
                WHERE id_tutorial = $(id_tutorial)
                  AND ordinal = $(ordinal)
                  AND id_student = $(id_student)
                `,
        {
          id_tutorial,
          id_student: req.session.studentId,
          ordinal: req.params.ordinal,
        }
      );
      if (ticket && ticket[0])
        res.json({
          success: true,
          conversation: utils.formatTicketConversation(ticket[0].comments),
        });
      else
        res.json({
          success: false,
          conversation: 'No comments.',
        });
    } catch (err) {
      winston.error(err);
      res.send('Error occured fetchin ticket conversation.');
    }
  }
);

// SERVER SIDE:

router.get(
  '/tutorial/:id_tutorial([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      const app_user = await db.one(
        `SELECT app_user.email as subscribe_email, tutorial_ticket_subscription.id as subscribed, tutorial_title
                FROM app_user
                JOIN tutorial
                  ON tutorial.id = $(id_tutorial)
                LEFT JOIN tutorial_ticket_subscription
                       ON tutorial_ticket_subscription.id_tutorial = tutorial.id
                      AND tutorial_ticket_subscription.id_app_user = $(id_app_user_email)
                WHERE app_user.id = $(id_app_user_email)
                `,
        {
          id_tutorial: req.params.id_tutorial,
          id_app_user_email: req.session.appUserId,
        }
      );
      let tickets = await db.any(
        `
                SELECT student.id,
                    student.first_name || ' ' || student.last_name as student,
                    student.alt_id,
                    student.alt_id2,
                    tutorial.tutorial_title,
                    tutorial_step.title as step_title,
                    tutorial_step.id as id_tutorial_step,
                    tutorial_step.ordinal,
                    tutorial_ticket.status,
                    tutorial_ticket.ts_created::timestamp(0)::varchar || ' / ' ||
                    tutorial_ticket.ts_modified::timestamp(0)::varchar as ts_created_modified,
                    substring(app_user.first_name from 1 for 1) || '. ' || app_user.last_name as assigned,
                    substring(user_created.first_name from 1 for 1) || '. ' || user_created.last_name || ' / ' ||
                    substring(user_modified.first_name from 1 for 1) || '. ' || user_modified.last_name as user_created_modified,
                    app_user.alt_id2 as assigned_alt_id2,
                    id_assigned_user,
                    tutorial_ticket.id as id_ticket,
                    tutorial_ticket.comments,
                    tutorial.id as id_tutorial
                  FROM tutorial
                  JOIN tutorial_step
                    ON tutorial_step.id_tutorial = tutorial.id
                  JOIN tutorial_ticket
                    ON tutorial_ticket.id_tutorial_step = tutorial_step.id
                  JOIN student
                    ON tutorial_ticket.id_student = student.id
                  JOIN app_user user_created
                    ON tutorial_step.id_user_created = user_created.id
                  JOIN app_user user_modified
                    ON tutorial_step.id_user_modified = user_modified.id
                LEFT JOIN app_user
                       ON tutorial_ticket.id_assigned_user = app_user.id
                where tutorial_step.id_tutorial = $(id_tutorial)
                ORDER BY tutorial_ticket.status, tutorial_ticket.ts_modified DESC `,
        {
          id_tutorial: req.params.id_tutorial,
        }
      );
      let open = tickets.filter((x) => x.status.indexOf('open') >= 0).length;
      res.appRender('tutorialTicketsView', {
        id_tutorial: req.params.id_tutorial,
        autorefresh: req.query.ar,
        subscribe_email: app_user.subscribe_email,
        subscribed: app_user.subscribed,
        title: `Tickets for tutorial "${app_user.tutorial_title}": `,
        pageTitle: `Tickets: ${open}/${tickets.length}`,
        list: {
          headers: [
            {
              id: 'stud.Id',
            },
            {
              image: 'image',
              __raw: true,
              __formatter: function (row) {
                return utils.getTinyImageHtml(row.alt_id2);
              },
            },
            {
              student: 'Student',
            },
            {
              __raw: true,
              ts_created_modified: 'Ticket cr/mod',
              __formatter: function (row) {
                return row.ts_created_modified.replace('/', '<br>');
              },
            },
            {
              __raw: true,
              user_created_modified: 'Question cr/mod',
              __formatter: function (row) {
                return row.user_created_modified.replace('/', '<br>');
              },
            },
            {
              ordinal: 'Ordinal',
            },
            {
              step_title: 'Step title',
            },
          ],
          rows: tickets,
          rawhtml: [
            {
              label: 'Description',
              __html: function (row) {
                let buff = Buffer.from(utils.formatTicketConversation(row.comments), 'utf-8');
                return `<a href="#" data-toggle="popover"
                                    title="Ticket conversation" data-content2="${buff.toString(
                                      'base64'
                                    )}"><h2>
                                    <i class="fa fa-comment-dots"></h2></i></a>`;
              },
            },
            {
              label: '*********** Handle ticket here ***********',
              __html: function (row) {
                if (
                  row.id_assigned_user === req.session.appUserId &&
                  row.status.indexOf('open') >= 0
                ) {
                  return `<form action="/tutorial/ticket/close/${row.id_ticket}" method="POST">
                                            <textarea class="form-control" style="min-width:500px;"
                                                name="comment"
                                                rows = "3"
                                                placeholder="Provide clear reply here. Use plain text."></textarea>
                                            <button edgar_confirm_text = "Are you sure?"
                                                class="btn btn-primary"
                                                type="submit"> Close ticket
                                            </button>
                                        </form>`;
                } else {
                  return '';
                }
              },
            },
            {
              label: 'Status',
              __html: function (row) {
                let badgeClass = '';
                if (row.status === 'open') {
                  badgeClass = row.assigned ? 'badge-warning' : 'badge-danger';
                } else if (row.status === 'reopened') {
                  badgeClass = 'badge-warning';
                } else if (row.status === 'closed') {
                  badgeClass = 'badge-success';
                } else {
                  badgeClass = 'badge-info';
                  row.status = '?? ' + row.status;
                }
                return `<h3><span class="badge ${badgeClass}">${row.status}</span></h3>`;
              },
            },
            {
              label: 'Assigned',
              __html: function (row) {
                if (row.assigned) {
                  return utils.getTinyImageHtml(row.assigned_alt_id2) + '&nbsp;' + row.assigned;
                } else {
                  return `<form action="/tutorial/ticket/assign/${row.id_ticket}" method="POST">
                                                <button edgar_confirm_text = "Are you sure?<br>Cannot reassign later."
                                                    class="btn btn-outline-danger"
                                                    type="submit"> Assign to me
                                                </button>
                                            </form>`;
                }
              },
            },
          ],
          links: [
            {
              __formatter: function (row) {
                return row.ordinal;
              },
              target: '_blank',
              label: 'Edit step',
              href: function (row) {
                return `/tutorial/def/${row.id_tutorial}/step/${row.ordinal}/edit`;
              },
            },
          ],
        },
      });
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('back');
    }
  }
);
router.post(
  '/close/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      try {
        let ticket = {
          ts_created: new Date(),
          reply: req.body.comment,
        };

        let recAff = await db.result(
          `
                                    BEGIN WORK;
                                        UPDATE tutorial_ticket
                                             SET status = 'closed',
                                                 comments = array_append(comments, $(entry)::json)
                                        WHERE id = $(id_ticket)
                                          AND id_assigned_user = $(id_user);
                                        ---
                                        INSERT INTO tutorial_ticket_message(id_tutorial_ticket)
                                        SELECT id
                                          FROM tutorial_ticket
                                         WHERE tutorial_ticket.id = $(id_ticket)
                                           AND status = 'closed';
                                    COMMIT WORK;`,
          {
            id_user: req.session.appUserId,
            id_ticket: req.params.id_ticket,
            entry: JSON.stringify(ticket),
          }
        );

        if (recAff.rowCount === 0) {
          req.flash('info', 'CLosing failed.');
        } else {
          req.flash('info', 'Ticket closed.');
        }
      } catch (error) {
        req.flash('error', `${error} <br/> ${JSON.stringify(error)}`);
        winston.error(error);
      }
      res.redirect('back');
    }
  }
);
router.post(
  '/assign/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      try {
        let recAff = await db.result(
          `UPDATE tutorial_ticket
                                      SET id_assigned_user = $(id_user)
                                    WHERE id = $(id_ticket)
                                      AND id_assigned_user IS NULL
                                        `,
          {
            id_user: req.session.appUserId,
            id_ticket: req.params.id_ticket,
          }
        );
        if (recAff.rowCount === 0) {
          req.flash('info', 'Somebody already took that ticket before you.');
        } else {
          req.flash('info', 'Ticket assigned.');
        }
      } catch (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
      }
      res.redirect('back');
    }
  }
);

router.post(
  '/togglesubscription/:id_tutorial([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let status = await toggleSubscription(req.params.id_tutorial, req.session.appUserId, true);
      req.flash('info', status);
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
    }
    res.redirect('back');
  }
);

async function toggleSubscription(id_tutorial, id_app_user) {
  let recAff = await db.result(
    `DELETE FROM tutorial_ticket_subscription
                                        WHERE id_tutorial = $(id_tutorial)
                                        AND id_app_user = $(id_app_user)
                                            `,
    {
      id_app_user,
      id_tutorial,
    }
  );
  // console.log('recAff', recAff);
  if (recAff && recAff.rowCount > 0) {
    return 'Unsubscribed.';
  } else {
    recAff = await db.result(
      `INSERT INTO tutorial_ticket_subscription (id_tutorial, id_app_user)
                                        VALUES($(id_tutorial), $(id_app_user));
                                            `,
      {
        id_app_user,
        id_tutorial,
      }
    );
    return 'Subscribed';
  }
}

// *********************

module.exports = router;
