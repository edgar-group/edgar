'use strict';
var express = require('express');
var router = express.Router();

router.get('/q', function (req, res) {
  res.redirect('/lecture/enter');
});

module.exports = router;
