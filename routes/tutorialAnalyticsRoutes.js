'use strict';
const express = require('express');
const router = express.Router();
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const utils = require('../common/utils');
const db = require('../db').db;
const winston = require('winston');
const TutorialProgress = require.main.require('./models/TutorialProgressModel');

const tutorialAnalyticsService = require('../services/tutorialAnalyticsService');

router.get(
  '/:id_tutorial([0-9]{1,10})/groups',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await tutorialAnalyticsService.byGroups(
      parseInt(req.params.id_tutorial),
      req.session.currCourseId,
      req.session.currAcademicYearId
    );

    const chartData = data.data.map((row) =>
      Object.entries(row)
        .map(([key, value]) => (!isNaN(key) ? parseInt(key) : null))
        .filter((x) => x != null)
    );

    res.appRender(
      'groupsView',
      {
        analytics: {
          headers: [
            {
              class_group: 'Class group',
            },
            {
              started: 'Students started',
            },
            {
              finished: 'Students finished',
            },
            ...data.ordinals.map((o) => ({
              [o.ordinal]: o.ordinal,
            })),
          ],
          rows: data.data,
        },
        chartData: chartData,
      },
      'groupsViewScript',
      'groupsViewCSS'
    );
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/students',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res) => {
    const students = await db.any(
      `select id_student, class_group, alt_id2, (first_name || ' ' || last_name || '<br />(' || alt_id2 || ')') as student
         from student_course
         join student
           on student_course.id_student = student.id
        where id_course = ${req.session.currCourseId}
          and id_academic_year = ${req.session.currAcademicYearId}`
    );
    const tutorial = await db.one(
      `select id_tutorial, count(*) as steps
         from tutorial
         join tutorial_step on tutorial.id = tutorial_step.id_tutorial
         where tutorial.id = ${parseInt(req.params.id_tutorial)}
        group by id_tutorial`
    );
    let usage = await TutorialProgress.getUsageReport(
      req.session.currCourseId,
      parseInt(req.params.id_tutorial),
      students.map((x) => x.id_student)
    );

    const altIdLookup = students.reduce((acc, s) => {
      acc[s.id_student] = s;
      return acc;
    }, {});
    usage = usage.map((u) => {
      return {
        ...u,
        ...u.steps,
        ...altIdLookup[u.studentId],
      };
    });
    const stepsArray = Array.from({ length: tutorial.steps }, (_, i) => i + 1).map((i) => {
      return {
        [i]: `Step ${i}`,
        __raw: true,
        __formatter: function (row) {
          const stepStats = row[`${i}`] || { count: 0, totalMinutes: 0 };
          return stepStats.count
            ? `<span title="times opened, and mins spent"># ${
                stepStats.count
              }<br>⏱️ ${stepStats.totalMinutes.toFixed(1)} mins</span>`
            : '';
        },
      };
    });
    res.appRender('tutorialUsageView', {
      analytics: {
        headers: [
          {
            image: 'Image',
            __raw: true,
            __formatter: function (row) {
              return utils.getTinyImageHtml(row.alt_id2);
            },
          },
          {
            student: 'Student',
            __raw: true,
          },
          {
            class_group: 'Class group',
          },
          {
            totalMinutes: 'Total minutes',
            __formatter: function (row) {
              return row.totalMinutes.toFixed(1);
            },
          },
          {
            totalReasonableMinutes: 'Total reasonable minutes',
            __formatter: function (row) {
              return row.totalReasonableMinutes.toFixed(1);
            },
          },
          {
            loadCount: 'Load count',
          },
          ...stepsArray,
        ],
        rows: usage,
        links: [
          {
            label: 'Log',
            target: '_blank',
            href: (row) => `student/${row.id_student}/log`,
          },
        ],
      },
    });
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/student/:id_student([0-9]{1,10})/log',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    const data = await tutorialAnalyticsService.studentLog(
      parseInt(req.params.id_tutorial),
      req.session.currCourseId,
      parseInt(req.params.id_student)
    );

    res.appRender('studentLogView', {
      events: data.events,
    });
  }
);

module.exports = router;
