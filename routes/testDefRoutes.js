'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var utils = require('../common/utils');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
var globals = require.main.require('./common/globals');

const mongoose = require('mongoose');
var testService = require.main.require('./services/testService');
var testDefinitionValidityService = require.main.require(
  './services/testDefinitionValidityService'
);

router.post(
  '/testgen',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    var id_test_instance;
    try {
      let data = await db.func('chk_get_test_instance', [
        req.session.studentId,
        req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        req.body.password,
        req.session.currCourseId,
      ]);
      if (data[0].err_code === 0) {
        id_test_instance = data[0].id_test_instance;
        let qs = await testService.getTestInstancePreviewModel(id_test_instance);
        await db.func('del_test_instance', [id_test_instance]);
        // await db.result(
        //     `DELETE FROM test_correction
        //                 WHERE id_test_instance_question IN
        //                     (SELECT id FROM test_instance_question WHERE id_test_instance = $(id_test_instance));
        //             DELETE FROM test_instance_question_run
        //                 WHERE id_test_instance_question IN
        //                     (SELECT id FROM test_instance_question WHERE id_test_instance = $(id_test_instance));
        //             DELETE FROM test_instance_question
        //                 WHERE id_test_instance = $(id_test_instance);

        //             DELETE FROM test_instance
        //                 WHERE id = $(id_test_instance);`, {
        //         id_test_instance: id_test_instance
        //     });
        // break out of Layout - "full screen"
        res.fullScreenRender('~/shared/dumpTestView', {
          qs: qs,
        });
      } else {
        req.flash('error', 'chk_get_test_instance failed:' + data[0].comment);
        res.redirect('/test/def/edit/' + req.body.id_test);
      }
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('/test/def/edit/' + req.body.id_test);
    }
  }
);

router.get('/list', middleware.requireRole(globals.ROLES.TEACHER), async function (req, res, next) {
  try {
    let test_types = await db.any(`SELECT id as value,
                                type_name as name
                            FROM test_type
                            WHERE standalone`);
    let data = await db.any(
      `SELECT test.*, left(password, 3) || '***' as spassword,
                                (ts_available_from :: timestamp) :: varchar as ts_from,
                                (ts_available_to :: timestamp) :: varchar as ts_to,
                                test_type.type_name,
                                (first_name || ' ' || last_name) as user_created,
                                CASE WHEN is_global THEN 'GLBL' ELSE '-' END
                                || CASE WHEN is_public THEN ' PBLC' ELSE '-' END
                                || CASE WHEN use_in_stats THEN ' STATS' ELSE '-' END
                                || CASE WHEN test_score_ignored THEN ' SCIGNORED' ELSE '-' END as test_flags
                        FROM test
                        JOIN test_type
                            ON test.id_test_type = test_type.id
                        AND type_name <> 'Lecture quiz'
                        JOIN app_user
                            ON test.id_user_created = app_user.id
                        WHERE id_course = $(cId)
                        AND id_academic_year = $(yId)
                    ORDER BY test_ordinal`,
      {
        cId: req.session.currCourseId,
        yId: req.session.currAcademicYearId,
      }
    );

    res.appRender('testDefsView', {
      pageTitle: 'Exam defs',
      test_types: test_types,
      tests: {
        headers: [
          {
            id: 'Id',
          },
          {
            title: 'Title',
          },
          {
            user_created: 'Created',
          },
          {
            type_name: 'Test type',
          },
          {
            test_ordinal: 'Test ordinal',
          },
          {
            title_abbrev: 'Abbrev.',
          },
          {
            max_runs: 'Max runs (0=∞)',
          },
          {
            test_flags: 'Global Public UseStats ScoreIgnored',
          },
          {
            show_solution: 'Show solutions',
          },
          {
            max_score: 'Max score',
          },
          {
            spassword: 'Password',
          },
          {
            questions_no: 'No of qs',
          },
          {
            duration_seconds: 'Duration (s)',
          },
          {
            ts_from: 'Valid from',
          },
          {
            ts_to: 'Valid to',
          },
        ],
        rows: data,
        buttons: [
          {
            label: 'Delete',
            method: 'POST',
            action: function (row) {
              return '/test/def/delete/' + row['id'];
            },
            class: function (row) {
              return 'btn btn-outline-danger';
            },
            props: {
              edgar_confirm_text: 'Do you really want to delete this test?',
            },
          },
        ],
        links: [
          {
            label: 'Edit',
            href: function (row) {
              return '/test/def/edit/' + row['id'];
            },
          },
        ],
      },
    });
  } catch (error) {
    winston.error(error);
    req.flash('error', JSON.stringify(error));
    res.redirect('/');
  }
});

router.post('/new', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  if (req.body.id_test_type) {
    // TODO: validate args
    db.func('new_test', [
      req.body.id_test_type,
      'Generated, please update...',
      req.session.currCourseId,
      req.session.currAcademicYearId,
      req.session.appUserId,
    ])
      .then(function (data) {
        winston.debug('new_test returned: ' + data);
        req.flash('info', 'New test created, please edit.');
        res.redirect('/test/def/edit/' + data[0].new_test);
      })
      .catch(function (error) {
        winston.error(error);
        res.errRender(error);
      });
  } else {
    req.flash('error', 'Test type NOT set.');
    res.redirect('/test/def/new');
  }
});

router.post(
  '/delete/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test) {
      db.func('del_test', [req.params.id_test])
        .then(function (data) {
          winston.debug('del_test returned: ' + data);
          req.flash('info', 'Test deleted.');
          res.redirect('/test/def/list');
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect('/test/def/list');
        });
    } else {
      req.flash('error', 'Test id NOT set.');
      res.redirect('/test/def/list');
    }
  }
);

var getTestParts = function (id_test) {
  return db.any(
    `SELECT    test_part.id,
                       test_part.ordinal,
                       test_part.id_test,
                       test_part.id_node,
                       test_part.id_grading_model,
                       test_part.id_question_type,
                       test_part.min_questions,
                       test_part.max_questions,
                       test_part.pass_percentage,
                       test_part.ts_created,
                       node.node_name,
                       node_type.type_name,
                       count(question.id) as cnt_active_qs
                FROM test_part
                JOIN node ON test_part.id_node = node.id
                JOIN node_type ON node.id_node_type = node_type.id
                LEFT JOIN question_node ON question_node.id_node = node.id
                LEFT JOIN question ON question_node.id_question = question.id
                AND question.is_active
                WHERE id_test =  $(tId)
                GROUP BY test_part.id,
                         test_part.ordinal,
                         test_part.id_test,
                         test_part.id_node,
                         test_part.id_grading_model,
                         test_part.id_question_type,
                         test_part.min_questions,
                         test_part.max_questions,
                         test_part.pass_percentage,
                         test_part.ts_created,
                         node.node_name,
                         node_type.type_name
               ORDER BY test_part.ordinal, ts_created;
               `,
    {
      tId: id_test,
    }
  );
};
router.get(
  '/testparts/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var tps;
    getTestParts(req.params.id_test)
      .then(function (test_part) {
        tps = test_part;
        return db.many(
          `select id as value, model_name as name From grading_model order by correct_Score`
        );
      })
      .then(function (grading_model) {
        res.publicRender('partials/testParts', {
          // TODO
          grading_model: grading_model,
          test_part: tps,
        });
      })
      .catch(function (error) {
        winston.error(
          new errors.DbError('An error occured while fetching test parts: ' + error.message)
        );
        res.status(500).send('An error occured while fetching test parts.');
      });
  }
);
router.delete(
  '/testparts/:id_test_part([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.result(
      `DELETE FROM test_part
        WHERE id = $(id_test_part)`,
      {
        id_test_part: req.params.id_test_part,
      }
    )
      .then(function (result) {
        winston.debug('delete test part returned: ' + result);
        req.flash('info', result.rowCount + ' row(s) deleted.');
        res.status(200).send();
      })
      .catch(function (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
        return next(
          new errors.DbError('An error occured while deleting test part: ' + error.message)
        );
      });
  }
);

// var getPeerTestQuestions = function(id_test) {
//     return db.any(
//             `SELECT    peer_test_group_question.id,
//                        peer_test.id_test_phase_2 as id_test,
//                        peer_test_group_question.id_question,
//                        peer_test_group_question.ordinal,
//                        peer_test_group_question.id_grading_model,
//                        question.question_text,
//                        question.id_question_type,
//                        question_type.type_name
//                 FROM peer_test_group_question
//                 JOIN peer_test_group
//                   ON peer_test_group_question.id_peer_test_group = peer_test_group.id
//                 JOIN peer_test
//                   ON peer_test_group.id_peer_test = peer_test.id
//                  AND id_test_phase_2 = $(tId)
//                 JOIN question ON peer_test_group_question.id_question = question.id
//                 JOIN question_type ON question.id_question_type = question_type.id
//                 ORDER BY peer_test_group_question.ordinal`, {
//             tId: id_test
//         });
// };
var getTestQuestions = function (id_test) {
  return db.any(
    `SELECT    test_question.id,
                test_question.id_test,
                test_question.id_question,
                test_question.ordinal,
                test_question.id_grading_model,
                test_question.id_question,
                question.question_text,
                question.id_question_type,
                question_type.type_name
        FROM test_question
        JOIN question ON test_question.id_question = question.id
        JOIN test ON test_question.id_test = test.id
        JOIN question_type ON question.id_question_type = question_type.id
        WHERE id_test =  $(tId)
        ORDER BY test_question.ordinal`,
    {
      tId: id_test,
    }
  );
};
let getNodeName = function (req) {
  return db.many(
    `SELECT null::int as value, '-- please select --' as node_name,  '-- please select --' as name UNION ALL
         SELECT DISTINCT node.id as value, node_name, '[' || type_name || '] ' || node_name as name
            FROM v_roots_children join node on id_child = node.id
            JOIN node_type on node.id_node_type = node_type.id
            WHERE id_root = (select id_root_node from course where id = $(id_course))
            ORDER BY name;`,
    {
      id_course: req.session.currCourseId,
    }
  );
};
let getTest = function (req) {
  return db.oneOrNone(
    ` select test.*,
        test.ts_created::timestamp(0)::varchar,
        test.ts_modified::timestamp(0)::varchar,
        to_char(test.ts_available_to, 'YYYY-MM-DD HH24:MI:SS') as ts_available_to,
        to_char(test.ts_available_from, 'YYYY-MM-DD HH24:MI:SS') as ts_available_from,
        type_name, fixed_questions, first_name, last_name,
        peer_test.id_test_phase_1,
        peer_test.id_test_phase_2,
        peer_test.assessments_no,
        peer_test.calib_assessments_no,
        test_pace.id_pace_test,
        test_pace.id_pace_question
    FROM test
    JOIN test_type
        ON test.id_test_type = test_type.id
    JOIN app_user
        ON test.id_user_created = app_user.id
    LEFT JOIN peer_test
        ON test.id = peer_test.id_test_phase_2
    LEFT JOIN test_pace
        ON test.id = test_pace.id_test
    WHERE id_course = $(cId)
    AND id_academic_year = $(yId)
    AND test.id = $(tId)
    ORDER BY test_ordinal`,
    {
      cId: req.session.currCourseId,
      yId: req.session.currAcademicYearId,
      tId: req.params.id_test,
    }
  );
};

var getPeerTests = function (req) {
  return db.many(
    `SELECT null::int as value, '-- please select --' as name UNION ALL
            SELECT test.id as value, title as name
               FROM test
               JOIN test_type ON test.id_test_type = test_type.id
              WHERE type_name = 'Peer assessment PHASE1'
                AND id_course = $(cId)
                AND id_academic_year = $(yId)
           ORDER BY name`,
    {
      cId: req.session.currCourseId,
      yId: req.session.currAcademicYearId,
    }
  );
};
var getPeerTestQuestions = function (id_test) {
  return db.any(
    `SELECT peer_test_group_question.id,
                peer_test_group_question.id_peer_test_group,
                peer_test_group_question.id_question,
                peer_test_group_question.ordinal,
                peer_test_group_question.id_grading_model,
                question.question_text,
                question.id_question_type,
                question_type.type_name
            FROM peer_test_group_question
            JOIN peer_test_group
              ON peer_test_group_question.id_peer_test_group = peer_test_group.id
            JOIN peer_test
              ON peer_test_group.id_peer_test = peer_test.id
            JOIN question ON peer_test_group_question.id_question = question.id
            JOIN question_type ON question.id_question_type = question_type.id
                WHERE id_test_phase_2 = $(id_test)
                ORDER BY peer_test_group.id_question, peer_test_group_question.ordinal`,
    {
      id_test: id_test,
    }
  );
};
var getPeerTestGroups = function (id_test) {
  return db.any(
    `SELECT peer_test_group.id,
                    peer_test_group.id_question,
                    peer_test_group.id_calib_assessments_node,
                    SUBSTRING (question.question_text, 1, 500) || '...' as question_text
               FROM peer_test_group
               JOIN peer_test
                 ON peer_test_group.id_peer_test = peer_test.id
               JOIN question
                 ON peer_test_group.id_question = question.id
              WHERE id_test_phase_2 = $(tId)
           ORDER BY id_question`,
    {
      tId: id_test,
    }
  );
};
router.get(
  '/peertestquestions/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      res.publicRender('test/def/editPeerPhase2DefView1', {
        grading_model: await db.many(
          `select id as value, model_name as name From grading_model order by correct_Score`
        ),
        peer_tests: await getPeerTests(req),
        peer_test_groups: await getPeerTestGroups(req.params.id_test),
        peer_test_group_questions: await getPeerTestQuestions(req.params.id_test),
        test: getTest(req),
        node_name: await getNodeName(req),
      });
    } catch (error) {
      winston.error(error);
      res.status(500).send('An error occured while fetching peer test questions.');
    }
  }
);
router.get(
  '/testquestions/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var qs;
    var gradingModel;
    getTestQuestions(req.params.id_test)
      .then(function (test_question) {
        qs = test_question;
        return db.many(
          `select id as value, model_name as name From grading_model order by correct_score`
        );
      })
      .then(function (grading_model) {
        gradingModel = grading_model;
        res.publicRender('partials/testQuestions', {
          grading_model: gradingModel,
          test_question: qs,
        });
      })
      .catch(function (error) {
        winston.error(
          new errors.DbError('An error occured while fetching test questions: ' + error.message)
        );
        res.status(500).send('An error occured while fetching test questions.');
      });
  }
);
router.delete(
  '/testquestions/:id_test_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.func(`delete_test_question`, [req.params.id_test_question])
      .then(function (result) {
        winston.debug('delete test question returned: ' + result);
        req.flash('info', result.rowCount + ' row(s) deleted.');
        res.status(200).send();
      })
      .catch(function (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
        return next(
          new errors.DbError('An error occured while deleting test question: ' + error.message)
        );
      });
  }
);
router.delete(
  '/peertestquestions/:id_peer_test_group_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.func(`delete_peer_test_question`, [req.params.id_peer_test_group_question])
      .then(function (result) {
        winston.debug('delete peer test question returned: ' + result);
        req.flash('info', result.rowCount + ' row(s) deleted.');
        res.status(200).send();
      })
      .catch(function (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
        return next(error);
      });
  }
);

router.get(
  '/node/list',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var where = '';
    where += req.query.id_node_type ? ' AND id_node_type = $(id_node_type) ' : '';
    where += req.query.node_name ? " AND lower(node_name) LIKE '%${filter#}%'" : '';
    db.any(
      `select node.id, node_name, type_name
                from node
                join node_type on node.id_node_type = node_type.id
                where node.id in
                        (selecT id_child from v_roots_children where id_root = (select id_root_node from course where id = $(id_course)))
                        ${where}`,
      {
        id_course: req.session.currCourseId,
        id_node_type: req.query.id_node_type,
        filter: req.query.node_name.toLowerCase(),
      }
    )
      .then(function (nodes) {
        res.publicRender('partials/nodes', {
          // TODO
          nodes: nodes,
        });
      })
      .catch(function (error) {
        winston.error(new errors.DbError('An error occured while fetching nodes ' + error.message));
        res.status(500).send('An error occured while fetching test parts.');
      });
  }
);

router.get(
  '/question/list',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let ttype = await db.one(
        `SELECT type_name
                FROM test
                JOIN test_type
                ON test.id_test_type = test_type.id
                WHERE test.id = $(id_test)`,
        {
          id_test: req.query.id_test,
        }
      );
      var where = ` question.id NOT IN (
                SELECT id_question FROM test_question WHERE id_test = $(id_test)
                UNION ALL
                SELECT ptgq.id_question FROM peer_test_group_question ptgq
                        JOIN peer_test_group ptg
                          ON ptgq.id_peer_test_group = ptg.id
                        JOIN peer_test
                          ON ptg.id_peer_test = peer_test.id
                       WHERE id_test_phase_2 = $(id_test)
                )`; // assumed that users will not reuse questions from one tab (group) to another
      where += req.query.id_node ? ' AND id_node = $(id_node) ' : '';
      where += req.query.id_node && req.query.question_text ? ' AND ' : '';
      where += req.query.question_text ? " lower(question_text) LIKE '%${filter#}%'" : '';
      let questions = await db.any(
        `select question.id, question_text, type_name
                    from question
                    join question_node on question.id = question_node.id_question
                    join question_type on question.id_question_type = question_type.id
                    where ${where}`,
        {
          id_course: req.session.currCourseId,
          id_node: req.query.id_node,
          id_test: req.query.id_test,
          filter: req.query.question_text.toLowerCase(),
        }
      );
      if (ttype.type_name === 'Peer assessment PHASE2 MASTER') {
        res.publicRender('partials/peerQuestions', {
          questions: questions,
        });
      } else {
        res.publicRender('partials/questions', {
          questions: questions,
        });
      }
    } catch (error) {
      winston.error(error);
      res.status(500).send('An error occured while fetching questions.');
    }
  }
);
router.get(
  '/shuffle/:id_test([0-9]{1,10})/:mode([0-1]{1,1})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let mode = parseInt(req.params.mode);
      let peer_test = await db.one(
        `SELECT
                        id,
                        id_test_phase_1,
                        id_test_phase_2,
                        assessments_no,
                        calib_assessments_no
                    FROM peer_test
                   WHERE id_test_phase_2 = $(id_test)`,
        {
          id_test: req.params.id_test,
        }
      );
      // let orderby = 'random()';
      // if (mode === 0) { // keep groups -> groups are distinct question ids
      //     orderby = 'test_instance_question.id_question, ' + orderby; // keep them binned
      // }
      let ids = await db.any(
        `SELECT test_instance_question.id, test_instance_question.id_question
                    FROM test_instance_question
                    JOIN test_instance
                      ON test_instance.id = test_instance_question.id_test_instance
                    JOIN student
                      ON test_instance.id_student = student.id
                   WHERE id_test = $(id_test)
                     AND test_instance_question.ordinal = 1
                     AND student.id_app_user is null
                     AND ts_submitted is not null
                     ORDER BY random()`,
        {
          id_test: peer_test.id_test_phase_1,
        }
      );
      let qs = peer_test.assessments_no - peer_test.calib_assessments_no;
      // Actually, this should be done on per student basis.
      // However, I don't expect such use cases, and I don't want to slow things down
      // Does it make sense to have a pool of calibration questions and give different ones?
      // console.log('ids.length = ', ids.length);
      let calibIds, rvShuffle;
      if (mode === 0) {
        rvShuffle = {};
        let peer_test_groups = await db.many(
          `SELECT
                                id,
                                id_calib_assessments_node,
                                id_question
                            FROM peer_test_group
                        WHERE id_peer_test = $(id_peer_test)`,
          {
            id_peer_test: peer_test.id,
          }
        );
        for (let i = 0; i < peer_test_groups.length; ++i) {
          // console.log(
          //   'filtering on ',
          //   peer_test_groups[i].id_question,
          //   typeof peer_test_groups[i].id_question
          // );
          let groupIds = ids
            .filter((x) => x.id_question === peer_test_groups[i].id_question)
            .map((x) => x.id);
          if (groupIds && groupIds.length) {
            if (peer_test.calib_assessments_no > 0) {
              calibIds = await db.any(
                `SELECT id_question
                                FROM question_node
                                WHERE id_node = $(id_calib_assessments_node)
                                ORDER BY random()
                                LIMIT ${peer_test.calib_assessments_no}
                            `,
                {
                  id_peer_test: peer_test.id,
                  id_calib_assessments_node: peer_test_groups[i].id_calib_assessments_node,
                }
              );
            } else {
              calibIds = [];
            }
            // console.log('groupIds.length = ', groupIds.length);
            let groupShuffle = getShuffle(qs, groupIds, calibIds);
            // console.log('groupShuffle.length ' + i, Object.keys(groupShuffle).length);
            rvShuffle = { ...rvShuffle, ...groupShuffle };
          } else {
            winston.error('Shuffle def error: peer group with 0 instances?');
          }
        }
      } else {
        calibIds = await db.any(
          `SELECT id_question
                    FROM question_node
                    WHERE id_node IN (select id_calib_assessments_node from peer_test_group where id_peer_test = $(id_peer_test))
                    ORDER BY random()
                    LIMIT ${peer_test.calib_assessments_no}
                `,
          {
            id_peer_test: peer_test.id,
          }
        );
        ids = ids.map((x) => x.id);
        rvShuffle = getShuffle(qs, ids, calibIds);
      }
      // console.log('rvShuffle.length = ', Object.keys(rvShuffle).length);
      req.session.peer_shuffle = rvShuffle;
      // console.log('skipped ', skip, skip / (qs * (ids.length - 1)));
      res.json({
        success: true,
        data: rvShuffle,
      });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: JSON.stringify(error),
        },
      });
    }
  }
);

let getShuffle = function (qs, ids, calibIds) {
  let studJobs = {};
  //ids = ids.map(x => x.id);
  for (let i = 0; i < ids.length; ++i) {
    let id = ids[i];
    studJobs[id] = [];
    for (let j = 0; j < qs; ++j) {
      studJobs[id].push(ids[(i + j + 1) % ids.length]);
    }
    //console.log(id + ' done. initial jobs = ', studJobs[id]);
  }
  let skip = 0;
  for (let q = 0; q < qs; ++q) {
    //console.log('q' + q + "********************");
    for (let i = ids.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let namei = ids[i];
      let namej = ids[j];
      //console.log('swaping? ', namei, namej);
      if (
        studJobs[namei][q] === namej ||
        studJobs[namej][q] === namei ||
        studJobs[namei].indexOf(studJobs[namej][q]) >= 0 ||
        studJobs[namej].indexOf(studJobs[namei][q]) >= 0
      ) {
        //console.log('skipping swap of ', namei, namej, studJobs[namei][q], studJobs[namej][q]);
        skip++;
      } else {
        let x = studJobs[namei][q];
        studJobs[namei][q] = studJobs[namej][q];
        studJobs[namej][q] = x;
      }
    }
  }
  for (let id in studJobs) {
    for (let calibRow of calibIds) {
      studJobs[id].splice(
        utils.randomIntInclusive(0, studJobs[id].length),
        0,
        -1 * calibRow.id_question
      );
    }
  }
  return studJobs;
};

router.post(
  '/shuffle/save/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      // console.log('shuffle post', req.body);
      db.tx((t) => {
        let statements = [
          t.result(`DELETE FROM peer_shuffle WHERE id_test_phase_2 = $1`, [req.params.id_test]),
        ];
        let jobs = req.session.peer_shuffle;
        for (let tiid in jobs) {
          statements.push(
            t.result(
              `INSERT into peer_shuffle (
                        id_test_instance_question,
                        id_test_phase_2,
                        jobs ) VALUES
                    ($1, $2,  $3);`,
              [tiid, req.params.id_test, jobs[tiid]]
            )
          );
          // console.log('insert into ', [tiid, req.params.id_test, jobs[tiid]]);
        }
        return t.batch(statements);
      })
        .then((result) => {
          res.json({
            success: result && result.length && true,
            data: result,
          });
        })
        .catch((error) => {
          winston.error(error);
          res.json({
            success: false,
            data: error,
          });
        });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: JSON.stringify(error),
        },
      });
    }
  }
);

router.post(
  '/peerexam/generate/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let no = 0;
      let examMaster = await db.one('SELECT * FROM test WHERE id = $(id)', {
        id: req.params.id_test,
      });
      let examNo = await db.oneOrNone(
        'SELECT assessments_no FROM peer_test WHERE id_test_phase_2 = $(id)',
        {
          id: req.params.id_test,
        }
      );
      if (examNo && examNo.assessments_no) {
        for (let i = 0; i < examNo.assessments_no; ++i) {
          let result = await db.result(
            `
                       BEGIN WORK;
                       DELETE FROM test_question WHERE id_test =
                         (SELECT id
                            FROM test
                            WHERE test_ordinal = $(ordinal)
                            AND id_academic_year = $(id_academic_year)
                            AND id_course = $(id_course)
                            AND id_parent = $(id_parent)
                            AND NOT EXISTS (SELECT * FROM test_instance WHERE id_test = test.id));

                       DELETE FROM test
                        WHERE test_ordinal = $(ordinal)
                         AND id_academic_year = $(id_academic_year)
                         AND id_course = $(id_course)
                         AND id_parent = $(id_parent)
                         AND NOT EXISTS (SELECT * FROM test_instance WHERE id_test = test.id);
                       COMMIT WORK;
                         `,
            {
              ordinal: examMaster.test_ordinal + i + 1,
              id_academic_year: examMaster.id_academic_year,
              id_course: examMaster.id_course,
              id_parent: examMaster.id,
            }
          );
          //console.log('result', result);
          let existing = await db.oneOrNone(
            `SELECT id FROM test
                        WHERE test_ordinal = $(ordinal)
                          AND id_academic_year = $(id_academic_year)
                          AND id_course = $(id_course)
                          AND id_parent = $(id_parent)
                        `,
            {
              ordinal: examMaster.test_ordinal + i + 1,
              id_academic_year: examMaster.id_academic_year,
              id_course: examMaster.id_course,
              id_parent: examMaster.id,
            }
          );
          //console.log('existing', existing);
          if (!existing) {
            // Either this is a first generate, or regenerate delete a non-referenced test
            let cloned = await db.func('clone_test', [
              examMaster.id,
              examMaster.id_academic_year,
              req.session.appUserId,
            ]);
            await db.result(
              `UPDATE test
                                SET title = $(title),
                                    test_ordinal = $(ordinal),
                                    password = $(password),
                                    id_parent = $(id_parent),
                                    id_test_type = (SELECT id FROM test_type WHERE type_name = 'Peer assessment PHASE2')
                                WHERE id = $(id)`,
              {
                title: examMaster.title + ' #' + (1 + i),
                ordinal: examMaster.test_ordinal + i + 1,
                id: cloned[0].clone_test,
                password: examMaster.password + (1 + i),
                id_parent: examMaster.id,
              }
            );
            ++no;
          }
        }
      }
      res.json({
        success: true,
        message: '(Re)generated ' + no + ' exams. Please reload the page manually.',
      });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        message: 'Error occured: ' + JSON.stringify(error),
      });
    }
  }
);

router.get(
  '/edit/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    var test;
    var tps;
    var qs;
    var tests = await db.any(`SELECT name, id "value" FROM plag_detection_algorithm;`);
    tests.unshift({
      name: 'Real-time plag detection is OFF',
      value: 0,
    });
    getTest(req)
      .then(function (t) {
        test = t;
        if (test && test.fixed_questions === true) {
          if (test.type_name === 'Peer assessment PHASE2 MASTER') {
            // return getPeerTestQuestions(req.params.id peer...);
          } else {
            return getTestQuestions(req.params.id_test);
          }
        } else if (test) {
          return getTestParts(req.params.id_test);
        } else {
          res
            .status(404)
            .send(
              'Missed it. Are you in the right course? If not, adjust your course in the context menu and click the link again.'
            );
        }
      })
      .then(function (test_part) {
        if (test.fixed_questions === true) {
          qs = test_part;
          var promises = [];
          promises.push(
            db.many(
              `select id as value, model_name as name From grading_model order by correct_Score`
            )
          );
          promises.push(
            db.many(
              `SELECT null::int as value, 'None' as name UNION ALL SELECT id as value, name From email_reminder_scheme order by 1`
            )
          );
          promises.push(getNodeName(req));
          promises.push(getPeerTests(req));
          promises.push(
            db.any(
              `SELECT id_test_instance_question, jobs
                               FROM peer_shuffle
                              WHERE id_test_phase_2 = $(tId)
                           ORDER BY id_test_instance_question`,
              {
                tId: req.params.id_test,
              }
            )
          );
          promises.push(
            db.any(
              `SELECT test.id, test.test_ordinal, test.password, to_char(test.ts_available_to, 'YYYY-MM-DD HH24:MI:SS') as ts_available_to,
                            to_char(test.ts_available_from, 'YYYY-MM-DD HH24:MI:SS') as ts_available_from, type_name
                               FROM test
                               JOIN test_type
                                 ON test.id_test_type = test_type.id
                              WHERE id_parent = $(tId)
                           ORDER BY test_ordinal`,
              {
                tId: req.params.id_test,
              }
            )
          );
          promises.push(getPeerTestGroups(req.params.id_test));
          promises.push(
            db.many(
              `SELECT null::int as value, 'None' as name UNION ALL SELECT id as value, pace_name as name From pace order by 1`
            )
          );
          promises.push(
            db.many(`SELECT id as value, policy_name as name From ticket_policy order by 1`)
          );

          Promise.all(promises).then(async function (args) {
            let peer_shuffle = undefined;
            if (args[4]) {
              //console.log('p.shuffle', args[4]);
              peer_shuffle = {};
              for (let row of args[4]) {
                peer_shuffle[row.id_test_instance_question] = row.jobs;
              }
            }
            // let arrPeerGroupQuestions = undefined;
            // if (args[6]) {
            //     arrPeerGroupQuestions = [];

            //     for (let row of args[6]) {
            //         arrPeerGroupQuestions.push(await getPeerTestQuestions(row.id));
            //     }
            // }
            res.appRender(
              'editTestDefView',
              {
                pageTitle: `Exam: ${test.id} 🔨${test.last_name}`,
                test: test,
                test_question: qs,
                grading_model: args[0],
                email_reminder_schemes: args[1],
                node_name: args[2],
                type_name: test.type_name,
                peer_tests: args[3],
                peer_shuffle: peer_shuffle,
                peer_exams: args[5],
                peer_test_groups: args[6],
                peer_test_group_questions: args[6]
                  ? await getPeerTestQuestions(req.params.id_test)
                  : undefined,
                pace_schemes: args[7],
                ticket_policies: args[8],
                tests: tests,
                // partial_points: args[9],
              },
              'editTestDefViewScript'
            );
          });
        } else {
          tps = test_part;
          var promises = [];
          promises.push(
            db.many(
              `select id as value, model_name as name From grading_model order by correct_Score`
            )
          );
          promises.push(
            db.many(
              `select id as value, type_name as name  From node_type where type_name <> 'Course' order by id desc`
            )
          );
          promises.push(
            db.many(
              `SELECT null::int as value, 'None' as name UNION ALL SELECT id as value, name From email_reminder_scheme order by 1`
            )
          );
          promises.push(
            db.many(`SELECT id as value, node_name as name From node order by node_name asc`)
          );
          promises.push(
            db.many(
              `SELECT null::int as value, 'None' as name UNION ALL SELECT id as value, pace_name as name From pace order by 1`
            )
          );
          promises.push(
            db.many(`SELECT id as value, policy_name as name From ticket_policy order by 1`)
          );
          Promise.all(promises).then(function (args) {
            res.appRender(
              'editTestDefView',
              {
                test: test,
                test_part: tps,
                grading_model: args[0],
                node_type: args[1],
                email_reminder_schemes: args[2],
                node_name: args[3],
                pace_schemes: args[4],
                ticket_policies: args[5],
                tests: tests,
              },
              'editTestDefViewScript'
            );
          });
        }
      })
      .catch(function (error) {
        winston.error(error);
        return next(error);
      });
  }
);

router.post('/save', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.one(
    'SELECT type_name, fixed_questions FROM test_type JOIN test ON test.id_test_type = test_type.id WHERE test.id = $(id_test)',
    {
      id_test: req.body.id_test,
    }
  )
    .then(function (type) {
      return db.tx((t) => {
        var queries = [];

        queries.push(
          t.none(
            `UPDATE test
                        SET   title = $(title)
                            , title_abbrev = $(title_abbrev)
                            , test_ordinal = $(test_ordinal)
                            , max_runs = $(max_runs)
                            , is_public = $(is_public)
                            , is_global = $(is_global)
                            , use_in_stats = $(use_in_stats)
                            , show_solutions = $(show_solutions)
                            , hint_result = $(hint_result)
                            , max_score = $(max_score)
                            , test_score_ignored = $(test_score_ignored)
                            , async_submit = $(async_submit)
                            , trim_clock = $(trim_clock)
                            , forward_only = $(forward_only)
                            , allow_anonymous_stalk = $(allow_anonymous_stalk)
                            , password = $(password)
                            , questions_no = $(questions_no)
                            , duration_seconds = $(duration_seconds)
                            , pass_percentage = $(pass_percentage)
                            , review_period_mins = $(review_period_mins)
                            , id_email_reminder_scheme = $(id_email_reminder_scheme)
                            , ts_available_to = $(ts_available_to)
                            , ts_available_from = $(ts_available_from)
                            , is_competition = $(is_competition)
                            , eval_comp_score = $(eval_comp_score)
                            , upload_file_limit = $(upload_file_limit)
                            , id_plag_detection_algorithm = $(id_plag_detection_algorithm)
                            , id_ticket_policy = $(id_ticket_policy)
                            WHERE id = $(id_test)`,
            {
              id_test: req.body.id_test,
              title: req.body.title,
              title_abbrev: req.body.title_abbrev,
              test_ordinal: req.body.test_ordinal,
              max_runs: req.body.max_runs,
              is_public: req.body.is_public === 'true' ? true : false,
              use_in_stats: req.body.use_in_stats === 'true' ? true : false,
              is_global: req.body.is_global === 'true' ? true : false,
              show_solutions: req.body.show_solutions === 'true' ? true : false,
              hint_result: req.body.hint_result === 'true' ? true : false,
              max_score: req.body.max_score,
              test_score_ignored: req.body.test_score_ignored === 'true' ? true : false,
              async_submit: req.body.async_submit === 'true' ? true : false,
              trim_clock: req.body.trim_clock === 'true' ? true : false,
              forward_only: req.body.forward_only === 'true' ? true : false,
              allow_anonymous_stalk: req.body.allow_anonymous_stalk === 'true' ? true : false,
              password: req.body.password,
              questions_no: req.body.questions_no,
              duration_seconds: req.body.duration_seconds,
              pass_percentage: req.body.pass_percentage,
              review_period_mins: req.body.review_period_mins,
              id_email_reminder_scheme: req.body.id_email_reminder_scheme
                ? req.body.id_email_reminder_scheme
                : null,
              ts_available_to: req.body.ts_available_to,
              ts_available_from: req.body.ts_available_from,
              is_competition: req.body.is_competition === 'true' ? true : false,
              eval_comp_score: req.body.eval_comp_score,
              upload_file_limit: req.body.upload_file_limit,
              id_plag_detection_algorithm:
                req.body.id_plag_detection_algorithm == '0'
                  ? null
                  : req.body.id_plag_detection_algorithm,
              id_ticket_policy: req.body.id_ticket_policy ? req.body.id_ticket_policy : null,
            }
          )
        );
        queries.push(
          t.none(`DELETE FROM test_pace WHERE id_test = $(id_test)`, {
            id_test: req.body.id_test,
          })
        );
        if (req.body.id_pace_question || req.body.id_pace_test) {
          queries.push(
            t.none(
              `INSERT INTO test_pace (id_test, id_pace_test, id_pace_question)
                        VALUES ($(id_test), $(id_pace_test), $(id_pace_question))`,
              {
                id_test: req.body.id_test,
                id_pace_test: req.body.id_pace_test ? req.body.id_pace_test : null,
                id_pace_question: req.body.id_pace_question ? req.body.id_pace_question : null,
              }
            )
          );
        }

        if (type.fixed_questions === true && type.type_name !== 'Peer assessment PHASE2 MASTER') {
          //todo
          req.body.test_questions.forEach(function (tq) {
            queries.push(
              t.none(
                `UPDATE test_question
                    SET id_grading_model = $(id_grading_model)
                  WHERE id = $(id_test_question)`,
                {
                  id_grading_model: tq.id_grading_model,
                  id_test_question: tq.id,
                }
              )
            );
          });
        } else if (type.type_name === 'Peer assessment PHASE2 MASTER') {
          // todo...
          //id_peer_test_group: [ '1', '2' ],
          //id_calib_assessments_node: [ '1001', '' ]
          if (req.body.id_peer_test_group && req.body.id_peer_test_group.length) {
            req.body.id_peer_test_group.forEach(function (idPtg, idx) {
              queries.push(
                t.none(
                  `UPDATE peer_test_group
                                    SET   id_calib_assessments_node = $(id_calib_assessments_node)
                                        WHERE id = $(idPtg)`,
                  {
                    id_calib_assessments_node:
                      req.body.id_calib_assessments_node[idx] &&
                      req.body.id_calib_assessments_node[idx] != 'null'
                        ? req.body.id_calib_assessments_node[idx]
                        : null,
                    idPtg: idPtg,
                  }
                )
              );
            });
          }
          if (req.body.peer_test_group_question && req.body.peer_test_group_question.length) {
            req.body.peer_test_group_question.forEach(function (row, idx) {
              queries.push(
                t.none(
                  `UPDATE peer_test_group_question
                                    SET
                                        id_grading_model = $(id_grading_model),
                                        ordinal = $(ordinal)
                                        WHERE id = $(id)`,
                  {
                    id_grading_model: row.id_grading_model,
                    ordinal: row.ordinal,
                    id: row.id,
                  }
                )
              );
            });
          }
          // peer_test_group_question: [
          //     { id: '24', id_test: '', id_grading_model: '2' },
          //     { id: '25', id_test: '', id_grading_model: '27' }
          //   ]
        } else {
          if (req.body.test_parts) {
            req.body.test_parts.forEach(function (tp) {
              queries.push(
                t.none(
                  `UPDATE test_part
                                SET   id_grading_model = $(id_grading_model)
                                    , min_questions = $(min_questions)
                                    , max_questions = $(max_questions)
                                    , pass_percentage = $(pass_percentage)
                                    , ordinal = $(ordinal)
                                    WHERE id = $(id_test_part)`,
                  {
                    id_grading_model: tp.id_grading_model,
                    min_questions: tp.min_questions,
                    max_questions: tp.max_questions,
                    pass_percentage: tp.pass_percentage,
                    ordinal: tp.ordinal,
                    id_test_part: tp.id_test_part,
                  }
                )
              );
            });
          }
        }

        if (req.body.id_test_phase_1) {
          queries.push(
            t.none(
              `
                            INSERT INTO peer_test (id_test_phase_1, id_test_phase_2, assessments_no, calib_assessments_no )
                                VALUES ($(id_test_phase_1), $(id_test_phase_2), $(assessments_no), $(calib_assessments_no) )
                            ON CONFLICT ON CONSTRAINT uidx_peer_test
                            DO UPDATE
                                SET
                                    assessments_no = $(assessments_no),
                                    calib_assessments_no = $(calib_assessments_no)
                                `,
              {
                // id_test: req.body.id_test,
                id_test_phase_1: req.body.id_test_phase_1,
                id_test_phase_2: req.body.id_test,
                assessments_no: req.body.assessments_no,
                calib_assessments_no: req.body.calib_assessments_no,
              }
            )
          );
          queries.push(
            t.none(
              `INSERT INTO peer_test_group(id_peer_test, id_question)
                            SELECT DISTINCT peer_test.id, id_question
                              FROM peer_test
                              JOIN test_instance
                                ON peer_test.id_test_phase_1 = test_instance.id_test
                              JOIN test_instance_question
                                ON test_instance.id = test_instance_question.id_test_instance
                             WHERE id_test_phase_2 = $(id_test)
                               AND NOT exists (SELECT * from peer_test_group ptg
                                    JOIN peer_test pt
                                      ON ptg.id_peer_test = pt.id
                                   WHERE pt.id = peer_test.id
                                     AND ptg.id_question = test_instance_question.id_question)
                            `,
              {
                id_test: req.body.id_test,
              }
            )
          );
        }
        return t.batch(queries);
      });
    })
    .then(function (data) {
      winston.debug('update test definition returned: ' + data);
      req.flash('info', data.length + ' row(s) affected.');
      res.redirect('/test/def/edit/' + req.body.id_test);
    })
    .catch((err) => {
      req.flash('error', JSON.stringify(err));
      res.redirect('/test/def/edit/' + req.body.id_test);
    });
});

router.post('/validate', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.one(
    'SELECT test_type.id, type_name, fixed_questions FROM test_type JOIN test ON test.id_test_type = test_type.id WHERE test.id = $(id_test)',
    {
      id_test: req.body.id_test,
    }
  )
    .then(function (type) {
      req.body.id_type = type.id;
      req.body.fixed_questions = type.fixed_questions;
      req.body.type_name = type.type_name;
      testDefinitionValidityService
        .validate(req.body)
        .then(function (validity) {
          // perform tasks if test is not valid
          /*
                    if (!validity.valid) {

                    }
                    */
          if (req.query.ajax !== 'true' && validity.warning && validity.warning.length > 0) {
            req.flash(
              'info',
              `<p>The test definition is valid, but you may consider the following warning(s): <ul>${validity.warning}</ul></p>`
            );
          }

          winston.debug('test definition validation returned: ' + validity);
          res.json(validity).send();
        })
        .catch(function (error) {
          winston.error(error);
          res.send(JSON.stringify(error));
        });
    })
    .catch(function (error) {
      winston.error(error);
      res.send(JSON.stringify(error));
    });
});

router.post(
  '/testpart/addnode',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.result(
      `INSERT INTO test_part(id_test, id_node, id_grading_model, min_questions, max_questions)
                VALUES (
                $(id_test),
                $(id_node),
                (SELECT COALESCE( min(id_grading_model), 0) from test_part where id_test = $(id_test)),
                0,
                1)`,
      {
        id_test: req.body.id_test,
        id_node: req.body.id_node,
      }
    )
      .then(function (result) {
        winston.debug('add test_part node returned: ' + result);
        res.redirect('/test/def/testparts/' + req.body.id_test);
      })
      .catch(function (error) {
        winston.error(error);
        res.status(404).send('Error adding node: ' + JSON.stringify(error));
      });
  }
);

router.post(
  '/testquestion/addquestion',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.result(
      `INSERT INTO test_question(ordinal, id_test, id_question, id_grading_model)
                VALUES (
                (select coalesce(max(ordinal), 0) + 1 from test_question where id_test = $(id_test)),
                $(id_test),
                $(id_question),
                (SELECT COALESCE( min(id_grading_model), 0) from test_question where id_test = $(id_test))
                )`,
      {
        id_test: req.body.id_test,
        id_question: req.body.id_question,
      }
    )
      .then(function (result) {
        winston.debug('add test_question question returned: ' + result);
        res.redirect('/test/def/testquestions/' + req.body.id_test);
      })
      .catch(function (error) {
        winston.error(error);
        res.status(404).send('Error adding question: ' + JSON.stringify(error));
      });
  }
);
router.post(
  '/peertest/addquestion',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res) {
    console.log(req.body);
    db.result(
      `INSERT INTO peer_test_group_question(ordinal, id_peer_test_group, id_question, id_grading_model)
                VALUES (
                (select coalesce(max(ordinal), 0) + 1 from peer_test_group_question where id_peer_test_group = $(id_peer_test_group)),
                $(id_peer_test_group),
                $(id_question),
                (SELECT COALESCE( min(id_grading_model), 0) from peer_test_group_question where id_peer_test_group = $(id_peer_test_group))
                )`,
      {
        id_peer_test_group: req.body.id_peer_test_group,
        id_question: req.body.id_question,
      }
    )
      .then(function (result) {
        winston.debug('add test_question question returned: ' + result);
        res.redirect('/test/def/peertestquestions/' + req.body.id_peer_test_group);
      })
      .catch(function (error) {
        winston.error(error);
        res.status(500).send('Error adding question: ' + JSON.stringify(error));
      });
  }
);

router.post('/cloneup', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.func('clone_test', [
    req.body.id_test,
    1 + req.session.currAcademicYearId,
    req.session.appUserId,
  ])
    .then(function (data) {
      winston.debug('test returned: ' + data);
      req.flash(
        'info',
        `Test ${req.body.id_test} was successfully cloned to test id = ${data[0].clone_test} in the next academic year.`
      );
      res.redirect('/test/def/edit/' + req.body.id_test);
    })
    .catch(function (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('/test/def/edit/' + req.body.id_test);
    });
});
router.post('/clone', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.func('clone_test', [req.body.id_test, req.session.currAcademicYearId, req.session.appUserId])
    .then(function (data) {
      winston.debug('test returned: ' + data);
      req.flash(
        'info',
        `Exam ${req.body.id_test} was successfully cloned to (this) test id = ${data[0].clone_test}. You may edit further.`
      );
      res.redirect('/test/def/edit/' + data[0].clone_test);
    })
    .catch(function (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('/test/def/edit/' + req.body.id_test);
    });
});
router.post('/deepclone', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.func('deep_clone_test', [
    req.body.id_test,
    req.session.currAcademicYearId,
    req.session.appUserId,
  ])
    .then(function (data) {
      winston.debug('deep clone returned: ' + data);
      req.flash(
        'info',
        `Exam ${req.body.id_test} was successfully DEEP cloned to (this) exam id = ${data[0].deep_clone_test}. "Deep" meaning that the questions were also cloned. You may edit further (maybe fix question nodes?).`
      );
      res.redirect('/test/def/edit/' + data[0].deep_clone_test);
    })
    .catch(function (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('/test/def/edit/' + req.body.id_test);
    });
});
// *********************

module.exports = router;
