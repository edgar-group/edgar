'use strict';
const express = require('express');
const router = express.Router();
const errors = require('../common/errors');
const middleware = require('../common/middleware');
const globals = require('../common/globals');
const db = require('../db').db;
const winston = require('winston');

const courseService = require('../services/courseService');
const questionService = require('../services/questionService');
const tutorialDefService = require('../services/tutorialDefService');

router.get('/', async (req, res) => {
  const isTeacher = req.session.rolename === globals.ROLES.TEACHER;
  const data = await tutorialDefService.listTutorials(req.session.currCourseId, !isTeacher);

  data.forEach((row) => {
    row.analytics = `<a href="/tutorial/analytics/${row['id']}/students">Usage</a>`;
    // <br />
    // <a href="/tutorial/analytics/${row['id']}/groups">Groups</a><br />
    // <a href="/feedback/?tutorialId[]=${row['id']}">Feedback</a>`;
  });

  res.appRender('~/tutorial/listTutorialsView', {
    tutorials: {
      headers: [
        {
          id_act_vir: 'Id/Act/Vir',
          __raw: true,
        },
        {
          tutorial_title: 'Title',
        },
        {
          tutorial_desc: 'Description',
        },
        {
          user_created_name: 'Created by',
        },
        {
          ts_created: 'Created at',
        },
        {
          user_modified_name: 'Modified by',
        },
        {
          ts_modified: 'Modified at',
        },
        {
          courses: 'Course(s)',
        },
        {
          no_steps: 'Steps',
        },
        {
          analytics: 'Analytics',
          __raw: true,
        },
      ],
      rows: data,
      buttons: [
        {
          label: 'Delete',
          method: 'POST',
          action: function (row) {
            return `/tutorial/def/${row['id']}/delete`;
          },
          class: function (row) {
            return 'btn btn-outline-danger';
          },
          props: {
            edgar_confirm_text: 'Do you really want to delete this tutorial?',
          },
        },
        {
          label: 'Edit',
          method: 'GET',
          action: function (row) {
            return `/tutorial/def/${row['id']}/edit/`;
          },
        },
        {
          label: 'Tickets',
          method: 'GET',
          action: function (row) {
            return `/tutorial/ticket/tutorial/${row['id']}`;
          },
        },
      ],
    },
  });
});

router.get('/new', middleware.requireRoles([globals.ROLES.TEACHER]), (req, res, next) => {
  db.task((t) =>
    t.batch([
      db.any(
        `SELECT id as value, type_name as name
                    FROM question_type
                    ORDER BY type_name`
      ),
      db.many(`SELECT id as value, name  FROM code_runner ORDER BY id`),
    ])
  ).then((data) => {
    res.appRender('newTutorialDefView', {
      question_types: data[0],
      is_active: false,
      allow_random_access: true,
      tutorial_title: '',
      tutorial_desc: '',
      tutorial_no_steps: 10,
      code_runners: data[1],
    });
  });
});

router.post('/new', middleware.requireRoles([globals.ROLES.TEACHER]), async (req, res, next) => {
  if (req.body.tutorial_title === '' || req.body.tutorial_no_steps === '') {
    winston.error('Tutorial title and number of steps not defined');
    res.redirect('/tutorial/def/new');
  }

  let tutorialId;
  try {
    tutorialId = await tutorialDefService.createNewTutorial(
      req.body.tutorial_title,
      req.body.tutorial_desc,
      req.body.is_active ? true : false,
      req.body.tutorial_no_steps,
      req.body.id_question_type,
      req.body.answers_count,
      req.body.id_code_runner,
      req.session.currCourseId,
      req.session.appUserId
    );
  } catch (err) {
    req.flash('error', JSON.stringify(err));
    res.redirect('/tutorial/def');
  }

  // redirect to first step edit:
  req.flash('info', 'Tutorial successfully created! Please continue editing the steps');
  res.redirect(`/tutorial/def/${tutorialId}/step/1/edit`);
});

router.get(
  '/:id_tutorial([0-9]{1,10})/edit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    // to show title, description and steps with reordering option
    db.task((t) =>
      t.batch([
        db.one(
          `SELECT tutorial.*,
                        is_virgin_tutorial(tutorial.id) AS is_virgin
                    FROM tutorial
                    WHERE id = $(id_tutorial)`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
          }
        ),
        db.any(
          `SELECT *
                    FROM tutorial_step
                    WHERE id_tutorial = $(id_tutorial)
                    ORDER BY ordinal`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
          }
        ),
      ])
    ).then((results) => {
      const tutorial = results[0],
        steps = results[1];
      res.appRender('~/tutorial/def/editTutorialDefView', {
        tutorial_id: tutorial.id,
        tutorial_title: tutorial.tutorial_title,
        tutorial_desc: tutorial.tutorial_desc,
        is_active: tutorial.is_active ? true : false,
        allow_random_access: tutorial.allow_random_access ? true : false,
        is_virgin: tutorial.is_virgin,
        steps: steps,
        courses: req.session.courses.map((c) => ({
          value: c.id,
          name: `${c.course_acronym} (${c.course_name})`,
        })),
        currCourse: req.session.currCourseId,
      });
    });
  }
);

router.post(
  '/:id_tutorial([0-9]{1,10})/edit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    if (req.body.save == 'save') {
      db.tx((t) => {
        let operations = [
          t.none(
            `UPDATE tutorial
                        SET tutorial_title = $(title),
                            tutorial_desc = $(desc),
                            allow_random_access = $(allow_random_access),
                            is_active = $(is_active)
                        WHERE id = $(id_tutorial)`,
            {
              title: req.body.tutorial_title,
              desc: req.body.tutorial_desc,
              allow_random_access: req.body.allow_random_access ? true : false,
              is_active: req.body.is_active ? true : false,
              id_tutorial: parseInt(req.params.id_tutorial),
            }
          ),
          t.none(
            `UPDATE node
                        SET node_name = $(title)
                        WHERE id = (SELECT id_node FROM tutorial WHERE id = $(id_tutorial))`,
            {
              title: req.body.tutorial_title,
              id_tutorial: parseInt(req.params.id_tutorial),
              id_course: req.session.currCourseId,
            }
          ),
        ];

        if (req.body.virgin) {
          operations.push(t.none('SET CONSTRAINTS tutorial_step_ordinal_uniq DEFERRED'));
          operations.push(
            req.body.steps.map((step) =>
              t.none(
                `UPDATE tutorial_step
                                SET ordinal = $(ordinal)
                                WHERE id = $(id)`,
                {
                  id: step.id,
                  ordinal: step.ordinal,
                }
              )
            )
          );
        }

        return t.batch(operations);
      })
        .then((results) => {
          req.flash('info', `Tutorial definition saved.`);
          res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
        })
        .catch((err) => {
          req.flash('error', JSON.stringify(err));
          res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
        });
    } else if (req.body.virginize == 'virginize') {
      try {
        await tutorialDefService.virginize(req.params.id_tutorial);
        req.flash('info', `Tutorial successfully virginized.`);
        res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
      } catch (err) {
        req.flash('error', 'failed to virginize tutorial', JSON.stringify(err));
        res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
      }
    } else if (req.body.clone == 'clone') {
      try {
        const newTutorialId = await tutorialDefService.clone(
          parseInt(req.params.id_tutorial),
          req.body.id_course,
          req.session.appUserId,
          req.body.tutorial_title
        );
        req.flash('info', `Tutorial successfully cloned (id = ${newTutorialId}).`);
        res.redirect(`/tutorial/def/${newTutorialId}/edit`);
      } catch (err) {
        req.flash('error', 'Failed to clone tutorial: ', JSON.stringify(err));
        res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
      }
    }
  }
);

router.post(
  '/:id_tutorial([0-9]{1,10})/delete',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      await tutorialDefService.deleteTutorial(req.params.id_tutorial);
      req.flash('info', `Tutorial ${req.params.id_tutorial} deleted.`);
    } catch (err) {
      req.flash('error', JSON.stringify(err));
    }

    res.redirect('/tutorial/def');
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/edit/step/add',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  (req, res, next) => {
    db.task((t) =>
      t.batch([
        db.any(`SELECT id as value, type_name as name
                    FROM question_type
                    ORDER BY type_name`),
        db.many(
          `SELECT id as value, name
                    FROM code_runner
                    ORDER BY id`
        ),
      ])
    ).then(async (results) => {
      res.appRender(
        '~/tutorial/def/newTutorialStepDefView',
        {
          code_runners: results[1],
        },
        '~/shared/editQuestionViewScript'
      );
    });
  }
);

router.post(
  '/:id_tutorial([0-9]{1,10})/edit/step/add',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      const step = await tutorialDefService.addStep(
        parseInt(req.params.id_tutorial),
        req.session.currCourseId,
        parseInt(req.body.id_question_type),
        parseInt(req.body.id_code_runner),
        req.body.answers_count,
        req.session.appUserId
      );

      req.flash('info', 'New step created, please edit.');
      res.redirect(`/tutorial/def/${req.params.id_tutorial}/step/${step.ordinal}/edit`);
    } catch (err) {
      req.flash('error', JSON.stringify(err));
      res.redirect(`/tutorial/def/${req.params.tutorial}/edit/step/add`);
    }
  }
);

router.get(
  '/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})/edit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      // let idTutorial = parseInt(req.params.id_tutorial);
      // const isOld = await db.one(
      //   `SELECT COUNT(*) as cnt
      //          FROM old_tutorials_tbd
      //         WHERE id_tutorial = $(idTutorial)`,
      //   {
      //     idTutorial,
      //   }
      // );
      // if (isOld.cnt == 1) {
      //   winston.info(`OLD tutorial id=${idTutorial} edited. PLease, upgrade...`);
      //   res.redirect(`/tutorial/def/${idTutorial}/step/${req.params.step_ordinal}/oldedit`);
      // }
      let promises = [];
      promises.push(
        db.oneOrNone(
          `SELECT tutorial_title, is_virgin_tutorial(id) AS is_virgin
                    FROM tutorial
                    WHERE id = $(id_tutorial)`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
          }
        )
      );
      promises.push(
        db.oneOrNone(
          `SELECT *
                    FROM tutorial_step
                    WHERE id_tutorial = $(id_tutorial)
                        AND ordinal = $(step_ordinal)`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
            step_ordinal: req.params.step_ordinal,
          }
        )
      );
      promises.push(
        db.any(
          `SELECT tutorial_step_hint.*
                   FROM tutorial_step_hint JOIN tutorial_step
                     ON tutorial_step_hint.id_step = tutorial_step.id
                  WHERE tutorial_step.id_tutorial = $(id_tutorial)
                    AND tutorial_step.ordinal = $(step_ordinal)
                    ORDER BY tutorial_step_hint.ordinal ASC`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
            step_ordinal: req.params.step_ordinal,
          }
        )
      );

      promises.push(courseService.getProgrammingLanguages(req.session.currCourseId));
      promises.push(courseService.getCodeRunners(req.session.currCourseId));
      // promises.push(db.any(
      //     `SELECT * FROM answer_regex_trigger_template ORDER BY ordinal;`
      // ));

      let [tutorial, step, stepHints, programming_languages, code_runners] = await Promise.all(
        promises
      );
      if (!tutorial || !step) {
        req.flash('error', 'Step not found.');
        return res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit/step/add`);
      }
      res.appRender(
        '~/tutorial/def/editTutorialStepView',
        {
          tutorial_title: tutorial.tutorial_title,
          is_virgin: tutorial.is_virgin,
          tutorial_step: step,
          step_hints: stepHints,
          code_runners,
          programming_languages,
          google_translate_key: (
            await db.one('SELECT google_translate_key FROM app_user WHERE username = $(username)', {
              username: req.session.passport.user,
            })
          ).google_translate_key,
        },
        '~/shared/editQuestionViewScript'
      );
    } catch (error) {
      winston.error(error);
      return next(error);
    }
  }
);
router.get(
  '/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})/oldedit',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async (req, res, next) => {
    try {
      let promises = [];
      promises.push(
        db.oneOrNone(
          `SELECT tutorial_title, is_virgin_tutorial(id) AS is_virgin
                    FROM tutorial
                    WHERE id = $(id_tutorial)`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
          }
        )
      );
      promises.push(
        db.oneOrNone(
          `SELECT *
                    FROM tutorial_step
                    WHERE id_tutorial = $(id_tutorial)
                        AND ordinal = $(step_ordinal)`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
            step_ordinal: req.params.step_ordinal,
          }
        )
      );
      promises.push(
        db.any(
          `SELECT tutorial_step_hint.*
                    FROM tutorial_step_hint JOIN tutorial_step
                        ON tutorial_step_hint.id_step = tutorial_step.id
                    WHERE tutorial_step.id_tutorial = $(id_tutorial)
                        AND tutorial_step.ordinal = $(step_ordinal)
                    ORDER BY tutorial_step_hint.ordinal ASC`,
          {
            id_tutorial: parseInt(req.params.id_tutorial),
            step_ordinal: req.params.step_ordinal,
          }
        )
      );
      promises.push(
        db.many(
          `SELECT id as value, name
                    FROM code_runner
                    ORDER BY id`
        )
      );
      promises.push(db.any(`SELECT * FROM answer_regex_trigger_template ORDER BY ordinal;`));

      let [tutorial, step, stepHints, codeRunners, regexTemplate] = await Promise.all(promises);
      if (!tutorial || !step) {
        req.flash('error', 'Step not found.');
        return res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit/step/add`);
      }
      const questionData = await questionService.getQuestion(
        step.id_question,
        req.session.currCourseId,
        req.session.passport.user
      );
      questionData.reviewers = await db.any(
        `SELECT substring(first_name  from 1 for 1) || last_name as reviewer,
                                                        question_review.ts_created :: timestamp(0) :: varchar
                                                FROM question_review
                                                JOIN app_user
                                                ON id_user_reviewed = app_user.id
                                                WHERE id_question = $(id_question)
                                                ORDER BY question_review desc
                                `,
        {
          id_question: step.id_question,
        }
      );
      questionData.readOnlyReviewers = true;
      const questionHints =
        questionData.question.id_question_type === 1
          ? await db.any(
              `SELECT *
                            FROM tutorial_question_hint
                            WHERE id_step = $(id_step)`,
              {
                id_step: step.id,
              }
            )
          : await db.any(
              `SELECT *
                            FROM tutorial_code_question_hint
                            WHERE id_step = $(id_step)`,
              {
                id_step: step.id,
              }
            );
      res.appRender(
        '~/tutorial/def/oldEditTutorialStepView',
        {
          tutorial_title: tutorial.tutorial_title,
          is_virgin: tutorial.is_virgin,
          tutorial_step: step,
          step_hints: stepHints,
          code_runners: codeRunners,
          question_hints: questionHints,
          code_question_hints: questionHints,
          answer_regex_trigger_template: regexTemplate,
          google_translate_key: (
            await db.one('SELECT google_translate_key FROM app_user WHERE username = $(username)', {
              username: req.session.passport.user,
            })
          ).google_translate_key,
          ...questionData,
        },
        '~/shared/editQuestionViewScript'
      );
    } catch (error) {
      winston.error(error);
      return next(error);
    }
  }
);

router.post(
  '/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})/edit',
  middleware.requireRole(globals.ROLES.TEACHER),
  async (req, res) => {
    if (req.body.saveinplace === 'saveinplace') {
      try {
        //TODO: fix this - all this parsing is redundant, since Željko parses
        // the req.body AGAIN ab ovo in prepareSaveFunctionArguments
        // except mayble leave flash bcs i have a rew reference here

        await tutorialDefService.saveStep(
          req.body.tutorial_step.id,
          req.body.tutorial_step,
          req.body.step_hints,
          req.session.appUserId
        );
        req.flash('info', `Step ${req.params.step_ordinal} saved.`);
      } catch (err) {
        req.flash('error', JSON.stringify(err));
      }
      res.redirect(`/tutorial/def/${req.params.id_tutorial}/step/${req.params.step_ordinal}/edit`);
    } else if (req.body.delete === 'delete') {
      try {
        await tutorialDefService.deleteStep(req.body.tutorial_step.id);
        req.flash('info', `Step ${req.params.step_ordinal} deleted.`);
        res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
      } catch (err) {
        req.flash('error', JSON.stringify(err));
        res.redirect(
          `/tutorial/def/${req.params.id_tutorial}/step/${req.params.step_ordinal}/edit`
        );
      }
    }
  }
);
router.post(
  '/:id_tutorial([0-9]{1,10})/step/:step_ordinal([0-9]{1,10})/oldedit',
  middleware.requireRole(globals.ROLES.TEACHER),
  async (req, res, next) => {
    if (req.body.save === 'save' || req.body.saveinplace === 'saveinplace') {
      try {
        //TODO: fix this - all this parsing is redundant, since Željko parses the req.body AGAIN ab ovo in prepareSaveFunctionArguments
        // except mayble leave flash bcs i have a rew reference here
        let correctAnswers = [],
          answers = [],
          thresholds = [],
          penalty_percentages = [];

        for (let i = 0; i < 100; ++i) {
          if (req.body['answer' + i]) {
            correctAnswers.push(parseInt(req.body[`corrAnswerCheckboxes${i}`]) === i ? 1 : 0);
            answers.push(req.body['answer' + i]);
            thresholds.push(req.body['threshold_weight' + i]);
            penalty_percentages.push(req.body['penalty_percentage' + i]);
          } else {
            break;
          }
        }
        if (correctAnswers.length && correctAnswers.reduce((a, v) => a + v) === 0) {
          req.flash('error', 'Zero correct answers in multiple choice question!!');
        }
        await tutorialDefService.saveStepOld(
          req.body.tutorial_step.id,
          req.body.tutorial_step,
          req.body.step_hints,
          req.body.tutorial_question,
          req.body.question_hints,
          req.body.code_question_hints,
          answers,
          correctAnswers,
          thresholds,
          penalty_percentages,
          req.session.appUserId,
          {
            id_question: req.body.tutorial_question.id,
            question_text: req.body.tutorial_question.question_text,
            question_tags: [],
            is_active: req.body.is_active ? true : false,
            ...req.body,
          }
        );
        req.flash('info', `Step ${req.params.step_ordinal} saved.`);
      } catch (err) {
        req.flash('error', JSON.stringify(err));
      }
      res.redirect(`/tutorial/def/${req.params.id_tutorial}/step/${req.params.step_ordinal}/edit`);
    } else if (req.body.delete === 'delete') {
      try {
        await tutorialDefService.deleteStep(req.body.tutorial_step.id);
        req.flash('info', `Step ${req.params.step_ordinal} deleted.`);
        res.redirect(`/tutorial/def/${req.params.id_tutorial}/edit`);
      } catch (err) {
        req.flash('error', JSON.stringify(err));
        res.redirect(
          `/tutorial/def/${req.params.id_tutorial}/step/${req.params.step_ordinal}/edit`
        );
      }
    }
  }
);

module.exports = router;
