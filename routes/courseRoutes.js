var express = require('express');
var router = express.Router();
var AppUserSettings = require.main.require('./models/AppUserSettingsModel.js');
var winston = require('winston');
var utils = require.main.require('./common/utils');
var middleware = require('../common/middleware');
var db = require('../db').db;
var globals = require('../common/globals');

router.get('/:id(\\d+)/', async function (req, res) {
  try {
    req.session.currCourseId = parseInt(req.params.id);
    await utils.setCourseParams(req.session);
    await AppUserSettings.saveProps(req.user, {
      currCourseId: req.session.currCourseId,
    });
    winston.debug(`AppUserSettings for ${req.user} saved.`);
  } catch (error) {
    winston.error(error);
  }
  res.redirect('/');
});

router.get(
  '/dataobject',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let data = await db.any(
        `SELECT data_object from global_data_object
                UNION ALL
             SELECT data_object from course where course.id =$(id_course) AND data_object IS NOT NULL`,
        {
          id_course: req.session.currCourseId,
        }
      );
      res.appRender(
        'dataObjectView',
        {
          global_data_object: data[0] ? data[0].data_object : '{}',
          course_data_object: data[1] ? data[1].data_object : '{}',
        },
        'dataObjectViewScript'
      );
    } catch (error) {
      winston.error(error);
      next(error);
    }
  }
);
router.post(
  '/dataobject',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let result = await db.result(
        `UPDATE course SET data_object = $(data_object) WHERE course.id =$(id_course);`,
        {
          data_object: req.body.course_data_object,
          id_course: req.session.currCourseId,
        }
      );
      if (result && result.rowCount === 1) {
        req.flash('info', `Updated!`);
      } else {
        req.flash('info', 'Unexpected rows affected: ' + JSON.stringify(result));
      }
    } catch (error) {
      winston.error(error);
      req.flash('error', error);
    }
    res.redirect('/course/dataobject');
  }
);

module.exports = router;
