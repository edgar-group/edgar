var express = require('express');
var router = express.Router();
var winston = require('winston');
var mailer = require.main.require('./common/mailer');
var errors = require('../common/errors');
var config = require.main.require('./config/config');
const db = require('../db').db;

router.route('/onemail/:id').get(function (req, res, next) {
  db.any(
    `SELECT email, first_name, id
                FROM student
                WHERE id = $(id)`,
    { id: req.params.id }
  )
    .then((data) => {
      data.forEach(function (row) {
        if (row.email && row.first_name && row.id) {
          mailer.sendRegisterMail(
            req.session.passport.user,
            row.email,
            row.first_name,
            row.id,
            req
          );
        }
      });
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching user data: ' + error.message)
      );
    });
  req.flash('info', 'Mail sent');
  res.redirect('/admin/student/list');
});

router.route('/groupmail/').get(function (req, res, next) {
  db.any(
    `SELECT student.email, student.first_name, student.id
                  FROM student_course
                  JOIN student ON student_course.id_student = student.id
                  WHERE id_course = $(cId)
                    AND id_academic_year = $(yId)`,
    {
      cId: req.session.currCourseId,
      yId: req.session.currAcademicYearId,
    }
  )
    .then((data) => {
      data.forEach(function (row) {
        if (row.email && row.first_name && row.id) {
          mailer.sendRegisterMail(
            req.session.passport.user,
            row.email,
            row.first_name,
            row.id,
            req
          );
        }
      });
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching user data: ' + error.message)
      );
    });
  req.flash('info', 'Mails sent');
  res.redirect('/admin/student/list');
});

module.exports = router;
