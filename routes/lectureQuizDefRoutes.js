'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var db = require('../db').db;

router.post('/new', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  if (req.body.id_test_type) {
    db.func('new_test', [
      req.body.id_test_type,
      'Generated, please update...',
      req.session.currCourseId,
      req.session.currAcademicYearId,
      req.session.appUserId,
    ])
      .then(function (data) {
        winston.debug('new_test returned: ' + data);
        req.flash('info', 'New quiz created, please edit.');
        res.redirect('/test/def/edit/' + data[0].new_test);
      })
      .catch(function (error) {
        winston.error(error);
        res.errRender(error);
      });
  } else {
    req.flash('error', 'Test type NOT set.');
    res.redirect('/test/def/new');
  }
});
router.get('/list', middleware.requireRole(globals.ROLES.TEACHER), async function (req, res, next) {
  try {
    let test_types = await db.any(`SELECT id as value,
                       type_name as name
                FROM test_type
                WHERE type_name = 'Lecture quiz' `);
    let data = await db.any(
      `SELECT test.*, left(password, 3) || '***' as spassword,
                    (ts_available_from :: timestamp) :: varchar as ts_from,
                    (ts_available_to :: timestamp) :: varchar as ts_to,
                    test_type.type_name,
                    (first_name || ' ' || last_name) as user_created
              FROM test
              JOIN test_type
                ON test.id_test_type = test_type.id
               AND type_name = 'Lecture quiz'  
              JOIN app_user
                ON test.id_user_created = app_user.id
             WHERE id_course = $(cId)
               AND id_academic_year = $(yId)
          ORDER BY test_ordinal`,
      {
        cId: req.session.currCourseId,
        yId: req.session.currAcademicYearId,
      }
    );
    res.appRender('quizDefView', {
      test_types: test_types,
      tests: {
        headers: [
          {
            id: 'Id',
          },
          {
            title: 'Title',
          },
          {
            user_created: 'User create',
          },
          {
            type_name: 'Test type',
          },
          {
            test_ordinal: 'Test ordinal',
          },
          {
            max_runs: 'Max runs (0=∞)',
          },
          {
            show_results: 'Show results',
          },
          {
            max_score: 'Max score',
          },
          {
            spassword: 'Password',
          },
          {
            questions_no: 'No of qs',
          },
          {
            duration_seconds: 'Duration (s)',
          },
          {
            pass_percentage: 'Pass %',
          },
          {
            ts_from: 'Valid from',
          },
          {
            ts_to: 'Valid to',
          },
        ],
        rows: data,
        buttons: [
          {
            label: 'Delete',
            method: 'POST',
            action: function (row) {
              return '/test/def/delete/' + row['id'];
            },
            class: function (row) {
              return 'btn btn-outline-danger';
            },
            props: {
              edgar_confirm_text: 'Do you really want to delete this quiz?',
            },
          },
          {
            label: 'Edit',
            method: 'GET',
            action: function (row) {
              return '/test/def/edit/' + row['id'];
            },
          },
        ],
      },
    });
  } catch (error) {
    winston.error(error);
    req.flash('error', JSON.stringify(error));
    res.redirect('/');
  }
});

module.exports = router;
