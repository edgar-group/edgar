'use strict';
var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var utils = require('../common/utils');
var RunningInstances = require('../common/runningInstances');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
const moment = require('moment');

router.get(
  '/isresolved',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    winston.debug(
      `Ticket resolved?: ${moment().format('YYYY-MM-DD, h:mm:ss')} ${req.session.passport.user}`
    );
    let id_test_instance =
      (await RunningInstances.getCurrTestInstanceId(req, false)) ||
      (await RunningInstances.getCurrIdTestInstanceRev(req, false));
    if (id_test_instance) {
      let ticket = await db.oneOrNone(
        `SELECT ticket_message.id, ordinal
                    FROM ticket_message
                    JOIN test_instance
                      ON ticket_message.id_test = test_instance.id_test
                     AND ticket_message.id_student = test_instance.id_student
                   WHERE ts_sent is null
                     AND test_instance.id = $(id_test_instance)
                    ORDER BY ticket_message.ts_created
                    LIMIT 1`,
        {
          id_test_instance: id_test_instance,
        }
      );

      res.json({
        done: true,
        ticket_question_ordinal: ticket && ticket.ordinal,
      });

      if (ticket && ticket.id) {
        await db.none(
          `UPDATE ticket_message
              SET ts_sent = CURRENT_TIMESTAMP
              WHERE id = $(id)`,
          {
            id: ticket.id,
          }
        );
      }
    } else {
      // Apparently, this CAN happen. Not entirely sure how, maybe when users leaves the test open for a looong time and the session expires?
      winston.error(
        'Unknown req.session.id_test_instance/rev in ticketresloved, user: ' +
          req.session.passport.user
      );
      res.json({
        done: true,
        error: 'Session expired? Please login and reload your test.',
      });
    }
  }
);
router.post(
  '/raise',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let ticket = req.body;
      let id_test_instance =
        (await RunningInstances.getCurrTestInstanceId(req, false)) ||
        (await RunningInstances.getCurrIdTestInstanceRev(req, false));
      if (ticket.ordinal && ticket.description) {
        ticket.ts_created = new Date();
        //ticket.description = html.encode(ticket.description);
        let existing = await db.one(
          `SELECT id_test_instance_question
            FROM test_instance_question
      LEFT JOIN ticket
              ON ticket.id_test_instance_question = test_instance_question.id
          WHERE id_test_instance = $(id_test_instance)
            AND ordinal = $(ordinal)`,
          {
            id_test_instance: id_test_instance,
            ordinal: ticket.ordinal,
          }
        );
        let recAff;
        if (existing.id_test_instance_question) {
          recAff = await db.result(
            `UPDATE ticket
                SET status = 'reopened',
                    comments = array_append(comments, $(entry)::json)
              WHERE id_test_instance_question = $(id_test_instance_question)
              `,
            {
              id_test_instance_question: existing.id_test_instance_question,
              entry: JSON.stringify(ticket),
            }
          );
        } else {
          recAff = await db.result(
            `INSERT INTO ticket (id_test_instance_question, comments)
                                    VALUES ((SELECT
                                            tiq.id
                                            FROM test_instance_question tiq
                                            WHERE tiq.id_test_instance = $(id_test_instance)
                                            AND tiq.ordinal = $(ordinal)),
                                        array[$(entry)]::json[])
                                        `,
            {
              id_test_instance: id_test_instance,
              ordinal: ticket.ordinal,
              entry: JSON.stringify(ticket),
            }
          );
        }

        res.json({
          success: recAff.rowCount === 1,
        });
      } else {
        winston.error('Invalid ticket payload:' + JSON.stringify(ticket));
        res.json({
          success: false,
          error: 'Ticket payload is invalid. Please provide the description of the issue.',
        });
      }
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: JSON.stringify(error),
      });
    }
  }
);
router.get(
  '/statuses',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let id_test_instance =
        (await RunningInstances.getCurrTestInstanceId(req, false)) ||
        (await RunningInstances.getCurrIdTestInstanceRev(req, false));

      let test = await db.one(
        `SELECT ARRAY_AGG(ticket.status::text ORDER BY ordinal)  as ticket_statuses
                            FROM test_instance_question
                            LEFT JOIN ticket ON test_instance_question.id = ticket.id_test_instance_question
                            WHERE test_instance_question.id_test_instance = $(id_test_instance)
                  `,
        {
          id_test_instance: id_test_instance,
        }
      );

      res.json({
        done: true,
        ticket_statuses: JSON.stringify(test.ticket_statuses),
      });
    } catch (error) {
      winston.error(
        'An error occured while fetching ticketstatuses (' +
          req.session.id_test_instance +
          '): ' +
          error.message
      );
      res.json({
        success: false,
        error:
          process.env.NODE_ENV !== 'development' ? 'Logged at the server' : error.message || error,
      });
    }
  }
);
router.get(
  '/conversation/:ordinal([0-9]{1,2})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    let id_test_instance =
      (await RunningInstances.getCurrTestInstanceId(req, false)) ||
      (await RunningInstances.getCurrIdTestInstanceRev(req, false));
    try {
      let ticket = await db.any(
        `
                SELECT ticket.comments
                  FROM test_instance_question
                  JOIN ticket
                    ON ticket.id_test_instance_question = test_instance_question.id
                WHERE id_test_instance = $(id_test_instance)
                  AND ordinal = $(ordinal)
                `,
        {
          id_test_instance: id_test_instance,
          ordinal: req.params.ordinal,
        }
      );
      if (ticket && ticket[0])
        res.json({
          success: true,
          conversation: utils.formatTicketConversation(ticket[0].comments),
        });
      else
        res.json({
          success: false,
          conversation: 'No comments.',
        });
    } catch (err) {
      winston.error(err);
      res.send('Error occured fetchin ticket conversation.');
    }
  }
);

// ************** Server side:
router.get(
  '/mytickets',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      let assignedTicketsList;
      let tickets = await db.any(
        `
        SELECT student.id,
            student.first_name,
            student.last_name,
            student.alt_id,
            student.alt_id2,
            test.title,
            test_instance_question.id_question,
            test_instance_question.ordinal,
            ticket.status,
            ticket.ts_created::timestamp(0)::varchar as ts_created,
            ticket.ts_modified::timestamp(0)::varchar as ts_modified,
            ticket.id as id_ticket,
            ticket.comments
          FROM test_instance
          JOIN test
            ON test_instance.id_test = test.id
          JOIN test_instance_question
            ON test_instance_question.id_test_instance = test_instance.id
          JOIN ticket
            ON ticket.id_test_instance_question = test_instance_question.id
          JOIN student
            ON test_instance.id_student = student.id
        LEFT JOIN app_user
                ON ticket.id_assigned_user = app_user.id
        WHERE test_instance.id_student = $(id_student)
          AND test.id_academic_year = $(id_academic_year)
          AND test.id_course = $(id_course)

        ORDER BY ticket.status, ticket.ts_modified DESC`,
        {
          id_student: req.session.studentId,
          id_academic_year: req.session.currAcademicYearId,
          id_course: req.session.currCourseId,
        }
      );
      if (req.session.rolename === globals.ROLES.TEACHER) {
        let assignedTickets = await db.any(
          `
          SELECT student.id,
              student.first_name,
              student.last_name,
              student.alt_id,
              student.alt_id2,
              test.title,
              test_instance_question.id_question,
              test_instance_question.ordinal,
              ticket.status,
              ticket.ts_created::timestamp(0)::varchar as ts_created,
              ticket.ts_modified::timestamp(0)::varchar as ts_modified,
              ticket.id as id_ticket,
              ticket.comments
            FROM test_instance
            JOIN test
              ON test_instance.id_test = test.id
            JOIN test_instance_question
              ON test_instance_question.id_test_instance = test_instance.id
            JOIN ticket
              ON ticket.id_test_instance_question = test_instance_question.id
            JOIN student
              ON test_instance.id_student = student.id
          LEFT JOIN app_user
                  ON ticket.id_assigned_user = app_user.id
          WHERE ticket.id_assigned_user = $(id_assigned_user)
            AND test.id_academic_year = $(id_academic_year)
            AND test.id_course = $(id_course)

          ORDER BY ticket.status, ticket.ts_modified DESC`,
          {
            id_assigned_user: req.session.appUserId,
            id_academic_year: req.session.currAcademicYearId,
            id_course: req.session.currCourseId,
          }
        );
        assignedTicketsList = {
          headers: [
            {
              title: 'Exam',
            },
            {
              ordinal: 'Q.no.',
            },
            {
              ts_created: 'Ticket created',
            },
            {
              ts_modified: 'Ticket modified',
            },
          ],
          rows: assignedTickets,
          rawhtml: [
            {
              label: 'Status',
              __html: function (row) {
                let badgeClass = '';
                if (row.status === 'open') {
                  badgeClass = row.assigned ? 'badge-warning' : 'badge-danger';
                } else if (row.status === 'reopened') {
                  badgeClass = 'badge-warning';
                } else if (row.status === 'closed') {
                  badgeClass = 'badge-success';
                } else {
                  badgeClass = 'badge-info';
                  row.status = '?? ' + row.status;
                }
                return `<h3><span class="badge ${badgeClass}">${row.status}</span></h3>`;
              },
            },
            {
              label: 'Description',
              __html: function (row) {
                return `<div>${utils.formatTicketConversation(row.comments)}<div>`;
              },
            },
          ],
        };
      }
      res.appRender('myTicketsView', {
        assignedTicketsList: assignedTicketsList,
        title:
          tickets && tickets[0]
            ? `Tickets for student ${tickets[0].first_name} ${tickets[0].last_name}: `
            : 'No tickets for this student.',
        list: {
          headers: [
            {
              title: 'Exam',
            },
            {
              ordinal: 'Q.no.',
            },
            {
              ts_created: 'Ticket created',
            },
            {
              ts_modified: 'Ticket modified',
            },
          ],
          rows: tickets,
          rawhtml: [
            {
              label: 'Status',
              __html: function (row) {
                let badgeClass = '';
                if (row.status === 'open') {
                  badgeClass = row.assigned ? 'badge-warning' : 'badge-danger';
                } else if (row.status === 'reopened') {
                  badgeClass = 'badge-warning';
                } else if (row.status === 'closed') {
                  badgeClass = 'badge-success';
                } else {
                  badgeClass = 'badge-info';
                  row.status = '?? ' + row.status;
                }
                return `<h3><span class="badge ${badgeClass}">${row.status}</span></h3>`;
              },
            },
            {
              label: 'Description',
              __html: function (row) {
                //let buff = Buffer.from(utils.formatTicketConversation(row.comments), 'utf-8');
                return `<div>${utils.formatTicketConversation(row.comments)}<div>`;
              },
            },
          ],
        },
      });
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('back');
    }
  }
);
router.get(
  '/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let tickets = await db.any(
        `
          SELECT student.id,
              student.first_name || ' ' || student.last_name as student,
              student.alt_id,
              student.alt_id2,
              test.title,
              -- ts_started::timestamp(0)::varchar,
              test_instance_question.id_question,
              test_instance_question.ordinal,
              ticket.status,
              ticket.ts_created::timestamp(0)::varchar || ' / ' ||
              ticket.ts_modified::timestamp(0)::varchar as ts_created_modified,
              substring(app_user.first_name from 1 for 1) || '. ' || app_user.last_name as assigned,
              substring(user_created.first_name from 1 for 1) || '. ' || user_created.last_name || ' / ' ||
              substring(user_modified.first_name from 1 for 1) || '. ' || user_modified.last_name as user_created_modified,
              app_user.alt_id2 as assigned_alt_id2,
              id_assigned_user,
              ticket.id as id_ticket,
              ticket.comments,
              test_instance.id as id_test_instance,
              test_instance.ts_submitted,
              substring(manual_app_user.first_name from 1
                for 1) || '. ' || manual_app_user.last_name as manual_grader,
              manual_app_user.alt_id2 as manual_grader_alt_id2
            FROM test_instance
            JOIN test
              ON test_instance.id_test = test.id
            JOIN test_instance_question
              ON test_instance_question.id_test_instance = test_instance.id
            JOIN ticket
              ON ticket.id_test_instance_question = test_instance_question.id
            JOIN student
              ON test_instance.id_student = student.id
            JOIN question
              ON test_instance_question.id_question = question.id
            JOIN app_user user_created
              ON question.id_user_created = user_created.id
            JOIN app_user user_modified
              ON question.id_user_modified = user_modified.id
          LEFT JOIN app_user
                  ON ticket.id_assigned_user = app_user.id
          LEFT JOIN test_instance_question_manual_grade
                  ON test_instance_question.id = test_instance_question_manual_grade.id_test_instance_question
          LEFT JOIN app_user manual_app_user
                  ON test_instance_question_manual_grade.id_app_user = manual_app_user.id
          where id_test = $(id_test)
          ORDER BY ticket.status, ticket.ts_modified DESC`,
        {
          id_test: req.params.id_test,
        }
      );
      let open = tickets.filter((x) => x.status.indexOf('open') >= 0).length;

      let rawhtml = [
        {
          label: '************** Ticket conversation  **************',
          __html: function (row) {
            // let buff = Buffer.from(utils.formatTicketConversation(row.comments), 'utf-8');
            let comments = utils.formatTicketConversation(row.comments);
            let lastComment = row.comments[row.comments.length - 1];
            let title =
              '<b>' +
              moment(lastComment.ts_created).fromNow() +
              '</b>: ' +
              (lastComment.description || lastComment.reply);
            if (title.length > 50) title = title.substring(0, 50) + '...';
            return `<details>
                      <summary>${title}</summary>
                      <div style="font-size: 0.8em; white-space: pre;" class="mt-3">${comments}</div>
                    </details>`;
          },
        },
        {
          label: '*********** Handle ticket here ***********',
          __html: function (row) {
            if (row.id_assigned_user === req.session.appUserId && row.status.indexOf('open') >= 0) {
              return `<form action="/ticket/close/${row.id_ticket}" method="POST">
                                            <textarea class="form-control" style="min-width:500px;"
                                                name="comment"
                                                rows = "3"
                                                placeholder="Provide clear reply here. Use plain text."></textarea>
                                            <button edgar_confirm_text = "Are you sure?"
                                                class="btn btn-primary"
                                                type="submit"> Close ticket
                                            </button>
                                        </form>`;
            } else if (row.id_assigned_user === req.session.appUserId) {
              return `<form action="/ticket/reopen/${row.id_ticket}" method="POST">
                          <button edgar_confirm_text = "Are you sure?"
                              class="btn btn-outline-warning"
                              type="submit"> Re-open ticket
                          </button>
                      </form>`;
            } else {
              return '';
            }
          },
        },
        {
          label: 'Review',
          __html: function (row) {
            if (row.ts_submitted) {
              return `<a class="btn btn-outline-success" target="_blank" href="/exam/review/${row.id_test_instance}/${row.ordinal}">Review</a>`;
            } else {
              return 'Not submitted';
            }
          },
          __raw: true,
        },
        // {
        //   label: 'Client-side log',
        //   __html: function (row) {
        //     return `<a class="btn btn-outline-success" target="_blank" href="/test/instance/${row.id_test_instance}/details/">what they see...</a>`;
        //   },
        //   __raw: true,
        // },
        {
          label: 'Status',
          __html: function (row) {
            let badgeClass = '';
            if (row.status === 'open') {
              badgeClass = row.assigned ? 'badge-warning' : 'badge-danger';
            } else if (row.status === 'reopened') {
              badgeClass = 'badge-warning';
            } else if (row.status === 'closed') {
              badgeClass = 'badge-success';
            } else {
              badgeClass = 'badge-info';
              row.status = '?? ' + row.status;
            }
            return `<h3><span class="badge ${badgeClass}">${row.status}</span></h3>`;
          },
        },
        {
          label: 'Assigned',
          __html: function (row) {
            if (row.assigned) {
              return `<div class="d-flex">
                      <div>
                        <form action="/ticket/reassign/${row.id_ticket}" method="POST">
                            <button edgar_confirm_text = "Are you sure you want to take over this ticket?"
                                title = "Take over: reassign to me!"
                                class="btn btn-outline-danger"
                                type="submit"><i class="fas fa-people-arrows"></i>
                            </button>
                        </form>
                      </div>
                      <div>${utils.getTinyImageHtml(row.assigned_alt_id2)} &nbsp; ${
                row.assigned
              }</div>
                      </div>`;
            } else {
              return `<form action="/ticket/assign/${row.id_ticket}" method="POST">
                          <button edgar_confirm_text = "Are you sure?"
                              class="btn btn-outline-danger"
                              type="submit"> Assign to me
                          </button>
                      </form>`;
            }
          },
        },
      ];
      let hasManualGraders = tickets.filter((x) => x.manual_grader_alt_id2).length > 0;
      if (hasManualGraders) {
        rawhtml.push({
          label: 'Manual grader',
          __html: function (row) {
            if (row.manual_grader_alt_id2) {
              return (
                utils.getTinyImageHtml(row.manual_grader_alt_id2) + '&nbsp;' + row.manual_grader
              );
            } else {
              return '';
            }
          },
        });
      }
      res.appRender('examTicketsView', {
        id_test: req.params.id_test,
        autorefresh: req.query.ar,
        title: `Tickets for exam "${tickets && tickets[0] && tickets[0].title}": `,
        pageTitle: `Tickets: ${open}/${tickets.length}`,
        list: {
          headers: [
            // {
            //   id: 'stud.Id',
            // },
            {
              id: 'Student',
              __raw: true,
              __formatter: function (row) {
                return (
                  'ID: ' +
                  row.id +
                  '<br>' +
                  utils.getTinyImageHtml(row.alt_id2) +
                  '<br>' +
                  row.student
                );
              },
            },
            // {
            //   student: 'Student',
            // },
            // {
            //   __raw: true,
            //   ts_created_modified: 'Ticket cr/mod',
            //   __formatter: function (row) {
            //     return `<small>${row.ts_created_modified.replace('/', '<br>')}</small>`;
            //   },
            // },
            {
              __raw: true,
              user_created_modified: 'Question',
              __formatter: function (row) {
                return (
                  'Ordinal: <span class="badge badge-primary">' +
                  row.ordinal +
                  '</span><br>' +
                  row.user_created_modified.replace('/', '<br>') +
                  '<br><a href="/question/edit/' +
                  row.id_question +
                  '" class="btn btn-outline-success" role="button" target="_blank">' +
                  row.id_question +
                  '</a>'
                );
              },
            },
            {
              __raw: true,
              id_test_instance: 'Stalk',
              __formatter: function (row) {
                return `<a class="btn btn-outline-success" target="_blank" href="/test/instances/stalk/id/${row.id_test_instance}"><i class="fa fa-2 fa-eye"></i></a>
                   <br><a class="btn btn-outline-success" target="_blank" href="/test/instance/${row.id_test_instance}/details/">what they see...</a>`;
              },
            },
            // {
            //   ordinal: 'Ordinal',
            // },
          ],
          rows: tickets,
          rawhtml,
          // links: [
          //   // {
          //   //   __formatter: function (row) {
          //   //     return row.id_question;
          //   //   },
          //   //   target: '_blank',
          //   //   label: 'Question',
          //   //   href: function (row) {
          //   //     return `/question/edit/${row.id_question}`;
          //   //   },
          //   // },
          //   {
          //     __formatter: function (row) {
          //       return '<i class="fa fa-2 fa-eye"></i>';
          //     },
          //     target: '_blank',
          //     label: 'Stalk (logs)',
          //     __raw: true,
          //     href: function (row) {
          //       return (
          //         `/test/instances/stalk/id/${row.id_test_instance}` +
          //         `<a class="btn btn-outline-success" target="_blank" href="/test/instance/${row.id_test_instance}/details/">what they see...</a>`
          //       );
          //     },
          //   },
          // ],
        },
      });
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('back');
    }
  }
);
router.post(
  '/close/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      try {
        let ticket = {
          ts_created: new Date(),
          reply: req.body.comment,
        };

        let recAff = await db.result(
          `
          BEGIN WORK;
              UPDATE ticket
                    SET status = 'closed',
                        comments = array_append(comments, $(entry)::json)
              WHERE id = $(id_ticket)
                AND id_assigned_user = $(id_user);
              ---
              INSERT INTO ticket_message (id_test, id_student, ordinal)
              SELECT id_test, id_student, ordinal
                FROM test_instance
                JOIN test_instance_question
                  ON test_instance.id = test_instance_question.id_test_instance
                JOIN ticket
                  ON test_instance_question.id  = ticket.id_test_instance_question
                WHERE ticket.id = $(id_ticket)
                  AND status = 'closed';
          COMMIT WORK;`,
          {
            id_user: req.session.appUserId,
            id_ticket: req.params.id_ticket,
            entry: JSON.stringify(ticket),
          }
        );

        if (recAff.rowCount === 0) {
          req.flash('info', 'CLosing failed.');
        } else {
          req.flash('info', 'Ticket closed.');
        }
      } catch (error) {
        req.flash('error', `${error} <br/> ${JSON.stringify(error)}`);
        winston.error(error);
      }
      res.redirect('back');
    }
  }
);
router.post(
  '/reopen/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      try {
        let ticket = {
          ts_created: new Date(),
          reply: 'Ticket reopened by teacher.',
        };

        let recAff = await db.result(
          `
          BEGIN WORK;
              UPDATE ticket
                    SET status = 'reopened',
                        comments = array_append(comments, $(entry)::json)
              WHERE id = $(id_ticket)
                AND id_assigned_user = $(id_user);
              ---
              INSERT INTO ticket_message (id_test, id_student, ordinal)
              SELECT id_test, id_student, ordinal
                FROM test_instance
                JOIN test_instance_question
                  ON test_instance.id = test_instance_question.id_test_instance
                JOIN ticket
                  ON test_instance_question.id  = ticket.id_test_instance_question
                WHERE ticket.id = $(id_ticket)
                  AND status = 'reopened';
          COMMIT WORK;`,
          {
            id_user: req.session.appUserId,
            id_ticket: req.params.id_ticket,
            entry: JSON.stringify(ticket),
          }
        );

        if (recAff.rowCount === 0) {
          req.flash('info', 'Reopening failed.');
        } else {
          req.flash('info', 'Ticket reopened.');
        }
      } catch (error) {
        req.flash('error', `${error} <br/> ${JSON.stringify(error)}`);
        winston.error(error);
      }
      res.redirect('back');
    }
  }
);

const assignTicket = async (req, res, is_reassign) => {
  try {
    let recAff = await db.result(
      `UPDATE ticket
              SET id_assigned_user = $(id_user)
            WHERE id = $(id_ticket)
              ${is_reassign ? '' : ' AND id_assigned_user IS NULL'}
                `,
      {
        id_user: req.session.appUserId,
        id_ticket: req.params.id_ticket,
      }
    );
    if (recAff.rowCount === 0) {
      req.flash('info', 'Somebody already took that ticket before you.');
    } else {
      req.flash('info', 'Ticket assigned.');
    }
  } catch (error) {
    winston.error(error);
    req.flash('error', JSON.stringify(error));
  }
  res.redirect('back');
};

router.post(
  '/assign/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      return assignTicket(req, res, false);
    }
  }
);
router.post(
  '/reassign/:id_ticket([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_ticket) {
      return assignTicket(req, res, true);
    }
  }
);

router.get(
  '/subscriptions/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    let id_test = req.params.id_test;
    try {
      let subs = await db.any(
        `SELECT
          all_questions.id_question,
          ucre.first_name || ' ' || ucre.last_name as user_created,
          umod.first_name || ' ' || umod.last_name as user_modified,
          SUBSTRING(question.question_text from 1 for 100) || '...' as question_text,
          ticket_subscription.ts_created::timestamptz::text as sub_ts_created,
          (SELECT COUNT( * ) FROM ticket_subscription WHERE id_test = $(id_test) AND id_app_user = $(id_app_user) AND id_question IS NULL) as is_all,
          me.email,
          (SELECT string_agg(SUBSTRING(otherteacher.first_name from 1 for 1) || ' ' || otherteacher.last_name, ', '
                  ORDER BY otherteacher.last_name)
              FROM ticket_subscription
              JOIN app_user otherteacher
                ON ticket_subscription.id_app_user = otherteacher.id
            WHERE id_test = $(id_test)
              AND id_app_user <> $(id_app_user)
              AND (id_question IS NULL or id_question = question.id)) as others
          FROM
              (SELECT test_question.id_question
                  FROM test_question
                WHERE id_test = $(id_test)
              UNION
              SELECT question_node.id_question
                FROM test_part
                JOIN node
                  ON test_part.id_node = node.id
                JOIN question_node
                  ON question_node.id_node = node.id
                WHERE id_test = $(id_test)
              UNION
              SELECT peer_test_group_question.id_question
                FROM peer_test_group_question
                JOIN peer_test_group
                  ON peer_test_group.id = peer_test_group_question.id_peer_test_group
                JOIN peer_test
                  ON peer_test_group.id_peer_test = peer_test.id
                JOIN test
                  ON test.id = $(id_test)
                  AND test.id_parent = peer_test.id_test_phase_2
              ) all_questions
              JOIN question
                ON all_questions.id_question = question.id
              JOIN app_user ucre
                ON id_user_created = ucre.id
              JOIN app_user umod
                ON id_user_modified = umod.id
              JOIN app_user me
                ON me.id = $(id_app_user)
              LEFT JOIN ticket_subscription
                      ON ticket_subscription.id_test = $(id_test)
                    AND id_app_user = $(id_app_user)
                    AND (ticket_subscription.id_question is null or ticket_subscription.id_question = question.id)

              WHERE is_active
              ORDER BY id_question
                `,
        {
          id_test,
          id_app_user: req.session.appUserId,
        }
      );
      subs.forEach(function (row) {
        row.question_link = `<a href="/question/edit/${row.id_question}" target="_blank">${row.id_question}</a>`;
      });
      res.appRender('ticketSubscriptionsView', {
        id_test,
        is_all: subs[0].is_all,
        pageTitle: 'Ticket subscriptions',
        list: {
          headers: [
            {
              question_link: 'Question',
              __raw: true,
            },
            {
              user_created: 'Created',
            },
            {
              user_modified: 'Modified',
            },
            {
              question_text: 'Question text',
            },
            {
              sub_ts_created: 'Subscribed at',
            },
            {
              email: 'Address used',
            },
            {
              others: 'Other subscribers',
            },
          ],
          rows: subs,
          buttons: [
            {
              label: '(Un)subscribe',
              flabel: function name(row) {
                return row.sub_ts_created ? 'Unsubscribe' : 'Subscribe';
              },
              method: 'POST',
              action: function (row) {
                return `/ticket/${
                  row.sub_ts_created ? 'un' : ''
                }subscribe/test/${id_test}/question/${row.id_question}`;
              },
              class: function (row) {
                return 'btn ' + (row.sub_ts_created ? 'btn-warning' : 'btn-success');
              },
            },
          ],
        },
      });
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('back');
    }
  }
);
router.post(
  '/subscribe/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let recAff = await toggleSubscription(
        req.params.id_question,
        req.params.id_test,
        req.session.appUserId,
        true
      );
      req.flash('info', 'Subscriptions updated (' + recAff + ')');
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
    }
    res.redirect('back');
  }
);
router.post(
  '/unsubscribe/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let recAff = await toggleSubscription(
        req.params.id_question,
        req.params.id_test,
        req.session.appUserId,
        false
      );
      req.flash('info', 'Subscriptions updated (' + recAff + ')');
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
    }
    res.redirect('back');
  }
);

async function toggleSubscription(id_question, id_test, id_app_user, on) {
  let recAff = await db.result(
    `DELETE FROM ticket_subscription
      WHERE id_test = $(id_test)
      AND id_question = $(id_question)
      AND id_app_user = $(id_app_user)
          `,
    {
      id_app_user,
      id_question,
      id_test,
    }
  );
  if (on) {
    recAff = await db.result(
      `INSERT INTO ticket_subscription (id_test, id_question, id_app_user)
                                        VALUES($(id_test), $(id_question), $(id_app_user));
                                            `,
      {
        id_app_user,
        id_question,
        id_test,
      }
    );
  }
  return recAff;
}

router.post(
  '/subscribeall/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let recAff = await db.result(
        `DELETE FROM ticket_subscription
          WHERE id_test = $(id_test)
          AND id_app_user = $(id_app_user)`,
        {
          id_app_user: req.session.appUserId,
          id_test: req.params.id_test,
        }
      );
      recAff = await db.result(
        `INSERT INTO ticket_subscription(id_test, id_app_user)
          VALUES($(id_test), $(id_app_user));
          `,
        {
          id_app_user: req.session.appUserId,
          id_test: req.params.id_test,
        }
      );
      req.flash('info', 'Subscriptions updated (' + recAff + ')');
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
    }
    res.redirect('back');
  }
);
router.post(
  '/unsubscribeall/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let recAff = await db.result(
        `DELETE FROM ticket_subscription
          WHERE id_test = $(id_test)
          AND id_app_user = $(id_app_user)`,
        {
          id_app_user: req.session.appUserId,
          id_test: req.params.id_test,
        }
      );
      req.flash('info', 'Subscriptions updated (' + recAff + ')');
    } catch (error) {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
    }
    res.redirect('back');
  }
);
// *********************

module.exports = router;
