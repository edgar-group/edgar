'use strict';
var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var db = require('../db').db;
var moment = require('moment');
const tutorialService = require('../services/tutorialService2');
const exerciseService = require('../services/exerciseService');
var OnlineUsers = require.main.require('./models/OnlineUsersModel.js');

router.get(
  '/',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    if (req.session.id_test_instance_rev || req.session.id_test_instance) {
      if (req.session.id_test_instance_rev) {
        let newArray = req.session.running_rev_instances
          ? req.session.running_rev_instances.filter(function (item) {
              return item.id_test_instance_rev != req.session.id_test_instance_rev;
            })
          : [];
        req.session.running_rev_instances = newArray;
        delete req.session.id_test_instance_rev;
      }
    }
    winston.debug('Jello world for: ' + req.user);
    let viewdata = {
      isTeacher: req.session.rolename === globals.ROLES.TEACHER,
    };

    try {
      if (req.session.rolename === globals.ROLES.TEACHER) {
        let upcomingExams =
          await db.any(`SELECT course.course_name, title, test_type.type_name, ts_available_from::timestamp(0)::varchar, ts_available_to::timestamp(0)::varchar, duration_seconds
                    , (SELECT COUNT(*) FROM student_course WHERE id_course = test.id_course AND id_academic_year = test.id_academic_year AND class_group <> 'Teachers' ) as enrolled
                    , SUM ( CASE WHEN (ts_started IS NOT NULL AND class_group <> 'Teachers') THEN 1 ELSE 0 END) as started
                    , SUM ( CASE WHEN (ts_submitted IS NOT NULL AND class_group <> 'Teachers') THEN 1 ELSE 0 END) as submitted
                FROM test
                JOIN course
                ON test.id_course = course.id
                JOIN test_type
                ON test.id_test_type = test_type.id
                LEFT JOIN test_instance
                ON test_instance.id_test = test.id
                LEFT JOIN student
                    ON test_instance.id_student = student.id
                LEFT JOIN student_course
                    ON student.id = student_course.id_student
                    AND student_course.id_course = test.id_course
                    AND student_course.id_academic_year = test.id_academic_year
                WHERE ts_available_from between CURRENT_TIMESTAMP + '-1 day'::interval and CURRENT_TIMESTAMP + '7 days'::interval
                  AND NOT test.is_global
                GROUP BY course.course_name, test.id_course, test.id_academic_year, test.title, test_type.type_name, ts_available_from, ts_available_to, duration_seconds
                ORDER BY ts_available_from
                `);
        viewdata.upcomingExams = {
          headers: [
            {
              course_name: 'Course',
            },
            {
              title: 'Exam title',
            },
            {
              type_name: 'Exam type',
            },
            {
              ts_available_from: 'Available from',
            },
            {
              ts_available_to: 'Available to',
            },
            {
              duration_seconds: 'Duration (sec)',
            },
            {
              enrolled: 'Enrolled',
            },
            {
              started: 'Started',
            },
            {
              submitted: 'Submitted',
            },
          ],
          rows: upcomingExams,
          class: 'table table-sm table-hover edgar no-dt',
        };
        let unresolvedTickets = await db.any(
          `SELECT id_test as id, test.title, count(*) as cnt, 'exam' as type
                    FROM ticket
                    JOIN test_instance_question
                      ON ticket.id_test_instance_question = test_instance_question.id
                    JOIN test_instance
                      ON test_instance_question.id_test_instance = test_instance.id
                    JOIN test
                      ON test_instance.id_test = test.id
                    WHERE id_course = $(id_course)
                      AND status <> 'closed'
                    GROUP by id_test, test.title, type
                UNION ALL
                   SELECT tutorial.id, tutorial.tutorial_title, count(*) as cnt, 'tutorial' as type
                    FROM tutorial_ticket
                    JOIN tutorial_step
                      ON tutorial_ticket.id_tutorial_step = tutorial_step.id
                    JOIN tutorial
                      ON tutorial_step.id_tutorial = tutorial.id
                    JOIN tutorial_course
                      ON tutorial.id = tutorial_course.id_tutorial
                    WHERE id_course = $(id_course)
                      AND status <> 'closed'
                    GROUP by tutorial.id, tutorial.tutorial_title, type
                    ORDER BY type, cnt DESC
                `,
          {
            id_course: req.session.currCourseId,
          }
        );
        if (unresolvedTickets.length > 0) {
          viewdata.unresolvedTickets = unresolvedTickets;
          viewdata.unresolvedTicketsCnt = unresolvedTickets
            .map((x) => parseInt(x.cnt))
            .reduce((a, b) => a + b, 0);
        }
      }
      let promises = [];
      promises.push(
        db.any(
          `SELECT
                academic_year.title as ac_year, test.title, test.max_runs, questions_no, duration_seconds,
                    test_score_ignored, forward_only, use_in_stats, is_global, is_public, password,
                    EXTRACT(EPOCH FROM(test.ts_available_to - CURRENT_TIMESTAMP)) as seconds_available_to,
                    ts_available_to::timestamp(0)::varchar,
                    SUM(CASE WHEN(ts_started IS NULL) THEN 0 ELSE 1 END) as no_started,
                    SUM(CASE WHEN(ts_submitted IS NULL) THEN 0 ELSE 1 END) as no_submitted,
                    homescreen_category.category,
                    homescreen_category.ordinal as category_ordinal,
                    homescreen_category.max_items_visible,
                    homescreen_exam_category.ordinal as exam_ordinal
                FROM test
                JOIN academic_year
                  ON test.id_academic_year = academic_year.id
                LEFT JOIN test_instance
                       ON test.id = test_instance.id_test
                      AND test_instance.id_student = $(id_student)
                LEFT JOIN homescreen_exam_category
                       ON test.id = homescreen_exam_category.id_test
                LEFT JOIN homescreen_category
                       ON homescreen_exam_category.id_homescreen_category = homescreen_category.id
             WHERE test.is_public
               AND test.id_course = $(id_course)
               AND (CURRENT_TIMESTAMP BETWEEN test.ts_available_from AND test.ts_available_to)
             GROUP BY academic_year.title, test.title, test.max_runs, questions_no, duration_seconds,
                 test_score_ignored, forward_only, use_in_stats, is_global, is_public, password, test.ts_available_to, test.ts_created
                 , homescreen_category.category, category_ordinal, max_items_visible, exam_ordinal
             ORDER BY category_ordinal, test.ts_created DESC `,
          {
            id_course: req.session.currCourseId,
            id_student: req.session.studentId,
          }
        )
      );

      promises.push(
        db.any(
          `SELECT test.id, title,
                        to_char(test_instance.ts_started, 'YYYY-MM-DD HH24:MI:SS') as ts_started,
                        to_char(ts_available_to, 'YYYY-MM-DD HH24:MI:SS') as ts_available_to,
                        CASE WHEN (CURRENT_TIMESTAMP BETWEEN ts_available_from AND ts_available_to) THEN true ELSE false END AS is_available
                  FROM test_instance
                  JOIN test ON test_instance.id_test = test.id
                 WHERE NOT test.is_public
                   AND id_student = $(id_student)
                   AND ts_submitted IS NULL
                   AND ts_started IS NOT NULL
                   AND id_academic_year = $(id_academic_year)
                   AND id_course = $(id_course)
                `,
          {
            id_student: req.session.studentId,
            id_academic_year: req.session.currAcademicYearId,
            id_course: req.session.currCourseId,
          }
        )
      );
      promises.push(
        db.one(`SELECT is_competitive FROM course WHERE id = $1`, [req.session.currCourseId])
      );

      promises.push(
        tutorialService.listTutorials(
          req.session.currCourseId,
          req.session.studentId,
          req.session.rolename === globals.ROLES.STUDENT
        )
      );

      promises.push(
        exerciseService.listExercises(
          req.session.currCourseId,
          req.session.currAcademicYearId,
          req.session.studentId,
          req.session.rolename === globals.ROLES.STUDENT
        )
      );
      if (req.session.rolename === globals.ROLES.TEACHER) {
        promises.push(
          db.any(
            `
        WITH last_two_weeks as (
            SELECT DATE_TRUNC('day', ts) AS day,
                  EXTRACT(HOUR FROM ts) AS hour
            FROM generate_series(current_timestamp - '1 week'::interval, current_timestamp, '1 hour'::interval) as ts)

        SELECT last_two_weeks.day::date, last_two_weeks.hour
            , count_submitted
            , count_started
            , count_submitted_course
            , count_started_course
          FROM last_two_weeks

        LEFT JOIN (
            SELECT
                DATE_TRUNC('day', ts_submitted) AS day,
                EXTRACT(HOUR FROM ts_submitted) AS hour,
                COUNT(*) AS count_submitted,
                SUM(CASE WHEN(id_course = $(id_course)) THEN 1 ELSE 0 END) as count_submitted_course
            FROM test_instance
            JOIN test on test_instance.id_test = test.id
            WHERE ts_submitted >= current_timestamp - INTERVAL '1 week'
            GROUP BY DATE_TRUNC('day', ts_submitted), EXTRACT(HOUR FROM ts_submitted)
            ) submitted
              ON submitted.day = last_two_weeks.day
              AND submitted.hour = last_two_weeks.hour

        LEFT JOIN (
            SELECT
                DATE_TRUNC('day', ts_started) AS day,
                EXTRACT(HOUR FROM ts_started) AS hour,
                COUNT(*) AS count_started,
                SUM(CASE WHEN(id_course = $(id_course)) THEN 1 ELSE 0 END) as count_started_course
            FROM test_instance
            JOIN test on test_instance.id_test = test.id
            WHERE ts_started >= current_timestamp - INTERVAL '1 week'
            GROUP BY DATE_TRUNC('day', ts_started), EXTRACT(HOUR FROM ts_started)
            ORDER BY day, hour
            ) started
              ON started.day = last_two_weeks.day
              AND started.hour = last_two_weeks.hour

        ORDER BY last_two_weeks.day::date, last_two_weeks.hour;`,
            {
              id_course: req.session.currCourseId,
            }
          )
        );
        promises.push(OnlineUsers.getOnlineUsersCount(15));
      }
      let [
        publicExams,
        unsubmittedTests,
        course,
        tutorials,
        exercises,
        startedSubmittedStats,
        onlineUsersCount,
      ] = await Promise.all(promises);

      viewdata.publicExams = {
        headers: [
          {
            title: 'Exam title, questions no',
            __formatter: function (row) {
              return `<b title="Defined in ${row.ac_year}">${row.title}</b> <br><i>${row.questions_no} question(s)</i>`;
            },
            __raw: true,
          },
          {
            max_runs: 'Runs/Max',
            __formatter: function (row) {
              return `${row.no_started}/${row.max_runs}`;
            },
          },
          {
            duration_seconds: 'Duration',
            __formatter: function (row) {
              return moment.duration({ seconds: row.duration_seconds }).humanize();
            },
          },
          {
            test_score_ignored: 'Score ignored',
            __formatter: function (row) {
              return row.test_score_ignored
                ? '<div class="text-center w-100"><i class="far fa-check-circle fa-2x" style="color:green"></i></div>'
                : '<div class="text-center w-100"><i class="far fa-times-circle fa-2x" style="color:orange"></i></div>';
            },
            __raw: true,
          },
          {
            forward_only: 'Forward only',
            __formatter: function (row) {
              return row.forward_only
                ? '<div class="text-center w-100"><i class="far fa-check-circle fa-2x" style="color:green"></i></div>'
                : '<div class="text-center w-100"><i class="far fa-times-circle fa-2x" style="color:orange"></i></div>';
            },
            __raw: true,
          },
          {
            use_in_stats: 'Used in stats',
            __formatter: function (row) {
              return row.use_in_stats
                ? '<div class="text-center w-100"><i class="far fa-check-circle fa-2x" style="color:green"></i></div>'
                : '<div class="text-center w-100"><i class="far fa-times-circle fa-2x" style="color:orange"></i></div>';
            },
            __raw: true,
          },
          {
            is_global: 'Global',
            __formatter: function (row) {
              return row.is_global
                ? '<div class="text-center w-100"><i class="far fa-check-circle fa-2x" style="color:green"></i></div>'
                : '<div class="text-center w-100"><i class="far fa-times-circle fa-2x" style="color:orange"></i></div>';
            },
            __raw: true,
          },
          {
            is_public: 'Public',
            __formatter: function (row) {
              return row.is_public
                ? '<div class="text-center w-100"><i class="far fa-check-circle fa-2x" style="color:green"></i></div>'
                : '<div class="text-center w-100"><i class="far fa-times-circle fa-2x" style="color:orange"></i></div>';
            },
            __raw: true,
          },
          {
            ts_available_to: 'Available for',
            __formatter: function (row) {
              return `<b>${moment
                .duration({
                  seconds: row.seconds_available_to,
                })
                .humanize()} </b><br><i>${'expires: ' + row.ts_available_to}</i > `;
            },
            __raw: true,
          },
          {
            password: 'Start exam',
            __formatter: function (row) {
              return `
                            <form action="/exam/run/new" method="POST">
                            <input type="hidden" name="password" value="${row.password}">

                                ${
                                  row.no_submitted < row.no_started
                                    ? '<button class="btn btn-outline-warning" type="submit">Continue exam</button>'
                                    : '<button class="btn btn-outline-success" type="submit"><i class="fas fa-play-circle"></i>&nbsp;Start</button>'
                                }
                            </form>`;
            },
            __raw: true,
          },
        ],
        rows: publicExams,
        class: 'table table-sm table-hover edgar no-dt',
      };
      viewdata.unsubmittedTests = {
        headers: [
          {
            title: 'Title',
          },
          {
            ts_started: 'Started at',
          },
          {
            ts_available_to: 'Available until',
          },
          {
            is_available: 'Can continue?',
            __raw: true,
            __formatter: function (row) {
              return row.is_available
                ? 'To continue type in the password above...'
                : `Can't continue: expired.`;
            },
          },
        ],
        rows: unsubmittedTests,
      };

      viewdata.course = course;
      viewdata.tutorials = {
        headers: [
          {
            tutorial_title: 'Title',
            __formatter: function (row) {
              return `<b>${row.tutorial_title}</b> ${
                row.is_active ? '' : '<span class="ml-3 badge badge-warning">not active</span>'
              }`;
            },
            __raw: true,
          },
          {
            tutorial_desc: 'Description',
          },
          {
            latestStepOrdinal: 'Last step',
          },
          {
            no_steps: 'Steps',
          },
          {
            startedTs: 'Started',
            __formatter: function (row) {
              return row.startedTs ? moment(row.startedTs).format('YYYY-MM-DD HH:mm:ss') : '';
            },
            __raw: true,
          },
          // {
          //   finished: 'Finished',
          // },
          {
            ts_finished: 'Finished at',
          },
        ],
        rows: tutorials,
        links: [
          {
            label: 'Start',
            __raw: true,
            href: (row) => `/tutorial/${row['id']}`,
            __formatter: function (row) {
              return row.startedTs ? 'Continue' : 'Start';
            },
          },
        ],
        class: 'table table-sm table-hover edgar no-dt',
      };

      viewdata.exercises = {
        headers: [
          {
            title: 'Title',
          },
          {
            description: 'Description',
          },
          {
            no_questions: 'Questions',
          },
        ],
        rows: exercises,
        buttons: [
          {
            label: 'Start',
            method: 'GET',
            action: (row) => `/exercise/${row['id']}`,
          },
        ],
        class: 'table table-sm table-hover edgar no-dt',
      };
      viewdata.startedSubmittedStats = startedSubmittedStats;
      viewdata.onlineUsersCount = onlineUsersCount;
    } catch (error) {
      winston.error(error);
    }
    res.appRender('homeView', viewdata);
    //res.appRender('homeView', viewdata, '~/shared/winterViewScript', '~/shared/winterViewCSS');
  }
);

module.exports = router;
