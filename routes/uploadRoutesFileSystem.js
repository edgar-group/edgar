'use strict';

var express = require('express');
var router = express.Router();
var multer = require('multer');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var fs = require('fs');
var utils = require.main.require('./common/utils');
var db = require('../db').db;

// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({extended: true}));

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    var dir = `${global.appRoot}/public/images/qs/${req.params.id_question}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    var ext = file.originalname.substring(file.originalname.length - 4, file.originalname.length);
    var newfilename = utils.getHashedImageName(
      req.params.id_question + ':' + file.originalname,
      ext
    );
    cb(null, newfilename);
  },
});

var upload = multer({ storage: storage }).array('images', 10);

//router.post('/:id_question([0-9]{1,10})', [middleware.requireRole(globals.ROLES.TEACHER), upload], function(req, res, next) {
router.post(
  '/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    upload(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        winston.error('error! ' + err);
        res.status(500).send('Upload failed');
      }
      var images = [];
      req.files.forEach(function (file) {
        images.push(
          `![Image\\${file.originalname}-${req.params.id_question}](${globals.IMG_FOLDER_PLACEHOLDER}/${req.params.id_question}/${file.filename})`
        );
      });
      winston.debug('req,files ' + req.files);
      res.json(images);
    });
  }
);

var uploadAttachments = multer({
  dest: globals.getQuestionsAttachmentsFolder(),
});

router.get(
  '/attachment/:filename(*)',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    try {
      let attachment = await db.oneOrNone(
        `
            SELECT filename, original_name
              FROM question_attachment
             WHERE filename = $(filename)
        `,
        {
          filename: req.params.filename,
        }
      );

      if (!attachment) {
        return res.sendStatus(404);
      }

      const file = `${globals.getQuestionsAttachmentsFolder()}/${attachment.filename}`;

      let downloadName = attachment.original_name.trim().split(new RegExp('\\s+')).join('_'); // + '.' + attachment.original_name.split('.').pop();

      res.download(file, downloadName);
    } catch (err) {
      res.sendStatus(400);
    }
  }
);

router.delete(
  '/attachment/:filename(*)',
  middleware.requireRole(globals.ROLES.TEACHER),
  async (req, res) => {
    try {
      let result = await db.result(
        `
            DELETE FROM question_attachment
            WHERE filename = $(filename)
        `,
        {
          filename: req.params.filename,
        }
      );

      if (!result || result.rowCount !== 1) {
        return res.sendStatus(404);
      }

      fs.stat(
        `${globals.getQuestionsAttachmentsFolder()}/${req.params.filename}`,
        function (err, stats) {
          if (err) {
            console.error(err);
            return res.sendStatus(400);
          }

          fs.unlink(
            `${globals.getQuestionsAttachmentsFolder()}/${req.params.filename}`,
            function (err) {
              if (err) {
                return res.sendStatus(400);
              }

              winston.debug('file deleted successfully');

              return res.sendStatus(200);
            }
          );
        }
      );
    } catch (err) {
      res.sendStatus(400);
    }
  }
);

router.post(
  '/attachment',
  [middleware.requireRole(globals.ROLES.TEACHER), uploadAttachments.single('attachment')],
  async function (req, res) {
    try {
      console.log(req.file);
      let idQuestion = req.body.id_question;
      let originalName = req.file.originalname; // what user uploaded
      let filename = req.file.filename; // hash
      let label = req.body.label; // input label
      let isPublic = req.body.is_public;

      await db.none(
        `
            INSERT INTO question_attachment(id_question, original_name, filename, label, is_public)
            VALUES ($1, $2, $3, $4, $5)
        `,
        [idQuestion, originalName, filename, label || '', isPublic]
      );

      res.json({ filename, label, isPublic });
    } catch (err) {
      res.sendStatus(400);
    }
  }
);

var storageFaces = multer.diskStorage({
  destination: function (req, file, cb) {
    var dir = `${global.appRoot}/public/images/faces/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    var ext = file.originalname.substring(file.originalname.length - 4, file.originalname.length);
    var newfilename = utils.getHashedImageName(file.originalname, ext);
    cb(null, newfilename);
  },
});

var uploadFaces = multer({ storage: storageFaces }).array('images', 1000);

router.post('/faces', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  uploadFaces(req, res, function (err) {
    if (err) {
      // An error occurred when uploading
      winston.error('error! ' + err);
      res.status(500).send('Upload failed');
    }
    var images = [];
    req.files.forEach(function (file) {
      images.push(`/images/faces/${file.filename}`);
    });
    winston.debug('req,files ' + req.files);
    res.appRender('uploadFacesView', { hrefs: images });
  });
});

var storageTestInstancePrivate = multer.diskStorage({
  destination: function (req, file, cb) {
    var dir = `${global.appRoot}/${globals.TMP_UPLOAD_FOLDER}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${globals.TI_UPLOAD_FOLDER}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${req.session.id_test_instance}/`; // TODO: check if this is correct
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    dir = `${dir}/${req.params.ordinal}/`;
    if (!fs.existsSync(dir)) {
      winston.debug('Creating dir ' + dir);
      fs.mkdirSync(dir);
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, 'upload_' + req.params.fileOrdinal + '_' + file.originalname);
  },
});

const FILE_SIZE_LIMIT = 50 * 1024 * 1024; // 50MB;

var uploadTestInstancePrivate = multer({
  storage: storageTestInstancePrivate,
  limits: {
    fileSize: FILE_SIZE_LIMIT,
  },
}).single('testInstanceFile');

module.exports = router;
