'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var courseService = require.main.require('./services/courseService');
var db = require('../db').db;
var utils = require('../common/utils');
var QuestionSequence = require.main.require('./models/QuestionSequenceModel.js');
const Scorer = require('../lib/Scorer');
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');
const RandomInputGenerator = require('../lib/RandomInputGeneratorCommonJS');
const templateService = require('../services/templateService');

router.post(
  '/rendertemplate/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let idCourse = req.session.currCourseId;
      res.json({
        success: true,
        data: await templateService.compileTemplates(
          idCourse,
          req.body.templates,
          req.body.data_object
        ),
      });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: 'An error occured: ' + error + '.',
        },
      });
    }
  }
);

router.post('/evaldo', middleware.requireRoles([globals.ROLES.TEACHER]), async function (req, res) {
  let idCourse = req.session.currCourseId;
  try {
    let obj = await templateService.evalDataObject(idCourse, req.body.data_object);
    res.json({
      success: true,
      data: {
        data_object: JSON.stringify(obj.data_object, null, 2),
        log: obj.log,
      },
    });
  } catch (error) {
    res.json({
      success: false,
      error: {
        message: 'An error occured: ' + error + '.',
      },
    });
  }
});
router.post(
  '/evalscript/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    let idCourse = req.session.currCourseId;
    let idQuestion = parseInt(req.params.id_question);
    try {
      let obj = await Scorer.scoreQuestionNoExam(
        idCourse,
        idQuestion,
        req.body.code || req.body.freetextanswer,
        req.body.id_programming_language,
        undefined,
        req.body.script
      );
      res.json({
        success: true,
        data: obj,
      });
    } catch (error) {
      res.json({
        success: false,
        error: {
          message: 'An error occured: ' + error + '.',
        },
      });
    }
  }
);
router.post(
  '/exec/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    let idQuestion = parseInt(req.params.id_question);
    let idCourse = req.session.currCourseId;
    try {
      winston.debug(`executing custom code (design time):\n ${JSON.stringify(req.body)}\n`);
      if (req.body.isCtype) {
        let scoreResult = await Scorer.scoreQuestionNoExam(
          idCourse,
          idQuestion,
          req.body.source,
          req.body.id_programming_language
        );
        res.json({
          success: true,
          data: utils.judge0response2Html({
            resp: scoreResult.j0,
            score: scoreResult.score,
            showExpected: true,
          }),
        });
      } else if (req.body.sql) {
        // false, false: do not append prefix/suffix/present code, client will do that (design time)
        res.json(
          await CodeRunnerService.execSQLCodeQuestion(idQuestion, req.body.sql, false, false)
        );
      } else if (req.body.js) {
        res.json(await CodeRunnerService.execJSCodeQuestion(idQuestion, req.body.js, false, false));
      }
    } catch (error) {
      res.json({
        success: false,
        error: {
          message: 'An error occured: ' + error + '.',
        },
      });
    }
  }
);

//  to generate inputs based on given configuration
router.post('/trygenerator', middleware.requireRole(globals.ROLES.TEACHER), function (req, res) {
  try {
    res.json(
      RandomInputGenerator.randomGenerator(
        Number(req.body.config.randType.trim()),
        Number(req.body.config.low.trim()),
        Number(req.body.config.up.trim()),
        Number(req.body.config.count.trim())
      )
    );
  } catch (error) {
    res.json({
      success: false,
      error: {
        message: 'An error occured: ' + error + '.',
      },
    });
  }
});

router.post(
  '/setreviewer',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.func('chk_can_see_question', [req.session.passport.user, req.body.id_question])
      .then((perm) => {
        if (perm[0].chk_can_see_question) {
          db.result(
            `INSERT INTO question_review(id_question, id_user_reviewed)
                            VALUES ( $(id_question), $(id_user_reviewed) )`,
            {
              id_user_reviewed: req.session.appUserId,
              id_question: req.body.id_question,
            }
          )
            .then(function (result) {
              // console.log(result);
              req.flash('info', `Reviewer set. Now it's your fault :).`);
              res.redirect('/question/edit/' + req.body.id_question);
            })
            .catch(function (error) {
              req.flash('error', JSON.stringify(error));
              res.redirect('/question/edit/' + req.body.id_question);
            });
        } else {
          winston.error(
            `Security breach, ${req.session.passport.user} does not have permissions to access question ${req.body.id_question}. Logged and noted!`
          );
          res
            .status(403)
            .send(
              'Forbidden, you do not have permissions to access this question. Logged and noted!'
            );
        }
      })
      .catch((err) => {
        winston.error(err);
        res.status(500).send('Error checking permissions.');
      });
  }
);
router.post(
  '/claimauthor',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.func('chk_can_see_question', [req.session.passport.user, req.body.id_question])
      .then((perm) => {
        if (perm[0].chk_can_see_question) {
          db.result(
            `UPDATE question
                            SET id_user_created = $(id_user_created)
                            WHERE id = $(id_question)`,
            {
              id_user_created: req.session.appUserId,
              id_question: req.body.id_question,
            }
          )
            .then(function (result) {
              req.flash('info', `Authorship set. Now it's your fault :).`);
              res.redirect('/question/edit/' + req.body.id_question);
            })
            .catch(function (error) {
              req.flash('error', JSON.stringify(error));
              res.redirect('/question/edit/' + req.body.id_question);
            });
        } else {
          winston.error(
            `Security breach, ${req.session.passport.user} does not have permissions to access question ${req.body.id_question}. Logged and noted!`
          );
          res
            .status(403)
            .send(
              'Forbidden, you do not have permissions to access this question. Logged and noted!'
            );
        }
      })
      .catch((err) => {
        winston.error(err);
        res.status(500).send('Error checking permissions.');
      });
  }
);

router.post(
  '/md2html',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    res.json({
      success: true,
      html: await utils.md2Html(req.body.md),
    });
  }
);

router.post(
  '/save',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res, next) {
    var correctAnswers;
    var answers;
    var answersStatic;
    var answersDynamic;
    var thresholds;
    var penalty_percentages;
    // C-stuff:
    var _c_test_type_ids;
    var _c_percentages;
    var _c_allow_diff_orders;
    var _c_allow_diff_letter_sizes;
    var _c_trim_whitespaces;
    var _c_comments;
    var _c_regexs;
    var _c_question_test_ids;
    var _c_inputs;
    var _c_outputs;
    var _c_generator_test_file_ids;
    var _c_argumentss;
    var _c_random_test_type_ids;
    var _c_low_bounds;
    var _c_upper_bounds;
    var _c_elem_counts;
    let complexQuestionTestCases = [];

    var _c_test_is_public;

    var _c_rtc_test;
    var _c_rtc_question;

    var programming_languages;
    // /C
    db.func('chk_can_see_question', [req.session.passport.user, req.body.id_question])
      .then(async (perm) => {
        if (perm[0].chk_can_see_question) {
          var sequenceParam = req.body.sequenceId ? `?seq=${req.body.sequenceId}` : '';
          if (req.body.toggleactive === 'toggleactive') {
            try {
              let result = await db.result(
                'UPDATE question SET is_active = NOT is_active WHERE id = $(id_question)',
                {
                  id_question: req.body.id_question,
                }
              );
              req.flash(
                'info',
                'Toggled question active status (' + result.rowCount + ' rows affected).'
              );
              res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
            } catch (error) {
              req.flash('error', 'Error toggling active status:' + JSON.stringify(error));
              res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
            }
          } else if (req.body.save === 'save' || req.body.saveinplace === 'saveinplace') {
            correctAnswers = [];
            answers = [];
            answersStatic = [];
            answersDynamic = [];
            thresholds = [];
            penalty_percentages = [];

            for (let i = 0; i < 100; ++i) {
              if (req.body['answer' + i]) {
                let answer = req.body['answer' + i];
                if (Array.isArray(answer)) {
                  // for connecting elements questions
                  answersStatic.push(req.body['answer' + i][0]);
                  answersDynamic.push(req.body['answer' + i][1]);
                } else {
                  answers.push(req.body['answer' + i]);
                }
                //thresholds.push(req.body['threshold_weight' + i]);
                thresholds.push(1);
                penalty_percentages.push(req.body['penalty_percentage' + i]);
                correctAnswers.push(parseInt(req.body[`corrAnswerCheckboxes${i}`]) === i ? 1 : 0);
              } else {
                break;
              }
            }

            if (answers.length !== 0 && answersDynamic.length !== 0) {
              answersDynamic.push(...answers);
              answers = [...answersDynamic];
            }
            if (answersStatic.length !== answersDynamic.length) {
              if (answersStatic.length < answersDynamic.length) {
                let difference = answersDynamic.length - answersStatic.length;
                for (let i = 0; i < difference; i++) {
                  answersStatic.push('');
                }
              } else {
                let difference = answersDynamic.length - answersStatic.length;
                for (let i = 0; i < difference; i++) {
                  answersDynamic.push(null);
                }
              }
            }
            if (
              answersStatic.length === 0 &&
              correctAnswers.length &&
              correctAnswers.reduce((a, v) => a + v) === 0
            ) {
              req.flash('error', 'Zero correct answers in multiple choice question!!');
            }
            // C-stuff:
            _c_question_test_ids = [];
            _c_comments = [];
            _c_percentages = [];
            _c_allow_diff_orders = [];
            _c_allow_diff_letter_sizes = [];
            _c_trim_whitespaces = [];
            _c_regexs = [];
            _c_test_type_ids = [];
            _c_inputs = [];
            _c_outputs = [];
            _c_generator_test_file_ids = [];
            _c_argumentss = [];
            _c_random_test_type_ids = [];
            _c_low_bounds = [];
            _c_upper_bounds = [];
            _c_elem_counts = [];
            _c_test_is_public = [];
            _c_rtc_test = [];
            _c_rtc_question = [];
            if (req.body.setup_script) {
              req.body.setup_script = req.body.setup_script.replace(/\r\n/g, '\n');
            }

            programming_languages = req.body.programming_languages
              ? req.body.programming_languages.split(',')
              : [];

            var runtime_constraints = await db.manyOrNone('SELECT id FROM runtime_constraint');

            // check question runtime constraints
            for (let i = 0; i < programming_languages.length; i++) {
              for (let j = 0; j < runtime_constraints.length; j++) {
                let rtc_q_value =
                  req.body[
                    'rtc_q_lang_' +
                      programming_languages[i] +
                      '_constr_' +
                      runtime_constraints[j].id
                  ];

                _c_rtc_question.push({
                  id_question: req.body.id_question,
                  id_language: programming_languages[i],
                  id_rtc: runtime_constraints[j].id,
                  rtc_value: rtc_q_value,
                });
              }
            }

            for (let i = 0; i < 100; ++i) {
              if (req.body['c_question_test_id' + i]) {
                let current_test_id = req.body['c_question_test_id' + i];

                for (let j = 0; j < programming_languages.length; j++) {
                  for (let k = 0; k < runtime_constraints.length; k++) {
                    let rtc_t_value =
                      req.body[
                        'rtc_t_' +
                          current_test_id +
                          '_lang_' +
                          programming_languages[j] +
                          '_constr_' +
                          runtime_constraints[k].id
                      ];

                    _c_rtc_test.push({
                      ordinal: i + 1,
                      id_test: current_test_id,
                      id_language: programming_languages[j],
                      id_rtc: runtime_constraints[k].id,
                      rtc_value: rtc_t_value,
                    });
                  }
                }

                _c_test_is_public.push(
                  req.body['is_public_' + current_test_id] === 'on' ? 'true' : 'false'
                );
                _c_comments.push(req.body['comment' + i]);
                _c_test_type_ids.push(req.body['id_test_type' + i]);
                _c_allow_diff_orders.push(req.body['diff_order' + i] === 'on' ? 'true' : 'false');
                _c_allow_diff_letter_sizes.push(
                  req.body['check_letters' + i] === 'on' ? 'true' : 'false'
                );
                _c_question_test_ids.push(req.body['c_question_test_id' + i]);
                _c_trim_whitespaces.push(
                  req.body['trim_whitespace' + i] === 'on' ? 'true' : 'false'
                );
                _c_percentages.push(req.body['percent' + i]);
                _c_inputs.push(req.body['c_input' + i] || '');
                _c_outputs.push(req.body['c_output' + i]);
                _c_random_test_type_ids.push(req.body['randGenType' + i]);
                _c_low_bounds.push(req.body['lowBound' + i]);
                _c_upper_bounds.push(req.body['upperBound' + i]);
                _c_elem_counts.push(req.body['count' + i]);
                _c_generator_test_file_ids.push(req.body['generators' + i]);
                _c_argumentss.push(req.body['params' + i]);
                _c_regexs.push(req.body['regex_override' + i]);
              } else if (req.body['run_script' + i] && req.body['eval_script' + i]) {
                complexQuestionTestCases.push({
                  run_script: req.body['run_script' + i],
                  eval_script: req.body['eval_script' + i],
                  comment: req.body['comment' + i],
                  is_public: req.body['is_public_' + i] === 'on' ? true : false,
                  expected: req.body['expected' + i],
                  percentage: +req.body['percent' + i],
                });
              } else {
                break;
              }
            }
          }
          // console.log('JSON.stringify(_c_rtc_question),', JSON.stringify(_c_rtc_question, null, 2));
          // console.log('JSON.stringify(_c_rtc_test),', JSON.stringify(_c_rtc_test, null, 2));
          if (req.body.save === 'save') {
            db.func('save_question', [
              req.body.id_question,
              req.body.is_active === 'true' ? true : false,
              req.body.upload_files_no,
              req.body.required_upload_files_no,
              req.body.question_text,
              req.body.displayOption,
              req.body.time_limit ? req.body.time_limit : null,
              req.body.question_comment,
              req.body.text_answer,
              req.body.initial_diagram_answer,
              req.body.diagram_answer,
              correctAnswers.join(globals.CSV.DELIM),
              answers.join(globals.CSV.DELIM),
              answersStatic.join(globals.CSV.DELIM),
              answersDynamic.join(globals.CSV.DELIM),
              thresholds.join(globals.CSV.DELIM),
              penalty_percentages ? penalty_percentages.join(globals.CSV.DELIM) : null,
              req.body.sql_answer,
              req.body.sql_alt_assertion,
              req.body.sql_test_fixture,
              req.body.sql_alt_presentation_query,
              req.body.check_tuple_order === 'true' ? true : false,
              req.body.id_check_column_mode,
              // C-stuff:
              req.body.prefix,
              req.body.suffix,
              req.body.c_answer,
              _c_test_type_ids.join(globals.CSV.DELIM),
              _c_percentages.join(globals.CSV.DELIM),
              _c_allow_diff_orders.join(globals.CSV.DELIM),
              _c_allow_diff_letter_sizes.join(globals.CSV.DELIM),
              _c_trim_whitespaces.join(globals.CSV.DELIM),
              _c_comments.join(globals.CSV.DELIM),
              _c_regexs.join(globals.CSV.DELIM),
              _c_question_test_ids.join(globals.CSV.DELIM),
              _c_inputs.join(globals.CSV.DELIM) || globals.CSV.DELIM, // This is a hack to make sure that the string_to_array in PG does not choke on empty string. However, it produces two element array but it is ok, because the second element is ignored in the function.
              _c_outputs.join(globals.CSV.DELIM),
              _c_generator_test_file_ids.join(globals.CSV.DELIM),
              _c_argumentss.join(globals.CSV.DELIM),
              _c_random_test_type_ids.join(globals.CSV.DELIM),
              _c_low_bounds.join(globals.CSV.DELIM),
              _c_upper_bounds.join(globals.CSV.DELIM),
              _c_elem_counts.join(globals.CSV.DELIM),
              // /C-stuff
              req.session.appUserId,
              req.body.question_tags ? req.body.question_tags.join(globals.CSV.DELIM) : null,
              // JSON stuff:
              req.body.json_answer,
              req.body.json_alt_assertion,
              req.body.json_test_fixture,
              req.body.json_alt_presentation_query,
              req.body.assert_deep === 'true' ? true : false,
              req.body.assert_strict === 'true' ? true : false,

              JSON.stringify(_c_rtc_question),
              JSON.stringify(_c_rtc_test),
              req.body.c_question_answer_pl,
              _c_test_is_public.join(globals.CSV.DELIM),
              req.body.id_scale,
              req.body.data_object,
              req.body.eval_script,
              req.body.id_script_programming_language,
              req.body.grader_object,
              req.body.id_ordered_element_correctness_model,
              req.body.id_connected_elements_correctness_model,
              req.body.setup_script,
              JSON.stringify(complexQuestionTestCases),
            ])
              .then(function (data) {
                winston.debug('save_question returned: ' + data);
                if (parseInt(data[0].save_question) === parseInt(req.body.id_question)) {
                  req.flash('info', 'Virgin question, successfully updated in place.');
                  res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
                } else {
                  req.flash(
                    'info',
                    `Question ${req.body.id_question} was deactivated, new version (child, id=${data[0].save_question}) successfully instantiated. You may edit further.`
                  );
                  res.redirect('/question/edit/' + data[0].save_question + sequenceParam);
                }
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          } else if (req.body.clone === 'clone') {
            db.func('clone_question', [
              req.body.id_question,
              req.session.appUserId,
              req.session.appUserId,
              false,
            ])
              .then(function (data) {
                winston.debug('clone_question returned: ' + data);
                req.flash(
                  'info',
                  `Question ${req.body.id_question} was successfully cloned to this (${data[0].clone_question}) question. You may edit now.`
                );
                res.redirect('/question/edit/' + data[0].clone_question);
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          } else if (req.body.saveinplace === 'saveinplace') {
            db.func('save_in_place_question', [
              req.body.id_question,
              req.body.is_active === 'true' ? true : false,
              req.body.upload_files_no,
              req.body.required_upload_files_no,
              req.body.question_text,
              req.body.displayOption,
              req.body.time_limit ? req.body.time_limit : null,
              req.body.question_comment,
              req.body.text_answer,
              req.body.initial_diagram_answer,
              req.body.diagram_answer,
              correctAnswers.join(globals.CSV.DELIM),
              answers.join(globals.CSV.DELIM),
              answersStatic.join(globals.CSV.DELIM),
              answersDynamic.join(globals.CSV.DELIM),
              thresholds.join(globals.CSV.DELIM),
              penalty_percentages.join(globals.CSV.DELIM),
              req.body.sql_answer,
              req.body.sql_alt_assertion,
              req.body.sql_test_fixture,
              req.body.sql_alt_presentation_query,
              req.body.check_tuple_order === 'true' ? true : false,
              req.body.id_check_column_mode,
              // C-stuff:
              req.body.prefix,
              req.body.suffix,
              req.body.c_answer,
              _c_test_type_ids.join(globals.CSV.DELIM),
              _c_percentages.join(globals.CSV.DELIM),
              _c_allow_diff_orders.join(globals.CSV.DELIM),
              _c_allow_diff_letter_sizes.join(globals.CSV.DELIM),
              _c_trim_whitespaces.join(globals.CSV.DELIM),
              _c_comments.join(globals.CSV.DELIM),
              _c_regexs.join(globals.CSV.DELIM),
              _c_question_test_ids.join(globals.CSV.DELIM),
              _c_inputs.join(globals.CSV.DELIM) || globals.CSV.DELIM, // This is a hack to make sure that the string_to_array in PG does not choke on empty string. However, it produces two element array but it is ok, because the second element is ignored in the function.
              _c_outputs.join(globals.CSV.DELIM),
              _c_generator_test_file_ids.join(globals.CSV.DELIM),
              _c_argumentss.join(globals.CSV.DELIM),
              _c_random_test_type_ids.join(globals.CSV.DELIM),
              _c_low_bounds.join(globals.CSV.DELIM),
              _c_upper_bounds.join(globals.CSV.DELIM),
              _c_elem_counts.join(globals.CSV.DELIM),
              // /C-stuff
              req.session.appUserId,
              req.body.question_tags ? req.body.question_tags.join(globals.CSV.DELIM) : null,
              // JSON stuff:
              req.body.json_answer,
              req.body.json_alt_assertion,
              req.body.json_test_fixture,
              req.body.json_alt_presentation_query,
              req.body.assert_deep === 'true' ? true : false,
              req.body.assert_strict === 'true' ? true : false,

              JSON.stringify(_c_rtc_question),
              JSON.stringify(_c_rtc_test),
              req.body.c_question_answer_pl,
              _c_test_is_public.join(globals.CSV.DELIM),
              req.body.id_scale,
              req.body.data_object,
              req.body.eval_script,
              req.body.id_script_programming_language,
              req.body.grader_object,
              req.body.id_ordered_element_correctness_model,
              req.body.id_connected_elements_correctness_model,
              req.body.setup_script,
              JSON.stringify(complexQuestionTestCases),
              true, // -- bump version
            ])
              .then(function (data) {
                winston.debug('save_in_place_question returned: ', data);
                if (parseInt(data[0].save_in_place_question) === parseInt(req.body.id_question)) {
                  req.flash('info', 'Successfully updated in place, version bumped up.');
                  res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
                } else {
                  req.flash('error', 'Save failed? Unexpected question id returned.');
                  res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
                }
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          } else if (req.body.delete === 'delete') {
            db.func('delete_question', [req.body.id_question])
              .then(function (data) {
                winston.debug('delete_question returned: ' + data);
                if (data[0].delete_question === true) {
                  req.flash('info', 'Question ' + req.body.id_question + ' deleted.');
                  res.redirect('/question/');
                } else {
                  req.flash('error', 'Delete failed? Unexpected return value.');
                  res.redirect('/question/edit/' + req.body.id_question);
                }
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          } else if (req.body.addNewCtest === 'addNewCtest') {
            db.func('add_c_question_test', [req.body.c_question_answer_id, req.session.appUserId])
              .then(function (data) {
                winston.debug('add_c_question_test returned: ' + data);
                req.flash('info', 'Successfully added new default C test, please edit further...');
                res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          } else if (req.body.addNewComplexTest === 'addNewComplexTest') {
            try {
              await db.none(
                `UPDATE complex_question
                    SET test_cases = test_cases || '{
                    "run_script": "run1.sh",
                    "eval_script": "eval1.out",
                    "comment": ""
                  }'::jsonb
                  WHERE id_question = $(id_question)`,
                {
                  id_question: req.body.id_question,
                }
              );
              req.flash('info', 'Successfully added new default test, please edit further...');
            } catch (error) {
              req.flash('error', JSON.stringify(error));
            }
            res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
          } else if (req.body.removeComplexTest && parseInt(req.body.removeComplexTest) >= 0) {
            try {
              await db.none(
                `UPDATE complex_question
                    SET test_cases = test_cases - $(index)
                  WHERE id_question = $(id_question)
                    AND jsonb_array_length(test_cases) > 1`,
                {
                  index: parseInt(req.body.removeComplexTest),
                  id_question: req.body.id_question,
                }
              );
              req.flash('info', 'Successfully removed the test, please edit further...');
            } catch (error) {
              req.flash('error', JSON.stringify(error));
            }
            res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
          } else if (req.body.cloneCodeTest && parseInt(req.body.cloneCodeTest) > 0) {
            db.func('clone_c_question_test', [
              parseInt(req.body.cloneCodeTest),
              req.session.appUserId,
            ])
              .then(function (data) {
                winston.debug('clone_c_question_test returned: ' + data);
                req.flash('info', 'Successfully cloned code test, please edit further...');
                res.redirect('/question/edit/' + req.body.id_question + sequenceParam);
              })
              .catch(function (error) {
                winston.error(error);
                res.errRender(error);
              });
          }
        } else {
          winston.error(
            `Security breach, ${req.session.passport.user} does not have permissions to access question ${req.body.id_question}. Logged and noted!`
          );
          res
            .status(403)
            .send(
              'Forbidden, you do not have permissions to access this question. Logged and noted!'
            );
        }
      })
      .catch((err) => {
        winston.error(err);
        res.status(500).send('Error checking permissions.');
      });
  }
);

//#kristina only
//  to delete a test for question
router.post(
  '/ctest/delete/:c_question_test_id([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    var IdQuestionTest = parseInt(req.params.c_question_test_id);
    winston.debug('deleting c_question_test : ' + IdQuestionTest);

    db.func('delete_c_question_test', [IdQuestionTest])
      .then((data) => {
        winston.debug('delete c_question_test returned: ' + data);
        if (data[0].delete_c_question_test == true) {
          res.json({
            success: true,
          });
        } else {
          res.json({
            success: false,
            error: {
              message: `Question answer ${IdQuestionTest} doesnt exist.`,
            },
          });
        }
      })
      .catch((error) => {
        winston.error(error);
        res.redirect({
          success: false,
          error: {
            message: 'An error occured: ' + error + '.',
          },
        });
      });
  }
);
// router.post(
//   '/complex-test/delete/:ordinal([0-9]{1,10})',
//   middleware.requireRole(globals.ROLES.TEACHER),
//   function (req, res) {
//     var ordinal = parseInt(req.params.ordinal);
//     winston.debug('deleting complex question test ordinal: ' + ordinal);
//     let tests = db.one
//     db.func('delete_c_question_test', [IdQuestionTest])
//       .then((data) => {
//         winston.debug('delete c_question_test returned: ' + data);
//         if (data[0].delete_c_question_test == true) {
//           res.json({
//             success: true,
//           });
//         } else {
//           res.json({
//             success: false,
//             error: {
//               message: `Question answer ${IdQuestionTest} doesnt exist.`,
//             },
//           });
//         }
//       })
//       .catch((error) => {
//         winston.error(error);
//         res.redirect({
//           success: false,
//           error: {
//             message: 'An error occured: ' + error + '.',
//           },
//         });
//       });
//   }
// );

router.get('/', middleware.requireRole(globals.ROLES.TEACHER), async function (req, res, next) {
  db.any(
    `SELECT DISTINCT question.id, question.is_active, id_question_type, question.ts_created :: timestamp(0) :: varchar,
                                question.ts_modified :: timestamp(0) :: varchar,
                                type_name,
                                ucre.first_name || ' ' || ucre.last_name as user_created,
                                umod.first_name || ' ' || umod.last_name as user_modified
                  FROM question
                    JOIN question_node on question.id = question_node.id_question
                     AND id_node in
                         (SELECT id_child FROM v_roots_children WHERE id_root = (select id_root_node from course where id = $(id_course)))
                    JOIN question_type on id_question_type = question_type.id
                    JOIN app_user ucre on id_user_created = ucre.id
                    JOIN app_user umod on id_user_modified = umod.id
                    ORDER BY question.id
                            `,
    {
      id_course: req.session.currCourseId,
    }
  )
    .then(async (data) => {
      //let sequenceId = await QuestionSequence.insertSequence(req.session.passport.user, data.map(x => x.id));

      res.appRender('questionView', {
        qs: data,
        distinctTypes: data
          .map((x) => x.type_name)
          .filter(function (value, index, self) {
            return self.indexOf(value) === index;
          })
          .sort(),
        //sequenceId: sequenceId
      });
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching questions: ' + error.message)
      );
    });
});

router.get('/new', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  var promises = [];
  promises.push(courseService.getNodes(req.session.currCourseId));
  promises.push(courseService.getProgrammingLanguages(req.session.currCourseId));
  promises.push(courseService.getCodeRunners(req.session.currCourseId));
  Promise.all(promises)
    .then(function (results) {
      res.appRender(
        'newQuestionView',
        {
          nodes: results[0],
          programming_languages: results[1],
          code_runners: results[2],
        },
        'newQuestionViewScript'
      );
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching  question/new: ' + error.message)
      );
    });
});

router.post('/new', middleware.requireRole(globals.ROLES.TEACHER), async function (req, res) {
  var node_ids = [];
  for (let i = 1; i < 5; ++i) {
    if (req.body['node' + i]) {
      node_ids.push(parseInt(req.body['node' + i]));
    }
  }

  var questionInfo = JSON.parse(req.body.question_info);

  if (!questionInfo.id_programming_languages) {
    if (req.body.programming_languages && !Array.isArray(req.body.programming_languages)) {
      req.body.programming_languages = Array.of(req.body.programming_languages);
    }
    questionInfo.id_programming_languages = req.body.programming_languages
      ? req.body.programming_languages.map((lang_id) => parseInt(lang_id))
      : [];
  }
  // console.log(req.body);
  let cc2 = '';
  if (req.body.cards_count1 && req.body.cards_count2) {
    cc2 = '' + (parseInt(req.body.cards_count1) + parseInt(req.body.cards_count2));
  }
  if (node_ids.length) {
    // TODO: validate args
    db.func('new_question', [
      questionInfo.id_question_type,
      req.body.id_code_runner,
      req.body.answers_count[0],
      req.body.cards_count1,
      cc2,
      req.session.appUserId,
      node_ids,
      req.body.use_template ? 'true' : 'false',
      req.body.use_script ? 'true' : 'false',
      questionInfo.id_programming_languages,
    ])
      .then(function (data) {
        // console.log(data);
        winston.debug('new_question returned: ' + data);
        req.flash('info', 'New question created, please edit.');
        res.redirect('/question/edit/' + data[0].new_question);
      })
      .catch(function (error) {
        winston.error(error);
        res.errRender(error);
      });
  } else {
    req.flash('error', 'At least one node has to be set.');
    res.redirect('/question/new');
  }
});

router.get('/search', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  let selectedNodesIds = [],
    selectedTagsIds = [];

  if (req.query.nodes) {
    if (!(req.query.nodes instanceof Array)) req.query.nodes = [req.query.nodes];

    selectedNodesIds = req.query.nodes.map((n) => parseInt(n));
  }
  if (req.query.tags) {
    if (!(req.query.tags instanceof Array)) req.query.tags = [req.query.tags];

    selectedTagsIds = req.query.tags.map((t) => parseInt(t));
  }

  let nodes = [],
    tags = [];
  db.task('get-nodes-and-tags', (t) => {
    return t.batch([
      t.any(
        `SELECT DISTINCT node.id as value, node_name, type_name,
                    '[' || type_name || '] ' || node_name as name
                FROM v_roots_children join node on id_child = node.id
                JOIN node_type on node.id_node_type = node_type.id
                WHERE id_root = (select id_root_node from course where id = $(id_course))
                ORDER BY node_name;`,
        {
          id_course: req.session.currCourseId,
        }
      ),
      t.any(
        ` SELECT id as value, tag as name
                        FROM course_tag
                        WHERE id_course = $(id_course)
                        ORDER BY name`,
        {
          id_course: req.session.currCourseId,
        }
      ),
    ]);
  })
    .then((data) => {
      nodes = data[0];
      tags = data[1];
      let searchPromise = new Promise((resolve, reject) => resolve());
      if (
        req.query.fts ||
        selectedNodesIds.length > 0 ||
        selectedTagsIds.length > 0 ||
        req.query.created_like ||
        req.query.modified_like
      ) {
        const tsquery = req.query.fts ? req.query.fts.trim().split(' ').join('&') : '';
        const ts_headline = `ts_headline(question_text, to_tsquery($(tsquery)), 'MinWords=12,MaxWords=32,HighlightAll=true')`;
        let nodesFilter = '',
          tagsFilter = 'true',
          ftsFilter = 'true';

        if (selectedNodesIds.length > 0)
          nodesFilter = `and node.id = ANY ('{${selectedNodesIds}}'::int[])`;

        if (selectedTagsIds.length > 0) {
          if (req.query.tagsOperator == 'and')
            tagsFilter = `array_agg(coalesce(course_tag.id, -1)) @> ('{${selectedTagsIds}}'::int[])`;
          else if (req.query.tagsOperator == 'or')
            tagsFilter = `array_agg(coalesce(course_tag.id, -1)) && ('{${selectedTagsIds}}'::int[])`;
        }

        // Determine if SQL LIKE or trigram fuzzy matching is performed
        if (req.query.fts) {
          if (req.query.fts.includes('_') || req.query.fts.includes('%')) {
            ftsFilter = `lower(question_text) LIKE lower($(fts))`;
          } else {
            ftsFilter = `similarity(question_text, $(fts)) > 0`;
          }
        }
        let createdLikeFilter = ' 1 = $(created_like)';
        let createdLikeValue = 1;
        if (req.query.created_like) {
          createdLikeFilter = ` lower(ucreated.last_name) LIKE $(created_like)`;
          createdLikeValue = `%${req.query.created_like.toLowerCase()}%`;
        }
        let modifiedLikeFilter = ' 1 = $(modified_like)';
        let modifiedLikeValue = 1;
        if (req.query.modified_like) {
          modifiedLikeFilter = ` lower(umodified.last_name) LIKE $(modified_like)`;
          modifiedLikeValue = `%${req.query.modified_like.toLowerCase()}%`;
        }

        searchPromise = db.any(
          `SELECT DISTINCT question.id, version,
                            CASE WHEN (is_active)  THEN '&#10003;' ELSE '&#10008;' END AS is_active,
                            CASE WHEN (is_active) THEN
                                ${ts_headline}
                            ELSE
                            '<p class="text-muted"><strike>' || ${ts_headline} || '</strike></p>'
                            END AS question_text,
                            string_agg(DISTINCT course_tag.tag, ', 'ORDER BY course_tag.tag) AS tags,
                            substring(umodified.first_name from 1 for 1) || '. ' || umodified.last_name AS user_modified_sl,
                            to_char(question.ts_modified, 'YYYY-MM-DD HH24:MI:SS') AS ts_modified_sl,
                            round(EXTRACT(EPOCH FROM current_timestamp-question.ts_modified)/(3600*24)) AS  days_ago,
                            substring(ucreated.first_name from 1 for 1) || '. ' || ucreated.last_name AS user_created,
                            to_char(question.ts_created, 'YYYY-MM-DD HH24:MI:SS') AS ts_created_sl,
                            question_type.type_name,
                            to_char(MAX(test_instance_question.ts_created), 'YYYY-MM-DD HH24:MI:SS') AS ts_last_used,
                            student.id AS last_used_by_id,
                            substring(student.first_name from 1 for 1) || '. ' || student.last_name AS last_used_by_name,
                            student.alt_id2 AS last_used_by_alt_id2,
                            test_instance.id AS test_instance_id
                            ${
                              req.query.fts
                                ? `, similarity(lower(question_text), lower($(fts))) AS sim`
                                : ''
                            }
                            FROM v_roots_children
                            JOIN node ON id_child = node.id
                            JOIN question_node ON question_node.id_node = id_child
                            JOIN question ON question.id = question_node.id_question
                            JOIN question_type ON id_question_type = question_type.id
                            JOIN app_user umodified ON id_user_modified = umodified.id
                            JOIN app_user ucreated ON id_user_created = ucreated.id
                            LEFT JOIN question_course_tag ON question_course_tag.id_question = question.id
                            LEFT JOIN course_tag ON question_course_tag.id_course_tag = course_tag.id
                            LEFT JOIN LATERAL (
                                SELECT tiq.ts_created, tiq.id_question, tiq.id_test_instance
                                FROM test_instance_question AS tiq
                                WHERE tiq.id_question = question.id
                                ORDER BY tiq.ts_created DESC
                                LIMIT 1
                            ) AS test_instance_question ON true
                            LEFT JOIN test_instance ON test_instance.id = test_instance_question.id_test_instance
                            LEFT JOIN test ON test.id = test_instance.id_test
                            LEFT JOIN student ON student.id = test_instance.id_student
                            WHERE id_root = (select id_root_node from course where id = $(id_course))
                            ${nodesFilter}
                            AND
                            ${ftsFilter}
                            AND ${createdLikeFilter}
                            AND ${modifiedLikeFilter}
                            GROUP BY question.id, version, is_active, question_text, user_modified_sl, ts_modified_sl
                                    , days_ago, substring(ucreated.first_name from 1 for 1) || '. ' || ucreated.last_name, ts_created_sl, question_type.type_name, last_used_by_id, last_used_by_name, test_instance_id
                            HAVING
                            ${tagsFilter}
                            ORDER BY ${
                              req.query.fts ? 'sim DESC, ' : ''
                            }ts_modified_sl DESC, is_active DESC
                            LIMIT $(no_results)
                        `,
          {
            fts: req.query.fts,
            tsquery: tsquery,
            id_course: req.session.currCourseId,
            no_results: req.query.no_results,
            created_like: createdLikeValue,
            modified_like: modifiedLikeValue,
          }
        );

        const startTime = process.hrtime();

        searchPromise.then(async (searchResults) => {
          const elapsedTime = Math.round(process.hrtime(startTime)[1] / 1000000);

          searchResults.forEach(function (row) {
            //row.id_ver_act = `<a href="/question/edit/${row.id}?seq=${sequenceId}" target="_blank">${row.id}</a><br>${row.version}<br>${row.is_active}`;
            row.id_ver_act = `..., ${row.id}, ...`;
            row.id_ver_act_link = `<a href="/question/edit/${row.id}" target="_blank">${row.id}</a><br>${row.version}<br>${row.is_active}`;
            if (row.last_used_by_alt_id2) {
              row.img = utils.getImageWithNameHtml(row.last_used_by_alt_id2, row.last_used_by_name);
            } else {
              row.img = '';
            }
          });

          res.appRender(
            'questionSearchView',
            {
              nodes: nodes,
              selectedNodes: selectedNodesIds,
              tags: tags,
              tagsOperator: req.query.tagsOperator,
              selectedTags: selectedTagsIds,
              fts: req.query.fts || '',
              created_like: req.query.created_like || '',
              modified_like: req.query.modified_like || '',
              no_results: req.query.no_results,
              elapsedTime: elapsedTime,
              qs: {
                headers: [
                  {
                    id_ver_act_link: 'Id/Ver/Act',
                    __raw: true,
                  },
                  {
                    sim: 'Similarity',
                  },
                  {
                    question_text: 'Text',
                    __raw: true,
                  },
                  {
                    tags: 'Tags',
                  },
                  {
                    type_name: 'Type',
                  },
                  {
                    user_created: 'User created',
                  },
                  {
                    user_modified_sl: 'User modified',
                  },
                  {
                    ts_modified_sl: 'TS modified',
                  },
                  {
                    days_ago: 'Days ago',
                  },
                  {
                    ts_last_used: 'TS last used',
                  },
                  {
                    img: 'Last used by',
                    __raw: true,
                  },
                ],
                rows: searchResults ? searchResults : [],
                leftButtons: [
                  {
                    label: 'Sequence',
                    flabel: function name(row) {
                      return row.id_ver_act;
                    },
                    fprops: {
                      qid: function (row) {
                        return row.id;
                      },
                    },
                    method: 'POST',
                    action: function (row) {
                      return `/question/browsesequence/` + row.id;
                    },
                    class: function (row) {
                      return 'btn btn-light btn-browsesequence';
                    },
                  },
                ],
              },
            },
            'questionSearchViewScript',
            'questionSearchViewCSS'
          );
        });
      } else {
        res.appRender(
          'questionSearchView',
          {
            nodes: nodes,
            tags: tags,
            tagsOperator: 'and',
            selectedNodes: [],
            selectedTags: [],
            fts: '',
            created_like: '',
            modified_like: '',
            no_results: 200,
            qs: null,
          },
          'questionSearchViewScript',
          'questionSearchViewCSS'
        );
      }
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching questions: ' + error.message)
      );
    });
});

router.get(
  '/:id_question([0-9]{1,10})/correction/delete/:id_test_instance_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test_instance_question) {
      db.func('undo_correction', [req.params.id_test_instance_question])
        .then(function (data) {
          winston.debug('undo_correction returned: ' + data);
          req.flash('info', 'Test correction deleted.');
          res.redirect(`/question/instances/${req.params.id_question}`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/question/instances/${req.params.id_question}`);
        });
    } else {
      req.flash('error', 'argumens NOT set.');
      res.redirect(`/question/instances/${req.params.id_question}`);
    }
  }
);

router.get(
  '/:id_question([0-9]{1,10})/correction/accept/:id_test_instance_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test_instance_question) {
      db.func('accept_answer', [
        req.params.id_test_instance_question,
        req.session.appUserId,
        'Answer accepted',
      ])
        .then(function (data) {
          winston.debug('accept_answer returned: ' + data);
          req.flash('info', 'Answer accepted.');
          res.redirect(`/question/instances/${req.params.id_question}`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/question/instances/${req.params.id_question}`);
        });
    } else {
      req.flash('error', 'argumens NOT set.');
      res.redirect(`/question/instances/${req.params.id_question}`);
    }
  }
);

function renderQuestionInstances(req, res, next, alltime) {
  db.any(
    `SELECT test_instance_question.id,
                       model_name,
                       student.alt_id2,
                       test_instance.id test_instance_id,
                       test.title,
                       student.id student_id,
                       student.first_name,
                       student.last_name,
                       student.email,
                       is_correct,
                       is_incorrect,
                       (is_correct || '<br>' || is_incorrect) as is_correct_incorrect,
                       is_unanswered,
                       is_partial,
                       (is_unanswered || '<br>' || is_partial) as is_unanswered_partial,
                       test_instance_question.score,
                       test_instance_question.score_perc,
                       test_instance.score as test_score,
                       test_instance.score_perc as test_score_perc,
                       hint,
                       ts_submitted::timestamp(0)::varchar,
                       app_user.last_name || ', delta:' || score_delta
                        || ', reason:' || reason || ', ts:'
                        || test_correction.ts_created::timestamp(0)::varchar as modified,
                       test_instance_question.ordinal
                FROM test_instance_question
                JOIN grading_model ON test_instance_question.id_grading_model = grading_model.id
                JOIN test_instance ON test_instance_question.id_test_instance = test_instance.id
                JOIN test ON test_instance.id_test = test.id
                JOIN student ON test_instance.id_student = student.id
                LEFT JOIN test_correction ON test_instance_question.id = test_correction.id_test_instance_question
                LEFT JOIN app_user ON test_correction.id_user_created = app_user.id
                WHERE id_question = $(id_question)
                  AND (($(alltime) = true) OR test.id_academic_year = $(id_academic_year))
                ORDER BY ts_submitted DESC `,
    {
      id_question: req.params.id_question,
      id_academic_year: req.session.currAcademicYearId,
      alltime: alltime,
    }
  )
    .then((data) => {
      res.appRender(
        'questionInstancesView',
        {
          id_question: req.params.id_question,
          alltime: alltime,
          list: {
            headers: [
              {
                select: 'Sel',
                __raw: true,
                __formatter: function (row) {
                  // return row.modified ? row.id : `<label class="form-check-label">
                  //     <input type="checkbox" class="form-check-input" id="chk${row.id}" name="ids[]" value="${row.id}">
                  //         ${row.id}
                  //     </label>`;
                  return (
                    `<label class="form-check-label"><input type="checkbox" class="form-check-input" id="chk${row.id}" name="ids[]" value="${row.id}">` +
                    (row.modified
                      ? `<a class="btn btn-outline-danger" href="/question/${req.params.id_question}/correction/delete/${row.id}">Undo ${row.id}</a>`
                      : `<a class="btn btn-outline-success" href="/question/${req.params.id_question}/correction/accept/${row.id}">Accept ${row.id}</a>
                                                </label>`)
                  );
                },
              },
              {
                ts_submitted: 'Submitted',
              },
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return (
                    utils.getTinyImageHtmlMinimal(row.alt_id2) +
                    row.alt_id2 +
                    ' <br>' +
                    row.first_name +
                    ' ' +
                    row.last_name +
                    ' <a href = "mailto: ' +
                    row.email +
                    '"><i class="far fa-envelope"></i></a>'
                  );
                },
              },
              {
                is_correct_incorrect: 'Correct / Incorrect', // TODO: turn this into badges
                __raw: true,
              },
              {
                is_unanswered_partial: 'Unanswered / Partial',
                __raw: true,
              },
              {
                score: 'Q score',
              },
              {
                score_perc: 'Q score%',
              },
              {
                hint: 'hint',
              },
              {
                title: 'Test title',
              },
              {
                test_score: 'Test score',
              },
              {
                test_score_perc: 'Test score%',
              },
              {
                modified: 'Modified',
              },
              {
                ordinal: 'Ordnl',
              },
            ],
            rows: data,
            links: [
              {
                label: 'Stalk',
                target: '_blank',
                class: (row) => `btn btn-outline-success`,
                href: (row) => `/test/instances/stalk/id/${row['test_instance_id']}`,
              },
              {
                label: 'Review',
                target: '_blank',
                class: (row) => `btn btn-outline-success`,
                href: (row) => `/exam/review/${row['test_instance_id']}/${row['ordinal']}`,
              },
            ],
          },
        },
        'questionInstancesViewScript'
      );
    })
    .catch((error) => {
      return next(
        new errors.DbError('An error occured while fetching question/new: ' + error.message)
      );
    });
}

router.get(
  '/instances/alltime/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    renderQuestionInstances(req, res, next, true);
  }
);
router.get(
  '/instances/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    renderQuestionInstances(req, res, next, false);
  }
);

router.post(
  '/instances/modifyscore/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    // console.log(req.body);
    // TODO: validate
    var ids = [];
    req.body.ids.forEach(function (val) {
      ids.push(parseInt(val));
    });
    db.func('save_correction', [req.body.reason, req.body.score_delta, req.session.appUserId, ids])
      .then(function (data) {
        winston.debug('save_correction returned: ' + data);
        if (parseInt(data[0].save_correction) === 0) {
          req.flash('info', 'Saved.');
        } else {
          req.flash('error', `Not saved! save_correction returned:${JSON.stringify(data)}`);
        }
        res.redirect('/question/instances/' + req.params.id_question);
      })
      .catch(function (error) {
        winston.error(error);
        res.errRender(error);
      });
  }
);

router.post(
  '/browsesequence/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res, next) {
    var sequenceId;
    if (req.body.sequence) {
      var sequence = JSON.parse(req.body.sequence);
      sequenceId = await QuestionSequence.insertSequence(req.session.passport.user, sequence);
      res.redirect(`/question/edit/${req.params.id_question}?seq=${sequenceId}`);
    } else {
      res.redirect(`/question/edit/${req.params.id_question}`);
    }
  }
);

router.get(
  '/edit/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let question,
      complex_question,
      tags,
      selectedTags,
      code_question_answer,
      reviewers,
      programming_languages,
      question_rtc,
      question_tests_rtc,
      attachments,
      course;
    try {
      const sequenceId = req.query.seq;
      let promises = [];
      promises.push(
        db.one(
          `
                SELECT show_attachments, show_runtime_constraints
                FROM course
                WHERE id = $(id_course)
            `,
          {
            id_course: req.session.currCourseId,
          }
        )
      );

      promises.push(
        db.any(
          `
                SELECT filename, label, is_public
                FROM question_attachment
                WHERE id_question = $(id_question)
            `,
          {
            id_question: req.params.id_question,
          }
        )
      );

      promises.push(
        db.any(
          `SELECT id as value, tag as name
                            FROM course_tag
                           WHERE id_course = $(id_course)
                           ORDER BY name
                         `,
          {
            id_course: req.session.currCourseId,
          }
        )
      );

      promises.push(
        db.any(
          `SELECT course_tag.id
                            FROM question_course_tag
                            JOIN course_tag
                              ON question_course_tag .id_course_tag = course_tag.id
                           WHERE id_question = $(id_question)
                                -- id_course = $(id_course), let the user SEE all tags, across courses
                         `,
          {
            id_course: req.session.currCourseId,
            id_question: req.params.id_question,
          }
        )
      );
      promises.push(
        db.any(
          `SELECT substring(first_name  from 1 for 1) || last_name as reviewer,
                                                question_review.ts_created :: timestamp(0) :: varchar
                                        FROM question_review
                                        JOIN app_user
                                          ON id_user_reviewed = app_user.id
                                        WHERE id_question = $(id_question)
                                        ORDER BY question_review desc
                         `,
          {
            id_question: req.params.id_question,
          }
        )
      );

      promises.push(
        db.oneOrNone(
          ` select question.*,
                  question.ts_created :: timestamp(0) :: varchar,
                  question.ts_modified :: timestamp(0) :: varchar,
                  (SELECT MAX(next.id) FROM question next  WHERE next.id_prev_question = question.id) as id_next_question,
                  type_name,
                  has_answers,
                  ucre.first_name || ' ' || ucre.last_name as user_created,
                  umod.first_name || ' ' ||  umod.last_name as user_modified,
                  (select string_agg(rpath, '<br/>' order by rpath)
                          from v_node_path
                          join question_node on question.id = id_question
                          where v_node_path.id = question_node.id_node
                          ) as paths,
                  last_used :: timestamp(0) :: varchar,
                  coalesce(count_used, 0) ::int as count_used,
                  question_data_object.data_object,
                  question_eval_script.eval_script,
                  question_eval_script.id_script_programming_language,
                  display_option,
                  id_ordered_element_correctness_model,
                  id_connected_elements_correctness_model

              FROM question
              JOIN question_type on id_question_type = question_type.id
              JOIN app_user ucre on id_user_created = ucre.id
              JOIN app_user umod on id_user_modified = umod.id

              LEFT JOIN ordered_element_question
                      ON question.id = ordered_element_question.id_question
              LEFT JOIN connected_elements_question
                      ON question.id = connected_elements_question.id_question
              LEFT JOIN question_data_object
                      ON question.id = question_data_object.id_question
              LEFT JOIN question_eval_script
                      ON question.id = question_eval_script.id_question
              LEFT JOIN (
                      select id_question, max(ts_submitted) as last_used, count(*) as count_used
                      from test_instance_question tiq
                          join test_instance ti on tiq.id_test_instance = ti.id
                          group by id_question) as usage
            on usage.id_question = question.id
            WHERE question.id = $(id_question)
              AND chk_can_see_question($(username), $(id_question)) = true
                         `,
          {
            id_question: req.params.id_question,
            username: req.session.passport.user,
          }
        )
      );

      [course, attachments, tags, selectedTags, reviewers, question] = await Promise.all(promises);

      if (!question) {
        res.status(404).send('Missed it. Wanna try not guessing?');
      } else {
        let answers,
          script_programming_languages,
          ordered_element_correctness_model,
          connected_elements_correctness_model;
        if (question.eval_script || question.eval_script === '') {
          script_programming_languages = await db.many(
            `SELECT id as value, name
                           FROM programming_language
                            ORDER BY id`
          );
        }

        if (question && question.type_name.toUpperCase().indexOf('TRUE/FALSE') >= 0) {
          answers = await db.any(
            `SELECT is_correct, answer_text, ordinal, threshold_weight, penalty_percentage :: int
                                 FROM question_answer
                                WHERE id_question = $(id_question)
                             ORDER BY ordinal`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('CONNECTEDELEMENTS') >= 0) {
          answers = await db.any(
            `SELECT answer_text_static, answer_text_dynamic, ordinal, penalty_percentage :: int
                                 FROM connected_elements_question_answer
                                WHERE id_question = $(id_question)
                             ORDER BY ordinal`,
            {
              id_question: req.params.id_question,
            }
          );
          connected_elements_correctness_model = await db.many(
            `SELECT id as value, model_name as name
               FROM connected_elements_correctness_model
              ORDER BY id`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('ORDEREDELEMENTS') >= 0) {
          answers = await db.any(
            `SELECT is_correct, answer_text, ordinal, threshold_weight, penalty_percentage :: int
               FROM question_answer
              WHERE id_question = $(id_question)
              ORDER BY ordinal`,
            {
              id_question: req.params.id_question,
            }
          );
          ordered_element_correctness_model = await db.many(
            `SELECT id as value, model_name as name
               FROM ordered_element_correctness_model
              ORDER BY id`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('DIAGRAM') >= 0) {
          answers = await db.any(
            `SELECT initial_diagram_answer, diagram_answer
               FROM diagram_question_answer
              WHERE id_question = $(id_question)`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.has_answers === true) {
          // ABC
          answers = await db.any(
            `SELECT is_correct, answer_text, ordinal, threshold_weight, penalty_percentage :: int
                                 FROM question_answer
                                WHERE id_question = $(id_question)
                             ORDER BY ordinal`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('SQL') >= 0) {
          answers = await db.any(
            `SELECT sql_answer, sql_alt_assertion, sql_test_fixture, sql_alt_presentation_query, check_tuple_order, id_check_column_mode, code_runner.name as code_runner_name
                                 FROM sql_question_answer
                                 LEFT JOIN code_runner
                                        ON sql_question_answer.id_code_runner = code_runner.id
                                WHERE id_question = $(id_question)`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('JSON') >= 0) {
          answers = await db.any(
            `SELECT json_answer, json_alt_assertion, json_test_fixture, json_alt_presentation_query, assert_deep, assert_strict, code_runner.name as code_runner_name
                                 FROM json_question_answer
                                 LEFT JOIN code_runner
                                        ON json_question_answer.id_code_runner = code_runner.id
                                WHERE id_question = $(id_question)`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('FREE TEXT') >= 0) {
          answers = await db.any(
            `SELECT text_answer
                       FROM text_question_answer
                      WHERE id_question = $(id_question)`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (question && question.type_name.toUpperCase().indexOf('SCALE') >= 0) {
          answers = await db.any(
            `SELECT 0, null::int as value, '-- please select --' as name, (SELECT id_scale FROM question_scale WHERE  id_question = $(id_question)) as selected
                       UNION ALL
                      SELECT 1, scale.id, scale.name, 0
                        FROM scale
                    ORDER BY 1, 3`,
            {
              id_question: req.params.id_question,
            }
          );
        } else if (
          question &&
          (question.type_name.toUpperCase().indexOf('C-LANG') >= 0 ||
            question.type_name.toUpperCase().indexOf('JAVA') >= 0 ||
            question.type_name.toUpperCase().indexOf('CODE') >= 0)
        ) {
          let cpromises = [];
          cpromises.push(
            db.oneOrNone(
              `SELECT c_prefix, c_suffix, c_source, c_question_answer.id, id_programming_language
                                FROM c_question_answer
                                WHERE id_question = $(id_question)`,
              {
                id_question: req.params.id_question,
              }
            )
          );

          cpromises.push(
            db.many(
              `SELECT programming_language.id, name
                 FROM question_programming_language
                 JOIN programming_language
                   ON question_programming_language.id_programming_language = programming_language.id
                  AND question_programming_language.id_question = $(id_question)
                ORDER BY programming_language.id`,
              {
                id_question: req.params.id_question,
              }
            )
          );
          cpromises.push(
            db.func('get_question_runtime_constraints', [
              req.session.currCourseId,
              req.params.id_question,
            ])
          );

          cpromises.push(
            db.func('get_question_tests_runtime_constraints', [
              req.session.currCourseId,
              req.params.id_question,
            ])
          );
          cpromises.push(
            db.any(
              `SELECT id_programming_language, c_test_type_id, percentage, allow_diff_order,
                        allow_diff_letter_size, trim_whitespace, comment, regex_override,
                        input, output, generator_test_file_id, arguments, description, random_test_type_id,
                        low_bound, upper_bound, elem_count, random_test_type_name, c_question_test.id,
                        c_question_test.is_public, ordinal
                                 FROM c_question_answer
                                 left JOIN c_question_test on c_question_test.c_question_answer_id = c_question_answer.id
                                 left JOIN fixed_test on fixed_test.c_question_test_id = c_question_test.id
                                 left JOIN random_test on random_test.c_question_test_id = c_question_test.id
                                 left JOIN random_test_type on random_test_type.id = random_test.random_test_type_id
                                 left JOIN generator_test on generator_test.c_question_test_id = c_question_test.id
                                 left JOIN generator_test_file on generator_test_file.id = generator_test.generator_test_file_id
                                WHERE id_question = $(id_question)
                                ORDER BY c_question_test.ordinal, c_question_test.id`,
              {
                id_question: req.params.id_question,
              }
            )
          );
          [code_question_answer, programming_languages, question_rtc, question_tests_rtc, answers] =
            await Promise.all(cpromises);
        } else if (question && question.type_name.toUpperCase().indexOf('COMPLEX') >= 0) {
          let complexPromises = [];
          complexPromises.push(
            db.oneOrNone(
              `SELECT *
                FROM complex_question
                WHERE id_question = $(id_question)`,
              {
                id_question: req.params.id_question,
              }
            )
          );
          complexPromises.push(
            db.func('get_question_runtime_constraints', [
              req.session.currCourseId,
              req.params.id_question,
            ])
          );
          [complex_question, question_rtc] = await Promise.all(complexPromises);
          console.log(complex_question);
        }
        let nextQuestion,
          prevQuestion,
          navLabel = '1/1';
        if (sequenceId) {
          let seq = await QuestionSequence.getById(sequenceId);
          if (seq) {
            seq = seq.sequence;
            let curr = seq.findIndex((x) => x === question.id);
            navLabel = `${curr + 1}/${seq.length}`;
            if (curr >= 0) {
              if (curr + 1 < seq.length) nextQuestion = seq[curr + 1];
              if (curr > 0) prevQuestion = seq[curr - 1];
            }
          }
        }
        res.appRender(
          'editQuestionView',
          {
            pageTitle: `Q: ${question.id} 🔨${question.user_created}`,
            show_attachments: course.show_attachments,
            show_rtc: course.show_runtime_constraints,
            attachments: attachments,
            nextQuestion: nextQuestion,
            prevQuestion: prevQuestion,
            navLabel: navLabel,
            sequenceId: sequenceId,
            tags: tags,
            selectedTags: selectedTags.map((x) => x.id),
            reviewers: reviewers,
            retired: question.id_next_question ? true : false,
            question: question,
            complex_question,
            answers: question.has_answers ? answers : [],
            ordered_element_correctness_model,
            connected_elements_correctness_model,
            programming_languages: programming_languages, // TODO
            code_question_answer: code_question_answer,
            code_question_tests: answers,
            code_question_rtc: question_rtc,
            code_question_tests_rtc: question_tests_rtc,
            data_object: question.data_object,
            eval_script: question.eval_script,
            id_script_programming_language: question.id_script_programming_language,
            script_programming_languages,
            sql_answer: question.type_name.toUpperCase().indexOf('SQL') >= 0 ? answers[0] : null,
            json_answer: question.type_name.toUpperCase().indexOf('JSON') >= 0 ? answers[0] : null,
            text_question_answer:
              question.type_name.toUpperCase().indexOf('FREE TEXT') >= 0 ? answers[0] : null,
            diagram_question_answer:
              question.type_name.toUpperCase().indexOf('DIAGRAM') >= 0 ? answers[0] : null,
            question_scale: question.type_name.toUpperCase().indexOf('SCALE') >= 0 ? answers : null,
            code_runner_name: code_question_answer
              ? code_question_answer.code_runner_name
              : question.type_name.toUpperCase().indexOf('SQL') >= 0
              ? answers[0].code_runner_name
              : undefined,
            google_translate_key: (
              await db.one(
                'SELECT google_translate_key FROM app_user WHERE username = $(username)',
                {
                  username: req.session.passport.user,
                }
              )
            ).google_translate_key,
          },
          '~/shared/editQuestionViewScript'
        );
      }
    } catch (error) {
      winston.error(error);
      next(error);
    }
  }
);

module.exports = router;
