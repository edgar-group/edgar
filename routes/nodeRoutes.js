'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
//var testService = require.main.require('./services/testService');
var db = require('../db').db;
var querystring = require('querystring');
var utils = require('../common/utils');

router.post('/update', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  if (req.body.btn === 'delete') {
    db.tx((t) => {
      // creating a sequence of transaction queries:
      var q1 = t.none(
        `INSERT INTO node_parent(id_child, id_parent)
                                SELECT npc.id_child, npp.id_parent
                                  FROM  node_parent npp
                                  JOIN node_parent npc
                                    ON npp.id_child = $1
                                   AND npc.id_parent = $1
                                 WHERE not exists (SELECT *
                                    FROM node_parent WHERE id_child = npc.id_child
                                    AND id_parent = npp.id_parent)`,
        [req.body.node_id]
      );
      var q2 = t.none('DELETE FROM node_parent WHERE id_parent = $1', [req.body.node_id]);
      var q3 = t.none('DELETE FROM node_parent WHERE id_child = $1', [req.body.node_id]);
      var q4 = t.none('DELETE FROM node WHERE id = $1', [req.body.node_id]);
      // returning a promise that determines a successful transaction:
      return t.batch([q1, q2, q3, q4]); // all of the queries are to be resolved;
    })
      .then((data) => {
        req.flash('info', JSON.stringify(data));
        res.redirect('/node/vis');
      })
      .catch((error) => {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
        res.redirect('/node/vis');
      });
  } else if (req.body.btn === 'update') {
    db.result(
      `
               UPDATE node
                  SET node_name = $1,
                      id_node_type = $2
                WHERE id = $3
            `,
      [req.body.node_name, req.body.id_node_type, req.body.node_id]
    )
      .then((result) => {
        req.flash('info', result.rowCount + ' node(s) updated, keep up the good work ;-).');
        res.redirect('/node/vis');
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while updating node: ' + error.message));
      });
  } else if (req.body.btn === 'reassign') {
    db.result(
      `
               UPDATE node_parent
                  SET id_parent = $1
                WHERE id_child = $2
                  AND (SELECT COUNT(*) FROM node_parent WHERE id_child = $2) = 1
            `,
      [req.body.parent_id, req.body.node_id]
    )
      .then((result) => {
        req.flash(
          'info',
          result.rowCount +
            ' node(s) updated.' +
            (result.rowCount === 0
              ? 'This options works only if node has exactly ONE parent (to be reassigned).'
              : '')
        );
        res.redirect('/node/vis');
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while updating node: ' + error.message));
      });
  } else {
    req.flash('error', 'Unknown op.');
    res.redirect('/node/vis');
  }
});

router.post('/insert', middleware.requireRole(globals.ROLES.TEACHER), function (req, res, next) {
  db.tx((t) => {
    // creating a sequence of transaction queries:
    var q1 = t.none(
      `
                BEGIN WORK;
                    INSERT INTO node(id_node_type, node_name, description) VALUES ($1, $2, $3);
                    INSERT INTO node_parent (id_child, id_parent) VALUES ((SELECT currval('node_id_seq')), $4);
                COMMIT WORK;`,
      [req.body.id_node_type, req.body.node_name, req.body.description, req.body.parent_id]
    );
    return t.batch([q1]); // all of the queries are to be resolved;
  })
    .then((data) => {
      req.flash('info', JSON.stringify(data));
      res.redirect('/node/vis');
    })
    .catch((error) => {
      winston.error(error);
      req.flash('error', JSON.stringify(error));
      res.redirect('/node/vis');
    });
});

router.get(
  '/:id_node([0-9]{1,10})/questions',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.any(
      `
               SELECT '<a target="_blank" href="/question/edit/' || question.id || '">' || question.id || '</a>' || '<br/>' || version || '<br>' || is_active as "id/ver/act",
                      substring(question_text from 1 for 45), substring(first_name from 1 for 1) || '. ' || last_name || '<br/>' || question.ts_modified as "user/time",
                      type_name as type
                 FROM question
                 JOIN question_node on question.id = id_question
                 JOIN question_type on id_question_type = question_type.id
                 JOIN app_user on id_user_Created = app_user.id
                WHERE question_node.id_node = $(id_node)
                ORDER BY is_active desc, question.id
            `,
      {
        id_node: req.params.id_node,
      }
    )
      .then((data) => {
        res.json({ qs: data });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);

router.get(
  '/vis/:id_node([0-9]{1,10})?',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var filter = '';
    if (req.params.id_node) {
      filter = 'WHERE id_root = $(id_node)';
    } else {
      filter = 'WHERE id_root = (SELECT id_root_node FROM course WHERE id = $(id_course))';
    }
    var promises = [];
    promises.push(
      db.any(
        `
               SELECT id_root, node_name, vrc.id_child, vrc.id_parent, id_node_type,
                    SUM(CASE WHEN(is_active = true)  THEN 1 ELSE 0 END) AS cnt_active,
                    SUM(CASE WHEN(is_active = false) THEN 1 ELSE 0 END) AS cnt_inactive
                 FROM v_roots_children vrc
                         JOIN node
                           ON vrc.id_child = node.id
                    LEFT JOIN question_node
                           ON question_node.id_node = vrc.id_child
                    LEFT JOIN question
                           ON question_node.id_question = question.id
                ${filter}
                GROUP BY node_name, vrc.id_child, vrc.id_parent, id_root, id_node_type
                ORDER BY id_child, id_node_type
            `,
        {
          id_course: req.session.currCourseId,
          id_node: req.params.id_node,
        }
      )
    );
    promises.push(
      db.any(`
               SELECT id, type_name as name, id as value
                 FROM node_type
                ORDER BY id
            `)
    );

    Promise.all(promises)
      .then(function (results) {
        var data = results[0];
        var nodes = [];
        var bag = {};
        for (let i = 0; i < data.length; ++i) {
          if (bag[data[i].id_child] === undefined) {
            bag[data[i].id_child] = 1;
            nodes.push(data[i]);
          }
        }
        res.appRender(
          'questionVisView',
          {
            id_root: nodes[0].id_root,
            nodes: nodes,
            edges: data,
            node_types: results[1],
          },
          'questionVisViewScript'
        );
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching vis: ' + error.message));
      });
  }
);

router.get('/vistree', middleware.requireRoles([globals.ROLES.TEACHER]), function (req, res, next) {
  db.any(
    `
               SELECT *  FROM v_d3tree where id_course = $(id_course)
            `,
    {
      id_course: req.session.currCourseId,
    }
  )
    .then((nodes) => {
      res.appRender(
        'questionVisTreeView',
        {
          nodes: nodes,
        },
        'questionTreeVisViewScript',
        'questionTreeVisViewCSS'
      );
    })
    .catch((error) => {
      return next(new errors.DbError('An error occured while vistree ' + error.message));
    });
});

router.get(
  '/list/nodes/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.any(
      `
               SELECT id_node
                 FROM question_node
                WHERE id_question = $(id_question)
                ORDER BY id_node
            `,
      {
        id_question: req.params.id_question,
      }
    )
      .then((data) => {
        res.json({ nodes: data.map((i) => i.id_node) });
      })
      .catch((error) => {
        return next(new errors.DbError('An error occured while fetching nodes: ' + error.message));
      });
  }
);
router.get(
  '/list/questions/:id_node([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.any(
      `
               SELECT id_question
                 FROM question_node
                WHERE id_node = $(id_node)
                ORDER BY id_question
            `,
      {
        id_node: req.params.id_node,
      }
    )
      .then((data) => {
        res.json({ questions: data.map((i) => i.id_question) });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching questions: ' + error.message)
        );
      });
  }
);
router.post(
  '/:id_node([0-9]{1,10})/question/delete/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    db.result(
      `
               DELETE
                 FROM question_node
                WHERE id_node = $1
                  AND id_question = $2
            `,
      [req.params.id_node, req.params.id_question]
    )
      .then((result) => {
        res.json({
          success: true,
          rowsAffected: result.rowCount,
        });
      })
      .catch((error) => {
        return res.json({
          success: false,
          rowsAffected: 0,
        });
      });
  }
);
router.post(
  '/:id_node([0-9]{1,10})/question/add/:id_question([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    // console.log('body', req.body);
    db.result(
      `
               INSERT INTO question_node (id_node, id_question)
                VALUES ($1, $2)
            `,
      [req.params.id_node, req.params.id_question]
    )
      .then((result) => {
        res.json({
          success: true,
          rowsAffected: result.rowCount,
        });
      })
      .catch((error) => {
        return res.json({
          success: false,
          rowsAffected: 0,
        });
      });
  }
);
router.get(
  '/questions',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var promises = [];
    promises.push(
      db.any(
        `select DISTINCT question.id, '<a target="_blank" href="/question/edit/' || question.id || '">' || question.id ||  '/' || version || '/' || is_active || '</a>' as id_ver_act,
                            substring(question_text from 1 for 45) as txt,
                            substring(first_name from 1 for 1) || '. ' || last_name || ' : ' || question.ts_modified::timestamp(0)::varchar as user_time,
                            type_name
                  from question join question_node on question.id = question_node.id_question
                  join question_type on id_question_type = question_type.id
                  JOIN app_user on id_user_Created = app_user.id
                   and id_node in
                            (selecT id_child from v_roots_children where id_root = (select id_root_node from course where id = $(id_course)))
                    ORDER BY question.id DESC
                            `,
        {
          id_course: req.session.currCourseId,
        }
      )
    );
    promises.push(
      db.any(
        `
               SELECT DISTINCT node.id, node_name, type_name
                 FROM v_roots_children vrc
                         JOIN node
                           ON vrc.id_child = node.id
                         JOIN node_type on node.id_node_type = node_type.id
                WHERE id_root = (SELECT id_root_node FROM course WHERE id = $(id_course))
                ORDER BY node.id DESC
            `,
        {
          id_course: req.session.currCourseId,
        }
      )
    );

    Promise.all(promises)
      .then((data) => {
        res.appRender(
          'nodeQuestionView',
          {
            questions: data[0],
            nodes: data[1],
          },
          'nodeQuestionViewScript'
        );
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching nodes & questions: ' + error.message)
        );
      });
  }
);
module.exports = router;
