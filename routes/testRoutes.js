'use strict';
var express = require('express');
var router = express.Router();
var errors = require('../common/errors');
var middleware = require('../common/middleware');
var utils = require('../common/utils');
var globals = require('../common/globals');
var db = require('../db').db;
var winston = require('winston');
const moment = require('moment');
var escape = require('escape-html');

var TestLog = require.main.require('./models/TestLogModel.js');
var CodeRunLog = require.main.require('./models/CodeRunLogModel.js');
var TestLogDetails = require.main.require('./models/TestLogDetailsModel.js');
var OngoingTests = require.main.require('./models/OngoingTests.js');
var Heartbeat = require.main.require('./models/HeartbeatModel.js');

var testService = require.main.require('./services/testService');
var questionService = require.main.require('./services/questionService');
var realTimePlagService = require.main.require('./services/realTimePlagService');

var csv = require('express-csv');
var tmp = require('tmp');
var sanitize = require('sanitize-filename');
var fs = require('fs');
var unzip = require('unzipper');
const minioService = require('../services/minioService');
const RunningInstances = require('../common/runningInstances');

var listInstances = function (req, res, next) {
  var rv = {
    currTestInstanceId: undefined,
  };
  var isOverAll = true;
  if (req.params.currId) {
    rv.currTestInstanceId = Number.parseInt(req.params.currId);
    isOverAll = false;
  }
  var studentId = req.params.studentId
    ? Number.parseInt(req.params.studentId)
    : req.session.studentId;
  db.any(
    `SELECT test_instance.id, test_ordinal, title, passed, score_perc, first_name || ' ' || last_name as full_name, test_score_ignored
                  FROM test_instance
                  JOIN student on test_instance.id_student = student.id
                  JOIN test on test_instance.id_test = test.id
                  JOIN test_type ON test.id_test_type = test_type.id
                 WHERE id_student = $(id_student)
                    AND id_academic_year = $(id_academic_year)
                    AND id_course = $(id_course)
                   AND ts_submitted IS NOT NULL
                   AND test_type.type_name <> 'Lecture quiz'
              ORDER BY ts_submitted desc`,
    {
      id_student: studentId,
      id_academic_year: req.session.currAcademicYearId,
      id_course: req.session.currCourseId,
    }
  )
    .then((data) => {
      rv.testInstances = data;
      if (isOverAll) {
        return db.any(
          `SELECT
                                test.title,
                                test.test_score_ignored,
                                ti.ts_started as tosort,
                                ti.ts_started::timestamp(0)::varchar,
                                ti.ts_submitted::timestamp(0)::varchar,
                                ((ti.ts_submitted - ti.ts_started)::interval(0))::varchar as duration,
                                ti.correct_no,
                                ti.incorrect_no,
                                ti.unanswered_no,
                                ti.partial_no,
                                ti.score as t_score,
                                round(ti.score_perc * 100, 1) || '%' AS t_score_perc,
                                ti.passed,
                                ti.id as id_test_instance,
                                (EXTRACT(EPOCH FROM (current_timestamp- ts_submitted)) < review_period_mins * 60) as show_review
                             FROM test_instance ti
                             JOIN test
                               ON ti.id_test = test.id
                            WHERE id_student = $(id_student)
                              AND (id_academic_year = $(id_academic_year) OR test.is_global)
                              AND id_course = $(id_course)
                              AND ti.ts_submitted IS NOT NULL
                            ORDER BY ti.ts_submitted DESC`,
          {
            id_student: studentId,
            id_academic_year: req.session.currAcademicYearId,
            id_course: req.session.currCourseId,
          }
        );
      } else {
        return db.any(
          `SELECT
                                ti.ts_started::timestamp(0)::varchar,
                                ti.ts_submitted::timestamp(0)::varchar,
                                ((ti.ts_submitted - ti.ts_started)::interval(0))::varchar as duration,
                                ti.correct_no,
                                ti.incorrect_no,
                                ti.unanswered_no,
                                ti.partial_no,
                                ti.score as t_score,
                                test.test_score_ignored,
                                round(ti.score_perc * 100, 1) || '%' AS t_score_perc,
                                ti.passed,
                                ti.id as id_test_instance,
                                (EXTRACT(EPOCH FROM (current_timestamp- ts_submitted)) < review_period_mins * 60) as show_review,
                                ----------
                                tiq.ordinal,
                                tiq.is_correct,
                                tiq.is_incorrect,
                                tiq.is_unanswered,
                                tiq.is_partial,
                                tiq.score,
                                round(tiq.score_perc * 100, 1) || '%' AS score_perc,
                                app_user.last_name || ', delta:' || score_delta
                                    || ', reason:' || reason || ', ts:'
                                    || test_correction.ts_created:: timestamp(0):: varchar as correction
                             FROM test_instance ti
                             JOIN test on ti.id_test = test.id
                             JOIN test_instance_question tiq
                               ON ti.id = tiq.id_test_instance
                            LEFT JOIN test_correction ON tiq.id = test_correction.id_test_instance_question
                            LEFT JOIN app_user ON test_correction.id_user_created = app_user.id
                            WHERE id_student = $(id_student)
                              AND (id_academic_year = $(id_academic_year) OR test.is_global)
                              AND id_course = $(id_course)
                              AND ti.id = $(ti_id)
                              AND ti.ts_submitted IS NOT NULL
                            ORDER BY id_test_instance, ordinal`,
          {
            id_student: studentId,
            ti_id: rv.currTestInstanceId,
            id_academic_year: req.session.currAcademicYearId,
            id_course: req.session.currCourseId,
          }
        );
      }
    })
    .then((results) => {
      if (isOverAll) {
        rv.tests = results;
        rv.questions = undefined;
        return new Promise((resolve, reject) => {
          resolve(null);
        });
      } else {
        rv.tests = results[0] ? [results[0]] : undefined;
        rv.questions = results;
        if (req.session.showEventLog || req.session.rolename === globals.ROLES.TEACHER) {
          return TestLog.getLogEvents(rv.currTestInstanceId);
        } else {
          return null;
        }
      }
    })
    .then((events) => {
      rv.events = events;
      res.appRender('testInstancesView', rv);
    })
    .catch((error) => {
      winston.error(error);
      return next(new errors.DbError('An error occured while fetching test: ' + error.message));
    });
};

router.get(
  '/instances/stalk/student/:studentId([0-9]{1,10})/instance/:currId([0-9]{1,10})?',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  listInstances
);
router.get(
  '/instance/:currId([0-9]{1,10})/details/:ts([0-9]{1,20})?',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    TestLogDetails.getLoggedResults(req.params.currId)
      .then((data) => {
        let titleSuffix = '';
        if (req.params.ts) {
          let currTs = new Date(parseInt(req.params.ts));
          titleSuffix = ' in the neigbourhood of ' + moment(currTs).format('YYYY-MM-DD hh:mm:ss');
          if (data.length > 3) {
            let [i1, i2, i3] = data;
            let i = 3;
            while (i < data.length && data[i].mongoTs < currTs) {
              ++i;
            }
            i3 = data[i - 1];
            i2 = data[i - 2];
            i1 = data[i - 3];
            data = [i1, i2, i3];
          }
        }
        data.sort((a, b) => Date.parse(b.mongoTs) - Date.parse(a.mongoTs));
        res.appRender('testLogDetailsView', {
          pageTitle: 'Exam log (' + req.params.currId + ')',
          testInstanceId: req.params.currId,
          title:
            'Details for test instance id =  ' +
            req.params.currId +
            titleSuffix +
            ' ordered by time DESECENDING (last items first)',
          list: {
            headers: [
              {
                score: 'Info',
                __raw: true,
                __formatter: function (row) {
                  return `
                                        Date: <span class="badge badge-info">
                                            ${moment(row.mongoTs).format('MMMM Do YYYY')}
                                        </span>
                                        <br>Time: <span class="badge badge-info">
                                            ${moment(row.mongoTs).format('hh:mm:ss')}
                                        </span>
                                        <hr>Ordinal: <h3 class="d-inline"><span class="badge badge-success">
                                            ord-${row.ordinal ? row.ordinal : '?'}
                                        </span></h3>
                                        <hr>Score:
                                        <pre>
                                        ${
                                          row.score &&
                                          JSON.stringify(row.score, undefined, 2).replace(
                                            /\,/g,
                                            ', '
                                          )
                                        }
                                        </pre>
                                        `;
                },
              },
              {
                code: 'Code',
                __raw: true,
                __style: 'min-width: 600px',
                __formatter: function (row) {
                  return `<pre>${escape(row.code)}</pre>`;
                },
              },
              {
                result: 'Result (what student sees as feedback)',
                __raw: true,
                __formatter: function (row) {
                  if (row.coderesult) {
                    return row.coderesult;
                  } else if (row.data) {
                    return utils.getHtmlTable(row);
                  } else {
                    return 'unknown result type';
                  }
                },
              },
            ],
            rows: data,
            class: 'table table-sm table-hover edgar no-dt',
          },
        });
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);

// instances/stalk/student/6231/instance/53525

router.get(
  '/instances/:currId([0-9]{1,10})?',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    // the client gets redirected here straight after the submit:
    if (req.session.id_test_instance) {
      winston.info(
        'PANIC? req.session.id_test_instance is set on the landing page, but should not be!'
      );
      RunningInstances.removeFromRunningTestInstances(req, req.session.id_test_instance);
    }
    if (req.session.id_test_instance_rev) {
      winston.info(
        'Redirect landing page: removing req.session.id_test_instance_rev = ' +
          req.session.id_test_instance_rev
      );
      delete req.session.id_test_instance_rev;
    }

    if (req.session.rolename === globals.ROLES.TEACHER && req.params.currId) {
      db.oneOrNone(
        ` SELECT id_student
                             FROM test_instance
                             JOIN test ON test_instance.id_test = test.id
                            WHERE test_instance.id = $(currId)
                            AND id_academic_year = $(id_academic_year)
                            AND id_course = $(id_course)
                            `,
        {
          currId: req.params.currId,
          id_academic_year: req.session.currAcademicYearId,
          id_course: req.session.currCourseId,
        }
      )
        .then(function (stud) {
          if (stud) {
            req.params.studentId = stud.id_student;
            listInstances(req, res, next);
          } else {
            res.status(404).send('Missed it. Wanna try not guessing?');
          }
        })
        .catch(function (error) {
          winston.error(
            new errors.DbError(
              'An error occured while fetching instances/:currId([0-9]{1,10}): ' + error.message
            )
          );
          return next(
            new errors.DbError(
              'An error occured while fetching instances/:currId([0-9]{1,10}): ' + error.message
            )
          );
        });
    } else {
      listInstances(req, res, next);
    }
  }
);

router.post(
  '/instances/sendmessage/test/:id_test([0-9]{1,10})/send',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    // console.log(req.body);
    if (req.params.id_test) {
      try {
        let ids = req.body.ids.map((x) => parseInt(x)).join(',');

        let res = await db.result(
          `INSERT INTO test_message (id_test, id_student, message_title, message_body)
                                 SELECT $(id_test), id, $(message_title), $(message_body)
                                   FROM unnest(ARRAY[${ids}]) AS id`,
          {
            id_test: req.params.id_test,
            message_title: req.body.title,
            message_body: req.body.message,
          }
        );

        req.flash(
          'info',
          `Yay, result is: ${res.rowCount} messages queued up, refresh this page to check when they are dispatched.`
        );
      } catch (error) {
        req.flash('error', JSON.stringify(error));
      }
    }
    res.redirect('/test/instances/sendmessage/test/' + req.params.id_test);
  }
);
router.get(
  '/instances/sendmessage/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.any(
      `
        SELECT  student.id,
                student.first_name,
                last_name,
                test.title,
                student_course.class_group,
                case when (test_instance.ts_started is null) then 'not started'
                else 'ongoing'
                end as status,
                id_test_instance,
                (SELECT string_agg(test_message.id::varchar, ', ' order by test_message.id)
                    FROM test_message WHERE test.id = test_message.id_test
                        AND student.id = test_message.id_student) as messages,
                                (SELECT string_agg(ts_sent::timestamp(0)::varchar, ', ' order by test_message.id)
                    FROM test_message WHERE test.id = test_message.id_test
                        AND student.id = test_message.id_student) as ts_sent,
                (SELECT string_agg(ts_displayed::timestamp(0)::varchar, ', ' order by test_message.id)
                    FROM test_message WHERE test.id = test_message.id_test
                        AND student.id = test_message.id_student) as ts_displayed,
                string_agg('[' || test_instance_question.id_question || ']' , ', ' order by ordinal) as  question_ids
            FROM student
            JOIN student_course ON student.id = student_course.id_student
            JOIN test ON student_course.id_course = test.id_course
             AND student_course.id_academic_year = test.id_academic_year
             AND test.id = $(id_test)
            LEFT JOIN test_instance
                   ON test_instance.id_student = student.id
                  AND test_instance.id_test = test.id
            LEFT JOIN test_instance_question
                   ON test_instance.id = test_instance_question.id_test_instance
             AND id_test = $(id_test)
            LEFT JOIN test_message
                ON test.id = test_message.id_test
               AND student.id = test_message.id_student
             WHERE ts_submitted IS NULL
            GROUP BY 1, 2, 3, 4, 5, 6, 7, test.id
            ORDER BY last_name
            `,
      {
        id_test: req.params.id_test,
      }
    )
      .then((data) => {
        res.appRender(
          'testInstancesSendMessageView',
          {
            pageTitle: 'Send message',
            title:
              'Student that have NOT submited the test "' +
              (data && data[0] && data[0].title) +
              '" (ongoing and not-started): ',
            id_test: req.params.id_test,
            list: {
              headers: [
                {
                  select: 'Sel',
                  __raw: true,
                  __formatter: function (row) {
                    return `<label class="form-check-label"><input type="checkbox" class="form-check-input" id="chk${row.id}" name="ids[]" value="${row.id}">
                                <button class="btn btn-success edgar-btn-select-up" value="${row.id}">Select up</button>`;
                  },
                },
                {
                  id: 'Id',
                },
                {
                  image: 'image',
                  __raw: true,
                  __formatter: function (row) {
                    return utils.getTinyImageHtml(row.alt_id2);
                  },
                },
                {
                  first_name: 'First name',
                },
                {
                  last_name: 'Last name',
                },
                {
                  class_group: 'Class group',
                },
                {
                  status: 'Status',
                },
                {
                  id_test_instance: 'TI ID',
                },
                {
                  question_ids: 'Question IDs',
                },
                {
                  messages: 'Message IDs',
                },
                {
                  ts_sent: 'Sent',
                },
                {
                  ts_displayed: 'Displayed',
                },
              ],
              rows: data,
            },
          },
          'testInstancesSendMessageViewScript'
        );
      })
      .catch((error) => {
        return next(error);
      });
  }
);

router.post(
  '/manualgrading/grade/',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let data = await db.any(
        `
                BEGIN;
                    DELETE FROM test_instance_question_manual_grade WHERE id_test_instance_question = $(id_test_instance_question);
                    INSERT INTO test_instance_question_manual_grade(id_test_instance_question, score, comment, id_app_user)
                        VALUES ($(id_test_instance_question),  $(score), $(comment), $(id_app_user));
                COMMIT;
                SELECT * FROM test_instance_question_manual_grade WHERE id_test_instance_question = $(id_test_instance_question);
                `,
        {
          id_test_instance_question: req.body.id_test_instance_question,
          score: req.body.score,
          comment: req.body.comment,
          id_app_user: req.session.appUserId,
        }
      );
      if (data && data.length) {
        // console.log(data[0]);
        res.json({
          success: true,
          data: data[0],
        });
      } else {
        res.json({
          success: false,
        });
      }
    } catch (error) {
      res.json({
        success: false,
        error: JSON.stringify(error),
      });
    }
  }
);
router.post(
  '/manualgrading/setfilter/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.body.action === 'clear') {
      delete req.session.filter;
      req.flash('info', `Filter cleared.`);
    } else if (
      req.body.action === 'set' &&
      (req.body.filterFrom + req.body.filterTo + '').trim() !== ''
    ) {
      req.session.filter = {
        filterFrom: (req.body.filterFrom || '').trim(),
        filterTo: (req.body.filterTo || '').trim(),
      };
      req.flash('info', `Filter set to [${req.body.filterFrom}, ${req.body.filterTo}]`);
    } else {
      req.flash('error', 'Nothing done, filter invalid.');
    }
    res.redirect('back');
  }
);

router.post(
  '/manualgrading/applygrading/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    try {
      let applyRes = await db.func('apply_manual_grading', [parseInt(req.params.id_test)]);
      if (applyRes && applyRes[0] && applyRes[0].apply_manual_grading === 0) {
        req.flash(
          'info',
          `Scores updated. Please check the scores in "All exam instances" and students' reviews.`
        );
      } else {
        req.flash('error', `Error, function returned: ${JSON.stringify(applyRes)}`);
      }
    } catch (error) {
      winston.error(error);
      req.flash('error', error.toString());
    }
    res.redirect('back');
  }
);

router.post(
  '/manualgrading/initgrading/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    let idTest = parseInt(req.params.id_test);
    if (idTest) {
      try {
        let res = await db.result(
          `INSERT INTO
                        test_instance_question_manual_grade (id_test_instance_question, score, comment)
                    SELECT
                        tiq.id,
                        tiq.score,
                        '-- automated assessment --'
                    FROM
                        test_instance_question tiq
                        join test_instance on tiq.id_test_instance = test_instance.id
                        and test_instance.id_test = $(id_test)
                    WHERE
                        NOT EXISTS (
                            SELECT
                                *
                            FROM
                                test_instance_question_manual_grade man_grade
                            WHERE
                                man_grade.id_test_instance_question = tiq.id
                        )`,
          {
            id_test: idTest,
          }
        );
        req.flash('info', `${res.rowCount} record(s) affected.`);
      } catch (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
      }
      res.redirect('back');
    }
  }
);

router.get(
  '/instances/manualgrading/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    let filterClause = '';
    if (req.session.filter) {
      // console.log('FILTER', req.session.filter);
      if (req.session.filter.filterFrom && req.session.filter.filterTo) {
        filterClause = ' AND (student.last_name BETWEEN $(from) AND $(to)) ';
      } else if (req.session.filter.filterFrom) {
        filterClause = ' AND (student.last_name >= $(from)) ';
      } else if (req.session.filter.filterTo) {
        filterClause = ' AND (student.last_name <= $(to)) ';
      }
    }
    db.any(
      `
        SELECT  test_instance_question.id,
                test.title,
                student.first_name,
                student.last_name,
                student_course.class_group,
                id_test_instance,
                '[' || id_question || ']' as id_question,
                '(' ||  test_instance_question.ordinal || ')' as ordinal,
                substring(question_text from 1 for 100) as question_text,
                test_instance_question.score as submit_score,
                man_grade.score as man_grade_score,
                teacher.last_name || ', ' || substring(teacher.first_name from 1 for 1) || '. ' as teacher_name
            FROM student
            JOIN student_course ON student.id = student_course.id_student
            JOIN test ON student_course.id_course = test.id_course
             AND student_course.id_academic_year = test.id_academic_year
             AND test.id = $(id_test)
            JOIN test_instance
              ON test_instance.id_student = student.id
             AND test_instance.id_test = test.id
            JOIN test_instance_question
              ON test_instance.id = test_instance_question.id_test_instance
            JOIN question
              ON test_instance_question.id_question = question.id
             AND id_test = $(id_test)
             LEFT JOIN test_instance_question_manual_grade man_grade
                    ON test_instance_question.id = man_grade.id_test_instance_question
             LEFT JOIN app_user teacher
                    ON man_grade.id_app_user = teacher.id
             WHERE ts_submitted IS NOT NULL
                ${filterClause}
            ORDER BY last_name, ordinal
            `,
      {
        id_test: req.params.id_test,
        from: (req.session.filter && req.session.filter.filterFrom) || '',
        to: (req.session.filter && req.session.filter.filterTo) || '',
      }
    )
      .then((data) => {
        res.appRender(
          'testInstancesManualGradingView',
          {
            pageTitle: 'Manual grading (' + (data && data.length) + ' items)',
            title:
              'Student that have SUBMITED the test "' + (data && data[0] && data[0].title) + '"',
            id_test: req.params.id_test,
            list: {
              headers: [
                {
                  select: '(De)Select UP',
                  __raw: true,
                  __formatter: function (row) {
                    return `
                                <label class="form-check-label d-flex justify-content-between"><input type="checkbox" class="form-check-input" id="chk${row.id}" name="ids[]" value="${row.id}">
                                <button class="btn btn-success edgar-btn-select-up" value="${row.id}">
                                    <i class="fas fa-check-square"></i>
                                </button>
                                <button class="btn btn-warning edgar-btn-deselect-up" value="${row.id}">
                                <i class="far fa-check-square"></i>
                                </button>
                                `;
                  },
                },
                {
                  id: 'Id',
                },
                {
                  image: 'image',
                  __raw: true,
                  __formatter: function (row) {
                    return utils.getTinyImageHtml(row.alt_id2);
                  },
                },
                {
                  first_name: 'First name',
                },
                {
                  last_name: 'Last name',
                },
                {
                  class_group: 'Class group',
                },
                {
                  id_test_instance: 'TI Id',
                },
                {
                  id_question: 'Q.Id',
                },
                {
                  ordinal: 'Ordinal',
                },
                {
                  question_text: 'Question text (first 100 chars)',
                },
                {
                  submit_score: 'Score',
                },
                {
                  man_grade_score: 'Manual score',
                },
                {
                  teacher_name: 'Teacher',
                },
              ],
              rows: data,
            },
          },
          'testInstancesManualGradingViewScript'
        );
      })
      .catch((error) => {
        return next(error);
      });
  }
);
router.get(
  '/instances/manualgrading/stats/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let qperOrdinal = db.any(
        `
                select tiq.ordinal, array_agg(mg.score) as scores
                 from test_instance_question_manual_grade mg
                  join test_instance_question tiq
                    on mg.id_test_instance_question = tiq.id
                  join test_instance ti
                    on tiq.id_test_instance = ti.id
                  join app_user
                    on mg.id_app_user = app_user.id
                 where ti.id_test = $(id_test)
                 group by tiq.ordinal
                 ORDER BY tiq.ordinal
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qperExamPart = db.any(
        `
                select test_part.ordinal, array_agg(mg.score) as scores
                 from test_instance_question_manual_grade mg
                  join test_instance_question tiq
                    on mg.id_test_instance_question = tiq.id
                  JOIN test_part
                    ON tiq.id_test_part = test_part.id
                  join test_instance ti
                    on tiq.id_test_instance = ti.id
                  join app_user
                    on mg.id_app_user = app_user.id
                 where ti.id_test = $(id_test)
                 group by test_part.ordinal
                 ORDER BY test_part.ordinal
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qperExamPartPerTeacher = db.any(
        `
                select test_part.ordinal, last_name, array_agg(mg.score) as scores
                 from test_instance_question_manual_grade mg
                  join test_instance_question tiq
                    on mg.id_test_instance_question = tiq.id
                  JOIN test_part
                    ON tiq.id_test_part = test_part.id
                  join test_instance ti
                    on tiq.id_test_instance = ti.id
                  join app_user
                    on mg.id_app_user = app_user.id
                 where ti.id_test = $(id_test)
                 group by test_part.ordinal, last_name
                 ORDER BY test_part.ordinal, last_name
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qperQuestionOrdinal = db.any(
        `
	            select tiq.id_question
                    ,  case when (avg(tiq.ordinal)::int = avg(tiq.ordinal)) then avg(tiq.ordinal)::int else round(avg(tiq.ordinal),2) end as  avg_ordinal
                    , array_agg(mg.score) as scores
                from test_instance_question_manual_grade mg
                    join test_instance_question tiq
                    on mg.id_test_instance_question = tiq.id
                    join test_instance ti
                    on tiq.id_test_instance = ti.id
                    join app_user
                    on mg.id_app_user = app_user.id
                where id_test = $(id_test)
                group by tiq.id_question
                ORDER BY avg(tiq.ordinal)
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qperQuestionOrdinalPerTeacher = db.any(
        `
                select tiq.id_question, app_user.last_name, substring(question_text from 1 for 75) as question_text
                    , case when (avg(tiq.ordinal)::int = avg(tiq.ordinal)) then avg(tiq.ordinal)::int else round(avg(tiq.ordinal),2) end as  avg_ordinal
                    , array_agg(mg.score) as scores

                from test_instance_question_manual_grade mg
                    join test_instance_question tiq
                    on mg.id_test_instance_question = tiq.id
                    join test_instance ti
                    on tiq.id_test_instance = ti.id
                    join app_user
                    on mg.id_app_user = app_user.id
                    join question
                    on tiq.id_question = question.id
                where id_test = $(id_test)
                group by tiq.id_question, app_user.last_name, question_text
                ORDER BY last_name
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qExamTeacherStats = db.any(
        `
                 with perStudent as (select id_student, mg.id_app_user, alt_id2, last_name
                 , sum(mg.score) as exam_score
                 , 100. * count(*) / test.questions_no as exam_coverage
                 from test_instance_question_manual_grade mg
                 join test_instance_question tiq
                 on mg.id_test_instance_question = tiq.id
                 join test_instance ti
                 on tiq.id_test_instance = ti.id
                 join test
                 on ti.id_test = test.id
                 join app_user
                 on mg.id_app_user = app_user.id
                 where id_test = $(id_test)
                 group by id_student, mg.id_app_user, test.questions_no, alt_id2, last_name
                    )
                    select alt_id2, last_name, count(*) as exams
                    , round(avg (exam_coverage), 2) || '%' as avg_coverage
                    , round(avg(exam_score), 2) as avg_score
                    , PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY exam_score) as median
                    , round(stddev(exam_score), 2) as stddev
                    , array_agg(exam_score) as scores
                    from perStudent
                    group by 1, 2
                    ORDER BY last_name
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let qTableStatsQuestion = db.any(
        `
                 select tiq.ordinal, string_agg(distinct '' || tiq.id_question, ', ') as id_question
                 , app_user.alt_id2, app_user.last_name as teacher
                 , count(mg.score)
                 , round(min(mg.score), 2) as min_score
                 , round(avg(mg.score), 2) as avg_score
                 , PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY mg.score) as median, round(max(mg.score), 2) as max_score
                 , round(stddev(mg.score), 2) as stddev
                 from test_instance_question_manual_grade mg
                 join test_instance_question tiq
                 on mg.id_test_instance_question = tiq.id
                 join test_instance ti
                 on tiq.id_test_instance = ti.id
                 join app_user
                 on mg.id_app_user = app_user.id
                 where id_test = $(id_test)
                 group by tiq.ordinal, teacher, alt_id2
                ORDER BY tiq.ordinal, last_name
                `,
        {
          id_test: req.params.id_test,
        }
      );
      let [
        perOrdinal,
        perExamPart,
        perExamPartPerTeacher,
        perQuestionOrdinal,
        perQuestionOrdinalPerTeacher,
        perExamTeacher,
        tableStatsQuestion,
      ] = await Promise.all([
        qperOrdinal,
        qperExamPart,
        qperExamPartPerTeacher,
        qperQuestionOrdinal,
        qperQuestionOrdinalPerTeacher,
        qExamTeacherStats,
        qTableStatsQuestion,
      ]);

      res.appRender('manualGradingStatsView', {
        pageTitle: 'Man grading stats',
        id_test: req.params.id_test,
        perOrdinal,
        perExamPart,
        perExamPartPerTeacher,
        perQuestionOrdinal,
        perQuestionOrdinalPerTeacher,
        perExamTeacher,
        examTeacherTableStats: {
          headers: [
            {
              image: 'image',
              __raw: true,
              __formatter: function (row) {
                return utils.getTinyImageHtml(row.alt_id2);
              },
            },
            {
              last_name: 'Last name',
            },
            {
              exams: 'Exams',
            },
            {
              avg_coverage: 'Avg coverage',
            },
            {
              avg_score: 'Avg score',
            },
            {
              median: 'Median',
            },
            {
              stddev: 'StdDev',
            },
          ],
          rows: perExamTeacher,
          class: 'table table-sm table-hover edgar no-dt',
        },
        tableStatsQuestion: {
          headers: [
            {
              image: 'image',
              __raw: true,
              __formatter: function (row) {
                return utils.getTinyImageHtml(row.alt_id2);
              },
            },
            {
              ordinal: 'Ordinal',
            },
            {
              id_question: 'Question(s)',
            },
            {
              teacher: 'Last name',
            },
            {
              count: 'Count',
            },
            {
              min_score: 'Min score',
            },
            {
              avg_score: 'Avg score',
            },
            {
              median: 'Median',
            },
            {
              max_score: 'Max score',
            },
            {
              stddev: 'StdDev',
            },
          ],
          rows: tableStatsQuestion,
          // links: [{
          //     __formatter: function (row) {
          //         return row.id_question;
          //     },
          //     target: '_blank',
          //     label: 'Question',
          //     href: function (row) {
          //         return `/question/edit/${row.id_question}`;
          //     },
          // }],
          class: 'table table-sm table-hover edgar no-dt',
        },
      }); // , 'manualGradingStatsViewScript'
    } catch (error) {
      return next(error);
    }
  }
);
router.get(
  '/manualgrading/report',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let ids = req.query.ids.split(',');
      ids = 'IN ( ' + ids.map((x) => parseInt(x)).join(',') + ')';
      let data = await db.any(`
            SELECT  student.alt_id2,
                    student.first_name,
                    last_name,
                    id_test_instance,
                    question.id  as id_question,
                    test_instance_question.ordinal as ordinal,
                    question_text,
                    question_comment,
                    type_name,
                    case when (sql_question_Answer is not null) then
                        sql_question_answer.sql_answer
                    when (c_question_answer is not null) then
                        c_question_Answer.c_source
                    when (json_question_answer.json_answer is not null) then
                        json_question_answer.json_answer
                    else
                        text_question_answer.text_answer
                    end as correct_answer,
                    case when (student_answer_text is not null) then
                        student_answer_text
                    else
                        student_answer_code
                    end as student_answer,
                    uploaded_files,
                    test_instance_question.score,
                    test_instance_question.score_perc,
                    test_instance_question.hint,
                    test_instance_question.c_eval_data,
                    test_instance_question.id as id_test_instance_question,
                    test_instance_question_manual_grade.score as manual_score,
                    test_instance_question_manual_grade.comment as manual_comment,
                    question.grader_object,
                    json_build_object('correct_score', correct_score, 'incorrect_score', incorrect_score, 'unanswered_score', unanswered_score) as grading_model_json
                    FROM test_instance
                    JOIN student
                    ON test_instance.id_student = student.id
                    JOIN test
                      ON test_instance.id_test = test.id
                    JOIN test_instance_question
                      ON test_instance.id = test_instance_question.id_test_instance
                    JOIN question
                      ON test_instance_question.id_question = question.id
                    JOIN question_type
                      ON question.id_question_type = question_type.id
                    JOIN grading_model
                      ON test_instance_question.id_grading_model = grading_model.id
                    LEFT JOIN text_question_answer
                       ON question.id = text_question_answer.id_question
                    LEFT JOIN sql_question_answer
                       ON question.id = sql_question_Answer.id_question
                    LEFT JOIN c_question_answer
                       ON question.id = c_question_answer.id_question
                    LEFT JOIN json_question_answer
                       ON question.id = json_question_answer.id_question
                    LEFT JOIN test_instance_question_manual_grade
                       ON test_instance_question.id = test_instance_question_manual_grade.id_test_instance_question
                WHERE test_instance_question.id ${ids}
                ORDER BY last_name, test_instance_question.ordinal
            `);
      for (let i = 0; i < data.length; ++i) {
        data[i].questionHtml = await utils.md2Html(data[i].question_text);
        if (data[i].question_comment)
          data[i].questionCommentHtml = await utils.md2Html(data[i].question_comment);

        data[i].correctHtml =
          data[i].type_name.toUpperCase().indexOf('FREE TEXT') >= 0
            ? await utils.md2Html(data[i].correct_answer || '')
            : '<pre><code>' +
              (await utils.md2Html('````\n' + (data[i].correct_answer || '') + '\n````')) +
              '</code></pre>';
        data[i].imageUrl = utils.getTinyImageHtml(data[i].alt_id2);
        //data[i].student_answer = escape(data[i].student_answer);
        data[i].uploaded_files = data[i].uploaded_files
          ? `/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/resolved/${data[i].uploaded_files}`
          : undefined;
      }
      res.fullScreenRender(
        '~/test/testInstancesManualGradingReportView',
        {
          pageTitle: 'Grading: ' + (data && data[0] && data[0].last_name),
          qs: data,
        },
        'testInstancesManualGradingReportViewScript'
      );
    } catch (error) {
      return next(error);
    }
  }
);
router.get(
  '/instances/notstarted/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.any(
      `
                SELECT student.id,
                       student.first_name || ' ' || last_name AS full_name,
                       alt_id,
                       alt_id2,
                       email,
                       test.title,
                       student_course.class_group
                FROM student
                JOIN student_course ON student.id = student_course.id_student
                JOIN test ON student_course.id_course = test.id_course
                AND student_course.id_academic_year = $(id_academic_year) --BCS global exams...test.id_academic_year
                AND test.id = $(id_test)
                LEFT JOIN test_instance ON test_instance.id_student = student.id
                AND id_test = $(id_test)
                WHERE test_instance.ts_started IS NULL
                ORDER BY last_name`,
      {
        id_test: req.params.id_test,
        id_academic_year: req.session.currAcademicYearId,
      }
    )
      .then((data) => {
        res.appRender('testInstancesStalkView', {
          pageTitle: 'Not started',
          title: 'Student that have NOT started the test: ',
          list: {
            headers: [
              {
                id: 'Id',
              },
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return utils.getTinyImageHtml(row.alt_id2);
                },
              },
              {
                alt_id: 'alt_id',
              },
              {
                alt_id2: 'alt_id2',
              },
              {
                email: 'email',
              },
              {
                full_name: 'Full name',
              },
              {
                class_group: 'Class group',
              },
            ],
            rows: data,
          },
        });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching /instances/notstarted/test')
        );
      });
  }
);

router.get(
  '/instances/submitted/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    var studList;
    db.any(
      `
                SELECT student.id,
                       test_instance.id as id_test_instance,
                       student.first_name,
                       last_name,
                        alt_id,
                        alt_id2,
                        test.title,
                        ts_started::timestamp(0)::varchar,
                        ts_submitted::timestamp(0)::varchar,
                        (ts_submitted - ts_started)::interval(0)::varchar as duration,
                        ip_address,
                        score,
                        100 * score_perc as score_perc,
                        student_course.class_group
                FROM student
                JOIN student_course ON student.id = student_course.id_student
                JOIN test ON student_course.id_course = test.id_course
                AND student_course.id_academic_year = $(id_academic_year)
                JOIN test_instance ON test_instance.id_student = student.id
                AND test_instance.id_test = test.id
                WHERE id_test = $(id_test)
                  AND ts_submitted IS NOT NULL
                ORDER BY score DESC`,
      {
        id_test: req.params.id_test,
        id_academic_year: req.session.currAcademicYearId,
      }
    )
      .then((data) => {
        studList = data;
        return TestLog.find({
          _id: { $in: studList.map((x) => x.id_test_instance) },
          'events.eventName': 'Running code',
        });
      })
      .then((coderuns) => {
        studList.forEach(function (row) {
          row.coderuns = 0;
          for (let i = 0; i < coderuns.length; ++i) {
            if (row.id_test_instance === coderuns[i]._id) {
              if (coderuns[i].events) {
                row.coderuns = coderuns[i].events.filter(
                  (x) => x.eventName === 'Running code'
                ).length;
              }
              break;
            }
          }
        });
        res.appRender('testInstancesStalkView', {
          pageTitle: 'Submitted',
          title: 'Student that have submitted the test: ',
          list: {
            headers: [
              {
                id_test_instance: 'ti.Id',
              },
              {
                id: 'stud.Id',
              },
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return utils.getTinyImageHtml(row.alt_id2);
                },
              },
              {
                alt_id: 'alt_id',
              },
              {
                alt_id2: 'alt_id2',
              },
              {
                first_name: 'First name',
              },
              {
                last_name: 'Last name',
              },
              {
                class_group: 'Class group',
              },
              {
                ts_started: 'Started',
              },
              {
                ts_submitted: 'Submitted',
              },
              {
                duration: 'Duration',
              },
              {
                coderuns: 'Code runs',
              },
              {
                ip_address: 'IP Address',
              },
              {
                score: 'Score',
              },
              {
                score_perc: 'Score %',
              },
            ],
            rows: studList,
            buttons: [
              {
                label: 'Re-evaluate',
                method: 'POST',
                action: function (row) {
                  // '/:id_test([0-9]{1,10})/submittedinstances/delete/:id_test_instance([0-9]{1,10})'
                  return (
                    `/test/${req.params.id_test}/submittedinstances/re-evaluate/` +
                    row['id_test_instance']
                  );
                },
                class: function (row) {
                  return 'btn btn-outline-warning';
                },
                props: {
                  edgar_confirm_text:
                    'Do you really want to re-evaluate (re-score) this test instance?',
                },
              },
              {
                label: 'Unsubmit',
                method: 'POST',
                action: function (row) {
                  // '/:id_test([0-9]{1,10})/submittedinstances/delete/:id_test_instance([0-9]{1,10})'
                  return (
                    `/test/${req.params.id_test}/submittedinstances/unsubmit/` +
                    row['id_test_instance']
                  );
                },
                class: function (row) {
                  return 'btn btn-outline-warning';
                },
                props: {
                  edgar_confirm_text: 'Do you really want to unsubmit this test instance?',
                },
              },
              {
                label: 'Delete',
                method: 'POST',
                action: function (row) {
                  // '/:id_test([0-9]{1,10})/submittedinstances/delete/:id_test_instance([0-9]{1,10})'
                  return (
                    `/test/${req.params.id_test}/submittedinstances/delete/` +
                    row['id_test_instance']
                  );
                },
                class: function (row) {
                  return 'btn btn-outline-danger';
                },
                props: {
                  edgar_confirm_text: 'Do you really want to delete this test instance?',
                },
              },
            ],
            links: [
              {
                label: 'Review',
                target: '_blank',
                href: function (row) {
                  return `/test/instances/stalk/student/${row.id}/instance/${row.id_test_instance}`;
                },
              },
            ],
          },
        });
      })
      .catch((error) => {
        return next(
          new errors.DbError('An error occured while fetching /instances/submitted/test/')
        );
      });
  }
);
// instances/stalk/student/6231/instance/53525

router.get(
  '/instances/stalk/id/:id_test_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    var studTest;
    db.one(
      `
                SELECT student.alt_id, student.id, student.first_name, student.last_name,
                       test.title, ts_submitted::timestamp(0)::varchar, test_instance.id as id_test_instance
                  FROM test_instance
                  JOIN student on test_instance.id_student = student.id
                  JOIN test ON test_instance.id_test = test.id
                 WHERE test_instance.id =  $(id_test_instance)`,
      {
        id_test_instance: req.params.id_test_instance,
      }
    )
      .then(function (student) {
        studTest = student;
        studTest.stalkLink = `/test/instances/stalk/student/${studTest.id}/instance/${req.params.id_test_instance}`;
        var promises = [];
        promises.push(
          Heartbeat.find({
            _id: req.params.id_test_instance,
          }).exec()
        );
        promises.push(
          TestLog.find({
            _id: req.params.id_test_instance,
          }).exec()
        );
        promises.push(
          OngoingTests.find({
            _id: student.alt_id,
          }).exec()
        );
        promises.push(
          db.any(
            `
                    SELECT  ordinal,
                            id_question,
                            to_char(ts_created, 'YYYY-MM-DD HH24:MI:SS') as ts_created,
                            id_test_instance
                      FROM test_instance_question
                     WHERE id_test_instance = $(id_test_instance)
                     ORDER BY ordinal`,
            {
              id_test_instance: req.params.id_test_instance,
            }
          )
        );
        return Promise.all(promises);
      })
      .then(function (args) {
        res.appRender('testInstancesStalkOngoingView', {
          pageTitle: `Stalk (${studTest && studTest.length} students)`,
          hbs: args[0],
          testlogs: args[1],
          ongoingtests: args[2][0],
          questions: args[3],
          studTest: studTest,
        });
      })
      .catch(function (error) {
        winston.error(error);
      });
  }
);

router.get(
  '/plagiarism/compare/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let data = await realTimePlagService.getQuestionCompareData(
      req.params.id_test,
      req.params.id_question,
      req.query.idFirstStudent,
      req.query.idSecondStudent
    );
    res.appRender(
      'realTimeQuestionCompareView',
      {
        ...data,
        autorefresh: req.query.ar,
      },
      'realTimeQuestionCompareViewScript'
    );
  }
);

router.get(
  '/plagiarism/compare/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    const id_plag_method = (
      await db.one(`SELECT id_plag_detection_algorithm FROM test WHERE id = ${req.params.id_test}`)
    ).id_plag_detection_algorithm;

    let data = await realTimePlagService.getTestCompareData(
      req.params.id_test,
      req.query.idFirstStudent,
      req.query.idSecondStudent,
      id_plag_method
    );
    res.appRender(
      'realTimeTestCompareView',
      {
        pageTitle: 'RT compare',
        ...data,
        autorefresh: req.query.ar,
      },
      'realTimeTestCompareViewScript'
    );
  }
);

router.get(
  '/instances/plagiarism/test/:id_test([0-9]{1,10})/question/:id_question([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    const id_plag_method = (
      await db.one(`SELECT id_plag_detection_algorithm FROM test WHERE id = ${req.params.id_test}`)
    ).id_plag_detection_algorithm;

    let data = await realTimePlagService.getQuestionTableData(
      req.params.id_test,
      req.params.id_question,
      req.query.cutoff,
      id_plag_method
    );
    res.appRender(
      'realTimePlagView',
      {
        pageTitle: 'RT plag',
        ...data,
        autorefresh: req.query.ar,
        cutoff: req.query.cutoff,
      },
      'realTimePlagViewScript'
    );
  }
);

router.get(
  '/instances/plagiarism/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    const id_plag_method = (
      await db.one(`SELECT id_plag_detection_algorithm FROM test WHERE id = ${req.params.id_test}`)
    ).id_plag_detection_algorithm;

    let data = await realTimePlagService.getExamTableData(
      req.params.id_test,
      req.query.cutoff,
      id_plag_method
    );
    res.appRender(
      'realTimePlagView',
      {
        pageTitle: 'RT plag',
        ...data,
        autorefresh: req.query.ar,
        cutoff: req.query.cutoff,
      },
      'realTimePlagViewScript'
    );
  }
);

router.get(
  '/instances/ongoing/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res) {
    try {
      let stalkData = await testService.getStalkData(
        req.params.id_test,
        req.session.currAcademicYearId
      );
      let tabledata;
      stalkData.fullView = true;
      if (req.query.view !== 'cards') {
        tabledata = {
          students: {
            headers: [
              {
                image: 'image',
                __raw: true,
                __formatter: function (row) {
                  return utils.getTinyImageHtml(row.alt_id2);
                },
              },
              {
                full_name: 'Name',
              },
              {
                ip_address: 'IP(s)',
              },
              {
                ts_started: 'Started',
              },
              {
                duration: 'Duration',
              },
              {
                room: 'Room',
              },
              {
                hbCount: 'HB count',
                __raw: true,
                __formatter: function (row) {
                  return `<strong><i class="fa fa-heart" style="color:${
                    row.hbColor
                  }"></i></strong>&nbsp;${row.hbCount ? row.hbCount : ''}`;
                },
              },
              {
                lastHbAgo: 'HB ago',
              },
              {
                progress: 'Progress',
              },
              {
                lfs: 'LF count',
              },
              {
                lfsTotal: 'LF total',
              },
              {
                lfsMax: 'LF max',
              },
              {
                lfsOn: 'LF since',
              },
            ],
            rows: stalkData.rs,
            links: [
              {
                label: 'Stalk',
                target: '_blank',
                __raw: true,
                href: function (row) {
                  return '/test/instances/stalk/id/' + row.id_test_instance;
                },
                __formatter: function (row) {
                  return `<i class="fa fa-2 fa-eye"></i>`;
                },
              },
            ],
            buttons: [
              {
                label: 'Delete',
                method: 'POST',
                action: function (row) {
                  return `/test/${row.id_test}/ongoinginstances/delete/${row.id_test_instance}`;
                },
                class: function (row) {
                  return 'btn btn-outline-danger';
                },
                props: {
                  edgar_confirm_text: 'Do you really want to delete this test instance?',
                },
              },
            ],
          },
        };
      }

      // let questions = await db.any(`
      //           SELECT DISTINCT id_question
      //           FROM test_instance_question
      //           JOIN test_instance ON test_instance.id = test_instance_question.id_test_instance
      //           WHERE id_test_instance in (SELECT id from test_instance where id_test = ${req.params.id_test})
      //           ORDER BY id_question;
      //       `);
      // let questionList = [];
      // for (let question of questions) {
      //   questionList.push(question.id_question);
      // }

      // const id_plag_method = (
      //   await db.one(
      //     `SELECT id_plag_detection_algorithm FROM test WHERE id = ${req.params.id_test}`
      //   )
      // ).id_plag_detection_algorithm;

      res.appRender('testInstancesOngoingView', {
        pageTitle: `Ongoing exams for ` + req.params.id_test,
        view: req.query.view || 'table',
        ...stalkData,
        ...tabledata,
        id_test: req.params.id_test,
      });
    } catch (error) {
      winston.error(error);
    }
  }
);

var getStudResultList = function (idCourse, idAcademicYear) {
  return db
    .any(
      `
       WITH test_manual as (
     SELECT distinct test.id + 1e6 as id, test.id as id_orig, test.title || '(manual)' as title, id_course, id_academic_year, test_score_ignored,
            title_abbrev || '-M' as title_abbrev, test.test_ordinal + 100 as test_ordinal
       FROM test_instance_question_manual_grade
       JOIN test_instance_question
         ON test_instance_question_manual_grade.id_test_instance_question = test_instance_question.id
       JOIN test_instance
         ON test_instance_question.id_test_instance = test_instance.id
       JOIN test
         ON test_instance.id_test = test.id
       WHERe id_course = $(id_course)
         ANd id_academic_year = $(id_academic_year)
         AND not test_score_ignored
         AND title_abbrev IS NOT NULL
         AND length(trim(title_abbrev)) <> 0
       ),
      test_all as (
            SELECT id, title, id_course, id_academic_year, test_score_ignored, title_abbrev, test_ordinal from test
            UNION ALL
            SELECT id, title, id_course, id_academic_year, test_score_ignored, title_abbrev, test_ordinal from test_manual  )

        SELECT string_agg(COALESCE('"' || title_abbrev || '"', 'Exam' || test_ordinal ) :: varchar(100) || ' varchar(100)', ', ' ORDER BY test_ordinal) as test_abbrev
            FROM test_all
            WHERE id_course = $(id_course)
            AND id_academic_year = $(id_academic_year)
            AND not test_score_ignored
            AND title_abbrev IS NOT NULL
            AND length(trim(title_abbrev)) <> 0
        `,
      {
        id_course: idCourse,
        id_academic_year: idAcademicYear,
      }
    )
    .then((result) => {
      if (result && result.length && result[0] && result[0].test_abbrev) {
        return db.any(
          `
            SELECT * from CROSSTAB (
            $$
        WITH  test_manual as (
                    SELECT DISTINCT test.id + 1e6 as id, test.id as id_orig, test.title || '(manual)' as title, id_course, id_academic_year, test_score_ignored,
                    title_abbrev  || '-M' as title_abbrev, test.test_ordinal + 100 as test_ordinal
                    FROM test_instance_question_manual_grade
                    JOIN test_instance_question
                        ON test_instance_question_manual_grade.id_test_instance_question = test_instance_question.id
                    JOIN test_instance
                        ON test_instance_question.id_test_instance = test_instance.id
                    JOIN test
                        ON test_instance.id_test = test.id
                    WHERE id_course = $(id_course)
                    AND id_academic_year = $(id_academic_year)
                    AND not test_score_ignored
                    AND title_abbrev IS NOT NULL
                    AND length(trim(title_abbrev)) <> 0
            ),
            test_instance_manual as (
                    SELECT id_student, id_test + 1e6 as id_test, sum(test_instance_question_manual_grade.score) as score
                    FROM test_instance
                    JOIN test_instance_question
                        ON test_instance_question.id_test_instance = test_instance.id
                    JOIN test_instance_question_manual_grade
                        ON test_instance_question_manual_grade.id_test_instance_question = test_instance_question.id
                    WHERE test_instance.id_test in (select id_orig from test_manual)
                    GROUP BY id_student, id_test
            ),
            test_all as (
                    SELECT id, title, id_course, id_academic_year, test_score_ignored, title_abbrev, test_ordinal
                     FROM test
                     WHERE id_course = $(id_course)
                       AND id_academic_year = $(id_academic_year)
                       AND NOT test_score_ignored
                       AND title_abbrev IS NOT NULL
                       AND length(trim(title_abbrev)) <> 0
                    UNION ALL
                    SELECT id, title, id_course, id_academic_year, test_score_ignored, title_abbrev, test_ordinal from test_manual
            ),
            test_instance_all as (
                    SELECT id_student, id_test, score
                      FROM test_instance
                      JOIN test_all
                        ON test_instance.id_test = test_all.id
                    UNION ALL
                    SELECT id_student, id_test, score
                      FROM test_instance_manual
            )

              SELECT alt_id2:: varchar(100), last_name, first_name, COALESCE(title_abbrev, 'Exam' || test_ordinal ):: varchar(100) as test_abbrev, MAX(score) :: numeric(10,3)
                FROM test_all
                JOIN student_course
                  ON student_course.id_course = test_all.id_course
                 AND student_course.id_academic_year = test_all.id_academic_year
                JOIN student
                  ON student.id = student_course.id_student
                 AND id_app_user is null
                LEFT JOIN test_instance_all
                       ON student.id = test_instance_all.id_student
                      AND test_instance_all.id_test = test_all.id
               WHERE test_all.id_course = $(id_course)
                 AND test_all.id_academic_year = $(id_academic_year)
                 AND not test_score_ignored
            GROUP BY last_name, first_name, student.alt_id2, test_abbrev, test_ordinal
            ORDER BY last_name, first_name, student.alt_id2, test_ordinal
        $$
            ,
        $$
             WITH  test_manual as (
                    SELECT DISTINCT test.id + 1e6 as id, test.id as id_orig, id_course, id_academic_year, test_score_ignored,
                       title_abbrev || '-M' as title_abbrev, test.test_ordinal + 100 as test_ordinal
                    FROM test_instance_question_manual_grade
                    JOIN test_instance_question
                        ON test_instance_question_manual_grade.id_test_instance_question = test_instance_question.id
                    JOIN test_instance
                        ON test_instance_question.id_test_instance = test_instance.id
                    JOIN test
                        ON test_instance.id_test = test.id
                    WHERE id_course = $(id_course)
                    AND id_academic_year = $(id_academic_year)
                    AND not test_score_ignored
                    AND title_abbrev IS NOT NULL
                    AND length(trim(title_abbrev)) <> 0
                ),
                test_all as (
                        SELECT id, title_abbrev, test_ordinal, id_course, id_academic_year, test_score_ignored from test
                        UNION ALL
                        SELECT id, title_abbrev, test_ordinal, id_course, id_academic_year, test_score_ignored from test_manual
                )
                select COALESCE(title_abbrev, 'Exam' || test_ordinal ) :: varchar(100) as test_abbrev from test_all where id_course = $(id_course) and id_academic_year = $(id_academic_year) and not test_score_ignored AND title_abbrev IS NOT NULL AND length(trim(title_abbrev)) <> 0 order by test_ordinal
        $$
            )
                as ct (alt_id2 varchar(100), last_name varchar(100),first_name varchar(100), ${result[0].test_abbrev}
            )`,
          {
            id_course: idCourse,
            id_academic_year: idAcademicYear,
          }
        );
      } else {
        return new Promise((resolve, reject) => resolve([]));
      }
    });
};

router.get(
  '/instances/csvresults',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    getStudResultList(req.session.currCourseId, req.session.currAcademicYearId).then((result) => {
      var header = {};
      Object.getOwnPropertyNames(result[0]).forEach(function (val) {
        header[val] = val;
      });
      result.unshift(header);
      csv.separator = ';';
      res.setHeader(
        'Content-disposition',
        `attachment; filename=EdgarTestResults_${req.session.currCourseId}_${req.session.currAcademicYearId}.csv`
      );
      res.csv(result);
    });
  }
);

router.post(
  '/instances/forwardgenerate/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res, next) {
    if (req.params.id_test) {
      try {
        // TODO: check teacher's permissions
        let exam = await db.one(
          `SELECT id, id_academic_year, id_course, password
                    FROM test
                    WHERE id = $(id_test)`,
          { id_test: req.params.id_test }
        );
        let studIds = await db.manyOrNone(
          `SELECT id_student
                    FROM student_course
                   WHERE id_academic_year = $(id_academic_year)
                     AND id_course = $(id_course)
                     AND NOT EXISTS (
                        SELECT id
                        FROM test_instance
                        WHERE student_course.id_student = test_instance.id_student
                            AND test_instance.id_test = $(id_test)
                        )
                    LIMIT 100`,
          {
            id_academic_year: exam.id_academic_year,
            id_course: exam.id_course,
            id_test: exam.id,
          }
        );
        let generatedIds = [];

        let promises = [];
        // for (let rowStud of studIds || []) {
        //     promises.push(
        //         db.tx(t => {
        //             // creating a sequence of transaction queries:
        //             // const q1 = t.none('START TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
        //             const q2 = t.func('chk_get_test_instance', [
        //                 rowStud.id_student,
        //                 req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        //                 exam.password,
        //                 exam.id_course,
        //                 true
        //             ]);

        //             // returning a promise that determines a successful transaction:
        //             return t.batch([q2]); // all of the queries are to be resolved;
        //         })
        //     );
        // }
        // let results = await Promise.all(promises);
        // req.flash('info', JSON.stringify(results));
        for (let rowStud of studIds || []) {
          let genRes = await db.func('chk_get_test_instance', [
            rowStud.id_student,
            req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            exam.password,
            exam.id_course,
            true,
          ]);
          if (genRes[0].err_code === 0) {
            generatedIds.push(genRes[0].id_test_instance);
          } else {
            req.flash(
              'error',
              'Generated ' +
                generatedIds.length +
                ' test instances, stopped on error:' +
                JSON.stringify(error)
            );
            break;
          }
        }
        req.flash(
          'info',
          `Yay, generated ${generatedIds.length} test instances: ${generatedIds.join(', ')}.`
        );
      } catch (error) {
        req.flash('error', JSON.stringify(error));
        res.redirect('/test/instances/list');
      }
    } else {
      req.flash('error', 'Test id NOT set.');
    }
    res.redirect('/test/instances/list');
  }
);

router.post(
  '/instances/deleteall/:id_test([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test) {
      db.func('del_all_test_instances', [req.params.id_test])
        .then(function (data) {
          winston.debug('del_all_test_instances returned: ' + data);
          req.flash('info', `All test instances for test ${req.params.id_test} deleted.`);
          res.redirect('/test/instances/list');
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect('/test/instances/list');
        });
    } else {
      req.flash('error', 'Test id NOT set.');
      res.redirect('/test/instances/list');
    }
  }
);
router.post(
  '/:id_test([0-9]{1,10})/submittedinstances/delete/:id_test_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test_instance) {
      db.func('del_test_instance', [req.params.id_test_instance])
        .then(function (data) {
          winston.debug('del_test_instance returned: ' + data);
          req.flash('info', `Test instance id ${req.params.id_test_instance} deleted.`);
          res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
        });
    } else {
      req.flash('error', 'Test id NOT set.');
      res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
    }
  }
);
router.post(
  '/:id_test([0-9]{1,10})/re-evaluate/:id_test_instance([0-9]{1,10})',
  async function (req, res) {
    if (req.params.id_test_instance) {
      try {
        await testService.reevaluateTestAsync(req.params.id_test_instance);
        req.flash('info', `Instance ${req.params.id_test_instance} re-evaluated.`);
        res.send('OK');
      } catch (error) {
        winston.error(error);
        res.send('ERROR');
      }
    } else {
      res.send('ERROR');
    }
  }
);
router.post(
  '/:id_test([0-9]{1,10})/submittedinstances/re-evaluate/:id_test_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  async function (req, res) {
    if (req.params.id_test_instance) {
      try {
        await testService.reevaluateTestAsync(req.params.id_test_instance);
        req.flash('info', `Instance ${req.params.id_test_instance} re-evaluated.`);
        res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
      } catch (error) {
        winston.error(error);
        req.flash('error', JSON.stringify(error));
        res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
      }
    } else {
      req.flash('error', 'Test instance id NOT set.');
      res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
    }
  }
);
router.post(
  '/:id_test([0-9]{1,10})/submittedinstances/unsubmit/:id_test_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test_instance) {
      db.result('UPDATE test_instance SET ts_submitted = NULL WHERE id = $(id_test_instance)', {
        id_test_instance: req.params.id_test_instance,
      })
        .then(function (data) {
          winston.debug('unsumbit returned: ' + data);
          req.flash(
            'info',
            `Test instance id ${req.params.id_test_instance} unsubmitted, student may start the test again.`
          );
          res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
        });
    } else {
      req.flash('error', 'Test id NOT set.');
      res.redirect(`/test/instances/submitted/test/${req.params.id_test}`);
    }
  }
);
router.post(
  '/:id_test([0-9]{1,10})/ongoinginstances/delete/:id_test_instance([0-9]{1,10})',
  middleware.requireRole(globals.ROLES.TEACHER),
  function (req, res, next) {
    if (req.params.id_test_instance) {
      db.func('del_test_instance', [req.params.id_test_instance])
        .then(function (data) {
          winston.debug('del_test_instance returned: ' + data);
          req.flash('info', `Test instance id ${req.params.id_test_instance} deleted.`);
          res.redirect(`/test/instances/ongoing/test/${req.params.id_test}`);
        })
        .catch(function (error) {
          winston.error(error);
          req.flash('error', JSON.stringify(error));
          res.redirect(`/test/instances/ongoing/test/${req.params.id_test}`);
        });
    } else {
      req.flash('error', 'Test id NOT set.');
      res.redirect(`/test/instances/ongoing/test/${req.params.id_test}`);
    }
  }
);

router.get(
  '/instances/list',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    db.any(
      `SELECT test.id,
                   test.test_ordinal,
                   test.title,
                   academic_year.title as academic_year_title,
                   test.ts_available_from::timestamp(0)::varchar as ts_available_from,
                   EXTRACT(EPOCH FROM(test.ts_available_from - CURRENT_TIMESTAMP)) as seconds_available_from,
                   test_type.type_name,
                   CASE WHEN is_global THEN 'GLBL' ELSE '-' END
                                || CASE WHEN is_public THEN ' PBLC' ELSE '-' END
                                || CASE WHEN use_in_stats THEN ' STATS' ELSE '-' END
                                || CASE WHEN test_score_ignored THEN ' SCIGNORED' ELSE '-' END as test_flags,
                   allow_anonymous_stalk,
                   (SELECT COUNT(DISTINCT id_test_instance)
                      FROM test_instance_question tiq
                      JOIN test_instance ti ON tiq.id_test_instance = ti.id
                     WHERE id_test = test.id
                       AND uploaded_files IS NOT NULL) as uploaded_files_no,
                   COUNT(DISTINCT CASE WHEN ts_started IS NULL THEN NULL ELSE student.id END) AS started,
                   COUNT(DISTINCT CASE WHEN ts_generated IS NULL THEN NULL ELSE student.id END) AS generated,
                   COUNT (DISTINCT CASE
                                       WHEN ts_submitted IS NULL THEN NULL
                                       ELSE student.id
                                   END) AS submitted,
                         COUNT(DISTINCT student.id) AS total
            FROM student
            JOIN student_course ON student.id = student_course.id_student
            JOIN test ON student_course.id_course = test.id_course
             AND student_course.id_academic_year = $(id_academic_year)  -- BCS global exams...
            JOIN academic_year ON test.id_academic_year = academic_year.id
            JOIN test_type
              ON test.id_test_type = test_type.id
             AND test_type.standalone = true
            LEFT JOIN test_instance
                   ON test_instance.id_student = student.id
                  AND id_test = test.id
            WHERE test.id_course = $(id_course)
              AND (test.id_academic_year = $(id_academic_year) OR test.is_global)
            GROUP BY test.id,
                     test.test_ordinal,
                     test.title,
                     academic_year_title,
                     test_type.type_name,
                     allow_anonymous_stalk
            ORDER BY academic_year_title DESC, test_ordinal DESC`,
      {
        id_course: req.session.currCourseId,
        id_academic_year: req.session.currAcademicYearId,
      }
    )
      .then((results) => {
        res.appRender('testInstancesStalkView', {
          pageTitle: 'Exam instances',
          title: 'Exams for the current year and course: ',
          list: {
            headers: [
              {
                id: 'Id',
              },
              {
                test_ordinal: '#Ord',
                __formatter: function (row) {
                  return `<b>${row.test_ordinal}</b><br><small>${row.academic_year_title}</small>`;
                },
                __raw: true,
              },
              {
                title: 'Title',
                __formatter: function (row) {
                  return `<b>${row.title}</b><br><small>${row.type_name}</small>`;
                },
                __raw: true,
              },
              {
                ts_available_from: 'Valid from',
                __formatter: function (row) {
                  return `${row.ts_available_from}<br><small><i>${moment
                    .duration({
                      seconds: row.seconds_available_from,
                    })
                    .humanize()} </i></small>`;
                },
                __raw: true,
              },
              {
                test_flags: 'Global Public UseStats ScoreIgnored',
              },
              {
                total: 'Total',
              },
              {
                started_perc: 'Started %',
                __formatter: function (row) {
                  return ((row.started / row.total) * 100).toFixed(2);
                },
              },
              {
                submitted_perc: 'Submitted %',
                __formatter: function (row) {
                  return ((row.submitted / row.total) * 100).toFixed(2);
                },
              },
            ],
            rows: results,
            buttons: [
              {
                label: 'Delete All',
                method: 'POST',
                action: function (row) {
                  return '/test/instances/deleteall/' + row['id'];
                },
                class: function (row) {
                  return 'btn btn-outline-danger';
                },
                props: {
                  edgar_confirm_text:
                    'Do you really want to delete ALL instances (started and submitted) for this test? <br>Like, doubly-doubly sure?',
                },
              },
              {
                label: 'Forward generate',
                method: 'POST',
                action: function (row) {
                  return '/test/instances/forwardgenerate/' + row['id'];
                },
                class: function (row) {
                  return 'btn btn-outline-warning';
                },
                props: {
                  edgar_confirm_text:
                    'Do you really want to forward generate 100 (or less) instances for this test? <br>Forward generating tests is useful when you expect a large number of users staring (generating) the test at the same time.',
                },
              },
            ],
            links: [
              {
                label: 'Generated',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/generated/test/' + row.id;
                },
                __formatter: function (row) {
                  return row.generated;
                },
              },
              {
                label: 'Files uploaded',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/attachments/' + row.id;
                },
                __formatter: function (row) {
                  return row.uploaded_files_no;
                },
              },
              {
                label: 'Submitted',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/submitted/test/' + row.id;
                },
                __formatter: function (row) {
                  return row.submitted;
                },
              },
              {
                label: 'Ongoing',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/ongoing/test/' + row.id;
                },
                __raw: true,
                __formatter: function (row) {
                  return (
                    row.started -
                    row.submitted +
                    (row.allow_anonymous_stalk
                      ? '<i title="Anonymous stalk enabled!" class="ml-2 fas fa-eye"></i>'
                      : '')
                  );
                },
              },
              {
                label: 'Not started',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/notstarted/test/' + row.id;
                },
                __formatter: function (row) {
                  return row.total - row.started;
                },
              },
              {
                label: 'Send message',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/sendmessage/test/' + row.id;
                },
                __raw: true,
                __formatter: function (row) {
                  return '<i class="fa fa-comment-dots"></i>';
                },
              },
              {
                label: 'Manual grading',
                target: '_blank',
                href: function (row) {
                  return '/test/instances/manualgrading/test/' + row.id;
                },
                __raw: true,
                __formatter: function (row) {
                  return '<i class="fas fa-pencil-alt"></i>';
                },
              },
              {
                label: 'Tickets',
                target: '_blank',
                href: function (row) {
                  return '/ticket/test/' + row.id;
                },
                __raw: true,
                __formatter: function (row) {
                  return '<i class="fas fa-tools"></i>';
                },
              },
            ],
          },
        });
      })
      .catch((error) => {
        winston.error(error);
        return next(
          new errors.DbError(
            `An error occured while fetching /instances/stalk: ${JSON.stringify(error)}`
          )
        );
      });
  }
);
router.get(
  '/instances/generated/test/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let results = await db.any(
        `SELECT test.id,
                test.test_ordinal,
                test.title,
                student.id,
                student.first_name || ' ' || last_name AS full_name,
                alt_id,
                alt_id2,
                email,
                test.title,
                student_course.class_group,
                test_instance.id as id_test_instance,
                test_instance.ts_generated::text as ts_generated,
                test_instance.ts_started::text as ts_started,
                test_instance.ts_submitted::text as ts_submitted
            FROM test
            JOIN student_course
              ON student_course.id_course = test.id_course
             AND student_course.id_academic_year = test.id_academic_year
            JOIN student
              ON student.id = student_course.id_student
            JOIN test_instance
              ON test_instance.id_student = student.id
             AND id_test = test.id
            WHERE test.id = $(id_test)
            ORDER BY student.last_name, student.first_name`,
        {
          id_test: req.params.id_test,
        }
      );

      res.appRender('generatedTestInstancesView', {
        pageTitle: 'Generated exams',
        title: 'Generated exams: ',
        list: {
          headers: [
            {
              title: 'Title',
            },
            {
              id: 'Id',
            },
            {
              image: 'image',
              __raw: true,
              __formatter: function (row) {
                return utils.getTinyImageHtml(row.alt_id2);
              },
            },
            {
              alt_id: 'alt_id',
            },
            {
              alt_id2: 'alt_id2',
            },
            {
              email: 'email',
            },
            {
              full_name: 'Full name',
            },
            {
              class_group: 'Class group',
            },
            {
              ts_generated: 'Generated',
            },
            {
              ts_started: 'Started',
            },
            {
              ts_submitted: 'Submitted',
            },
          ],
          rows: results,
          links: [
            {
              label: 'Preview test',
              target: '_blank',
              __raw: true,
              href: function (row) {
                return '/test/instances/preview/' + row.id_test_instance;
              },
              __formatter: function (row) {
                return `<i class="fa fa-2 fa-eye"></i>`;
              },
            },
          ],
        },
      });
    } catch (error) {
      winston.error(error);
      return next(error);
    }
  }
);
router.get(
  '/instances/preview/:id_test_instance([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    let qs = await testService.getTestInstancePreviewModel(req.params.id_test_instance);
    // break out of Layout - "full screen"
    res.fullScreenRender('~/shared/dumpTestView', {
      qs: qs,
    });
  }
);
router.get(
  '/instances/dumpcsv',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  function (req, res, next) {
    getStudResultList(req.session.currCourseId, req.session.currAcademicYearId)
      .then((results) => {
        var csvheaders = [];
        if (results && results.length) {
          Object.getOwnPropertyNames(results[0]).forEach(function (val) {
            var obj = {};
            obj[val] = val;
            csvheaders.push(obj);
          });
        }
        res.appRender('dumpCSVView', {
          pageTitle: 'Dump CSV',
          title: 'Scores for the current year and course: ',
          studList: {
            headers: csvheaders,
            rows: results,
          },
        });
      })
      .catch((error) => {
        winston.error(error);
        return next(
          new errors.DbError(
            `An error occured while fetching /instances/dumpcsv: ${JSON.stringify(error)}`
          )
        );
      });
  }
);

router.get(
  '/instances/attachments/:id_test([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER]),
  async function (req, res, next) {
    try {
      let files = await db.many(
        `SELECT last_name, first_name, alt_id2, uploaded_files
                          FROM test_instance_question tiq
                          JOIN test_instance ti on tiq.id_test_instance = ti.id
                          JOIN student on ti.id_student = student.id
                         WHERE id_test = $(id_test)
                           AND uploaded_files is not null
                      ORDER BY last_name
                `,
        {
          id_test: req.params.id_test,
        }
      );
      if (files && files.length) {
        var tmpMasterDir = tmp.dirSync();
        let errCount = 0;
        let errMessages = [];
        for (let i = 0; i < files.length; ++i) {
          let folderName = sanitize(
            files[i].last_name +
              '_' +
              files[i].first_name +
              (files[i].alt_id2 ? '_' + files[i].alt_id2 : '')
          );
          folderName = `${tmpMasterDir.name}/${folderName}`;
          if (!fs.existsSync(folderName)) {
            fs.mkdirSync(folderName);
          }

          try {
            if (utils.useMinio()) {
              winston.debug(
                'unzipping ' +
                  `minio(${globals.PUBLIC_TI_DOWNLOAD_FOLDER}, ${files[i].uploaded_files}` +
                  ' to ' +
                  folderName
              );
              await minioService.unzipToFolder(
                globals.PUBLIC_TI_DOWNLOAD_FOLDER,
                files[i].uploaded_files,
                folderName
              );
            } else {
              winston.debug(
                'unzipping ' +
                  `${global.appRoot}/${globals.PUBLIC_FOLDER}/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${files[i].uploaded_files}` +
                  ' to ' +
                  folderName
              );
              await unzipPromiseFromFileSystem(
                `${global.appRoot}/${globals.PUBLIC_FOLDER}/${globals.PUBLIC_TI_DOWNLOAD_FOLDER}/${files[i].uploaded_files}`,
                folderName
              );
            }
          } catch (error) {
            ++errCount;
            errMessages.push(
              `Error occured while unzipping ${files[i].uploaded_files} to ${folderName}: ${error.message}`
            );
          }
        }
        winston.debug('zipping files in ' + tmpMasterDir.name);
        if (errCount > 0) {
          errMessages.push(`---------------\nTry downloading these attachments manually.`);
          fs.writeFileSync(`${tmpMasterDir.name}/errors.txt`, errMessages.join('\n'));
        }
        res.zip({
          files: [{ path: tmpMasterDir.name, name: 'attachments' }],
          filename: `attachments-for-${req.params.id_test}${
            errCount ? 'errCount-' + errCount : ''
          }.zip`,
        });
        tmpMasterDir.removeCallback();
      } else {
        return null;
      }
    } catch (error) {
      winston.error('Error preparing download bundle.');
      winston.error(error);
    }
  }
);
const unzipPromiseFromFileSystem = (from, to) => {
  return new Promise((resolve, reject) => {
    fs.createReadStream(from)
      .pipe(unzip.Extract({ path: to }))
      .on('finish', function () {
        resolve();
      })
      .on('error', function (e) {
        winston.error('Error unzipping from: ' + from + ' to: ' + to);
        reject(e);
      });
  });
};

router.get(
  '/scoreboard',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    try {
      let scoreboard = await db.any(
        `
            SELECT COALESCE(comp_username, alt_id2) AS competitor_name,
               tiqcs.*,
               correct_score,
               tiq.ordinal,
               student.id AS id_student
            FROM test
                JOIN test_instance ti ON test.id = ti.id_test
                JOIN test_instance_question tiq ON ti.id = tiq.id_test_instance
                JOIN grading_model ON tiq.id_grading_model = grading_model.id
                LEFT JOIN test_instance_question_comp_score tiqcs  ON tiq.id = tiqcs.id_test_instance_question
                JOIN student ON ti.id_student = student.id
            WHERE test.id = (SELECT id_test FROM test_instance WHERE test_instance.id = $(id_test_instance))
            AND ti.ts_submitted IS NULL
            ORDER BY ti.ts_created DESC, tiq.ordinal
        `,
        {
          id_test_instance: req.session.id_test_instance,
        }
      );

      scoreboard = scoreboard.reduce((map, row) => {
        map[row.id_student] = map[row.id_student] || {
          isCurrentUser: req.session.studentId === row.id_student,
          username: row.competitor_name,
          questions: [],
        };

        map[row.id_student].questions.push({
          ordinal: row.ordinal,
          correctScore: row.correct_score,
          compScore: row.comp_score,
          timePassed: row.time_passed && utils.secondsToHHmm(row.time_passed),
        });

        return map;
      }, {});

      res.json(scoreboard);
    } catch (err) {
      winston.error(err);
      res.json({
        success: false,
        error: {
          message: 'An error occured (more info @server).',
        },
      });
    }
  }
);
router.get(
  '/pa/whoiswho',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async (req, res) => {
    const idPa1Test = req.query.id_test ? parseInt(req.query.id_test) : null;
    const idTestScore = req.query.id_test ? parseInt(req.query.id_test_score) : null;
    let headerLabels = [
      'First',
      'Second',
      'Third',
      'Fourth',
      'Fifth',
      'Sixth',
      'Seventh',
      'Eighth',
      'Ninth',
      'Tenth',
    ];
    try {
      const pa1List = await db.many(
        `SELECT id as value, title as name
          FROM test
          WHERE id_test_Type = 30
            AND id_course = $(id_course)
            AND id_academic_year = $(id_academic_year)
            ORDER BY 1`,
        {
          id_course: req.session.currCourseId,
          id_academic_year: req.session.currAcademicYearId,
        }
      );
      const pa2List = await db.many(
        `SELECT id as value, title as name
          FROM test
          WHERE id_test_Type = 32
            AND id_course = $(id_course)
            AND id_academic_year = $(id_academic_year)
            ORDER BY 1`,
        {
          id_course: req.session.currCourseId,
          id_academic_year: req.session.currAcademicYearId,
        }
      );
      let byAssessed, byAssessor, byCalibTest;
      if (idPa1Test) {
        const assNo = await db.one(
          `SELECT assessments_no - calib_assessments_no as ano FROM peer_test WHERE peer_test.id_test_phase_1 = $(idPa1Test)`,
          {
            idPa1Test,
          }
        );
        headerLabels = headerLabels.slice(0, assNo.ano);
        const labels = headerLabels.map((x) => `${x} varchar`).join(', ');
        byAssessed = await db.any(
          `
            SELECT * from CROSSTAB (
            $$
              SELECT last_name2,
                     round(100 * ti_pa1.score_perc, 2) || '%' as awarded,
                     row_number() over (partition by id_student2 order by id_student1) as rn,
                     last_name1 || '#' || COALESCE(test_instance.id::varchar, 'Missing!')
                     || '#' || COALESCE(test_instance.score_perc::varchar, 'Missing!')
                FROM v_peer_shuffle
                LEFT JOIN test_instance
                       ON test_instance.id_test = id_pa_test
                      AND test_instance.id_student = id_Student1
                LEFT JOIN test_instance ti_pa1
                       ON ti_pa1.id = v_peer_shuffle.id_test_instance2
               WHERE id_test_phase_1 = $(idPa1Test)
               ORDER by last_name2, id_student2, id_student1
            $$
            ,
            $$
              SELECT * FROM generate_series(1, $(assNumber))
            $$
            )
            AS peer_crosstab (assessed varchar, awarded varchar, ${labels})`,
          {
            assNumber: assNo.ano,
            idPa1Test,
          }
        );
        byAssessed = byAssessed.map((x) => {
          let obj = {
            assessed: x.assessed,
            awarded: x.awarded,
          };
          for (const [key, value] of Object.entries(x)) {
            if (key !== 'assessed' && key !== 'awarded') {
              let values = value.split('#');
              obj[key] =
                values[0] +
                (values[1].toLocaleLowerCase() === 'missing!'
                  ? ' <span class="bg-danger">!!! MISSING !!!</span>'
                  : ` (<a href="https://edgar.fer.hr/exam/review/${values[1]}" target="_blank">${values[1]}</a>)`);
            }
          }
          return obj;
        });
        if (idTestScore) {
          byCalibTest = await db.any(
            `SELECT
                -- ti_pa1.id,
                ti_pa1.id_student,
                ti_pa2.id as id_test_instance_calib
            FROM test_instance ti_pa1
                JOIN peer_Test
                ON ti_pa1.id_test = peer_Test.id_test_phase_1
            JOIN test
                ON id_test_phase_2 = test.id_parent
            JOIN test master
                ON test.id_parent = master.id
            JOIN test_instance_question
                ON test_instance_question.id_test_instance = ti_pa1.id
                and test_instance_question.ordinal = 1
            JOIN peer_shuffle
                ON peer_shuffle.id_test_instance_question = test_instance_question.id
            JOIN question calib_question
                ON calib_question.id = -1 * jobs[test.test_ordinal - master.test_ordinal]
            LEFT JOIN test_instance ti_pa2
                ON test.id = ti_pa2.id_test
                AND ti_pa1.id_student = ti_pa2.id_student
            WHERE ti_pa1.id_test = $(idPa1Test)
            -- ORDER BY student.last_name, student.first_name`,
            {
              idPa1Test,
            }
          );
          const calibTestDict = {};
          for (let row of byCalibTest) {
            calibTestDict[row.id_student] = row.id_test_instance_calib
              ? `<a href="https://edgar.fer.hr/exam/review/${row.id_test_instance_calib}" target="_blank">${row.id_test_instance_calib}</a>`
              : ' <span class="bg-danger">!!! MISSING !!!</span>';
          }
          byAssessor = await db.any(
            `
                      SELECT * from CROSSTAB (
                      $$
                          SELECT
                                id_student1,
                                last_name1,
                                round(100 * test_instance.score_perc, 2) || '%' as awarded,
                                row_number() over (partition by id_student1 order by id_student2) as rn,
                                last_name2 || '#' || COALESCE(test_instance.id::varchar, 'Missing!')
                            FROM v_peer_shuffle
                            LEFT JOIN test_instance
                                  ON test_instance.id_test = $(idTestScore)
                                  AND test_instance.id_student = id_Student1
                          WHERE id_test_phase_1 =  $(idPa1Test)
                          ORDER by last_name1, id_student1, id_student2
                      $$
                      ,
                      $$
                        SELECT * FROM generate_series(1, $(assNumber))
                      $$
                      )
                      AS peer_crosstab (id_student1 int, assessor varchar, awarded varchar, ${labels})`,
            {
              assNumber: assNo.ano,
              idPa1Test,
              idTestScore,
            }
          );
          byAssessor = byAssessor.map((x) => {
            let obj = {
              assessor: x.assessor,
              awarded: x.awarded,
              calib: calibTestDict[x.id_student1],
            };
            for (const [key, value] of Object.entries(x)) {
              if (key !== 'assessor' && key !== 'awarded' && key !== 'id_student1') {
                let values = value.split('#');
                obj[key] =
                  values[0] +
                  (values[1].toLocaleLowerCase() === 'missing!'
                    ? ' <span class="bg-danger">!!! MISSING !!!</span>'
                    : ` (<a href="https://edgar.fer.hr/exam/review/${values[1]}" target="_blank">${values[1]}</a>)`);
              }
            }
            return obj;
          });
        }
      }
      headerLabels = headerLabels.map((x) => {
        let obj = {};
        obj[x.toLocaleLowerCase()] = x;
        obj.__raw = true;
        return obj;
      });

      res.appRender('paWhoIsWho', {
        pageTitle: 'Who is who in PA',
        id_test: idPa1Test,
        pa1List: pa1List,
        pa2List: pa2List,
        byAssessed: byAssessed
          ? {
              headers: [
                {
                  assessed: 'Assessed student',
                },
                {
                  awarded: 'Awarded',
                },
                ...headerLabels,
              ],
              rows: byAssessed,
            }
          : undefined,
        byAssessor: byAssessor
          ? {
              headers: [
                {
                  assessor: 'Assessor',
                },
                {
                  awarded: 'Awarded',
                },
                {
                  calib: 'Calib test submitted',
                  __raw: true,
                },
                ...headerLabels,
              ],
              rows: byAssessor,
            }
          : undefined,
        // byCalibTest: byCalibTest
        //   ? {
        //       headers: [
        //         {
        //           assessor: 'Assessor',
        //         },
        //         {
        //           awarded: 'Awarded',
        //         },
        //         ...headerLabels,
        //       ],
        //       rows: byAssessor,
        //     }
        //   : undefined,
      });
    } catch (err) {
      winston.error(err);
      res.json({
        success: false,
        error: {
          message: 'An error occured (more info @server).',
        },
      });
    }
  }
);

// *********************

module.exports = router;
