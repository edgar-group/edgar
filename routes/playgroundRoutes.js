'use strict';

var express = require('express');
var router = express.Router();
var middleware = require('../common/middleware');
var globals = require('../common/globals');
var winston = require('winston');
var testService = require.main.require('./services/testService');
var db = require('../db').db;
var errors = require('../common/errors');
var utils = require.main.require('./common/utils');
var moment = require('moment');
const CodeRunnerService = require('../lib/CodeRunnerServiceCommonJS');

router.get(
  '/sandbox',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    var condition = req.session.rolename === globals.ROLES.TEACHER ? '' : ' AND student_can_see ';
    db.any(
      ` SELECT code_runner.id, student_can_see, name, ordinal
               FROM code_runner join playground_code_runner
                 ON code_runner.id = playground_code_runner.id_code_runner
              WHERE id_course = $(id_course)
                  ${condition}
              ORDER BY ordinal`,
      {
        id_course: req.session.currCourseId,
      }
    )
      .then((data) => {
        res.appRender(
          'sandboxView',
          { snippet: null, runners: data },
          'sandboxViewScript',
          '~/shared/cmSkinViewCSS'
        );
      })
      .catch((error) => {
        return next(
          new errors.DbError(
            'An error occured while fetching code runner for this course: ' + error.message
          )
        );
      });
  }
);

router.get(
  '/sandbox/snippet/:id_code_snippet([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    var condition = req.session.rolename === globals.ROLES.TEACHER ? '' : ' AND student_can_see ';
    db.any(
      ` SELECT student_can_see, name, ordinal, code_snippet.title, code_snippet.code,
                      code_snippet.id, code_snippet.is_public, first_name, last_name , to_char(code_snippet.ts_modified, 'YYYY-MM-DD HH24:MI:SS') as ts_modified,
                      id_student
               FROM code_runner join playground_code_runner
                 ON code_runner.id = playground_code_runner.id_code_runner
               JOIN code_snippet
                 ON code_runner.id = code_snippet.id_code_runner
                AND code_snippet.id_course = playground_code_runner.id_course
               JOIN student
                 ON code_snippet.id_student = student.id
              WHERE playground_code_runner.id_course = $(id_course)
                AND (id_student = $(id_student) OR is_public)
                AND code_snippet.id =$(id_code_snippet)
                  ${condition}
              ORDER BY ordinal`,
      {
        id_course: req.session.currCourseId,
        id_student: req.session.studentId,
        id_code_snippet: req.params.id_code_snippet,
      }
    )
      .then((data) => {
        // Someone might come here by a public link and if it is not his/hers snippet, then load it into the sandbox
        if (data[0] && data[0].id_student && data[0].id_student === req.session.studentId) {
          res.appRender(
            'sandboxView',
            { snippet: data[0], runners: data },
            'sandboxViewScript',
            '~/shared/cmSkinViewCSS'
          );
        } else if (data[0]) {
          res.appRender(
            'sandboxView',
            { snippet: null, publicCode: data[0].code, runners: data },
            'sandboxViewScript',
            '~/shared/cmSkinViewCSS'
          );
        } else {
          res.status(403).send('Forbidden, you do not have permissions to access this resource!');
        }
      })
      .catch((error) => {
        return next(
          new errors.DbError(
            'An error occured while fetching code runner for this course: ' + error.message
          )
        );
      });
  }
);
router.post(
  '/sandbox/snippet/:id_code_snippet([0-9]{1,10})/delete',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.result(
      `
               DELETE FROM code_snippet
                WHERE id = $1
                  AND id_student = $2
            `,
      [req.params.id_code_snippet, req.session.studentId]
    )
      .then((result) => {
        if (result.rowCount === 1) {
          req.flash('info', 'Snippet deleted, oh well...');
          res.redirect('/playground/sandbox');
        } else {
          req.flash('info', result.rowCount + ' rows deleted !?');
          res.redirect('/playground/sandbox/snippet/' + req.params.id_code_snippet);
        }
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);

router.post(
  '/sandbox/snippet/:id_code_snippet([0-9]{1,10})/togglepublish',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.one(
      `
               UPDATE code_snippet
                  SET is_public = NOT is_public
                WHERE id = $1
                  AND id_student = $2
                RETURNING is_public
            `,
      [req.params.id_code_snippet, req.session.studentId]
    )
      .then((result) => {
        if (result.is_public) {
          req.flash(
            'info',
            'Snippet published! It is now visible in the snippet store. Well done!'
          );
        } else {
          req.flash('info', 'Snippet unpublished, it is no more visible in the snippet store.');
        }
        res.redirect('/playground/sandbox/snippet/' + req.params.id_code_snippet);
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);

router.get(
  '/sandbox/snippet/code/:id_code_snippet([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    var condition = req.session.rolename === globals.ROLES.TEACHER ? '' : ' AND student_can_see ';
    db.oneOrNone(
      ` SELECT code
               FROM code_runner join playground_code_runner
                 ON code_runner.id = playground_code_runner.id_code_runner
               JOIN code_snippet
                 ON code_runner.id = code_snippet.id_code_runner
                AND code_snippet.id_course = playground_code_runner.id_course
               JOIN student
                 ON code_snippet.id_student = student.id
              WHERE playground_code_runner.id_course = $(id_course)
                AND id_student = $(id_student)
                AND code_snippet.id =$(id_code_snippet)
                  ${condition}
              ORDER BY ordinal`,
      {
        id_course: req.session.currCourseId,
        id_student: req.session.studentId,
        id_code_snippet: req.params.id_code_snippet,
      }
    )
      .then((data) => {
        // console.log(data);
        res.json({
          success: true,
          code: data.code,
        });
      })
      .catch((error) => {
        winston.error('Error executing code' + req.body.code + ') ' + error);
        res.json({
          success: false,
          error: {
            message: moment().format('LTS') + ': An error occured: ' + error + '.',
          },
        });
      });
  }
);

router.post(
  '/snippet/exec/:id_code_runner_ordinal([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    winston.info(`executing custom code (Code playground):\n ${req.body}\n`);
    var condition = req.session.rolename === globals.ROLES.TEACHER ? '' : ' AND student_can_see ';
    db.oneOrNone(
      `SELECT id_code_runner
               FROM playground_code_runner
              WHERE id_course = $(id_course)
                AND ordinal = $(ordinal)
                  ${condition}
              `,
      {
        id_course: req.session.currCourseId,
        ordinal: req.params.id_code_runner_ordinal,
      }
    )
      .then(function (course) {
        if (course.id_code_runner) {
          let code = `-- ${moment().format('YYYY-MM-DD, kk:mm:ss')} ${
            req.session.passport.user
          } from PLAYGROUND\n\n${req.body.code}`;
          return CodeRunnerService.execSQLCodeCodeRunner(course.id_code_runner, code);
        } else {
          res.status(404).send('Course does not have playground runner defined.');
        }
      })
      .then(function (result) {
        res.json(result);
      })
      .catch(function (error) {
        winston.error('Error executing code' + req.body.code + ') ' + error);
        res.json({
          success: false,
          error: {
            message: moment().format('LTS') + ': An error occured: ' + error + '.',
          },
        });
      });
    //var IdCodeRunner = parseInt(req.params.id_code_runner);
  }
);
router.post(
  '/snippet/saveas',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  async function (req, res) {
    try {
      var condition = req.session.rolename === globals.ROLES.TEACHER ? '' : ' AND student_can_see ';
      const cr = await db.oneOrNone(
        `SELECT id_code_runner
                                            FROM playground_code_runner
                                            WHERE id_course = $(id_course)
                                            AND ordinal = $(ordinal)
                                                ${condition}
                                            `,
        {
          id_course: req.session.currCourseId,
          ordinal: req.body.id_code_runner_ordinal,
        }
      );
      let data = await db.result(
        'INSERT INTO code_snippet (title, code, id_code_runner, id_student, id_course) VALUES ($1, $2, $3, $4, $5) RETURNING id',
        [
          req.body.title,
          req.body.code,
          cr.id_code_runner,
          req.session.studentId,
          req.session.currCourseId,
        ]
      );
      res.json({
        success: true,
        rowsAffected: data.rowCount,
        snippetId: data.rows[0].id,
      });
    } catch (error) {
      winston.error(error);
      res.json({
        success: false,
        error: {
          message: moment().format('LTS') + ': An error occured: ' + error + '.',
        },
      });
    }
  }
);
router.post(
  '/snippet/save/:snippetId([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.result(
      `UPDATE code_snippet
                    SET code = $1
                  WHERE id = $2
                    AND id_student = $3`,
      [req.body.code, req.params.snippetId, req.session.studentId]
    )
      .then((result) => {
        res.json({
          success: true,
          rowsAffected: result.rowCount,
          snippetId: req.params.snippetId,
        });
      })
      .catch((error) => {
        res.json({
          success: false,
          error: {
            message: moment().format('LTS') + ': An error occured: ' + error + '.',
          },
        });
      });
  }
);

router.post(
  '/snippet/fork/:snippetId([0-9]{1,10})',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.result(
      `INSERT INTO code_snippet (title, code, id_code_runner, id_student, id_course)
                    SELECT title, code, id_code_runner, $1, id_course FROM code_snippet
                     WHERE is_public
                       AND id = $2
                     RETURNING id`,
      [req.session.studentId, req.params.snippetId]
    )
      .then((result) => {
        if (result.rowCount === 1) {
          req.flash('info', 'Snippet forked, keep calm and code on...');
          res.redirect('/playground/sandbox/snippet/' + result.rows[0].id);
        } else {
          req.flash(
            'info',
            'Failed, have you tried to fork a custom id (logged) !? Rowcount:' + result.rowCount
          );
          res.redirect('/playground/snippet/store');
        }
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);

router.get(
  '/snippet/all',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.any(
      ` SELECT code_snippet.id, title, to_char(code_snippet.ts_modified, 'YYYY-MM-DD HH24:MI:SS') as ts_modified
               FROM code_snippet
              WHERE id_course = $(id_course)
                AND id_student = $(id_student)
              ORDER BY title, code_snippet.ts_modified DESC`,
      {
        id_course: req.session.currCourseId,
        id_student: req.session.studentId,
      }
    )
      .then((data) => {
        res.json({
          success: true,
          data: data,
        });
      })
      .catch((error) => {
        res.json({
          success: false,
          error: {
            message: moment().format('LTS') + ': An error occured: ' + error + '.',
          },
        });
      });
  }
);

router.get(
  '/snippet/store',
  middleware.requireRoles([globals.ROLES.TEACHER, globals.ROLES.STUDENT]),
  function (req, res, next) {
    db.any(
      ` SELECT code_snippet.id, title, to_char(code_snippet.ts_modified, 'YYYY-MM-DD HH24:MI:SS') as ts_modified
                    , student.first_name, student.last_name, student.alt_id2, student.email, class_group
               FROM code_snippet
               JOIN student
                 ON code_snippet.id_student = student.id
               JOIN student watcher
                 ON watcher.id = $(id_student)
               JOIN student_course
                 ON student_course.id_student = student.id
                AND student_course.id_course = code_snippet.id_course
                AND id_academic_year = $(id_academic_year)
               JOIN playground_code_runner
                 ON playground_code_runner.id_course = code_snippet.id_course
                AND playground_code_runner.id_code_runner = code_snippet.id_code_runner
                AND (student_can_see OR watcher.id_app_user IS NOT NULL)
              WHERE code_snippet.id_course = $(id_course)
                AND is_public
              ORDER BY student.last_name, student.id, title, code_snippet.ts_modified DESC`,
      {
        id_course: req.session.currCourseId,
        id_academic_year: req.session.currAcademicYearId,
        id_student: req.session.studentId,
      }
    )
      .then((data) => {
        data.forEach(function (row) {
          row.img = utils.getTinyImageHtml(row.alt_id2);
        });
        res.appRender('snippetStoreView', {
          snippets: {
            headers: [
              {
                img: 'Image',
                __raw: true,
              },
              {
                first_name: 'First name',
              },
              {
                last_name: 'Last name',
              },
              {
                email: 'Email',
              },
              {
                class_group: 'Group',
              },
              {
                id: 'Id',
              },
              {
                title: 'Snippet title',
              },
              {
                ts_modified: 'ts_modified',
              },
            ],
            rows: data,
            buttons: [
              {
                label: 'Fork',
                method: 'POST',
                action: function (row) {
                  return '/playground/snippet/fork/' + row['id'];
                },
              },
            ],
            links: [
              {
                label: 'Open in my sandbox',
                target: '_blank',
                href: function (row) {
                  return '/playground/sandbox/snippet/' + row.id;
                },
              },
            ],
          },
        });
      })
      .catch((error) => {
        winston.error(error);
        return next(error);
      });
  }
);
module.exports = router;
