#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username postgres --dbname edgar <<-EOSQL
    CREATE ROLE edgar;
EOSQL

pg_restore -U postgres --dbname=edgar --verbose /docker-entrypoint-initdb.d/edgar.dmp
