import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  Output,
  OnChanges,
} from '@angular/core';
import { Tutorial } from '../../models/Tutorial';
import { TutorialStep } from '../../models/TutorialStep';
import { TutorialService } from '../../tutorial.service';
import { TutorialStepHint } from '../../models/TutorialHint';
import { Question } from '../../../../common/models/question';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { environment } from 'projects/config/environments/environment';

@Component({
  selector: 'tutorial-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  // OnChanges
  _tutorial: Tutorial;
  _currStep!: TutorialStep;
  _currStepIsNull: boolean;
  _currStepHint: TutorialStepHint;

  @Input() nullWarning: boolean = false;
  @Input() correctStatus!: boolean;
  // @Output() submitCalled = new EventEmitter<boolean>();
  @Output() finished = new EventEmitter<boolean>();
  @Output() stepChanged = new EventEmitter<boolean>();
  @Output() stepNullChange = new EventEmitter<boolean>();
  @Input() latestStepOrdinal: number = 1;
  @Input() stepCorrectStatuses: Array<boolean> = [];
  constructor(
    protected sanitizer: DomSanitizer,
    private _tutorialService: TutorialService,
    private _router: Router
  ) {
    this._tutorial = this._tutorialService.getTutorial();
    this._currStepHint = new TutorialStepHint(-1, 'This step has no hints :('); // might be overriden later
    this._currStepIsNull = false;
  }

  public async ngOnInit() {
    this._tutorialService.getCurrentStepObservable().subscribe((s) => {
      this._currStep = s;
    });
    this._currStep = this._tutorialService.getCurrentStep();
  }
  getStepDropdownCssClass(ordinal) {
    return (
      'badge badge-pill m-1 ' +
      (this.stepCorrectStatuses[ordinal - 1]
        ? 'badge-success'
        : 'badge-primary')
    );
  }
  // ngOnChanges() {
  //   this.stepCorrectStatuses = [];
  //   for (let i = 0; i < this._tutorial.steps.length; ++i) {
  //     this.stepCorrectStatuses.push(
  //       this._tutorialService.getStepCorrectStatus(i + 1)
  //     );
  //   }
  // }
  getDevUrlStub() {
    return environment.production ? '' : environment.devWebApiUrl ?? '';
  }
  private hintOrdinal: number = 0;
  showNextStepHint() {
    console.log('this._currStepHint.', this._currStepHint);
    if (this._currStep.hints.length > 0) {
      this._currStepHint = this._currStep.hints[this.hintOrdinal];
      this.hintOrdinal = (this.hintOrdinal + 1) % this._currStep.hints.length;
    }
    console.log('this._currStepHint.', this._currStepHint);
  }

  closeHint() {
    this._currStepHint = new TutorialStepHint(-1, 'This step has no hints :(');
  }

  async goToStep(ordinal: number) {
    await this._tutorialService.setCurrentStepOrdinal(ordinal);
    this._tutorialService.getCurrentStepObservable().subscribe((s) => {
      this._currStep = s;
    });
    this._currStep = this._tutorialService.getCurrentStep();
    if (this._currStep) {
      this.stepNullChange.emit(false);
      this.stepChanged.emit(true); //novi korak
      this._currStepIsNull = false;
    } else {
      this.stepNullChange.emit(true);
      this._currStepIsNull = true;
    }
    this._router.navigate([
      '/tutorial/' + this._tutorial.id + '/step/' + this._currStep.ordinal,
    ]);
  }

  previousStep() {
    this.goToStep(this._currStep.ordinal - 1);
  }

  nextStep() {
    // if all steps finished, congratulate
    if (this._currStep.ordinal === this._tutorial.steps.length) {
      this._currStep = {} as any;
      this.stepNullChange.emit(true);
      this._currStepIsNull = true;
      this.finished.emit(true);
      // else if the next step is unlocked, go to next step
    } else if (this.correctStatus || this._tutorial.allowRandomAccess) {
      this.goToStep(this._currStep.ordinal + 1);
      let urlId = this._currStep.ordinal + 1;
      //this._router.navigate(['/tutorial/' + urlId]);
    }
  }
}
