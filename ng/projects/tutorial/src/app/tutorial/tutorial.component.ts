import {
  AfterViewChecked,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { Tutorial } from '../../models/Tutorial';
import { TutorialStep } from '../../models/TutorialStep';
import { TutorialService } from '../../tutorial.service';
import {
  TutorialAnswerHint,
  TutorialStepHint,
} from '../../models/TutorialHint';
import { TutorialAnswer } from '../../models/TutorialAnswer';
import { Question } from '../../../../common/models/question';
import { DomSanitizer } from '@angular/platform-browser';
import {
  CODERUNNER_SERVICE_CONFIG,
  TUTORIAL_CODERUNNER_SERVICE_CONFIG,
} from '../../../../config/code-runner.config';
import {
  DEFAULT_TUTORIAL_SERVICE_CONFIG,
  TUTORIAL_SERVICE_CONFIG,
} from '../../tutorial.service.config';
import { ActivatedRoute, Params, Router } from '@angular/router';

import {
  TutorialContent,
  TutorialContentType,
} from '../../models/TutorialContent';
import { LogEvent } from 'projects/common/models/log-event';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css'],
  providers: [
    // LogService,
    // { provide: LOG_SERVICE_CONFIG, useValue: TUTORIAL_LOG_SERVICE_CONFIG },
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: TUTORIAL_CODERUNNER_SERVICE_CONFIG,
    },
    {
      provide: TUTORIAL_SERVICE_CONFIG,
      useValue: DEFAULT_TUTORIAL_SERVICE_CONFIG,
    },
  ],
})
export class TutorialComponent implements OnInit {
  _tutorial: Tutorial;
  _currStep!: TutorialStep;
  _currStepIsNull: boolean = false;
  _currAnswers!: TutorialAnswer[];

  _currStepHint!: TutorialStepHint;
  _currAnswerHint!: TutorialAnswerHint;

  // _correctStatus!: boolean[];
  _allQuestionsCorrect: boolean = false;

  _dummyQuestion!: Question;
  currQuestionOrdinal: number = 1;

  _finished: boolean = false;
  _tutorialLoaded: boolean = false;
  _headOrBottomNull: boolean = false;
  stepCorrectStatuses: Array<boolean> = [];
  hasCM: boolean = false;
  tutorialContent: TutorialContent[] = [];
  constructor(
    protected sanitizer: DomSanitizer,
    private _tutorialService: TutorialService,
    private _logService: LogService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this._tutorial = this._tutorialService.getTutorial();
    this._currAnswers = [];
    this.refreshStepCorrectStatuses();
  }

  public async ngOnInit() {
    this._tutorialService.getCurrentStepObservable().subscribe((s) => {
      this._currStep = s;
    });

    this._currStep = this._tutorialService.getCurrentStep();

    this.tutorialContent = this._tutorialService.getTutorialContent(
      this._currStep.ordinal
    );
    this._allQuestionsCorrect = this._tutorialService.getStepCorrectStatus(
      this._currStep.ordinal
    );

    this._route.params.subscribe(async (params: Params) => {
      let ordinal = +params['stepid'];
      if (ordinal > 0) {
        this._tutorialLoaded = true;
        this._tutorialService.setCurrentStepOrdinal(ordinal);
      }
    });
    this._route.fragment.subscribe((fragment) => {
      if (fragment) {
        let element = document.getElementById(fragment);
        if (element) {
          element.scrollIntoView({ block: 'center' });
        }
      }
    });
  }
  getBookmarks() {
    return this.tutorialContent.filter((x) => x.isCodeRunner() || x.isAbc());
  }
  getCodeRunnerCssClass(cr: TutorialContent): string {
    if (cr.type === TutorialContentType.CodePlayground) {
      return 'bg-info';
    } else if (
      cr.type === TutorialContentType.CodeQuestion ||
      cr.type === TutorialContentType.AbcQuestion
    ) {
      return this._tutorialService.getQuestionCorrectStatus(
        this._currStep.ordinal,
        cr.questionOrdinal || 0
      )
        ? 'bg-success'
        : 'bg-warning';
    } else {
      return 'bg-danger';
    }
  }

  gotoHashtag(ordinal: string) {
    this._router.navigate(
      [`/tutorial/${this._tutorial.id}/step/${this._currStep.ordinal}`],
      {
        fragment: 'q-' + ordinal,
      }
    );
  }
  refreshStepCorrectStatuses() {
    let statuses: Array<boolean> = [];
    for (let i = 0; i < this._tutorial.steps.length; ++i) {
      statuses.push(this._tutorialService.getStepCorrectStatus(i + 1));
    }
    this.stepCorrectStatuses = statuses;
  }

  onAnswerUpdated($event) {
    // this._tutorialService.updateAnswers(
    //   this._currStep.ordinal,
    //   $event.ordinal,
    //   $event.answer
    // );
  }
  onAnswerSaved($event) {
    this.refreshStepCorrectStatuses();
    this._tutorialService
      .updateAnswers
      // this._currStep.ordinal,
      // $event.ordinal,
      // $event.answer
      ();
  }

  onAnswerEvaluated($event) {
    // console.log('cr.correctStatus', $event);
    this.refreshStepCorrectStatuses();
    this._tutorialService.updateAnswers();
    this._allQuestionsCorrect = this._tutorialService.getStepCorrectStatus(
      this._currStep.ordinal
    );
  }

  showAppropriateHints() {}

  finishedInfo(data) {
    this._finished = data;
  }

  currStepIsNull(data) {
    this._currStepIsNull = data;
    if (data) {
      this._currStep = {} as any;
    }

    if (data) {
      this._headOrBottomNull = true;
    } else {
      this._headOrBottomNull = false;
    }
  }

  stepChanged(data) {
    this._logService.log(
      new LogEvent(
        'Submitting answer',
        `Step ordinal: ${this._currStep.ordinal}`
      )
    );
    //first save correct status for that step
    this.tutorialContent = this._tutorialService.getTutorialContent(
      this._currStep.ordinal
    );
    this._allQuestionsCorrect = this._tutorialService.getStepCorrectStatus(
      this._currStep.ordinal
    );
    // this._correctStatus = this._tutorialService.getCorrectStatus(
    //   this._currStep.ordinal
    // );

    window.scroll({ top: 0 });
  }

  getCodeMirrorSkin(): string {
    return this._tutorialService.getCodeMirrorSkin();
  }
}
