import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Answer } from '../../../../common/models/answer';
import { Question } from '../../../../common/models/question';
import {
  TutorialContent,
  TutorialContentType,
} from '../../models/TutorialContent';
import { TutorialService } from '../../tutorial.service';

@Component({
  selector: 'tutorial-abc',
  templateUrl: './tutorial-abc.component.html',
  styleUrls: ['./tutorial-abc.component.css'],
})
export class TutorialAbcComponent implements OnInit {
  @Input() tutorialContent!: TutorialContent;
  // @Input() currAnswer!: Answer;
  @Output() answerUpdated = new EventEmitter();
  @Output() answerEvaluated = new EventEmitter();
  currQuestion!: Question;

  constructor(
    private _tutorialService: TutorialService,
    public sanitizer: DomSanitizer
  ) {}
  getAnswers() {
    return this.currQuestion && this.currQuestion.answers;
  }
  async ngOnInit() {
    if (this.tutorialContent.type === TutorialContentType.AbcQuestion) {
      this.currQuestion = await this._tutorialService.getQuestion(
        this.tutorialContent.stepOrdinal,
        this.tutorialContent.questionOrdinal || 0
      );
    } else {
      console.error('Non multichoice?');
    }
  }

  getQuestionCorrectStatus(): boolean {
    return this._tutorialService.getQuestionCorrectStatus(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
  }

  isSelected(answer: any) {
    let currTutorialAnswer = this._tutorialService.getCurrentTutorialAnswer(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
    return (
      this.currQuestion &&
      currTutorialAnswer.currAnswer &&
      currTutorialAnswer.currAnswer.multichoice.indexOf(answer.ordinal) >= 0
    );
  }

  onSelect(answer: any) {
    if (this.currQuestion) {
      let currTutorialAnswer = this._tutorialService.getCurrentTutorialAnswer(
        this.tutorialContent.stepOrdinal,
        this.tutorialContent.questionOrdinal || 0
      );
      var arrAnswers =
        (currTutorialAnswer.currAnswer &&
          currTutorialAnswer.currAnswer.multichoice) ||
        [];
      if (this.currQuestion.multiCorrect) {
        var idx;
        if ((idx = arrAnswers.indexOf(answer.ordinal)) >= 0) {
          arrAnswers.splice(idx, 1);
        } else {
          arrAnswers.push(answer.ordinal);
        }
      } else {
        if (arrAnswers.indexOf(answer.ordinal) >= 0) {
          arrAnswers = [];
        } else {
          arrAnswers = [answer.ordinal];
        }
      }
      currTutorialAnswer.currAnswer.multichoice = arrAnswers;
      let tmpArr = this.currQuestion.answers.map((x) => {
        return {
          ordinal: x.ordinal,
          isOK: !x.isCorrect,
        };
      });
      for (let a of arrAnswers) {
        if (!tmpArr[a - 1].isOK) {
          tmpArr[a - 1].isOK = true;
        } else {
          tmpArr[a - 1].isOK = false;
        }
      }
      let correct = true;
      for (let a of tmpArr) {
        if (!a.isOK) {
          correct = false;
          break;
        }
      }
      currTutorialAnswer.correctStatus = correct;
      let emitObj = {
        ordinal: this.tutorialContent.questionOrdinal,
        answer: currTutorialAnswer.currAnswer,
      };
      this.answerUpdated.emit(emitObj);
      this.answerEvaluated.emit(emitObj);
    }
  }
}
