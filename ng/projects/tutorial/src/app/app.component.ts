import { Component, OnInit } from '@angular/core';
import { LogService } from '../../../common/services/log.service';
import { TutorialService } from '../tutorial.service';
import { LogEvent } from '../../../common/models/log-event';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
  RoutesRecognized,
} from '@angular/router';
import {
  CODERUNNER_SERVICE_CONFIG,
  TUTORIAL_CODERUNNER_SERVICE_CONFIG,
} from '../../../config/code-runner.config';
import { environment } from 'projects/config/environments/environment';
import { Tutorial } from '../models/Tutorial';

@Component({
  selector: 'app',
  templateUrl: `app.component.html`,
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: TUTORIAL_CODERUNNER_SERVICE_CONFIG,
    },
  ],
})
export class AppComponent implements OnInit {
  _intialQuestionLoaded: boolean = false;
  _tutorialLoaded: boolean = false;
  _tutorial!: Tutorial;

  constructor(
    private _tutorialService: TutorialService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}

  public async ngOnInit() {
    // let routeParams = this.collectRouteParams(this._router);
    this._router.events.subscribe(async (val) => {
      if (val instanceof RoutesRecognized) {
        // console.log("xx", val.state.root.firstChild && val.state.root.firstChild.params);
        if (val.state.root.firstChild) {
          const routeParams = val.state.root.firstChild.params || { stepid: 1 };
          if (!environment.production && location.port == '4200') {
            console.log('Dev mode -> dummy login to last started tutorial!');
            await this._tutorialService.devlogin();
            this._tutorial = await this._tutorialService.loadTutorial(6); // HARD-CODED FOR DEV
          } else {
            this._tutorial = await this._tutorialService.loadTutorial(
              routeParams['tutid']
            );
          }

          if (this._tutorial == null) {
            alert('tutorial is null!?');
          } else {
            this._tutorialLoaded = true;
            this._tutorial.latestStepOrdinal =
              routeParams['stepid'] || this._tutorial.latestStepOrdinal || 1;

            await this._tutorialService.setCurrentStepOrdinal(
              this._tutorial.latestStepOrdinal
            );

            this._router.navigate(
              [
                `/tutorial/${this._tutorial.id}/step/${this._tutorial.latestStepOrdinal}`,
              ],
              {
                relativeTo: this._route,
              }
            );

            this._intialQuestionLoaded = true;
          }
        }
      }
    });
  }
}
