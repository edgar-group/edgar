import { TutorialStepHint } from './TutorialHint';

export class TutorialStep {
  constructor(
    public id: number,
    public ordinal: number,
    public title: string,
    public html: string,
    public hints: TutorialStepHint[]
  ) {}
}
