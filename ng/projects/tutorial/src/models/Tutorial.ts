import { Answer } from '../../../common/models/answer';
import { TutorialAnswer } from './TutorialAnswer';

export class Tutorial {
  constructor(
    public id: number,
    public title: string,
    public steps: [], // TODO: strongly type to Step
    public currAnswers: TutorialAnswer[][],
    public allowRandomAccess: boolean,
    public cmSkin: string,
    public latestStepOrdinal: number
  ) {
    let ca: TutorialAnswer[][] = [];
    for (let i = 0; i < this.steps.length; ++i) {
      ca[i] = [];
      if (this.currAnswers && this.currAnswers[i]) {
        ca[i] = this.currAnswers[i];
      } else {
        //ca.push(new Answer());
      }
    }
    this.currAnswers = ca;

    // if (!environment.production) {
    //   this.imgSrc = environment.devWebApiUrl + this.imgSrc;
    // }
  }
}
