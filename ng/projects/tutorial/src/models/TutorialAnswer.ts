import { Answer } from 'projects/common/models/answer';

export class TutorialAnswer {
  constructor(
    public questionOrdinal: number,
    public currAnswer: Answer,
    public correctStatus: boolean,
    public required: boolean
  ) {}
}
