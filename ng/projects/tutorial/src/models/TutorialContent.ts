import { Answer } from '../../../common/models/answer';

//html --> tekst prije code runnera
//question --> pitanje koje se obrađuje
//answer --> odgovor na pitanje
//title --> naslov gumba

export class TutorialContent {
  constructor(
    public stepOrdinal: number,
    public html: string,
    public ordinal: number,
    public questionOrdinal: number | null,
    // public currAnswer: Answer | null,
    public type: TutorialContentType,
    // public required: boolean = false,
    public langids: number[] // public correctStatus: boolean
  ) {}

  isCodeRunner(): boolean {
    return (
      this.type === TutorialContentType.CodePlayground ||
      this.type === TutorialContentType.CodeQuestion
    );
  }
  isAbc(): boolean {
    return this.type === TutorialContentType.AbcQuestion;
  }
  // mustBeCorrect() {
  //   return (
  //     (this.type === TutorialContentType.AbcQuestion ||
  //       this.type === TutorialContentType.CodeQuestion) &&
  //     this.required
  //   );
  // }
}

export enum TutorialContentType {
  CodeQuestion,
  CodePlayground,
  HTML,
  AbcQuestion,
  TBD,
}
