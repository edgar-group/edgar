import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const TUTORIAL_SERVICE_CONFIG =
  new InjectionToken<TutorialServiceConfig>('tutorial.service.config');

export interface TutorialServiceConfig {
  //startTutorialUrl: string;  // izbacio jer sam spojio ove dvije rute na serveru
  getTutorialUrl: string;
  getQuestionUrl: string;
  getCodeRunnerUrl: string;
  // getExamReviewUrl: string;
  // getPeerAssesmentQuestionUrl: string;
  // getAllPeerAssesmentQuestionsUrl: string;
}

export const DEFAULT_TUTORIAL_SERVICE_CONFIG: TutorialServiceConfig = {
  getTutorialUrl: `${environment.devWebApiUrl ?? ''}/tutorial/api`,
  getQuestionUrl: `${environment.devWebApiUrl ?? ''}/tutorial/question`,
  getCodeRunnerUrl: `${environment.devWebApiUrl ?? ''}/tutorial/coderunner`,
  // getExamReviewUrl: `${environment.devWebApiUrl ?? ''}/exam/review/instance`,
  // getPeerAssesmentQuestionUrl: `${environment.devWebApiUrl ?? ''}/exam/review/peerquestion`,
  // getAllPeerAssesmentQuestionsUrl: `${environment.devWebApiUrl ?? ''}/exam/review/peerquestions`,
};
