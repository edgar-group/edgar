import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'projects/common/components/page-not-found-component/page-not-found.component';
import { TutorialComponent } from './app/tutorial/tutorial.component';

const routes: Routes = [
  // { path: 'tutorial/:tutid', component: TutorialComponent },
  // { path: 'tutorial/:tutid/step/:stepid', component: TutorialComponent },
  { path: 'tutorial/:tutid', component: TutorialComponent },
  { path: 'tutorial/:tutid/step/:stepid', component: TutorialComponent },
  // { path: '', component: AppComponent, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }, // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
