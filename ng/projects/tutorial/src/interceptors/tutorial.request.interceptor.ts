// import {
//   HttpInterceptor,
//   HttpRequest,
//   HttpHandler,
//   HttpEvent,
// } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { TutorialService } from 'projects/tutorial/src/tutorial.service';
// import {
//   Observable,
// } from 'rxjs';

// @Injectable()
// export class TutorialRequestInterceptor implements HttpInterceptor {
//   constructor(
//     private _tutorialService: TutorialService
//   ) {}

//   intercept(
//     request: HttpRequest<any>,
//     next: HttpHandler
//   ): Observable<HttpEvent<any>> {
//     console.log(
//       'intercepting request',
//       this._tutorialService.tutorialLoaded(),
//       this._tutorialService.tutorialLoaded() &&
//         this._tutorialService.getTutorial()
//     );
//     if (this._tutorialService.tutorialLoaded()) {
//       const clonedReq = request.clone({
//         params: request.params.set(
//           'idtut',
//           this._tutorialService.getTutorial().id
//         ),
//       });
//       return next.handle(clonedReq);
//     } else {
//       return next.handle(request);
//     }
//   }
// }
