import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { CodeRunnerQuestion } from 'projects/common/models/code-runner-question';
import { Question } from 'projects/common/models/question';
import { TutorialService } from '../tutorial.service';
import {
  TutorialContent,
  TutorialContentType,
} from '../models/TutorialContent';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'code-runner',
  templateUrl: './code-runner.component.html',
  styleUrls: ['./code-runner.component.css'],
})
export class CodeRunnerComponent implements OnInit, OnChanges {
  uuid?: string;
  question?: Question;
  crQuestion?: CodeRunnerQuestion;

  @Input() tutorialContent!: TutorialContent;

  @Output() codeRun = new EventEmitter();
  @Output() answerChanged = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  cmSkin!: string;
  timesRun: number = 0;

  constructor(
    private _tutorialService: TutorialService,
    protected sanitizer: DomSanitizer
  ) {
    this.question = undefined;
    this.crQuestion = undefined;
    this.uuid = undefined;
  }

  async ngOnInit() {
    this.generateUUID();

    this.cmSkin = this.getCodeMirrorSkin();

    if (this.tutorialContent.questionOrdinal) {
      if (this.tutorialContent.type === TutorialContentType.CodeQuestion) {
        this.question = await this._tutorialService.getQuestion(
          this.tutorialContent.stepOrdinal,
          this.tutorialContent.questionOrdinal
        );
      } else if (
        this.tutorialContent.type === TutorialContentType.CodePlayground
      ) {
        this.crQuestion = await this._tutorialService.getCodeRunner(
          this.tutorialContent.stepOrdinal,
          this.tutorialContent.questionOrdinal
        );
      }
    }
  }

  getQuestionCorrectStatus(): boolean {
    return this._tutorialService.getQuestionCorrectStatus(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
  }
  generateUUID() {
    this.uuid = `${this.tutorialContent.questionOrdinal}-${this.tutorialContent.type}`;
  }

  getCurrentAnswer() {
    let ta = this._tutorialService.getCurrentTutorialAnswer(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
    // console.log(
    //   'this._tutorialService.getCurrentTutorialAnswer',
    //   this.tutorialContent.stepOrdinal,
    //   this.tutorialContent.questionOrdinal || 0,
    //   this.tutorialContent,
    //   ta
    // );
    return ta.currAnswer;
  }
  onAnswerUpdated(answer) {
    let ta = this._tutorialService.getCurrentTutorialAnswer(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
    ta.currAnswer = answer;
    this.answerChanged.emit({
      answer: ta.currAnswer,
      ordinal: this.tutorialContent.ordinal,
    });
  }
  onAnswerSaved() {
    let ta = this._tutorialService.getCurrentTutorialAnswer(
      this.tutorialContent.stepOrdinal,
      this.tutorialContent.questionOrdinal || 0
    );
    this.answerSaved.emit({
      answer: ta.currAnswer,
      ordinal: this.tutorialContent.ordinal,
    });
    // this._logService.log(
    //   new LogEvent(
    //     'Answer saved',
    //     JSON.stringify({
    //       ordinal: this.currQuestion.ordinal,
    //       answer: this._examService.getAnswer(this.currQuestion.ordinal),
    //     })
    //   )
    // );
  }
  ngOnChanges(changes: SimpleChanges): void {}

  onAnswerEvaluated(result: any) {
    if (result.score) {
      let ta = this._tutorialService.getCurrentTutorialAnswer(
        this.tutorialContent.stepOrdinal,
        this.tutorialContent.questionOrdinal || 0
      );
      if (result.score.is_correct) {
        ta.correctStatus = true;
        this.codeRun.emit({
          questionOrdinal: this.tutorialContent.questionOrdinal,
          ordinal: this.tutorialContent.ordinal,
          correct: true,
          type: this.tutorialContent.type,
          required: ta.required,
          answer: ta.currAnswer,
        });
      } else {
        ta.correctStatus = false;
        this.codeRun.emit({
          questionOrdinal: this.tutorialContent.questionOrdinal,
          ordinal: this.tutorialContent.ordinal,
          correct: false,
          type: this.tutorialContent.type,
          required: ta.required,
          answer: ta.currAnswer,
        });
      }
    }

    this.timesRun++;
  }

  getCodeMirrorSkin(): string {
    return this._tutorialService.getCodeMirrorSkin();
  }
}
