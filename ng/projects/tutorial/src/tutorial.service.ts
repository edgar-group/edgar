import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { firstValueFrom, Observable, map, of, Subject, tap } from 'rxjs';
import {
  TutorialServiceConfig,
  TUTORIAL_SERVICE_CONFIG,
} from './tutorial.service.config';

import { environment } from '../../config/environments/environment';
import { TutorialStep } from './models/TutorialStep';
import { Tutorial } from './models/Tutorial';
import { Question } from '../../common/models/question';
import { TutorialStepHint } from './models/TutorialHint';
import { Answer, CodeAnswer } from '../../common/models/answer';
import { TutorialContent, TutorialContentType } from './models/TutorialContent';
import { CodeRunnerQuestion } from '../../common/models/code-runner-question';
import { LogService } from 'projects/common/services/log.service';
import { LogEvent } from 'projects/common/models/log-event';
import { TutorialAnswer } from './models/TutorialAnswer';

@Injectable({ providedIn: 'root' })
export class TutorialService {
  // static DEV_TUTORIAL_ID = 19;
  _currentStepOrdinal: number = 1;
  public currentStepObservable = new Subject<TutorialStep>();
  private _steps: TutorialStep[];
  private _tutorial!: Tutorial;
  private _questions: Question[][];
  private _tutorialContent: TutorialContent[][];
  private _codeRunners: CodeRunnerQuestion[][];

  constructor(
    private _http: HttpClient,
    private _logService: LogService,
    @Inject(TUTORIAL_SERVICE_CONFIG) private _config: TutorialServiceConfig
  ) {
    this._steps = [];
    this._questions = [];
    this._tutorialContent = [];
    // this._httpJsonHeaders = new HttpHeaders();
    // this._httpJsonHeaders.set('Content-Type', 'application/json');
    this._codeRunners = [];
  }

  getCurrentStepObservable(): Observable<any> {
    return this.currentStepObservable;
  }

  getCurrentStep(): TutorialStep {
    return this._steps[this._currentStepOrdinal - 1];
  }

  async setCurrentStepOrdinal(ordinal: number): Promise<void> {
    await this.getStep(ordinal);
    this._currentStepOrdinal = ordinal;

    this.currentStepObservable.next(this._steps[ordinal - 1]);
  }

  // getAnswer(ordinal: number): Answer {
  //   return new Answer('');
  // }

  tutorialLoaded(): boolean {
    return !!this._tutorial;
  }

  getTutorial(): Tutorial {
    if (this._tutorial) return this._tutorial;
    else {
      throw 'Missing cached _tutorial!?';
    }
  }

  getStepQuestions(ordinal: number): Question[] {
    return this._questions[ordinal - 1];
  }

  devlogin() {
    console.log('TUTORIAL devlogin, posting...');
    var formData: any = new FormData();
    return firstValueFrom(
      this._http
        .post(`${environment.devWebApiUrl ?? ''}/auth/ngdevlogin`, formData, {
          withCredentials: true,
          responseType: 'text',
          observe: 'response',
        })
        .pipe(
          map((response) => {
            return 'asdas';
          })
        )
    );
  }

  parseStep(html: string, stepOrdinal: number): TutorialContent[] {
    let service = this;
    var newHtml = html;
    let tokens = [];
    var matches = html.match(
      /(<!--\s*(code-playground|question|multichoice|code-question).*-->)((?!(<!-- \/code-playground|<!-- \/question|<!-- \/multichoice|<!-- \/code-question))[.\s\S])*<!--\s*\/(code-playground|question|multichoice|code-question)\s*-->/gi
      // /(<!--\s*(code-playground|question|multichoice|code-question).*-->)((?!(code-playground|question|multichoice|code-question))[^])*<!--\s*\/(code-playground|question|multichoice|code-question)\s*-->/gi
    );
    var chunks: TutorialContent[] = [];
    let types: TutorialContentType[] = [];
    // var currAnswers: Answer[] = [];
    var requiredList: boolean[] = [];
    var langidsNumList: number[][] = [];

    if (matches && matches.length) {
      var newHtml = html;
      matches.forEach(function (val) {
        var langidsNum: number[] = [];
        var langids: string[] = [];
        var required = false;

        var header = val.substring(0, val.indexOf('-->') + 3);
        // console.log("header", header);
        var cridType = header.split('crid');
        var hasRequired = header.match('required');
        var holder = header.split('langids=')[1];
        if (holder) {
          langids = holder.split('(')[1].split(')')[0].split(',');
        }

        for (let i = 0; i < langids.length; i++) {
          langidsNum.push(parseInt(langids[i]));
        }
        langidsNumList.push(langidsNum);

        if (hasRequired) {
          required = true;
        }
        requiredList.push(required);

        val = val.replace(header, '');
        var content = val.substring(0, val.lastIndexOf('<!--'));

        let initialMarkupCodeAnswer = new CodeAnswer(
          TutorialService.unescapeHtml(content)
        );
        // answer.codeAnswer.code = content.replace(/<[^>]*>/g, '');
        // currAnswers.push(answer);
        let questionOrdinal = types.length + 1;
        if (!service._tutorial.currAnswers[stepOrdinal - 1]) {
          service._tutorial.currAnswers[stepOrdinal - 1] = [];
        }
        if (cridType.length > 1) {
          types.push(TutorialContentType.CodePlayground);
        } else if (header.indexOf('multichoice') >= 0) {
          types.push(TutorialContentType.AbcQuestion);
          let AbcQuestion;
          try {
            // console.log("parsing", content)
            let abcparts = content.split(/<hr>|@@@|<hr\/>/);
            //if (!abcparts.length) abcparts = content.split("<hr/>");
            AbcQuestion = {
              question: abcparts[0],
              answers: [],
            };
            for (let i = 1; i < abcparts.length; ++i) {
              AbcQuestion.answers.push({
                aHtml: abcparts[i].replace('-- correct --', ''),
                isCorrect: abcparts[i].indexOf('-- correct --') >= 0,
                ordinal: i,
              });
            }
          } catch (error) {
            AbcQuestion = {
              question:
                'Could not parse multichice question, please check your tutorial.',
              answers: [{ aHtml: 'OK', isCorrect: true, ordinal: 1 }],
            };
          }
          service.setQuestion(
            stepOrdinal,
            questionOrdinal,
            new Question(
              service._tutorial.id * 1000000 +
                stepOrdinal * 1000 +
                questionOrdinal,
              `${stepOrdinal}/${questionOrdinal}`,
              stepOrdinal * 1000 + questionOrdinal,
              [],
              AbcQuestion.question,
              AbcQuestion.answers,
              AbcQuestion.answers.filter((x) => x.isCorrect).length > 1,
              '',
              '',
              false,
              '',
              'Classic (ABC) question',
              [],
              [],
              [],
              0,
              0,
              [],
              'horizontal',
              '1/0/0',
              null,
              null
            )
          );
        } else {
          var qidType = header.split('qid');
          if (qidType.length > 1) {
            types.push(
              header.indexOf('code-question') >= 0
                ? TutorialContentType.CodeQuestion
                : TutorialContentType.AbcQuestion
            );
          } else {
            console.error('Unsupported code runner type!');
            types.push(TutorialContentType.HTML);
          }
        }

        // init answers if they are not provided via server - note that teacher may change the layout...
        if (
          !service._tutorial.currAnswers[stepOrdinal - 1][questionOrdinal - 1]
        ) {
          let t = types[types.length - 1];
          if (t === TutorialContentType.AbcQuestion) {
            service._tutorial.currAnswers[stepOrdinal - 1][
              questionOrdinal - 1
            ] = new TutorialAnswer(
              questionOrdinal,
              new Answer('Tutorial ABC'),
              false,
              required
            );
          } else {
            service._tutorial.currAnswers[stepOrdinal - 1][
              questionOrdinal - 1
            ] = new TutorialAnswer(
              questionOrdinal,
              new Answer('Tutorial code', initialMarkupCodeAnswer),
              false,
              required
            );
          }
        }
        newHtml = newHtml.replace(val, '|*|*|CR|*|*|');
      });

      var newHtmls = newHtml.split('|*|*|');
      let j = 1;
      for (let i = 0; i < newHtmls.length; i++) {
        if (newHtmls[i] == 'CR') {
          chunks.push(
            new TutorialContent(
              stepOrdinal,
              newHtmls[i],
              i,
              j,
              types[j - 1],
              langidsNumList[j - 1]
            )
          );
          j++;
        } else {
          chunks.push(
            new TutorialContent(
              stepOrdinal,
              newHtmls[i],
              i,
              null,
              TutorialContentType.HTML,
              []
            )
          );
        }
      }
      // console.log('chunks', chunks);
      return chunks;
    }

    chunks.push(
      new TutorialContent(
        stepOrdinal,
        newHtml,
        0,
        0,
        TutorialContentType.HTML,
        []
      )
    );

    return chunks;
  }
  public static unescapeHtml(code): string {
    const doc = new DOMParser().parseFromString(code, 'text/html');
    return doc.documentElement.textContent || '\n\n\n\n\n\n';
  }
  async getStep(ordinal: number) {
    if (this._steps[ordinal - 1]) {
      return this._steps[ordinal - 1];
    } else {
      this._logService.log(new LogEvent('Request step', 'ordinal: ' + ordinal));

      await firstValueFrom(
        this._http
          .get<TutorialStep>(
            `${this._config.getTutorialUrl}/${
              this.getTutorial().id
            }/step/${ordinal}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map((res) => {
              const hints = res.hints.map(
                (hint) => new TutorialStepHint(hint.ordinal, hint.html)
              );

              this._steps[ordinal - 1] = new TutorialStep(
                res.id,
                res.ordinal,
                res.title,
                res.html,
                hints
              );
              this._tutorialContent[ordinal - 1] = this.parseStep(
                this._steps[ordinal - 1].html,
                ordinal
              );
              // restore answers for lazy loaded step
              // let ca = this._tutorial.currAnswers[ordinal - 1];
              // if (ca) {
              //   for (let tc of this._tutorialContent[ordinal - 1]) {
              //     let answer = ca.find(
              //       (item) => item.questionOrdinal == tc.questionOrdinal
              //     );
              //     // console.log(
              //     //   'found answer for',
              //     //   answer,
              //     //   this._tutorialContent[ordinal - 1]
              //     // );
              //     if (answer) {
              //       tc.currAnswer = answer.currAnswer;
              //       tc.correctStatus = answer.correctStatus;
              //       tc.required = answer.required ? true : tc.required;
              //     }
              //   }
              // }
              this._logService.log(
                new LogEvent(
                  'Received step',
                  `ordinal: ${ordinal}, id: ${res.id}`
                )
              );

              return this._steps[ordinal - 1];
            })
          )
      );
      return Promise.resolve(this._steps[ordinal - 1]);
    }
  }

  //   async loadAllSteps() {
  //     //console.log("Steps are being loaded!")
  //     let promises: any[] = [];
  //     for (let i = 1; this._tutorial && i <= this._tutorial.steps.length; i++) {
  //       promises.push(this.getStep(i));
  //     }
  //     return await Promise.all(promises);
  //   }

  //   async getAllSteps(): Promise<TutorialStep[]> {
  //     let rv: any[] = [];
  //     for (let i = 0; i < this._tutorial.steps.length; ++i) {
  //       rv.push(await this.getStep(i + 1));
  //     }
  //     //console.log("Got all steps!");
  //     return rv;
  //   }

  updateAnswers() {
    // stepOrdinal: number, questionOrdinal: number, answer
    // Not gonna wait for this one:
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    this._http
      .put<{ success: boolean }>(
        `${this._config.getTutorialUrl}/${this.getTutorial().id}/answers`,
        { answers: this._tutorial.currAnswers },
        {
          headers,
          withCredentials: true,
        }
      )
      .subscribe((r) => {
        if (!r.success) {
          console.error('Failed to update answers', r);
        }
      });
  }

  getQuestionCorrectStatus(
    stepOrdinal: number,
    questionOrdinal: number
  ): boolean {
    return (
      this._tutorial.currAnswers[stepOrdinal - 1][questionOrdinal - 1] &&
      this._tutorial.currAnswers[stepOrdinal - 1][questionOrdinal - 1]
        .correctStatus
    );
  }
  getStepCorrectStatus(stepOrdinal: number): boolean {
    if (this._tutorial.currAnswers[stepOrdinal - 1]) {
      return (
        this._tutorial.currAnswers[stepOrdinal - 1].filter(
          (x) => !x.correctStatus && x.required
        ).length === 0
      );
    } else {
      return false;
    }
  }

  async loadTutorial(idTutorial): Promise<Tutorial> {
    if (!this._tutorial) {
      this._logService.log(new LogEvent('Loading tutorial', ''));
      await firstValueFrom(
        this._http
          .get<Tutorial>(`${this._config.getTutorialUrl}/${idTutorial}`, {
            withCredentials: true,
          })
          .pipe(
            map((res) => {
              this._tutorial = new Tutorial(
                res.id,
                res.title,
                res.steps,
                res.currAnswers,
                res.allowRandomAccess,
                res.cmSkin,
                res.latestStepOrdinal
              );
              this._logService.log(
                new LogEvent('Loaded tutorial', 'id:' + this._tutorial.id)
              );
              return this._tutorial;
            })
          )
      );
    }
    return Promise.resolve(this._tutorial);
  }

  hasQuestion(stepOrdinal: number, questionOrdinal: number): boolean {
    return !!(
      this._questions[stepOrdinal - 1] &&
      this._questions[stepOrdinal - 1][questionOrdinal - 1]
    );
  }
  hasCodeRunner(stepOrdinal: number, codeRunnerOrdinal: number): boolean {
    return !!(
      this._codeRunners[stepOrdinal - 1] &&
      this._codeRunners[stepOrdinal - 1][codeRunnerOrdinal - 1]
    );
  }
  setQuestion(
    stepOrdinal: number,
    questionOrdinal: number,
    newQuestion: Question
  ) {
    if (!this._questions[stepOrdinal - 1]) {
      this._questions[stepOrdinal - 1] = [];
    }
    this._questions[stepOrdinal - 1][questionOrdinal - 1] = newQuestion;
  }
  public getCurrentTutorialAnswer(
    stepOrdinal: number,
    questionOrdinal: number
  ): TutorialAnswer {
    return this._tutorial.currAnswers[stepOrdinal - 1][questionOrdinal - 1];
  }

  public async getQuestion(
    stepOrdinal: number,
    questionOrdinal: number
  ): Promise<Question> {
    if (this.hasQuestion(stepOrdinal, questionOrdinal)) {
    } else {
      //this._logService.log(new LogEvent('Get question', 'num:' + ordinal));
      await firstValueFrom(
        this._http
          .get<any>(
            `${this._config.getQuestionUrl}/${stepOrdinal}/${questionOrdinal}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map((res) => {
              let q = res;
              this.setQuestion(
                stepOrdinal,
                questionOrdinal,
                new Question(
                  q.id,
                  `${stepOrdinal}/${questionOrdinal}`,
                  stepOrdinal * 1000 + questionOrdinal,
                  q.answers,
                  q.content,
                  q.answers,

                  q.answersStatic,
                  q.answersDynamic,
                  q.initialDiagramAnswer,

                  q.multiCorrect,
                  q.modeDesc,
                  q.type,
                  q.programmingLanguages,
                  q.attachments,
                  q.scaleItems,
                  0, // upload_files_no
                  0, // required_upload_files_no
                  q.tempUploadedFileNames,
                  q.displayOption,
                  q.gradingModelName,
                  q.timeLeft,
                  q.timeLimit
                )
              );
            })
          )
      );
    }
    return Promise.resolve(
      this._questions[stepOrdinal - 1][questionOrdinal - 1]
    );
  }

  // getLangids(stepOrdinal: number, questionOrdinal: number): number[] {
  //   var ids = this._questionIds[stepOrdinal - 1][questionOrdinal - 1];
  //   return [];
  // }

  public async getCodeRunner(
    stepOrdinal: number,
    codeRunnerOrdinal: number
  ): Promise<CodeRunnerQuestion> {
    if (this.hasCodeRunner(stepOrdinal, codeRunnerOrdinal)) {
    } else {
      //this._logService.log(new LogEvent('Get question', 'num:' + ordinal));
      await firstValueFrom(
        this._http
          .get<any>(
            `${this._config.getCodeRunnerUrl}/${stepOrdinal}/${codeRunnerOrdinal}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map((res) => {
              let q = res;
              if (!this._codeRunners[stepOrdinal - 1]) {
                this._codeRunners[stepOrdinal - 1] = [];
              }
              let newCodeRunner = new CodeRunnerQuestion(
                stepOrdinal * 1000 + codeRunnerOrdinal,
                q.idCodeRunner,
                q.programmingLanguages
              );
              this._codeRunners[stepOrdinal - 1][codeRunnerOrdinal - 1] =
                newCodeRunner;
            })
          )
      );
    }
    return Promise.resolve(
      this._codeRunners[stepOrdinal - 1][codeRunnerOrdinal - 1]
    );
  }

  getCodeMirrorSkin(): string {
    return this._tutorial.cmSkin || 'ambiance';
  }

  getTutorialContent(stepOrdinal: number): TutorialContent[] {
    return this._tutorialContent[stepOrdinal - 1];
  }

  submitSuccess() {}
}
