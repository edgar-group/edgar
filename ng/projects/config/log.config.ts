import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const LOG_SERVICE_CONFIG = new InjectionToken<LogServiceConfig>(
  'log.service.config'
);

export interface LogServiceConfig {
  logEnabled: boolean;
  logUrl: string;
  hbUrl: string;
}

export const EXERCISE_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logEnabled: true,
  logUrl: `${environment.devWebApiUrl ?? ''}/exercise/log`,
  hbUrl: `${environment.devWebApiUrl ?? ''}/exercise/hb`,
};

export const EXAM_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logEnabled: true,
  logUrl: `${environment.devWebApiUrl ?? ''}/exam/run/log`,
  hbUrl: `${environment.devWebApiUrl ?? ''}/exam/run/hb`,
};

export const EXAM_REVIEW_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logEnabled: false,
  logUrl: ``,
  hbUrl: ``,
};

export const TUTORIAL_LOG_SERVICE_CONFIG: LogServiceConfig = {
  logEnabled: true,
  logUrl: `${environment.devWebApiUrl ?? ''}/tutorial/log`,
  hbUrl: `${environment.devWebApiUrl ?? ''}/tutorial/hb`,
};
