import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const CODERUNNER_SERVICE_CONFIG =
  new InjectionToken<CodeRunnerServiceConfig>(`coderunner.service.config`);

export interface CodeRunnerServiceConfig {
  execCodeUrl: string;
  execCodeReviewUrl: string;
}
export const EXAM_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  execCodeUrl: `${environment.devWebApiUrl ?? ''}/exam/run/question`,
  execCodeReviewUrl: `${environment.devWebApiUrl ?? ''}/exam/review/exec`,
};

// TODO: fix these urls:
export const EXERCISE_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  execCodeUrl: `${environment.devWebApiUrl ?? ''}/exercise/run/question`,
  execCodeReviewUrl: `${
    environment.devWebApiUrl ?? ''
  }/exercise/review/question`,
};
export const TUTORIAL_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  execCodeUrl: `${environment.devWebApiUrl ?? ''}/tutorial/run/step`,
  execCodeReviewUrl: `${
    environment.devWebApiUrl ?? ''
  }/tutorial/review/question`,
};
