import {
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { LogEvent } from '../../../../common/models/log-event';
import { Question } from 'projects/common/models/question';
import { environment } from 'projects/config/environments/environment';
import { HttpClient } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit, OnChanges {
  static readonly SELECT_FILE_STRING = 'Select file and click upload ...';

  @Input() currQuestion!: Question;
  @Input() uploadFileLimit!: number;
  public _selectedFiles!: Array<string | null>;
  public _selectedFileNames!: Array<string>;
  public _uploadErrors!: Array<string>;
  public isRunning: Array<boolean> = new Array();
  constructor(
    private _logService: LogService,
    private _http: HttpClient,
    private _ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    this._uploadErrors = Array(this.currQuestion.uploadFilesNo)
      .fill(0)
      .map(() => '');
    this._selectedFiles = Array(this.currQuestion.uploadFilesNo)
      .fill(0)
      .map(() => null);
    this._selectedFileNames = Array(this.currQuestion.uploadFilesNo)
      .fill(0)
      .map(() => FileUploadComponent.SELECT_FILE_STRING);
    this.isRunning = Array(this.currQuestion.uploadFilesNo)
      .fill(0)
      .map(() => false);
  }
  fileSelected(idx: number, event) {
    if (event.target.files[0].size > this.uploadFileLimit) {
      this._selectedFiles = this._selectedFiles.map((x, i) =>
        i == idx ? null : x
      );
      let errMessage = `File too big: ${event.target.files[0].size}B  > ${this.uploadFileLimit}B (this is the limit)`;
      this._uploadErrors[idx] = errMessage;
      this._logService.log(new LogEvent('File too big', errMessage));
    } else {
      let file = this._selectedFiles[idx];
      if (file) file = file.split('\\').pop() || null;
      this._selectedFileNames = this._selectedFileNames.map((x, i) =>
        i == idx ? file || '?' : x
      );
      this._uploadErrors[idx] = '';
    }
  }
  async deleteUploadedFile(idx: number) {
    let toLogDeletedFileName = this.currQuestion.tempUploadedFileNames![idx];
    //this.currQuestion.tempUploadedFileNames![idx] =  FileUploadComponent.RUNNING_STRING;
    this.isRunning[idx] = true;
    try {
      // this.currQuestion.uploaded![idx] = false;
      const resp: any = await firstValueFrom(
        this._http.post(
          `${environment.devWebApiUrl ?? ''}/exam/run/file/delete/${
            this.currQuestion.ordinal
          }/${idx}`,
          {},
          {
            withCredentials: true,
          }
        )
      );
      if (resp.success) {
        this.currQuestion.tempUploadedFileNames![idx] = '';
        this.currQuestion.uploaded![idx] = false;
        this._logService.log(
          new LogEvent('Deleted uploaded file', toLogDeletedFileName)
        );
      } else {
        this._uploadErrors[idx] = resp.error && resp.error.message;
      }
    } catch (error) {
      this._uploadErrors[idx] = JSON.stringify(error);
      this._logService.log(
        new LogEvent(
          'Failed deleting uploaded file' + error,
          toLogDeletedFileName
        )
      );
    } finally {
      this.isRunning[idx] = false;
      this._ref.markForCheck();
    }
  }

  uploadButtonDisabled(idx: number) {
    let elem: any = document.getElementById(
      'fUpload' + this.currQuestion.ordinal + idx
    );
    return '' == elem.value;
  }

  async uploadFile(idx: number) {
    var form = document.forms.namedItem(
      'fileinfo' + this.currQuestion.ordinal + idx
    );
    var formData = new FormData(form!);

    // this.currQuestion.tempUploadedFileNames![idx] =  FileUploadComponent.RUNNING_STRING;
    this.isRunning[idx] = true;
    try {
      this.currQuestion.uploaded![idx] = false;
      const resp: any = await firstValueFrom(
        this._http.post(
          `${environment.devWebApiUrl ?? ''}/exam/run/file/upload/${
            this.currQuestion.ordinal
          }/${idx}`,
          formData,
          {
            withCredentials: true,
          }
        )
      );
      if (resp.success) {
        this.currQuestion.tempUploadedFileNames![idx] =
          resp.data && resp.data.originalname;
        this.currQuestion.uploaded![idx] = true;
        this._logService.log(
          new LogEvent(
            'Uploaded file',
            this.currQuestion.tempUploadedFileNames![idx]
          )
        );
      } else {
        this.setUploadFailed(idx, resp.error && resp.error.message);
      }
      this._ref.markForCheck();
    } catch (error) {
      console.error(error);
      this.setUploadFailed(idx, JSON.stringify(error));
      // this.currQuestion.tempUploadedFileNames![idx] =
      //   'Error occurred when trying to upload your file: ' +
      //   JSON.stringify(error);
      this._logService.log(
        new LogEvent(
          'Error uploading file',
          this.currQuestion.tempUploadedFileNames![idx] +
            ' Error: ' +
            JSON.stringify(error)
        )
      );
    } finally {
      this.isRunning[idx] = false;
      this._selectedFileNames = this._selectedFileNames.map((x, i) => {
        return i == idx ? FileUploadComponent.SELECT_FILE_STRING : x;
      });
      // clearing the input field, so that user can choose the same file (and trigger the change event again)
      (
        document.querySelector(
          `form[name="fileinfo${this.currQuestion.ordinal}${idx}"]`
        ) as HTMLFormElement
      ).reset();
    }
  }
  setUploadFailed(idx: number, message: string) {
    this._uploadErrors[idx] = message;
    this.currQuestion.uploaded![idx] = false;
    this.currQuestion.tempUploadedFileNames![idx] = '';
  }
}
