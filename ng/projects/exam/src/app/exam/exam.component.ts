import {
  Component,
  HostListener,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { Question } from 'projects/common/models/question';
import { ExamService } from '../exam.service';
import { LogEvent } from '../../../../common/models/log-event';
import { ViewChild } from '@angular/core';
import { SubmitComponent } from '../submit/submit.component';
import { environment } from 'projects/config/environments/environment';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXAM_CODERUNNER_SERVICE_CONFIG,
} from '../../../../config/code-runner.config';
import {
  DEFAULT_EXAM_CONFIG,
  EXAM_SERVICE_CONFIG,
} from '../exam.service.config';
import {
  ActivatedRoute,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Params,
  Router,
  Event as NavigationEvent,
} from '@angular/router';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app',
  templateUrl: `exam.component.html`,
  styleUrls: ['exam.component.css'],
  providers: [
    // LogService,
    // { provide: LOG_SERVICE_CONFIG, useValue: EXAM_LOG_SERVICE_CONFIG },
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
    },
    { provide: EXAM_SERVICE_CONFIG, useValue: DEFAULT_EXAM_CONFIG },
  ],
})
export class ExamComponent implements OnInit {
  _currQuestion!: Question;
  // _currAnswers: Answer[] = [];
  // _exam: Exam;

  // _confirm: boolean;
  _layoutHorizontal: boolean;
  _showScoreboard: boolean;
  _examRecievedAt: Date = new Date();
  _secondsLeft: number = 100;
  _PA2Questions: Question[] = [];

  _paceIntervalRef: any;
  _currQuestionWait?: number;
  _examLoaded: boolean = false;

  _heartBeatFailWarning: string;
  // @ViewChild(SubmitComponent) private submitComponent!: SubmitComponent;

  constructor(
    private _examService: ExamService,
    private _logService: LogService,
    private _route: ActivatedRoute
  ) {
    this._layoutHorizontal = false;
    // this._confirm = false;
    this._showScoreboard = false;
    // this._exam = this._examService.getExam();
    this._heartBeatFailWarning = '';
  }
  public isOffline(): boolean {
    return !window.navigator.onLine;
  }
  public async ngOnInit() {
    // this._router.events.subscribe((event: NavigationEvent) => {
    //   if (event instanceof NavigationStart) {
    //     // Show progress spinner or progress bar
    //     console.log('NavigationStart', event);
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide progress spinner or progress bar
    //     console.log('NavigationEnd', event);
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide progress spinner or progress bar

    //     // Present error to user
    //     console.log('NavigationError', event.error);
    //   }
    // });
    this._examService.currentQuestionObservable.subscribe((currQuestion) => {
      this._currQuestion = currQuestion;
      // console.log(this._currQuestion);
      this.refreshPaceInterval();
    });
    this._logService.networkFailCounterObservable.subscribe((hbFailCount) => {
      if (hbFailCount === 0) {
        this._heartBeatFailWarning = '';
      } else if (hbFailCount === 1) {
        this._heartBeatFailWarning =
          'Missed one heartbeat, could be a problem with a connection to the backend.';
      } else if (hbFailCount === 2) {
        this._heartBeatFailWarning =
          'Missed TWO heartbeats, probably a problem with a connection to the backend. Backup work, check your internet connection.';
      } else {
        this._heartBeatFailWarning =
          'Missed ' +
          hbFailCount +
          ' heartbeats! There is a problem with a connection to the backend. Backup work, check your internet connection!';
      }
    });
    this._route.params.subscribe(async (params: Params) => {
      let ordinal = +params['id'];
      if (ordinal > 0) {
        this._examLoaded = true;
        this._examService.setCurrentQuestionOrdinal(ordinal);
      }
    });
  }

  refreshPaceInterval() {
    // console.log('refreshPaceInterval._currQuestion', this._currQuestion);

    if (this._paceIntervalRef) {
      clearInterval(this._paceIntervalRef);
    }

    if (this._currQuestion && this._examService.getExam().runStats) {
      for (let runStat of this._examService.getExam().runStats) {
        if (this._currQuestion.ordinal == runStat.ordinal) {
          this._currQuestionWait = runStat.time_wait;
          this._currQuestion.runs = runStat.qcnt;
          break;
        }
      }

      this._paceIntervalRef = setInterval(() => {
        this._currQuestionWait = 0;
        if (this._examLoaded && this._currQuestion) {
          for (let runStat of this._examService.getExam().runStats) {
            runStat.time_wait--;
            if (this._currQuestion.ordinal === runStat.ordinal) {
              this._currQuestionWait = runStat.time_wait;
            }
          }
        }
        // this._ref.markForCheck();
      }, 1000);

      // this._ref.markForCheck();
    }
  }
  // //calback from q-content
  // // MOVED below, to Question Content
  // onAnswerUpdated($event: { $event: any; ordinal: any }) {
  //   console.log('unnecessary? to be deleted...');
  // }

  //refershes exam statistics
  onAnswerEvaluated($newValue: any) {
    if (this._examLoaded) {
      this._examService.getExam().setRunStats($newValue.runStats);
    }
    this.refreshPaceInterval();
  }

  //event from header
  onLayoutHorizontalChanged(isHorizontal: boolean) {
    this._layoutHorizontal = isHorizontal;
  }

  //event from header
  onShowScoreboard() {
    this._showScoreboard = !this._showScoreboard;
  }

  // //callback from header
  // onSubmit(shouldConfirm: boolean) {
  //   this._confirm = shouldConfirm;
  //   // this.refreshQAFormatted();
  // }

  // onFocus() {
  //   this._logService.log(new LogEvent('Got focus', '-'));
  // }

  // onBlur() {
  //   this._logService.log(
  //     new LogEvent(
  //       'Lost focus',
  //       'Warning - student could be cheating (alt+tab)?'
  //     )
  //   );
  // }
}
