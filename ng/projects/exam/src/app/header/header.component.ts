import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ExamService } from '../exam.service';
import { Exam } from 'projects/models/exam/exam';

import { Router } from '@angular/router';
import { environment } from 'projects/config/environments/environment';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'header-navigation',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderNavigationComponent implements OnInit {
  @Input() _isReview: boolean = false;
  _layoutHorizontal: boolean;
  _questionSelected?: boolean;
  _exam: Exam;
  _currQuestion!: Question;
  PAAssignementUrl: string;
  @Output() layoutHorizontalChanged = new EventEmitter<boolean>();
  @Output() confirmed = new EventEmitter<boolean>();
  @Output() showScoreboard = new EventEmitter<boolean>();

  constructor(
    private _examService: ExamService,
    private _ref: ChangeDetectorRef,
    private _router: Router
  ) {
    this._layoutHorizontal = false;
    this._exam = this._examService.getExam();
    this.PAAssignementUrl = `${
      environment.devWebApiUrl ?? ''
    }/exam/run/peerattachment`;
  }

  public async ngOnInit() {
    this._examService.getCurrentQuestionObservable().subscribe((q) => {
      this._currQuestion = q;
    });
    this._examService.getCurrentAnswersObservable().subscribe((arr) => {
      this._ref.markForCheck();
    });
    this._currQuestion = this._examService.getCurrentQuestion();
  }

  onLayoutHorizontalChanged() {
    this._layoutHorizontal = !this._layoutHorizontal;
    this.layoutHorizontalChanged.emit(this._layoutHorizontal);
  }

  onQuestionSelected($event: { ordinal: number }) {
    // this.questionSelected.emit($event);
  }

  onShowScoreboard() {
    this.showScoreboard.emit();
  }
  navigateToSubmit() {
    this._router.navigate([`/submit`]);
  }

  // @HostListener('window:resize', ['$event'])
  // onResize() {
  //   let h = $('.edgar-sticky').height();
  //   if (h) {
  //     $('.edgar-header-bg').height(h + 75);
  //   }
  // }

  // ngAfterViewInit(): void {
  //   let h = $('.edgar-sticky').height();
  //   if (h) {
  //     $('.edgar-header-bg').height(h + 75);
  //   }
  // }
}
