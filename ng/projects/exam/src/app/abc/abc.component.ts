import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';

@Component({
  selector: 'abc',
  templateUrl: './abc.component.html',
  styleUrls: ['./abc.component.css'],
})
export class AbcComponent {
  @Input()
  currQuestion!: Question;
  @Input() currAnswer!: Answer;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  constructor(
    protected sanitizer: DomSanitizer,
    private _ref: ChangeDetectorRef,
  ) {}

  isSelected(answer: any) {
    // TODO igor
    // console.log("QContentComponent -->  isSelected ");
    // TODO: when selecting an answer from multilanguage component check if already selected
    return (
      this.currQuestion &&
      this.currAnswer.multichoice.indexOf(answer.ordinal) >= 0
    );
  }

  onSelect(answer: any) {
    // TODO igor
    if (this.currQuestion) {
      var arrAnswers = this.currAnswer.multichoice;
      if (this.currQuestion.multiCorrect) {
        var idx;
        if ((idx = arrAnswers.indexOf(answer.ordinal)) >= 0) {
          arrAnswers.splice(idx, 1);
        } else {
          arrAnswers.push(answer.ordinal);
        }
      } else {
        if (arrAnswers.indexOf(answer.ordinal) >= 0) {
          arrAnswers = [];
        } else {
          arrAnswers = [answer.ordinal];
        }
      }
      this.currAnswer.multichoice = arrAnswers;

      this.answerUpdated.emit(this.currAnswer);
      this.answerSaved.emit(); // TODO this is async but there is no visual cue to the saving...
    }
  }


}
