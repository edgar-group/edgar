import {
  Component,
  ChangeDetectorRef,
  EventEmitter,
  Input,
  OnInit,
  OnChanges,
  Output,
  ViewChild,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { ScaleItem } from '../../../../common/models/scale-item';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';
// import { QuestionCodeComponent } from '../question-code/question-code.component';
import { ExamService } from '../exam.service';
import { LogEvent } from 'projects/common/models/log-event';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'question-content',
  templateUrl: './question-content.component.html',
  styleUrls: ['./question-content.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionContentComponent implements OnInit, OnChanges {
  @Input()
  currQuestion!: Question;
  @Input()
  currQuestionWait!: number;
  // @Input()
  // cmSkin!: string;
  @Input()
  layoutHorizontal!: boolean;
  @Input()
  layoutTutorial!: boolean;
  @Input()
  uploadFileNo!: number;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerEvaluated = new EventEmitter();

  errorUpdatingServerClock: string = '';

  constructor(
    private _logService: LogService,
    private _examService: ExamService,
    protected sanitizer: DomSanitizer,
    private _ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('question content ngOnChanges', changes);
    this.errorUpdatingServerClock = '';
    this.currQuestion.setTimerListener(this.clockTicked.bind(this));
  }

  onScaleItemSelected(si: ScaleItem) {
    let answer = this._examService.getAnswer(this.currQuestion.ordinal);
    answer.multichoice = [si.value];
    this._examService.setAnswer(this.currQuestion.ordinal, answer);

    this.answerUpdated.emit({
      $event: this._examService.getAnswer(this.currQuestion.ordinal),
    });
  }

  onAnswerUpdated(answer: Answer) {
    this.answerUpdated.emit({ $event: answer });
    this._examService.setAnswer(this.currQuestion.ordinal, answer);
  }
  onAnswerSaved() {
    this._logService.log(
      new LogEvent(
        'Answer saved',
        JSON.stringify({
          ordinal: this.currQuestion.ordinal,
          answer: this._examService.getAnswer(this.currQuestion.ordinal),
        })
      )
    );
  }

  onAnswerEvaluated(result: any) {
    this.answerEvaluated.emit(result);
  }

  async startTheClock() {
    this.errorUpdatingServerClock = '';
    let notifyServer = await this._examService.startTheClock();
    if (notifyServer) {
      this.currQuestion.startTheClock(this.clockTicked.bind(this));
    } else {
      this.errorUpdatingServerClock = 'Error updating server clock (are you online?), try again later.';
      this._ref.markForCheck();
    }
  }
  clockTicked(secondsLeft: number) {
    this._ref.markForCheck();
  }
}
