import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { LogService } from '../../../common/services/log.service';
import { firstValueFrom, map, Observable, of, Subject, tap } from 'rxjs';

import { LogEvent } from '../../../common/models/log-event';
import { ExamServiceConfig, EXAM_SERVICE_CONFIG } from './exam.service.config';

import { Answer, CodeAnswer } from 'projects/common/models/answer';
import { Exam } from 'projects/models/exam/exam';
import { environment } from 'projects/config/environments/environment';
import { IExamService } from 'projects/common/models/IExamService';
import { Question } from 'projects/common/models/question';
import { DiagramService } from 'projects/common/services/diagram.service';

@Injectable({
  providedIn: 'root',
})
export class ExamService implements IExamService {
  private _questions: Question[];
  private _exam!: Exam;
  _examRecievedAt: Date = new Date();
  _secondsLeft: number = 60;
  _currentQuestionOrdinal: number = 0;
  public currentQuestionObservable = new Subject<Question>();
  public currentAnswersObservable = new Subject<Array<Answer>>();

  constructor(
    private _http: HttpClient,
    private _logService: LogService,
    private _diagramService: DiagramService,
    @Inject(EXAM_SERVICE_CONFIG) private _config: ExamServiceConfig
  ) {
    this._questions = [];
    // console.log('config is', _config);
  }
  getCurrentQuestionObservable(): Observable<any> {
    return this.currentQuestionObservable;
  }
  getCurrentAnswersObservable(): Observable<any> {
    return this.currentAnswersObservable;
  }

  hasQuestion(ordinal: number): boolean {
    return !!this._questions[ordinal - 1];
  }

  // getCachedQuestion(num: number): Question | undefined {
  //     return (this.hasQuestion(num)) ? this._questions[num]: null;
  // }
  updateSecondsLeft(): boolean {
    if (this._exam) {
      var diff = new Date().getTime() - this._examRecievedAt.getTime();
      this._secondsLeft = this._exam.secondsLeft - Math.round(diff / 1000);
    }
    return true;
  }
  getSecondsLeft(): number {
    return this._secondsLeft;
  }
  async loadAllQuestions() {
    let promises: any[] = [];
    for (let i = 1; this._exam && i <= this._exam.noOfQuestions; i++) {
      promises.push(this.getQuestion(i));
    }
    return await Promise.all(promises);
    // this._examService.getQuestion(i).subscribe((q) => {
    //   this._PA2Questions[i - 1] = q;
    //   this._ref.markForCheck();
    // });
  }
  async getAllQuestions(): Promise<Question[]> {
    let rv: any[] = [];
    for (let i = 0; i < this._exam.noOfQuestions; ++i) {
      rv.push(await this.getQuestion(i + 1));
    }
    return rv;
  }

  getCurrentQuestion(): Question {
    return this._questions[this._currentQuestionOrdinal - 1];
  }
  getCodeMirrorSkin(): string {
    return this._exam.cmSkin || 'ambiance';
  }
  getAnswer(ordinal: number): Answer {
    return this._exam.currAnswers[ordinal - 1];
  }
  setAnswer(ordinal: number, newAnswer: Answer) {
    this._exam.currAnswers[ordinal - 1] = newAnswer;
    // this below was a bug, TBD, this old save format somehow caused update bugs...
    // if (
    //   this._questions[ordinal - 1].isMultiChoice() ||
    //   this._questions[ordinal - 1].isScaleQuestion()
    // ) {
    //   this._logService.log(
    //     new LogEvent('Answer saved', JSON.stringify(this._exam.currAnswers))
    //   );
    // }
    this.currentAnswersObservable.next(this._exam.currAnswers);
  }

  private async getQuestion(ordinal: number): Promise<Question> {
    if (this.hasQuestion(ordinal)) {
      // console.log('getQuestion(' + ordinal + ') from CACHE');
    } else {
      this._logService.log(new LogEvent('Get question', 'num:' + ordinal));

      await firstValueFrom(
        this._http
          .get<any>(`${this._config.getQuestionUrl}/${ordinal}`, {
            withCredentials: true,
          })
          .pipe(
            map(async (res) => {
              let q = res;
              q.content = await this._diagramService.getInjectSvgDiagrams(
                q.content
              );
              this._questions[ordinal - 1] = new Question(
                q.id,
                q.ordinal.toString(),
                q.ordinal,
                q.answers,
                q.content,
                q.answers,
                q.answersStatic,
                q.answersDynamic,
                q.initialDiagramAnswer,
                q.multiCorrect,
                q.modeDesc,
                q.type,
                q.programmingLanguages,
                q.attachments,
                q.scaleItems,
                q.upload_files_no,
                q.required_upload_files_no,
                q.tempUploadedFileNames,
                q.displayOption,
                q.gradingModelName,
                q.timeLeft,
                q.timeLimit
              );
              q = this._questions[ordinal - 1];
              this._logService.log(
                new LogEvent(
                  'Received question from web',
                  'ordinal:' + q.ordinal
                )
              );
              if (this._exam) {
                this._exam.currAnswers[q.ordinal - 1].type = q.type;
                if (!this._exam.currAnswers[q.ordinal - 1].codeAnswer) {
                  this._exam.currAnswers[q.ordinal - 1].codeAnswer =
                    new CodeAnswer();
                  this._exam.currAnswers[q.ordinal - 1].codeAnswer.code =
                    'Recovered legacy test, ongoing code lost...';
                }
                if (
                  !this._exam.currAnswers[q.ordinal - 1].codeAnswer
                    .languageId &&
                  q.programmingLanguages &&
                  q.programmingLanguages.length > 0
                ) {
                  this._exam.currAnswers[q.ordinal - 1].codeAnswer.languageId =
                    q.programmingLanguages[0].id;
                }
              }
              //this.currentQuestionObservable.next(this._questions[num]);this._questions[num];
            })
          )
      );
      this._questions[ordinal - 1].initUploadVars();
    }
    return Promise.resolve(this._questions[ordinal - 1]);
  }

  async setCurrentQuestionOrdinal(ordinal: number): Promise<void> {
    let q = await this.getQuestion(ordinal);
    this._currentQuestionOrdinal = ordinal;
    this.currentQuestionObservable.next(this._questions[ordinal - 1]);
    this._logService.log(
      new LogEvent('Question selected', `{id: ${q.id}, ordinal: ${q.ordinal} )`)
    );
  }

  // num is zero based, that is num = ordinal - 1
  // getQuestion(num: number): Observable<Question> {

  // }
  devlogin() {
    console.log('devlogin, posting...');
    var formData: any = new FormData();
    formData.append('username', 'jv54183@fer.hr');
    formData.append('password', 'jv54183@fer.hr@node.js');
    // formData.append('username', 'nk51530@fer.hr');
    // formData.append('password', 'nk51530@fer.hr@node.js');
    return firstValueFrom(
      this._http
        .post(`${environment.devWebApiUrl ?? ''}/auth/ngdevlogin`, formData, {
          withCredentials: true,
          responseType: 'text',
          observe: 'response',
        })
        .pipe(
          map((response) => {
            // console.log('simple login response', response);
            return 'asdas';
          })
        )
    );
  }
  examLoaded(): boolean {
    return !!this._exam;
  }

  getExam(): Exam {
    if (this._exam) return this._exam;
    else {
      throw 'Missing cached exam!?';
    }
  }
  async loadExam(): Promise<Exam> {
    if (!this._exam) {
      this._logService.log(new LogEvent('Get exam', '-'));
      // console.log('getting', `${this._config.getExamInstanceUrl}`);
      await firstValueFrom(
        this._http
          .get<any>(`${this._config.getExamInstanceUrl}`, {
            withCredentials: true,
          })
          .pipe(
            map((res) => {
              let t = res;
              if (t.success === false) {
                this._logService.log(
                  new LogEvent('Received NULL exam!', JSON.stringify(t))
                );
                return null;
              } else {
                if (typeof t.currAnswers === 'string') {
                  t.currAnswers = JSON.parse(t.currAnswers);
                }
                this._exam = new Exam(
                  t.idTestInstance,
                  t.noOfQuestions,
                  t.secondsLeft,
                  t.prolonged,
                  t.title,
                  t.currAnswers,
                  t.student,
                  t.alt_id,
                  t.alt_id2,
                  t.cmSkin ? t.cmSkin : 'ambinace',
                  t.imgSrc,
                  t.is_competition,
                  t.upload_file_limit,
                  t.forward_only,
                  t.last_ordinal,
                  t.type_name,
                  t.runStats,
                  t.uses_ticketing
                );
                this._logService.log(
                  new LogEvent('Received exam.', JSON.stringify(this._exam))
                );
                this._examRecievedAt = new Date();
                this.updateSecondsLeft();
                return this._exam;
              }
            })
          )
      );
    }
    return Promise.resolve(this._exam);
  }

  submitTest(): Observable<any> {
    this._logService.log(
      new LogEvent('Submitting exam.', JSON.stringify(this._exam.currAnswers))
    );

    return this._http
      .post(
        `${this._config.submitExamInstanceUrl}`,
        { answers: this._exam.currAnswers },
        {
          // headers: headers,
          withCredentials: true,
        }
      )
      .pipe(
        tap((res) => {
          this._logService.log(
            new LogEvent('Exam submitted.', JSON.stringify(res))
          );
        })
      );
  }
  async getWhyCantSubmit() {
    let rv = '';
    for (let i = 0; i < this._exam.currAnswers.length; ++i) {
      let question = await this.getQuestion(i + 1);
      rv += question.getWhyCantSubmit();
    }
    return rv;
  }
  async getExamSummaryHtml() {
    // console.log(' this._exam.currAnswers', this._exam.currAnswers);
    let rv = '<div class="container p-2"><h3>My answers:</h3></div>';
    for (let i = 0; i < this._exam.currAnswers.length; ++i) {
      let question = await this.getQuestion(i + 1);
      rv += question.getQAFormatted(
        this._exam.currAnswers[question.ordinal - 1]
      );
    }
    return rv;
  }
  getScoreboard(): Observable<any> {
    this._logService.log(new LogEvent('Get scoreboard.', '-'));

    return this._http.get<any>(`${this._config.getScoreboardUrl}`).pipe(
      tap((res) => {
        let scoreboard = res;

        this._logService.log(
          new LogEvent('Received scoreboard.', JSON.stringify(scoreboard))
        );

        //console.log('scoreboard', scoreboard);
      })
    );
  }

  //type AbortSignalWithTimeout = AbortSignal & { timeout(milliseconds: number): AbortSignal }

  async startTheClock(): Promise<boolean> {
    try {
      const response = await fetch(
        `${this._config.startTheClock ?? ''}/${this._currentQuestionOrdinal}`,
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          credentials: 'include', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json',
          },
          signal: AbortSignal.timeout(5000),
          //body: JSON.stringify(data)
        }
      );

      let res = await response.json();
      // if (res && res.message) {
      //   console.log('res.message', res.message);
      // }
      return res && res.success;
    } catch (err: any) {
      console.error('error', err);
      return false;
    }
  }
}
