import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { environment } from 'projects/config/environments/environment';
import { ExamService } from '../exam.service';

@Component({
  selector: 'submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.css'],
})
export class SubmitComponent implements OnInit {
  //@Input() submitting: boolean = false;
  _submitting: boolean;
  _submittedAsync: boolean = false;
  _errMessage?: string;
  _QAFormatted?: string;
  _reasonCantSubmit = 'Checking for submission eligibility, please wait...';

  // @Output() continueWriting = new EventEmitter<void>();
  // @Output() submit = new EventEmitter<void>();

  // <submit *ngIf="_confirm" [submitting]="_submitting" [submittedAsync]="_submittedAsync" [QAFormatted]="_QAFormatted"
  //   (continueWriting)="updateSecondsLeft() && _confirm = false" (submit)="submit()"></submit>

  constructor(
    private _examService: ExamService,
    private _router: Router,
    protected sanitizer: DomSanitizer
  ) {
    this._submitting = false;
    this._submittedAsync = false;
  }
  ngOnInit(): void {
    this._QAFormatted = 'Refreshing summary, please wait...';
    this._examService
      .getExamSummaryHtml()
      .then((html) => (this._QAFormatted = html))
      .then(() => {
        return this._examService.getWhyCantSubmit();
      })
      .then((reason) => {
        this._reasonCantSubmit = reason;
      });
  }

  onContinueWriting() {
    this._examService.updateSecondsLeft();
    if (this._examService.getExam().isPA2()) {
      this._router.navigate([`/pa`]);
    } else {
      this._router.navigate([
        `/${this._examService.getCurrentQuestion().ordinal}`,
      ]);
    }
  }

  onSubmit() {
    this._submitting = true;
    this._errMessage = undefined;

    this._examService.submitTest().subscribe((result) => {
      if (result.success) {
        if (result.async_submit) {
          this.onSubmitAsync();
          setTimeout(function () {
            window.location.href = `${
              environment.devWebApiUrl ?? ''
            }/test/instances`;
          }, 5000);
        } else {
          this.onSubmitSuccess();
          window.location.href = `${
            environment.devWebApiUrl ?? ''
          }/test/instances`;
        }
      } else {
        this._errMessage = result.error;
      }
    });
  }

  onSubmitSuccess() {
    this._submitting = false;
  }

  onSubmitAsync() {
    this._submittedAsync = true;
  }
}
