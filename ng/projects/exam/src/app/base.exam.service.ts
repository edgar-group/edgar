import { Injectable } from '@angular/core';
import { IExamService } from 'projects/common/models/IExamService';
import { ExamService } from './exam.service';

@Injectable({
  providedIn: 'root',
})
export class BaseExamService implements IExamService {
  constructor(private _service: ExamService) {}
  examLoaded(): boolean {
    return this._service.examLoaded();
  }
  getExam() {
    return this._service.getExam();
  }
  devlogin() {
    return this._service.devlogin();
  }
}
