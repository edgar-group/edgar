import {
  Component,
  EventEmitter,
  Input,
  NgModule,
  OnInit,
  Output,
} from '@angular/core';
import { Exam } from '../../../../models/exam/exam';
import { Question } from 'projects/common/models/question';
import { ExamService } from '../exam.service';

@Component({
  selector: 'peer-assessment',
  templateUrl: './peer-assessment.component.html',
  styleUrls: ['./peer-assessment.component.css'],
})
export class PeerAssessmentComponent implements OnInit {
  exam!: Exam;
  questions!: Question[];

  @Output() answerUpdated = new EventEmitter();
  @Output() answerEvaluated = new EventEmitter();

  constructor(private _examService: ExamService) {}

  async ngOnInit() {
    this.exam = this._examService.getExam();
    this.questions = await this._examService.getAllQuestions();
  }
  // TODO igor any
  onAnswerUpdated($event: any, ordinal: number) {
    $event = $event.$event;
    this.answerUpdated.emit({ $event, ordinal });
  }

  onAnswerEvaluated($event: any) {
    this.answerEvaluated.emit($event);
  }
}
