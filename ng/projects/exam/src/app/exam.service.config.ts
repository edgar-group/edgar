import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const EXAM_SERVICE_CONFIG = new InjectionToken<ExamServiceConfig>(
  'exam.service.config'
);

export interface ExamServiceConfig {
  getQuestionUrl: string;
  getExamInstanceUrl: string;
  submitExamInstanceUrl: string;
  getScoreboardUrl: string;
  startTheClock: string;
}

export const DEFAULT_EXAM_CONFIG: ExamServiceConfig = {
  getQuestionUrl: `${environment.devWebApiUrl ?? ''}/exam/run/question`,
  getExamInstanceUrl: `${environment.devWebApiUrl ?? ''}/exam/run/instance`,
  submitExamInstanceUrl: `${environment.devWebApiUrl ?? ''}/exam/run/submit`,
  getScoreboardUrl: `${environment.devWebApiUrl ?? ''}/exam/run/scoreboard`,
  startTheClock: `${environment.devWebApiUrl ?? ''}/exam/run/startTheClock`,
};
