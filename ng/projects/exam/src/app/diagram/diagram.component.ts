import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { Answer } from 'projects/common/models/answer';
import { Question } from 'projects/common/models/question';
import { DiagramService } from 'projects/common/services/diagram.service';

@Component({
  selector: 'diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css'],
})
export class DiagramComponent implements AfterViewInit {
  @Input() currQuestion!: Question;
  @Input() currAnswer!: Answer;
  @Input() cmSkin!: string;

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();

  @ViewChild('mermaidOutput') mermaidOutputRef!: ElementRef<HTMLDivElement>;

  public saved: boolean;

  constructor(
    private _ref: ChangeDetectorRef,
    private _diagramService: DiagramService
  ) {
    this.saved = true;
  }

  ngAfterViewInit(): void {
    this.renderMermaid();
  }

  checkSetDefaultAnswer() {
    if (!this.currAnswer.textAnswer || !this.currAnswer.textAnswer.trim()) {
      // to avoid ExpressionChangedAfterItHasBeenCheckedError, bcs I dont have a separate local var for the answer
      setTimeout(() => {
        this.currAnswer.textAnswer =
          this.currQuestion.initialDiagramAnswer || '\n\n\n\n\n';
          this._ref.markForCheck();
      }, 0);
    }
  }
  ngOnChanges(): void {
    this.saved = false;
    this.checkSetDefaultAnswer();
  }

  save() {
    // Will always save the entire contents, not just the selected part (that might come through the $event, ie runHotKey event)
    this.answerSaved.emit(); // this.currAnswer
    console.log('Lying here, this should be async....');
    this.saved = true;
  }

  clear() {
    this.saved = false;
    this.currAnswer.textAnswer = '\n\n\n\n\n';
    this.renderMermaid();
    this.answerUpdated.emit(this.currAnswer);
  }

  reset() {
    this.saved = false;
    this.currAnswer.textAnswer = this.currQuestion.initialDiagramAnswer;
    this.renderMermaid();
    this.answerUpdated.emit(this.currAnswer);
  }

  onTextChanged($event: string) {
    // on enter, this is invoked twice, so the second one will be ignored
    if ($event !== this.currAnswer.textAnswer) {
      this.saved = false;
      this.currAnswer.textAnswer = $event;
      this.renderMermaid();
      this.answerUpdated.emit(this.currAnswer);
    }
  }

  getCurrText() {
    return this.currAnswer.textAnswer;
  }


  async renderMermaid(): Promise<void> {
    await this._diagramService.renderToNativeElement(this.getCurrText(), this.mermaidOutputRef.nativeElement);
  }
}
