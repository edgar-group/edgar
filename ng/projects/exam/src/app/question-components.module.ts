import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionFreeTextComponent } from './question-free-text/question-free-text.component';
import { ScaleItemsComponent } from './scale-items/scale-items.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { AbcComponent } from './abc/abc.component';
import { PeerAssessmentComponent } from './peer-assessment/peer-assessment.component';
import { SharedModule } from '../../../common/components/shared.module';
import { QuestionContentComponent } from './question-content/question-content.component';
import { HeaderNavigationComponent } from './header/header.component';
import { QuestionNavigationComponent } from './question-navigation/question-navigation.component';
import { QuestionContentStaticComponent } from './question-content-static/question-content-static.component';
import { QuestionTrueFalseComponent } from './question-true-false/question-true-false.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { OrderedElementsComponent } from './question-ordered-elements/question-ordered-elements.component';
import { ConnectedElementsComponent } from './connected-elements/connected-elements.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DiagramComponent } from './diagram/diagram.component';

const COMPONENTS = [
  QuestionFreeTextComponent,
  QuestionContentComponent,
  QuestionContentStaticComponent,
  ScaleItemsComponent,
  FileUploadComponent,
  AbcComponent,
  PeerAssessmentComponent,
  HeaderNavigationComponent,
  QuestionNavigationComponent,
  QuestionTrueFalseComponent,
  OrderedElementsComponent,
  ConnectedElementsComponent,
  DiagramComponent,
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [CommonModule, SharedModule, DragDropModule, ReactiveFormsModule],
  exports: [COMPONENTS],
})
export class QuestionComponentsModule {}
