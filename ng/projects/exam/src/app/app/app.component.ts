import {
  Component,
  HostListener,
  OnInit,
  ChangeDetectorRef,
  AfterViewChecked,
  AfterViewInit,
} from '@angular/core';
import { LogService } from '../../../../common/services/log.service';
import { ExamService } from '../exam.service';
import { LogEvent } from '../../../../common/models/log-event';
import { environment } from 'projects/config/environments/environment';
import * as $ from 'jquery';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXAM_CODERUNNER_SERVICE_CONFIG,
} from '../../../../config/code-runner.config';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Params,
  Router,
} from '@angular/router';

@Component({
  selector: 'app',
  host: {
    '(window:focus)': 'onFocus()',
    '(window:blur)': 'onBlur()',
  },
  templateUrl: `app.component.html`,
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
    },
  ],
})
export class AppComponent implements OnInit, AfterViewInit {
  _intialQuestionLoaded: boolean = false;
  _examLoaded: boolean = false;
  _abortMessage: string = '';
  _focusCounter: number = 0;
  constructor(
    private _examService: ExamService,
    private _logService: LogService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    _logService.getServerCommandObservable().subscribe((cmd) => {
      if (cmd.command === 'abort') {
        this._abortMessage = cmd.message;
      }
    });
  }

  collectRouteParams(router: Router): any {
    let params = {};
    let stack: ActivatedRouteSnapshot[] = [router.routerState.snapshot.root];
    while (stack.length > 0) {
      const route = stack.pop()!;
      params = { ...params, ...route.params };
      stack.push(...route.children);
    }
    return params;
  }
  public async ngOnInit() {
    if (!environment.production && location.port == '4200') {
      console.log('Dev mode -> dummy login to last started exam!');
      await this._examService.devlogin();
    }
    let exam = await this._examService.loadExam();
    if (exam == null) {
      alert('exam is null!?');
    } else {
      this._examLoaded = true;
      if (this._examService.getExam().isPA2()) {
        await this._examService.loadAllQuestions();
        this._router.navigate(['/pa'], {
          relativeTo: this._route,
        });
      } else {
        let routeParams = this.collectRouteParams(this._router);
        let initialOrdinal = exam.forward_only
          ? exam.last_ordinal
          : routeParams.id || 1;

        if (
          +initialOrdinal > +this._examService.getExam().noOfQuestions ||
          +initialOrdinal < 1
        ) {
          console.log('initialOrdinal overflow, back to 1.', initialOrdinal);
          initialOrdinal = 1;
        }
        await this._examService.setCurrentQuestionOrdinal(initialOrdinal);
        this._router.navigate([initialOrdinal], {
          relativeTo: this._route,
        });
      }
      this._intialQuestionLoaded = true;
    }
    setInterval(() => {
      this._logService.hb();
    }, 60 * 1000);
    this._logService.log(
      new LogEvent(
        'Application constructed.',
        JSON.stringify(this.getClientInfo())
      )
    );
  }
  public async ngAfterViewInit() {
    await this.checkClipboard();
  }
  private async checkClipboard() {
    window.focus();
    // first check for cheating
    if (navigator.clipboard && navigator.clipboard.readText) {
      try {
        let text = await navigator.clipboard.readText();
        console.log('Clipboard text:', text);
        if (text && text.length > 1000) {
          const len = text.length;
          console.error(
            'Clipboard text suspiciously  long (' + len + ' chars), dumping...'
          );
          console.error(text);
          text = text.substring(0, 1000) + '...';
          this._logService.log(
            new LogEvent(
              'Large clipboard content (cheating?)',
              'First 1000 of ' +
                len +
                ' chars (whole text is dumped to console at the client): ' +
                text
            )
          );
        }
      } catch (error: any) {
        this._logService.log(
          new LogEvent(
            'Error reading clipboard content (cheating?)',
            error.message
          )
        );
        console.error('Error reading clipboard', error);
      }
    } else {
      this._logService.log(
        new LogEvent(
          'Clipboard API not supported',
          'Can not check clipboard for cheating :-(.'
        )
      );
    }
    // then clear clipboard
    if (navigator.clipboard) {
      try {
        console.log('Clearing clipboard');
        await navigator.clipboard.writeText('');
        console.log('Cleared clipboard');
      } catch (error: any) {
        this._logService.log(
          new LogEvent('Error clearing clipboard content', error.message)
        );
        console.error('Error clearing clipboard', error);
      }
    } else {
      this._logService.log(
        new LogEvent(
          'Clipboard API not supported',
          'Can not clear clipboard :-(.'
        )
      );
    }
  }
  private getClientInfo() {
    try {
      return {
        timeOpened: new Date(),
        timezone: new Date().getTimezoneOffset() / 60,
        platform: navigator?.platform || 'unknown',
        pageon: window.location.pathname,
        referrer: document.referrer,
        browserName: navigator.appName,
        browserEngine: navigator.product,
        browserVersion1a: navigator.appVersion,
        browserVersion1b: navigator.userAgent,
        browserLanguage: navigator.language,
        browserOnline: navigator.onLine,
        browserPlatform: navigator.platform,
        sizeScreenW: screen.width,
        sizeScreenH: screen.height,
        sizeInW: innerWidth,
        sizeInH: innerHeight,
        sizeAvailW: screen.availWidth,
        sizeAvailH: screen.availHeight,
        scrColorDepth: screen.colorDepth,
        scrPixelDepth: screen.pixelDepth,
      };
    } catch (err) {
      return {
        warning: 'Error getting user info',
      };
    }
  }

  // //event from header
  // onShowScoreboard() {
  //   this._showScoreboard = !this._showScoreboard;
  // }

  onFocus() {
    this._focusCounter++;
    this._logService.log(
      new LogEvent('Got focus', 'Focus counter: ' + this._focusCounter)
    );
    // this event is not reliable, it does not fire when the app is started
    // if (this._focusCounter === 1) {
    //   this.checkClipboard();
    // }
  }

  onBlur() {
    this._logService.log(
      new LogEvent(
        'Lost focus',
        'Warning - student could be cheating (alt+tab)?'
      )
    );
  }
}
