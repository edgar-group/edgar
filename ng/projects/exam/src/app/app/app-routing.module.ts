import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'projects/common/components/page-not-found-component/page-not-found.component';
import { ExamComponent } from '../exam/exam.component';
import { PeerAssessmentComponent } from '../peer-assessment/peer-assessment.component';
import { SubmitComponent } from '../submit/submit.component';

const routes: Routes = [
  { path: '', component: ExamComponent, pathMatch: 'full' },
  { path: 'submit', component: SubmitComponent },
  { path: 'pa', component: PeerAssessmentComponent },
  { path: ':id', component: ExamComponent },
  { path: '**', component: PageNotFoundComponent }, // Wildcard route for a 404 page
];
// const routes: Routes = [
//   { path: '', redirectTo: '/1', pathMatch: 'full' },
//   { path: ':ordinal', component: AppComponent },
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
