import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of } from 'rxjs';
import { Feedback } from '../components/feedback/feedback';

@Injectable()
export class FeedbackService {
  private _feedbackHistory: Feedback[] = [];

  constructor(private _http: HttpClient) {}

  get(feedback: Feedback) {
    let previousFeedbacks = this._feedbackHistory.filter(
      (fb) =>
        fb.exerciseId == feedback.exerciseId &&
        fb.exerciseQuestionOrdinal == feedback.exerciseQuestionOrdinal &&
        fb.tutorialId == feedback.tutorialId &&
        fb.tutorialStepId == feedback.tutorialStepId
    );

    if (previousFeedbacks.length) {
      // console.log('using previous feedbacks');
      return of(previousFeedbacks);
    } else {
      // console.log('getting previous feedbacks from api');
      return this._http
        .get<any>(
          `/feedback/api?eid=${feedback.exerciseId}&eqo=${feedback.exerciseQuestionOrdinal}&tid=${feedback.tutorialId}&tsid=${feedback.tutorialStepId}`
        )
        .pipe(
          map((res) => {
            const feedbacks = res;
            this._feedbackHistory.push(feedbacks);
            return feedbacks;
          })
        );
    }
  }

  send(feedback: Feedback): Observable<any> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this._http.post(`/feedback/api`, JSON.stringify(feedback), {
      headers: headers,
    });
  }
}
