import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import mermaid from 'mermaid';

@Injectable({
  providedIn: 'root',
})
export class DiagramService {
  private renderer: Renderer2;
  constructor(rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }
  public async getInjectSvgDiagrams(html: string): Promise<string> {
    let newHtml = html;
    var matches = html.match(
      /(<!--\s*(diagram|inline-diagram).*-->)((?!\/(diagram|inline-diagram))[^])*<!--\s*\/(diagram|inline-diagram)\s*-->/gi
    );
    if (matches && matches.length) {
      for (let i = 0; i < matches.length; i++) {
        try {
          let graphDef = ('' + matches[i])
            .replace(/(<!--\s*(diagram|inline-diagram).*-->)/gi, '')
            .replace(/(<!--\s*\/(diagram|inline-diagram).*-->)/gi, '');
          let id = (new Date().getMilliseconds() + '-' + Math.random()).replace(
            '.',
            ''
          );
          const { svg } = await mermaid.render('graph' + id, graphDef);

          if (matches[i].indexOf('inline-diagram') > 0) {
            newHtml = newHtml.replace(matches[i], svg);
          } else {
            newHtml = newHtml.replace(
              matches[i],
              `<div id="svg${id}" class="mermaid-centered">${svg}</div>`
            );
          }
        } catch (error) {
          console.error('diagram error', error);
          newHtml = newHtml.replace(
            matches[i],
            `<div class="text-danger border rounded m-3">${error}</div>`
          );
        }
      }
    }
    return newHtml;
  }

  public async renderToNativeElement(
    diagramDefiniton: string,
    element: any,
    elementId?: string
  ): Promise<void> {
    if (diagramDefiniton.trim() === '') {
      this.renderer.setProperty(
        element,
        'innerHTML',
        '(nothing to render ¯_(ツ)_/¯)'
      );
    } else {
      try {
        await mermaid.parse(diagramDefiniton);
      } catch (e: any) {
        const error: Error = e;
        console.error(error);
        let message = JSON.stringify(error);
        this.renderer.setProperty(
          element,
          'innerHTML',
          '<pre>Error rendering diagram: \n' + message + '</pre>'
        );
        return;
      }

      try {
        const { svg } = await mermaid.render(
          elementId ? elementId : ('mermaidOutput-' + Math.random()).replace('.', ''),
          diagramDefiniton
        );
        this.renderer.setProperty(element, 'innerHTML', svg);
      } catch (error) {
        console.error('diagram error', error);
        this.renderer.setProperty(
          element,
          'innerHTML',
          `<div class="text-danger border rounded m-3">${error}</div>`
        );
      }
    }
  }
}
