import { Inject, Injectable, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { firstValueFrom, map, Observable, Subject } from 'rxjs';
import { LogServiceConfig, LOG_SERVICE_CONFIG } from '../../config/log.config';
import { LogEvent } from '../models/log-event';
import { ServerCommand } from '../models/ServerCommand';

declare var Swal: any;

@Injectable({
  providedIn: 'root',
})
export class LogService {
  private _failedLogQueue: Array<LogEvent>;
  private _networkFailCounter: number;
  public networkFailCounterObservable = new Subject<number>();
  private _serverCommandObservable = new Subject<ServerCommand>();
  constructor(
    private _http: HttpClient,
    @Inject(LOG_SERVICE_CONFIG) private _config: LogServiceConfig
  ) {
    this._failedLogQueue = [];
    this._networkFailCounter = 0;
  }

  async log(event: LogEvent) {
    let toLog;
    if (this._failedLogQueue.length === 0) {
      toLog = {
        entries: [event],
      };
    } else {
      this._failedLogQueue.forEach((element) => {
        element.retry++;
      });
      toLog = {
        entries: this._failedLogQueue.concat([event]),
      };
      this._failedLogQueue = [];
    }
    try {
      let response = await firstValueFrom(
        this._http.post(this._config.logUrl, toLog, {
          withCredentials: true,
        })
      );
      if (response && response['command']) {
        this._serverCommandObservable.next(
          new ServerCommand(response['command'])
        );
      }
    } catch (error) {
      this._failedLogQueue = toLog.entries;
      console.error('Failed to log:', toLog);
      console.error('Error:', error);
    }
  }
  getServerCommandObservable(): Observable<ServerCommand> {
    return this._serverCommandObservable;
  }
  logLecture(event: LogEvent) {
    var headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    //console.log('saving event:', event.eventName);
    this._http
      .post(`/lecture/log`, JSON.stringify({ e: event }), {
        headers: headers,
      })
      .subscribe((res) => {
        //console.log('Server (log) returned: ',  res.json());
      });
  }

  // heartbeat
  hb() {
    this._http
      .post<any>(
        this._config.hbUrl,
        '"Trying is the 1st step towards failure." H.Simpson',
        { withCredentials: true }
      )
      .subscribe(
        (res) => {
          if (res.error) {
            Swal.fire({
              title: 'Error on heartbeat',
              icon: 'error',
              html: res.error,
              showCloseButton: true,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
              confirmButtonAriaLabel: 'OK',
            });
          } else if (res.msg) {
            Swal.fire({
              title: res.msg.message_title,
              icon: 'info',
              html: res.msg.message_body,
              showCloseButton: true,
              showCancelButton: false,
              focusConfirm: false,
              confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
              confirmButtonAriaLabel: 'OK',
            });
            console.log('Exam message title: ', res.msg.message_title);
            console.log('Exam message: ', res.msg.message_body);
            this.log(new LogEvent('Displayed exam message.', res.msg.id));
          }
        },
        (error) => {
          console.log('HB', error);
          this._networkFailCounter++;
          this.networkFailCounterObservable.next(this._networkFailCounter);
        }
      );
  }
}
