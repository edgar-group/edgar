import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpClient,
  HttpErrorResponse,
} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
// import { ExamService } from 'projects/exam/src/app/exam.service';
import { environment } from 'projects/config/environments/environment';
import {
  catchError,
  firstValueFrom,
  from,
  map,
  Observable,
  of,
  mergeMap,
} from 'rxjs';
import { IExamService, I_EXAM_SERVICE_TOKEN } from '../models/IExamService';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(
    private _http: HttpClient,
    @Inject(I_EXAM_SERVICE_TOKEN) private _examService: IExamService
  ) {}

  private cookie: any;

  getAuth() {
    if (!!this.cookie) {
      console.log('cookie SET, resolving!');
      return from(Promise.resolve('igor'));
    } else {
      console.log('cookie not set, posting...');
      var formData: any = new FormData();
      formData.append('username', 'nk51530@fer.hr');
      formData.append('password', 'nk51530@fer.hr@node.js');
      return this._http
        .post(`${environment.devWebApiUrl ?? ''}/auth/ngdevlogin`, formData, {
          withCredentials: true,
          responseType: 'text',
          observe: 'response',
        })
        .pipe(
          map((response) => {
            console.log('simple login response', response);
            this.cookie = 'xxx';
            return 'asdas';
          })
        );
    }
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // console.log(
    //   'intercepting request',
    //   this._examService.examLoaded(),
    //   this._examService
    // );
    if (
      this._examService.examLoaded()
    ) {
      const clonedReq = request.clone({
        params: request.params.set(
          'idti',
          this._examService.getExam().idTestInstance
        ),
      });
      return next.handle(clonedReq);
    } else {
      return next.handle(request);
    }
    // if (request.url.indexOf('auth/ngdevlogin') >= 0){
    //     console.log('NOT intercepting', request);

    // }
    // else {
    //   console.log('INTERCEPTing', request);
    //   return this.getAuth().pipe(
    //     mergeMap((token: string) => {
    //       request = request.clone({
    //         setHeaders: {
    //           Authorization: `Bearer${token}}`,
    //         },
    //       });
    //       return next.handle(request);
    //       // new
    //     })
    //   );
    // }
  }

  // intercept(req: HttpRequest<any>, next: HttpHandler) {
  //   console.log('Request is on its way', req);
  //   if (!environment.production) {
  //       //return from(this.authenticate(req, next))
  //       this.authenticate(req, next).then((r1) => {

  //       }).finally(() => {
  //         return next.handle(req);
  //       }

  //       );

  //   } else {
  //     return next.handle(req);
  //   }
  // }
  //   intercept2(req: HttpRequest<any>, next: HttpHandler) {
  //     // convert promise to observable using 'from' operator
  //     return from(this.handle(req, next))
  //   }

  //   async handle(req: HttpRequest<any>, next: HttpHandler) {
  //     // if your getAuthToken() function declared as "async getAuthToken() {}"
  //     await this.auth.getAuthToken()

  //     // if your getAuthToken() function declared to return an observable then you can use
  //     // await this.auth.getAuthToken().toPromise()

  //     const authReq = req.clone({
  //       setHeaders: {
  //         Authorization: authToken
  //       }
  //     })

  //     // Important: Note the .toPromise()
  //     return next.handle(authReq).toPromise()
  //   }
  //   authenticate(req: HttpRequest<any>, next: HttpHandler) {
  //     //return firstValueFrom(next.handle(req));
  //     return Promise.resolve(req);
  //   }
  //   async authenticate2(req: HttpRequest<any>): Promise<HttpRequest<any>> {
  //     if (!!this.cookie) {
  //       return Promise.resolve(req);
  //     } else {
  //         var formData: any = new FormData();
  //         formData.append('username', 'nk51530@fer.hr');
  //         formData.append('password', 'nk51530@fer.hr@node.js');
  //         return firstValueFrom(
  //           this._http
  //             .post(
  //               `${environment.devWebApiUrl ?? ''}/auth/ngdevlogin`,
  //               formData,
  //               {
  //                 withCredentials: true,
  //                 observe: 'response',
  //               }
  //             )
  //             .pipe(
  //               map((response) => {
  //                 console.log('simple login response', response);
  //                 return req;
  //               })
  //             )
  //         );
  //     }
  //   }
}
