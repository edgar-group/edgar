import { InjectionToken } from '@angular/core';

export const I_EXAM_SERVICE_TOKEN = new InjectionToken<IExamService>(
  'i.exam.service'
);

export interface IExamService {
  examLoaded(): boolean;
  getExam();
  devlogin();
}
