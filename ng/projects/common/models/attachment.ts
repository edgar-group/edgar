export interface Attachment {
  filename: string;
  label: string;
  is_public: boolean;
}
