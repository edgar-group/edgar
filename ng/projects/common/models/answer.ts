export class Answer {
  type: string;
  multichoice: number[] = [];
  // orderingElements: number[] = [];
  answersStatic: any[] = [];
  answersDynamic: any[] = [];
  // textCompletionCorrectAnswers: string[] = [];
  codeAnswer: CodeAnswer = new CodeAnswer();
  textAnswer: string = '\n\n\n\n\n';
  constructor(type: string, codeAnswer?: CodeAnswer) {
    this.type = type;
    this.codeAnswer = codeAnswer ?? new CodeAnswer();
  }
}

export class CodeAnswer {
  code!: string;
  languageId!: number;

  constructor(code?: string | undefined, id?: number | undefined) {
    this.code = code ?? '';
    this.languageId = id ?? 0;
  }
}
