import { ProgrammingLanguage } from './programming-language';

export class CodeRunnerQuestion {
  public runs?: number;
  constructor(
    public ordinal: number, // composite: step*1000 + crordinal
    public idCodeRunner: number,
    public programmingLanguages: ProgrammingLanguage[]
  ) {}
}
