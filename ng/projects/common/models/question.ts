import { Attachment } from './attachment';
import { ProgrammingLanguage } from './programming-language';
import { ScaleItem } from './scale-item';
import { Answer } from './answer';
import { QuestionInfo } from './QuestionInfo';
import { BehaviorSubject } from 'rxjs';

export class Question {
  public uploadNumbers?: number[];
  public uploadFileNames?: string[];
  public uploaded?: boolean[];
  public runs?: number;
  private _isClockTicking;
  private _timerSubject: BehaviorSubject<number> | undefined = undefined;
  constructor(
    public id: number,
    public caption: string,
    public ordinal: number,
    public currAnswers: number[],
    public content: string,
    public answers: any, // TODO igor
    public answersStatic: any,
    public answersDynamic: any,
    public initialDiagramAnswer: string,
    public multiCorrect: boolean,
    public modeDesc: string,
    public type: string,
    public programmingLanguages: ProgrammingLanguage[],
    public attachments: Attachment[],
    public scaleItems: ScaleItem[],
    public uploadFilesNo: number,
    public requiredUploadFilesNo: number,
    public tempUploadedFileNames: string[],
    public displayOption: string,
    public gradingModelName: string,
    public timeLeft: number | null,
    public timeLimit: number | null
  ) {
    if (!!timeLimit && !!timeLeft && timeLeft < timeLimit) {
      this.startTheClock(null); // this wuill start the clock ticking and set the _isClockTicking flag
    } else {
      this._isClockTicking = false;
    }
  }

  public isSQL(): boolean {
    return QuestionInfo.isSQL(this);
  }
  public isScaleQuestion(): boolean {
    return QuestionInfo.isScaleQuestion(this);
  }
  public isCodeLang(): boolean {
    return QuestionInfo.isCodeLang(this);
  }
  public isJSON(): boolean {
    return QuestionInfo.isJSON(this);
  }
  public isRunnable(): boolean {
    return QuestionInfo.isRunnable(this);
  }
  public isFreeText(): boolean {
    return QuestionInfo.isFreeText(this);
  }
  public isComplex(): boolean {
    return QuestionInfo.isComplex(this);
  }
  public isMultiChoice(): boolean {
    return QuestionInfo.isMultiChoice(this);
  }
  public isTrueFalse(): boolean {
    return QuestionInfo.isTrueFalse(this);
  }
  public isOrderedElements(): boolean {
    return QuestionInfo.isOrderedElements(this);
  }
  public isConnectedElements(): boolean {
    return QuestionInfo.isConnectedElements(this);
  }
  public isTextCompletion(): boolean {
    return QuestionInfo.isTextCompletion(this);
  }
  public isDiagram(): boolean {
    return QuestionInfo.isDiagram(this);
  }

  initUploadVars(): boolean {
    if (!(this.uploadNumbers && this.uploadNumbers.length)) {
      this.uploadNumbers = Array(this.uploadFilesNo)
        .fill(0)
        .map((x, i) => i);
      //this.uploadFileNames = Array(uploadFileNo).fill('');
      this.uploaded = this.tempUploadedFileNames.map((x) => !!x);
      return true;
    } else {
      return false;
    }
  }

  getWhyCantSubmit(): string {
    if (
      this.uploadFilesNo &&
      this.getUploadedFilesCount() < this.requiredUploadFilesNo
    ) {
      return (
        'Please attach all required files for the queston ' +
        this.ordinal +
        ' before submitting!'
      );
    } else {
      return '';
    }
  }
  // private supportsUpload(): boolean {
  //   return (
  //     this.uploadFilesNo > 0 &&
  //     !QuestionInfo.isMultiChoice(this) &&
  //     !QuestionInfo.isScaleQuestion(this)
  //   );
  // }

  htmlEncode(str: string): string {
    return String(str)
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;');
  }

  getQAFormatted(answers: Answer): string {
    let content = '(unanswered)';
    if (answers) {
      if (this.type === 'Scale (questionnaire)') {
        let scaleItems = this.scaleItems;
        content = answers.multichoice
          .map(function (num: number) {
            let sameValueItems = scaleItems.filter((item) => item.value == num);
            if (sameValueItems.length > 1) {
              alert('Multiple scale items with the same value!?');
            }
            let label =
              sameValueItems && sameValueItems.length > 0
                ? sameValueItems[0].label
                : 'Error!';
            return `<h2 class="text-left badge badge-success ml-5">
                                    ${label}
                                  </h2>`;
          })
          .join();
      } else if (this.isOrderedElements()) {
        let answersText = this.answers;
        content = '';
        for (let i = 0; i < answers.multichoice.length; i++) {
          content += `<h2 class="mr-2"><div style="min-height:2rem" class="edgar-answer-badge badge badge-primary">${
            answersText[answers.multichoice[i] - 1].aHtml
          }</div></h2>`;
        }
        content = `<div class="d-flex">${content}</div>`;
      } else if (this.isConnectedElements() && answers.multichoice.length) {
        // let answersStaticText = this.answersStatic;
        content = '';

        for (let i = 0; i < this.answersStatic.length; i++) {
          content += `<div class="row">
            <div class="col-5 offset-1 border border-secondary rounded">${
              this.answersStatic[i].aHtml
            }</div>
            <div class="col-5 border border-secondary rounded">${
              answers.multichoice[this.answersStatic[i].ordinal - 1] > 0
                ? this.answersDynamic[
                    answers.multichoice[this.answersStatic[i].ordinal - 1] - 1
                  ].aHtml
                : '(empty)'
            }</div>
            </div>`;
        }
      } else if (this.isTextCompletion()) {
        // TODO iako se ni sad ne moze editirat izgleda kao moze
        content = this.answers[0].aHtml;
      } else if (answers.multichoice && answers.multichoice.length) {
        let answersText = this.answers;
        content = answers.multichoice
          .map(function (num: number) {
            return (
              '<h2><div class="edgar-answer-badge badge badge-primary">' +
              String.fromCharCode(97 - 1 + parseInt('' + num)) +
              '</div></h2>' +
              `<div class="text-left" style="margin-left: 50px;">
                                    ${answersText[num - 1].aHtml}
                                  </div>  `
            );
          })
          .join();
      } else if (
        answers.codeAnswer &&
        answers.codeAnswer.code &&
        answers.codeAnswer.code.trim() !== ''
      ) {
        content = `<pre>${this.htmlEncode(answers.codeAnswer.code)}</pre>`;
      } else if (answers.textAnswer && answers.textAnswer.trim() !== '') {
        content = `<pre>${this.htmlEncode(answers.textAnswer)}</pre>`;
      }
    }
    let files = '';
    // console.log('this.tempUploadedFileNames', this.tempUploadedFileNames);
    if (this.uploadFilesNo > 0) {
      let badges: Array<string> = [];

      for (let i = 0; i < this.uploadFilesNo; ++i) {
        let filename = this.tempUploadedFileNames[i];
        if (filename) {
          badges.push(
            `<span class="badge badge-primary large-text">${filename}</span>`
          );
        } else {
          const cssClassName =
            this.getUploadedFilesCount() < this.requiredUploadFilesNo
              ? 'badge-danger'
              : 'badge-warning';
          badges.push(
            `<span class="badge ${cssClassName} large-text">Missing attachment</span>`
          );
        }
      }
      files = `<div class="row">
                        <div class="col-12">
                            <b>Attached files${
                              this.requiredUploadFilesNo
                                ? ' (' +
                                  this.requiredUploadFilesNo +
                                  ' required)'
                                : ''
                            }:</b>
                            ${badges.join(' ')}
                        </div>
                    </div>`;
    }
    return `<div class="container p-3 mb-1 border border-secondary rounded">
        <div class="row">
           <div class="col-1">
           <h2><div class="aii-answer-badge badge badge-secondary">${this.ordinal}</div></h2>
           </div>
           <div class="col-11 submit-question-content">
                          ${this.content}
           </div>
        </div>
        <div class="row mt-3">
           <div class="col-1">
            <h5>My answer:</h5>
           </div>
           <div class="col-11">
                ${content}
           </div>
        </div>
        ${files}
      </div>`;
  }
  getUploadedFilesCount(): number {
    return this.uploaded ? this.uploaded.filter((x) => !!x).length : 0;
  }
  isVisible(): boolean {
    return !this.isTimedOut() && !this.isInTimeLimitWarningMode();
  }
  isTimedOut(): boolean {
    return !!this.timeLimit && (this.timeLeft ?? 0) <= 0;
  }
  isInTimeLimitWarningMode(): boolean {
    return !!this.timeLimit && !this._isClockTicking && !this.isTimedOut();
  }
  isClockTicking(): boolean {
    return this._isClockTicking;
  }

  startTheClock(listener): void {
    if ((this.timeLeft ?? 0) > 0 && !this._isClockTicking) {
      this._isClockTicking = true;
      this._timerSubject = new BehaviorSubject<number>(this.timeLeft ?? 0);
      const intervalId = setInterval(() => {
        if (this._timerSubject) {
          this.timeLeft = this._timerSubject.getValue() - 1;
          if (this.timeLeft >= 0) {
            this._timerSubject.next(this.timeLeft);
          } else {
            clearInterval(intervalId);
            this._timerSubject.complete();
            this._isClockTicking = false;
          }
        }
      }, 1000);
      if (listener) this._timerSubject.subscribe(listener);
    }
  }
  // this is so that the UI can attache itself to the running clocks after the student refreshes the page while the clock is ticking
  setTimerListener(listener): void {
    // Hate this lib, they have deprecated the observers and now there is no way to find out if there are any observers, WTF rxjs!?
    if (this._timerSubject && this._timerSubject.observers.length === 0) {
      this._timerSubject.subscribe(listener);
    }
  }
}
