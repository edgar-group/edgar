export interface ScaleItem {
  label: string;
  value: number;
}
