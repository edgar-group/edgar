export class ServerCommand {
  command: 'abort' | 'warn';
  message: string;
  // constructor(command: 'abort' | 'warn', message: string) {
  //   this.command = command;
  //   this.message = message;
  // }
  constructor(commandObject) {
    this.command = commandObject['command'];
    this.message = commandObject['message'];
  }
}
