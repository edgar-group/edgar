interface IHasType {
  type: string;
}

export class QuestionInfo {
  // TODO: deduplicate these methods with question-review

  public static isSQL(qa: IHasType): boolean {
    // arg can be both question and answer
    return (qa.type || '').toUpperCase().indexOf('SQL') >= 0;
  }
  public static isScaleQuestion(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('SCALE') >= 0;
  }
  public static isCodeLang(qa: IHasType): boolean {
    return (
      !!qa.type &&
      (qa.type.toUpperCase().indexOf('C-LANG') >= 0 ||
        qa.type.toUpperCase().indexOf('JAVA') >= 0 ||
        qa.type.toUpperCase().indexOf('CODE') >= 0)
    );
  }
  public static isJSON(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('JSON') >= 0;
  }
  public static isRunnable(qa: IHasType): boolean {
    return (
      QuestionInfo.isCodeLang(qa) ||
      QuestionInfo.isSQL(qa) ||
      QuestionInfo.isJSON(qa)
    );
  }
  public static isFreeText(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('FREE TEXT') >= 0;
  }
  public static isComplex(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().startsWith('COMPLEX');
  }
  public static isMultiChoice(qa: IHasType): boolean {
    return (
      (qa.type || '').toUpperCase().indexOf('ABC') >= 0 ||
      (qa.type || '').toUpperCase().indexOf('CHOICE') >= 0
    );
  }
  public static isTrueFalse(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('TRUE/FALSE') >= 0;
  }
  public static isOrderedElements(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('ORDEREDELEMENTS') >= 0;
  }
  public static isConnectedElements(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('CONNECTEDELEMENTS') >= 0;
  }
  public static isTextCompletion(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('TEXT COMPLETION') >= 0;
  }
  public static isDiagram(qa: IHasType): boolean {
    return (qa.type || '').toUpperCase().indexOf('DIAGRAM') >= 0;
  }
}
