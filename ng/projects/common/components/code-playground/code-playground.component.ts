import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
  SimpleChanges,
} from '@angular/core';
import { CodeMirrorComponent } from '../code-mirror/code-mirror.component';
import { CodeRunnerService } from '../../services/code-runner.service';
import { Answer, CodeAnswer } from '../../models/answer';
import { CodeResult } from '../../../models/exam/code-result';
import { ProgrammingLanguage } from 'projects/common/models/programming-language';
import { CodeRunnerQuestion } from 'projects/common/models/code-runner-question';
declare var Swal: any;

@Component({
  selector: 'code-playground',
  templateUrl: './code-playground.component.html',
  styleUrls: ['./code-playground.component.css'],
})
export class CodePlaygroundComponent implements OnInit, OnChanges {
  @ViewChild('codeMirror') codeMirror!: CodeMirrorComponent;
  saved: boolean;

  @Input() cmSkin!: string;
  @Input() currCodeRunner!: CodeRunnerQuestion;
  @Input() currAnswer!: Answer;
  @Input() programmingLanguages!: ProgrammingLanguage[];

  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();
  @Output() codeRun = new EventEmitter();

  stdin: string;
  currResult: any;
  queryRunning: boolean;
  _questionLangCode: any; // Map<number, Map<number, string>> = new Map();
  _errMessage: string;

  constructor(
    private _ref: ChangeDetectorRef,
    private _codeRunnerService: CodeRunnerService
  ) {
    this.saved = true;
    this.currResult = undefined;
    this.queryRunning = false;
    this._errMessage = '';
    this.stdin = '';
    this._questionLangCode = {};
  }

  getResult(): CodeResult {
    return this.currResult;
  }

  getCurrentType(): string {
    return (
      this.currCodeRunner.programmingLanguages.find(
        (x) => x.id == this.getCurrLangId()
      )?.name || this.currCodeRunner.programmingLanguages[0].name
    );
  }

  isSQL(): boolean {
    return !!this.currCodeRunner.programmingLanguages.find(
      (x) => x.name.toUpperCase().indexOf('SQL') >= 0
    );
  }
  isJSON(): boolean {
    return !!this.currCodeRunner.programmingLanguages.find(
      (x) => x.name.toUpperCase().indexOf('JSON') >= 0
    );
  }
  isCodeLang(): boolean {
    return !this.isSQL() && !this.isJSON();
  }

  runAll() {
    this.run(this.getCurrCode());
  }

  run(code: string) {
    var self = this;
    this.save();
    this.queryRunning = true;
    this._errMessage = '';
    this.currResult = undefined;
    this._codeRunnerService
      .getCodeResult(
        this.currCodeRunner.ordinal,
        code,
        '' + this.getCurrLangId(),
        true,
        this.stdin
      )
      .subscribe({
        next: (result) => {
          this.currResult = result;
          this.queryRunning = false;
          this.codeRun.emit(result);
          self._ref.markForCheck();
        },
        error: (e) => {
          this._errMessage = e.error
            ? JSON.stringify(e.error)
            : JSON.stringify(e);
          this.queryRunning = false;
          this.codeRun.emit(JSON.stringify(e));
          self._ref.markForCheck();
        },
      });
  }
  // getPLs() {
  //   return [...this.currCodeRunner.programmingLanguages];
  // }
  save() {
    // Will always save the entire contents, not just the selected part
    // (that might come through the $event, ie runHotKey event)
    this.answerSaved.emit();
    console.log('TODO: lying here, should be async....');
    this.saved = true;
  }

  onCodeChanged(newCode: string) {
    this.saved = false;
    if (
      newCode.replace(/\r\n/g, '\n') !==
      this.currAnswer.codeAnswer.code.replace(/\r\n/g, '\n')
    ) {
      this.currAnswer.codeAnswer.code = newCode;
    }
    this._questionLangCode[this.currAnswer.codeAnswer.languageId] = newCode;
    this.answerUpdated.emit(this.currAnswer);
  }

  onLanguageChanged(newId) {
    this.saved = false;
    this.currAnswer = new Answer(
      'code playground',
      new CodeAnswer(this._questionLangCode[newId], newId)
    );
    this.saved = false;
    //this.answerUpdated.emit(this.currAnswer);
    this.onCodeChanged(this.currAnswer.codeAnswer.code);
  }

  getCurrCode(): string {
    return this.currAnswer.codeAnswer.code;
  }
  getCurrLangId() {
    // console.log('getCurrLangId()', this.currAnswer.codeAnswer);
    if (this.currAnswer.codeAnswer) {
      if (this.currAnswer.codeAnswer.languageId === 0)
        this.currAnswer.codeAnswer.languageId = 0;
      return this.currAnswer.codeAnswer.languageId;
    } else {
      console.error('getCurrLangId() is undefined...');
      return 0;
    }
  }

  checkSetDefaultAnswer() {
    // console.log('checkSetDefaultAnswer()', this.currAnswer.codeAnswer);
    if (!this.currAnswer.codeAnswer) {
      this.currAnswer.codeAnswer = new CodeAnswer(
        '\n\n\n\n\n',
        this.currCodeRunner.programmingLanguages[0].id
      );
    }
    if (!this.currAnswer.codeAnswer.code) {
      this.currAnswer.codeAnswer.code = '\n\n\n\n\n';
    }
    if (
      this.currAnswer.codeAnswer.languageId == 0 &&
      this.currCodeRunner.programmingLanguages.length
    ) {
      this.currAnswer.codeAnswer.languageId =
        this.currCodeRunner.programmingLanguages[0].id;
    }
  }

  ngOnInit() {
    // console.log(
    //   'ngOnInit --------------------->   this.currCodeRunner',
    //   this.currCodeRunner,
    //   this.programmingLanguages,
    //   this.currAnswer.codeAnswer
    // );
    // console.log(
    //   'ngOnInit --------------------->   this.currAnswer',
    //   this.currAnswer
    // );
    if (this.programmingLanguages) {
      for (let pl of this.programmingLanguages) {
        this._questionLangCode[pl.id] =
          this.currAnswer.codeAnswer.languageId == pl.id
            ? this.currAnswer.codeAnswer.code
            : pl.hello_world;
      }
    }
    this.checkSetDefaultAnswer();
  }
  ngOnChanges(changes: SimpleChanges) {
    this.saved = false;
  }
}
