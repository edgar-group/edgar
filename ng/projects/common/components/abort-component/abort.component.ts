import { Component, Input, OnChanges } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

@Component({
  selector: 'abort-app',
  templateUrl: './abort.component.html',
  styleUrls: ['./abort.component.css'],
  providers: [],
})
export class AbortComponent {
  @Input() message!: string;
  counter: number = 5;
  constructor() {
    const self = this;
    setInterval(function () {
      --self.counter;
      if (self.counter <= 0)
        window.location.href = `${
          environment.devWebApiUrl ?? ''
        }/test/instances`;
    }, 1000);
  }
}
