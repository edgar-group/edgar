import { Component, Input } from '@angular/core';
import { CodeResult } from '../../../models/exam/code-result';
import { Sorter } from './sorter';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
})
export class GridComponent {
  @Input()
  result!: CodeResult;
  columns!: Array<string>;
  rows!: Array<any>;

  constructor() {}

  sorter = new Sorter();

  sort(key: string) {
    this.sorter.sort(key, this.result.rs.data.rows);
  }

  formatValue(val: any) {
    return val == null ? 'null' : val;
  }

  hasRows() {
    return (
      this.result &&
      this.result.rs &&
      this.result.rs.data &&
      this.result.rs.data.rows
    );
  }

  getCount() {
    if (this.hasRows()) {
      return this.result.rs.data.rows.length;
    } else {
      return 0;
    }
  }

  getWarning(): string | null {
    if (this.result.warning || this.result.error) {
      return (
        (this.result.warning ? JSON.stringify(this.result.warning) : '') +
        (this.result.error ? JSON.stringify(this.result.error) : '')
      );
    } else {
      return null;
    }
  }
}
