import {
  ChangeDetectionStrategy,
  Component,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Input } from '@angular/core';
import { LogService } from '../../services/log.service';
import { LogEvent } from '../../models/log-event';

@Component({
  selector: 'clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css'],
})
export class ClockComponent implements OnInit {
  _overflow: boolean = false;
  _prolongedOverflow: boolean = false;
  _viewSeconds: number = 0;
  _calibrateMs: number = 0;
  _initialSecondsLeft: number = 0;
  @Input() isReview: boolean = false;
  @Input() seconds: number = 0;
  @Input() prolonged: boolean = false;

  constructor(private _logService: LogService) {}

  ngOnInit(): void {
    this._viewSeconds = this.seconds;
    this.getSecondsLeft(this.seconds);
    this._calibrateMs = new Date().getTime();
    this._initialSecondsLeft = this.seconds;
    if (!this.isReview) {
      setInterval(() => {
        this.seconds--;
        if (this.seconds % 100 === 0) {
          // calibrate every 100 seconds
          let now = new Date().getTime();
          let old = this.seconds;
          this.seconds =
            this._initialSecondsLeft -
            Math.round((now - this._calibrateMs) / 1000);
          if (old != this.seconds)
            console.log('Calibrating clock', old, this.seconds);
        }
        this.getSecondsLeft(this.seconds);
      }, 1000);
    }
  }

  getSecondsLeft(seconds: number) {
    this._overflow = false;
    this._prolongedOverflow = false;
    if (seconds < 0) {
      if (seconds === 0) {
        this._logService.log(new LogEvent('Time is up', '0 seconds left'));
      }
      if (this.prolonged) {
        this._prolongedOverflow = true;
      } else {
        this._overflow = true;
      }
      //seconds = -1 * seconds;
    }
    this._viewSeconds = seconds;
  }
}
