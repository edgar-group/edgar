import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'projects/config/environments/environment';
import { Question } from '../../models/question';
import { FileUploadComponent } from 'projects/exam/src/app/file-upload/file-upload.component';

@Pipe({
  name: 'uploadFile',
  pure: false,
})
export class UploadFilePipe implements PipeTransform {
  transform(
    idx: number,
    currQuestion: Question,
    selectedFileName: string,
    isRunning: boolean,
    uploadError: string
  ): string {
    if (uploadError) {
      return `<span class="text-danger">${uploadError}</span>`;
    } else if (isRunning) {
      return '<i class="fa fa-2 fa-spinner fa-spin"></i> please wait ... <i class="fa fa-2 fa-spinner fa-spin"></i>';
    } else if (currQuestion.uploaded && currQuestion.uploaded[idx]) {
      return `<a href="${
        environment.devWebApiUrl ?? ''
      }/exam/run/file/download/${currQuestion.ordinal}/${idx}"
      target="_blank">
      <span class="badge badge-success"><i class="file-icon fa fa-file"></i> ${
        currQuestion.tempUploadedFileNames[idx]
      }</span>
      </a>`;
    } else if (selectedFileName == FileUploadComponent.SELECT_FILE_STRING) {
      return '<small class="text-muted"><i>(nothing uploaded yet)</i></small>';
    } else {
      return '<span class="text-danger horizontal-shake">🠔 Nothing uploaded yet: click the cloud button to upload!</span>';
    }
  }
}
