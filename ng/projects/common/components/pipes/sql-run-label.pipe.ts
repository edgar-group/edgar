import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sqlRunLabel',
})
export class SqlRunLabelPipe implements PipeTransform {
  transform(queryRunning: boolean, currQuestionWait: number): string {
    if (queryRunning) {
      return '<i class="fa fa-2 fa-spinner fa-spin"></i>';
    } else if (currQuestionWait > 1e6) {
      return '<i class="fas fa-infinity" title="Quoth the Raven “Nevermore.”"></i>';
    } else if (currQuestionWait > 0) {
      return '' + Math.round(currQuestionWait);
    } else {
      return 'Run';
    }
  }
}
