import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lectureStudentLabel',
})
export class LectureStudentLabelPipe implements PipeTransform {
  transform(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      }
    }
    return '-';
  }
}
