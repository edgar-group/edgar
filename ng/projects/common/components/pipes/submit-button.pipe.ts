import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'submitButton' })
export class SubmitButtonPipe implements PipeTransform {
  transform(value: boolean): string {
    return value
      ? '<i class="fa fa-2 fa-spinner fa-spin"></i>Submitting, please wait....<i class="fa fa-2 fa-spinner fa-spin"></i>'
      : 'Yes, submit my exam!';
  }
}
