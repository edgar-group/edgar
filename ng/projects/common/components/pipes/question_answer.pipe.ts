import { Pipe, PipeTransform } from '@angular/core';
import { QuestionInfo } from 'projects/common/models/QuestionInfo';
import { Answer } from 'projects/common/models/answer';

@Pipe({
  name: 'questionAnswerLabel',
  pure: false,
})
export class QuestionAnswerPipe implements PipeTransform {
  // returns the label both for the:
  //  ABC questions (to choose from)
  // for the navbar - qnav component
  transform(answer: Answer): string {
    // console.log('pipe for answer', answer, currOr);
    if (answer) {
      if (QuestionInfo.isOrderedElements(answer) || QuestionInfo.isConnectedElements(answer)) {
        if (answer.multichoice && answer.multichoice.length) {
          return '<i class="fas fa-sort-amount-up-alt"></i>';
        }
      } else if (answer.answersDynamic && answer.answersDynamic.length) {
        // text completion questions
        return '<small><i class="fas fa-align-left"></i></small>';
      } else if (answer.answersStatic && answer.answersStatic.length) {
        // connected elements questions
        return '<small><i class="fas fa-align-left"></i></small>';
      } else if (answer.multichoice && answer.multichoice.length) {
        return answer.multichoice
          .map(function (num: number) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      } else if (
        answer.codeAnswer &&
        answer.codeAnswer.code &&
        answer.codeAnswer.code.trim() !== ''
      ) {
        return '<i class="fas fa-code"></i>';
      } else if (answer.textAnswer && answer.textAnswer.trim().length > 0) {
        return '<small><i class="fas fa-align-left"></i></small>';
      }
    }
    return '-';
  }
}
