import { Pipe, PipeTransform } from '@angular/core';
import {
  DomSanitizer,
  SafeHtml,
  SafeResourceUrl,
  SafeScript,
  SafeStyle,
  SafeUrl,
} from '@angular/platform-browser';

@Pipe({
  name: 'gridGetHint',
})
export class GridGetHintPipe implements PipeTransform {
  constructor(protected sanitizer: DomSanitizer) {}

  transform(
    score: any
  ): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
    let value = '';
    if (score) {
      if (score.is_correct) {
        value =
          '<span style="background-color:lime;"> <i class="far fa-smile" aria-hidden="true"></i> Correct! Well done! </span>';
      } else {
        value =
          '<span style="background-color:lemonchiffon;"> <i class="far fa-frown" aria-hidden="true"></i> Missed it. Hint: ' +
          score.hint +
          '</span>';
      }
    }
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }
}
