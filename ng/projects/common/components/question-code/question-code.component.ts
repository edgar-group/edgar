import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
  SimpleChanges,
} from '@angular/core';
import { CodeMirrorComponent } from '../code-mirror/code-mirror.component';
import { CodeRunnerService } from '../../services/code-runner.service';
import { Answer, CodeAnswer } from '../../models/answer';
import { CodeResult } from '../../../models/exam/code-result';
import { Question } from '../../models/question';
declare var Swal: any;

@Component({
  selector: 'question-code',
  templateUrl: './question-code.component.html',
  styleUrls: ['./question-code.component.css'],
  // providers: [
  //   CodeRunnerService,
  //   {
  //     provide: CODERUNNER_SERVICE_CONFIG,
  //     useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
  //   },
  // ],
})
export class QuestionCodeComponent implements OnInit, OnChanges {
  @ViewChild('codeMirror') codeMirror!: CodeMirrorComponent;
  saved: boolean;
  @Input()
  currQuestion!: Question;
  @Input()
  currQuestionWait!: number;
  @Input()
  currAnswer!: Answer;
  @Input()
  cmSkin!: string;
  @Input()
  isPlayground!: boolean;
  @Output() answerUpdated = new EventEmitter();
  @Output() answerSaved = new EventEmitter();
  @Output() codeRun = new EventEmitter();
  //@Output() codeChanged = new EventEmitter();
  //result : CodeResult;
  currResults: any;
  queryRunning: boolean;
  questionLangCode: Map<number, Map<number, string>> = new Map();
  _errMessage: string;
  constructor(
    private _ref: ChangeDetectorRef,
    private _codeRunnerService: CodeRunnerService
  ) {
    this.saved = true;
    this.currResults = {};
    this.queryRunning = false;
    this._errMessage = '';
  }

  getResult(): CodeResult {
    return this.currResults[this.currQuestion.ordinal - 1];
  }

  // TODO: delete this method once you figure out what to display for complex questions
  getScoreTemp() {
    return JSON.stringify(this.getResult().score);
  }

  // isSQL(): boolean {
  //   return this.currQuestion.isSQL();
  // }

  // isCodeLang(): boolean {
  //   return (
  //     this.currQuestion.type.toUpperCase().indexOf('C-LANG') >= 0 ||
  //     this.currQuestion.type.toUpperCase().indexOf('JAVA') >= 0 ||
  //     this.currQuestion.type.toUpperCase().indexOf('CODE') >= 0
  //   );
  // }

  // isJSON(): boolean {
  //   return this.currQuestion.type.toUpperCase().indexOf('JSON') >= 0;
  // }

  canRun() {
    return !(this.currQuestionWait && this.currQuestionWait > 0);
  }

  runAll() {
    this.run(this.getCurrCode());
  }

  run(code: string) {
    if (this.canRun()) {
      var self = this;
      this.save();
      this.queryRunning = true;
      this._errMessage = '';
      this.currResults[this.currQuestion.ordinal - 1] = undefined;
      this._codeRunnerService
        .getCodeResult(
          this.currQuestion.ordinal,
          code,
          '' + this.getCurrLangId(),
          this.isPlayground
        )
        .subscribe({
          next: (result) => {
            this.currResults[this.currQuestion.ordinal - 1] = result;
            //this.currQuestion.runs = result.runStats ? result.;
            this.queryRunning = false;
            // console.log("rasult that is emited ", result)
            // if (result && result.score) {
            //   if (result.score.is_correct) {

            //   } else if (!result.score.is_incorrect) {

            //   } else {

            //   }
            // }
            this.codeRun.emit(result);
            self._ref.markForCheck();
          },
          error: (e) => {
            this._errMessage = e.error
              ? JSON.stringify(e.error)
              : JSON.stringify(e);
            this.queryRunning = false;
            this.codeRun.emit(JSON.stringify(e));
            self._ref.markForCheck();
          },
        });
    } else {
      Swal.fire({
        title: 'Can not run code',
        icon: 'error',
        html: 'You can not run code (just now). Please wait while the button becomes enabled...',
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK',
        confirmButtonAriaLabel: 'OK',
      });
    }
  }

  save() {
    // Will always save the entire contents, not just the selected part
    // (that might come through the $event, ie runHotKey event)
    this.answerSaved.emit();
    console.log('TODO: lying , should be async....');
    this.saved = true;
  }

  onCodeChanged(newCode: string) {
    this.saved = false;

    let currCodeAnswer = this.currAnswer.codeAnswer;
    currCodeAnswer.code = newCode;

    if (currCodeAnswer.languageId && currCodeAnswer.code) {
      let x = this.questionLangCode.get(this.currQuestion.ordinal);
      if (x) x.set(currCodeAnswer.languageId, currCodeAnswer.code);
    }
    this.answerUpdated.emit(this.currAnswer);
  }

  onLanguageChanged(newLanguageId: number) {
    this.saved = false;
    let currCodeAnswer = this.currAnswer.codeAnswer;
    currCodeAnswer.languageId = newLanguageId;
    let obj = this.questionLangCode.get(this.currQuestion.ordinal);
    if (
      obj &&
      !obj.has(currCodeAnswer.languageId) &&
      this.currQuestion.programmingLanguages &&
      this.currQuestion.programmingLanguages.length
    ) {
      let helloWorld = this.currQuestion.programmingLanguages.find(
        (pl) => pl.id === currCodeAnswer.languageId
      );
      obj.set(
        currCodeAnswer.languageId,
        helloWorld?.hello_world + '\n\n\n\n\n'
      );
    }
    if (obj) this.onCodeChanged(obj.get(currCodeAnswer.languageId) || '');
  }

  getCurrCode(): string {
    return this.currAnswer.codeAnswer.code;
    // if (   this.currAnswer.codeAnswer
    //     && this.currAnswer.codeAnswer.code) {

    // } else {
    //     return '\n\n\n\n\n';
    // }
  }

  getCurrLangId() {
    console.log(
      'getCurrLangId',
      this.currAnswer,
      this.currQuestion.programmingLanguages
    );
    if (this.currAnswer.codeAnswer) {
      // this should always end in this branch, but for legacy issues, there is an else branch
      if (
        this.currQuestion.programmingLanguages &&
        this.currQuestion.programmingLanguages.length == 1
      ) {
        this.currAnswer.codeAnswer.languageId =
          this.currQuestion.programmingLanguages[0].id;
      } else if (
        // I'm not sure about this branch, TODO: check
        this.currAnswer.codeAnswer.languageId === 0 &&
        this.currQuestion.programmingLanguages
      ) {
        this.currAnswer.codeAnswer.languageId = 0;
      }
      return this.currAnswer.codeAnswer.languageId; // ie for SQL and JSOIN it will still be 0
    } else if (this.currQuestion.programmingLanguages) {
      // this is reserved for CONTINUED test thata did not have lang ids
      console.log('recovering legacy tests...');
      return this.currQuestion.programmingLanguages[0].id;
    } else {
      console.error('getCurrLangId() is undefined...');
      return 0;
    }
  }

  checkSetDefaultAnswer() {
    if (!this.currAnswer.codeAnswer) {
      this.currAnswer.codeAnswer = new CodeAnswer();
    }
    if (!this.currAnswer.codeAnswer.code) {
      this.currAnswer.codeAnswer.code = '\n\n\n\n\n';
    }
  }

  ngOnInit() {
    // is ngInit enough?
    this.checkSetDefaultAnswer();
  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log('ngOnChanges', changes);
    // console.log('this.currAnswer', this.currAnswer);
    // console.log('this', this);

    if (
      changes['currQuestion'] &&
      changes['currQuestion'].currentValue &&
      this.currAnswer.codeAnswer &&
      this.currAnswer.codeAnswer.languageId
    ) {
      if (!this.questionLangCode.has(this.currQuestion.ordinal)) {
        this.questionLangCode.set(
          this.currQuestion.ordinal,
          new Map<number, string>()
        );

        if (this.currAnswer.codeAnswer.code) {
          this.questionLangCode
            .get(this.currQuestion.ordinal)
            ?.set(
              this.currAnswer.codeAnswer.languageId,
              this.currAnswer.codeAnswer.code
            );
        } else {
          this.onLanguageChanged(this.currAnswer.codeAnswer.languageId);
        }
      }
    }
    if (changes['currAnswer']) {
      this.checkSetDefaultAnswer();
    }
    this.saved = false;
  }
}
