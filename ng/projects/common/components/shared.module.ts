import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PipesModule } from './pipes/pipes.module';
import { ClockComponent } from './clock/clock.component';
import { GradingModelLabelComponent } from './grading-model-label/grading-model-label.component';
import { CodeMirrorComponent } from './code-mirror/code-mirror.component';
import { JsonResultComponent } from './json-result/json-result.component';
import { ClangResultComponent } from './clang-result/clang-result.component';
import { GridComponent } from './grid/grid.component';
import { TicketComponent } from './ticket/ticket.component';
import { AbortComponent } from './abort-component/abort.component';

import { PageNotFoundComponent } from './page-not-found-component/page-not-found.component';
import { QuestionAttachmentsComponent } from './question-attachments/question-attachments.component';
import { CodePlaygroundComponent } from './code-playground/code-playground.component';
import { QuestionCodeComponent } from './question-code/question-code.component';

const COMPONENTS = [
  ClockComponent,
  GradingModelLabelComponent,
  CodeMirrorComponent,
  JsonResultComponent,
  ClangResultComponent,
  GridComponent,
  TicketComponent,
  PageNotFoundComponent,
  QuestionAttachmentsComponent,
  QuestionCodeComponent,
  CodePlaygroundComponent,
  AbortComponent,
];

const MODULES = [
  CommonModule,
  BrowserModule,
  HttpClientModule,
  FormsModule, // TODO do i need this?
  PipesModule,
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [MODULES],
  exports: [MODULES, COMPONENTS],
})
export class SharedModule {
  public static getAllComponents() {
    return COMPONENTS;
  }
}
