import { RunStats } from './run-stats';

export class CodeResult {
  constructor(
    public success: boolean,
    public error: any,
    public rs: any, // SQLData
    public db: string,
    public warning: string,
    public codeResult: any, // C data
    public score: any,
    public runStats: RunStats[]
  ) {
    if (this.codeResult) this.rs = undefined; // TODO: this is a mess, DB and C results are crammed into the same object
  }
}
