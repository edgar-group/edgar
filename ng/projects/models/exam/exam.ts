import { environment } from 'projects/config/environments/environment';
import { Answer } from 'projects/common/models/answer';
import { RunStats } from './run-stats';

export class Exam {
  constructor(
    public idTestInstance: number,
    public noOfQuestions: number,
    public secondsLeft: number,
    public prolonged: boolean,
    public title: string,
    public currAnswers: Answer[],
    public student: string,
    public alt_id: string,
    public alt_id2: string,
    public cmSkin: string,
    public imgSrc: string,
    public is_competition: boolean,
    public upload_file_limit: number,
    public forward_only: boolean,
    public last_ordinal: number,
    public type_name: string,
    public runStats: RunStats[],
    public usesTicketing: boolean
  ) {
    let ca: Answer[] = [];
    for (let i = 0; i < this.noOfQuestions; ++i) {
      if (this.currAnswers && this.currAnswers[i]) {
        ca.push(this.currAnswers[i]);
      } else {
        ca.push(new Answer('unknown'));
      }
    }
    this.currAnswers = ca;
    // if (!(this.currAnswers && this.currAnswers.length > 0)) {
    //   this.currAnswers = [];
    //   for (let i = 0; i < this.noOfQuestions; ++i) {
    //     this.currAnswers.push(new Answer());
    //   }
    // }
    if (!environment.production) {
      this.imgSrc = environment.devWebApiUrl + this.imgSrc;
    }
  }

  isPA2() {
    return this.type_name === 'Peer assessment PHASE2';
  }
  public setRunStats(rs: RunStats[]) {
    this.runStats = rs;
  }
}
