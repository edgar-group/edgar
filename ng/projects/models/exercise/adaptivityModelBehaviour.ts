export class AdaptivityModelBehaviour {
  constructor(
    public questionDifficultyLevelId: number,
    public levelUpTracking: number,
    public levelUpCorrect: number,
    public levelDownTracking: number,
    public levelDownIncorrect: number
  ) {}
}
