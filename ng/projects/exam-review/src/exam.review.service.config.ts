import { InjectionToken } from '@angular/core';
import { environment } from 'projects/config/environments/environment';

export const EXAM_REVIEW_SERVICE_CONFIG =
  new InjectionToken<ExamReviewServiceConfig>('exam.review.service.config');

export interface ExamReviewServiceConfig {
  getQuestionReviewUrl: string;
  getExamReviewUrl: string;
  getPeerAssesmentQuestionUrl: string;
  getAllPeerAssesmentQuestionsUrl: string;
}

export const DEFAULT_EXAM_REVIEW_CONFIG: ExamReviewServiceConfig = {
  getQuestionReviewUrl: `${
    environment.devWebApiUrl ?? ''
  }/exam/review/question`,
  getExamReviewUrl: `${environment.devWebApiUrl ?? ''}/exam/review/instance`,
  getPeerAssesmentQuestionUrl: `${
    environment.devWebApiUrl ?? ''
  }/exam/review/peerquestion`,
  getAllPeerAssesmentQuestionsUrl: `${
    environment.devWebApiUrl ?? ''
  }/exam/review/peerquestions`,
};
