import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from 'projects/common/components/shared.module';
import { RequestInterceptor } from 'projects/common/interceptors/request.interceptor';
import { LogService } from 'projects/common/services/log.service';
import {
  EXAM_REVIEW_LOG_SERVICE_CONFIG,
  LOG_SERVICE_CONFIG,
} from 'projects/config/log.config';
import {
  DEFAULT_TICKET_SERVICE_CONFIG,
  TICKET_SERVICE_CONFIG,
} from 'projects/config/ticket.config';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app/app.component';
import { ExamReviewComponent } from './app/exam-review/exam-review.component';
import { QuestionReviewContentComponent } from './app/question-review-content/question-review-content.component';
import { ExamReviewService } from './exam.review.service';
import {
  DEFAULT_EXAM_REVIEW_CONFIG,
  EXAM_REVIEW_SERVICE_CONFIG,
} from './exam.review.service.config';
import {
  IExamService,
  I_EXAM_SERVICE_TOKEN,
} from 'projects/common/models/IExamService';
import { FreeTextComponent } from './app/free-text/free-text.component';
// import { PeerAssessmentComponent } from './app/peer-assessment/peer-assessment.component';
import { QuestionMultichoiceComponent } from './app/question-multichoice/question-multichoice.component';
import { QuestionReviewTrueFalseComponent } from './app/question-review-true-false/question-review-true-false.component';
import { ScaleItemsComponent } from './app/scale-items/scale-items.component';
import { CodeRunnerService } from 'projects/common/services/code-runner.service';
import {
  CODERUNNER_SERVICE_CONFIG,
  EXAM_CODERUNNER_SERVICE_CONFIG,
} from 'projects/config/code-runner.config';
import { FileUploadComponent } from './app/file-upload/file-upload.component';
import { HeaderReviewComponent } from './app/header-review/header-review.component';
import { BaseExamReviewService } from './base.exam.review.service';
import { Pa1ExamReviewComponent } from './app/pa1-exam-review/pa1-exam-review.component';
import { Pa2ExamReviewComponent } from './app/pa2-exam-review/pa2-exam-review.component';
import { QuestionReviewOrderedElementsComponent } from './app/question-review-ordered-elements/question-review-ordered-elements.component';
import { QuestionReviewConnectedElementsComponent } from './app/question-review-connected-elements/question-review-connected-elements.component';
import { QuestionReviewDiagramComponent } from './app/question-review-diagram/question-review-diagram.component';

@NgModule({
  declarations: [
    AppComponent,
    ExamReviewComponent,
    Pa1ExamReviewComponent,
    Pa2ExamReviewComponent,
    QuestionReviewContentComponent,
    FreeTextComponent,
    // PeerAssessmentComponent,
    QuestionMultichoiceComponent,
    QuestionReviewTrueFalseComponent,
    QuestionReviewOrderedElementsComponent,
    QuestionReviewConnectedElementsComponent,
    QuestionReviewDiagramComponent,
    ScaleItemsComponent,
    FileUploadComponent,
    HeaderReviewComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, SharedModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true,
    },
    {
      provide: I_EXAM_SERVICE_TOKEN,
      useClass: BaseExamReviewService,
      multi: false,
    },
    ExamReviewService,
    { provide: TICKET_SERVICE_CONFIG, useValue: DEFAULT_TICKET_SERVICE_CONFIG },
    {
      provide: EXAM_REVIEW_SERVICE_CONFIG,
      useValue: DEFAULT_EXAM_REVIEW_CONFIG,
    },
    LogService,
    { provide: LOG_SERVICE_CONFIG, useValue: EXAM_REVIEW_LOG_SERVICE_CONFIG },
    {
      provide: CODERUNNER_SERVICE_CONFIG,
      useValue: EXAM_CODERUNNER_SERVICE_CONFIG,
    },
    CodeRunnerService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
