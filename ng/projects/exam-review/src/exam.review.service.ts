import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { firstValueFrom, map, Observable, of, Subject, tap } from 'rxjs';

import {
  ExamReviewServiceConfig,
  EXAM_REVIEW_SERVICE_CONFIG,
} from './exam.review.service.config';

import { QuestionReview } from './models/QuestionReview';
import { ExamReview } from './models/ExamReview';
import { environment } from 'projects/config/environments/environment';
import { IExamService } from 'projects/common/models/IExamService';
import { GenericResponse } from 'projects/common/models/GenericResponse';
import { DiagramService } from 'projects/common/services/diagram.service';

@Injectable({
  providedIn: 'root',
})
export class ExamReviewService implements IExamService {
  private _questions: QuestionReview[];
  private _prQuestions: QuestionReview[][];
  private prQuestionsLoadingErrors: string[];
  private _examReview!: ExamReview;
  _currentQuestionOrdinal: number = 0;
  public currentQuestionObservable = new Subject<QuestionReview>();

  constructor(
    private _http: HttpClient,
    @Inject(EXAM_REVIEW_SERVICE_CONFIG)
    private _config: ExamReviewServiceConfig,
    private _diagramService: DiagramService
  ) {
    this._questions = [];
    this._prQuestions = [];
    this.prQuestionsLoadingErrors = [];
  }

  async getQuestionReview(ordinal: number): Promise<QuestionReview> {
    if (!this._questions[ordinal - 1]) {
      await firstValueFrom(
        this._http
          .get<QuestionReview>(
            `${this._config.getQuestionReviewUrl}/${ordinal}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map(async (res) => {
              let q = res;
              q.content = await this._diagramService.getInjectSvgDiagrams(q.content);
              this._questions[ordinal - 1] = new QuestionReview(
                q.ordinal.toString(),
                q.ordinal,
                q.content,
                q.answers,
                q.answersStatic,
                q.answersDynamic,
                q.correctAnswers,
                q.studentAnswers,
                q.multiCorrect,
                q.studentAnswerCode,
                q.studentAnswerText,
                q.correctAnswerCode,
                q.correctAnswerDiagram,
                q.hint,
                q.type,
                q.codeEvalData,
                q.uploadedFiles,
                q.scaleItems,
                q.manualComment,
                q.graderImage,
                q.graderName,
                q.score,
                q.scorePerc,
                q.gradingModelName,
                q.displayOption
              );
              return this._questions[ordinal - 1];
            })
          )
      );
    }
    return Promise.resolve(this._questions[ordinal - 1]);
  }

  devlogin() {
    console.log('REVIEW devlogin, posting...');
    var formData: any = new FormData();
    formData.append('username', 'nk51530@fer.hr');
    formData.append('password', 'nk51530@fer.hr@node.js');
    return firstValueFrom(
      this._http
        .post(`${environment.devWebApiUrl ?? ''}/auth/ngdevlogin`, formData, {
          withCredentials: true,
          responseType: 'text',
          observe: 'response',
        })
        .pipe(
          map((response) => {
            // console.log('simple login response', response);
            return 'asdas';
          })
        )
    );
  }
  examLoaded(): boolean {
    return !!this._examReview;
  }
  getExam(): ExamReview {
    if (this._examReview) return this._examReview;
    else {
      throw 'Missing cached exam!?';
    }
  }
  async loadExamReview(): Promise<ExamReview> {
    if (!this._examReview) {
      await firstValueFrom(
        this._http
          .get<ExamReview>(`${this._config.getExamReviewUrl}`, {
            withCredentials: true,
          })
          .pipe(
            map((res) => {
              let t = res;
              if (typeof t.answersOutcome === 'string') {
                t.answersOutcome = JSON.parse(t.answersOutcome);
              }
              this._examReview = new ExamReview(
                t.idTestInstance,
                t.title,
                t.questionsNo,
                t.secondsLeft,
                t.prolonged,
                t.correctNo,
                t.incorrectNo,
                t.unansweredNo,
                t.partialNo,
                t.score,
                t.scorePerc,
                t.passed,
                t.answersOutcome,
                t.student,
                t.altId,
                t.altId2,
                t.showSolutions,
                t.cmSkin ? t.cmSkin : 'ambiance',
                t.imgSrc,
                t.typeName,
                t.noReviews,
                t.noReviewQuestions,
                t.usesTicketing
              );
              return this._examReview;
            })
          )
      );
    }
    return Promise.resolve(this._examReview);
  }
  async loadAllQuestions() {
    let promises: any[] = [];
    for (
      let i = 1;
      this._examReview && i <= this._examReview.questionsNo;
      i++
    ) {
      promises.push(this.getQuestionReview(i));
    }
    return await Promise.all(promises);
  }
  async loadAllPA1Questions() {
    let promises: any[] = [];
    for (let r = 1; r <= this._examReview.noReviews; r++) {
      promises.push(
        this.getAllPeerAssesmentQuestions(r, this._examReview.noReviewQuestions)
      );
    }
    return await Promise.all(promises);
  }

  // async loadAllPA1Questions() {
  //   let promises: any[] = [];
  //   for (let r = 1; r <= this._examReview.noReviews; r++) {
  //     for (
  //       let q = 1;
  //       this._examReview && q <= this._examReview.noReviewQuestions;
  //       q++
  //     ) {
  //       promises.push(this.getPeerAssesmentQuestion(r, q));
  //     }
  //   }
  //   return await Promise.all(promises);
  // }
  async setCurrentQuestionOrdinal(ordinal: number): Promise<void> {
    await this.getQuestionReview(ordinal);
    this._currentQuestionOrdinal = ordinal;
    this.currentQuestionObservable.next(this._questions[ordinal - 1]);
  }
  getCurrentQuestion(): QuestionReview {
    return this._questions[this._currentQuestionOrdinal - 1];
  }
  getPeerAssessmentReviewLoadingError(revNo: number) {
    return this.prQuestionsLoadingErrors[revNo - 1];
  }
  async getAllPeerAssesmentQuestions(
    revNo: number,
    count: number
  ): Promise<Array<QuestionReview>> {
    if (this._prQuestions[revNo] && this._prQuestions[revNo]) {
      return Promise.resolve(this._prQuestions[revNo]);
    } else {
      return firstValueFrom(
        this._http
          .get<Array<QuestionReview>>(
            `${this._config.getAllPeerAssesmentQuestionsUrl}/${revNo}/count/${count}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map((res: QuestionReview[] | GenericResponse) => {
              if (!this._prQuestions[revNo]) this._prQuestions[revNo] = [];
              if ((res as GenericResponse).success === false) {
                try {
                  this.prQuestionsLoadingErrors[revNo - 1] = (
                    res as GenericResponse
                  ).error.message;
                } catch (error) {
                  this.prQuestionsLoadingErrors[revNo - 1] =
                    JSON.stringify(res);
                }
              } else {
                for (let q of res as QuestionReview[]) {
                  this._prQuestions[revNo][q.ordinal] = new QuestionReview(
                    q.ordinal.toString(),
                    q.ordinal,
                    q.content,
                    q.answers,
                    q.answersStatic,
                    q.answersDynamic,
                    q.correctAnswers,
                    q.studentAnswers,
                    q.multiCorrect,
                    q.studentAnswerCode,
                    q.studentAnswerText,
                    q.correctAnswerCode,
                    q.correctAnswerDiagram,
                    q.hint,
                    q.type,
                    q.codeEvalData,
                    q.uploadedFiles,
                    q.scaleItems,
                    q.manualComment,
                    q.graderImage,
                    q.graderName,
                    q.score,
                    q.scorePerc,
                    q.gradingModelName,
                    q.displayOption
                  );
                }
              }
              return this._prQuestions[revNo];
            })
          )
      );
    }
  }
  async getPeerAssesmentQuestion(
    revNo: number,
    qNo: number
  ): Promise<QuestionReview> {
    if (this._prQuestions[revNo] && this._prQuestions[revNo][qNo]) {
      return Promise.resolve(this._prQuestions[revNo][qNo]);
    } else {
      return firstValueFrom(
        this._http
          .get<QuestionReview>(
            `${this._config.getPeerAssesmentQuestionUrl}/${revNo}/question/${qNo}`,
            {
              withCredentials: true,
            }
          )
          .pipe(
            map((res) => {
              let q = res;
              if (!this._prQuestions[revNo]) this._prQuestions[revNo] = [];
              this._prQuestions[revNo][qNo] = new QuestionReview(
                q.ordinal.toString(),
                q.ordinal,
                q.content,
                q.answers,
                q.answersStatic,
                q.answersDynamic,
                q.correctAnswers,
                q.studentAnswers,
                q.multiCorrect,
                q.studentAnswerCode,
                q.studentAnswerText,
                q.correctAnswerCode,
                q.correctAnswerDiagram,
                q.hint,
                q.type,
                q.codeEvalData,
                q.uploadedFiles,
                q.scaleItems,
                q.manualComment,
                q.graderImage,
                q.graderName,
                q.score,
                q.scorePerc,
                q.gradingModelName,
                q.displayOption
              );
              return this._prQuestions[revNo][qNo];
            })
          )
      );
    }
  }
}
