import { Component, OnInit } from '@angular/core';

import { ExamReviewService } from '../../exam.review.service';

import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { environment } from 'projects/config/environments/environment';

@Component({
  selector: 'app',
  templateUrl: `app.component.html`,
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  _intialQuestionLoaded: boolean = false;
  _examLoaded: boolean = false;

  constructor(
    private _examReviewService: ExamReviewService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {}
  collectRouteParams(router: Router): any {
    let params = {};
    let stack: ActivatedRouteSnapshot[] = [router.routerState.snapshot.root];
    while (stack.length > 0) {
      const route = stack.pop()!;
      params = { ...params, ...route.params };
      stack.push(...route.children);
    }
    return params;
  }
  public async ngOnInit() {
    if (!environment.production && location.port == '4200') {
      console.log('Dev mode -> dummy login to last started exam!');
      await this._examReviewService.devlogin();
    }
    let exam = await this._examReviewService.loadExamReview();
    if (exam == null) {
      alert('exam review is null!?');
    } else {
      this._examLoaded = true;
      if (exam.isPA1()) {
        await this._examReviewService.loadAllPA1Questions();
        this._router.navigate(['/pa1'], {
          relativeTo: this._route,
        });
      } else if (exam.isPA2()) {
        await this._examReviewService.loadAllQuestions();
        this._router.navigate(['/pa2'], {
          relativeTo: this._route,
        });
      } else {
        let routeParams = this.collectRouteParams(this._router);
        let initialOrdinal = routeParams.id || 1;
        if (initialOrdinal > this._examReviewService.getExam().questionsNo) {
          initialOrdinal = 1;
        }
        await this._examReviewService.setCurrentQuestionOrdinal(initialOrdinal);
        this._router.navigate([initialOrdinal], {
          relativeTo: this._route,
        });
      }
      this._intialQuestionLoaded = true;
    }
  }
}
