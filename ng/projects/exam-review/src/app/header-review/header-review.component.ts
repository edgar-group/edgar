import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'projects/config/environments/environment';
import { ExamReviewService } from '../../exam.review.service';
import { ExamReview } from '../../models/ExamReview';

@Component({
  selector: 'header-review',
  templateUrl: './header-review.component.html',
  styleUrls: ['./header-review.component.css'],
})
export class HeaderReviewComponent implements OnInit {
  @Input() examReview!: ExamReview;
  currentOrdinal: number = 0;
  _isReview: boolean = true;
  _layoutHorizontal: boolean;
  // _questionSelected: boolean;
  PAAssignementUrl: string;

  constructor(
    private _examReviewService: ExamReviewService,
    private _router: Router
  ) {
    this._layoutHorizontal = false;
    this.PAAssignementUrl = `${
      environment.devWebApiUrl ?? ''
    }/exam/review/peerattachment`;
  }

  getDevUrlStub() {
    return environment.production ? '' : environment.devWebApiUrl ?? '';
  }

  navigateTo(ordinal: number) {
    this._router.navigate([`/${ordinal}`]);
  }
  ngOnInit(): void {
    if (
      !this._examReviewService.getExam().isPA1() &&
      !this._examReviewService.getExam().isPA2()
    ) {
      this._examReviewService.currentQuestionObservable.subscribe((q) => {
        // console.log('QN ordinal changed (subsc)');
        this.currentOrdinal = q.ordinal;
      });
      this.currentOrdinal =
        this._examReviewService.getCurrentQuestion().ordinal;
    }

    // this._route.params.subscribe(async (params: Params) => {
    //   console.log(params, this._route);

    //   this.currentOrdinal = (await this._examService.getCurrentQuestion()).ordinal;
    // });
    // if (this.exam)
    //   this._idSelected = this.exam.forward_only
    //     ? this.exam.last_ordinal - 1
    //     : this.currQuestionOrdinal - 1;
  }

  isSelected(idx: number) {
    return idx === this.currentOrdinal - 1;
  }

  done() {
    window.location.href = '/';
  }

  getSeconds() {
    return this.examReview.secondsLeft;
  }
}
