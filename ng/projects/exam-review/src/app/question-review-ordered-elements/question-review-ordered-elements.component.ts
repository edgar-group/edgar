import {
  Component,
  Input,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'question-review-ordered-elements',
  templateUrl: './question-review-ordered-elements.component.html',
  styleUrls: ['./question-review-ordered-elements.component.css'],
})
export class QuestionReviewOrderedElementsComponent {
  @Input() currQuestion!: QuestionReview;


  constructor(protected sanitizer: DomSanitizer) {
  }

  getElementsCorrect(): Array<any> {
    return this.currQuestion.correctAnswers.map((ordinal: number) => {
      return this.currQuestion.answers[ordinal - 1];
    });
  }
  getElementsStudent(): Array<any> {
    return this.currQuestion.studentAnswers.map((ordinal: number) => {
      return this.currQuestion.answers[ordinal - 1];
    });
  }
}
