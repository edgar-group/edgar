import { Component, Input, OnInit } from '@angular/core';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'peer-assessment',
  templateUrl: './peer-assessment.component.html',
  styleUrls: ['./peer-assessment.component.css'],
})
export class PeerAssessmentComponentTBD implements OnInit {
  @Input() cmSkin!: string;
  @Input() currQuestion!: QuestionReview;
  @Input() PA2Questions!: QuestionReview[];
  @Input() showSolutions!: boolean;

  constructor() {}

  ngOnInit(): void {}
}
