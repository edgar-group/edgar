import {
  ChangeDetectorRef,
  Component,
  Injectable,
  Input,
  OnInit,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CodeRunnerService } from 'projects/common/services/code-runner.service';
import { CodeResult } from 'projects/models/exam/code-result';
import { forkJoin } from 'rxjs';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'question-review-content',
  templateUrl: './question-review-content.component.html',
  styleUrls: ['./question-review-content.component.css'],
})
@Injectable()
export class QuestionReviewContentComponent implements OnInit {
  @Input() currQuestion!: QuestionReview;
  @Input() cmSkin!: string;
  @Input() showSolutions!: boolean;

  // @Output() answerSelected = new EventEmitter();
  studentResults: any;
  correctResults: any;
  showHourGlassStudent: any;
  showHourGlassCorrect: any;
  parsed_eval_data: any;

  constructor(
    private _codeRunnerService: CodeRunnerService,
    public sanitizer: DomSanitizer
  ) {
    this.studentResults = {};
    this.correctResults = {};
    this.showHourGlassStudent = {};
    this.showHourGlassCorrect = {};
    this.parsed_eval_data = {};
  }

  ngOnInit(): void {}

  getStudentResult(): CodeResult {
    return this.studentResults[this.currQuestion.ordinal];
  }

  getCorrectResult(): CodeResult {
    return this.correctResults[this.currQuestion.ordinal];
  }

  getShowHourGlassStudent() {
    return !!this.showHourGlassStudent[this.currQuestion.ordinal];
  }

  getShowHourGlassCorrect() {
    return !!this.showHourGlassCorrect[this.currQuestion.ordinal];
  }

  rerunStudent(): void {
    let qOrdinal: number = this.currQuestion.ordinal;
    let self = this;
    this.showHourGlassStudent[qOrdinal] = true;
    this.studentResults[qOrdinal] = undefined;
    this._codeRunnerService
      .getCodeReviewStudentResult(qOrdinal)
      .subscribe((result) => {
        console.log('res', result);
        this.studentResults[qOrdinal] = result;
        this.showHourGlassStudent[qOrdinal] = false;
        // if (qOrdinal === this.currQuestion.ordinal) {
        //   self._ref.markForCheck();
        // }
      });
  }

  rerunCorrect(): void {
    let qOrdinal: number = this.currQuestion.ordinal;
    this.showHourGlassCorrect[qOrdinal] = true;
    this.correctResults[qOrdinal] = undefined;
    let self = this;
    this._codeRunnerService
      .getCodeReviewCorrectResult(qOrdinal)
      .subscribe((result) => {
        this.correctResults[qOrdinal] = result;
        this.showHourGlassCorrect[qOrdinal] = false;
        // if (qOrdinal === this.currQuestion.ordinal) {
        //   // self._ref.markForCheck();
        // }
      });
  }

  ngOnChanges(change) {
    var self = this;
    if (this.currQuestion && this.currQuestion.ordinal) {
      // not true for PA
      ($('.comment-popover[data-toggle="popover"]') as any).popover('hide');
      if (this.currQuestion.codeEvalData) {
        try {
          this.parsed_eval_data[this.currQuestion.ordinal] = JSON.parse(
            this.currQuestion.codeEvalData
          );
        } catch (error) {
          console.log('could not parse', this.currQuestion.codeEvalData);
        }
      }
      // self._ref.markForCheck();

      setTimeout(function () {
        ($('.comment-popover[data-toggle="popover"]') as any).popover(
          'dispose'
        );
        ($('.comment-popover[data-toggle="popover"]') as any).popover({
          placement: 'right',
          trigger: 'click',
          html: true,
          title: `<div class="media">${self.currQuestion.graderImage}
                        <div class="media-body">
                            <h5 class="media-heading">${self.currQuestion.graderName} says:</h5>
                        </div>
                    </div>`,
          content: `<pre>${self.currQuestion.manualComment}</pre>`,
        });
      }, 500);
    }
  }
}
