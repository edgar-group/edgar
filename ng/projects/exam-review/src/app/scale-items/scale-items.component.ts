import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ScaleItem } from 'projects/common/models/scale-item';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'scale-items',
  templateUrl: './scale-items.component.html',
  styleUrls: ['./scale-items.component.css'],
})
export class ScaleItemsComponent implements OnInit {
  @Input() currQuestion!: QuestionReview;

  constructor(public sanitizer: DomSanitizer) {}

  ngOnInit(): void {}

  isSelectedScaleItem(si: ScaleItem) {
    // if (!this.currQuestion.studentAnswers) {
    //   console.log('Empty answers for ', this.currQuestion.ordinal, si);
    // }
    return (
      this.currQuestion.studentAnswers &&
      this.currQuestion.studentAnswers.indexOf(si.value) >= 0
    );
  }
}
