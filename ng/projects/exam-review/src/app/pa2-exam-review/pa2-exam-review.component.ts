import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ExamReviewService } from '../../exam.review.service';
import { ExamReview } from '../../models/ExamReview';
import { QuestionReview } from '../../models/QuestionReview';

@Component({
  selector: 'pa2-exam-review',
  templateUrl: './pa2-exam-review.component.html',
  styleUrls: ['./pa2-exam-review.component.css'],
})
export class Pa2ExamReviewComponent implements OnInit {
  _examLoaded: boolean = false;
  questions: Array<QuestionReview> = [];

  constructor(
    private _examReviewService: ExamReviewService // private _route: ActivatedRoute
  ) {}

  public async ngOnInit() {
    this._examLoaded = true;
    for (let i = 0; i < this._examReviewService.getExam().questionsNo; ++i) {
      this.questions.push(
        await this._examReviewService.getQuestionReview(i + 1)
      );
    }
    // this._examReviewService.setCurrentQuestionOrdinal(1); // required for header component
    // this._examReviewService.currentQuestionObservable.subscribe(
    //   (currQuestion) => {
    //     this._currQuestion = currQuestion;
    //   }
    // );

    // this._route.params.subscribe(async (params: Params) => {
    //   let ordinal = +params['id'];
    //   if (ordinal > 0) {
    //     this._examLoaded = true;
    //     this._examReviewService.setCurrentQuestionOrdinal(ordinal);
    //   }
    // });
  }
}
