import { QuestionInfo } from 'projects/common/models/QuestionInfo';
import { ScaleItem } from 'projects/common/models/scale-item';
import { environment } from 'projects/config/environments/environment';

export class QuestionReview {
  constructor(
    public caption: string,
    public ordinal: number,
    public content: string,
    public answers: any,
    public answersStatic: any,
    public answersDynamic: any,
    public correctAnswers: number[],
    public studentAnswers: number[],
    public multiCorrect: boolean,
    public studentAnswerCode: string,
    public studentAnswerText: string,
    public correctAnswerCode: string,
    public correctAnswerDiagram: string,
    public hint: string,
    public type: string,
    public codeEvalData: string,
    public uploadedFiles: string,
    public scaleItems: ScaleItem[],
    public manualComment: string,
    public graderImage: string,
    public graderName: string,
    public score: number,
    public scorePerc: number,
    public gradingModelName: string,
    public displayOption: string
  ) {
    if (!environment.production && this.uploadedFiles) {
      this.uploadedFiles = `${environment.devWebApiUrl ?? ''}${
        this.uploadedFiles
      }`;
    }
  }

  public isSQL(): boolean {
    return QuestionInfo.isSQL(this);
  }
  public isScaleQuestion(): boolean {
    return QuestionInfo.isScaleQuestion(this);
  }
  public isCodeLang(): boolean {
    return QuestionInfo.isCodeLang(this);
  }
  public isJSON(): boolean {
    return QuestionInfo.isJSON(this);
  }
  public isRunnable(): boolean {
    return QuestionInfo.isRunnable(this);
  }
  public isFreeText(): boolean {
    return QuestionInfo.isFreeText(this);
  }
  public isMultiChoice(): boolean {
    return QuestionInfo.isMultiChoice(this);
  }
  public isTrueFalse(): boolean {
    return QuestionInfo.isTrueFalse(this);
  }
  public isOrderedElements(): boolean {
    return QuestionInfo.isOrderedElements(this);
  }
  public isConnectedElements(): boolean {
    return QuestionInfo.isConnectedElements(this);
  }
  public isTextCompletion(): boolean {
    return QuestionInfo.isTextCompletion(this);
  }
  public isDiagram(): boolean {
    return QuestionInfo.isDiagram(this);
  }
  public isComplex(): boolean {
    return QuestionInfo.isComplex(this);
  }
  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      } else if (typeof answers === 'string' && answers.trim() !== '') {
        return '<code/>';
      }
    }
    return '-';
  }
}
