import { Component, Injectable, OnInit } from 'angular2/core';
import { LectureStudentService, Test, Question } from './lecturestudent.service';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { LogEvent, LogService } from '../common/log.service';
import { testLogServiceConfigProvider } from './../common/log.service.provider';
import { QuestionComponent } from './question.component';

@Component({
  selector: 'student-app',
  template: `<div *ngIf="_test" class="room-header">
      <div class="col-md-2">
        <h2 id="roomName">{{ _test.roomName }}</h2>
      </div>
      <div class="col-md-4 col-md-offset-2">
        <h1>{{ _test.title }}</h1>
      </div>
    </div>

    <div *ngIf="_test && !_currQuestion">
      <div class="jumbotron col-md-6 col-md-offset-3">
        <h2 class="welcome">Welcome to room:</h2>
        <h2 id="room">{{ _test.roomName }}</h2>
        <br />
        <h2 class="welcome">Get ready!</h2>
      </div>
    </div>

    <div *ngIf="_currQuestion && !_isStoppedQuestion">
      <div class="col-md-8 col-md-offset-2">
        <question-cont
          [currQuestion]="_currQuestion"
          [currAnswers]="_currAnswers"
          (answerUpdated)="onAnswerUpdated($event)"
        >
        </question-cont>
      </div>
    </div>

    <div *ngIf="_currQuestion && _isStoppedQuestion && !_testEnded">
      <h2>Question ended!</h2>
      <h2>Look up to see the answers!</h2>
    </div>

    <div *ngIf="_testEnded && _score">
      <div class="jumbotron col-md-6 col-md-offset-3">
        <h2 class="welcome">Quiz finished!</h2>
        <br />
        <h2 class="welcome">Your score is:</h2>
        <h2 id="score">{{ _score.score }}</h2>
      </div>
    </div> `,
  styles: [
    `
      .room-header {
        border-bottom: 3px #9de5fb solid;
        padding-bottom: 10px;
        margin-top: 10px;
        margin-bottom: 50px;
        overflow: hidden;
      }

      #roomName {
        color: #6a0000;
      }

      .jumbotron {
        background: #f5f3f5;
        box-shadow: 0 8px 16px -6px black;
      }

      .welcome {
        font-size: 3vw;
      }

      #room,
      #score {
        color: #6a0000;
        font-weight: bold;
        font-size: 4vw;
      }
    `,
  ],
  directives: [QuestionComponent],
  providers: [LectureStudentService, WINDOW_PROVIDERS, LogService, testLogServiceConfigProvider],
})
@Injectable()
export class AppComponent implements OnInit {
  _win: Window;
  _test: Test;
  messages = [];
  connection;
  _currQuestion: Question;
  _currQuestionOrdinal;
  _currAnswers: any[];
  _isStoppedQuestion: boolean;
  _testEnded: boolean;
  _score: any;

  constructor(
    private _lectureStudentService: LectureStudentService,
    private _logService: LogService,
    win: WINDOW
  ) {
    this._win = win.nativeWindow;

    this._currQuestionOrdinal = 0;
    this._isStoppedQuestion = true;
    this._testEnded = false;

    this._lectureStudentService.getTest().subscribe((test) => {
      if (test === null) {
        this._win.location.href = '/lecture/enter';
      } else {
        this._test = test;

        // current answers always empty in the beginning
        this._currAnswers = [];
        for (let _i = 0; _i < test.noOfQuestions; _i++) {
          this._currAnswers.push([]);
        }

        this._logService.logLecture(
          new LogEvent('Lecture started', JSON.stringify(this._currAnswers))
        );

        //this.connection = this._lectureStudentService.connect();
        this._lectureStudentService.connect();

        this._lectureStudentService.getQuestions().subscribe((ordinal: any) => {
          this._lectureStudentService.getQuestion(ordinal.ordinal).subscribe((q) => {
            this._currQuestion = q;
            //console.log(this._currQuestion);
          });
          this._currQuestionOrdinal = ordinal;
          this._isStoppedQuestion = false;
        });

        this._lectureStudentService.stopQuestion().subscribe(() => {
          this._isStoppedQuestion = true;
        });

        this._lectureStudentService.endTest().subscribe(() => {
          this._isStoppedQuestion = true;
          this._testEnded = true;
          var self = this;
          this._lectureStudentService.emitAllAnswers(this._currAnswers);
          this._lectureStudentService.submitAnswers(this._currAnswers).subscribe((score) => {
            if (score.success) {
              this._score = score;
            }
          });
        });
      }
    });
  }

  onAnswerUpdated($newValue) {
    this._currAnswers[this._currQuestion.ordinal - 1] = $newValue;
    // console.log(this._currAnswers);
    // this._currAnswers = this._currAnswers.slice();  // :( to force ng onchange
    this._logService.logLecture(new LogEvent('Answer updated', JSON.stringify(this._currAnswers)));
    // console.log('this._logService.log(new LogEvent("Answer updated", JSON.stringify(this._currAnswers)))', this._currAnswers);

    this._lectureStudentService.emitFreeTextAnswer($newValue[0]);
  }

  ngOnInit() {
    // this.connection = this._lectureStudentService.getMessages().subscribe(message => {
    //     this.messages.push(message);
    // })
    // this.connection = this._lectureStudentService.getQuestions().subscribe(question => {
    //     this._lectureStudentService.getQuestion(this._currQuestionOrdinal + 1).
    //     subscribe(q => {
    //         this._currQuestion = q;
    //         console.log(this._currQuestion);
    //     });
    //     this._currQuestionOrdinal++;
    // })
  }
}
