/// <reference path="../../../node_modules/@types/socket.io-client/index.d.ts"/>

import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from './../common/log.service';
import * as io from 'socket.io-client';
// import 'ng-socket-io';
// import { Socket } from 'ng-socket-io';

@Injectable()
export class LectureStudentService {
  private _questions: Question[];
  private _test: Test;

  private url = 'http://localhost:1337';
  private socket;

  constructor(private _http: Http, private _logService: LogService) {
    this._questions = [];
  }

  getQuestion(num: number) {
    let rv: Question;
    if (this._questions[num]) {
      return Observable.of(this._questions[num]);
    } else {
      this._logService.logLecture(new LogEvent('Get question', 'num:' + num));
      return this._http.get(`/lecture/student/getquestion/${num}`).map((res) => {
        let q = res.json();
        this._questions[num] = new Question(
          q.ordinal.toString(),
          q.ordinal,
          q.content,
          q.answers,
          q.multiCorrect,
          q.type,
          q.correctAnswers
        );
        this._logService.logLecture(new LogEvent('Received question', 'num:' + q.ordinal));
        return this._questions[num];
      });
    }
  }

  getTest() {
    if (this._test) {
      return Observable.of(this._test);
    } else {
      this._logService.logLecture(new LogEvent('Get test', '-'));
      return this._http.get(`/lecture/student/gettest`).map((res) => {
        let t = res.json();
        if (t.success === false) {
          this._logService.logLecture(new LogEvent('Received NULL test', JSON.stringify(t)));
          return null;
        } else {
          this._test = new Test(t.noOfQuestions, t.title, t.password, t.roomName);
          this._logService.logLecture(new LogEvent('Received test', JSON.stringify(this._test)));
          return this._test;
        }
      });
    }
  }

  connect() {
    this.socket = io.connect(window.location.host, {
      path: '/wss/socket.io',
      query: 'room=' + this._test.roomName,
    });
    this.socket.on('connection', function (data) {
      // console.log('Client connected', data);
      this.socket.emit('connected', 'someusername'); // emit an event to the socket
    });
  }

  getQuestions() {
    let observable = new Observable((observer) => {
      this.socket.on('start', (ordinal) => {
        observer.next(ordinal);
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  stopQuestion() {
    let observable = new Observable((observer) => {
      this.socket.on('stop', () => {
        observer.next();
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  endTest() {
    let observable = new Observable((observer) => {
      this.socket.on('end', () => {
        observer.next();
      });

      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  submitAnswers(answers) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post('/lecture/submitanswers', JSON.stringify({ answers: answers }), {
        headers: headers,
      })
      .map((res) => {
        var rv = res.json();
        this._logService.logLecture(new LogEvent('Answer submitted', JSON.stringify(rv)));
        return rv;
      });
  }

  emitFreeTextAnswer(answer) {
    this.socket.emit('free-text-answer', answer);
  }

  emitAllAnswers(answers) {
    this.socket.emit('all-answers', answers);
  }

  emitScore(score) {
    this.socket.emit('score', score);
  }

  getScore() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post('/lecture/getscore', JSON.stringify({}), { headers: headers })
      .map((res) => {
        var score = res.json();
        this.emitScore(score);
      });
  }
}

export class Test {
  constructor(
    public noOfQuestions: number,
    public title: string,
    public password: string,
    public roomName: string
  ) {}
}

export class Question {
  constructor(
    public caption: string,
    public ordinal: number,
    public content,
    public answers, // all possible answers
    public multiCorrect,
    public type: string,
    public correctAnswers: number[] // correct answers ordinal
  ) {}

  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      }
    }
    return '-';
  }
}

// export class Test {
//
//     constructor(
//         public noOfQuestions: number,
//         public secondsLeft: number,
//         public prolonged: boolean,
//         public title: string,
//         public currAnswers: number[][],
//         //public currCodeAnswers: string[],
//         public student: string,
//         public cmSkin: string,
//         public imgSrc: string
//     ) {
//
//         if (!(this.currAnswers && this.currAnswers.length)) {
//             this.currAnswers = [];
//             for (let i = 0; i < this.noOfQuestions; ++i) {
//                 this.currAnswers.push([]);
//             }
//         }
//     }
//
// }
//
//
// export class Question {
//
//     constructor(public caption: string,
//                 public ordinal: number,
//                 public currAnswers: number[],
//                 public content,
//                 public answers,
//                 public multiCorrect,
//                 public modeDesc,
//                 public type: string) {
//
//     }
//
// }
