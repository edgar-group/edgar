import { Component, EventEmitter, Input, Output } from 'angular2/core';
import { Question } from './lecturestudent.service';

@Component({
  selector: 'question-cont',
  template: `<div
      class="lecture-q-content"
      [innerHTML]="currQuestion && currQuestion.content"
    ></div>
    <ng-container *ngIf="currQuestion">
      <div *ngIf="currQuestion.type == 'Free text' && !_textAnswerSubmitted">
        <textarea
          id="answerTextArea"
          class="form-control"
          rows="5"
          placeholder="Enter your answer here..."
        ></textarea>
        <br />
        <button type="button" class="btn btn-success btn-lg" (click)="onSubmit()">
          Submit answer
        </button>
      </div>

      <div *ngIf="currQuestion.type == 'Free text' && _textAnswerSubmitted">
        <h1>Answer submitted, please wait for the next question.</h1>
      </div>

      <button
        type="button"
        class="edgar-q-answer"
        *ngFor="#a of currQuestion.answers"
        (click)="onSelect(a)"
        [class.edgar-selected]="isSelected(a)"
      >
        <div *ngIf="!showCorrect" class="edgar-answer-badge badge badge-secondary">
          {{ getLabel([a.ordinal]) }}
        </div>
        <div
          *ngIf="showCorrect"
          class="edgar-answer-badge badge badge-secondary"
          [class.badge-success]="isCorrect(a)"
        >
          {{ getLabel([a.ordinal]) }}
        </div>
        <div style="margin-left: 50px;" class="text-left" [innerHTML]="a.aHtml"></div>
      </button>
    </ng-container> `,
  styles: [
    `
      .lecture-q-content {
        font-size: 30px;
        margin-bottom: 10px;
      }
      .edgar-q-answer {
        min-height: 50px;
        padding: 0.75rem 1.25rem;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, 0.25);
        display: block;
        width: 100%;
        border-radius: 5px;
        margin-bottom: 2px;
      }
      .edgar-q-answer:hover {
        background-color: rgba(0, 0, 0, 0.05);
      }
      .edgar-selected {
        border: darkgreen solid 3px;
        background-color: lemonchiffon !important;
      }
      .edgar-answer-badge {
        width: 35px;
        float: left;
        font-size: 1.5rem;
      }
    `,
  ],
  providers: [],
})
export class QuestionComponent {
  @Input() currQuestion: Question;
  @Input() currAnswers: any[];

  @Output() answerUpdated = new EventEmitter();

  _textAnswerSubmitted: boolean;

  constructor() {
    this._textAnswerSubmitted = false;
  }

  getLabel(ordinal): string {
    return Question.getAnswerLabel(ordinal);
  }

  onSubmit() {
    var answer = (document.getElementById('answerTextArea') as HTMLTextAreaElement).value;
    // document.getElementById('answerTextArea').value = '';

    this.currAnswers[this.currQuestion.ordinal - 1] = [answer];
    this.answerUpdated.emit([answer]);

    this._textAnswerSubmitted = true;
  }

  onSelect(answer) {
    var arrAnswers = this.currAnswers[this.currQuestion.ordinal - 1];
    if (this.currQuestion.multiCorrect) {
      var idx;
      if ((idx = arrAnswers.indexOf(answer.ordinal)) >= 0) {
        arrAnswers.splice(idx, 1);
      } else {
        arrAnswers.push(answer.ordinal);
      }
    } else {
      if (arrAnswers.indexOf(answer.ordinal) >= 0) {
        arrAnswers = [];
      } else {
        arrAnswers = [answer.ordinal];
      }
    }
    this.currAnswers[this.currQuestion.ordinal - 1] = arrAnswers;
    this.answerUpdated.emit(arrAnswers);
  }

  isSelected(answer) {
    return this.currAnswers[this.currQuestion.ordinal - 1].indexOf(answer.ordinal) >= 0;
  }

  isCorrect(answer) {
    return this.currQuestion.correctAnswers.indexOf(answer.ordinal) >= 0;
  }
}
