import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { ScaleItem } from '../common/question';

@Injectable()
export class TestService {
  private _questions: Question[];
  private _prQuestions: Question[][];
  private _test: Test;
  constructor(private _http: Http) {
    this._questions = [];
    this._prQuestions = [];
  }

  getQuestion(num: number) {
    let rv: Question;
    if (this._questions[num]) {
      return Observable.of(this._questions[num]);
    } else {
      return this._http.get(`/test/rev/question/${num}`).map((res) => {
        let q = res.json();
        this._questions[num] = new Question(
          q.ordinal.toString(),
          q.ordinal,
          q.content,
          q.answers,
          q.correctAnswers,
          q.studentAnswers,
          q.multiCorrect,
          q.studentAnswerCode,
          q.studentAnswerText,
          q.correctAnswerCode,
          q.hint,
          q.type,
          q.c_eval_data,
          q.uploaded_files,
          q.scaleItems,
          q.manualComment,
          q.graderImage,
          q.graderName,
          q.score,
          q.scorePerc
        );
        return this._questions[num];
      });
    }
  }
  // router.get('/rev/peer/:peerordinal([0-9]{1,2})/question/:ordinal([0-9]{1,2})',
  getPeerAssesmentQuestion(revNo: number, qNo: number) {
    if (this._prQuestions[revNo] && this._prQuestions[revNo][qNo]) {
      return Observable.of(this._prQuestions[revNo][qNo]);
    } else {
      return this._http.get(`/test/rev/peer/${revNo + 1}/question/${qNo + 1}`).map((res) => {
        let q = res.json();
        if (!this._prQuestions[revNo]) this._prQuestions[revNo] = [];
        this._prQuestions[revNo][qNo] = new Question(
          q.ordinal.toString(),
          q.ordinal,
          q.content,
          q.answers,
          q.correctAnswers,
          q.studentAnswers,
          q.multiCorrect,
          q.studentAnswerCode,
          q.studentAnswerText,
          q.correctAnswerCode,
          q.hint,
          q.type,
          q.c_eval_data,
          q.uploaded_files,
          q.scaleItems,
          q.manualComment,
          q.graderImage,
          q.graderName,
          q.score,
          q.scorePerc
        );
        return this._prQuestions[revNo][qNo];
      });
    }
  }
  /*getStaticQuestion1() {
        return new Question("1", 1, "-- please wait... ---", [], [1], [], false, undefined, undefined, undefined);
    }*/

  getTest() {
    if (this._test) {
      return Observable.of(this._test);
    } else {
      return this._http.get(`/test/rev/instance`).map((res) => {
        let t = res.json();
        if (t.success === false) {
          return null;
        } else {
          if (typeof t.answersOutcome === 'string') {
            t.answersOutcome = JSON.parse(t.answersOutcome);
          }
          this._test = new Test(
            t.title,
            t.questionsNo,
            t.secondsLeft,
            t.prolonged,
            t.correctNo,
            t.incorrectNo,
            t.unansweredNo,
            t.partialNo,
            t.score,
            t.scorePerc,
            t.passed,
            t.answersOutcome,
            t.student,
            t.altId,
            t.altId2,
            t.showSolutions,
            t.cmSkin ? t.cmSkin : 'ambiance',
            t.imgSrc,
            t.typeName,
            t.noReviews,
            t.noReviewQuestions,
            t.usesTicketing
          );
          return this._test;
        }
      });
    }
  }
}
export class Test {
  constructor(
    public title: string,
    public questionsNo: number,
    public secondsLeft: number,
    public prolonged: boolean,
    public correctNo: number,
    public incorrectNo: number,
    public unansweredNo: number,
    public partialNo: number,
    public score: any,
    public scorePerc: any,
    public passed: boolean,
    public answersOutcome: string[],
    public student: string,
    public altId: string,
    public altId2: string,
    public showSolutions: boolean,
    public cmSkin: string,
    public imgSrc: string,
    public type_name: string,
    public noReviews: number,
    public noReviewQuestions: number,
    public usesTicketing: boolean
  ) {}

  public isPA1(): boolean {
    return this.type_name === 'Peer assessment PHASE1';
  }
  public isPA2(): boolean {
    return this.type_name === 'Peer assessment PHASE2';
  }
}
export class Question {
  constructor(
    public caption: string,
    public ordinal: number,
    public content: string,
    public answers: any,
    public correctAnswers: number[],
    public studentAnswers: number[],
    public multiCorrect: boolean,
    public studentAnswerCode: string,
    public studentAnswerText: string,
    public correctAnswerCode: string,
    public hint: string,
    public type: string,
    public c_eval_data: string,
    public uploadedFiles: string,
    public scaleItems: ScaleItem[],
    public manualComment: string,
    public graderImage: string,
    public graderName: string,
    public score: number,
    public scorePerc: number
  ) {}

  public isSQL(): boolean {
    return this.type.toUpperCase().indexOf('SQL') >= 0;
  }
  public isScaleQuestion(): boolean {
    return this.type.toUpperCase().indexOf('SCALE') >= 0;
  }
  public isCodeLang(): boolean {
    return (
      this.type.toUpperCase().indexOf('C-LANG') >= 0 ||
      this.type.toUpperCase().indexOf('JAVA') >= 0 ||
      this.type.toUpperCase().indexOf('CODE') >= 0
    );
  }
  public isJSON(): boolean {
    return this.type.toUpperCase().indexOf('JSON') >= 0;
  }
  public isRunnable(): boolean {
    return this.isCodeLang() || this.isSQL() || this.isJSON();
  }
  public isMultiChoice(): boolean {
    return (
      this.type.toUpperCase().indexOf('ABC') >= 0 || this.type.toUpperCase().indexOf('CHOICE') >= 0
    );
  }

  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      } else if (typeof answers === 'string' && answers.trim() !== '') {
        return '<code/>';
      }
    }
    return '-';
  }
}
