import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterViewInit,
} from 'angular2/core';
import { TestService, Question } from './revtest.service';
import { CodeMirrorComponent } from './../common/codemirror.component';
import { LogService, LogEvent } from './../common/log.service';
import { CodeRunnerService, CodeResult } from './../common/coderunner.service';
import { GridComponent } from './../common/grid/grid.component';
import { Observable } from 'rxjs/Rx';
// import {LogService} from './../common/log.service';
import { CLangResultComponent } from './../common/clangresult/clangresult.component';
import { JSONResultComponent } from './../common/jsonresult/jsonresult.component';
import { ScaleItem } from '../common/question';
declare var $: any; //  to use jQuery in angular!
@Component({
  selector: 'q-content',
  template: `
    <span *ngIf="!(currQuestion && currQuestion.content)"
      ><i class="fa fa-2 fa-spinner fa-spin"></i
    ></span>
    <div *ngIf="currQuestion && (currQuestion.score || currQuestion.score === 0)">
      <span class="bg-light text-primary">
        Score: {{ currQuestion.score }} (={{ (currQuestion.scorePerc * 100).toFixed(1) }}%)
      </span>
    </div>
    <div
      *ngIf="currQuestion && !currQuestion.isScaleQuestion()"
      class="edgar-q-content"
      [innerHTML]="currQuestion && currQuestion.content ? currQuestion.content : 'please wait...'"
    ></div>
    <div
      *ngIf="
        currQuestion &&
        !(currQuestion.correctAnswers && currQuestion.correctAnswers.length) &&
        currQuestion.type !== 'Free text' &&
        !currQuestion.isScaleQuestion()
      "
    >
      <div class="row">
        <div class="col-6">
          Student's answer:
          <div class="edgar-overlap border">
            <code-mirror
              [questionType]="currQuestion.type"
              [currCode]="currQuestion.studentAnswerCode"
              [ordinal]="currQuestion.ordinal"
              [cmSkin]="cmSkin"
              [readOnly]="true"
            ></code-mirror>
            <div *ngIf="currQuestion.manualComment" class="edgar-comment-overlap">
              <button
                class="btn btn-warning col-1 edgar-overlap-btn-w d-flex comment-popover"
                data-toggle="popover"
              >
                <div [innerHTML]="currQuestion.graderImage"></div>
                <h3><i class="fa fa-comment-dots"></i></h3>
              </button>
            </div>
          </div>
        </div>
        <div class="col-6">
          Correct answer:
          <code-mirror
            [questionType]="currQuestion.type"
            [currCode]="currQuestion.correctAnswerCode"
            [ordinal]="currQuestion.ordinal"
            [cmSkin]="cmSkin"
            [readOnly]="true"
          ></code-mirror>
        </div>
      </div>
      <br />
      <div *ngIf="!currQuestion.isJSON()" class="row d-flex justify-content-center">
        <hr />
        <em>Hint: {{ currQuestion.hint }}</em>
        <br />
      </div>
      <div *ngIf="parsed_eval_data[currQuestion.ordinal]">
        <hr />
        <h3>Exam results obtained during submission evaluation:</h3>
        <div class="card card-body">
          <table class="edgar2">
            <tr>
              <th>#</th>
              <th>stdin</th>
              <th>stdout</th>
              <th>expected</th>
              <th>stderr</th>
              <th>percentage</th>
              <th>hint</th>
              <th>mode</th>
              <th>correct?</th>
            </tr>
            <tr *ngFor="#row of parsed_eval_data[currQuestion.ordinal], #i = index">
              <td>{{ i + 1 }}</td>
              <td class="edgar-code">
                <pre>{{ row.input }}</pre>
              </td>
              <td class="edgar-code">
                <pre>{{ row.output }}</pre>
              </td>
              <td class="edgar-code">
                <pre>{{ row.expected }}</pre>
              </td>
              <td class="edgar-code">
                <pre>{{ row.stderr }}</pre>
              </td>
              <td class="edgar-code">{{ row.percentage }}</td>
              <td>{{ row.hint }}</td>
              <td>{{ row.mode }}</td>
              <td>{{ row.isCorrect }}</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="container-fluid">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tabstudent" role="tab"
              ><b>Student's result</b></a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabteacher" role="tab"
              ><b>Correct result</b></a
            >
          </li>
        </ul>

        <div class="tab-content edgar-tab-content">
          <div class="tab-pane fade show active" id="tabstudent" role="tabpanel">
            <ng-container *ngIf="currQuestion.isRunnable()">
              <div class="row d-flex justify-content-center">
                <button class="btn btn-primary m-2" type="button" (click)="rerunStudent()">
                  Rerun student's code
                </button>
              </div>
            </ng-container>
            <ng-container *ngIf="currQuestion.isCodeLang()">
              <clang-result
                [isRevision]="true"
                [result]="getStudentResult()"
                [showHourglass]="getShowHourGlassStudent()"
              ></clang-result>
            </ng-container>
            <ng-container *ngIf="currQuestion.isJSON()">
              <json-result
                [isRevision]="true"
                [c_eval_data]="currQuestion.c_eval_data"
                [result]="getStudentResult()"
              ></json-result>
            </ng-container>
            <div *ngIf="currQuestion.isSQL() && getStudentResult()">
              <div *ngIf="getStudentResult().success">
                <grid [result]="getStudentResult()"></grid>
              </div>
              <div *ngIf="!getStudentResult().success">
                <div class="text-left alert alert-danger">
                  {{ getStudentResult().error.message }}
                  <div *ngIf="getStudentResult().error.position">
                    Position:
                    {{ getStudentResult().error.position }}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tabteacher" role="tabpanel">
            <ng-container *ngIf="currQuestion.isRunnable()">
              <div class="row d-flex justify-content-center">
                <button class="btn btn-primary m-2" type="button" (click)="rerunCorrect()">
                  Rerun teacher's code
                </button>
              </div>
              <clang-result
                [isRevision]="true"
                [result]="getCorrectResult()"
                [showHourglass]="getShowHourGlassCorrect()"
              ></clang-result>
            </ng-container>
            <ng-container *ngIf="currQuestion.isJSON()">
              <json-result [isRevision]="true" [result]="getCorrectResult()"></json-result>
            </ng-container>
            <div *ngIf="currQuestion.isSQL() && getCorrectResult()">
              <div *ngIf="getCorrectResult().success">
                <grid [result]="getCorrectResult()"></grid>
              </div>
              <div *ngIf="!getCorrectResult().success">
                <div class="text-left alert alert-danger">
                  {{ getCorrectResult().error.message }}
                  <div *ngIf="getCorrectResult().error.position">
                    Position:
                    {{ getCorrectResult().error.position }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <ng-container *ngIf="currQuestion && currQuestion.isMultiChoice()">
      <button
        type="button"
        class="edgar-q-answer"
        *ngFor="#a of currQuestion.answers"
        (click)="onSelect(a)"
        [class.edgar-selected]="isSelected(a)"
      >
        <div class="edgar-answer-badge badge badge-secondary" [class.badge-success]="isCorrect(a)">
          {{ getLabel([a.ordinal]) }}
        </div>
        <div style="margin-left: 50px;" class="text-left" [innerHTML]="a.aHtml"></div>
      </button>
    </ng-container>
    <ng-container *ngIf="currQuestion && currQuestion.isScaleQuestion()">
      <div class="scale">
        <label class="statement" [innerHTML]="currQuestion.content"></label>

        <ol class="scale">
          <li class="scale" *ngFor="#si of currQuestion.scaleItems">
            <input
              type="radio"
              name="scale[{{ currQuestion.ordinal }}]"
              value="{{ si.value }}"
              (click)="onSelectScaleItem(si)"
              [checked]="isSelectedScaleItem(si)"
            />
            <label
              [innerHTML]="si.label"
              [class.selected-scale-item]="isSelectedScaleItem(si)"
            ></label>
          </li>
        </ol>
      </div>
    </ng-container>
    <ng-container *ngIf="currQuestion && currQuestion.type === 'Free text'">
      <div class="row">
        <div class="col-sm-12">
          <h3>Student's answer:</h3>
          <code-mirror
            [questionType]="currQuestion.type"
            [currCode]="currQuestion.studentAnswerText ? currQuestion.studentAnswerText : ''"
            [ordinal]="currQuestion.ordinal"
            [cmSkin]="cmSkin"
            [readOnly]="true"
          ></code-mirror>
          <div *ngIf="currQuestion.manualComment" class="edgar-comment-overlap">
            <button
              class="btn btn-warning col-1 edgar-overlap-btn-w d-flex comment-popover"
              data-toggle="popover"
            >
              <div [innerHTML]="currQuestion.graderImage"></div>
              <h3><i class="fa fa-comment-dots"></i></h3>
            </button>
          </div>
        </div>
      </div>
      <br />
      <div class="d-flex justify-content-center bg-light">
        <h5>Hint: {{ currQuestion.hint }}</h5>
      </div>
      <br />
      <div class="row">
        <div class="col-sm-12">
          <h3>Correct answer:</h3>
          <div
            *ngIf="currQuestion && currQuestion.correctAnswerCode"
            [innerHTML]="currQuestion.correctAnswerCode"
          ></div>
        </div>
      </div>
    </ng-container>
    <ng-container *ngIf="currQuestion && currQuestion.uploadedFiles">
      <hr />
      <div class="row">
        <div class="col-sm-12">
          <h2>
            <a href="{{ currQuestion.uploadedFiles }}" target="_blank" download>
              <i class="file-icon fa fa-file"></i> Uploaded files
            </a>
          </h2>
        </div>
      </div>
      <hr />
    </ng-container>
  `,
  styles: [
    `
      .edgar-q-content {
        min-height: 100px;
        margin-bottom: 50px;
      }
      .edgar-q-answer {
        min-height: 50px;
        padding: 0.75rem 1.25rem;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, 0.25);
        display: block;
        width: 100%;
        border-radius: 5px;
        margin-bottom: 2px;
      }
      .edgar-selected {
        border: darkgreen solid 3px;
        background-color: lemonchiffon !important;
      }
      .edgar-answer-badge {
        width: 35px;
        float: left;
        font-size: 1.5rem;
      }
      .nav-tabs li {
        width: 49%;
      }
      .edgar-tab-content {
        padding-left: 25px;
      }

      h1.scale-header {
        padding-left: 4.25%;
        margin: 20px 0 0;
      }

      div.scale .statement {
        display: block;
        font-size: 14px;
        font-weight: bold;
        margin-bottom: 30px;
      }

      ol.scale {
        display: flex;
        padding-left: 0;
        list-style: none;
        justify-content: space-between;
      }

      li.scale {
        position: relative;
        display: flex;
        padding-right: 16px;
        padding-left: 16px;
        text-align: center;
        align-items: center;
        flex-direction: column;
        width: 100%;
      }

      li.scale:not(:first-of-type) input::before {
        left: 0;
      }

      li.scale:first-of-type input::before,
      li.scale:last-of-type input::before {
        width: 50%;
      }

      li.scale:first-of-type input::before {
        left: 50%;
      }

      li.scale input {
        display: flex;
        align-items: center;
        width: 2rem;
        height: 2rem;
      }

      li.scale input::before {
        position: absolute;
        z-index: -1;
        width: 100%;
        height: 4px;
        background-color: green;
        content: '';
      }

      li.scale label {
        margin-top: 8px;
        font-size: 1.1em;
        font-weight: bold;
      }

      .selected-scale-item {
        background-color: yellowgreen;
        border: 1px black solid;
        border-radius: 5px;
      }
      .CodeMirror {
        border: 1px solid #eee;
        height: auto;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  directives: [CodeMirrorComponent, GridComponent, CLangResultComponent, JSONResultComponent],
  providers: [CodeRunnerService, LogService],
})
export class QContentComponent implements OnChanges {
  @Input() currQuestion: Question;
  @Input() cmSkin: string;
  @Input() showSolutions: boolean;
  // @Input() currAnswers: number[][];
  @Output() answerSelected = new EventEmitter();
  studentResults: any;
  correctResults: any;
  showHourGlassStudent: any;
  showHourGlassCorrect: any;
  parsed_eval_data: any;
  constructor(
    private _ref: ChangeDetectorRef,
    private _testService: TestService,
    private _codeRunnerService: CodeRunnerService
  ) {
    this.studentResults = {};
    this.correctResults = {};
    this.showHourGlassStudent = {};
    this.showHourGlassCorrect = {};
    this.parsed_eval_data = {};
  }

  getLabel(ordinal): string {
    return Question.getAnswerLabel(ordinal);
  }
  isSelected(answer) {
    return this.currQuestion.studentAnswers.indexOf(answer.ordinal) >= 0;
  }
  isSelectedScaleItem(si: ScaleItem) {
    return this.currQuestion.studentAnswers.indexOf(si.value) >= 0;
  }
  isCorrect(answer) {
    return this.currQuestion.correctAnswers.indexOf(answer.ordinal) >= 0;
  }

  getStudentResult(): CodeResult {
    return this.studentResults[this.currQuestion.ordinal];
  }
  getCorrectResult(): CodeResult {
    return this.correctResults[this.currQuestion.ordinal];
  }
  getShowHourGlassStudent() {
    return !!this.showHourGlassStudent[this.currQuestion.ordinal];
  }
  getShowHourGlassCorrect() {
    return !!this.showHourGlassCorrect[this.currQuestion.ordinal];
  }
  rerunStudent(): void {
    let qOrdinal: number = this.currQuestion.ordinal;
    let self = this;
    this.showHourGlassStudent[qOrdinal] = true;
    this.studentResults[qOrdinal] = undefined;
    Observable.forkJoin(this._codeRunnerService.getCodeReviewStudentResult(qOrdinal)).subscribe(
      (result) => {
        this.studentResults[qOrdinal] = result[0];
        this.showHourGlassStudent[qOrdinal] = false;
        if (qOrdinal === this.currQuestion.ordinal) {
          self._ref.markForCheck();
        }
      }
    );
  }
  rerunCorrect(): void {
    let qOrdinal: number = this.currQuestion.ordinal;
    this.showHourGlassCorrect[qOrdinal] = true;
    this.correctResults[qOrdinal] = undefined;
    let self = this;
    Observable.forkJoin(this._codeRunnerService.getCodeReviewCorrectResult(qOrdinal)).subscribe(
      (result) => {
        this.correctResults[qOrdinal] = result[0];
        this.showHourGlassCorrect[qOrdinal] = false;
        if (qOrdinal === this.currQuestion.ordinal) {
          self._ref.markForCheck();
        }
      }
    );
  }
  ngOnChanges(change) {
    var self = this;
    if (this.currQuestion && this.currQuestion.ordinal) {
      // not true for PA
      $('.comment-popover[data-toggle="popover"]').popover('hide');
      try {
        this.parsed_eval_data[this.currQuestion.ordinal] = JSON.parse(
          this.currQuestion.c_eval_data
        );
      } catch (error) {
        //console.log('could not parse', this.currQuestion.c_eval_data);
      }
      self._ref.markForCheck();

      setTimeout(function () {
        $('.comment-popover[data-toggle="popover"]').popover('dispose');
        $('.comment-popover[data-toggle="popover"]').popover({
          placement: 'right',
          trigger: 'click',
          html: true,
          title: `<div class="media">${self.currQuestion.graderImage}    
                            <div class="media-body">
                                <h5 class="media-heading">${self.currQuestion.graderName} says:</h5>
                            </div>
                        </div>`,
          content: `<pre>${self.currQuestion.manualComment}</pre>`,
        });
      }, 500);
    }
  }
}
