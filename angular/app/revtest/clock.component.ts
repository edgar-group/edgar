import { Component, Input, OnChanges } from 'angular2/core';
@Component({
  selector: 'clock',
  template: `
    <h3
      *ngIf="caption.length < 10"
      id="aii-countdown"
      [class.alert]="overflow || prolongedOverflow"
      [class.alert-danger]="overflow"
      [class.alert-warning]="prolongedOverflow"
    >
      {{ caption }}
    </h3>
    <div
      *ngIf="caption.length >= 10"
      id="aii-countdown"
      [class.alert]="overflow || prolongedOverflow"
      [class.alert-danger]="overflow"
      [class.alert-warning]="prolongedOverflow"
    >
      {{ caption }}
    </div>
  `,
})
export class ClockComponent implements OnChanges {
  public caption;
  public overflow: boolean;
  public prolongedOverflow: boolean;
  @Input() seconds: number = 0;
  @Input() prolonged: boolean = false;

  constructor() {
    var self = this;
    this.caption = this.getCaption(this.seconds);
  }

  getCaption(seconds): string {
    this.overflow = false;
    this.prolongedOverflow = false;
    if (seconds < 0) {
      if (this.prolonged) {
        this.prolongedOverflow = true;
      } else {
        this.overflow = true;
      }
      seconds = -1 * seconds;
    }
    let h = Math.floor(seconds / (60 * 60));
    let m = Math.floor((seconds - 60 * 60 * h) / 60);
    let s = seconds % 60;

    let days = '';
    if (h > 23) {
      days = Math.floor(h / 24) + ' days ';
      h = h % 24;
    }
    return (
      days + (h < 10 ? '0' : '') + h + ':' + (m < 10 ? '0' : '') + m + ':' + (s < 10 ? '0' : '') + s
    );
  }
  ngOnChanges(change) {
    this.caption = this.getCaption(this.seconds);
  }
}
