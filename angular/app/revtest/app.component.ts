import { Component, ChangeDetectorRef, Injectable, AfterViewInit } from 'angular2/core';
import { QNavComponent } from './qnav.component';
import { QContentComponent } from './qcontent.component';
import { ClockComponent } from './clock.component';
//import {CodeMirrorComponent} from './../common/codemirror.component';
import { LogService, LogEvent } from './../common/log.service';
import { testLogServiceConfigProvider } from './../common/log.service.provider';
import { TestService, Test, Question } from './revtest.service';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { TicketComponent } from './../common/ticket/ticket.component';
declare var $: any;
@Component({
  selector: 'test-review',
  host: {
    '(window: resize)': 'onResize()',
  },
  template: `
    <div class="edgar-header-bg">&nbsp;</div>
    <div *ngIf="_errMessage" class="alert alert-danger text-center">
      {{ _errMessage }}
    </div>
    <div *ngIf="currAnswers === undefined">Please wait, loading test instance...</div>
    <div *ngIf="_test">
      <div
        class="container-fluid edgar-test-header edgar-sticky bg-light d-flex justify-content-between align-items-center"
      >
        <div *ngIf="_test" style="position: fixed;top:5px;left: 260px;">
          <small>{{ _test.title }}</small>
        </div>
        <div class="d-flex align-items-center" style="min-width: 250px">
          <div>
            <img
              *ngIf="_test.scorePerc == 1.0"
              height="75"
              src="/images/edgar.cool.75.png"
              title="It's lonely at the top.."
            />
            <img
              *ngIf="_test.scorePerc != 1.0"
              height="75"
              src="/images/edgar75.png"
              title="Bcs middle-aged men with moustaches make great logos..."
            />
          </div>
          <div *ngIf="_test" class="d-flex  flex-column ml-5" style="flex-shrink: 0;">
            <div>
              <span title="Correct" class="badge badge-success border border-dark">
                <h4>{{ _test.correctNo }}</h4>
              </span>
              <span title="Incorrect" class="badge badge-danger border border-dark ml-1">
                <h4>{{ _test.incorrectNo }}</h4>
              </span>
              <span title="Unanswered" class="badge badge-secondary border border-dark ml-1">
                <h4>{{ _test.unansweredNo }}</h4>
              </span>
              <span title="Partial" class="badge badge-warning border border-dark ml-1">
                <h4>{{ _test.partialNo }}</h4>
              </span>
            </div>
            <div class="mt-3">
              <span class="bg-light text-primary">
                Score: {{ _test.score }} (={{ (_test.scorePerc * 100).toFixed(1) }}%)
              </span>
            </div>
          </div>
        </div>

        <div *ngIf="!_test.isPA2()" class="container-fluid d-flex justify-content-around">
          <q-nav
            [answersOutcome]="_test.answersOutcome"
            (questionSelected)="onQuestionSelected($event)"
          ></q-nav>
        </div>
        <div *ngIf="_test.isPA2()" class="container-fluid d-flex justify-content-around">
          <h4>
            <a href="/test/instance/peerattachment" target="_blank">
              Peer assessment assignment <i class="file-icon fa fa-file"></i
            ></a>
          </h4>
        </div>
        <div *ngIf="_test" class="d-flex align-items-center">
          <div class="profilepic">
            <img height="75" class="rounded-circle" src="{{ _test.imgSrc }}" />
          </div>

          <div>
            <div>{{ _test.student }}</div>
            <div>{{ _test.altId }} {{ _test.altId2 }}</div>
          </div>
          <div class="ml-4 d-flex flex-column">
            <clock [seconds]="getSeconds()" [prolonged]="_test.prolonged"></clock>
            <button class="btn btn-primary" type="submit" (click)="done()">Done</button>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div *ngIf="_test && _test.usesTicketing && _currQuestion && _currQuestion.ordinal">
          <ticket [ordinal]="_currQuestion.ordinal"></ticket>
        </div>
        <ng-container *ngIf="_test.isPA2()">
          <div class="pa2-question" *ngFor="#q of _PA2Questions, #i = index">
            <h2>
              <div class="aii-answer-badge badge badge-light">{{ i + 1 }}</div>
            </h2>
            <div class="row">
              <div class="offset-1 col-10">
                <q-content
                  [currQuestion]="q"
                  [showSolutions]="_test.showSolutions"
                  [cmSkin]="_test.cmSkin"
                >
                </q-content>
              </div>
            </div>
          </div>
        </ng-container>
        <div class="row" *ngIf="!_test.isPA2()">
          <div class="offset-1 col-10">
            <q-content
              [currQuestion]="_currQuestion"
              [showSolutions]="_test.showSolutions"
              [cmSkin]="_test.cmSkin"
            >
            </q-content>
          </div>
        </div>
        <ng-container *ngIf="_test.isPA1()">
          <div class="row">
            <div class="offset-1 col-10">
              <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="nav-item" *ngFor="#r of _reviewLabels, #i = index">
                  <a
                    class="nav-link"
                    [class.active]="i == 0"
                    data-toggle="tab"
                    href="#tabreview{{ i }}"
                    role="tab"
                    ><b>{{ r }}</b>
                  </a>
                </li>
              </ul>
              <div class="tab-content edgar-tab-content">
                <div
                  *ngFor="#r of _reviewLabels, #i = index"
                  class="tab-pane"
                  [class.active]="i == 0"
                  id="tabreview{{ i }}"
                  role="tabpanel"
                >
                  <h4 *ngIf="!_PA1Questions[i]" style="margin: 20px;">
                    Peer assessment pending...
                  </h4>
                  <div class="pa2-question" *ngFor="#q of _PA1Questions[i], #j = index">
                    <h2>
                      <div class="aii-answer-badge badge badge-light">
                        {{ j + 1 }}
                      </div>
                    </h2>
                    <div class="row">
                      <div class="offset-1 col-10">
                        <q-content
                          [currQuestion]="q"
                          [showSolutions]="_test.showSolutions"
                          [cmSkin]="_test.cmSkin"
                        >
                        </q-content>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </ng-container>
      </div>
    </div>
  `,
  styles: [
    `
      .pa2-question {
        border: 2px solid lightgray;
        border-radius: 10px;
        margin: 10px;
        padding: 10px;
      }
    `,
  ],
  directives: [QNavComponent, ClockComponent, QContentComponent, TicketComponent], // ROUTER_DIRECTIVES CodeMirrorComponent
  providers: [TestService, LogService, testLogServiceConfigProvider, WINDOW_PROVIDERS],
})
@Injectable()
export class AppComponent implements AfterViewInit {
  _currQuestion: Question;
  _test: Test;
  _errMessage: string;
  _win: Window;
  _PA1Questions: Question[][];
  _PA2Questions: Question[];
  _reviewLabels: string[];
  constructor(private _testService: TestService, private _ref: ChangeDetectorRef, win: WINDOW) {
    this._win = win.nativeWindow;
    this._testService.getTest().subscribe((test) => {
      if (test === null) {
        this._win.location.href = '/';
      } else {
        this._test = test;
        if (this._test.isPA2()) {
          this._PA2Questions = [];
          for (let i = 1; i <= this._test.questionsNo; ++i) {
            this._PA2Questions.push(
              new Question(
                'dummy',
                1,
                '<i class="fa fa-2 fa-spinner fa-spin"></i> please wait for question ' + i + '...',
                undefined,
                [],
                [],
                false,
                '',
                '',
                'Scale dummy question',
                '',
                'Scale question',
                '',
                null,
                [],
                null,
                null,
                null,
                null,
                null
              )
            );
            this._testService.getQuestion(i).subscribe((q) => {
              this._PA2Questions[i - 1] = q;
              this._ref.markForCheck();
            });
          }
        } else {
          this._testService.getQuestion(1).subscribe((q) => {
            this._currQuestion = q;
            this._ref.markForCheck();
          });
          if (this._test.isPA1()) {
            this._PA1Questions = [];
            this._reviewLabels = [];
            for (let i = 0; i < this._test.noReviews; i++) {
              this._reviewLabels.push(`Peer assessment #${i + 1}`);
              for (let j = 0; j < this._test.noReviewQuestions; ++j) {
                this._testService.getPeerAssesmentQuestion(i, j).subscribe((q) => {
                  if (!this._PA1Questions[i]) this._PA1Questions[i] = [];
                  this._PA1Questions[i][j] = q;
                  this._ref.markForCheck();
                });
              }
            }
          }
        }
        this._ref.markForCheck();
      }
    });
  }
  ngAfterViewInit() {
    var self = this;
    setTimeout(function () {
      self.onResize();
    }, 500);
  }
  onResize() {
    $('.edgar-header-bg').height($('.edgar-sticky').height() + 75);
  }
  getSeconds() {
    return this._test.secondsLeft;
  }
  done() {
    this._win.location.href = '/';
  }
  onQuestionSelected($event) {
    this._testService.getQuestion($event.ordinal).subscribe((q) => {
      this._currQuestion = q;
    });
  }
}
