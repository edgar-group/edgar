import { Component, Input } from 'angular2/core';
import { QuestionComponent } from './question.component';
import { LectureTeacherService, Question, Test } from './lectureteacher.service';

declare var $: any;

@Component({
  selector: 'results',
  template: `<!--<div class="container">-->
    <!--<div class="row">-->
    <div class="tab" role="tabpanel">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" (click)="updateStat()">
          <a role="tab" data-toggle="tab" href="#stats">Statistics</a>
        </li>
        <li role="presentation" class="active">
          <a role="tab" data-toggle="tab" href="#questions">All questions</a>
        </li>
        <li role="presentation">
          <a role="tab" data-toggle="tab" href="#results">Results</a>
        </li>
      </ul>

      <div class="tab-content tabs">
        <div id="stats" role="tabpanel" class="tab-pane fade">
          <div class="col-10 offset-1">
            <canvas id="barChart"></canvas>
          </div>
        </div>
        <div id="questions" role="tabpanel" class="tab-pane fade in active">
          <div class="row">
            <div style="margin-top: 20px; font-size: 25px;" class="col-1">
              <i
                *ngIf="!_isFirstQuestion"
                class="fa fa-chevron-left"
                (click)="showPrevQuestion()"
              ></i>
            </div>
            <div style="margin-top: 10px" class="col-8 offset-1">
              <question-cont
                [currQuestion]="_currQuestion"
                [studAnswers]="_studAnswers"
                [freeTextAnswers]="_freeTextAnswers"
                [showCorrect]="true"
              >
              </question-cont>
            </div>
            <div
              *ngIf="!_isLastQuestion"
              style="margin-top: 20px; font-size: 25px;"
              class="col-1 offset-1"
            >
              <i class="fa fa-chevron-right" (click)="showNextQuestion()"></i>
            </div>
          </div>
        </div>
        <div id="results" role="tabpanel" class="tab-pane fade">
          <div class="row">
            <div class="col-1 offset-1">
              <label>#</label>
            </div>
            <div class="col-4">
              <label>First name</label>
            </div>
            <div class="col-4">
              <label>Last name</label>
            </div>
            <div class="col-2">
              <label>Score</label>
            </div>
          </div>

          <div class="row" *ngFor="#s of scores">
            <div class="col-1 offset-1">
              {{ scores.indexOf(s) + 1 }}
            </div>
            <div class="col-4">
              {{ s.score.first_name }}
            </div>
            <div class="col-4">
              {{ s.score.last_name }}
            </div>
            <div class="col-2">
              {{ s.score.score }}
            </div>
            <!--{{ p.score.first_name + " " + p.score.last_name + " " + p.score.score }}-->
          </div>
        </div>
      </div>
    </div>
    <!--</div>-->
    <!--</div>-->`,
  styles: [
    `
      #barChart {
        margin-bottom: 30px;
      }

      .fa:hover {
        cursor: pointer;
      }

      a:hover,
      a:focus {
        outline: none;
        text-decoration: none;
      }

      .tab .nav-tabs {
        position: relative;
        text-align: center !important;
        border-bottom: 0 none;
        background: #ffffff;
        /*box-shadow: 0 8px 8px -6px black;*/
      }

      .tab .nav-tabs li {
        text-align: center;
      }

      .tab .nav-tabs li a {
        font-size: 20px;
        font-weight: 600;
        /*color: #999;*/
        text-transform: uppercase;
        padding: 15px 30px;
        background: #ffffff;
        margin-right: 0;
        border-radius: 0;
        border: 1px solid #d6d6d6;
        border-right: none;
        border-bottom: 3px solid #d6d6d6;
        position: relative;
        transition: all 0.5s ease 0s;
      }

      .tab .nav-tabs li:last-child a,
      .tab .nav-tabs li:last-child.active a,
      .tab .nav-tabs li:last-child a:hover {
        border-right: 1px solid #d6d6d6;
      }

      .tab .nav-tabs li a:hover,
      .tab .nav-tabs li.active a {
        color: #6a0000;
        border-bottom: 3px solid #6a0000;
        border-right: none;
      }

      @media only screen and (max-width: 768px) {
        .tab .nav-tabs li {
          width: 100%;
          border-right: 1px solid #d6d6d6;
          margin-bottom: 8px;
        }
      }

      .tabs {
        margin-top: 20px;
      }

      .tabs > #results {
        font-size: 1.5vw;
        text-align: left;
      }

      .table {
        border: 1px solid gray;
      }
    `,
  ],
  directives: [QuestionComponent],
  providers: [LectureTeacherService],
})
export class ResultsComponent {
  @Input() noOfQuestion;
  @Input() scores;
  @Input() percScores;

  _test: Test;
  _currQuestion: Question;
  _studAnswers = [];
  _isLastQuestion: boolean;
  _isFirstQuestion: boolean;
  _freeTextAnswers = [];
  chartOption: any;

  constructor(private _lectureTeacherService: LectureTeacherService) {
    this._lectureTeacherService.getTest().subscribe((test) => {
      this._test = test;

      this._lectureTeacherService.getQuestion(1).subscribe((q) => {
        this._currQuestion = q;
        // console.log(this._currQuestion);
        this._isFirstQuestion = true;
        if (this._currQuestion.ordinal === this._test.noOfQuestions) {
          this._isLastQuestion = true;
        } else {
          this._isLastQuestion = false;
        }

        this._lectureTeacherService
          .getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length)
          .subscribe((answers) => {
            // console.log(answers);
            this._studAnswers = answers;
          });
      });
    });
  }

  showNextQuestion() {
    this._lectureTeacherService.getQuestion(this._currQuestion.ordinal + 1).subscribe((q) => {
      this._currQuestion = q;
      // console.log(this._currQuestion);

      this._isFirstQuestion = false;
      if (this._currQuestion.ordinal === this._test.noOfQuestions) {
        this._isLastQuestion = true;
      }

      this._lectureTeacherService
        .getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length)
        .subscribe((answers) => {
          // console.log(answers);
          this._studAnswers = answers;
        });
    });
  }

  showPrevQuestion() {
    this._lectureTeacherService.getQuestion(this._currQuestion.ordinal - 1).subscribe((q) => {
      this._currQuestion = q;
      // console.log(this._currQuestion);

      this._isLastQuestion = false;
      if (this._currQuestion.ordinal === 1) {
        this._isFirstQuestion = true;
      }

      this._lectureTeacherService
        .getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length)
        .subscribe((answers) => {
          // console.log(answers);
          this._studAnswers = answers;
        });
    });
  }
}
