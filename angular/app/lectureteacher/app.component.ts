import { Component, Injectable, OnInit } from 'angular2/core';
import { LectureTeacherService, Question, Test } from './lectureteacher.service';
import { LogService } from '../common/log.service';
import { testLogServiceConfigProvider } from './../common/log.service.provider';
import { TestService } from '../test/test.service';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { QuestionComponent } from './question.component';
import { ResultsComponent } from './results.component';
import { AllQuestionsComponent } from './allquestions.component';

@Component({
  selector: 'teacher-app',

  template: `<div *ngIf="_test" class="row room-header">
      <div class="col-2">
        <h2 id="roomName">{{ _test.roomName }}</h2>
      </div>
      <div class="col-4">
        <h1>{{ _test.title }}</h1>
      </div>
      <div *ngIf="_test && !_started" class="col-2 offset-2">
        <button
          type="button"
          id="startButton"
          class="btn btn-success btn-lg"
          (click)="startLecture()"
        >
          Start
        </button>
      </div>
      <div *ngIf="_currQuestion" class="col-3 offset-1">
        <button
          *ngIf="!_showCorrect && !_showResults && _currQuestion.type != 'Free text'"
          type="button"
          class="btn btn-success btn-lg"
          (click)="showCorrect()"
        >
          Show correct
        </button>
        <button
          *ngIf="!_showCorrect && !_showResults && _currQuestion.type == 'Free text'"
          type="button"
          class="btn btn-success btn-lg"
          (click)="showAnswers()"
        >
          Show answers
        </button>
        <button
          *ngIf="!_isLastQuestion"
          type="button"
          class="btn btn-success btn-lg"
          (click)="showNextQuestion()"
        >
          Next question
        </button>
        <button
          *ngIf="_isLastQuestion && !_showResults"
          type="button"
          id="endButton"
          class="btn btn-danger btn-lg"
          (click)="endTest()"
        >
          End test
        </button>
        <button
          *ngIf="_showResults"
          type="button"
          id="resultsButton"
          class="btn btn-success btn-lg"
          (click)="getResults()"
        >
          Results
        </button>
      </div>
    </div>
    <br />
    <div *ngIf="_test && !_started">
      <div class="row">
        <div class="jumbotron col-5 offset-2">
          <h1 class="link">JOIN AT:</h1>
          <br />
          <h1 class="link">edgar.fer.hr/q</h1>
          <br />
          Room name:
          <h1 id="room">{{ _test.roomName }}</h1>
        </div>
        <div class="jumbotron col-2 offset-1">
          <h2 class="link"><i class="fa fa-user fa-5x"></i></h2>
          <h1 class="usrcount">{{ userCount }}</h1>
        </div>
      </div>
    </div>
    <div *ngIf="_currQuestion && !_showResults">
      <div class="col-8 offset-2">
        <question-cont
          [currQuestion]="_currQuestion"
          [studAnswers]="_studAnswers"
          [freeTextAnswers]="_freeTextAnswers"
          [showCorrect]="_showCorrect"
        >
        </question-cont>
      </div>
    </div>
    <div *ngIf="_showResults">
      <div class="col-10 offset-1">
        <all-questions [noOfQuestions]="_test.noOfQuestions" [allStudAns]="_allStudAns">
        </all-questions>
        <!--<results [noOfQuestion]="_test.noOfQuestions" [scores]="_scores" [percScores]="_percScores"></results>-->
      </div>
      <div class="col-1"></div>
    </div> `,

  styles: [
    `
      .room-header {
        border-bottom: 3px #9de5fb solid;
        padding-bottom: 10px;
        margin-top: 10px;
        margin-bottom: 5px;
        overflow: hidden;
      }

      #roomName {
        color: #6a0000;
      }

      .jumbotron {
        background: #f5f3f5;
        box-shadow: 0 8px 16px -6px black;
      }

      .link {
        font-size: 3vw;
      }

      .usrcount {
        font-size: 6vw;
        color: #6a0000;
        font-weight: bold;
      }

      #room {
        color: #6a0000;
        font-weight: bold;
        font-size: 6vw;
      }
    `,
  ],
  directives: [QuestionComponent, ResultsComponent, AllQuestionsComponent],
  providers: [
    LectureTeacherService,
    TestService,
    testLogServiceConfigProvider,
    LogService,
    WINDOW_PROVIDERS,
  ],
})
@Injectable()
export class AppComponent implements OnInit {
  _currQuestion: Question;
  _test: Test;
  _win: Window;
  _started: boolean;
  _showCorrect: boolean;
  _isLastQuestion: boolean;
  _showResults: boolean;
  connection;
  _studAnswers = [];
  _allStudAns: any[];
  _scores = [];
  _percScores: number[];
  _freeTextAnswers = [];
  _allAnswers: any[];
  userCount: number;

  constructor(private _lectureTeacherService: LectureTeacherService, win: WINDOW) {
    this._win = win.nativeWindow;
    this._started = false;
    this._showCorrect = false;
    this._isLastQuestion = false;
    this._showResults = false;
    this._percScores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this._allStudAns = [];
    this.userCount = 0;
    this._lectureTeacherService.getTest().subscribe((test) => {
      if (test === null) {
        this._win.location.href = '/lecture/startteacher';
      } else {
        this._test = test;
        // console.log(test);

        for (var _i = 0; _i < this._test.noOfQuestions; _i++) {
          this._allStudAns.push([]);
        }

        this.connection = this._lectureTeacherService.connect();
      }

      // this._lectureTeacherService.getFreeTextAnswer()
      //     .subscribe(answer => {
      //         this._freeTextAnswers.push(answer);
      //     });
      this._lectureTeacherService.userJoined().subscribe((user) => {
        this.userCount++;
      });
      this._lectureTeacherService.getAllAnswers().subscribe((answers) => {
        console.log(answers);
      });

      // this._lectureTeacherService.getScore()
      //     .subscribe(score => {
      //         this._scores.push(score);
      //
      //         // sort students' scores
      //         this._scores.sort((s1, s2) => {
      //             if (s1.score.score > s2.score.score)
      //                 return -1;
      //             if (s1.score.score < s2.score.score)
      //                 return 1;
      //             return 0;
      //         });
      //
      //         let roundPerc = parseInt((score.score.score_perc*10).toString());
      //         this._percScores[roundPerc]++;
      //
      //     });
    });
  }

  startLecture() {
    this._started = true;
    this._lectureTeacherService.getQuestion(1).subscribe((q) => {
      this._currQuestion = q;
      // console.log(this._currQuestion);

      if (this._currQuestion.ordinal === this._test.noOfQuestions) {
        this._isLastQuestion = true;
      }
    });
    this._lectureTeacherService.startStudent(1);
  }

  showCorrect() {
    // if (this._currQuestion.ordinal === this._test.noOfQuestions) {
    //     this._isLastQuestion = true;
    // }

    this._lectureTeacherService
      .getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length)
      .subscribe((answers) => {
        this._studAnswers = answers;
        // console.log(answers);
        // this._allStudAns.push(answers);
        this._allStudAns[this._currQuestion.ordinal - 1] = answers;
        console.log(this._allStudAns);
      });

    this._showCorrect = true;
    this._lectureTeacherService.stopQuestion();
  }

  showAnswers() {
    this._lectureTeacherService
      .getStudentAnswers(this._currQuestion.ordinal, 0)
      .subscribe((answers) => {
        this._studAnswers = answers;
        this._freeTextAnswers = answers;
        console.log(this._freeTextAnswers);
        // console.log(answers);
        // this._allStudAns.push(answers);
        this._allStudAns[this._currQuestion.ordinal - 1] = answers;
        console.log(this._allStudAns);
      });
    this._showCorrect = true;
    this._lectureTeacherService.stopQuestion();
  }

  showNextQuestion() {
    this._lectureTeacherService
      .getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length)
      .subscribe((answers) => {
        this._studAnswers = answers;
        // this._allStudAns.push(answers);
        this._allStudAns[this._currQuestion.ordinal - 1] = answers;
      });

    let newOrdinal = this._currQuestion.ordinal + 1;

    this._lectureTeacherService.getQuestion(this._currQuestion.ordinal + 1).subscribe((q) => {
      this._currQuestion = q;
      // console.log(this._currQuestion);

      if (this._currQuestion.ordinal === this._test.noOfQuestions) {
        this._isLastQuestion = true;
      }
    });

    this._freeTextAnswers = [];

    this._showCorrect = false;
    this._lectureTeacherService.startStudent(newOrdinal);
  }

  endTest() {
    let self = this;
    this._lectureTeacherService.endTest().subscribe((result) => {
      // igor override - don't need this, lets go straight to relational db and classic page:
      self.getResults();
      // self._lectureTeacherService.getStudentAnswers(this._currQuestion.ordinal, this._currQuestion.answers.length).
      //     subscribe(answers => {
      //         self._studAnswers = answers;
      //         // this._allStudAns.push(answers);
      //         self._allStudAns[this._currQuestion.ordinal - 1] = answers;
      //         self._showResults = true;
      //     });
    });
  }

  getResults() {
    this._win.location.href = '/lecture/instances/getresults';
  }

  ngOnInit() {
    // this.connection = this._lectureTeacherService.connect();
  }

  // sendMessage(){
  //     this._lectureTeacherService.sendMessage("Booook!");
  // }
}
