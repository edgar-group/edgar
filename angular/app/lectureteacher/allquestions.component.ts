import { Component, Input, OnChanges, OnInit } from 'angular2/core';
import { QuestionComponent } from './question.component';
import { LectureTeacherService, Question } from './lectureteacher.service';

@Component({
  selector: 'all-questions',
  template: `<div class="row">
    <div style="margin-top: 20px; font-size: 25px;" class="col-1">
      <i *ngIf="!_isFirstQuestion" class="fa fa-chevron-left" (click)="showPrevQuestion()"></i>
    </div>
    <div style="margin-top: 10px" class="col-8 offset-1">
      <question-cont
        [currQuestion]="_currQuestion"
        [studAnswers]="_studAnswers"
        [freeTextAnswers]="_freeTextAnswers"
        [showCorrect]="true"
      >
      </question-cont>
    </div>
    <div *ngIf="!_isLastQuestion" style="margin-top: 20px; font-size: 25px;" class="col-1 offset-1">
      <i class="fa fa-chevron-right" (click)="showNextQuestion()"></i>
    </div>
  </div>`,
  styles: [
    `
      .fa:hover {
        cursor: pointer;
      }
    `,
  ],
  directives: [QuestionComponent],
  providers: [LectureTeacherService],
})
export class AllQuestionsComponent {
  @Input() noOfQuestions;
  @Input() allStudAns;

  _currQuestion: Question;
  _studAnswers = [];
  _freeTextAnswers = [];
  _isFirstQuestion: boolean;
  _isLastQuestion: boolean;

  constructor(private _lectureTeacherService: LectureTeacherService) {
    this._lectureTeacherService.getQuestion(1).subscribe((q) => {
      this._currQuestion = q;
      this._studAnswers = this.allStudAns[q.ordinal - 1];

      if (q.type == 'Free text') {
        this._freeTextAnswers = this.allStudAns[q.ordinal - 1];
      }

      // console.log(this._currQuestion);
      this._isFirstQuestion = true;
      if (this._currQuestion.ordinal === this.noOfQuestions) {
        this._isLastQuestion = true;
      } else {
        this._isLastQuestion = false;
      }
    });
  }

  showPrevQuestion() {
    this._lectureTeacherService.getQuestion(this._currQuestion.ordinal - 1).subscribe((q) => {
      this._currQuestion = q;
      this._studAnswers = this.allStudAns[q.ordinal - 1];
      // console.log(this._currQuestion);

      if (q.type == 'Free text') {
        this._freeTextAnswers = this.allStudAns[q.ordinal - 1];
      }

      this._isLastQuestion = false;
      if (this._currQuestion.ordinal === 1) {
        this._isFirstQuestion = true;
      }
    });
  }

  showNextQuestion() {
    this._lectureTeacherService.getQuestion(this._currQuestion.ordinal + 1).subscribe((q) => {
      this._currQuestion = q;
      this._studAnswers = this.allStudAns[q.ordinal - 1];
      // console.log(this._currQuestion);

      if (q.type == 'Free text') {
        this._freeTextAnswers = this.allStudAns[q.ordinal - 1];
      }

      this._isFirstQuestion = false;
      if (this._currQuestion.ordinal === this.noOfQuestions) {
        this._isLastQuestion = true;
      }
    });
  }
}
