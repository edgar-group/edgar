import { Injectable, OpaqueToken } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from './log.service';

export const CODERUNNER_SERVICE_CONFIG = new OpaqueToken('coderunner.service.config');

export interface CodeRunnerServiceConfig {
  codeResultUrl: string;
  hbUrl: string;
}

export const EXERCISE_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  codeResultUrl: '/test/log',
  hbUrl: '/test/hb',
};

export const TEST_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  codeResultUrl: '/exercise/log',
  hbUrl: '/exercise/hb',
};

export const TUTORIAL_CODERUNNER_SERVICE_CONFIG: CodeRunnerServiceConfig = {
  codeResultUrl: '/test/log',
  hbUrl: '/test/hb',
};

@Injectable()
export class CodeRunnerService {
  private _urlStub: string;

  constructor(
    private _http: Http,
    private _logService: LogService /*, private _config: CodeRunnerServiceConfig*/
  ) {
    const path = window.location.pathname.split('/');
    this._urlStub = path[1];
  }
  private _getCodeResult(
    ordinal,
    code2: string,
    log: boolean,
    urlstub: string,
    itemModifier: string = 'question',
    programmingLanguageId: string
  ) {
    if (log) {
      this._logService.log(new LogEvent('Running code', code2));
    }
    var headers = new Headers();
    //headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Content-Type', 'application/json');
    //return this._http.post(`/${urlstub}/question/exec/${ordinal}`, `code=${encodeURIComponent(code)}`, {
    return this._http
      .post(
        `/${urlstub}/${itemModifier}/exec/${ordinal}`,
        JSON.stringify({
          code: code2,
          id_programming_language: programmingLanguageId || 0,
        }),
        {
          headers: headers,
        }
      )
      .map((res) => {
        var rv = res.json();
        if (log) {
          var toLog = res.json();
          // removing potential lenghty data and coderesult properties
          // they are already logged directly from the server to a separate compressed "testlogdetails" collection
          delete toLog.data;
          delete toLog.coderesult;
          this._logService.log(new LogEvent('Code exec result received', JSON.stringify(toLog)));
        }
        return new CodeResult(
          rv.success,
          rv.error,
          rv,
          rv.db,
          rv.warning,
          rv.coderesult,
          rv.score,
          rv.runStats
        );
      });
  }
  private _getCodeReviewResult(ordinal, urlstub: string): Observable<CodeResult> {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
      .post(`/test/rev/question/exec/${urlstub}/${ordinal}`, '', {
        headers: headers,
      })
      .map((res) => {
        var rv = res.json();
        return new CodeResult(
          rv.success,
          rv.error,
          rv,
          rv.db,
          rv.warning,
          rv.coderesult,
          rv.score,
          undefined
        );
      });
  }
  getCodeResult(
    ordinal,
    code: string,
    programmingLanguageId: string,
    isPlayground?: boolean
  ): Observable<CodeResult> {
    return this._getCodeResult(
      ordinal,
      code,
      true,
      this._urlStub,
      isPlayground ? 'code' : 'question',
      programmingLanguageId
    );
  }

  getCodeReviewStudentResult(ordinal): Observable<CodeResult> {
    return this._getCodeReviewResult(ordinal, 'student');
  }
  getCodeReviewCorrectResult(ordinal): Observable<CodeResult> {
    return this._getCodeReviewResult(ordinal, 'correct');
  }
}
export class RunStats {
  id: number;
  can_run: boolean;
  ordinal: number;
  qcnt: number;
  tcnt: number;
  timeout: number;
  time_wait: number;
}

export class CodeResult {
  constructor(
    public success: boolean,
    public error: string,
    public rs: any, // SQLData
    public db: string,
    public warning: string,
    public codeResult: any, // C data
    public score: any,
    public runStats: RunStats[]
  ) {
    if (this.codeResult) this.rs = undefined; // TODO: this is a mess, DB and C results are crammed into the same object
  }
}
