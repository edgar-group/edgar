import { Component, Input, OnInit, OnChanges } from 'angular2/core';
import { CodeResult } from './../coderunner.service'; // to be changed

@Component({
  selector: 'clang-result',
  //template: ` <p>Result:  <span [innerHTML]="getResult()"></span></p>
  // <div *ngIf="getWarning()" class="alert alert-warning text-xs-left">{{getWarning()}}</div>
  template: `
    <div *ngIf="result && result.score && result.score.hint && !isKidsMode()">
      <hr />
      <p>
        Hint (kids mode is off <i class="fas fa-user-ninja"></i>):
        <span [innerHTML]="formatHint()"></span>
      </p>
      <hr />
    </div>
    <div *ngIf="isRunning()" class="text-center">
      <div class="text-center">
        <h2>Please wait, your answers (and correct answers) are being reevaluated.</h2>
      </div>
      <img
        src="/images/waiting.gif"
        alt="Patience, grasshoper"
        style="width: 480px; height: 317px; left: 0px; top: 0px;"
      />
    </div>
    <div [innerHTML]="getHtml()"></div>
  `,
})
export class CLangResultComponent implements OnChanges {
  // @Input() c_eval_data : any;
  @Input() result: CodeResult;
  //parsed_eval_data : any;
  @Input() isRevision: boolean;
  @Input() showHourglass: boolean;

  constructor() {}
  isRunning(): boolean {
    return !!this.showHourglass;
  }

  isKidsMode(): boolean {
    return this.result.score.is_correct !== undefined;
  }
  // getParsedEvalData() {
  //     // if (!this.parsed_eval_data) {
  //     //     this.parsed_eval_data = JSON.parse(this.c_eval_data);
  //     // }
  //     return JSON.parse(this.c_eval_data); //this.parsed_eval_data;
  // }
  formatHint(): string {
    return this.result.score.hint.replace(/\n/g, '<br>');
  }
  getHtml(): string {
    return this.result ? this.result.codeResult : '';
  }
  // getResult() : string {
  //     console.log('CLangResultComponent:', this.result);
  //     var score : string;
  //     if (this.result.score) {
  //         if (this.isRevision) {
  //             score = `Score: ${this.result.score.score} (=${100 * this.result.score.score_perc}%)`;
  //         } else {
  //             score = `Score: ${100 * this.result.score.score_perc}%`;
  //         }
  //         if (this.result.score.is_correct) {
  //             return `<span style="background-color:lime;">
  //                 <i class="fa fa-smile-o" aria-hidden="true"></i> Correct! Well done! </span>
  //                 ${score}`;
  //         } else if (this.result.score.is_partial) {
  //             return `<span style="background-color:yellow;">
  //                <i class="fa fa-frown-o" aria-hidden="true"></i><i class="fa fa-smile-o" aria-hidden="true"></i> Partial, almost there... </span>
  //                 ${score}`;
  //         } else {
  //             return `<span style="background-color:lemonchiffon;">
  //                <i class="fa fa-frown-o" aria-hidden="true"></i> Missed it. </span>
  //                 ${score}`;
  //         }
  //     } else {
  //         return '';
  //     }
  // }
  // getWarning() : string {
  //     if (this.result.warning) {
  //         return this.result.warning;
  //     } else {
  //         return null;
  //     }
  // }
  ngOnChanges(change) {}
}
