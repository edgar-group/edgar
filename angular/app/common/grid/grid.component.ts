import { Component, Input, OnInit, OnChanges } from 'angular2/core';
import { Sorter } from './sorter';
import { CodeResult } from './../coderunner.service';

@Component({
  selector: 'grid',
  template: ` <p>
      Result (count = {{ getCount() }}, db = {{ result.db }} <span [innerHTML]="getHint()"></span>):
    </p>
    <div *ngIf="getWarning()" class="alert alert-warning text-left">
      {{ getWarning() }}
    </div>
    <table *ngIf="hasRows()" class="table table-striped">
      <tr>
        <td class="edgar-code">#</td>
        <td *ngFor="#col of result.rs.data.fields, #i = index" class="edgar-code edgar-code-header">
          <a (click)="sort('C' + i)">{{ col.name }}</a>
        </td>
      </tr>
      <tr *ngFor="#row of result.rs.data.rows, #i = index">
        <td class="edgar-code">{{ i + 1 }}</td>
        <td
          *ngFor="#col of result.rs.data.fields, #i = index"
          class="edgar-code"
          [class.text-muted]="row['C' + i] == null"
        >
          {{ formatValue(row['C' + i]) }}
        </td>
      </tr>
    </table>`,
})
export class GridComponent implements OnChanges {
  columns: Array<string>;
  rows: Array<any>;
  @Input() result: CodeResult;
  constructor() {}
  sorter = new Sorter();

  sort(key) {
    this.sorter.sort(key, this.result.rs.data.rows);
  }
  formatValue(val) {
    return val == null ? 'null' : val;
  }
  hasRows() {
    return this.result && this.result.rs && this.result.rs.data && this.result.rs.data.rows;
  }
  getCount() {
    if (this.hasRows()) {
      return this.result.rs.data.rows.length;
    } else {
      return 0;
    }
  }
  getHint(): string {
    if (this.result.rs.score) {
      if (this.result.rs.score.is_correct) {
        return ', <span style="background-color:lime;"> <i class="far fa-smile" aria-hidden="true"></i> Correct! Well done! </span>';
      } else {
        return (
          ', <span style="background-color:lemonchiffon;"> <i class="far fa-frown" aria-hidden="true"></i> Missed it. Hint: ' +
          this.result.rs.score.hint +
          '</span>'
        );
      }
    } else {
      return '';
    }
  }
  getWarning(): string {
    if (this.result.warning || this.result.error) {
      return (
        (this.result.warning ? JSON.stringify(this.result.warning) : '') +
        (this.result.error ? JSON.stringify(this.result.error) : '')
      );
    } else {
      return null;
    }
  }
  ngOnChanges(change) {}
}
