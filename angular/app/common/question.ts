export class Question {
  public uploadNumbers: number[];
  public uploadFileNames: string[];
  public uploaded: boolean[];
  public runs: number;
  constructor(
    public caption: string,
    public ordinal: number,
    public currAnswers: number[],
    public content,
    public answers,
    public multiCorrect,
    public modeDesc,
    public type: string,
    public programmingLanguages: ProgrammingLanguage[],
    public attachments: Attachment[],
    public scaleItems: ScaleItem[],
    public canUpload: boolean
  ) {}
  initUploadVars(uploadFileNo): boolean {
    if (uploadFileNo && !(this.uploadNumbers && this.uploadNumbers.length)) {
      this.uploadNumbers = Array(uploadFileNo)
        .fill(0)
        .map((x, i) => i);
      this.uploadFileNames = Array(uploadFileNo).fill('');
      this.uploaded = Array(uploadFileNo).fill(false);
      return true;
    } else {
      return false;
    }
  }
  // TODO: this is hard-coded for the time being, add this as a question property or something clever...
  supportsUpload(): boolean {
    return this.canUpload && this.type.trim().toLowerCase() === 'free text';
  }
  // returns the label both for the:
  //  ABC questions (to choose from)
  // for the navbar - qnav component
  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers.multichoice && answers.multichoice.length) {
        return answers.multichoice
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      } else if (
        answers.codeAnswer &&
        answers.codeAnswer.code &&
        answers.codeAnswer.code.trim() !== ''
      ) {
        return '<i class="fas fa-code"></i>';
      }
    }
    return '-';
  }
  htmlEncode(str: string): string {
    return String(str)
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;');
  }
  getQAFormatted(answers: any): string {
    let content = '(unanswered)';
    if (answers) {
      if (this.type === 'Scale (questionnaire)') {
        let scaleItems = this.scaleItems;
        content = answers.multichoice
          .map(function (num: number) {
            let sameValueItems = scaleItems.filter((item) => item.value == num);
            if (sameValueItems.length > 1) {
              alert('Multiple scale items with the same value!?');
            }
            let label =
              sameValueItems && sameValueItems.length > 0 ? sameValueItems[0].label : 'Error!';
            return `<h2 class="text-left badge badge-success ml-5">
                                    ${label}
                                  </h2>`;
          })
          .join();
      } else if (answers.multichoice && answers.multichoice.length) {
        let answersText = this.answers;
        content = answers.multichoice
          .map(function (num: number) {
            return (
              '<h2><div class="edgar-answer-badge badge badge-primary">' +
              String.fromCharCode(97 - 1 + parseInt('' + num)) +
              '</div></h2>' +
              `<div class="text-left" style="margin-left: 50px;">
                                    ${answersText[num - 1].aHtml}
                                  </div>  `
            );
          })
          .join();
      } else if (
        answers.codeAnswer &&
        answers.codeAnswer.code &&
        answers.codeAnswer.code.trim() !== ''
      ) {
        content = `<pre>${this.htmlEncode(answers.codeAnswer.code)}</pre>`;
      } else if (answers.text && answers.text.trim() !== '') {
        content = `<pre>${this.htmlEncode(answers.text)}</pre>`;
      }
    }
    let files = '';
    if (this.uploadFileNames && this.uploadFileNames.length && this.uploadFileNames[0]) {
      // TODO: there is a "bug" in this array: some questions have an empty name array
      files = `<div class="row">                       
                        <div class="col-12">
                            <b>Attached files:</b>
                            ${this.uploadFileNames
                              .map((name) => `<span class="badge badge-primary">${name}</span>`)
                              .join(', ')}
                        </div>
                    </div>`;
    }
    return `<div class="container p-3 mb-1 border border-secondary rounded">
        <div class="row">
           <div class="col-1">
           <h2><div class="aii-answer-badge badge badge-secondary">${this.ordinal}</div></h2>
           </div>
           <div class="col-11" style="max-height: 200px; overflow-y:hidden;opacity:0.3;">   
                          ${this.content}
           </div>
        </div>
        <div class="row mt-3">
           <div class="col-1">
            <h5>My answer:</h5>
           </div>
           <div class="col-11">
                ${content}
           </div>
        </div>
        ${files}
      </div>`;
  }
}

export interface ProgrammingLanguage {
  hello_world: string;
  id: number;
  name: string;
}

export interface Attachment {
  filename: string;
  label: string;
  is_public: boolean;
}

export interface ScaleItem {
  label: string;
  value: number;
}
