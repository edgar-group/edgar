import { Provider } from 'angular2/core';
import {
  LOG_SERVICE_CONFIG,
  TEST_LOG_SERVICE_CONFIG,
  TUTORIAL_LOG_SERVICE_CONFIG,
  EXERCISE_LOG_SERVICE_CONFIG,
} from './log.service';

export const exerciseLogServiceConfigProvider = new Provider(LOG_SERVICE_CONFIG, {
  useValue: EXERCISE_LOG_SERVICE_CONFIG,
});

export const testLogServiceConfigProvider = new Provider(LOG_SERVICE_CONFIG, {
  useValue: TEST_LOG_SERVICE_CONFIG,
});

export const tutorialLogServiceConfigProvider = new Provider(LOG_SERVICE_CONFIG, {
  useValue: TUTORIAL_LOG_SERVICE_CONFIG,
});
