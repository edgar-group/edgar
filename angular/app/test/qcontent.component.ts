import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  OnChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from 'angular2/core';
import { Question, ScaleItem } from './../common/question';
import { QCodeComponent } from './qcode.component';
import { QFreeTextComponent } from './qfreetext.component';
import { Answer } from '../common/answer';
import { LogService, LogEvent } from './../common/log.service';
import { $$ } from 'angular2/src/core/change_detection/parser/lexer';

declare var $: any;

@Component({
  selector: 'q-content',
  template: `
    <div class="row">
      <div
        [class.col-12]="layoutTutorial"
        [class.offset-1]="!layoutHorizontal && !layoutTutorial"
        [class.col-10]="!layoutHorizontal && !layoutTutorial"
        [class.col-6]="layoutHorizontal && !layoutTutorial"
      >
        <div
          *ngIf="currQuestion && !currQuestion.scaleItems"
          class="edgar-q-content"
          [innerHTML]="currQuestion && currQuestion.content"
        ></div>
      </div>
      <div
        [class.col-12]="layoutTutorial"
        [class.offset-1]="!layoutHorizontal && !layoutTutorial"
        [class.col-10]="!layoutHorizontal && !layoutTutorial"
        [class.col-6]="layoutHorizontal && !layoutTutorial"
      >
        <div *ngIf="currQuestion && currQuestion.modeDesc" class="row text-left">
          <i>Check column mode: {{ currQuestion.modeDesc }}</i>
        </div>
        <div
          *ngIf="
            currQuestion &&
            !(currQuestion.answers && currQuestion.answers.length) &&
            !currQuestion.scaleItems
          "
          class="row"
        >
          <q-free-text
            *ngIf="currQuestion.type === 'Free text'"
            [currAnswers]="currAnswers"
            [currQuestion]="currQuestion"
            [cmSkin]="cmSkin"
            (answerUpdated)="onAnswerUpdated($event)"
            class="w-100"
          ></q-free-text>
          <q-code
            class="container-fluid"
            *ngIf="currQuestion.type !== 'Free text'"
            [currAnswers]="currAnswers"
            [currQuestion]="currQuestion"
            [currQuestionWait]="currQuestionWait"
            [cmSkin]="cmSkin"
            (answerUpdated)="onAnswerUpdated($event)"
            (codeRun)="onAnswerEvaluated($event)"
          ></q-code>
        </div>
        <ng-container *ngIf="currQuestion">
          <button
            type="button"
            class="edgar-q-answer"
            *ngFor="#a of currQuestion.answers"
            (click)="onSelect(a)"
            [class.edgar-selected]="isSelected(a)"
          >
            <div class="edgar-answer-badge badge badge-secondary">
              {{ getLabel(a.ordinal) }}
            </div>
            <div style="margin-left: 50px;" class="text-left" [innerHTML]="a.aHtml"></div>
             
          </button>
        </ng-container>

        <ng-container *ngIf="currQuestion && currQuestion.scaleItems">
          <div class="scale">
            <label class="statement" [innerHTML]="currQuestion.content"></label>
            <ol class="scale">
              <li class="scale" *ngFor="#si of currQuestion.scaleItems">
                <input
                  type="radio"
                  name="scale[{{ currQuestion.ordinal }}]"
                  value="{{ si.value }}"
                  (click)="onSelectScaleItem(si)"
                  [checked]="isSelectedScaleItem(si)"
                />
                <label
                  (click)="onSelectScaleItem(si)"
                  [innerHTML]="si.label"
                  [class.selected-scale-item]="isSelectedScaleItem(si)"
                ></label>
              </li>
            </ol>
          </div>
        </ng-container>

        <ng-container *ngIf="currQuestion && currQuestion.supportsUpload() && uploadFileNo > 0">
          <div class="card">
            <div class="card-header">
              <h3>File uploads</h3>
            </div>
            <div class="card-body">
              <div class="row" style="margin:unset;" *ngFor="#fidx of currQuestion.uploadNumbers">
                <br />
                <div class="col-1">
                  <h4>File {{ 1 + fidx }}</h4>
                </div>
                <form
                  enctype="multipart/form-data"
                  method="post"
                  name="fileinfo{{ currQuestion.ordinal }}{{ fidx }}"
                >
                  <div class="col-3">
                    <input
                      id="fUpload{{ currQuestion.ordinal }}{{ fidx }}"
                      type="file"
                      name="testInstanceFile"
                      required
                      (change)="_ref.markForCheck()"
                    />
                  </div>
                  <div class="col-1">
                    <input
                      class="btn btn-info"
                      type="submit"
                      value="Upload"
                      (click)="uploadFile(fidx)"
                      [disabled]="uploadButtonDisabled(fidx)"
                    />
                  </div>
                </form>
                <div class="col-1" *ngIf="currQuestion.uploaded[fidx]">
                  <button class="btn btn-danger" type="submit" (click)="deleteUploadedFile(fidx)">
                    <i class="fa fa-trash"></i>
                  </button>
                </div>

                <div class="col-6" [class.bg-success]="currQuestion.uploaded[fidx]">
                  <h3 [innerHTML]="getFileUploadLabel(fidx)"></h3>
                </div>
              </div>
            </div>
          </div>
        </ng-container>
      </div>
    </div>
  `,
  styles: [
    `
      .edgar-q-content {
        min-height: 100px;
        margin-bottom: 50px;
      }
      .edgar-q-answer {
        min-height: 50px;
        padding: 0.75rem 1.25rem;
        margin-bottom: -1px;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, 0.25);
        display: block;
        width: 100%;
        border-radius: 5px;
        margin-bottom: 2px;
      }
      .edgar-q-answer:hover {
        background-color: rgba(0, 0, 0, 0.05);
      }
      .edgar-selected {
        border: darkgreen solid 3px;
        background-color: lemonchiffon !important;
      }
      .edgar-answer-badge {
        width: 35px;
        float: left;
        font-size: 1.5rem;
      }

      h1.scale-header {
        padding-left: 4.25%;
        margin: 20px 0 0;
      }

      div.scale .statement {
        display: block;
        font-size: 14px;
        font-weight: bold;
        margin-bottom: 30px;
      }

      ol.scale {
        display: flex;
        padding-left: 0;
        list-style: none;
        justify-content: space-between;
      }

      li.scale {
        position: relative;
        display: flex;
        padding-right: 16px;
        padding-left: 16px;
        text-align: center;
        align-items: center;
        flex-direction: column;
        width: 100%;
      }

      li.scale:not(:first-of-type) input::before {
        left: 0;
      }

      li.scale:first-of-type input::before,
      li.scale:last-of-type input::before {
        width: 50%;
      }

      li.scale:first-of-type input::before {
        left: 50%;
      }

      li.scale input {
        display: flex;
        align-items: center;
        width: 2rem;
        height: 2rem;
      }

      li.scale input::before {
        position: absolute;
        z-index: -1;
        width: 100%;
        height: 4px;
        background-color: green;
        content: '';
      }

      li.scale label {
        margin-top: 8px;
        font-size: 1.1em;
        font-weight: bold;
      }

      .selected-scale-item {
        background-color: yellowgreen;
        border: 1px black solid;
        border-radius: 5px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  directives: [QCodeComponent, QFreeTextComponent],
  providers: [LogService],
})
export class QContentComponent implements OnChanges, OnInit {
  @Input() currQuestion: Question;
  @Input() currQuestionWait: number;
  @Input() currAnswers: Answer[];
  @Input() cmSkin: string;
  @Input() layoutHorizontal: boolean;
  @Input() layoutTutorial: boolean;
  @Input() uploadFileNo: number;
  @Output() answerUpdated = new EventEmitter();
  @Output() answerEvaluated = new EventEmitter();

  constructor(private _ref: ChangeDetectorRef, private _logService: LogService) {}
  static readonly RUNNING_STRING = '..RUNNING_STRING..';
  getFileUploadLabel(idx: number) {
    return this.currQuestion.uploadFileNames[idx] === ''
      ? ''
      : this.currQuestion.uploadFileNames[idx] === QContentComponent.RUNNING_STRING
      ? '<i class="fa fa-2 fa-spinner fa-spin"></i>'
      : this.currQuestion.uploadFileNames[idx];
  }
  deleteUploadedFile(idx) {
    let self = this;
    let oReq = new XMLHttpRequest();
    oReq.open('POST', `/upload/test_instance/delete/${this.currQuestion.ordinal}/${idx}`, true);
    let toLogDeletedFileName = self.currQuestion.uploadFileNames[idx];
    oReq.onload = function (oEvent) {
      self.currQuestion.uploaded[idx] = true;
      if (oReq.status == 200) {
        let result = JSON.parse(oReq.response);
        if (result.success) {
          self.currQuestion.uploadFileNames[idx] = '';
          self.currQuestion.uploaded[idx] = false;
          self._logService.log(new LogEvent('Deleted uploaded file', toLogDeletedFileName));
        } else {
          self.currQuestion.uploadFileNames[idx] = oReq.response;
          self._logService.log(new LogEvent('Failed deleting uploaded file', toLogDeletedFileName));
        }
      } else {
        self.currQuestion.uploadFileNames[idx] =
          'Error ' + oReq.status + ' occurred when trying to upload your file.';
        self._logService.log(new LogEvent('Failed deleting uploaded file', toLogDeletedFileName));
      }
      self._ref.markForCheck();
    };
    this.currQuestion.uploadFileNames[idx] = QContentComponent.RUNNING_STRING;
    oReq.send();
  }
  uploadButtonDisabled(idx) {
    let elem: any = document.getElementById('fUpload' + this.currQuestion.ordinal + idx);
    return '' == elem.value;
  }
  uploadFile(idx) {
    var form = document.forms.namedItem('fileinfo' + this.currQuestion.ordinal + idx);
    var oData = new FormData(form);
    var self = this;
    var oReq = new XMLHttpRequest();
    oReq.open('POST', `/upload/test_instance/save/${this.currQuestion.ordinal}/${idx}`, true);
    oReq.onload = function (oEvent) {
      self.currQuestion.uploaded[idx] = false;
      if (oReq.status == 200) {
        let result = JSON.parse(oReq.response);
        if (result.success) {
          self.currQuestion.uploadFileNames[idx] = result.data && result.data.originalname;
          self.currQuestion.uploaded[idx] = true;
          self._logService.log(
            new LogEvent('Uploaded file', self.currQuestion.uploadFileNames[idx])
          );
        } else {
          self.currQuestion.uploadFileNames[idx] = oReq.response;
          self._logService.log(
            new LogEvent('Error uploading file', self.currQuestion.uploadFileNames[idx])
          );
        }
      } else {
        self.currQuestion.uploadFileNames[idx] =
          'Error ' + oReq.status + ' occurred when trying to upload your file.';
        self._logService.log(
          new LogEvent('Error uploading file', self.currQuestion.uploadFileNames[idx])
        );
      }
      $('#fUpload' + self.currQuestion.ordinal + idx).val(''); // clear the stupid label
      self._ref.markForCheck();
    };
    this.currQuestion.uploadFileNames[idx] = QContentComponent.RUNNING_STRING;
    oReq.send(oData);
  }

  getLabel(ordinal): string {
    return Question.getAnswerLabel({ multichoice: [ordinal] });
  }
  isSelected(answer) {
    // console.log("QContentComponent -->  isSelected ");
    // TODO: when selecting an answer from multilanguage component check if already selected
    return this.currAnswers[this.currQuestion.ordinal - 1].multichoice.indexOf(answer.ordinal) >= 0;
  }
  isSelectedScaleItem(si: ScaleItem) {
    return this.currAnswers[this.currQuestion.ordinal - 1].multichoice.indexOf(si.value) >= 0;
  }

  onSelect(answer) {
    var arrAnswers = this.currAnswers[this.currQuestion.ordinal - 1].multichoice;
    if (this.currQuestion.multiCorrect) {
      var idx;
      if ((idx = arrAnswers.indexOf(answer.ordinal)) >= 0) {
        arrAnswers.splice(idx, 1);
      } else {
        arrAnswers.push(answer.ordinal);
      }
    } else {
      if (arrAnswers.indexOf(answer.ordinal) >= 0) {
        arrAnswers = [];
      } else {
        arrAnswers = [answer.ordinal];
      }
    }
    this.currAnswers[this.currQuestion.ordinal - 1].multichoice = arrAnswers;

    this.answerUpdated.emit(this.currAnswers[this.currQuestion.ordinal - 1]);
  }
  onSelectScaleItem(si: ScaleItem) {
    this.currAnswers[this.currQuestion.ordinal - 1].multichoice = [si.value];
    this.answerUpdated.emit(this.currAnswers[this.currQuestion.ordinal - 1]);
  }
  onAnswerUpdated($event) {
    // this is invoked from the q-code on Run and Save
    // console.log("this.answerUpdated", this.answerUpdated);
    this.answerUpdated.emit($event);
  }
  onAnswerEvaluated($event) {
    this.answerEvaluated.emit($event);
  }
  ngOnChanges(change) {
    this.currQuestion &&
      this.currQuestion.initUploadVars(this.uploadFileNo) &&
      this._ref.markForCheck();
    /*var self = this;
        this._testService.getQuestion(this.currQuestion.ordinal).
        subscribe(q => {
            this.currQuestion = q;
            self._ref.markForCheck();
        });*/
  }

  ngOnInit() {
    // if (this.uploadFileNo) {
    //     this.uploadNumbers = Array(this.uploadFileNo).fill(0).map((x, i) => i);
    //     this.currQuestion.uploadFileNames = Array(this.uploadFileNo).fill('');
    //     this.uploaded = Array(this.uploadFileNo).fill(false);
    // }
  }
}
