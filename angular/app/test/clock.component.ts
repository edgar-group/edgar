import { Component, Input, ChangeDetectionStrategy, Injectable } from 'angular2/core';
import { LogService, LogEvent } from './../common/log.service';
@Component({
  selector: 'clock',
  template: `
    <h3
      *ngIf="caption.length < 10"
      class="border rounded"
      id="aii-countdown"
      [class.aii-clock-error]="overflow"
      [class.aii-clock-warning]="prolongedOverflow"
    >
      {{ caption }}
    </h3>
    <div
      *ngIf="caption.length >= 10"
      id="aii-countdown"
      [class.aii-clock-error]="overflow"
      [class.aii-clock-warning]="prolongedOverflow"
    >
      {{ caption }}
    </div>
  `,
})
@Injectable()
export class ClockComponent {
  public caption: string;
  public overflow: boolean;
  public prolongedOverflow: boolean;
  @Input() seconds: number = 0;
  @Input() prolonged: boolean = false;

  constructor(private _logService: LogService) {
    var self = this;
    setInterval(function () {
      self.seconds--;
      self.caption = self.getCaption(self.seconds);
    }, 1000);
    this.caption = this.getCaption(this.seconds);
  }

  getCaption(seconds): string {
    this.overflow = false;
    this.prolongedOverflow = false;
    if (seconds < 0) {
      if (seconds === 0) {
        this._logService.log(new LogEvent('Time is up', '0 seconds left'));
      }
      if (this.prolonged) {
        this.prolongedOverflow = true;
      } else {
        this.overflow = true;
      }
      seconds = -1 * seconds;
    }
    let h = Math.floor(seconds / (60 * 60));
    let m = Math.floor((seconds - 60 * 60 * h) / 60);
    let s = seconds % 60;
    let days = '';
    if (h > 23) {
      days = Math.floor(h / 24) + ' days ';
      h = h % 24;
    }
    return (
      days + (h < 10 ? '0' : '') + h + ':' + (m < 10 ? '0' : '') + m + ':' + (s < 10 ? '0' : '') + s
    );
  }
}
