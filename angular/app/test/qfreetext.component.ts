import {
  Component,
  OnInit,
  ElementRef,
  OnChanges,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from 'angular2/core';
import { CodeMirrorComponent } from '../common/codemirror.component';
import { LogService } from '../common/log.service';
import { Question } from '../common/question';
import { Answer, CodeAnswer } from '../common/answer';

@Component({
  selector: 'q-free-text',
  template: `
    <div class="row">
      <div class="col-12">
        <div class="edgar-overlap border">
          <code-mirror
            [currCode]="getCurrText()"
            [questionType]="currQuestion.type"
            [ordinal]="currQuestion.ordinal"
            [cmSkin]="cmSkin"
            (codeChanged)="onCodeChanged($event)"
          ></code-mirror>
          <div class="edgar-right-overlap">
            <button
              class="btn btn-light col-1 edgar-overlap-btn-w"
              type="submit"
              [disabled]="saved"
              (click)="save($event)"
            >
              Save
            </button>
          </div>
        </div>
      </div>
    </div>
    <div
      *ngIf="currQuestion.attachments && currQuestion.attachments.length > 0"
      class="row"
      style="margin-top: 20px"
    >
      <div class="form-group">
        <label for="attachments_row">Attachments:</label>
        <div id="attachments_row" class="filemanager">
          <div class="row" style="margin-left: 0;">
            <div class="data" style="display: block;">
              <div>
                <div class="row">
                  <div *ngFor="#attachment of currQuestion.attachments">
                    <div
                      *ngIf="attachment.is_public === true"
                      class="col-sm-2 resultitem resultitemFile"
                    >
                      <div class="details detailsFiles">
                        <a
                          style="display: table-row"
                          href="/upload/attachment/{{ attachment.filename }}"
                          target="_blank"
                          class="files visible"
                          download
                        >
                          <div class="icon-pos">
                            <i class="file-icon fa fa-file"></i>
                          </div>
                          <div class="file-info">
                            <div class="file_name" style="display: table-cell">
                              <span class="name">{{ attachment.label }}</span>
                            </div>
                            <div class="file_size">
                              <span class="details"></span>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [``],
  directives: [CodeMirrorComponent],
  providers: [LogService],
})
export class QFreeTextComponent {
  saved: boolean;
  @Input() currQuestion: Question;
  @Input() currAnswers: Answer[];
  @Input() cmSkin: string;

  @Output() answerUpdated = new EventEmitter();
  @Output() codeRun = new EventEmitter();

  constructor(private _ref: ChangeDetectorRef) {
    this.saved = true;
  }

  save() {
    // Will always save the entire contents, not just the selected part (that might come through the $event, ie runHotKey event)
    this.answerUpdated.emit(this.currAnswers[this.currQuestion.ordinal - 1]);
    this.saved = true;
  }

  onCodeChanged($event) {
    this.saved = false;
    if (!this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer) {
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer = new CodeAnswer();
    }
    let currCodeAnswer = this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer;
    currCodeAnswer.code = $event;
  }

  getCurrText() {
    if (
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer &&
      this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code
    ) {
      return this.currAnswers[this.currQuestion.ordinal - 1].codeAnswer.code;
    } else {
      return '\n\n\n\n\n';
    }
  }
}
