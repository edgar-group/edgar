import { Component, ChangeDetectorRef, Injectable, AfterViewInit } from 'angular2/core';
import { QNavComponent } from './qnav.component';
import { ClockComponent } from './clock.component';
import { ScoreboardComponent } from './scoreboard.component';
import { CodeMirrorComponent } from './../common/codemirror.component';
import { TicketComponent } from './../common/ticket/ticket.component';
// import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import { QContentComponent } from './qcontent.component';
import { TestService, Test } from './test.service';
import { Question } from './../common/question';
import { LogService, LogEvent } from './../common/log.service';
import { testLogServiceConfigProvider } from './../common/log.service.provider';
import { WINDOW, WINDOW_PROVIDERS } from './../common/window.service';
import { Answer, CodeAnswer } from '../common/answer';
import { HostListener } from 'angular2/core';
//import { ITicketListener } from '../common/ITicket';

declare var Swal: any;
declare var $: any;

@Component({
  selector: 'my-app',
  host: {
    '(window:focus)': 'onFocus()',
    '(window:blur)': 'onBlur()',
  },
  template: `
    <div class="edgar-header-bg">&nbsp;</div>
    <div *ngIf="_errMessage" class="alert alert-danger text-center">
      {{ _errMessage }}
    </div>
    <div *ngIf="_currAnswers === undefined">Please wait, loading test instance...</div>

    <div
      class="container-fluid edgar-test-header edgar-sticky bg-light d-flex justify-content-between align-items-center"
    >
      <div *ngIf="_test" style="position: fixed;top:5px;left: 100px;">
        <small>{{ _test.title }}</small>
      </div>
      <div *ngIf="_test" style="position: fixed;top:5px;left: 5px;">
        <button
          class="btn btn-light btn-outline-dark"
          title="Toggle layout"
          (click)="(_layoutHorizontal = !_layoutHorizontal) && false"
        >
          <i class="fas fa-grip-{{ _layoutHorizontal ? 'vertical' : 'horizontal' }}"></i>
        </button>
        <div *ngIf="_test.runStats && _test.runStats.length && _currQuestion" class="mt-3 ml-1">
          <h5>
            Runs:
            <span
              [class.text-warning]="_currQuestion.runs >= 15 && _currQuestion.runs < 25"
              [class.text-danger]="_currQuestion.runs >= 25"
              title="The number of times this question was run."
            >
              {{ _currQuestion.runs ? _currQuestion.runs : 0 }}</span
            >
            /
            <span
              [class.text-warning]="
                _test.runStats[0].tcnt >= 15 * _test.noOfQuestions &&
                _test.runStats[0].tcnt < 25 * _test.noOfQuestions
              "
              [class.text-danger]="_test.runStats[0].tcnt >= 25 * _test.noOfQuestions"
              title="Total number of times (all) questions have been run."
            >
              {{ _test.runStats[0].tcnt }}</span
            >
          </h5>
        </div>
      </div>
      <div *ngIf="_confirm">&nbsp;</div>
      <div
        *ngIf="!_confirm && _currAnswers && _currAnswers.length > 0 && !_test.isPA2()"
        class="container-fluid d-flex justify-content-around"
      >
        <q-nav
          [currAnswers]="_currAnswers"
          [currTest]="_test"
          (questionSelected)="onQuestionSelected($event)"
        ></q-nav>
      </div>
      <div
        *ngIf="!_confirm && _currAnswers && _currAnswers.length > 0 && _test.isPA2()"
        class="container-fluid d-flex justify-content-around"
      >
        <h4>
          <a href="/test/instance/peerattachment" target="_blank">
            Peer assessment assignment <i class="file-icon fa fa-file"></i
          ></a>
        </h4>
      </div>

      <div *ngIf="_test && _test.is_competition" class="col-1 text-center">
        <button
          class="btn btn-outline-info"
          type="submit"
          (click)="_showScoreboard = !_showScoreboard"
        >
          Scoreboard
        </button>
      </div>
      <div *ngIf="_test" class="d-flex align-items-center">
        <div class="profilepic">
          <img height="75" class="rounded-circle" src="{{ _test.imgSrc }}" />
        </div>

        <div>
          <div>{{ _test.student }}</div>
          <div>{{ _test.alt_id }} {{ _test.alt_id2 }}</div>
        </div>
        <div class="ml-4 d-flex flex-column">
          <clock [seconds]="_secondsLeft" [prolonged]="_test.prolonged"></clock>
          <button
            *ngIf="!_confirm && _currAnswers && _currAnswers.length > 0"
            class="btn btn-primary"
            type="submit"
            (click)="_confirm = true && refreshQAFormatted()"
          >
            Submit
          </button>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div *ngIf="_test && _test.usesTicketing && _currQuestion && _currQuestion.ordinal">
        <ticket [ordinal]="_currQuestion.ordinal"></ticket>
      </div>
      <ng-container *ngIf="!_confirm && _currAnswers && _currAnswers.length > 0 && _test.isPA2()">
        <div *ngIf="_test && _test.usesTicketing">
          <ticket [ordinal]="1"></ticket>
          <br /><br />
        </div>
        <div class="pa2-question" *ngFor="#q of _PA2Questions, #i = index">
          <h2>
            <div class="aii-answer-badge badge badge-light">{{ i + 1 }}</div>
          </h2>
          <div class="row">
            <div class="offset-1 col-10">
              <q-content
                [currAnswers]="_currAnswers"
                [currQuestion]="q"
                [currQuestionWait]="_currQuestionWait"
                [cmSkin]="_test.cmSkin"
                [uploadFileNo]="_test.upload_file_no"
                (answerUpdated)="onAnswerUpdated($event, i + 1)"
                (answerEvaluated)="onAnswerEvaluated($event)"
              >
              </q-content>
            </div>
          </div>
        </div>
      </ng-container>
      <div
        *ngIf="!_confirm && _currAnswers && _currAnswers.length > 0 && !_test.isPA2()"
        class="container-fluid"
      >
        <q-content
          *ngIf="_showScoreboard === false"
          [currAnswers]="_currAnswers"
          [currQuestion]="_currQuestion"
          [currQuestionWait]="_currQuestionWait"
          [cmSkin]="_test.cmSkin"
          [layoutHorizontal]="_layoutHorizontal"
          [uploadFileNo]="_test.upload_file_no"
          (answerUpdated)="onAnswerUpdated($event)"
          (answerEvaluated)="onAnswerEvaluated($event)"
        >
        </q-content>
        <scoreboard *ngIf="_showScoreboard === true"></scoreboard>
      </div>
    </div>

    <div *ngIf="_confirm">
      <div class="text-center">
        <h1>Are you sure you want to submit the exam?</h1>
      </div>
      <div class="text-center">
        <button
          class="btn btn-primary"
          type="submit"
          [disabled]="_submitting"
          (click)="updateSecondsLeft() && (_confirm = false)"
        >
          No, continue writing
        </button>
        <button
          class="btn btn-success"
          type="submit"
          [disabled]="_submitting"
          (click)="submit($event)"
        >
          <span [innerHTML]="getSubmitLabel()"></span>
        </button>
      </div>
      <br /><br /><br />
      <div *ngIf="_submitting" class="text-center">
        <div *ngIf="_submittedAsync" class="text-center">
          <h2>Your answers are succesfully submitted for review!</h2>
          <h2>
            You will be redirected to results page in<img
              src="/images/5s.gif"
              height="35px"
              alt="3s countdown"
            />seconds...
          </h2>
        </div>
        <img
          src="/images/waiting.gif"
          alt="Patience, grasshoper"
          style="width: 480px; height: 317px; left: 0px; top: 0px;"
        />
      </div>
    </div>
    <ng-container *ngIf="_confirm">
      <div [innerHTML]="_QAFormatted"></div>
    </ng-container>
  `,
  styles: [
    `
      .pa2-question {
        border: 2px solid lightgray;
        border-radius: 10px;
        margin: 10px;
        padding: 10px;
      }
    `,
  ],
  directives: [
    QNavComponent,
    ScoreboardComponent,
    ClockComponent,
    QContentComponent,
    CodeMirrorComponent,
    TicketComponent,
  ], // ROUTER_DIRECTIVES
  providers: [TestService, LogService, testLogServiceConfigProvider, WINDOW_PROVIDERS],
})
@Injectable()
export class AppComponent implements AfterViewInit {
  _currQuestion: Question;
  _currAnswers: Answer[];
  //_currCodeAnswers: string[];
  _test: Test;
  _win: Window;
  _errMessage: string;
  _confirm: boolean;
  _submitting: boolean;
  _submittedAsync: boolean;
  _layoutHorizontal: boolean;
  _showScoreboard: boolean = false;
  _testRecievedAt: Date;
  _secondsLeft: number;
  _PA2Questions: Question[];
  _QAFormatted: string;
  //_testInstanceRuns;
  _paceIntervalRef: any;
  _currQuestionWait: number;
  // _editor : CodeMirror.Editor;
  constructor(
    private _testService: TestService,
    private _logService: LogService,
    private _ref: ChangeDetectorRef,
    win: WINDOW
  ) {
    this._layoutHorizontal = false;
    this._confirm = false;
    this._submitting = false;
    this._submittedAsync = false;
    this._win = win.nativeWindow;
    //this._testInstanceRuns = 0;
    this._testService.getTest().subscribe((test) => {
      if (test === null) {
        this._win.location.href = '/';
      } else {
        this._test = test;
        this._testRecievedAt = new Date();
        //this._currQuestion = this._testService.getStaticQuestion1();
        if (this._test.isPA2()) {
          this._PA2Questions = [];

          for (let i = 1; i <= this._test.noOfQuestions; ++i) {
            this._PA2Questions.push(
              new Question(
                'dummy',
                1,
                [],
                '<i class="fa fa-2 fa-spinner fa-spin"></i> please wait for question ' + i + '...',
                undefined,
                '',
                '',
                'Scale dummy question',
                null,
                null,
                [],
                false
              )
            );
            this._testService.getQuestion(i).subscribe((q) => {
              this._PA2Questions[i - 1] = q;
              this._ref.markForCheck();
            });
          }
        } else {
          this._testService
            .getQuestion(test.forward_only ? test.last_ordinal : 1)
            .subscribe((q) => {
              this._currQuestion = q;

              if (!this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer) {
                this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer = new CodeAnswer();
                this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer.code =
                  'Recovered legacy test, ongoing code lost...';
              }
              if (
                !this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer.languageId &&
                this._currQuestion.programmingLanguages &&
                this._currQuestion.programmingLanguages.length > 0
              ) {
                this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer.languageId =
                  this._currQuestion.programmingLanguages[0].id;
              }

              // this._currAnswers.forEach(answer => {
              //     // if (!answer.codeAnswer) {
              //     // 	answer.codeAnswer = new CodeAnswer();
              //     // 	answer.codeAnswer.code = 'Recovered legacy test, ongoing code lost...';
              //     // }
              //     if (answer.codeAnswer
              //         && !answer.codeAnswer.languageId
              //         && this._currQuestion.programmingLanguages
              //         && this._currQuestion.programmingLanguages.length > 0) {
              //         answer.codeAnswer.languageId = this._currQuestion.programmingLanguages[0].id;
              //     }

              // });
              this.refreshPaceInterval(); // refreshin only after I've gotten the question
              this._ref.markForCheck();
            });
        }

        this.updateSecondsLeft();
        this._currAnswers = test.currAnswers;

        //console.log('_Test', this._test);
        //this._currCodeAnswers = test.currCodeAnswers;
        this._ref.markForCheck();
      }
    });

    setInterval(() => {
      this._logService.hb();
    }, 60 * 1000);
    this._logService.log(new LogEvent('Constructed component', 'AppComponent'));
  }
  ngAfterViewInit() {
    var self = this;
    setTimeout(function () {
      self.onResize();
    }, 500);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    $('.edgar-header-bg').height($('.edgar-sticky').height() + 75);
  }
  updateSecondsLeft() {
    var diff = new Date().getTime() - this._testRecievedAt.getTime();
    this._secondsLeft = this._test.secondsLeft - Math.round(diff / 1000);
    return true;
  }
  refreshPaceInterval() {
    let self = this;
    if (this._paceIntervalRef) {
      clearInterval(this._paceIntervalRef);
    }

    if (self._test.runStats) {
      for (let rs of self._test.runStats) {
        if (self._currQuestion.ordinal === rs.ordinal) {
          self._currQuestionWait = rs.time_wait;
          self._currQuestion.runs = rs.qcnt;
          break;
        }
      }

      this._paceIntervalRef = setInterval(function () {
        self._currQuestionWait = 0;
        for (let rs of self._test.runStats) {
          rs.time_wait--;
          if (self._currQuestion.ordinal === rs.ordinal) {
            self._currQuestionWait = rs.time_wait;
          }
        }
        self._ref.markForCheck();
      }, 1000);
      self._ref.markForCheck();
    }
  }

  onFocus() {
    this._logService.log(new LogEvent('Got focus', '-'));
  }
  refreshQAFormatted() {
    let qhtml = [];
    this._QAFormatted = 'Refreshing...';
    for (let i = 0; i < this._currAnswers.length; ++i) {
      qhtml.push(i + 1 + ' loading...');
      this._testService.getQuestion(i + 1).subscribe((q) => {
        qhtml[q.ordinal - 1] = q.getQAFormatted(this._currAnswers[q.ordinal - 1]);
        this._QAFormatted =
          '<div class="container p-2"><h3>My answers:</h3></div>' + qhtml.join('');
      });
    }

    return true;
  }

  onBlur() {
    this._logService.log(
      new LogEvent('Lost focus', 'Warning - student could be cheating (alt+tab)?')
    );
  }

  submit($event) {
    var self = this;
    this._errMessage = undefined;
    this._submitting = true;
    this._testService.submitTest(this._currAnswers).subscribe((result) => {
      if (result.success) {
        if (result.async_submit) {
          this._submittedAsync = true;
          setTimeout(function () {
            self._win.location.href = '/test/instances';
          }, 5000);
        } else {
          this._submitting = false;
          this._win.location.href = '/test/instances';
        }
      } else {
        this._confirm = false;
        this._errMessage = result.error;
      }
    });
  }

  getSubmitLabel() {
    return this._submitting
      ? '<i class="fa fa-2 fa-spinner fa-spin"></i>Submitting, please wait....<i class="fa fa-2 fa-spinner fa-spin"></i>'
      : 'Yes, submit my exam!';
  }

  // currAnswerOrdinal: number;
  onQuestionSelected($event) {
    //this._currQuestion = $event;
    this._testService.getQuestion($event.ordinal).subscribe((q) => {
      this._currQuestion = q;
      if (
        !this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer.languageId &&
        this._currQuestion.programmingLanguages &&
        this._currQuestion.programmingLanguages.length > 0
      ) {
        this._currAnswers[this._currQuestion.ordinal - 1].codeAnswer.languageId =
          this._currQuestion.programmingLanguages[0].id;
      }
      let rs = this._test.runStats && this._test.runStats.find((x) => x.ordinal === q.ordinal);
      if (rs) {
        this._currQuestionWait = rs.time_wait;
      } else {
        this._currQuestionWait = 0;
      }
      this._logService.log(new LogEvent('Question selected', '' + this._currQuestion.ordinal));
    });
    // this.currAnswerOrdinal = this._currAnswers[this._currQuestion.ordinal - 1];
    // console.log('this.currAnswerOrdinal', this.currAnswerOrdinal, this._currQuestion.ordinal, this._currAnswers);
  }

  // getCurrAnswerOrdinal() {
  //     console.log('getCurrAnswerOrdinal called, returning ', this.currAnswerOrdinal);
  //     return this.currAnswerOrdinal;
  // }
  onAnswerUpdated($newValue, ordinal) {
    // multichoice sends number[], code sends text code
    //TODO when answer updated - update currAnswers based on question and answer type
    if (ordinal !== undefined) {
      this._currAnswers[ordinal - 1] = $newValue;
    } else {
      this._currAnswers[this._currQuestion.ordinal - 1] = $newValue;
    }
    this._currAnswers = this._currAnswers.slice(); // :( to force ng onchange
    this._logService.log(new LogEvent('Answer updated', JSON.stringify(this._currAnswers)));
    // console.log('this._logService.log(new LogEvent("Answer updated", JSON.stringify(this._currAnswers)))', this._currAnswers);
  }
  onAnswerEvaluated($newValue) {
    //console.log('onAnswerEvaluated', $newValue);
    //this._testInstanceRuns = $newValue.testInstanceRuns;
    this._test.runStats = $newValue.runStats;
    this.refreshPaceInterval();
  }
}
