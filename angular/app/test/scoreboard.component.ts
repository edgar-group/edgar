import {
  ChangeDetectionStrategy,
  Component,
  Input,
  AfterViewInit,
  OnInit,
  OnChanges,
} from 'angular2/core';
import { Test, TestService } from './test.service';

@Component({
  selector: 'scoreboard',
  template: `
    <table
      *ngIf="_showScoreboard === true"
      class="table table-hover edgar"
      style="table-layout: fixed;"
    >
      <thead>
        <tr>
          <th scope="col">#</th>
          <th colspan="8" scope="col">competitor</th>
          <th colspan="2" scope="col"><i class="fa-sum"></i></th>
          <th colspan="8" scope="col" *ngFor="#header of _scoreboardHeader">
            <div class="col">Q{{ header.ordinal }}</div>
            <small
              ><b>{{ header.correctScore }}</b></small
            >
          </th>
        </tr>
      </thead>
      <tbody>
        <tr
          *ngFor="#competitor of _scoreboardBody; #i = index"
          [class.currentUser]="competitor.isCurrentUser"
        >
          <th scope="row">{{ i + 1 }}</th>
          <td colspan="8">{{ competitor.competitor }}</td>
          <td colspan="2">{{ competitor.scoreSum }}</td>

          <td colspan="8" *ngFor="#question of competitor.questions">
            <div class="col">
              {{ (question.compScore && question.compScore) || '-' }}
            </div>
            <small>{{ question.timePassed }}</small>
          </td>
        </tr>
      </tbody>
    </table>
  `,
  styles: [
    `
      .fa-sum:before {
        content: '\\03a3';
        font-family: sans-serif;
      }

      th,
      td {
        padding: 0;
        text-align: center;
        vertical-align: middle !important;
      }

      td {
        height: 60px;
        overflow: hidden;
        width: 25%;
      }

      .currentUser {
        color: orange !important;
      }
    `,
  ],
})
export class ScoreboardComponent implements OnChanges {
  private DELTA: number = 1e-3;

  _scoreboardHeader: any[];
  _scoreboardBody: any[] = [];

  _showScoreboard: boolean = false;

  constructor(private _testService: TestService) {
    this.ngOnChanges();
  }

  ngOnChanges() {
    this._showScoreboard = false;
    this._testService.getScoreboard().subscribe((scoreboard) => {
      // console.log('scoreboard', scoreboard);
      Object.keys(scoreboard).forEach((key) => {
        let competitor = scoreboard[key];

        if (!this._scoreboardHeader) {
          this._scoreboardHeader = [];

          competitor.questions.forEach((question) => {
            this._scoreboardHeader.push({
              ordinal: question.ordinal,
              correctScore: (+question.correctScore).toFixed(2),
            });
          });
        }

        let bodyBuilder = {
          competitor: competitor.username,
          isCurrentUser: competitor.isCurrentUser,
          scoreSum: '0.00',
          questions: [],
        };

        let scoreSum = 0.0;
        competitor.questions.forEach((question) => {
          scoreSum += parseFloat(question.compScore) || 0;
          bodyBuilder.questions.push({
            ordinal: question.ordinal,
            compScore: question.compScore ? (+question.compScore).toFixed(2) : '',
            timePassed: question.timePassed || '',
          });
        });
        bodyBuilder.scoreSum = '' + scoreSum;

        this._scoreboardBody.push(bodyBuilder);
      });

      this._scoreboardBody.sort((a, b) => {
        let scoreSumA = +a.scoreSum;
        let scoreSumB = +b.scoreSum;

        if (Math.abs(scoreSumA - scoreSumB) < this.DELTA) {
          return a.competitor > b.competitor ? -1 : 1;
        }

        return scoreSumA > scoreSumB ? -1 : 1;
      });

      this._scoreboardBody.forEach((row) => {
        row.scoreSum = (+row.scoreSum).toFixed(2);
      });

      this._showScoreboard = true;
    });
  }
}
