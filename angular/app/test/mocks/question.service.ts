import { Injectable } from 'angular2/core';

@Injectable()
export class QuestionService {
  getQuestion(num: number) {
    return [
      {
        content: 'What about Ryan Adams?',
      },
      {
        content: 'So you think you can code?',
      },
      {
        content: 'Are you serious?',
      },
    ][num % 3];
  }
}
