import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Observable } from 'rxjs/Rx';
import { LogService, LogEvent } from './../common/log.service';

import { Exercise } from './models/exercise';
import { ExerciseQuestion } from './models/question';
import { AdaptivityModelBehaviour } from './models/adaptivityModelBehaviour';
import { AdaptivityProgressTracker } from './models/adaptivityProgressTracker';
import { DifficultyLevel } from './models/difficultyLevel';

@Injectable()
export class ExerciseService {
  private _exercise: Exercise;
  private _adaptivityProgressTracker: AdaptivityProgressTracker;
  private _difficultyLevels: DifficultyLevel[];
  private _questions: ExerciseQuestion[];
  private _httpPostHeaders: Headers;

  constructor(private _http: Http, private _logService: LogService) {
    this._httpPostHeaders = new Headers();
    this._httpPostHeaders.append('Content-Type', 'application/json');
    this._questions = [];
  }

  startExercise(id: number) {
    this._logService.log(new LogEvent('Starting exercise', ''));
    return this._http
      .post(`/exercise/api/start/${id}`, '', { headers: this._httpPostHeaders })
      .map((res) => {
        return res.json();
        // to-do?
      });
  }

  getExercise(id: number) {
    if (this._exercise && this._adaptivityProgressTracker && this._difficultyLevels) {
      return Observable.of(this._exercise);
    } else {
      this._logService.log(new LogEvent('Request exercise', ''));
      return this._http.get(`/exercise/api/${id}`).map((res) => {
        let data = res.json();
        const behaviour = data.adaptivityModelBehaviour.map(
            (amb) =>
              new AdaptivityModelBehaviour(
                amb.id_question_difficulty_level,
                amb.level_up_questions_to_track,
                amb.level_up_correct_questions,
                amb.level_down_questions_to_track,
                amb.level_down_incorrect_questions
              )
          ),
          difficultyLevels = data.difficultyLevels.map(
            (dl) => new DifficultyLevel(dl.id, dl.name, dl.ordinal, dl.rgb_color)
          ),
          adaptivityProgressTracker = new AdaptivityProgressTracker(
            difficultyLevels[0].id,
            behaviour,
            difficultyLevels
          );

        //console.log('behaviour', behaviour, 'apt', adaptivityProgressTracker);

        this._exercise = new Exercise(
          data.exercise.id,
          data.exercise.title,
          data.exercise.description,
          behaviour,
          difficultyLevels,
          adaptivityProgressTracker,
          data.cmSkin
        );
        this._logService.log(new LogEvent('Receive exercise', ''));
        return this._exercise;
      });
    }
  }

  public getQuestion(ordinal: number, currAnswers?: any) {
    if (this._questions[ordinal - 1]) {
      return Observable.of(this._questions[ordinal - 1]);
    } else {
      this._logService.log(new LogEvent('Request question', `ordinal: ${ordinal}`));
      return this._http
        .get(
          `/exercise/api/${this._exercise.id}/question/${ordinal}?difficultyLevelId=${this._exercise.adaptivityProgressTracker.currentDifficultyLevelId}`
        )
        .map((res) => {
          let q = res.json();

          if (!q.noMoreQuestions) {
            this._questions[ordinal - 1] = new ExerciseQuestion(
              q.ordinal.toString(),
              q.ordinal,
              currAnswers || [],
              q.html,
              q.answers,
              q.multiCorrect,
              q.modeDesc,
              q.type,
              q.id_difficulty_level
            );
            this._logService.log(new LogEvent('Receive question', `ordinal: ${ordinal}`));
            return this._questions[ordinal - 1];
          } else {
            this._logService.log(
              new LogEvent(
                'No questions for requested difficulty level available',
                `difficulty level: ${this._exercise.getCurrentDifficultyLevel().name}`
              )
            );
            return null;
          }
        });
    }
  }

  updateAnswers(answers: any) {
    return this._http
      .put(`/exercise/api/${this._exercise.id}/answers`, JSON.stringify({ answers: answers }), {
        headers: this._httpPostHeaders,
      })
      .map((resp) => resp.json())
      .subscribe((r) => r);
  }

  updateDifficultyLevel(difficultyLevelId: number) {
    this._http
      .put(
        `/exercise/api/${this._exercise.id}/difficultyLevel`,
        JSON.stringify({
          difficultyLevelId: difficultyLevelId,
        }),
        { headers: this._httpPostHeaders }
      )
      .subscribe((x) => x);
  }

  submitAnswer(ordinal: number, answer: String) {
    return this._http
      .post(
        `/exercise/api/${this._exercise.id}/question/${ordinal}`,
        JSON.stringify({ answer: answer }),
        {
          headers: this._httpPostHeaders,
        }
      )
      .map((resp) => resp.json());
  }

  markAsFinished() {
    return this._http
      .post(`/exercise/api/${this._exercise.id}/finished`, JSON.stringify({ finished: true }))
      .subscribe((resp) => resp);
  }

  onFocus() {
    this._logService.log(new LogEvent('Got focus', ''));
  }
  onBlur() {
    this._logService.log(new LogEvent('Lost focus', ''));
  }
}
