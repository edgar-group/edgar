import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  OnChanges,
  ChangeDetectionStrategy,
} from 'angular2/core';
// import {Router, RouteParams, ROUTER_DIRECTIVES} from 'angular2/router'; // CanDeactivate
import { Question } from './../common/question';

@Component({
  selector: 'q-history',
  template: `
    <div class="edgar-qs">
      <span
        *ngFor="#a of currAnswers; #i = index"
        class="edgar-qn"
        style="color: #{{ questionDifficultyColors[i] || '000' }} !important"
        [class.selected]="isSelected(i + 1)"
        (click)="onSelect(i + 1)"
      >
        <span class="badge">{{ i + 1 }}(<span [innerHTML]="getLabel(a)"></span>)</span>
      </span>
    </div>
  `,
  styles: [
    `
      .edgar-qs {
      }
      .edgar-qn {
        color: #36363c;
        cursor: pointer;
        font-size: 1.5rem;
        margin-right: 3px;
        border-radius: 0.25rem;
        border: transparent solid 1px;
        display: inline-block;
      }
      .selected {
        border: black solid 1px;
        background-color: aliceblue;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QHistory implements OnInit, OnChanges {
  // routerCanDeactivate(n, p)
  @Input() currQuestionOrdinal: number;
  @Input() currAnswers: any[];
  @Input() questionDifficultyColors: string[];
  @Output() questionSelected = new EventEmitter();
  constructor() {
    this.currQuestionOrdinal = 1;
  }
  getLabel(a: any): string {
    return Question.getAnswerLabel(a);
  }

  isSelected(ordinal: number) {
    return ordinal === this.currQuestionOrdinal;
  }

  onSelect(ordinal: number) {
    this.currQuestionOrdinal = ordinal;
    this.questionSelected.emit({ ordinal: ordinal });
  }
  ngOnInit() {}
  ngOnChanges() {}
}
