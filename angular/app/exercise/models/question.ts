import { Question } from './../../common/question';

export class ExerciseQuestion extends Question {
  constructor(
    public caption: string,
    ordinal: number,
    currAnswers: number[],
    content,
    answers,
    multiCorrect,
    modeDesc,
    type: string,
    public difficultyLevelId: number
  ) {
    super(
      caption,
      ordinal,
      currAnswers,
      content,
      answers,
      multiCorrect,
      modeDesc,
      type,
      null,
      null,
      null,
      false
    );
  }
  static getAnswerLabel(answers: any): string {
    if (answers) {
      if (answers instanceof Array && answers.length) {
        return answers
          .map(function (num) {
            return String.fromCharCode(97 - 1 + parseInt('' + num));
          })
          .join();
      } else if (typeof answers === 'string' && answers.trim() !== '') {
        return '<code/>';
      }
    }
    return '-';
  }
}
