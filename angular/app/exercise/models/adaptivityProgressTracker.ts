import { AdaptivityModelBehaviour } from './adaptivityModelBehaviour';
import { DifficultyLevel } from './difficultyLevel';

export class AdaptivityProgressTracker {
  public attemptHistory: { o: number; s: boolean }[] = [];

  constructor(
    public currentDifficultyLevelId: number,
    private _behaviour: AdaptivityModelBehaviour[],
    private _difficultyLevels: DifficultyLevel[]
  ) {}

  public attempt(ordinal: number, successful: boolean): any {
    this.attemptHistory.push({
      o: ordinal,
      s: successful,
    });

    const levelUp = this.checkIfLevelUp(),
      levelDown = this.checkIfLevelDown();

    if (levelUp) {
      const rv = this.levelUp();
      return rv == -1;
    } else if (levelDown) {
      this.levelDown();
    }

    return false;
  }

  public checkIfLevelUp(): any {
    const desc = this._behaviour.find(
      (x) => x.questionDifficultyLevelId === this.currentDifficultyLevelId
    );

    const correct = this.attemptHistory
      .slice(-desc.levelUpTracking)
      .reduce((sum, curr) => sum + (curr.s ? 1 : 0), 0);

    const levelUp =
      this.attemptHistory.length >= desc.levelUpTracking && correct >= desc.levelUpCorrect;

    // console.log(
    //   'correct: ',
    //   `${correct} / ${desc.levelUpCorrect} / ${desc.levelUpTracking}`,
    //   'level up: ',
    //   levelUp
    // );

    return levelUp;
  }

  public checkIfLevelDown(): any {
    const desc = this._behaviour.find(
      (x) => x.questionDifficultyLevelId === this.currentDifficultyLevelId
    );

    const incorrect = this.attemptHistory
      .slice(-desc.levelDownTracking)
      .reduce((sum, curr) => sum + (curr.s ? 0 : 1), 0);

    const levelDown =
      this.attemptHistory.length >= desc.levelDownTracking && incorrect >= desc.levelDownIncorrect;

    // console.log(
    //   'incorrect: ',
    //   `${incorrect} / ${desc.levelDownIncorrect} / ${desc.levelDownTracking}`,
    //   'level down: ',
    //   levelDown
    // );

    return levelDown;
  }

  public levelDown() {
    const currOrd = this._difficultyLevels.find(
      (dl) => dl.id === this.currentDifficultyLevelId
    ).ordinal;
    const nextDl = this._difficultyLevels.find((dl) => dl.ordinal === currOrd - 1);
    this.clearHistory();

    if (nextDl) {
      // console.log('leveling down');
      this.currentDifficultyLevelId = nextDl.id;
      return true;
    } else {
      return -1;
    }
  }

  public levelUp() {
    const currOrd = this._difficultyLevels.find(
      (dl) => dl.id === this.currentDifficultyLevelId
    ).ordinal;
    const nextDl = this._difficultyLevels.find((dl) => dl.ordinal === currOrd + 1);
    this.clearHistory();

    if (nextDl) {
      // console.log('leveling up');
      this.currentDifficultyLevelId = nextDl.id;
      return true;
    } else {
      // console.log('Exercise finished');
      return -1;
    }
  }

  public setDifficultyLevel(difficultyLevelId) {
    this.currentDifficultyLevelId = difficultyLevelId;
  }

  clearHistory() {
    this.attemptHistory = [];
  }
}
