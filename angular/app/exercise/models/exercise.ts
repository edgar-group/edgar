import { AdaptivityModelBehaviour } from './adaptivityModelBehaviour';
import { AdaptivityProgressTracker } from './adaptivityProgressTracker';
import { DifficultyLevel } from './difficultyLevel';

export class Exercise {
  constructor(
    public id: number,
    public title: string,
    public description: string,
    public behaviour: AdaptivityModelBehaviour[],
    public difficultyLevels: DifficultyLevel[],
    public adaptivityProgressTracker: AdaptivityProgressTracker,
    public cmSkin: string
  ) {}

  public getCurrentDifficultyLevel() {
    return this.difficultyLevels.find(
      (dl) => dl.id === this.adaptivityProgressTracker.currentDifficultyLevelId
    );
  }
}
