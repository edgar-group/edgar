export class Tutorial {
  constructor(
    public id: number,
    public title: string,
    public description: string,
    public steps: [
      {
        ordinal: number;
        title: string;
      }
    ],
    public cmSkin: string,
    public allowRandomAccess: boolean
  ) {}
}
