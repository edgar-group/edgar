import { TutorialQuestion } from './question';
import { StepHint } from './hints';

export class Step {
  constructor(
    public id: number,
    public ordinal: number,
    public title: string,
    public html: string,
    public hints: [StepHint],
    public question: TutorialQuestion
  ) {}
}
