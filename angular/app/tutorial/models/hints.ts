export class StepHint {
  constructor(public ordinal: number, public html: string) {}
}

export class AnswerHint {}

export class AbcHint extends AnswerHint {
  constructor(public id_question_answer: number, public html: string) {
    super();
  }
}

export class CodeHint extends AnswerHint {
  constructor(public ordinal: number, public trigger: string, public html: string) {
    super();
  }
}
