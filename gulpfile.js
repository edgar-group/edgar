const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const ts = require('gulp-typescript');
const mocha = require('gulp-mocha');

gulp.task('test', () => {
  return (
    gulp
      .src('./test/*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});
gulp.task('test2', () => {
  return (
    gulp
      .src('./test/*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          reporter: 'nyan',
        })
      )
  ); // {reporter: 'nyan'}
});
gulp.task('test-core', () => {
  return (
    gulp
      .src(['./test/*EvalLibCore*.js'], { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});

gulp.task('test-c', () => {
  return (
    gulp
      .src('./test/*CodeRunner*.js', { read: false })
      // gulp-mocha needs filepaths so you can't have any plugins before it
      .pipe(
        mocha({
          require: ['should'],
          //timeout: 3000
          //reporter: 'nyan'
        })
      )
  ); // {reporter: 'nyan'}
});

gulp.task('copy-angular', function () {
  return gulp
    .src([
      './node_modules/es6-shim/es6-shim.min.js',
      './node_modules/systemjs/dist/system-polyfills.js',
      './node_modules/angular2/bundles/angular2-polyfills.js',
      './node_modules/systemjs/dist/system.src.js',
      './node_modules/rxjs/bundles/Rx.js',
      './node_modules/angular2/bundles/angular2.dev.js',
      './node_modules/angular2/bundles/router.dev.js',
      './node_modules/angular2/bundles/http.dev.js',
    ])
    .pipe(gulp.dest('./public/lib/angular'));
});

gulp.task('start-nodemon', function (done) {
  var options = {
    execMap: {
      js: 'npm start --harmony --use_strict',
    },
    script: 'server.js',
    delay: 1500,
    ext: 'js html',
    env: {
      NODE_ENV: 'development',
      PORT: 1337,
      METRICS: true,
      ADMIN_EMAIL: 'edgar.webapp@gmail.com',
      LOG_LEVEL: 'debug',
    },
    ignore: ['.git', 'node_modules', 'ng'],
    done: done,
    events: {
      start: 'clear',
    },
    //watch: jsFiles
  };
  nodemon(options).on('restart', function (argument) {
    console.log('Restarting ...');
  });
});

gulp.task('compile-lib', function () {
  const tsProject = ts.createProject('lib/tsconfig.json');
  return tsProject.src().pipe(tsProject()).js.pipe(gulp.dest('lib'));
});

gulp.task('dev', gulp.series('compile-lib', 'copy-angular', 'start-nodemon'), function (done) {
  console.log('Good luck!');
  done();
});
